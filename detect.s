.include "global.inc"
.include "vdc.inc"

;			C/Line	Lines	Clock		Val
;PAL		63		312		985248		516%312 = 204 cc
;NTSC		65		263		1022730		508%263 = 245 f5
;NTSC_OLD	64		262		1022730		512%262 = 250 fa
;DREAN		65		312		1023440		508%312 = 196 c4
;
;16384/63 = 260+256 = 516   
;16384/64 = 256+256 = 512 
;16384/65 = 252+256 = 508

.segment "START"

.export system
system: .byte 0

.export sidModel
sidModel: .byte 0

.export vdcPresent
vdcPresent: .byte 0

.segment "MAIN"

.export systemDetect
.proc systemDetect
		sei
		lda #0
		sta $d011
		sta $dc0e
		sta $dc0f
		bit $d011
		bpl *-3
		bit $d011
		bmi *-3

		lda #$7f
		sta $dc0d
		bit $dc0d

		lda #<(16384-1)
		sta $dc04
		lda #>(16384-1)
		sta $dc05
		lda #%10011001

		bit $d011
		bpl *-3
		
		sta $dc0e

		:lda $dc0d
		beq :-
		lda $d012

		ldx #SYSTEM_PAL ;SYSTEM_UNKNOWN (default PAL and live with gfx glitches instead of total crash)
		cmp #$cc
		bne :+
			ldx #SYSTEM_PAL
		:
		cmp #$f5
		bne :+
			ldx #SYSTEM_NTSC
		:
		cmp #$fa
		bne :+
			ldx #SYSTEM_NTSC_OLD
		:
		cmp #$c4
		bne :+
			ldx #SYSTEM_DREAN
		:
		stx system

		; Detect SID-model
		lda #$ff 	;Set frequency in voice 3 to $ffff
        sta $d412	;...and set testbit (other bits doesn''t matter) in $d412 to disable oscillator
        lda #$00    ;Sawtooth wave and gatebit OFF to start oscillator again.
        sta $d412
		lda #$20
		sta $d40e
		sta $d40f
		lda #%00110001
		sta $d412
		ldx #0
		stx TMP
		:
			lda $d41b
	        cmp TMP
	        bcc :+
    	     	sta TMP
			:
			dex
        bne :--
		lda #%00110000
		sta $d412
		lda TMP
		cmp #$c0
		lda #$00
		rol
        sta sidModel

		ldx #0
		ldy #5
		stx VDCADR
		:
			bit VDCADR
			bmi initVDC
			inx
			bne :-
			dey
		bpl :-
		rts

; Init code ripped from C128 Kernel
initVDC:
		inc vdcPresent

		ldx #0		;initialize 8563 (NTSC)
		jsr vdc_init
		lda VDCADR
		and #$07
		beq :+		;...branch if old 8563R7
			ldx #vdcpat-vdctbl
			jsr vdc_init	;...else apply -R8 patches
		:

		lda system
		cmp #SYSTEM_NTSC
		beq :+
		cmp #SYSTEM_NTSC_OLD
		beq :+
			ldx #vdcpal-vdctbl
			jsr vdc_patch	;...else apply PAL patches (318020-04 fix)
		:

		rts

vdc_init:
		ldy vdctbl,x	;get 8563 register #
		bmi :+			;...branch if end-of-table
			inx
			lda vdctbl,x	;get data for this register
			inx
			sty VDCADR
			sta VDCDAT
		bpl vdc_init	;always
		:inx
		rts

vdc_patch:
		ldy #0		;vdc register #0
		lda #$7f	;PAL horizontal total
		sty VDCADR
		sta VDCDAT
		jmp vdc_init	;resume 'normal' PAL init & RTS

vdctbl:
		.byte  0,$7e, 1,$50, 2,$66, 3,$49, 4,$20, 5,$00, 6,$19, 7,$1d	;8563 NTSC
		.byte  8,$00, 9,$07,10,$20,11,$07,12,$00,13,$00,14,$00,15,$00
		.byte 20,$08,21,$00,23,$08,24,$20,25,$40,26,$f0,27,$00,28,$20
		.byte 29,$07,34,$7d,35,$64,36,$05,22,$78,$ff

vdcpat:
		.byte 25,$47, $ff		;8563 patches

vdcpal:
		.byte  4,$26, 7,$20,$ff		;8563 PAL (318020-04 fix. see vdc_patch too)
.endproc
