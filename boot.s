.include "global.inc"

.segment "EAPI"
	.incbin "eapi/out/eapi-am29f040-14",2

.segment "NAME"
	.byte "ef-nAME:eOTb i     v",VERSION

.segment "VECTORS"
	.byte <_rti,>_rti
	.byte <boot,>boot
	.byte <_rti,>_rti

.segment "BOOT"

.proc boot
	; Setup stack, cpu-io and memory
	sei
	ldx #$ff
	txs
	cld
	lda #$2f
	sta 0
	lda #$37
	sta 1
	lda #8
	sta $d016
	lda #$fc
	sta $d030
	lda #$3f
	sta $dd02
	lda #0
	sta $d011
	sta $d01a
	dec $d019
	lda #6
	sta $d020

	; Scan keyboard and perform normal reset is cmd or q is pressed
	lda #$7f
	sta $dc00
	ldx #$ff
	stx $d02f
	stx $dc02
	inx
	stx $dc03
	lda $dc01
	stx $dc02
	stx $dc00
	and #$e0
	cmp #$e0
	beq kill_end
		ldx #<(kill_end-kill)
		:
			lda kill,x
			sta $0100,x
			dex
		bpl :-
		jmp $0100
kill:
		lda #$04
		sta $de02
		jmp ($fffc)
kill_end:

	jsr initKernel

	.assert __START_RUN__+__START_SIZE__-1 < $1000, error, "START segment must not go above $fff due to Ultimax mode"

	; Copy start to RAM
	lda #<__START_LOAD__
	sta MSRC+0
	lda #>__START_LOAD__
	sta MSRC+1
	lda #<__START_RUN__
	sta MDST+0
	lda #>__START_RUN__
	sta MDST+1
	ldx #<((__START_SIZE__+$ff)/$100)
	ldy #0
	:
			lda (MSRC),y
			sta (MDST),y
			iny
		bne :-
		inc MSRC+1
		inc MDST+1
		dex
	bne :-

	jmp callMain			
.endproc

_rti:	rti

.segment "MAIN"
.proc initKernel
	; Swap out CART ROM HI and swap in KERNEL, keep CART ROM LO (where this is)
	lda #$86
	sta $de02

	jsr $ff87 ; initialize ram & buffers
	jsr $fda3 ; initialize i/o
	jsr $ff8a ; restore vectors

	; Swap back CART ROM HI
	lda #$85
	sta $de02
	rts
.endproc
