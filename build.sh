#!/bin/sh

#java --version | grep "java 10" > /dev/null || { echo "Error: Java not found or not at version 10 or later"; exit 1; }
#which g++ > /dev/null || { echo "Error: g++ not found"; exit 1; }
#which node > /dev/null || { echo "Error: node not found"; exit 1; }

tar xvfz pcdata.tgz

make -C tools/dog/ || exit 1
make -C converters/eoblib/ || exit 1
make -C converters/common/ || exit 1
make -C converters/combiner/ || exit 1
make -C converters/inf/ || exit 1
make -C tools/EotBGfxEditor/ deploy_assets || exit 1
make -C converters/items/ || exit 1
make -C converters/doors/ || exit 1
make -C converters/decorations/ extract convert || exit 1
make -C converters/monsters/converted/ || exit 1
make -C converters/monsters/ || exit 1
make -C converters/outtake/ || exit 1  
make -C converters/deathanim/ || exit 1
make -C converters/intro/ || exit 1
make -C converters/ending/ || exit 1
make -C converters/portal/ || exit 1
