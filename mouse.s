.include "global.inc"

.segment "START"
.export mouseType
mouseType: 		.res 1

.export isMouseReal
isMouseReal:	.res 1

stateVars:
xpos:			.res 2
ypos:			.res 2
actualxpos:		.res 2
actualypos:		.res 2
.export currentxpos
currentxpos:	.res 2
.export currentypos
currentypos:	.res 2
dx:				.res 2
dy:				.res 2
oldPotX:		.res 1
oldPotY:		.res 1
tmp:			.res 1
buttons:		.res 1
acceleration: 	.res 1
justPlace:		.res 1
framesPressed:	.res 1
.export autoFireLimit
autoFireLimit:	.res 1

.export mouseConnected
mouseConnected:	.res 1
stateVars_end:

.segment "MOUSE"
.define xmin 0
.define xmax 319
.define ymin 0
.define ymax 199

.export mouse_init
.proc mouse_init
		txa
		pha

		lda #0
		ldx #62
		:
			sta mousepointer,x
			dex
		bpl :-
		sta POINTER_ITEM+0
		sta POINTER_ITEM+1
		jsrf updatePointerSprite

		pla
		tax
		; Fallthrough
.endproc

.export mouse_reload
.proc mouse_reload
		stx mouseType

		lda #0
		ldx #stateVars_end-stateVars-1
		:
			sta stateVars,x
			dex
		bpl :-

		lda #<((xmin+xmax)/2)
		sta xpos+0
		sta currentxpos+0
		sta actualxpos+0
		lda #>((xmin+xmax)/2)
		sta xpos+1
		sta currentxpos+1
		sta actualxpos+1
		lda #<((ymin+ymax)/2)
		sta ypos+0
		sta currentypos+0
		sta actualypos+0
		lda #>((ymin+ymax)/2)
		sta ypos+1
		sta currentypos+1
		sta actualypos+1

		lda #$ff
		sta MOUSEEVENT+0
		lda #0
		sta MOUSEEVENT+1
		sta MOUSEEVENT+2
		sta MOUSEEVENT+3
		sta dx+0
		sta dx+1
		sta dy+0
		sta dy+1
		sta buttons

		ldx mouseType
		lda accelerationEnabled,x
		sta acceleration
		lda justPlaceEnabled,x
		sta justPlace

		lda $d419
		sta oldPotX
		lda $d41a
		sta oldPotY

		jsr placeSprite

		rts
.endproc

.export mouse_vic
.proc mouse_vic
		lda #MOUSE_COLOR
		sta $d027+1
		lda #<(mousepointer/$40)
		sta spriteptrs+1
		sta spriteptrs2+1
		rts
.endproc

.export placeSprite
.proc placeSprite
		.pushseg
		.segment "BSS"
			deltaX: .res 1
			deltaY: .res 1
		.popseg

		lda justPlace
		beq :+
			jmp skipFilter
		:

		lda acceleration
		beq :+
			clc
			lda dx+0
			adc #64
			tax

			clc
			lda xpos+0
			adc accelerationTable_lo,x
			tay
			lda xpos+1
			adc accelerationTable_hi,x
			tax
			jmp :++
		:
			clc
			lda xpos+0
			adc dx+0
			tay
			lda xpos+1
			adc dx+1
			tax
		:
		jsr limitX

		lda acceleration
		beq :+
			clc
			lda dy+0
			adc #64
			tax

			sec
			lda ypos+0
			sbc accelerationTable_lo,x
			tay
			lda ypos+1
			sbc accelerationTable_hi,x
			tax
			jmp :++
		:
			sec
			lda ypos+0
			sbc dy+0
			tay
			lda ypos+1
			sbc dy+1
			tax
		:
		jsr limitY

		; Filter xpos
		sec
		lda xpos+0
		sbc currentxpos+0
		cmp #<-1
		bmi :+
		cmp #2
		bpl :+
		jmp :++
		:
			lda xpos+0
			sta currentxpos+0
			lda xpos+1
			sta currentxpos+1
		:

		; Filter ypos
		sec
		lda ypos+0
		sbc currentypos+0
		cmp #<-1
		bmi :+
		cmp #2
		bpl :+
		jmp :++
		:
			lda ypos+0
			sta currentypos+0
			lda ypos+1
			sta currentypos+1
		:

		; Move actual towards current
skipFilter:
		clc
		lda currentxpos+0
		adc actualxpos+0
		sta actualxpos+0
		lda currentxpos+1
		adc actualxpos+1
		ror
		sta actualxpos+1
		ror actualxpos+0
		clc
		lda currentypos+0
		adc actualypos+0
		sta actualypos+0
		lda currentypos+1
		adc actualypos+1
		ror
		sta actualypos+1
		ror actualypos+0

		; Reset delta
		lda #0
		sta dx+0
		sta dx+1
		sta dy+0
		sta dy+1

		; Calculate hotspot
		ldx #$18-2 ;compensate for shadow to the left
		ldy #$32
		lda POINTER_ITEM+0
		ora POINTER_ITEM+1
		beq :+
			ldx #$18-8
			ldy #$32-8
		:
		stx deltaX
		sty deltaY

		; Place sprite
		clc
		lda actualxpos+0
		adc deltaX
		sta $d000+1*2
		lda actualxpos+1
		adc #0
		beq :+
			lda $d010
			ora #(1<<1)
			jmp :++
		:
			lda $d010
			and #!(1<<1)
		:
		sta $d010

		clc
		lda actualypos+0
		adc deltaY
		sta $d001+1*2

		rts

accelerationTable_lo:
		.incbin "mouse_acc.bin"
accelerationTable_hi = accelerationTable_lo+128
.endproc

.proc limitX
		; Limit the X coordinate to the bounding box
	    cpy #<xmin
		sbc #>xmin
        bpl :+
        	ldy #<xmin
	       	ldx #>xmin
	       	jmp :++
		:
		txa
        cpy #<xmax
        sbc #>xmax
        bmi :+
        	ldy #<xmax
			ldx #>xmax
		:
		sty xpos+0
        stx xpos+1
        rts
.endproc

.proc limitY
		; Limit the Y coordinate to the bounding box
    	cpy #<ymin
    	sbc #>ymin
    	bpl :+
        	ldy #<ymin
    		ldx #>ymin
    		jmp :++
    	:
       	txa
    	cpy #<ymax
    	sbc #>ymax
    	bmi :+
    		ldy #<ymax
    		ldx #>ymax
		:
		sty ypos+0
    	stx ypos+1
        rts
.endproc

.include "mouse_1351.inc"
.include "mouse_none.inc"

.export mouse_irq
.proc mouse_irq
		lda pointerEnabled
		beq :+
			; First track motion with the selected device
			ldx mouseType
			lda INDJMP+0
			pha
			lda INDJMP+1
			pha
			lda motionHandler_lo,x
			sta INDJMP+0
			lda motionHandler_hi,x
			sta INDJMP+1
			jsr INDJMPOP
			pla
			sta INDJMP+1
			pla
			sta INDJMP+0
		:

		lda MOUSEEVENT+MouseEvent::buttons
		cmp #$ff
		beq :+
			; Unprocessed event, abort
			rts
		:

		ldx mouseType
		lda INDJMP+0
		pha
		lda INDJMP+1
		pha
		lda buttonHandler_lo,x
		sta INDJMP+0
		lda buttonHandler_hi,x
		sta INDJMP+1
		jsr INDJMPOP
		tax
		pla
		sta INDJMP+1
		pla
		sta INDJMP+0
		txa

		bne :+
			; No mouse button pressed, abort
			sta buttons
			sta framesPressed
			rts
		:

		cmp buttons
		bne :+++
			; Still the same mouse button, abort
			ldx autoFireLimit
			beq :++
				cpx framesPressed
				bne :+
					lda #0
					jmp :+++
				:
				inc framesPressed
			:
			rts
		:

		; Generate event (but only for 0->1 transitions)
		sta tmp
		lda buttons
		eor #3
		and tmp
		bne :+
			; No 0->1 transition
			lda tmp
			sta buttons
			rts
		:
		sta MOUSEEVENT+MouseEvent::buttons
		lda tmp
		sta buttons

		lda currentxpos+0
		sta MOUSEEVENT+MouseEvent::xpos+0
		lda currentxpos+1
		sta MOUSEEVENT+MouseEvent::xpos+1
		lda currentypos
		sta MOUSEEVENT+MouseEvent::ypos

		rts
.endproc

.export fakeMouseEvent 
.proc fakeMouseEvent
		stx MOUSEEVENT+MouseEvent::buttons
		lda currentxpos+0
		sta MOUSEEVENT+MouseEvent::xpos+0
		lda currentxpos+1
		sta MOUSEEVENT+MouseEvent::xpos+1
		lda currentypos+0
		sta MOUSEEVENT+MouseEvent::ypos
		rts
.endproc

accelerationEnabled:
		.byte 1 ;MOUSETYPE_1351_ACC
		.byte 0 ;MOUSETYPE_1351_NOACC
		.byte 0 ;MOUSETYPE_NONE

justPlaceEnabled:
		.byte 0
		.byte 0
		.byte 1

motionHandler_lo:
		.byte <mouse_1351
		.byte <mouse_1351
		.byte <mouse_none

motionHandler_hi:
		.byte >mouse_1351
		.byte >mouse_1351
		.byte >mouse_none

buttonHandler_lo:
		.byte <buttons_1351
		.byte <buttons_1351
		.byte <buttons_none

buttonHandler_hi:
		.byte >buttons_1351
		.byte >buttons_1351
		.byte >buttons_none

