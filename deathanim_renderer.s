.include "global.inc"

; x=frame index
.proc deathanim_draw_normal
	jsr draw_setup
	jsr drawGfxObject
	rts
.endproc

.proc deathanim_draw_flipped ;Door
	jsr draw_setup
	lda #3
	sta XPOS
	lda #1
	sta YPOS
	jsr drawGfxObjectWithRowSkip
	rts
.endproc

.proc draw_setup
	lda _width,x
	sta WIDTH
	lda _height,x
	sta HEIGHT
	lda _dx,x
	sta XPOS
	lda _dy,x
	sta YPOS

	lda _data_ptr_lo,x
	sta SRC_DATA+0	
	lda _data_ptr_hi,x
	sta SRC_DATA+1

	lda _char_offset_lo,x
	sta TMP+0
	lda _char_offset_hi,x
	sta TMP+1

	clc
	lda #<_screen
	adc TMP+0
	sta SRC_SCREEN+0
	lda #>_screen
	adc TMP+1
	sta SRC_SCREEN+1

	clc
	lda #<_d800
	adc TMP+0
	sta SRC_D800+0
	lda #>_d800
	adc TMP+1
	sta SRC_D800+1

	rts
.endproc
