.include "global.inc"

; ByteBoozer Decruncher    /HCL May.2003
; B2 Decruncher            December 2014

; call: Y = AddrLo
;       X = AddrHi

;Variables..        #Bytes

bits = BB_BITS
put = BB_DST

.segment "START"
.macro GetNextBit
	.scope
	asl z:bits
	bne __skip
		jsr GetNewBits
	__skip:
	.endscope
.endmacro

.macro GetLen
	.scope
	lda #1
	__loop:
		GetNextBit
		bcc __break
		GetNextBit
		rol
	bpl __loop
	__break:
	.endscope
.endmacro

.export decrunch
.proc decrunch
		clc
to:
		sty Get1+1
		sty Get2+1
		sty Get3+1
		stx Get1+2
		stx Get2+2
		stx Get3+2

		lda 1
		sta _1+1

		lda #$94 ;sty zp,x
		bcc :+
			lda #$b4; ldy zp,x
		:
		sta sm

		ldx #0
		:
			jsr GetNewBits
			sm:sty put-1,x
			cpx #2
		bcc :-

		lda #$80
		sta bits

DLoop:	
		GetNextBit

		bcs Match
Literal:
		; Literal run.. get length.
		GetLen
		sta LLen+1

		ldy #0
		:
			Get3:lda $feed,x
			inx
			bne :+
				jsr GnbInc
			:
			sta (put),y
			iny
			LLen:cpy #0
		bne :--

		clc
		tya
		adc put
		sta put
		bcc :+
			inc put+1
		:
		iny
		beq DLoop

	; Has to continue with a match..
Match:
	; Match.. get length.
	GetLen
	sta MLen+1

	; Length 255 -> EOF
	cmp #$ff
	beq End

	; Get num bits
	cmp #2
	lda #0
	rol
	GetNextBit
	rol
	GetNextBit
	rol
	tay
	lda Tab,y
	beq :++
		; Get bits < 8
		:
			GetNextBit
			rol
		bcs :-
		bmi MShort
	:

	; Get byte
	eor #$ff
	tay
	Get2:lda $feed,x
	inx
	bne :+
		jsr GnbInc
	:
	jmp Mdone

MShort:
	ldy #$ff

Mdone:
	;clc
	adc put
	sta MLda+1
	tya
	adc put+1
	sta MLda+2

	; Copy runs has to read and write from RAM only!
	lda #$31
	sta 1
	ldy #$ff
	:	
		iny
		MLda:lda $beef,y
		sta (put),y
		MLen:cpy #0
	bne :-
_1:	lda #$37
	sta 1

	;sec
	tya
	adc put
	sta put
	bcc :+
	inc put+1
	:

	jmp DLoop

End:
	rts

GetNewBits:
	Get1:ldy $feed,x
	sty bits
	rol bits
	inx
	bne :+
GnbInc:
		inc Get1+2
		inc Get2+2
		inc Get3+2
	:
	rts

Tab:
	; Short offsets
	.byte %11011111 ; 3
	.byte %11111011 ; 6
	.byte %00000000 ; 8
	.byte %10000000 ; 10
	; Long offsets
	.byte %11101111 ; 4
	.byte %11111101 ; 7
	.byte %10000000 ; 10
	.byte %11110000 ; 13
.endproc

.export decrunchTo
.proc decrunchTo
	sec
	jmp decrunch::to
.endproc

;a=bank, x=hibyte, y=lobyte
.export decrunchFarToFreeRam 
.proc decrunchFarToFreeRam
		sta :+ +1
		lda BANK
		pha
		:lda #0
		sta BANK
		sta $de00

		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1

		jsr decrunchTo

		pla
		sta BANK
		sta $de00

		rts
.endproc

;a=bank, x=hibyte, y=lobyte
;.export freeRamEnding
;freeRamEnding = __ENDING_BSS_RUN__ + __ENDING_BSS_SIZE__
.export decrunchFarToFreeRamEnding 
.proc decrunchFarToFreeRamEnding
		sta :+ +1
		lda BANK
		pha
		:lda #0
		sta BANK
		sta $de00

		lda #<freeRamEnding
		sta BB_DST+0
		lda #>freeRamEnding
		sta BB_DST+1

		jsr decrunchTo

		pla
		sta BANK
		sta $de00

		rts
.endproc

;a=bank, x=hibyte, y=lobyte
.export decrunchFar
.proc decrunchFar
		sta :+ +1
		lda BANK
		pha
		:lda #0
		sta BANK
		sta $de00

		jsr decrunchTo

		pla
		sta BANK
		sta $de00

		rts
.endproc
