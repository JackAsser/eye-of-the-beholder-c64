.include "global.inc"

.segment "MAIN"

.export swappingMemberTimerHandler
.proc swappingMemberTimerHandler
		lda swappingFlipFlop
		eor #1
		sta swappingFlipFlop
		ldx SWAPPING_MEMBER_INDEX
		inc partyMemberStatsChanged,x

		rts
.endproc