.include "global.inc"

.segment "ZP": zeropage
streamPos:		.res 2

.segment "START"
.export muted
	muted:		.byte 1

.segment "BSS"
bss_start:
	freq1: 			.res 2
	freq2: 			.res 2
	freq3: 			.res 2
	newsustain1: 	.res 1
	oldsustain1: 	.res 1
	newsustain2: 	.res 1
	oldsustain2: 	.res 1
	newsustain3: 	.res 1
	oldsustain3: 	.res 1
	newwaveform1: 	.res 1
	oldwaveform1: 	.res 1
	newwaveform2: 	.res 1
	oldwaveform2: 	.res 1
	newwaveform3: 	.res 1
	oldwaveform3: 	.res 1
	streamEnd: 		.res 2
	currentVolume:	.res 1
	requestToSwitch:.res 1
	okToSwitch:	.res 1
bss_stop:

.define filter		$b0
.define	resonance	$b7
.define cutoff		$0800

.segment "AUDIO"
.export audio_playSoundAtPartyPosition
.proc audio_playSoundAtPartyPosition
		soundIndex = ARGS+0

		ldy soundIndex
		ldx idToIndex,y
		bmi :+
			ldy #15
			jsr selectSample
		:
		rts
.endproc

.export audio_playSound
.proc audio_playSound
		soundIndex = ARGS+0
		position = ARGS+1

		lda soundIndex
		pha
		lda position+0
		sta ARGS+0
		lda position+1
		sta ARGS+1
		lda partyPosition+0
		sta ARGS+2
		lda partyPosition+1
		sta ARGS+3
		jsrf gameState_getDistance
		lda ARGS+0

		; Volume = 15 - distance*2
		asl
		sta TMP
		sec
		lda #15
		sbc TMP
		bpl :+
			pla
			rts
		:
		sta TMP

		pla
		tay
		ldx idToIndex,y
		bmi :+
			ldy TMP
			jsr selectSample
		:
		rts
.endproc

; x = effectIndex
.export audio_playSoundEffect
.proc audio_playSoundEffect
		lda idToIndex,x
		bmi :+
			tax
			ldy #15
			jsr selectSample
		:
		rts
.endproc

idToIndex: .byte <-1,4,27,8,8,9,4,4,19,10,19,33,4,25,3,34,34,5,2,<-1,<-1,26,7,32,11, 6,6,3,32,2,<-1,11,27,16,8,12,21,1,31,27,22,33,31,1,22,1,23,27,23,28,22,15,29,23,0,17,30,18,14,<-1,28,23,27,23,1,23,1,23,27,23,<-1,23,0,27,23,22,4,<-1,2,32,20,3,<-1,<-1,<-1,13,<-1,13,11,13,<-1,19,19,19,20,20,13,20,13,13,19,20,20,34,19,19,19,20,20,20,20,19,20,19,19,19,19,20,19

.proc sample0
	.incbin "audio/new50hz/beastatk1.sam.raw.c64data.raw"
.endproc

.proc sample1
	.incbin "audio/new50hz/blade1.sam.raw.c64data.raw"
.endproc

.proc sample2
	.incbin "audio/new50hz/bump1.sam.raw.c64data.raw"
.endproc

.proc sample3
	.incbin "audio/new50hz/chain1.sam.raw.c64data.raw"
.endproc

.proc sample4
	.incbin "audio/new50hz/click1.sam.raw.c64data.raw"
.endproc

.proc sample5
	.incbin "audio/new50hz/creak1.sam.raw.c64data.raw"
.endproc

.proc sample6
	.incbin "audio/new50hz/dart1.sam.raw.c64data.raw"
.endproc

.proc sample7
	.incbin "audio/new50hz/death1.sam.raw.c64data.raw"
.endproc

.proc sample8
	.incbin "audio/new50hz/door1.sam.raw.c64data.raw"
.endproc

.proc sample9
	.incbin "audio/new50hz/doorslam1.sam.raw.c64data.raw"
.endproc

.proc sample10
	.incbin "audio/new50hz/eat1.sam.raw.c64data.raw"
.endproc

.proc sample11
	.incbin "audio/new50hz/electric1.sam.raw.c64data.raw"
.endproc

.proc sample12
	.incbin "audio/new50hz/explode1.sam.raw.c64data.raw"
.endproc

.proc sample13
	.incbin "audio/new50hz/fireball1.sam.raw.c64data.raw"
.endproc

.proc sample14
	.incbin "audio/new50hz/flayeratk1.sam.raw.c64data.raw"
.endproc

.proc sample15
	.incbin "audio/new50hz/houndatk1.sam.raw.c64data.raw"
.endproc

.proc sample16
	.incbin "audio/new50hz/hum1.sam.raw.c64data.raw"
.endproc

.proc sample17
	.incbin "audio/new50hz/kuotoamov1.sam.raw.c64data.raw"
.endproc

.proc sample18
	.incbin "audio/new50hz/leechmov1.sam.raw.c64data.raw"
.endproc

.proc sample19
	.incbin "audio/new50hz/magica1.sam.raw.c64data.raw"
.endproc

.proc sample20
	.incbin "audio/new50hz/magicb1.sam.raw.c64data.raw"
.endproc

.proc sample21
	.incbin "audio/new50hz/mantismov1.sam.raw.c64data.raw"
.endproc

.proc sample22
	.incbin "audio/new50hz/move1.sam.raw.c64data.raw"
.endproc

.proc sample23
	.incbin "audio/new50hz/move21.sam.raw.c64data.raw"
.endproc

;Not used
.proc sample24
	.incbin "audio/new50hz/move31.sam.raw.c64data.raw"
.endproc

.proc sample25
	.incbin "audio/new50hz/passage1.sam.raw.c64data.raw"
.endproc

.proc sample26
	.incbin "audio/new50hz/playhit1.sam.raw.c64data.raw"
.endproc

.proc sample27
	.incbin "audio/new50hz/playswing1.sam.raw.c64data.raw"
.endproc

.proc sample28
	.incbin "audio/new50hz/rustatk1.sam.raw.c64data.raw"
.endproc

.proc sample29
	.incbin "audio/new50hz/scream1.sam.raw.c64data.raw"
.endproc

.proc sample30
	.incbin "audio/new50hz/sloshsuck1.sam.raw.c64data.raw"
.endproc

.proc sample31
	.incbin "audio/new50hz/spidermov1.sam.raw.c64data.raw"
.endproc

.proc sample32
	.incbin "audio/new50hz/text1.sam.raw.c64data.raw"
.endproc

.proc sample33
	.incbin "audio/new50hz/throw1.sam.raw.c64data.raw"
.endproc

.proc sample34
	.incbin "audio/new50hz/undead1.sam.raw.c64data.raw"
.endproc

samples_lo: .byte <sample0,<sample1,<sample2,<sample3,<sample4,<sample5,<sample6,<sample7,<sample8,<sample9,<sample10,<sample11,<sample12,<sample13,<sample14,<sample15,<sample16,<sample17,<sample18,<sample19,<sample20,<sample21,<sample22,<sample23,<sample24,<sample25,<sample26,<sample27,<sample28,<sample29,<sample30,<sample31,<sample32,<sample33,<sample34
samples_hi: .byte >sample0,>sample1,>sample2,>sample3,>sample4,>sample5,>sample6,>sample7,>sample8,>sample9,>sample10,>sample11,>sample12,>sample13,>sample14,>sample15,>sample16,>sample17,>sample18,>sample19,>sample20,>sample21,>sample22,>sample23,>sample24,>sample25,>sample26,>sample27,>sample28,>sample29,>sample30,>sample31,>sample32,>sample33,>sample34
sampleSize_lo: .byte <.sizeof(sample0),<.sizeof(sample1),<.sizeof(sample2),<.sizeof(sample3),<.sizeof(sample4),<.sizeof(sample5),<.sizeof(sample6),<.sizeof(sample7),<.sizeof(sample8),<.sizeof(sample9),<.sizeof(sample10),<.sizeof(sample11),<.sizeof(sample12),<.sizeof(sample13),<.sizeof(sample14),<.sizeof(sample15),<.sizeof(sample16),<.sizeof(sample17),<.sizeof(sample18),<.sizeof(sample19),<.sizeof(sample20),<.sizeof(sample21),<.sizeof(sample22),<.sizeof(sample23),<.sizeof(sample24),<.sizeof(sample25),<.sizeof(sample26),<.sizeof(sample27),<.sizeof(sample28),<.sizeof(sample29),<.sizeof(sample30),<.sizeof(sample31),<.sizeof(sample32),<.sizeof(sample33),<.sizeof(sample34)
sampleSize_hi: .byte >.sizeof(sample0),>.sizeof(sample1),>.sizeof(sample2),>.sizeof(sample3),>.sizeof(sample4),>.sizeof(sample5),>.sizeof(sample6),>.sizeof(sample7),>.sizeof(sample8),>.sizeof(sample9),>.sizeof(sample10),>.sizeof(sample11),>.sizeof(sample12),>.sizeof(sample13),>.sizeof(sample14),>.sizeof(sample15),>.sizeof(sample16),>.sizeof(sample17),>.sizeof(sample18),>.sizeof(sample19),>.sizeof(sample20),>.sizeof(sample21),>.sizeof(sample22),>.sizeof(sample23),>.sizeof(sample24),>.sizeof(sample25),>.sizeof(sample26),>.sizeof(sample27),>.sizeof(sample28),>.sizeof(sample29),>.sizeof(sample30),>.sizeof(sample31),>.sizeof(sample32),>.sizeof(sample33),>.sizeof(sample34)

.proc selectSample
		lda muted
		beq :+
			rts
		:

		cpy currentVolume
		beq :+
		bcs :+
			rts
		:

		inc requestToSwitch
		:lda okToSwitch
		beq :-

		clc
		lda samples_lo,x
		sta streamPos+0
		adc sampleSize_lo,x
		sta streamEnd+0
		lda samples_hi,x
		sta streamPos+1
		adc sampleSize_hi,x
		sta streamEnd+1

		lda #0
		sta requestToSwitch

		sty currentVolume

		lda #$88
		sta $d402+14
		sta $d403+14
		sta $d402
		sta $d403
		sta $d402+7
		sta $d403+7

		lda #$f0
		sta $d406+14
		sta $d406
		sta $d406+7

		lda #<(cutoff&7)
		sta $d415
		lda #>(cutoff)
		sta $d416
		lda #resonance
		sta $d417
		lda #$f|filter
		sta $d418

		rts
.endproc

.export audio_init
.proc audio_init
		lda #1
		sta muted
		lda #0
		sta currentVolume
		sta requestToSwitch
		sta okToSwitch

reset:
		ldx #$18
		lda #0
		:
			sta $d400,x
			dex
		bpl :-

		lda #$ff
		sta $d400+14
		sta $d401+14
		sta $d400
		sta $d401
		sta $d400+7
		sta $d401+7

		lda #$f0
		sta newsustain1
		sta oldsustain1
		sta newsustain2
		sta oldsustain2
		sta newsustain3
		sta oldsustain3
		lda #$00
		sta newwaveform1
		sta newwaveform2
		sta newwaveform3
		lda #$11
		sta oldwaveform1
		sta oldwaveform2
		sta oldwaveform3

		rts
.endproc
.export audio_reset = audio_init::reset

.export audio_irq
.proc audio_irq
		lda muted
		beq :+
			rts
		:

		lda requestToSwitch
		beq :+
			lda #0
			sta $d404+0
			sta $d404+7
			sta $d404+14
			lda #1
			sta okToSwitch
			rts
		:
		sta okToSwitch

		lda currentVolume
		beq :+
			jmp player
		:

		sta $d404+0
		sta $d404+7
		sta $d404+14

		rts
.endproc

.proc player
			; Read 7 bytes of sample data
			ldy #$00
			lda (streamPos),y
			sta freq1+$01
			iny
			lda (streamPos),y
			sta freq2+$01
			iny
			lda (streamPos),y
			sta freq3+$01
			iny
			lax (streamPos),y
			lsr
			lsr
			lsr
			lsr
			sta freq1+$00
			txa
			and #$0f
			sta freq2+$00
			iny
			lax (streamPos),y
			lsr
			lsr
			lsr
			lsr
			sta freq3+$00
			txa
			asl
			asl
			asl
			asl
			ora currentVolume
			tay
			lda susscale,y
			sta newsustain1
			ldy #5
			lax (streamPos),y
			and #$f0
			ora currentVolume
			tay
			lda susscale,y
			sta newsustain2
			txa
			asl
			asl
			asl
			asl
			ora currentVolume
			tay
			lda susscale,y
			sta newsustain3
			ldy #6
			lax (streamPos),y
			lsr
			lsr
			lsr
			lsr
			and #$03
			tay
			lda wavetab,y
			sta newwaveform1
			txa
			lsr
			lsr
			and #$03
			tay
			lda wavetab,y
			sta newwaveform2
			txa
			and #$03
			tay
			lda wavetab,y
			sta newwaveform3
			clc
			lda freq1+$01
			rol
			rol freq1+$00
			rol
			rol freq1+$00
			rol
			rol freq1+$00
			rol
			rol freq1+$00
			sta freq1+$01
			
			clc
			lda freq2+$01
			rol
			rol freq2+$00
			rol
			rol freq2+$00
			rol
			rol freq2+$00
			rol
			rol freq2+$00
			sta freq2+$01

			clc
			lda freq3+$01
			rol
			rol freq3+$00
			rol
			rol freq3+$00
			rol
			rol freq3+$00
			rol
			rol freq3+$00
			sta freq3+$01

			; Increase read pointer
			lda streamPos+$00
			clc
			adc #$07
			sta streamPos+$00
			bcc :+
				inc streamPos+$01
			:

			; Check end
			lda streamPos+$00
			cmp streamEnd+0
			bne :+
			lda streamPos+$01
			cmp streamEnd+1
			bne :+
				lda #0
				sta currentVolume
				rts
			:
		
			; Update freq / sustain
			lda newsustain1
			bne :+
				lda #$10
				sta newsustain1
				lda #$00
				sta freq1+0
				sta freq1+1
			:
			
			lda newsustain2
			bne :+
				lda #$10
				sta newsustain2
				lda #$00
				sta freq2+0
				sta freq2+1
			:

			lda newsustain3
			bne :+
				lda #$10
				sta newsustain3
				lda #$00
				sta freq3+0
				sta freq3+1
			:

			; Update SID
			ldx oldwaveform1
			dex
			lda newwaveform1
			ldy newsustain1
			cpy oldsustain1
			beq skipsus1
			bcc audsus1a
			stx $d404+0
			sty $d406+0
			sta $d404+0
			jmp audsus1b
audsus1a:	sty $d406+0
audsus1b:	lda newsustain1
			sta oldsustain1
skipsus1:	lda freq1+$00
			sta $d401+0
			lda freq1+$01
			sta $d400+0

			ldx oldwaveform2
			dex
			lda newwaveform2
			ldy newsustain2
			cpy oldsustain2
			beq skipsus2
			bcc audsus2a
			stx $d404+7
			sty $d406+7
			sta $d404+7
			jmp audsus2b
audsus2a:	sty $d406+7
audsus2b:	lda newsustain2
			sta oldsustain2
skipsus2:	lda freq2+$00
			sta $d401+7
			lda freq2+$01
			sta $d400+7

			ldx oldwaveform3
			dex
			lda newwaveform3
			ldy newsustain3
			cpy oldsustain3
			beq skipsus3
			bcc audsus3a
			stx $d404+14
			sty $d406+14
			sta $d404+14
			jmp audsus3b
audsus3a:	sty $d406+14
audsus3b:	lda newsustain3
			sta oldsustain3
skipsus3:	lda freq3+$00
			sta $d401+14
			lda freq3+$01
			sta $d400+14

			lda newwaveform1
			sta oldwaveform1
			lda newwaveform2
			sta oldwaveform2
			lda newwaveform3
			sta oldwaveform3
			
			rts

wavetab: 	.byte $21,$11,$41,$81

susscale:
		.repeat 16,I
			.repeat 16,J
				.byte (((I<<4)/(16-J))+8) & $f0
			.endrep
		.endrep
.endproc
