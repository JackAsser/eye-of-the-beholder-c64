.include "global.inc"

.segment "MEMCODE_RW"
.export documentTextModeBackground
documentTextModeBackground: .byte $c

.export twomhzMode
twomhzMode: .byte 0

.segment "MEMCODE_RO"

.define SPRITES_YPOS	$33

.export screenModeGameNormal
screenModeGameNormal:
	.word init
	.word teardown
	.byte 10
	.word $2d, spriteColors
	.word $31, normalSpeed
	.word SPRITES_YPOS+21*0+19, plex0
sm1:.word SPRITES_YPOS+21*1+18, plex1
	.word SPRITES_YPOS+21*2+19, plex2
	.word SPRITES_YPOS+21*3+19, plex3
	.word SPRITES_YPOS+21*4+18, plex4
	.word $c8, campMultiplex
timing1:
	.word $e1, normalTextModeNTSC
	.word $f8, reset_multiplexer

.export screenModeGameDocument
screenModeGameDocument:
	.word init
	.word teardown
	.byte 9
	.word $2d, spriteColors
	.word $31, normalSpeed
	.word SPRITES_YPOS+21*0+19, plex0
sm2:.word SPRITES_YPOS+21*1+18, plex1
	.word SPRITES_YPOS+21*2+19, plex2
	.word SPRITES_YPOS+21*3+19, plex3
	.word SPRITES_YPOS+21*4+18, plex4
	.word $a9, documentTextMode
	.word $fa, reset_multiplexer

.export screenModeGameDocumentWithHeader
screenModeGameDocumentWithHeader:
	.word init
	.word teardown
	.byte 10
	.word $10, header0
	.word $2d, spriteColors
	.word $38, header1
	.word SPRITES_YPOS+21*0+19, plex0
sm3:.word SPRITES_YPOS+21*1+18, plex1
	.word SPRITES_YPOS+21*2+19, plex2
	.word SPRITES_YPOS+21*3+19, plex3
	.word SPRITES_YPOS+21*4+18, plex4
	.word $a9, documentTextMode
	.word $fa, reset_multiplexer

.export screenModeMainMenu
screenModeMainMenu:
	.word init
	.word teardown
	.byte 4
	.word $2d, spriteColors
	.word $31, normalSpeed
	.word $a9+16, mainMenuTextMode
	.word $fa, reset_multiplexer

.proc header0
		lda #%00000010
		sta $d015
		lda #$c
		sta $d021
		lda #$1b
		and screenEnabled
		sta $d011
		lda #(((vic_screen2/$400)&15)<<4) | ((vic_font/$400)&14)
		sta $d018
		lda #$08
		sta $d016
		jmp exitIrq_quick
.endproc

.proc header1
		ldy #0
		lda #$7b
		and screenEnabled
		ldx $d012
		:cpx $d012
		beq :-
		sta $d011
		sty $d021
		lda #$18
		sta $d016
		lda #(((vic_screen/$400)&15)<<4) | ((vic_bitmap/$400)&8)
		sta $d018
		lda system
		cmp #SYSTEM_PAL
		beq :+
			cmp #SYSTEM_DREAN
			bne :+
				nop
		:
		nop
		nop
		nop
		lda #$3b
		and screenEnabled
		sta $d011
		lda #$ff
		sta $d015
		jmp exitIrq_quick
.endproc

.proc init
	lda #$38
	sta $d018
	lda #$18
	sta $d016
	lda $dd00
	and #$fc
	sta $dd00

	ldx #<normalTextMode
	ldy #>normalTextMode
	lda system
	cmp #SYSTEM_PAL
	beq :++
	cmp #SYSTEM_DREAN
	bne :+
		ldx #<normalTextModeDrean
		ldy #>normalTextModeDrean
		jmp :++
	:
		ldx #<normalTextModeNTSC
		ldy #>normalTextModeNTSC
	:
	stx timing1+2
	sty timing1+3

	lda system
	cmp #SYSTEM_PAL
	beq :+
		lda #<plex1_nonpal
		sta sm1+2
		sta sm2+2
		sta sm3+2
		lda #>plex1_nonpal
		sta sm1+3
		sta sm2+3
		sta sm3+3
	:

	lda #$18
	sta spriteXbase

	lda #0
	sta $d010
	sta $d017
	sta $d01b
	lda #$ff
	sta $d015

	lda #SPRITE_COL0
	sta $d025
	lda #SPRITE_COL1
	ldx #7
	:
		sta $d027,x
		dex
	bpl :-
	lda #SPRITE_COL2
	sta $d026
	jsrf mouse_vic
	rts
.endproc

.proc teardown
	rts
.endproc

.macro incrementSpritePointers
	sta spriteptrs+0
	sty spriteptrs+2
	stx spriteptrs+3
	inx
	stx spriteptrs+4
	inx
	stx spriteptrs+5
	inx
	stx spriteptrs+6
	inx
	stx spriteptrs+7
.endmacro

.proc normalSpeed
	lda #$fc
	sta $d030
	jmp exitIrq_quick
.endproc

.proc spriteColors
	_d025: lda #0
	sta $d025
	_d026: lda #0
	sta $d026
	color: lda #SPRITE_COL1
	sta $d027
	sta $d029
	sta $d02a
	sta $d02b
	sta $d02c
	sta $d02d
	sta $d02e
	sta $d02f
	jmp exitIrq_quick
.endproc
.export dungeonSpriteColors = spriteColors::color+1
.export dungeonSpriteColorD025 = spriteColors::_d025+1
.export dungeonSpriteColorD026 = spriteColors::_d026+1

.proc plex0
	nop
	lda #SPRITES_YPOS + 21*1
	sta $d001+0*2
	sta $d001+2*2
	sta $d001+3*2
	sta $d001+4*2
	sta $d001+5*2
	sta $d001+6*2
	sta $d001+7*2

	lda #<(vic_sprites/$40 + 7*1+0)
	ldy #<(vic_sprites/$40 + 7*1+1)
	ldx #<(vic_sprites/$40 + 7*1+2)
	incrementSpritePointers

	jmp exitIrq_quick
.endproc

plex1_nonpal:
	nop
.proc plex1
	nop
	lda #SPRITES_YPOS + 21*2
	sta $d001+0*2
	sta $d001+2*2
	sta $d001+3*2
	sta $d001+4*2
	sta $d001+5*2
	sta $d001+6*2
	sta $d001+7*2

	lda #<(vic_sprites/$40 + 7*2+0)
	ldy #<(vic_sprites/$40 + 7*2+1)
	ldx #<(vic_sprites/$40 + 7*2+2)
	incrementSpritePointers
	jmp exitIrq_quick
.endproc

.proc plex2
	bit $ff
	lda #SPRITES_YPOS + 21*3
	sta $d001+0*2
	sta $d001+2*2
	sta $d001+3*2
	sta $d001+4*2
	sta $d001+5*2
	sta $d001+6*2
	sta $d001+7*2

	lda #<(vic_sprites/$40 + 7*3+0)
	ldy #<(vic_sprites/$40 + 7*3+1)
	ldx #<(vic_sprites/$40 + 7*3+2)
	incrementSpritePointers

	jmp exitIrq_quick
.endproc

.proc plex3
	bit $ff
	lda #SPRITES_YPOS + 21*4
	sta $d001+0*2
	sta $d001+2*2
	sta $d001+3*2
	sta $d001+4*2
	sta $d001+5*2
	sta $d001+6*2
	sta $d001+7*2

	lda #<(vic_sprites/$40 + 7*4+0)
	ldy #<(vic_sprites/$40 + 7*4+1)
	ldx #<(vic_sprites/$40 + 7*4+2)
	incrementSpritePointers

	jmp exitIrq_quick
.endproc

.proc plex4
	nop
	bit $ff
	lda #SPRITES_YPOS + 21*5
	sta $d001+0*2
	sta $d001+2*2
	sta $d001+3*2
	sta $d001+4*2
	sta $d001+5*2
	sta $d001+6*2
	sta $d001+7*2

	lda #<(vic_sprites/$40 + 7*5+0)
	ldy #<(vic_sprites/$40 + 7*5+1)
	ldx #<(vic_sprites/$40 + 7*5+2)
	incrementSpritePointers

	lda $d012
	:cmp $d012
	beq :-

	lda $d015
	and #2
	sta $d015

	lda SCREENMODE+0
	cmp #<screenModeGameDocument
	bne :+
	lda SCREENMODE+1
	cmp #>screenModeGameDocument
	bne :+
		jmp exitIrq_quick
	:
	jmp exitIrq_quick
.endproc

.proc mainMenuTextMode
	lda #6
	sta $d021
	lda #$1b
	ldx #(((vic_screen2/$400)&15)<<4) | ((vic_font/$400)&14)
	ldy #$08
	sta $d011
	stx $d018
	sty $d016
	jmp exitIrq
.endproc

.proc documentTextMode
	lda #$3c
	sta $d011
	ldx documentTextModeBackground
	lda #$ff
	sta $ffff

	; Sprite 2,3,6&7 will be affected by the IRQ-vectors, move them away
	lda #0
	sta $d000
	sta $d004
	sta $d006
	sta $d008
	sta $d00a
	sta $d00c
	sta $d00e

	stx $d021
	lda #$1c
	ldx #(((vic_screen2/$400)&15)<<4) | ((vic_font/$400)&14)
	ldy #$18
	stx $d018
	sta $d011
	sty $d016
	lda $d012
	cmp $d012
	beq *-3
	lda #>irqHandler
	sta $ffff

	jmp exitIrq
.endproc

.proc campMultiplex
	lda $d015
	ora #%11100000
	sta $d015
	lda #$e5
	sta $d001+2*5
	sta $d001+2*6
	sta $d001+2*7
	lda #$2a
	sta $d000+2*5
	sta $d000+2*6
	lda #$2a+24
	sta $d000+2*7
	lda $d010
	ora #%11100000
	sta $d010

	lda #%11000010
	sta $d01c
	lda #%00100000
	sta $d01d

	ldx #<(camp_spr/$40)
	stx vic_screen+$3fe
	inx
	stx vic_screen+$3ff
	inx
	stx vic_screen+$3fd

	jmp exitIrq
.endproc

normalTextModeDrean:
	jmp normalTextMode
normalTextModeNTSC:
	nop
.proc normalTextMode
	ldx #10
	dex
	bne *-1
	bit $ff
	lda #$1b
	ldx #(((vic_screen/$400)&15)<<4) | ((vic_font/$400)&14)
	ldy #$08
	stx $d018
	sta $d011
	sty $d016

	lda #7
	sta $d027+5
	lda #$a
	sta $d027+6
	sta $d027+7
	lda #9
	sta $d025
	lda #4
	sta $d026

	jmp exitIrq_quick
.endproc

.pushseg
.segment "BSS"
.export spriteXbase
spriteXbase: .res 1
.export screenModeGame_bgcolor
screenModeGame_bgcolor: .res 1
.popseg

.proc reset_multiplexer
	lda #0
	sta $d01d
	lda #$ff
	sta $d01c

	lda #<(mousepointer/$40)
	sta spriteptrs+1
	sta spriteptrs2+1

	clc
	lda spriteXbase
	sta $d000+0*2
	adc #24
	sta $d000+2*2
	adc #24
	sta $d000+3*2
	adc #24
	sta $d000+4*2
	adc #24
	sta $d000+5*2
	adc #24
	sta $d000+6*2
	adc #24
	sta $d000+7*2

	lda #SPRITES_YPOS
	sta $d001+0*2
	sta $d001+2*2
	sta $d001+3*2
	sta $d001+4*2
	sta $d001+5*2
	sta $d001+6*2
	sta $d001+7*2

	lda #<(vic_sprites/$40+0)
	ldy #<(vic_sprites/$40+1)
	ldx #<(vic_sprites/$40+2)
	incrementSpritePointers

	lda #%11111101
	sta $d015

	lda screenModeGame_bgcolor
	sta $d021
	lda $d010
	and #2
	sta $d010
	lda #$3b
	and screenEnabled
	sta $d011
	lda #$18
	sta $d016
	lda #(((vic_screen/$400)&15)<<4) | ((vic_bitmap/$400)&8)
	sta $d018

	ldx #$fc
	lda twomhzMode
	beq :+
		ldx #$fd
	:
	stx $d030

	jmp exitIrq_quick

;flashColors:
;	.byte 0,6,2,4,$c,5,3,7,1,$d,$f,$a,$e,8,$b,9
.endproc
