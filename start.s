.include "global.inc"

.segment "GAMESTATEM"
.export _tick
_tick:	.res 4

.segment "START"

.export screenEnabled
screenEnabled:
	.res 1

.export pointerEnabled
pointerEnabled:
	.res 1

.export musicEnabled
musicEnabled:
	.res 1

.export musicVolume
musicVolume:
	.res 1

.export latchTick
latchTick: .res 4

.export restart
restart:
	sei
	ldx #$ff
	txs
	lda #$fc
	sta $d030
	lda #0
	sta $d011
	sta $d015
	lda #6
	sta $d020

.export callMain
callMain:
	lda #$87
	sta $de02
	jmpf main

.pushseg
.segment "COMPRESSED"
memcode_ro:
	.incbin "BANK07bb0.prg.b2",2
memcode_rw:
	.incbin "BANK07bb1.prg.b2",2
itemdat:
	.incbin "BANK07bb2.prg.b2",2
hiram:
	.incbin "BANK07bb3.prg.b2",2
.popseg


.export installMemCodeRO
.proc installMemCodeRO
	lda #<__MEMCODE_RO_RUN__
	sta BB_DST+0
	lda #>__MEMCODE_RO_RUN__
	sta BB_DST+1
	lda #<.bank(memcode_ro)
	ldy #<memcode_ro
	ldx #>memcode_ro
	jmp decrunchFar
.endproc

; x=0 clear itemdat, x=1 do not clear
.export installMemCode
.proc installMemCode
	txa
	pha

	jsr installMemCodeRO

	lda #<__MEMCODE_RW_RUN__
	sta BB_DST+0
	lda #>__MEMCODE_RW_RUN__
	sta BB_DST+1
	lda #<.bank(memcode_rw)
	ldy #<memcode_rw
	ldx #>memcode_rw
	jsr decrunchFar

	lda #<__HIRAM_RUN__
	sta BB_DST+0
	lda #>__HIRAM_RUN__
	sta BB_DST+1
	lda #<.bank(hiram)
	ldy #<hiram
	ldx #>hiram
	jsr decrunchFar

	pla
	bne :+
		lda #<__ITEMDAT_RUN__
		sta BB_DST+0
		lda #>__ITEMDAT_RUN__
		sta BB_DST+1
		lda #<.bank(itemdat)
		ldy #<itemdat
		ldx #>itemdat
		jmp decrunchFar
	:

	rts
.endproc

; x=bank
.export indirectJsr
.proc indirectJsr
	sta areg+1
	lda BANK
	pha
	stx BANK
	stx $de00
	lda INDJMP+0
	sta :+ +1
	lda INDJMP+1
	sta :+ +2
	areg:lda #0
	:jsr $dead
	pla
	sta BANK
	sta $de00
	rts
.endproc

.export initMusic
.proc initMusic
	lda #$35
	sta 1
	lda #0
	tax
	tay
	jsr $9000
	lda #$37
	sta 1
	rts
.endproc

.export indirectJsr_nobank
.proc indirectJsr_nobank
	pha
	lda INDJMP+0
	sta :+ +1
	lda INDJMP+1
	sta :+ +2
	pla
	:jsr $dead
	rts
.endproc

.export copyFont
.proc copyFont
	bcs :+
		; Install font under I/O
		lda #$33	; Switch out i/o, switch in char rom. Write to RAM under ROM
		sta 1
		memcpy vic_font, $a000, $0400
		lda #$37
		sta 1
		rts
	:
		memcpy volatileFont, $a000, $0400
		rts
.endproc

; A = <size
; X = >size
.export _memcpy
.proc _memcpy
	cpx #0
	beq finalPage
	pha
	ldy #0
		:
			lda (MSRC),y
			sta (MDST),y
			iny
		bne :-
		inc MSRC+1
		inc MDST+1
		dex
	bne :-
	pla

finalPage:
	tay
	:
		dey
		cpy #$ff
		bne :+
			rts
		:
		lda (MSRC),y
		sta (MDST),y
	jmp :--
.endproc

.export _memcpy_ram
.proc _memcpy_ram
	tay
	lda 1
	pha
	lda #$35
	sta 1
	tya
	jsr _memcpy
	pla
	sta 1
	rts
.endproc

.export _memcpy_pureram
.proc _memcpy_pureram
	tay
	lda 1
	pha
	lda #$30
	sta 1
	tya
	jsr _memcpy
	pla
	sta 1
	rts
.endproc

.export _memcpy_ram_underio
.proc _memcpy_ram_underio
	tay
	lda 1
	sta TMP
	tya

	cpx #0
	beq finalPage
	pha
	ldy #0
		:
			lda (MSRC),y
			pha
			lda #$30
			sta 1
			pla
			sta (MDST),y
			lda TMP
			sta 1
			iny
		bne :-
		inc MSRC+1
		inc MDST+1
		dex
	bne :-
	pla

finalPage:
	tay
	:
		dey
		cpy #$ff
		bne :+
			rts
		:
		lda (MSRC),y
		pha
		lda #$30
		sta 1
		pla
		sta (MDST),y
		lda TMP
		sta 1
	jmp :--
.endproc

.export jsrf_impl
.proc jsrf_impl
	pla
	sta BANK
	sta $de00
	rts	; jumps to dest first, then to real return address on the second run
.endproc

; Y = BANK
; A = <size
; X = >size
.export _memcpy_bank
.proc _memcpy_bank
	sta YREG

	lda BANK
	pha
	sty BANK
	sty $de00

	ldy #0
	:
			lda (MSRC),y
			sta (MDST),y
			iny
		bne :-
		inc MSRC+1
		inc MDST+1
		dex
	bne :-
	ldy YREG
	:
		dey
		cpy #$ff
		bne :+
			pla
			sta BANK
			sta $de00
			rts
		:
		lda (MSRC),y
		sta (MDST),y
	jmp :--
.endproc

; A = <size
; X = >size
; Y = value
.export _memset
.proc _memset
	pha
	tya
	ldy #0
	:
			sta (MDST),y
			iny
		bne :-
		inc MDST+1
		dex
	bne :-
	tax
	pla
	tay
	txa
	:
		dey
		cpy #$ff
		bne :+
			rts
		:
		sta (MDST),y
	jmp :--
.endproc

.export _memset_underio
.proc _memset_underio
	sta TMP
	lda 1
	pha
	lda #$30
	sta 1
	lda TMP
	jsr _memset
	pla
	sta 1
	rts
.endproc

.if 0
.export debug_a
.proc debug_a
	php
	sta :+ +1
	pha
	txa
	pha
	tya
	pha
	lda #1
	sta textColor
	lda ARGS+0
	pha
	:lda #0
	sta ARGS+0
	jsrf text_writeHexByte
	pla
	sta ARGS+0
	pla
	tay
	pla
	tax
	pla
	plp
	rts
.endproc
.endif

.if 0

.export debug_x
.proc debug_x
	php
	stx :+ +1
	pha
	txa
	pha
	tya
	pha
	lda ARGS+0
	pha
	:lda #0
	sta ARGS+0
	jsrf text_writeHexByte
	pla
	sta ARGS+0
	pla
	tay
	pla
	tax
	pla
	plp
	rts
.endproc

.export debug_y
.proc debug_y
	php
	sty :+ +1
	pha
	txa
	pha
	tya
	pha
	lda ARGS+0
	pha
	:lda #0
	sta ARGS+0
	jsrf text_writeHexByte
	pla
	sta ARGS+0
	pla
	tay
	pla
	tax
	pla
	plp
	rts
.endproc
.endif

.proc nmiHandler
	sei
	jmp ($0318)
.endproc

.export _rti
_rti: rti

.export irqHandler
.proc irqHandler
	pha				;3
	txa				;2
	pha				;3
	tya				;2
	pha				;3
	tsx				;2
	lda $0104,x		;4
	and #$10		;2
	beq :+			;3
		nop         ; 
	:               ; 
	jmp ($0314)		;5
                    ; 
; * = ($0314)       ; 
kernel:             ; 
	lda 1			;3
	pha				;3
	lda #$37		;2
	sta 1			;4
	jmp (RASTER_IRQ);5

	; Total IRQ-setup is 46c
.endproc

.pushseg
.segment "MAIN"
.export restore_irq
.proc restore_irq
	; IRQ handler supporting kernel on/off
	lda #<irqHandler
	sta $fffe
	lda #>irqHandler
	sta $ffff
	lda #<irqHandler::kernel
	sta $0314
	lda #>irqHandler::kernel
	sta $0315

	; NMI handler supporting kernel on/off
	lda #<nmiHandler
	sta $fffa
	lda #>nmiHandler
	sta $fffb
	lda #<_rti
	sta $0318
	lda #>_rti
	sta $0319

	; Kill kernel setup timers
	lda #$7f
	sta $dc0d
	sta $dd0d
	bit $dc0d
	bit $dd0d

	; The master game timer runs in 55ms ticks
	;
	; PAL
	; 985248 = 1s
	; 985.248 = 1ms
	; 985.248*55 = 54189 = 55ms
	;
	; NTSC & OLD NTSC
	; 1022730 = 1s
	; 1022.730 = 1ms
	; 1022.730*55 = 56250 = 55ms
	;
	; DREAN
	; 1023440 = 1s
	; 1023.440 = 1ms
	; 1023.440*55 = 56289 = 55ms

	; PAL
	ldx #<(54189-1)
	ldy #>(54189-1)
	lda system
	cmp #SYSTEM_NTSC
	bne :+
		ldx #<(56250-1)
		ldy #>(56250-1)
	:
	cmp #SYSTEM_NTSC_OLD
	bne :+
		ldx #<(56250-1)
		ldy #>(56250-1)
	:
	cmp #SYSTEM_DREAN
	bne :+
		ldx #<(56289-1)
		ldy #>(56289-1)
	:
	stx $dc04
	sty $dc05

	lda #$11
	sta $dc0e

	lda #1
	sta $d01a
	dec $d019
	rts
.endproc
.popseg

; a=rows
; x=xpos
; y=ypos
; SRC=*bitmap
; SRC2=*screen
; SRC3=*d800
; TMP2+0=cols*8
; TMP2+1=cols
.export _bitblt
.proc _bitblt
		pha

		jsrf setupDestination
 
		pla
		tax
		rowLoop:
			ldy #0
			:
				lda (SRC),y
				sta (DST),y
				iny
				cpy TMP2+0
			bne :-
			ldy #0
			:
				lda (SRC2),y
				sta (DST2),y
				lda (SRC3),y
				sta (DST3),y
				iny
				cpy TMP2+1
			bne :-

			clc
			lda SRC+0
			adc TMP2+0
			sta SRC+0
			bcc :+
				inc SRC+1
				clc
			:
			lda SRC2+0
			adc TMP2+1
			sta SRC2+0
			bcc :+
				inc SRC2+1
				clc
			:
			lda SRC3+0
			adc TMP2+1
			sta SRC3+0
			bcc :+
				inc SRC3+1
				clc
			:
			lda DST+0
			adc #<320
			sta DST+0
			lda DST+1
			adc #>320
			sta DST+1
			lda DST2+0
			adc #40
			sta DST2+0
			sta DST3+0
			bcc :+
				inc DST2+1
				inc DST3+1
			:
			dex
		bpl rowLoop
		rts
.endproc

.export kernelSave
.proc kernelSave
		pha
 		; Switch out cart and basic, only keep I/O and KERNAL
		lda #$86
		sta $de02
		lda #$36
		sta 1

		pla
        JSR $FFD8     ; call SAVE
        php

		; Restore cart rom
		lda #$87
		sta $de02
		lda #$37
		sta 1

		plp
		rts
.endproc
