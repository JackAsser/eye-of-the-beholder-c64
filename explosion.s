.include "global.inc"

.segment "BSS"
	frameCount: .res 1

.segment "EXPLOSION"

.define GRAVITY 5*4
.define MAX_PARTICLES 75

xpos_fixpoint_lo = frameBuffer_bitmap+MAX_PARTICLES*0
xpos_fixpoint_hi = frameBuffer_bitmap+MAX_PARTICLES*1
ypos_fixpoint_lo = frameBuffer_bitmap+MAX_PARTICLES*2
ypos_fixpoint_hi = frameBuffer_bitmap+MAX_PARTICLES*3
vx_fixpoint_lo = frameBuffer_bitmap+MAX_PARTICLES*4
vx_fixpoint_hi = frameBuffer_bitmap+MAX_PARTICLES*5
vy_fixpoint_lo = frameBuffer_bitmap+MAX_PARTICLES*6
vy_fixpoint_hi = frameBuffer_bitmap+MAX_PARTICLES*7
darkness_lo = frameBuffer_bitmap+MAX_PARTICLES*8
darkness_hi = frameBuffer_bitmap+MAX_PARTICLES*9
old_address_lo = frameBuffer_bitmap+MAX_PARTICLES*10
old_address_hi = frameBuffer_bitmap+MAX_PARTICLES*11
delayUntilStart = frameBuffer_bitmap+MAX_PARTICLES*12
fadeSpeed = frameBuffer_bitmap+MAX_PARTICLES*13
old_value = frameBuffer_bitmap+MAX_PARTICLES*14

.assert frameBuffer_bitmap+MAX_PARTICLES*15 < frameBuffer_sprites, error, "Temporary particle memory overflows"

backupSprites = frameBuffer_sprites

; x: zoomLevel
; y: deltaX
.export renderExplosion
.proc renderExplosion
		.pushseg
		.segment "BSS"
			zoomLevel: .res 1
			deltaX: .res 1
		.popseg

		COLOR = TMP2
		FLOORLEVEL = TMP2+1

		stx zoomLevel
		sty deltaX

		lda timersEnabled
		pha
		lda #0
		sta timersEnabled

		memcpy backupSprites, vic_sprites, (7*6*64)

		; Shift clip-values
		lda #0
		sta WALLX0
		lda #20
		sta WALLX1
		lda WALLX0
		asl
		asl
		asl
		sta WALLX0
		lda WALLX1
		asl
		asl
		asl
		sta WALLX1

		; Mode
		ldy zoomLevel
		lda zoomToFrameCount,y
		sta frameCount
		lda zoomToFadeValue,y
		sta TMP
		lda zoomToFloorLevel,y
		sta FLOORLEVEL
		ldx zoomToMode,y
		lda vx_fixpoint_mode_lo_lo,x
		sta SRC+0
		lda vx_fixpoint_mode_lo_hi,x
		sta SRC+1
		lda vx_fixpoint_mode_hi_lo,x
		sta SRC2+0
		lda vx_fixpoint_mode_hi_hi,x
		sta SRC2+1
		lda vy_fixpoint_mode_lo_lo,x
		sta SRC3+0
		lda vy_fixpoint_mode_lo_hi,x
		sta SRC3+1
		lda vy_fixpoint_mode_hi_lo,x
		sta SRC4+0
		lda vy_fixpoint_mode_hi_hi,x
		sta SRC4+1
		ldy nbrParticles_mode,x
		:
			lda #0
			sta xpos_fixpoint_lo,y
			sta xpos_fixpoint_hi,y
			sta ypos_fixpoint_lo,y
			sta ypos_fixpoint_hi,y
			sta darkness_lo,y
			lda TMP
			sta darkness_hi,y
			lda (SRC),y
			sta vx_fixpoint_lo,y
			lda (SRC2),y
			sta vx_fixpoint_hi,y
			lda (SRC3),y
			sta vy_fixpoint_lo,y
			lda (SRC4),y
			sta vy_fixpoint_hi,y
			lda #<vic_sprites
			sta old_address_lo,y
			lda #>vic_sprites
			sta old_address_hi,y
			dey
		bpl :-

		loop:
			ldx zoomLevel
			ldy zoomToMode,x
			ldx nbrParticles_mode,y
			nextParticle:
				stx XREG

				; Deceleration in the x direction
				clc
				lda vx_fixpoint_hi,x
				bpl :+
					lda vx_fixpoint_lo,x
					adc #<4
					sta vx_fixpoint_lo,x
					lda vx_fixpoint_hi,x
					adc #>4
					sta vx_fixpoint_hi,x
					jmp :++
				:
					lda vx_fixpoint_lo,x
					adc #<-4
					sta vx_fixpoint_lo,x
					lda vx_fixpoint_hi,x
					adc #>-4
					sta vx_fixpoint_hi,x					
				:

				; Acceleration due to gravity
				lda vy_fixpoint_lo,x
				adc #<GRAVITY
				sta vy_fixpoint_lo,x
				lda vy_fixpoint_hi,x
				adc #>GRAVITY
				sta vy_fixpoint_hi,x

				;xpos_fixpoint[i]+=vx_fixpoint[i];
				clc
				lda xpos_fixpoint_lo,x
				adc vx_fixpoint_lo,x
				sta xpos_fixpoint_lo,x
				lda xpos_fixpoint_hi,x
				adc vx_fixpoint_hi,x
				sta xpos_fixpoint_hi,x

				;ypos_fixpoint[i]+=vy_fixpoint[i];
				clc
				lda ypos_fixpoint_lo,x
				adc vy_fixpoint_lo,x
				sta ypos_fixpoint_lo,x
				lda ypos_fixpoint_hi,x
				adc vy_fixpoint_hi,x
				sta ypos_fixpoint_hi,x

				;darkness[i]+=fadeSpeed_rom[i];
				clc
				lda darkness_lo,x
				adc fadeSpeed_rom,x
				sta darkness_lo,x
				bcc :+
					inc darkness_hi,x
				:

				ldy darkness_hi,x
				lda colorTable,y
				sta COLOR

				clc
				lda old_address_lo,x
				sta DST2+0
				adc #<(backupSprites-vic_sprites)
				sta SRC2+0
				lda old_address_hi,x
				sta DST2+1
				adc #>(backupSprites-vic_sprites)
				sta SRC2+1

				ldy #0
				lda (SRC2),y
				sta (DST2),y

				clc
				lda ypos_fixpoint_hi,x
				adc #48
				bmi discard
				tay

				; Inelastic floor bounce. Speed halved
				cpy FLOORLEVEL
				bcc :+
					; vy_fixpoint_hi /= 2
					lda vy_fixpoint_hi,x
					cmp #$80
					ror
					ror vy_fixpoint_lo,x
					sec
					lda #0
					sbc vy_fixpoint_lo,x
					sta vy_fixpoint_lo,x
					lda #0
					sbc vy_fixpoint_hi,x
					sta vy_fixpoint_hi,x
				:

				clc
				lda xpos_fixpoint_hi,x
				adc deltaX

				cmp WALLX0
				bcc discard
				cmp WALLX1
				bcs discard
					tax

					lda baseX_lo,x
					adc baseY_lo,y
					sta TMP+0
					lda baseX_hi,x
					adc baseY_hi,y
					sta TMP+1
					
					ldy #0
					lda (TMP),y
					and ibitmask,x
					sta TMP3
					lda COLOR
					and bitmask,x
					ora TMP3
					sta (TMP),y

					ldx XREG

					lda TMP+0
					sta old_address_lo,x
					lda TMP+1
					sta old_address_hi,x
					jmp :+
				discard:
					ldx XREG
					lda #<vic_sprites
					sta old_address_lo,x
					lda #>vic_sprites
					sta old_address_hi,x
				:

				dex
				bmi :+
			jmp nextParticle
			:
			dec frameCount
			beq :+
		jmp loop
		:

		jmp restore

zoomToMode: 		.byte 1,0,0,0
zoomToFloorLevel: 	.byte 119, 103, 79, 63
zoomToFadeValue: 	.byte 0,0,2,5
zoomToFrameCount:	.byte 47,47,42,38

fadeSpeed_rom: .byte $40,$33,$46,$51,$60,$5d,$42,$5f,$4a,$33,$4d,$5c,$42,$44,$3c,$42,$60,$59,$64,$5c,$5b,$3c,$56,$56,$4f,$4e,$55,$5b,$57,$3c,$39,$33,$45,$55,$45,$34,$44,$59,$4e,$57,$43,$5a,$48,$35,$4e,$4b,$42,$5d,$5b,$55
vx_fixpoint_0_lo: .byte <$009c,<$fef8,<$ff60,<$0110,<$0054,<$ff1c,<$0034,<$ff70,<$ff9c,<$ff24,<$fff8,<$ff30,<$fef8,<$ffd4,<$fee4,<$ff10,<$ff8c,<$011c,<$007c,<$00a8,<$ff48,<$00dc,<$0030,<$0058,<$0104,<$0040,<$ffa4,<$ffac,<$ff94,<$ff54,<$ff1c,<$ff50,<$ffac,<$ff84,<$010c
vx_fixpoint_0_hi: .byte >$009c,>$fef8,>$ff60,>$0110,>$0054,>$ff1c,>$0034,>$ff70,>$ff9c,>$ff24,>$fff8,>$ff30,>$fef8,>$ffd4,>$fee4,>$ff10,>$ff8c,>$011c,>$007c,>$00a8,>$ff48,>$00dc,>$0030,>$0058,>$0104,>$0040,>$ffa4,>$ffac,>$ff94,>$ff54,>$ff1c,>$ff50,>$ffac,>$ff84,>$010c
vy_fixpoint_0_lo: .byte <$ff24,<$feb8,<$fdc4,<$ff1c,<$ffd4,<$ffa8,<$ff6c,<$ff8c,<$ff50,<$ff28,<$ff88,<$fecc,<$ff48,<$fe7c,<$ffbc,<$ff68,<$ff94,<$fff8,<$fff4,<$fe14,<$ff3c,<$ffbc,<$ff64,<$fefc,<$ffb4,<$fdf4,<$fe70,<$ffc8,<$ffb4,<$ffac,<$fe88,<$fe60,<$fe80,<$ff88,<$ff40
vy_fixpoint_0_hi: .byte >$ff24,>$feb8,>$fdc4,>$ff1c,>$ffd4,>$ffa8,>$ff6c,>$ff8c,>$ff50,>$ff28,>$ff88,>$fecc,>$ff48,>$fe7c,>$ffbc,>$ff68,>$ff94,>$fff8,>$fff4,>$fe14,>$ff3c,>$ffbc,>$ff64,>$fefc,>$ffb4,>$fdf4,>$fe70,>$ffc8,>$ffb4,>$ffac,>$fe88,>$fe60,>$fe80,>$ff88,>$ff40
vx_fixpoint_1_lo: .byte <$fcb8,<$fe5c,<$01ec,<$fe7c,<$fde0,<$ff74,<$ffd4,<$004c,<$028c,<$0144,<$0150,<$0078,<$fcb4,<$fcc8,<$fda0,<$fe24,<$00b8,<$0388,<$ff30,<$ff1c,<$0358,<$0210,<$fd24,<$fdb8,<$fcd0,<$020c,<$0214,<$013c,<$02d8,<$fdd0,<$fdb0,<$fed8,<$fea4,<$fd1c,<$0204,<$0220,<$ffcc,<$fd74,<$028c,<$ff48,<$ff30,<$fcb0,<$ffac,<$ffc4,<$0054,<$fde0,<$020c,<$0378,<$00ec,<$0134
vx_fixpoint_1_hi: .byte >$fcb8,>$fe5c,>$01ec,>$fe7c,>$fde0,>$ff74,>$ffd4,>$004c,>$028c,>$0144,>$0150,>$0078,>$fcb4,>$fcc8,>$fda0,>$fe24,>$00b8,>$0388,>$ff30,>$ff1c,>$0358,>$0210,>$fd24,>$fdb8,>$fcd0,>$020c,>$0214,>$013c,>$02d8,>$fdd0,>$fdb0,>$fed8,>$fea4,>$fd1c,>$0204,>$0220,>$ffcc,>$fd74,>$028c,>$ff48,>$ff30,>$fcb0,>$ffac,>$ffc4,>$0054,>$fde0,>$020c,>$0378,>$00ec,>$0134
vy_fixpoint_1_lo: .byte <$0194,<$0298,<$00fc,<$01c0,<$fc3c,<$fefc,<$023c,<$ff08,<$fd2c,<$fccc,<$0010,<$fd60,<$fda8,<$fc5c,<$02fc,<$fcac,<$fee0,<$ff4c,<$00a4,<$fed4,<$fd90,<$fdf4,<$fec0,<$01c8,<$fcf0,<$0268,<$0294,<$fe64,<$ff74,<$0288,<$004c,<$0154,<$02a4,<$0220,<$0058,<$0004,<$0134,<$ff80,<$026c,<$fd2c,<$fde8,<$01b0,<$fc2c,<$0060,<$0078,<$fd5c,<$fc94,<$fc58,<$fd0c,<$0138
vy_fixpoint_1_hi: .byte >$0194,>$0298,>$00fc,>$01c0,>$fc3c,>$fefc,>$023c,>$ff08,>$fd2c,>$fccc,>$0010,>$fd60,>$fda8,>$fc5c,>$02fc,>$fcac,>$fee0,>$ff4c,>$00a4,>$fed4,>$fd90,>$fdf4,>$fec0,>$01c8,>$fcf0,>$0268,>$0294,>$fe64,>$ff74,>$0288,>$004c,>$0154,>$02a4,>$0220,>$0058,>$0004,>$0134,>$ff80,>$026c,>$fd2c,>$fde8,>$01b0,>$fc2c,>$0060,>$0078,>$fd5c,>$fc94,>$fc58,>$fd0c,>$0138

vx_fixpoint_mode_lo_lo: .byte <vx_fixpoint_0_lo, <vx_fixpoint_1_lo
vx_fixpoint_mode_lo_hi: .byte >vx_fixpoint_0_lo, >vx_fixpoint_1_lo
vx_fixpoint_mode_hi_lo: .byte <vx_fixpoint_0_hi, <vx_fixpoint_1_hi
vx_fixpoint_mode_hi_hi: .byte >vx_fixpoint_0_hi, >vx_fixpoint_1_hi
vy_fixpoint_mode_lo_lo: .byte <vy_fixpoint_0_lo, <vy_fixpoint_1_lo
vy_fixpoint_mode_lo_hi: .byte >vy_fixpoint_0_lo, >vy_fixpoint_1_lo
vy_fixpoint_mode_hi_lo: .byte <vy_fixpoint_0_hi, <vy_fixpoint_1_hi
vy_fixpoint_mode_hi_hi: .byte >vy_fixpoint_0_hi, >vy_fixpoint_1_hi

nbrParticles_mode: .byte 35-1,50-1
.endproc

colorTable:	.byte $ff,$ff,$aa,$ff,$aa,$aa,$55,$aa,$55,$55
			.res 256,0

.export renderConeOfCold
.proc renderConeOfCold
		lda timersEnabled
		pha
		lda #0
		sta timersEnabled

		memcpy backupSprites, vic_sprites, (7*6*64)

		ldx #0
		:
			lda coc_xpos_init_lo,x
			sta xpos_fixpoint_lo,x
			lda coc_xpos_init_hi,x
			sta xpos_fixpoint_hi,x
			
			lda coc_ypos_init_lo,x
			sta ypos_fixpoint_lo,x
			lda coc_ypos_init_hi,x
			sta ypos_fixpoint_hi,x

			lda coc_vx_init_lo,x
			sta vx_fixpoint_lo,x
			lda coc_vx_init_hi,x
			sta vx_fixpoint_hi,x
			
			lda coc_vy_init_lo,x
			sta vy_fixpoint_lo,x
			lda coc_vy_init_hi,x
			sta vy_fixpoint_hi,x

			lda coc_fadeSpeed_init,x
			sta fadeSpeed,x

			lda #0
			sta darkness_lo,x
			sta darkness_hi,x

			lda coc_delayUntilStart_init,x
			sta delayUntilStart,x

			lda #<vic_sprites
			sta old_address_lo,x
			lda #>vic_sprites
			sta old_address_hi,x
			lda #0
			sta old_value,x

			inx
			cpx #75
		bne :-

		lda #0
		sta TMP3+0
		sta TMP3+1
		sta frameCount
		frameLoop:
			ldx #74
			particleLoop:
				stx XREG

				lda delayUntilStart,x
				beq :+
					dec delayUntilStart,x
continue:			dex
					bpl particleLoop
					jmp particleLoopDone
				:

				ldy old_address_lo,x
				lda old_address_hi,x
				sta TMP3+1
				lda (TMP3),y
				eor old_value,x
				sta (TMP3),y

				;darkness[si]+=fadeSpeed[si];
				clc
				lda darkness_lo,x
				adc fadeSpeed,x
				sta darkness_lo,x
				lda darkness_hi,x
				adc #0
				sta darkness_hi,x
				tay
				lda colorTable,y
				bne :+
					lda #0
					sta old_value,x
					sta fadeSpeed,x
					jmp continue
				:
				sta TMP2

				updateVelocityX:
					lda xpos_fixpoint_hi,x
					bpl rightSide
					leftSide:
						ldy #40
						lda vx_fixpoint_hi,x
						bmi :+
							ldy #35
						:
						clc
						tya
						adc vx_fixpoint_lo,x
						sta vx_fixpoint_lo,x
						bcc :+
							inc vx_fixpoint_hi,x
						:
						jmp updateVelocityY
					rightSide:
						ldy #40
						lda vx_fixpoint_hi,x
						bpl :+
							ldy #35
						:
						sty TMP
						sec
						lda vx_fixpoint_lo,x
						sbc TMP
						sta vx_fixpoint_lo,x
						lda vx_fixpoint_hi,x
						sbc #0
						sta vx_fixpoint_hi,x

				updateVelocityY:
					lda ypos_fixpoint_hi,x
					bpl lowerSide
					upperSide:
						ldy #40
						lda vy_fixpoint_hi,x
						bmi :+
							ldy #35
						:
						clc
						tya
						adc vy_fixpoint_lo,x
						sta vy_fixpoint_lo,x
						bcc :+
							inc vy_fixpoint_hi,x
						:
						jmp updateVelocityDone
					lowerSide:
						ldy #40
						lda vy_fixpoint_hi,x
						bpl :+
							ldy #35
						:
						sty TMP
						sec
						lda vy_fixpoint_lo,x
						sbc TMP
						sta vy_fixpoint_lo,x
						lda vy_fixpoint_hi,x
						sbc #0
						sta vy_fixpoint_hi,x
				updateVelocityDone:

				;xpos[si]+=vx[si];
				clc
				lda xpos_fixpoint_lo,x
				adc vx_fixpoint_lo,x
				sta xpos_fixpoint_lo,x
				lda xpos_fixpoint_hi,x
				adc vx_fixpoint_hi,x
				sta xpos_fixpoint_hi,x
				adc #88
				sta TMP

				;ypos[si]+=vy[si];
				lda ypos_fixpoint_lo,x
				adc vy_fixpoint_lo,x
				sta ypos_fixpoint_lo,x
				lda ypos_fixpoint_hi,x
				adc vy_fixpoint_hi,x
				sta ypos_fixpoint_hi,x
				adc #55
				tay

				ldx TMP
				lda baseX_lo,x
				adc baseY_lo,y
				sta TMP+0
				lda baseX_hi,x
				adc baseY_hi,y
				sta TMP+1
				
				ldy #0
				lda TMP2
				and bitmask,x
				ldx XREG
				sta old_value,x
				eor (TMP),y
				sta (TMP),y

				lda TMP+0
				sta old_address_lo,x
				lda TMP+1
				sta old_address_hi,x
				dex
				bmi particleLoopDone
			jmp particleLoop
			particleLoopDone:
			dec frameCount
			beq :+
		jmp frameLoop
		:

		jmp restore

coc_fadeSpeed_init: .byte $10,$0b,$11,$0f,$0e,$12,$0b,$13,$0d,$0d,$0e,$0c,$0d,$0a,$0f,$10,$0a,$0f,$12,$0a,$11,$0b,$10,$10,$0a,$0e,$11,$11,$13,$0f,$0d,$0a,$11,$0c,$10,$12,$13,$10,$0a,$0b,$0c,$12,$11,$0f,$11,$0b,$0c,$12,$0d,$0b,$13,$12,$11,$0e,$12,$13,$0c,$10,$13,$12,$0e,$0a,$0a,$0d,$0e,$12,$10,$13,$0d,$10,$12,$0a,$11,$0c,$0a,$0b,$0e,$0a,$11,$0b,$13,$13,$13,$0b,$0e,$12,$13,$0b,$12,$12,$12,$13,$0b,$11,$12,$0f,$0f,$10,$0b,$0d,$10,$10,$0d,$0d,$11,$13,$11,$0a,$0d,$0e,$0d,$10,$0a,$0f,$0e,$0d,$0b,$13,$0b,$0b,$0d,$10,$13,$0f,$10,$0e,$11,$13,$0a,$0d,$0b,$0c,$0a,$0b,$0a,$0a,$13,$12,$12,$0c,$0d,$0d,$13,$0c,$0f,$10,$13,$0b,$0c,$12
coc_delayUntilStart_init: .byte $01,$07,$16,$0a,$0c,$09,$12,$01,$18,$09,$01,$08,$16,$08,$03,$14,$05,$01,$05,$10,$00,$11,$0c,$0a,$0f,$07,$0f,$06,$01,$04,$0b,$09,$0e,$0d,$17,$01,$07,$0d,$16,$0d,$18,$09,$01,$00,$13,$14,$14,$00,$17,$18,$04,$10,$0b,$10,$0a,$06,$15,$17,$0b,$15,$00,$0a,$15,$0a,$08,$0c,$15,$02,$03,$12,$0a,$00,$04,$05,$01,$09,$08,$02,$17,$08,$02,$10,$01,$0c,$13,$06,$00,$0b,$06,$02,$07,$14,$15,$07,$0b,$05,$14,$08,$0c,$07,$15,$16,$02,$11,$00,$10,$0f,$11,$0a,$17,$14,$16,$16,$17,$10,$01,$17,$18,$06,$0b,$08,$10,$15,$09,$11,$10,$07,$05,$07,$17,$05,$02,$15,$15,$17,$0a,$11,$0b,$09,$14,$0e,$0a,$17,$0a,$08,$06,$12,$0d,$16,$00
coc_vx_init_lo: .byte <$fd80,<$0000,<$0000,<$0258,<$fdd0,<$0000,<$0000,<$0000,<$0000,<$fc90,<$fce0,<$0398,<$fc68,<$0000,<$0000,<$fd08,<$0000,<$fc68,<$fcb8,<$0000,<$fc90,<$0000,<$0000,<$0000,<$fc90,<$03c0,<$0000,<$0370,<$0398,<$0348,<$fd08,<$fc90,<$fd80,<$03e8,<$0000,<$0000,<$0000,<$0000,<$fdd0,<$fcb8,<$0000,<$0000,<$0000,<$02f8,<$0000,<$0000,<$0000,<$fcb8,<$0000,<$fd30,<$0398,<$0000,<$fcb8,<$0000,<$0280,<$0000,<$02d0,<$0000,<$0230,<$0000,<$fc90,<$0000,<$fce0,<$0000,<$0000,<$0000,<$fc18,<$0000,<$0000,<$0000,<$fd80,<$fd58,<$fc40,<$0000,<$0000,<$02a8,<$0000,<$0000,<$0320,<$0000,<$0000,<$0000,<$0000,<$0280,<$0000,<$fda8,<$02d0,<$03c0,<$fc68,<$0398,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$fdf8,<$0000,<$0208,<$0000,<$0370,<$0000,<$0000,<$0000,<$0230,<$0000,<$03c0,<$0000,<$0000,<$0348,<$0230,<$0000,<$02f8,<$0348,<$03c0,<$0000,<$0348,<$fd58,<$0000,<$fda8,<$0000,<$0280,<$0348,<$0000,<$0000,<$0000,<$03c0,<$fda8,<$fc40,<$03e8,<$0000,<$fd30,<$0000,<$03c0,<$0000,<$0000,<$0000,<$fcb8,<$fd80,<$0000,<$0000,<$0000,<$0000,<$0230,<$0000,<$fc40,<$0000,<$0000,<$fc68,<$0258
coc_vx_init_hi: .byte >$fd80,>$0000,>$0000,>$0258,>$fdd0,>$0000,>$0000,>$0000,>$0000,>$fc90,>$fce0,>$0398,>$fc68,>$0000,>$0000,>$fd08,>$0000,>$fc68,>$fcb8,>$0000,>$fc90,>$0000,>$0000,>$0000,>$fc90,>$03c0,>$0000,>$0370,>$0398,>$0348,>$fd08,>$fc90,>$fd80,>$03e8,>$0000,>$0000,>$0000,>$0000,>$fdd0,>$fcb8,>$0000,>$0000,>$0000,>$02f8,>$0000,>$0000,>$0000,>$fcb8,>$0000,>$fd30,>$0398,>$0000,>$fcb8,>$0000,>$0280,>$0000,>$02d0,>$0000,>$0230,>$0000,>$fc90,>$0000,>$fce0,>$0000,>$0000,>$0000,>$fc18,>$0000,>$0000,>$0000,>$fd80,>$fd58,>$fc40,>$0000,>$0000,>$02a8,>$0000,>$0000,>$0320,>$0000,>$0000,>$0000,>$0000,>$0280,>$0000,>$fda8,>$02d0,>$03c0,>$fc68,>$0398,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$fdf8,>$0000,>$0208,>$0000,>$0370,>$0000,>$0000,>$0000,>$0230,>$0000,>$03c0,>$0000,>$0000,>$0348,>$0230,>$0000,>$02f8,>$0348,>$03c0,>$0000,>$0348,>$fd58,>$0000,>$fda8,>$0000,>$0280,>$0348,>$0000,>$0000,>$0000,>$03c0,>$fda8,>$fc40,>$03e8,>$0000,>$fd30,>$0000,>$03c0,>$0000,>$0000,>$0000,>$fcb8,>$fd80,>$0000,>$0000,>$0000,>$0000,>$0230,>$0000,>$fc40,>$0000,>$0000,>$fc68,>$0258
coc_vy_init_lo: .byte <$0000,<$fd30,<$fc68,<$0000,<$0000,<$fda8,<$03c0,<$fc90,<$0208,<$0000,<$0000,<$0000,<$0000,<$fd58,<$02d0,<$0000,<$0280,<$0000,<$0000,<$fd08,<$0000,<$0398,<$0320,<$0230,<$0000,<$0000,<$fd80,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0258,<$fc68,<$fd80,<$fd58,<$0000,<$0000,<$fc90,<$fcb8,<$fc18,<$0000,<$0208,<$fd58,<$03c0,<$0000,<$fdd0,<$0000,<$0000,<$fc18,<$0000,<$0258,<$0000,<$02a8,<$0000,<$0280,<$0000,<$fc68,<$0000,<$02f8,<$0000,<$fce0,<$fcb8,<$0398,<$0000,<$fd08,<$02a8,<$fc40,<$0000,<$0000,<$0000,<$fda8,<$fd58,<$0000,<$0348,<$0398,<$0000,<$0280,<$02a8,<$fc68,<$02f8,<$0000,<$fdd0,<$0000,<$0000,<$0000,<$0000,<$0000,<$03c0,<$03e8,<$0370,<$0258,<$0348,<$fc68,<$0000,<$0258,<$0000,<$fc40,<$0000,<$03e8,<$fc90,<$fc18,<$0000,<$0320,<$0000,<$fdf8,<$fc40,<$0000,<$0000,<$fd80,<$0000,<$0000,<$0000,<$fc90,<$0000,<$0000,<$fdd0,<$0000,<$0208,<$0000,<$0000,<$0370,<$0230,<$fcb8,<$0000,<$0000,<$0000,<$0000,<$fd08,<$0000,<$fd80,<$0000,<$0348,<$fda8,<$fd80,<$0000,<$0000,<$03e8,<$03e8,<$fd58,<$03e8,<$0000,<$0398,<$0000,<$fc68,<$fd30,<$0000,<$0000
coc_vy_init_hi: .byte >$0000,>$fd30,>$fc68,>$0000,>$0000,>$fda8,>$03c0,>$fc90,>$0208,>$0000,>$0000,>$0000,>$0000,>$fd58,>$02d0,>$0000,>$0280,>$0000,>$0000,>$fd08,>$0000,>$0398,>$0320,>$0230,>$0000,>$0000,>$fd80,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0258,>$fc68,>$fd80,>$fd58,>$0000,>$0000,>$fc90,>$fcb8,>$fc18,>$0000,>$0208,>$fd58,>$03c0,>$0000,>$fdd0,>$0000,>$0000,>$fc18,>$0000,>$0258,>$0000,>$02a8,>$0000,>$0280,>$0000,>$fc68,>$0000,>$02f8,>$0000,>$fce0,>$fcb8,>$0398,>$0000,>$fd08,>$02a8,>$fc40,>$0000,>$0000,>$0000,>$fda8,>$fd58,>$0000,>$0348,>$0398,>$0000,>$0280,>$02a8,>$fc68,>$02f8,>$0000,>$fdd0,>$0000,>$0000,>$0000,>$0000,>$0000,>$03c0,>$03e8,>$0370,>$0258,>$0348,>$fc68,>$0000,>$0258,>$0000,>$fc40,>$0000,>$03e8,>$fc90,>$fc18,>$0000,>$0320,>$0000,>$fdf8,>$fc40,>$0000,>$0000,>$fd80,>$0000,>$0000,>$0000,>$fc90,>$0000,>$0000,>$fdd0,>$0000,>$0208,>$0000,>$0000,>$0370,>$0230,>$fcb8,>$0000,>$0000,>$0000,>$0000,>$fd08,>$0000,>$fd80,>$0000,>$0348,>$fda8,>$fd80,>$0000,>$0000,>$03e8,>$03e8,>$fd58,>$03e8,>$0000,>$0398,>$0000,>$fc68,>$fd30,>$0000,>$0000
coc_xpos_init_lo: .byte <$0080,<$1ab8,<$2b20,<$0080,<$0080,<$ed40,<$2ee0,<$2788,<$0e38,<$0080,<$0080,<$0080,<$0080,<$17e8,<$1ab8,<$0080,<$1540,<$0080,<$0080,<$e250,<$0080,<$2b20,<$20d0,<$1068,<$0080,<$0080,<$eac0,<$0080,<$0080,<$0080,<$0080,<$0080,<$0080,<$0080,<$ed40,<$d4e0,<$1540,<$e818,<$0080,<$0080,<$2788,<$dbe8,<$cd38,<$0080,<$f1c8,<$e818,<$2ee0,<$0080,<$1068,<$0080,<$0080,<$32c8,<$0080,<$ed40,<$0080,<$e818,<$0080,<$eac0,<$0080,<$d4e0,<$0080,<$e250,<$0080,<$df30,<$2418,<$d4e0,<$0080,<$1db0,<$17e8,<$d120,<$0080,<$0080,<$0080,<$12c0,<$17e8,<$0080,<$2418,<$d4e0,<$0080,<$1540,<$17e8,<$d4e0,<$1db0,<$0080,<$1068,<$0080,<$0080,<$0080,<$0080,<$0080,<$d120,<$32c8,<$d878,<$12c0,<$dbe8,<$2b20,<$0080,<$12c0,<$0080,<$2ee0,<$0080,<$32c8,<$d878,<$cd38,<$0080,<$df30,<$0080,<$f1c8,<$d120,<$0080,<$0080,<$1540,<$0080,<$0080,<$0080,<$d878,<$0080,<$0080,<$1068,<$0080,<$f1c8,<$0080,<$0080,<$d878,<$1068,<$dbe8,<$0080,<$0080,<$0080,<$0080,<$1db0,<$0080,<$eac0,<$0080,<$2418,<$12c0,<$1540,<$0080,<$0080,<$cd38,<$cd38,<$e818,<$32c8,<$0080,<$2b20,<$0080,<$2b20,<$1ab8,<$0080,<$0080
coc_xpos_init_hi: .byte >$0080,>$1ab8,>$2b20,>$0080,>$0080,>$ed40,>$2ee0,>$2788,>$0e38,>$0080,>$0080,>$0080,>$0080,>$17e8,>$1ab8,>$0080,>$1540,>$0080,>$0080,>$e250,>$0080,>$2b20,>$20d0,>$1068,>$0080,>$0080,>$eac0,>$0080,>$0080,>$0080,>$0080,>$0080,>$0080,>$0080,>$ed40,>$d4e0,>$1540,>$e818,>$0080,>$0080,>$2788,>$dbe8,>$cd38,>$0080,>$f1c8,>$e818,>$2ee0,>$0080,>$1068,>$0080,>$0080,>$32c8,>$0080,>$ed40,>$0080,>$e818,>$0080,>$eac0,>$0080,>$d4e0,>$0080,>$e250,>$0080,>$df30,>$2418,>$d4e0,>$0080,>$1db0,>$17e8,>$d120,>$0080,>$0080,>$0080,>$12c0,>$17e8,>$0080,>$2418,>$d4e0,>$0080,>$1540,>$17e8,>$d4e0,>$1db0,>$0080,>$1068,>$0080,>$0080,>$0080,>$0080,>$0080,>$d120,>$32c8,>$d878,>$12c0,>$dbe8,>$2b20,>$0080,>$12c0,>$0080,>$2ee0,>$0080,>$32c8,>$d878,>$cd38,>$0080,>$df30,>$0080,>$f1c8,>$d120,>$0080,>$0080,>$1540,>$0080,>$0080,>$0080,>$d878,>$0080,>$0080,>$1068,>$0080,>$f1c8,>$0080,>$0080,>$d878,>$1068,>$dbe8,>$0080,>$0080,>$0080,>$0080,>$1db0,>$0080,>$eac0,>$0080,>$2418,>$12c0,>$1540,>$0080,>$0080,>$cd38,>$cd38,>$e818,>$32c8,>$0080,>$2b20,>$0080,>$2b20,>$1ab8,>$0080,>$0080
coc_ypos_init_lo: .byte <$eac0,<$0080,<$0080,<$ed40,<$1068,<$0080,<$0080,<$0080,<$0080,<$d878,<$20d0,<$2b20,<$2b20,<$0080,<$0080,<$e250,<$0080,<$2b20,<$2418,<$0080,<$2788,<$0080,<$0080,<$0080,<$2788,<$d120,<$0080,<$d878,<$d4e0,<$dbe8,<$e250,<$d878,<$eac0,<$cd38,<$0080,<$0080,<$0080,<$0080,<$1068,<$dbe8,<$0080,<$0080,<$0080,<$1db0,<$0080,<$0080,<$0080,<$dbe8,<$0080,<$e548,<$d4e0,<$0080,<$2418,<$0080,<$eac0,<$0080,<$1ab8,<$0080,<$ef98,<$0080,<$2788,<$0080,<$df30,<$0080,<$0080,<$0080,<$cd38,<$0080,<$0080,<$0080,<$1540,<$e818,<$2ee0,<$0080,<$0080,<$17e8,<$0080,<$0080,<$df30,<$0080,<$0080,<$0080,<$0080,<$eac0,<$0080,<$12c0,<$e548,<$2ee0,<$d4e0,<$2b20,<$0080,<$0080,<$0080,<$0080,<$0080,<$0080,<$0e38,<$0080,<$f1c8,<$0080,<$2788,<$0080,<$0080,<$0080,<$ef98,<$0080,<$2ee0,<$0080,<$0080,<$dbe8,<$1068,<$0080,<$e250,<$2418,<$2ee0,<$0080,<$2418,<$e818,<$0080,<$ed40,<$0080,<$eac0,<$2418,<$0080,<$0080,<$0080,<$d120,<$12c0,<$d120,<$cd38,<$0080,<$1ab8,<$0080,<$d120,<$0080,<$0080,<$0080,<$dbe8,<$eac0,<$0080,<$0080,<$0080,<$0080,<$1068,<$0080,<$2ee0,<$0080,<$0080,<$2b20,<$12c0
coc_ypos_init_hi: .byte >$eac0,>$0080,>$0080,>$ed40,>$1068,>$0080,>$0080,>$0080,>$0080,>$d878,>$20d0,>$2b20,>$2b20,>$0080,>$0080,>$e250,>$0080,>$2b20,>$2418,>$0080,>$2788,>$0080,>$0080,>$0080,>$2788,>$d120,>$0080,>$d878,>$d4e0,>$dbe8,>$e250,>$d878,>$eac0,>$cd38,>$0080,>$0080,>$0080,>$0080,>$1068,>$dbe8,>$0080,>$0080,>$0080,>$1db0,>$0080,>$0080,>$0080,>$dbe8,>$0080,>$e548,>$d4e0,>$0080,>$2418,>$0080,>$eac0,>$0080,>$1ab8,>$0080,>$ef98,>$0080,>$2788,>$0080,>$df30,>$0080,>$0080,>$0080,>$cd38,>$0080,>$0080,>$0080,>$1540,>$e818,>$2ee0,>$0080,>$0080,>$17e8,>$0080,>$0080,>$df30,>$0080,>$0080,>$0080,>$0080,>$eac0,>$0080,>$12c0,>$e548,>$2ee0,>$d4e0,>$2b20,>$0080,>$0080,>$0080,>$0080,>$0080,>$0080,>$0e38,>$0080,>$f1c8,>$0080,>$2788,>$0080,>$0080,>$0080,>$ef98,>$0080,>$2ee0,>$0080,>$0080,>$dbe8,>$1068,>$0080,>$e250,>$2418,>$2ee0,>$0080,>$2418,>$e818,>$0080,>$ed40,>$0080,>$eac0,>$2418,>$0080,>$0080,>$0080,>$d120,>$12c0,>$d120,>$cd38,>$0080,>$1ab8,>$0080,>$d120,>$0080,>$0080,>$0080,>$dbe8,>$eac0,>$0080,>$0080,>$0080,>$0080,>$1068,>$0080,>$2ee0,>$0080,>$0080,>$2b20,>$12c0
.endproc

bitmask:
		.repeat 22
			.byte %11000000
			.byte %11000000
			.byte %00110000
			.byte %00110000
			.byte %00001100
			.byte %00001100
			.byte %00000011
			.byte %00000011
		.endrep

ibitmask:
		.repeat 22
			.byte %11000000^$ff
			.byte %11000000^$ff
			.byte %00110000^$ff
			.byte %00110000^$ff
			.byte %00001100^$ff
			.byte %00001100^$ff
			.byte %00000011^$ff
			.byte %00000011^$ff
		.endrep

baseY_lo:
		.repeat 15*8,I
			.scope
				rowIndex = I/21
				spriteIndex = rowIndex*7
				.byte <(vic_sprites+spriteIndex*64 + (I .mod 21)*3)
			.endscope
		.endrep

baseY_hi:
		.repeat 15*8,I
			.scope
				rowIndex = I/21
				spriteIndex = rowIndex*7
				.byte >(vic_sprites+spriteIndex*64 + (I .mod 21)*3)
			.endscope
		.endrep

baseX_lo:
		.repeat 22*8,I
			.scope
				column = I/8
				spriteIndex = column/3
				spriteOffset = column .mod 3
				.byte <(spriteIndex*64 + spriteOffset)
			.endscope
		.endrep

baseX_hi:
		.repeat 22*8,I
			.scope
				column = I/8
				spriteIndex = column/3
				spriteOffset = column .mod 3
				.byte >(spriteIndex*64 + spriteOffset)
			.endscope
		.endrep

.proc restore
		lda #1
		ldx #20
		:
			sta affectedSpriteColumns,x
			dex
		bpl :-
		memcpy vic_sprites, backupSprites, (7*6*64)

		pla
		sta timersEnabled
		rts
.endproc
