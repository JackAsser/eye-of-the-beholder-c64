.include "global.inc"

.segment "GLIMMER"

animation:
	.byte 1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	.byte 2,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0
	.byte 3,2,2,2,1,1,1,1,0,0,0,0,0,0,0,0
	.byte 2,3,3,3,2,2,2,2,1,1,1,0,0,0,0,0
	.byte 1,2,2,2,3,3,3,3,2,2,2,1,1,1,1,0
	.byte 0,1,1,1,2,2,2,2,3,3,3,2,2,2,2,1
	.byte 0,0,0,0,1,1,1,1,2,2,2,3,3,3,3,2
	.byte 0,0,0,0,0,0,0,0,1,1,1,2,2,2,2,3
	.byte 0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,2
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1

; x = frameIndex 0..10
; y = 0:draw 4:undraw
.export drawBigMagicalGlimmerFrame
.proc drawBigMagicalGlimmerFrame
	sty TMP

	txa
	asl
	asl
	asl
	asl
	adc #<animation
	sta SRC+0
	lda #>animation
	adc #0
	sta SRC+1

	ldx #0

	ldy #$00
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+10*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+10*15*8)
		sta DST+1
		ldy #53
		jsr INDJMPOP
	:
	ldy #$01
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+14*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+14*15*8)
		sta DST+1
		ldy #47
		jsr INDJMPOP
	:
	ldy #$02
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+6*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+6*15*8)
		sta DST+1
		ldy #76
		jsr INDJMPOP
	:
	ldy #$03
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+13*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+13*15*8)
		sta DST+1
		ldy #27
		jsr INDJMPOP
	:
	ldy #$04
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+4*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+4*15*8)
		sta DST+1
		ldy #26
		jsr INDJMPOP
	:
	ldy #$05
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+12*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+12*15*8)
		sta DST+1
		ldy #76
		jsr INDJMPOP
	:
	ldy #$06
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+7*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+7*15*8)
		sta DST+1
		ldy #57
		jsr INDJMPOP
	:
	ldy #$07
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+15*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+15*15*8)
		sta DST+1
		ldy #31
		jsr INDJMPOP
	:
	ldy #$08
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+16*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+16*15*8)
		sta DST+1
		ldy #71
		jsr INDJMPOP
	:
	ldy #$09
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+9*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+9*15*8)
		sta DST+1
		ldy #50
		jsr INDJMPOP
	:
	ldy #$0a
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+11*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+11*15*8)
		sta DST+1
		ldy #40
		jsr INDJMPOP
	:
	ldy #$0b
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+5*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+5*15*8)
		sta DST+1
		ldy #44
		jsr INDJMPOP
	:
	ldy #$0c
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+12*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+12*15*8)
		sta DST+1
		ldy #60
		jsr INDJMPOP
	:
	ldy #$0d
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+8*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+8*15*8)
		sta DST+1
		ldy #32
		jsr INDJMPOP
	:
	ldy #$0e
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+14*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+14*15*8)
		sta DST+1
		ldy #26
		jsr INDJMPOP
	:
	ldy #$0f
	lda (SRC),y
	beq :+
		ora TMP
		tay
		lda jumptab_lo,y
		sta INDJMP+0
		lda jumptab_hi,y
		sta INDJMP+1
		lda #<(frameBuffer_sprites+9*15*8)
		sta DST+0
		lda #>(frameBuffer_sprites+9*15*8)
		sta DST+1
		ldy #72
		jsr INDJMPOP
	:
	rts

	jumptab_lo:	.byte 0,<drawFlare0,<drawFlare1,<drawFlare2
				.byte 0,<undrawFlare0,<undrawFlare1,<undrawFlare2
	jumptab_hi:	.byte 0,>drawFlare0,>drawFlare1,>drawFlare2
				.byte 0,>undrawFlare0,>undrawFlare1,>undrawFlare2

	drawFlare0:
		lda DST+0
		adc #15*8
		sta DST+0
		bcc :+
			inc DST+1
		:
		iny
		iny
		iny
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$3f
		ora #$80
		sta (DST),y
		rts

	drawFlare1:
		sty YREG
		iny
		iny
		iny
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$fc
		ora #$01
		sta (DST),y
		lda DST+0
		adc #15*8
		sta DST+0
		bcc :+
			inc DST+1
		:
		ldy YREG
		iny
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$3f
		ora #$40
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$3f
		ora #$80
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$0f
		ora #$d0
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$3f
		ora #$80
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$3f
		ora #$40
		sta (DST),y
		rts

	drawFlare2:
		sty YREG
		iny
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$fc
		ora #$02
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$f0
		ora #$07
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$f0
		ora #$0b
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$f0
		ora #$07
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$fc
		ora #$02
		sta (DST),y
		lda DST+0
		adc #15*8
		sta DST+0
		bcc :+
			inc DST+1
		:
		ldy YREG
		lda (DST),y
		sta backup_spr,x
		inx
		and #$3f
		ora #$40
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$3f
		ora #$80
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$0f
		ora #$e0
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$03
		ora #$f4
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$03
		ora #$f8
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$03
		ora #$f4
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$0f
		ora #$e0
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$3f
		ora #$80
		sta (DST),y
		iny
		lda (DST),y
		sta backup_spr,x
		inx
		and #$3f
		ora #$40
		sta (DST),y
		rts

	undrawFlare0:
		lda DST+0
		adc #15*8
		sta DST+0
		bcc :+
			inc DST+1
		:
		iny
		iny
		iny
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		rts

	undrawFlare1:
		sty YREG
		iny
		iny
		iny
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		lda DST+0
		adc #15*8
		sta DST+0
		bcc :+
			inc DST+1
		:
		ldy YREG
		iny
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		rts

	undrawFlare2:
		sty YREG
		iny
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		lda DST+0
		adc #15*8
		sta DST+0
		bcc :+
			inc DST+1
		:
		ldy YREG
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		iny
		lda backup_spr,x
		inx
		sta (DST),y
		rts
.endproc

