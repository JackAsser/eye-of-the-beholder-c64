.include "global.inc"

.segment "BSS"
.proc activeMonsters
	.export monster_typeIndex
	monster_typeIndex: .res 30
	.export monster_levelType
	monster_levelType: .res 30
	.export monster_position_lo
	monster_position_lo: .res 30
	.export monster_position_hi
	monster_position_hi: .res 30
	.export monster_subpos
	monster_subpos: .res 30
	.export monster_direction
	monster_direction: .res 30
	.export monster_legState
	monster_legState: .res 30
	.export monster_picture
	monster_picture: .res 30
	.export monster_phase
	monster_phase: .res 30
	.export monster_turnDirection
	monster_turnDirection: .res 30
	.export monster_state
	monster_state: .res 30
	.export monster_pause
	monster_pause: .res 30
;	.export monster_hpMax_lo
;	monster_hpMax_lo: .res 30
;	.export monster_hpMax_hi
;	monster_hpMax_hi: .res 30
	.export monster_hp_lo
	monster_hp_lo: .res 30
	.export monster_hp_hi
	monster_hp_hi: .res 30
	.export monster_goalPosition_lo
	monster_goalPosition_lo: .res 30
	.export monster_goalPosition_hi
	monster_goalPosition_hi: .res 30
	.export monster_weapon
	monster_weapon: .res 30
	.export monster_pocketItem
	monster_pocketItem: .res 30
	.export monster_flags
	monster_flags: .res 30
	.export monster_ticksUntilMove
	monster_ticksUntilMove: .res 30
	.export monster_thCount
	monster_thCount: .res 30
.endproc

.segment "QUICKSWAP"
.export monsterSwap
monsterSwap:
	.res .sizeof(activeMonsters)*12

.segment "GAMESTATE"
monsterSwap_lo:
	.repeat 12,I
		.byte <(monsterSwap+.sizeof(activeMonsters)*I)
	.endrep
monsterSwap_hi:
	.repeat 12,I
		.byte >(monsterSwap+.sizeof(activeMonsters)*I)
	.endrep

.export monster_swapOut
.proc monster_swapOut
	lda #<activeMonsters
	sta MSRC+0
	lda #>activeMonsters
	sta MSRC+1
	ldx levelIndex
	dex
	lda monsterSwap_lo,x
	sta MDST+0
	lda monsterSwap_hi,x
	sta MDST+1
	lda #<.sizeof(activeMonsters)
	ldx #>.sizeof(activeMonsters)
	jmp _memcpy_pureram
.endproc

.export monster_swapIn
.proc monster_swapIn
	jsr monster_clearMonsters

	lda #<activeMonsters
	sta MDST+0
	lda #>activeMonsters
	sta MDST+1
	ldx levelIndex
	dex
	lda monsterSwap_lo,x
	sta MSRC+0
	lda monsterSwap_hi,x
	sta MSRC+1
	lda #<.sizeof(activeMonsters)
	ldx #>.sizeof(activeMonsters)
	jsr _memcpy_pureram

	; Update monsterCounts
	ldx #29
	loop:
		lda monster_hp_hi,x
		bmi next
		bpl :+
		lda monster_hp_lo,x
		beq next
		:
		clc
		lda monster_position_lo,x
		adc #<triggerBitsAndMonsterCount
		sta SRC+0
		lda monster_position_hi,x
		bmi next
		adc #>triggerBitsAndMonsterCount
		sta SRC+1
		ldy #0
		lda (SRC),y
		adc #1
		sta (SRC),y
		next:
		dex
	bpl loop

	rts
.endproc	

.export monster_clearMonsters
.proc monster_clearMonsters
	ldx #29
	:
		lda #0
		sta monster_hp_lo,x
		sta monster_hp_hi,x
		sta monster_position_lo,x
		sta monster_position_hi,x
		lda #<-1
		sta monster_direction,x
		dex
	bpl :-

	ldx #0
	txa
	:
		sta triggerBitsAndMonsterCount+$000,x
		sta triggerBitsAndMonsterCount+$100,x
		sta triggerBitsAndMonsterCount+$200,x
		sta triggerBitsAndMonsterCount+$300,x
		inx
	bne :-
	rts
.endproc

.pushseg
.segment "MEMCODE_RW"
.export lda_monsterCreate_ptr_y
.proc lda_monsterCreate_ptr_y
	monsterCreate_ptr = ARGS+1 ;+2
	lda BANK
	pha
	lda INFBANK
	sta BANK
	sta $de00
	lda (monsterCreate_ptr),y
	sta :+ +1
	pla
	sta BANK
	sta $de00
	:lda #$00
	rts
.endproc
.popseg

.export monster_createFromMonsterCreate
.proc monster_createFromMonsterCreate
	index = ARGS+0
	monsterCreate_ptr = ARGS+1 ;+2

	ldx index
	lda #0
	sta monster_typeIndex,x
	sta monster_levelType,x
	sta monster_position_lo,x
	sta monster_position_hi,x
	sta monster_subpos,x
	sta monster_direction,x
	sta monster_legState,x
	sta monster_picture,x
	sta monster_phase,x
	sta monster_turnDirection,x
	sta monster_state,x
	sta monster_pause,x
;	sta monster_hpMax_lo,x
;	sta monster_hpMax_hi,x
	sta monster_hp_lo,x
	sta monster_hp_hi,x
	sta monster_goalPosition_lo,x
	sta monster_goalPosition_hi,x
	sta monster_weapon,x
	sta monster_pocketItem,x
	sta monster_flags,x
	sta monster_ticksUntilMove,x
	sta monster_thCount,x

	;thcount = type.thcount;
	ldy #MonsterCreate::type
	jsr lda_monsterCreate_ptr_y
	sta monster_typeIndex,x
	tay
	lda monsterType_thCount,y
	sta monster_thCount,x
	lda monsterType_hpDices,y
	sta TMP

	;levelType = monsterCreate.levelType*2 + (monsterCreate.index&1);
	txa
	clc
	adc #1 ; monsterCreate.index is always +1 in the .inf files.
	lsr
	ldy #MonsterCreate::levelType
	jsr lda_monsterCreate_ptr_y
	rol
	sta monster_levelType,x

	;subpos = monsterCreate.subpos;
	ldy #MonsterCreate::subpos
	jsr lda_monsterCreate_ptr_y
	sta monster_subpos,x

	;picture = monsterCreate.picture;
	ldy #MonsterCreate::picture
	jsr lda_monsterCreate_ptr_y
	sta monster_picture,x

	;phase = monsterCreate.phase;
	ldy #MonsterCreate::phase
	jsr lda_monsterCreate_ptr_y
	sta monster_phase,x

	;pause = monsterCreate.pause;
	ldy #MonsterCreate::pause
	jsr lda_monsterCreate_ptr_y
	sta monster_pause,x

	;direction = monsterCreate.direction;
	ldy #MonsterCreate::direction
	jsr lda_monsterCreate_ptr_y
	sta monster_direction,x

	;if (type.hpDices!=-1)
	lda TMP
	bmi :+
		;hpDice = new Dice(type.hpDices, 8, 0);
		sta DICE_ROLLS
		lda #8
		sta DICE_SIDES
		jmp :++
	:
		;hpDice = new Dice(1,4,0);
		lda #1
		sta DICE_ROLLS
		lda #4
		sta DICE_SIDES
	:
	lda #0
	sta DICE_BASE

	;hpMax = hpDice.roll();
	;hp = hpMax;
	txa
	pha
	jsrf rollDice
	pla
	tax

	lda DICE_RESULT+0
;	sta monster_hpMax_lo,x
	sta monster_hp_lo,x
	lda DICE_RESULT+1
;	sta monster_hpMax_hi,x
	sta monster_hp_hi,x

	;weapon = monsterCreate.weapon;
	ldy #MonsterCreate::weapon
	jsr lda_monsterCreate_ptr_y
	sta monster_weapon,x

	;pocketItem = monsterCreate.pocketItem;
	ldy #MonsterCreate::pocketItem
	jsr lda_monsterCreate_ptr_y
	sta monster_pocketItem,x

	;updatePositionAndDirection(maze, monsterCreate.position, monsterCreate.direction);
	ldy #MonsterCreate::position
	jsr lda_monsterCreate_ptr_y
	sta TMP+0	
	iny
	jsr lda_monsterCreate_ptr_y
	sta TMP+1
	ldy #MonsterCreate::direction
	jsr lda_monsterCreate_ptr_y
	sta TMP2+0

	jsr monster_updatePositionAndDirection
	rts
.endproc

.export monster_updatePositionAndDirection
.proc monster_updatePositionAndDirection
	index = ARGS+0
	newPosition = TMP+0 ;+1
	newDirection = TMP2+0

	;if (newPosition!=-1)
	lda newPosition+1
	bmi :++
		ldx index
		clc
		lda #<triggerBitsAndMonsterCount
		adc monster_position_lo,x
		sta SRC+0
		lda #>triggerBitsAndMonsterCount
		adc monster_position_hi,x
		sta SRC+1
		ldy #0
		lax (SRC),y
		and #7
		beq :+
			dex
			txa
			sta (SRC),y
		:

		ldx index
		clc
		lda newPosition+0
		sta monster_position_lo,x
		adc #<triggerBitsAndMonsterCount
		sta SRC+0
		lda newPosition+1
		sta monster_position_hi,x
		adc #>triggerBitsAndMonsterCount
		sta SRC+1
		lax (SRC),y
		inx
		txa
		sta (SRC),y
	:

	lda newDirection
	bmi :+
		ldx index
		sta monster_direction,x
	:

	rts
.endproc
