.include "global.inc"

renderDoor:
	   xpos = ARGS+0
	   ypos = ARGS+1
	   zoom = ARGS+2 ;0..2
	doorpos = ARGS+3 ;0..4 5=jammed

	ldx zoom
	lda mul6,x
	clc
	adc doorpos
	tax

	lda doorGraphicsWidth,x
	sta WIDTH
	lda doorGraphicsHeight,x
	sta HEIGHT

	lda doorGraphicsOffsetsLo_lo,x
	sta SRC+0	
	lda doorGraphicsOffsetsLo_hi,x
	sta SRC+1
	lda doorGraphicsOffsetsHi_lo,x
	sta SRC2+0	
	lda doorGraphicsOffsetsHi_hi,x
	sta SRC2+1
	lda doorGraphicsOffsetsScreen_lo,x
	sta SRC3+0	
	lda doorGraphicsOffsetsScreen_hi,x
	sta SRC3+1
	lda doorGraphicsOffsetsD800_lo,x
	sta SRC4+0	
	lda doorGraphicsOffsetsD800_hi,x
	sta SRC4+1

	; ypos -= height
	sec
	lda ypos
	sbc HEIGHT
	sta YPOS
	
	; xpos -= width/2
	lda WIDTH
	lsr
	sta TMP
	sec
	lda xpos
	sbc TMP
	sta XPOS

	jsrf drawGfxObjectSetup

	columnLoop:
		ldy XPOS
		cpy WALLX0
		bpl :+
			jmp doneRow
		:
		cpy WALLX1
		bmi :+
			rts
		:

		lda #1
		sta affectedSpriteColumns,y

		ldy #0
		lda DST_BITMAP+0
		sta DST_BITMAP_USE+0
		lda DST_BITMAP+1
		sta DST_BITMAP_USE+1
		lda DST_SPRITES+0
		sta DST_SPRITES_USE+0
		lda DST_SPRITES+1
		sta DST_SPRITES_USE+1
		rowLoop:
			lda (SRC2),y
			bpl :+
				jmp skipChar
			:
			sta TMP+1
			lda (SRC),y
			clc
			adc #<doorGraphicsBitmap
			sta TMP+0
			lda TMP+1
			adc #>doorGraphicsBitmap
			sta TMP+1

			lda (SRC4),y
			bmi spriteChar
			bitmapChar:
				sta (DST_D800),y
				lda (SRC3),y
				sta (DST_SCREEN),y
				sty YREG
				ldx #0
				.repeat 8,I
					.if I=0
						ldy #0
					.else
						iny
					.endif
					lda (TMP),y
					sta (DST_BITMAP_USE),y
					txa
					sta (DST_SPRITES_USE),y
				.endrep
				ldy YREG
				jmp skipChar
			spriteChar:
				sty YREG
				ldy #0
				lax (TMP),y
				lda mask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda mask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				ldy #1
				lax (TMP),y
				ldy #2
				lda mask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda mask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				ldy #2
				lax (TMP),y
				ldy #4
				lda mask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda mask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				ldy #3
				lax (TMP),y
				ldy #6
				lda mask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda mask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				clc
				lda TMP+0
				adc #4
				sta TMP+0
				bcc :+
					inc TMP+1
				:

				.repeat 8,I
					pla
					and (DST_SPRITES_USE),y
					ora (TMP),y
					sta (DST_SPRITES_USE),y
					dey
				.endrep
				ldy YREG
			skipChar:

			iny
			cpy HEIGHT
			beq doneRow

			clc
			lda DST_BITMAP_USE+0
			adc #8
			sta DST_BITMAP_USE+0
			bcc :+
				inc DST_BITMAP_USE+1
				clc
			:
			lda DST_SPRITES_USE+0
			adc #8
			sta DST_SPRITES_USE+0
			bcc :+
				inc DST_SPRITES_USE+1
			:
		jmp rowLoop
		doneRow:
		inc XPOS
		dec WIDTH
		bne :+
			rts
		:

		clc
		lda SRC+0
		adc HEIGHT
		sta SRC+0
		bcc :+
			inc SRC+1
			clc
		:
		lda SRC2+0
		adc HEIGHT
		sta SRC2+0
		bcc :+
			inc SRC2+1
			clc
		:
		lda SRC3+0
		adc HEIGHT
		sta SRC3+0
		bcc :+
			inc SRC3+1
			clc
		:
		lda SRC4+0
		adc HEIGHT
		sta SRC4+0
		bcc :+
			inc SRC4+1
		:

		jsr forwardDestination
	jmp columnLoop

mul6:	.byte 0,6,12
