.macro _clearFrameBufferSprites column
	.repeat 15*8-10,J
		sty frameBuffer_sprites + column*15*8 + J + 10
	.endrep
	sty affectedSpriteColumns+column
	rts
.endmacro

.segment "CLEARSPR0"
.export clearspr0
clearspr0:
_clearFrameBufferSprites 0

.segment "CLEARSPR1"
.export clearspr1
clearspr1:
_clearFrameBufferSprites 1

.segment "CLEARSPR2"
.export clearspr2
clearspr2:
_clearFrameBufferSprites 2

.segment "CLEARSPR3"
.export clearspr3
clearspr3:
_clearFrameBufferSprites 3

.segment "CLEARSPR4"
.export clearspr4
clearspr4:
_clearFrameBufferSprites 4

.segment "CLEARSPR5"
.export clearspr5
clearspr5:
_clearFrameBufferSprites 5

.segment "CLEARSPR6"
.export clearspr6
clearspr6:
_clearFrameBufferSprites 6

.segment "CLEARSPR7"
.export clearspr7
clearspr7:
_clearFrameBufferSprites 7

.segment "CLEARSPR8"
.export clearspr8
clearspr8:
_clearFrameBufferSprites 8

.segment "CLEARSPR9"
.export clearspr9
clearspr9:
_clearFrameBufferSprites 9

.segment "CLEARSPR10"
.export clearspr10
clearspr10:
_clearFrameBufferSprites 10

.segment "CLEARSPR11"
.export clearspr11
clearspr11:
_clearFrameBufferSprites 11

.segment "CLEARSPR12"
.export clearspr12
clearspr12:
_clearFrameBufferSprites 12

.segment "CLEARSPR13"
.export clearspr13
clearspr13:
_clearFrameBufferSprites 13

.segment "CLEARSPR14"
.export clearspr14
clearspr14:
_clearFrameBufferSprites 14

.segment "CLEARSPR15"
.export clearspr15
clearspr15:
_clearFrameBufferSprites 15

.segment "CLEARSPR16"
.export clearspr16
clearspr16:
_clearFrameBufferSprites 16

.segment "CLEARSPR17"
.export clearspr17
clearspr17:
_clearFrameBufferSprites 17

.segment "CLEARSPR18"
.export clearspr18
clearspr18:
_clearFrameBufferSprites 18

.segment "CLEARSPR19"
.export clearspr19
clearspr19:
_clearFrameBufferSprites 19

.segment "CLEARSPR20"
.export clearspr20
clearspr20:
_clearFrameBufferSprites 20
