.include "global.inc"

.proc door_draw_normal
	jsr door_draw_setup
	jmp drawGfxObject
.endproc

.proc door_draw_flipped
	rts
.endproc

.proc door_draw_setup
	   xpos = ARGS+0
	   ypos = ARGS+1
	   zoom = ARGS+2 ;0..2
	doorpos = ARGS+3 ;0..4 5=jammed

	ldx zoom
	lda mul6,x
	clc
	adc doorpos
	tax

	lda _width,x
	sta WIDTH
	clc
	adc #1
	lsr
	sta TMP
	lda _height,x
	sta HEIGHT

	sec
	lda xpos
	sbc TMP
	sta XPOS
	sec
	lda ypos
	sbc HEIGHT
	sta YPOS

	lda _data_ptr_lo,x
	sta SRC_DATA+0	
	lda _data_ptr_hi,x
	sta SRC_DATA+1

	lda _char_offset_lo,x
	sta TMP+0
	lda _char_offset_hi,x
	sta TMP+1

	clc
	lda #<_screen
	adc TMP+0
	sta SRC_SCREEN+0
	lda #>_screen
	adc TMP+1
	sta SRC_SCREEN+1

	clc
	lda #<_d800
	adc TMP+0
	sta SRC_D800+0
	lda #>_d800
	adc TMP+1
	sta SRC_D800+1

	rts
mul6:	.byte 0,6,12
.endproc
