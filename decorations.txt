.if 0
;brick1
		0: ladder up
		1: ladder down
		2: hole up
		3: pressure plate
		4: alcove
		5: hidden push button
		6: hidden push button pressed
		7: slime pipe with eyes
		8: grating
		9: grating with eyes
		10:grating with eyes 2
		11:dungeon blocking
		12:small push button
		13:inscription
		14:lever up
		15:lever down
		16:door slide
		17:button
		18:button pressed
		19:grating with bend
		20:slime pipe
		21:rune
		22:dungeon blocking

;brick2:
		0: ladder up
		1: ladder down
		2: hole up
		3: hole down
		4: pressure plate
		5: alcove
		6: hidden push button
		7: hidden push button pressed
		8: small button
		9: inscription
		10:key hole 1
		11:key hole 2
		12:lever up
		13:lever down
		14:rats
		15:door slide
		16:button
		17:button pressed
		18:grating with bend
		19:slime pipe
		20:rune
		21:dagger slot

;brick3:
		0: ladder up
		1: ladder down
		2: hole up
		3: hole down
		4: pressure plate
		5: alcove
		6: empty eye socket
		7: blue eye socket
		8: purple eye socket
		9: open gem socket
		10:closed gem socket
		11:hidden push button
		12:hidden push button pressed
		13:key hole 1
		14:blue button
		15:blue button pressed
		16:red button
		17:red button pressed
		18:small button
		19:inscription
		20:key hole 2
		21:lever up
		22:lever down
		23:rats
		24:door slide
		25:button
		26:button pressed

;blue:
		0:hole up
		1:hole down
		2:pressure plate
		3:alcove
		4:gargoyle up
		5:gargoyle down
		6:gargoyle
		7:chain
		8:chain pulled
		9:dart holes
		10:hidden button
		11:spider web
		12:hacked spider web
		13:portal markings
		14:portal markings
		15:portal markings
		16:small button
		17:inscription
		18:key hole 1
		19:key hole 2
		20:lever up
		21:lever down
		22:spider web corner
		23:door side
		24:spider web side
		25:rune

;drow:
		0:hole up
		1:hole down
		2:pressure plate
		3:alcove
		4:spider button
		5:spider socket
		6:portal markings
		7:portal markings
		8:portal markings
		9:portal markings
		10:portal markings
		11:portal markings
		12:portal markings
		13:launcher
		14:dart hole
		15:inscription
		16:key hole 1
		17:key hole 2
		18:lever up
		19:lever down
		20:door slide
		21:button
		22:button pressed
		23:button 2
		24:button 2 pressed

;green:
		0:hole up
		1:hole down
		2:pressure plate
		3:alcove
		4:hidden button
		5:button
		6:button pressed
		7:small hidden button
		8:small hidden button pressed
		9:portal markings
		10:portal markings
		11:portal markings
		12:portal markings
		13:launcher
		14:inscription
		15:keyhole
		16:lever up
		17:lever down
		18:button
		19:button pressed
		20:star
		21:crack
		22:chain
		23:door slide

;xanatha:
		0:pidestal
		1:eye left
		2:eye right
		3:pidestal with eye
		4:light
		5:pressure plate
		6:alcove
		7:launcher
		8:
		9:
		10:rope
		11:rope pulled
		12:portal markings
		13:lamp
		14:spikes
		15:inscription
		16:key hole
		17:lamp turned
		18:hidden eye button
		19:hidden eye button pressed
		20:book
		21:door slide
.endif