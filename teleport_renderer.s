.include "zp.auto.inc"

.segment "TELEPORTER"

; DST_SPRITES_USE to point to the correct destination column
; x = column (0 .. 9/6/4)
; y = zoom         0/1/2
.export teleporter_draw_even
.proc teleporter_draw_even
		clc
		txa
		adc zoomOffsets,y
		tay
		lda call_lo,y
		sta TMP+0
		lda call_hi,y
		sta TMP+1
		jmp (TMP)

zoomOffsets: .byte 0, 10, 17

call_lo:.byte <teleportZ0P0C0, <teleportZ0P0C1, <teleportZ0P0C2, <teleportZ0P0C3, <teleportZ0P0C4, <teleportZ0P0C5, <teleportZ0P0C6, <teleportZ0P0C7, <teleportZ0P0C8, <teleportZ0P0C9
	.byte <teleportZ1P0C0, <teleportZ1P0C1, <teleportZ1P0C2, <teleportZ1P0C3, <teleportZ1P0C4, <teleportZ1P0C5, <teleportZ1P0C6
	.byte <teleportZ2P0C0, <teleportZ2P0C1, <teleportZ2P0C2, <teleportZ2P0C3

call_hi:.byte >teleportZ0P0C0, >teleportZ0P0C1, >teleportZ0P0C2, >teleportZ0P0C3, >teleportZ0P0C4, >teleportZ0P0C5, >teleportZ0P0C6, >teleportZ0P0C7, >teleportZ0P0C8, >teleportZ0P0C9
	.byte >teleportZ1P0C0, >teleportZ1P0C1, >teleportZ1P0C2, >teleportZ1P0C3, >teleportZ1P0C4, >teleportZ1P0C5, >teleportZ1P0C6
	.byte >teleportZ2P0C0, >teleportZ2P0C1, >teleportZ2P0C2, >teleportZ2P0C3
.endproc

; DST_SPRITES_USE to point to the correct destination column
; x = column (0 .. 9/6/4)
; y = zoom         0/1/2
.export teleporter_draw_odd
.proc teleporter_draw_odd
		clc
		txa
		adc zoomOffsets,y
		tay
		lda call_lo,y
		sta TMP+0
		lda call_hi,y
		sta TMP+1
		jmp (TMP)

zoomOffsets: .byte 0, 10, 17

call_lo:.byte <teleportZ0P1C0, <teleportZ0P1C1, <teleportZ0P1C2, <teleportZ0P1C3, <teleportZ0P1C4, <teleportZ0P1C5, <teleportZ0P1C6, <teleportZ0P1C7, <teleportZ0P1C8, <teleportZ0P1C9
	.byte <teleportZ1P1C0, <teleportZ1P1C1, <teleportZ1P1C2, <teleportZ1P1C3, <teleportZ1P1C4, <teleportZ1P1C5, <teleportZ1P1C6
	.byte <teleportZ2P1C0, <teleportZ2P1C1, <teleportZ2P1C2, <teleportZ2P1C3

call_hi:.byte >teleportZ0P1C0, >teleportZ0P1C1, >teleportZ0P1C2, >teleportZ0P1C3, >teleportZ0P1C4, >teleportZ0P1C5, >teleportZ0P1C6, >teleportZ0P1C7, >teleportZ0P1C8, >teleportZ0P1C9
	.byte >teleportZ1P1C0, >teleportZ1P1C1, >teleportZ1P1C2, >teleportZ1P1C3, >teleportZ1P1C4, >teleportZ1P1C5, >teleportZ1P1C6
	.byte >teleportZ2P1C0, >teleportZ2P1C1, >teleportZ2P1C2, >teleportZ2P1C3
.endproc

.include "converters/particles/teleporter.s"
