.include "global.inc"

button_draw_flipped:
.proc button_draw_normal
	   xpos = ARGS+0
	   ypos = ARGS+1
	   zoom = ARGS+2 ;0..2
	doorpos = ARGS+3 ;0..4 5=jammed

	sec
	lda #1
	sbc zoom
	bpl :+
		rts
	:
	eor #1 ;0=close, 1=far
	tay

	ldx levelIndex
	cpx #4
	bcs :+
		; Brick
		clc
		lda xpos
		adc brick_xpos,y
		sta XPOS
		lda brick_ypos,y
		sta YPOS
		ldx brick_gfx,y
		jmp render
	:
	cpx #7
	bcs :+++
		; Blue
		ldx doorpos
		lda blue_ypos,y
		sta YPOS
		lda xpos
		cpy #0
		bne :+
			clc
			adc blue_xpos_0,x
			sta XPOS
			lda blue_gfx_0,x
			tax
			jmp :++
		:
			clc
			adc blue_xpos_1,x
			sta XPOS
			lda blue_gfx_1,x
			tax
		:
		jmp render
	:
	cpx #10
	bcs :+
		; Drow
		clc
		lda xpos
		adc drow_xpos,y
		sta XPOS
		lda drow_ypos,y
		sta YPOS
		ldx drow_gfx,y
		jmp render
	:
	cpx #12
	bcs :+
		; Green
		clc
		lda xpos
		adc green_xpos,y
		sta XPOS
		lda green_ypos,y
		sta YPOS
		ldx green_gfx,y
		jmp render
	:
	; Xanatha
	clc
	lda xpos
	adc xanatha_xpos,y
	sta XPOS
	lda xanatha_ypos,y
	sta YPOS
	ldx xanatha_gfx,y
	jmp render

render:
	lda _width,x
	sta WIDTH
	lda _height,x
	sta HEIGHT
	lda _data_ptr_lo,x
	sta SRC_DATA+0	
	lda _data_ptr_hi,x
	sta SRC_DATA+1
	lda _char_offset_lo,x
	sta TMP+0
	lda _char_offset_hi,x
	sta TMP+1
	clc
	lda #<_screen
	adc TMP+0
	sta SRC_SCREEN+0
	lda #>_screen
	adc TMP+1
	sta SRC_SCREEN+1
	clc
	lda #<_d800
	adc TMP+0
	sta SRC_D800+0
	lda #>_d800
	adc TMP+1
	sta SRC_D800+1

	lda SCANINDEX
	cmp #13
	bne :+
		lda XPOS
		sta triggerRect+0
		lda YPOS
		sta triggerRect+1
		lda WIDTH
		sta triggerRect+2
		lda HEIGHT
		sta triggerRect+3
	:

	jmp drawGfxObject

brick_gfx:
	.byte 0,1
brick_xpos:
	.byte 5,3
brick_ypos:
	.byte 4,4

blue_xpos_0:
	.byte 0,1,2,3,4
blue_gfx_0:
	.byte 2,3,4,5,6
blue_xpos_1:
	.byte 0,1,1,2,3
blue_gfx_1:
	.byte 7,8,9,10,11
blue_ypos:
	.byte 6,6

drow_gfx:
	.byte 12,13
drow_xpos:
	.byte 6,3
drow_ypos:
	.byte 4,5

green_gfx:
	.byte 14,15
green_xpos:
	.byte 4,3
green_ypos:
	.byte 4,4

xanatha_gfx:
	.byte 16,17
xanatha_xpos:
	.byte 5,3
xanatha_ypos:
	.byte 5,5

.endproc
