.include "global.inc"

.segment "BSS"
.export restoreFromPortalAnimation
restoreFromPortalAnimation: .res 1

seed: .res 1

.segment "PORTAL"

.export portalAnimation
.proc portalAnimation
	inc shouldSkipLevelCompress
	inc restoreFromPortalAnimation

	lda #$cf
	sta seed

	lda #0
	sta timersEnabled
	sta pointerEnabled
	jsrf saveMaze

	lda #<scratchArea
	sta BB_DST+0
	lda #>scratchArea
	sta BB_DST+1
	ldy #<portal_sprite_gfx
	ldx #>portal_sprite_gfx
	jsr decrunchTo

;	snd_playSoundEffect(33);
;	snd_playSoundEffect(19);
	ldx #33
	jsr playSoundEffect

 	jsrf clearSprites
 	ldx #$1a
 	stx spriteXbase
 	ldx #0
 	jsr setColors

 	jsr crossFade

	lda #<portalAnimationFrames
	sta BB_DST+0
	lda #>portalAnimationFrames
	sta BB_DST+1
	ldy #<portal_bitmap_gfx
	ldx #>portal_bitmap_gfx
	jsr decrunchTo

	ldx #1
 	jsr setColors
 	lda #6
 	jsr wait

 	ldx #2
 	jsr setColors

	ldx #0
	jsr blitCenter
	jsr clearCenter

	jsr sequence

	lda #1
	ldx #20
	:
		sta affectedSpriteColumns,x
		dex
	bpl :-
	jsrf clearFrameBufferSprites

	rts
.endproc

.proc blitCenter
	lda bitmap_lo,x
	sta SRC_BITMAP+0
	lda bitmap_hi,x
	sta SRC_BITMAP+1
	lda screen_lo,x
	sta SRC_SCREEN+0
	lda screen_hi,x
	sta SRC_SCREEN+1
	lda d800_lo,x
	sta SRC_D800+0
	lda d800_hi,x
	sta SRC_D800+1

	lda #<(vic_bitmap+(7+3*40)*8)
	sta DST_BITMAP+0
	lda #>(vic_bitmap+(7+3*40)*8)
	sta DST_BITMAP+1
	lda #<(vic_screen+(7+3*40))
	sta DST_SCREEN+0
	lda #>(vic_screen+(7+3*40))
	sta DST_SCREEN+1
	lda #<($d800+(7+3*40))
	sta DST_D800+0
	lda #>($d800+(7+3*40))
	sta DST_D800+1

	lda #10
	sta TMP
	nextRow:
.if 0
		ldy #63
		:
			lda (SRC_BITMAP),y
			sta (DST_BITMAP),y
			dey
		bpl :-
		ldy #7
		:
			lda (SRC_SCREEN),y
			sta (DST_SCREEN),y
			lda (SRC_D800),y
			sta (DST_D800),y
			dey
		bpl :-
.endif
		.repeat 64,I
			ldy #I
			lda (SRC_BITMAP),y
			sta (DST_BITMAP),y
			.if (I .mod 8) = 7
				ldy #(I/8)
				lda (SRC_SCREEN),y
				sta (DST_SCREEN),y
				lda (SRC_D800),y
				sta (DST_D800),y
			.endif
		.endrep

		clc
		lda SRC_BITMAP+0
		adc #<(88*8)
		sta SRC_BITMAP+0
		lda SRC_BITMAP+1
		adc #>(88*8)
		sta SRC_BITMAP+1
		lda SRC_SCREEN+0
		adc #<88
		sta SRC_SCREEN+0
		lda SRC_SCREEN+1
		adc #>88
		sta SRC_SCREEN+1
		lda SRC_D800+0
		adc #<88
		sta SRC_D800+0
		lda SRC_D800+1
		adc #>88
		sta SRC_D800+1

		clc
		lda DST_BITMAP+0
		adc #<320
		sta DST_BITMAP+0
		lda DST_BITMAP+1
		adc #>320
		sta DST_BITMAP+1
		lda DST_SCREEN+0
		adc #<40
		sta DST_SCREEN+0
		lda DST_SCREEN+1
		adc #>40
		sta DST_SCREEN+1
		lda DST_D800+0
		adc #<40
		sta DST_D800+0
		lda DST_D800+1
		adc #>40
		sta DST_D800+1
		dec TMP
		bne :+
			rts
		:
	jmp nextRow

bitmap_lo:
	.repeat 11,I
		.byte <(portalAnimationFrames+portal_bitmap+8*8*I)
	.endrep
bitmap_hi:
	.repeat 11,I
		.byte >(portalAnimationFrames+portal_bitmap+8*8*I)
	.endrep

screen_lo:
	.repeat 11,I
		.byte <(portalAnimationFrames+portal_screen+8*I)
	.endrep
screen_hi:
	.repeat 11,I
		.byte >(portalAnimationFrames+portal_screen+8*I)
	.endrep

d800_lo:
	.repeat 11,I
		.byte <(portalAnimationFrames+portal_d800+8*I)
	.endrep
d800_hi:
	.repeat 11,I
		.byte >(portalAnimationFrames+portal_d800+8*I)
	.endrep

.endproc

.proc clearCenter
	ldx #6*3
	:
		lda vic_sprites+9*64+0,x
		and #$fc
		sta vic_sprites+9*64+0,x
		lda #0
		sta vic_sprites+9*64+1,x
		sta vic_sprites+9*64+2,x
		sta vic_sprites+10*64+0,x
		sta vic_sprites+10*64+1,x
		sta vic_sprites+10*64+2,x
		sta vic_sprites+11*64+0,x
		sta vic_sprites+11*64+1,x
		lda vic_sprites+11*64+2,x
		and #$03
		sta vic_sprites+11*64+2,x
		inx
		inx
		inx
		cpx #63
	bne :-

	ldx #0
	:
		.repeat 3,I
			lda vic_sprites+((I+1)*7+9)*64+0,x
			and #$fc
			sta vic_sprites+((I+1)*7+9)*64+0,x
			lda #0
			sta vic_sprites+((I+1)*7+9)*64+1,x
			sta vic_sprites+((I+1)*7+9)*64+2,x
			sta vic_sprites+((I+1)*7+10)*64+0,x
			sta vic_sprites+((I+1)*7+10)*64+1,x
			sta vic_sprites+((I+1)*7+10)*64+2,x
			sta vic_sprites+((I+1)*7+11)*64+0,x
			sta vic_sprites+((I+1)*7+11)*64+1,x
			lda vic_sprites+((I+1)*7+11)*64+2,x
			and #$03
			sta vic_sprites+((I+1)*7+11)*64+2,x
		.endrep
		inx
		inx
		inx
		cpx #63
	bne :-
	rts
.endproc

.proc sequence
	ldy #0
	loop:
		sty TMP2

		lda #6
		jsr wait

		lda sequenceFrame,y
		bmi done
		clc
		adc #2
		tax
		jsr setColors

		ldx sequenceFade,y
		cpx #1
		bne :+
			ldy TMP2
			cpy #2
			bcc :+
				lda sequenceFade-2,y
				bne :+
					ldx #24
					jsr playSoundEffect
		:

		ldy TMP2
		ldx sequenceFrame,y
		txa
		pha
		cpx #1
		bne :+
			ldx #31
			jsr playSoundEffect
			jmp :++
		:
		cpx #3
		bne :+
			lda sequenceFrame-2,y
			cmp #3
			bne :+
				ldx #90
				jsr playSoundEffect
		:

		pla
		tax
		jsr blitCenter

		ldy TMP2
		iny
	jmp loop
done:
	rts

sequenceFade:
;	.byte 1,0,0,0,0,1,2,1,0,1,2,3,2,1,2,3,4,3,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,2,1,0,1,2,3,4,3,2,1,0,0,0,1,2,3,4,4,4,3,2,2,1,1,1,0,$ff

sequenceFrame:
	.byte 0,1,0,1,2,1,0,1,2,3,4,1,0,1,2,3,4,2,3,4,2,3,4,2,1,0,0,0,1,2,3,4,5,6,7,8,9,$A,$ff
.endproc

.proc playSoundEffect
		jsrf audio_playSoundEffect
		rts
.endproc

.proc srnd
	lda seed
	beq :+
	asl
	beq :++
	bcc :++
	:eor #$1d
	:sta seed
	rts
.endproc

.proc crossFade
	lda #0
	sta TMP2
	lda seed
	loop:
		sta TMP
		lda #0
		lsr TMP
		rol
		lsr TMP
		rol
		ldx TMP
		tay
		.repeat 5,J
			.repeat 5,I
				lda scratchArea+(I+5*J)*64,x
				and maskTab,y
				sta TMP
				lda vic_sprites+(1+I+7*J)*64,x
				and imaskTab,y
				ora TMP
				sta vic_sprites+(1+I+7*J)*64,x
			.endrep
		.endrep
		inc TMP2
		lda TMP2
		and #$3
		bne :+
			bit $d011
			bpl *-3
			bit $d011
			bmi *-3
		:
		jsr srnd
		cmp #$cf
		bne :+
			rts
		:
	jmp loop
	rts

maskTab: 	.byte $03    ,$0c    ,$30    ,$c0
imaskTab:	.byte $03^$ff,$0c^$ff,$30^$ff,$c0^$ff
.endproc

.proc setColors
	lda colors0,x
	sta dungeonSpriteColorD025
	lda colors1,x
	sta dungeonSpriteColorD026
	lda colors2,x
	sta dungeonSpriteColors
	rts
	;               1  2  3  4  5  6  7  8  9 10 11 12 13
	colors0: .byte $0,$0,$0,$0,$0,$0,$6,$0,$0,$0,$0,$0,$0
	colors1: .byte $b,$9,$6,$6,$e,$6,$e,$6,$6,$6,$6,$6,$6
	colors2: .byte $c,$a,$e,$4,$3,$a,$3,$4,$a,$e,$e,$e,$e
.endproc

portal_bitmap_gfx:
		.incbin  "converters/portal/converted/portal_bitmap.prg.b2",2
		.include "converters/portal/converted/portal_bitmap.inc"

portal_sprite_gfx:
		.incbin  "converters/portal/converted/portal_sprites.prg.b2",2
