.include "global.inc"

.proc wallset_draw_normal
	jsr wallset_draw_setup
	bcs :+
		rts
	:
	jmp drawWallObject
.endproc

.proc wallset_draw_flipped
	jsr wallset_draw_setup
	bcs :+
		rts
	:
	clc
	lda XPOS
	adc WIDTH
	sta XPOS
	dec XPOS
	jmp drawFlippedWallObject
.endproc

.proc wallset_draw_setup
	set  = ARGS+2 ;0..2 (walls1) 0..3 (walls2)
	index= ARGS+3 ;0..8

	ldx ARGS+2
	clc
	lda mul9,x
	adc index
	tax

	lda _width,x
	sta WIDTH
	lda _height,x
	sta HEIGHT
	ora WIDTH
	bne :+
		clc
		rts
	:

	lda _data_ptr_lo,x
	sta SRC_DATA+0	
	lda _data_ptr_hi,x
	sta SRC_DATA+1

	lda _char_offset_lo,x
	sta TMP+0
	lda _char_offset_hi,x
	sta TMP+1

	clc
	lda #<_screen
	adc TMP+0
	sta SRC_SCREEN+0
	lda #>_screen
	adc TMP+1
	sta SRC_SCREEN+1

	clc
	lda #<_d800
	adc TMP+0
	sta SRC_D800+0
	lda #>_d800
	adc TMP+1
	sta SRC_D800+1

	sec
	rts

mul9:	.byte 0,9,18,27
.endproc
