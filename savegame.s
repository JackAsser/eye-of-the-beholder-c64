.include "global.inc"

.segment "GAMESTATEM"
	.res 1

.segment "SAVE0"
.export quickSave
quickSave:
.incbin "save.bins",$38*$4000,$2000

.segment "SAVE1"
.incbin "save.bins",$39*$4000,$2000

.segment "SAVE2"
.incbin "save.bins",$3a*$4000,$2000

.if 0
	.byte $ff

.proc member0
	.byte 0
	.byte PARTYMEMBER_STATUS_ACTIVE
	.byte "Leader",0,0,0,0,0
	.byte 11,11
	.byte 0,0
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.word 100,100
	.byte 10
	.byte 0
	.byte RACE_ELF*2 + SEX_MALE
	.byte MULTICLASS_FIGHTER
	.byte ALIGNMENT_LAWFUL_GOOD
	.byte 10
	.byte 25
	.byte 1,1,1
	.dword 0,0,0
	.byte 0
	.res 5*6,0
	.res 5*6,0
	.dword 0

	.word $0005,$0015
	.word $0023
	.word $0029
	.res 24*2,0
.endproc

.proc member1
	.byte 0
	.byte PARTYMEMBER_STATUS_ACTIVE
	.byte "Alice",0,0,0,0,0,0
	.byte 11,11
	.byte 0,0
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.word 100,100
	.byte 10
	.byte 0
	.byte RACE_HUMAN*2 + SEX_FEMALE
	.byte MULTICLASS_FIGHTER
	.byte ALIGNMENT_LAWFUL_GOOD
	.byte 43
	.byte 50
	.byte 1,1,1
	.dword 0,0,0
	.byte 0
	.res 5*6,0
	.res 5*6,0
	.dword 0

	.word $0005,$0145

;	.word $0021 ;Dohrum
;	.word $0040 ;Taghor
	.word $0152
	.word $0153
	.word $0154
	.word $0155
	.word $0156
	.res (26-5)*2,0
.endproc

.proc member2
	.byte 0
	.byte PARTYMEMBER_STATUS_ACTIVE
	.byte "Wizzy",0,0,0,0,0,0
	.byte 18,18
	.byte 0,0
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.word 100,100
	.byte 10
	.byte 0
	.byte RACE_ELF*2 + SEX_FEMALE
	.byte MULTICLASS_MAGE
	.byte ALIGNMENT_LAWFUL_GOOD
	.byte 29
	.byte 75
	.byte 1,1,1
	.dword 0,0,0
	.byte 0

	.res 5*6,0
	.res 5*6,0
	.dword $26c+2

	.word $012b,$0007;$0020,$0007
	.word $0017,$00b1
	.res 24*2,0
.endproc

.proc member3
	.byte 0
	.byte PARTYMEMBER_STATUS_ACTIVE
	.byte "Mim",0,0,0,0,0,0,0,0
	.byte 18,18
	.byte 0,0
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.byte 18,18
	.word 100,100
	.byte 10
	.byte 0
	.byte RACE_HALFELF*2 + SEX_FEMALE
	.byte MULTICLASS_FIGHTER_CLERIC_MAGE
	.byte ALIGNMENT_LAWFUL_GOOD
	.byte 31
	.byte 100
	.byte 1,1,1
	.dword 0,0,0
	.byte 0

	.res 5*6,0
	.byte <-2,<-15,<-20,0,0,0
	.res 4*6,0
	.dword 0

	.word $00b9,$0008
	.res 26*2,0
.endproc

.export loadGame
.proc loadGame
	jsrf initGameState

	ldy #0
	:	
		lda member0,y
		sta party + .sizeof(PartyMember)*0,y
		lda member1,y
		sta party + .sizeof(PartyMember)*1,y
		lda member2,y
		sta party + .sizeof(PartyMember)*2,y
		lda member3,y
		sta party + .sizeof(PartyMember)*3,y
		iny
		cpy #.sizeof(member0) ; Not PartyMember here because everything after inventory is 0
	bne :-
	rts
.endproc
.endif
