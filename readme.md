# Background

This source code is a direct port of Eye of the Beholder from the DOS
version. This is a fan art made project made out of pure joy and love for
the original game. The original IP is owned by Wizards of the Coast and no
commerical releases can be made from this unless you redefine all assets and
AD&D game logic.

# Build instructions
This code base relies heavliy on alot of dependencies. For simplicity we've
created a public Docker image available on Docker Hub that contains every
you need to build it (https://hub.docker.com/r/jackasser/boozify)

1. Install Docker and have it running.
2. Make sure you have the boozify alias available in your shell:
```alias boozify="docker rm boozify > /dev/null 2> /dev/null; docker run -ti --name boozify -p 6510:6510 --mount type=bind,source=\"\$(pwd)\",target=/src
jackasser/boozify"```
3. Build using ```boozify make```

Enjoy!

