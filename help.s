.include "global.inc"

offScreen = scratchArea
offD800 = scratchArea+1024

.segment "HELP"

.export helpGo
.proc helpGo
		lda timersEnabled
		pha

		ldx #$1b
		ldy #(((vic_screen2/$400)&15)<<4) | ((vic_font/$400)&14)
		jsrf textScreen_open

showHelp:
		lda #<offScreen
		sta BB_DST+0
		lda #>offScreen
		sta BB_DST+1
		ldy #<data
		ldx #>data
		jsr decrunchTo
		lda #<offD800
		sta BB_DST+0
		lda #>offD800
		sta BB_DST+1
		ldy #<d800
		ldx #>d800
		jsr decrunchTo
		jsr showScreen

		lda #$ff
		sta screenEnabled

		jsr pollInput
		bcc close

showFaq:
		lda #<offScreen
		sta BB_DST+0
		lda #>offScreen
		sta BB_DST+1
		ldy #<data_faq
		ldx #>data_faq
		jsr decrunchTo
		lda #<offD800
		sta BB_DST+0
		lda #>offD800
		sta BB_DST+1
		ldy #<d800_faq
		ldx #>d800_faq
		jsr decrunchTo
		jsr showScreen

		jsr pollInput
		bcc close

		jmp showHelp

close:
		lda #$ff
		sta MOUSEEVENT+MouseEvent::buttons
		lda #0
		sta KEYEVENT+KeyEvent::keyPressed
		jsrf textScreen_close
		pla
		sta timersEnabled
		rts

showScreen:
		jsr invertButtons
		ldx #0
		.repeat 4,I
			:
				.if I < 3
					lda offScreen+I*256,x
					sta vic_screen2+I*256,x
					lda offD800+I*256,x
					sta $d800+I*256,x
				.else
					lda offScreen+I*256-8,x
					sta vic_screen2+I*256-8,x
					lda offD800+I*256-8,x
					sta $d800+I*256-8,x
				.endif
				inx
			bne :-
		.endrep
		rts

invertButtons:
		lda #6
		sta TMP
		:
			ldy TMP
			ldx offScreen,y
			jsrf invertChar
			lda TMP
			tax
			sta offScreen,x

			sec
			lda #39
			sbc TMP
			pha
			tay
			ldx offScreen,y
			lda TMP
			ora #8
			tay
			jsrf invertChar
			pla
			tax
			lda TMP
			ora #8
			sta offScreen,x

			dec TMP
		bpl :-

		rts

; X = source font index
; Y = destination font index
invertChar:
		; Calculate source adress from X (MSRC = vic_font+x*8)
		lda #>(vic_font>>3)
		sta MSRC+1
		txa
		asl
		rol MSRC+1
		asl
		rol MSRC+1
		asl
		rol MSRC+1
		sta MSRC+0

		; Calculate destination adress from Y (MDST = vic_font+y*8)
		lda #>(vic_font>>3)
		sta MDST+1
		tya
		asl
		rol MDST+1
		asl
		rol MDST+1
		asl
		rol MDST+1
		sta MDST+0

		; Copy and invert
		jmp copyAndInvertChar

.pushseg
.segment "MEMCODE_RO"
copyAndInvertChar:
		ldx 1
		lda #$30
		sta 1
		ldy #7
		:
			lda (MSRC),y
			eor #$ff
			sta (MDST),y
			dey
		bpl :-
		stx 1
		rts
.popseg

pollInput:
		lda #$ff
		sta MOUSEEVENT+MouseEvent::buttons
		lda #0
		sta KEYEVENT+KeyEvent::keyPressed
		sta KEYEVENT+KeyEvent::specialChar
		sta KEYEVENT+KeyEvent::modifiers

		:
			lda MOUSEEVENT+MouseEvent::buttons
			bmi :+
				jmp mouseEvent
			:
			lda KEYEVENT+KeyEvent::keyPressed
			bne keyEvent
		jmp :--

mouseEvent:
		; First row?
		lda MOUSEEVENT+MouseEvent::ypos
		lsr
		lsr
		lsr
		bne pollInput

		; TMP = xpos/8
		lda MOUSEEVENT+MouseEvent::xpos+0
		sta TMP+0
		lda MOUSEEVENT+MouseEvent::xpos+1
		lsr
		ror TMP+0
		lsr
		ror TMP+0
		lsr
		ror TMP+0
		lda TMP+0
		cmp #7
		bcs :+
			; Left
			rts
		:
		cmp #33
		bcc pollInput
		; Right
		rts

keyEvent:
		lda KEYEVENT+KeyEvent::specialChar
		beq :+
			and #$04 ;CRSR RL
			beq pollInput
			lda KEYEVENT+KeyEvent::modifiers
			and #$50 ;L-SHIFT | R-SHIFT
			bne :+
				; Right
				sec
				rts
		:
		; Left or escape
		clc
		rts

data:	.incbin "help.prg.b2",2
d800:	.incbin "help_d800.prg.b2",2
data_faq:.incbin "faq.prg.b2",2
d800_faq:.incbin "faq_d800.prg.b2",2
.endproc

.export invertButtons = helpGo::invertButtons
