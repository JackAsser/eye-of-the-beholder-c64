.include "global.inc"

.segment "START"
screenModeLatch: 	.byte 0
irqPosition: 		.byte 0
waitCount: 			.byte 0
.export waitCount

.export screenModeNoop 
.proc screenModeNoop
	.word noop
	.word noop
	.byte 0

	.proc noop
		rts
	.endproc
.endproc

.export initScreenMode
.proc initScreenMode
	lda #0
	sta waitCount
	sta screenModeLatch
	lda #<screenModeNoop
	sta SCREENMODE+0
	lda #>screenModeNoop
	sta SCREENMODE+1
	jmp resetIrq
.endproc

.proc resetIrq
	lda $d011
	ora #$80
	sta $d011
	lda #0
	sta irqPosition
	sta $d012
	lda #<screenModeIrq
	sta RASTER_IRQ+0
	lda #>screenModeIrq
	sta RASTER_IRQ+1
	rts
.endproc

; c=0. call teardown + setup
; c=1. skip teardown + setup
.export setScreenMode
.proc setScreenMode
	:lda screenModeLatch
	bne :-
	adc #1
	stx SCREENMODE_PENDING+0
	sty SCREENMODE_PENDING+1
	sta screenModeLatch
	:lda screenModeLatch
	bne :-
	rts
.endproc

.export callScreenModeFunction
.proc callScreenModeFunction
		lda (SCREENMODE),y
		sta :+ +1
		iny
		lda (SCREENMODE),y
		sta :+ +2
		:jmp $dead
.endproc

.export wait
.proc wait
	sta waitCount
	:lda waitCount
	bne :-
	rts
.endproc

.export waitWithAbort
.proc waitWithAbort
	sta waitCount
	:
		lda KEYEVENT+KeyEvent::keyPressed
		bne :+
		lda MOUSEEVENT+MouseEvent::buttons
		bpl :+
		lda waitCount
	bne :-
	:
	rts
.endproc

.proc playMusic
		lda system
		cmp #SYSTEM_PAL
		beq :+
		cmp #SYSTEM_DREAN
		beq :+
		dec frameSkip
		bne :+
			lda #6
			sta frameSkip
			rts
		:
		lda musicVolume
		jsr $9006
		jmp $9003
frameSkip: .byte 6
.endproc

.export screenModeIrq
.proc screenModeIrq
	jsrf keyboard_scan
	lda #$c0
	sta $dc02
	lda #%10111111
	sta $dc00

	lda waitCount
	beq :+
		dec waitCount
	:

	lda musicEnabled
	beq :+
		lda 1
		pha
		lda #$35
		sta 1
		jsr playMusic
		pla
		sta 1
		jmp :++
	:
		jsrf audio_irq
	:

	ldx mouseConnected
	beq :++
	ldx pointerEnabled
	cpx #POINTERENABLED_NO
	beq :++
	cpx #POINTERENABLED_ANY
	beq :+
	;POINTERENABLED_REAL
	ldx isMouseReal
	beq :++
	:
		lda $d015
		ora #%00000010
		sta $d015
		jsrf placeSprite
	:

	lda screenModeLatch
	beq noScreenModeSwitch
		cmp #1 
		bne :+
			;c=0. call teardown + setup
			ldy #ScreenMode::teardown
			jsr callScreenModeFunction
			lda SCREENMODE_PENDING+0
			sta SCREENMODE+0
			lda SCREENMODE_PENDING+1
			sta SCREENMODE+1
			ldy #ScreenMode::setup
			jsr callScreenModeFunction
			jmp :++
		: 	
			;c=1. skip teardown + setup
			lda SCREENMODE_PENDING+0
			sta SCREENMODE+0
			lda SCREENMODE_PENDING+1
			sta SCREENMODE+1		
		:
		lda #0
		sta screenModeLatch
	noScreenModeSwitch:

	jmp exitIrq_skipMouse
.endproc

.export exitIrq
.proc exitIrq
	jsrf mouse_irq
skipMouse:

	lda $dc0d
	beq :++
		lda timersEnabled
		beq :++
			inc _tick+0
			bne :+
				inc _tick+1
				bne :+
					inc _tick+2
					bne :+
						inc _tick+3
			:
			lda SHOULDEVALUATETIMERS
			bne :+
				lda _tick+0
				sta latchTick+0
				lda _tick+1
				sta latchTick+1
				lda _tick+2
				sta latchTick+2
				lda _tick+3
				sta latchTick+3
				inc SHOULDEVALUATETIMERS
	:

quick:
	jsr next

rerun:
	lsr $d019
	pla
	sta 1
	pla
	tay
	pla
	tax
	pla
	rti

next:
	lda irqPosition
	ldy #ScreenMode::splitLength
	cmp (SCREENMODE),y
	bne :+
		jsr resetIrq
		jmp :++
	:
		asl
		asl
		adc #ScreenMode::splitHandlers
		tay
		lda (SCREENMODE),y
		sta $d012
		iny
		lda $d011
		and #$7f
		ora (SCREENMODE),y
		sta $d011
		iny
		lda (SCREENMODE),y
		sta RASTER_IRQ+0
		iny
		lda (SCREENMODE),y
		sta RASTER_IRQ+1
		inc irqPosition
	:
	rts
.endproc
.export exitIrq_skipMouse = exitIrq::skipMouse
.export exitIrq_quick = exitIrq::quick
.export exitIrq_rerun = exitIrq::rerun
.export exitIrq_next = exitIrq::next

