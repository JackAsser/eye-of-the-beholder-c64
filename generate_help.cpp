#include <stdio.h>
#include <string.h>

void convert(const char* src, const char* screenOut, const char* d800Out) {
	const char* hex = "0123456789abcdef";
	char line[100];
	char screen[1000];
	char d800[1000];

	FILE* f = fopen(src, "r");
	for (int i=0; i<25; i++) {
		memset(line, ' ', sizeof(line));
		fgets(line, sizeof(line), f);
		for (int j=0; j<40; j++) {
			char c = line[j];
			if (c<32)
				c=32;
			screen[j+i*40] = c;
		}
	}
	for (int i=0; i<25; i++) {
		memset(line, '0', sizeof(line));
		fgets(line, sizeof(line), f);
		for (int j=0; j<40; j++) {
			char c = line[j];
			if (c<32)
				c='0';
			const char *colpos = strchr(hex, c);
			if (colpos==NULL)
				colpos=hex;
			int col = (int)(colpos-hex);
			d800[j+i*40] = col;
		}
	}
	fclose(f);

	f = fopen(screenOut, "w");
	fwrite(screen, sizeof(screen), 1, f);
	fclose(f);
	f = fopen(d800Out, "w");
	fwrite(d800, sizeof(d800), 1, f);
	fclose(f);	
}

int main(void) {
	convert("help.txt", "help.bin", "help_d800.bin");
	convert("faq.txt", "faq.bin", "faq_d800.bin");
	return 0;
}
