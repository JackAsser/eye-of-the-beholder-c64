.include "global.inc"

.proc item_draw_normal
	jsr item_draw_setup
	jmp drawGfxObject
.endproc

.proc item_draw_flipped
	jsr item_draw_setup
	clc
	lda XPOS
	adc WIDTH
	sta XPOS
	dec XPOS
	jmp drawFlippedGfxObject
.endproc

mul3: 	.repeat 64,I
		.byte I*3
	.endrep

.proc item_draw_setup
	xpos = ARGS+0
	ypos = ARGS+1
	zoom = ARGS+2
	index = ARGS+3
	isThrown = ARGS+4

	lda xpos
	sta XPOS
	lda ypos
	sta YPOS

	ldx index

	lda isThrown
	cmp #3
	bne :+
		lda _width,x
		sta WIDTH
		lda _height,x
		sta HEIGHT
		jmp directRender
	:

	clc
	lda mul3,x
	adc zoom
	tax 

	lda isThrown
	bne :+
		lda _width,x
		sta WIDTH
		clc
		adc #1
		lsr
		sta TMP
		lda _height,x
		sta HEIGHT
		sec
		lda xpos
		sbc TMP
		sta XPOS
		sec
		lda ypos
		sbc HEIGHT
		sta YPOS
		jmp done
	cmp #1
	bne :+
		lda #0
		sta TMP
		ldy zoom
		lda _width,x
		sta WIDTH
		cmp thrownZoomWidths,y
		bne noMatch
			lda thrownZoomXAdjust,y
			sta TMP
		noMatch:
		lda _height,x
		sta HEIGHT
		lda xpos
		clc
		adc TMP
		tay
		lda roundToChars,y
		sta XPOS
		ldy ypos
		lda roundToChars,y
		sta YPOS
		jmp done
	:
		;Centered spells
		lda _width,x
		sta WIDTH
		asl
		asl
		sta TMP
		sec
		lda xpos
		sbc TMP
		tay
		lda roundToChars,y
		sta XPOS

		lda _height,x
		sta HEIGHT
		asl
		asl
		sta TMP
		sec
		lda ypos
		sbc TMP
		tay
		lda roundToChars,y
		sta YPOS
	done:

	directRender:

	lda _data_ptr_lo,x
	sta SRC_DATA+0	
	lda _data_ptr_hi,x
	sta SRC_DATA+1

	lda _char_offset_lo,x
	sta TMP+0
	lda _char_offset_hi,x
	sta TMP+1

	clc
	lda #<_screen
	adc TMP+0
	sta SRC_SCREEN+0
	lda #>_screen
	adc TMP+1
	sta SRC_SCREEN+1

	clc
	lda #<_d800
	adc TMP+0
	sta SRC_D800+0
	lda #>_d800
	adc TMP+1
	sta SRC_D800+1

	rts
thrownZoomWidths: .byte 4,3,2,1
thrownZoomXAdjust:.byte 16,8,4,2
.endproc
