.proc mouse_1351
		lda #0
		sta mouseConnected
		lda #1
		sta isMouseReal

		lda $d419
	    ldy oldPotX
	    jsr moveCheck
	    bcc :+
	        sty oldPotX
	        clc
	        adc dx+0
	        sta dx+0
	        txa
	        adc dx+1
	        sta dx+1
	        inc mouseConnected
		:

		; Calculate the Y movement vector
		lda $d41a
        ldy oldPotY
    	jsr moveCheck               ; Calculate movement
        bcc :+
			sty oldPotY
	        clc
	        adc dy+0
	        sta dy+0
	        txa
	        adc dy+1
	        sta dy+1
	        inc mouseConnected
        :

        rts

; Returns delta in x,a (-64..63) (new pot value in y)
moveCheck:
		and #$fe
		sty tmp
		cmp #$fe
		beq disconnected
		tay

    	sec
    	sbc tmp

    	ldx #0
    	and #$7f
    	lsr

    	cmp #$20
    	bcs :+
    		sec
        	rts
        :
    	ora #$c0
    	dex
    	rts

disconnected:
        clc
        rts
.endproc

.proc buttons_1351
		; Bit 0: Right mouse button
		; Bit 4: Left mouse button
		lda $dc00
		eor #$ff
		pha
		and #1
		asl
		sta tmp
		pla
		lsr
		lsr
		lsr
		lsr
		and #1
		ora tmp
		rts
.endproc
