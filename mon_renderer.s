.include "global.inc"

.proc mon_draw_normal
	jsr mon_draw_setup
	lda DRAW_MONSTER_HIT
	beq :+
		jmp drawWhiteGfxObject
	:
	jmp drawGfxObject
.endproc

.proc mon_draw_flipped
	jsr mon_draw_setup
	clc
	lda XPOS
	adc WIDTH
	sta XPOS
	dec XPOS
	lda DRAW_MONSTER_HIT
	beq :+
		jmp drawFlippedWhiteGfxObject
	:
	jmp drawFlippedGfxObject
.endproc

.proc mon_draw_setup
	xpos = ARGS+0
	ypos = ARGS+1
	zoom = ARGS+2 ;0..2
	state= ARGS+3 ;0..5

	ldx zoom
	lda zoomOffsets,x
	clc
	adc state
	tax

	lda _width,x
	sta WIDTH
	clc
	adc #1
	lsr
	sta TMP
	lda _height,x
	sta HEIGHT

	sec
	lda xpos
	sbc TMP
	sta XPOS
	sec
	lda ypos
	sbc HEIGHT
	sta YPOS

	lda _data_ptr_lo,x
	sta SRC_DATA+0	
	lda _data_ptr_hi,x
	sta SRC_DATA+1

	lda _char_offset_lo,x
	sta TMP+0
	lda _char_offset_hi,x
	sta TMP+1

	clc
	lda #<_screen
	adc TMP+0
	sta SRC_SCREEN+0
	lda #>_screen
	adc TMP+1
	sta SRC_SCREEN+1

	clc
	lda #<_d800
	adc TMP+0
	sta SRC_D800+0
	lda #>_d800
	adc TMP+1
	sta SRC_D800+1

	rts
.endproc

zoomOffsets:	.byte 0,6,10
