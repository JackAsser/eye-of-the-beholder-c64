.include "global.inc"

.proc outtake_draw_normal
	jsr outtake_draw_setup
	jmp drawGfxObject
.endproc

.proc outtake_draw_flipped
	jsr outtake_draw_setup
	clc
	lda XPOS
	adc WIDTH
	sta XPOS
	dec XPOS
	jmp drawFlippedGfxObject
.endproc

.proc outtake_draw_setup
	xpos = ARGS+0
	ypos = ARGS+1
	index = ARGS+2 ;0..18

	ldx index
	cpx #2
	bcc :+
		dex
	:

	lda _width,x
	sta WIDTH
	clc
	adc #1
	lsr
	sta TMP
	lda _height,x
	sta HEIGHT

	sec
	lda xpos
	sbc TMP
	sta XPOS
	sec
	lda ypos
	sbc HEIGHT
	sta YPOS

	lda _data_ptr_lo,x
	sta SRC_DATA+0	
	lda _data_ptr_hi,x
	sta SRC_DATA+1

	lda _char_offset_lo,x
	sta TMP+0
	lda _char_offset_hi,x
	sta TMP+1

	clc
	lda #<_screen
	adc TMP+0
	sta SRC_SCREEN+0
	lda #>_screen
	adc TMP+1
	sta SRC_SCREEN+1

	clc
	lda #<_d800
	adc TMP+0
	sta SRC_D800+0
	lda #>_d800
	adc TMP+1
	sta SRC_D800+1

	rts
.endproc
