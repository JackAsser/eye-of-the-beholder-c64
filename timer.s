.include "global.inc"

.segment "START"
.export timersEnabled
timersEnabled:	.byte 0
.export tick
tick:		.dword 0

.segment "GAMESTATE"
.export defaultTimers
.proc defaultTimers
	jmp :+
	.proc values
		createTimer thrownTimerHandler, 3, 0, 0, 1
		createTimer monsterTimerHandler, 20, 0, 0, 1
		createTimer monsterTimerHandler, 20, 1, 0, 1
		createTimer monsterTimerHandler, 20, 2, 0, 1
		createTimer monsterTimerHandler, 20, 3, 0, 1
		createTimer partyMemberTimerHandler, 50, 0, 0, 0
		createTimer partyMemberTimerHandler, 50, 1, 0, 0
		createTimer partyMemberTimerHandler, 50, 2, 0, 0
		createTimer partyMemberTimerHandler, 50, 3, 0, 0
		createTimer partyMemberTimerHandler, 50, 4, 0, 0
		createTimer partyMemberTimerHandler, 50, 5, 0, 0
		createTimer doorTimerHandler, 5, 0, 0, 0
		createTimer teleporterTimerHandler, 10, 0, 0, 1
		createTimer foodTimerHandler, 1080, 0, 0, 1
		createTimer swappingMemberTimerHandler, 9, 0, 0, 0
	.endproc
	:

	ldx #0
	:
		lda values,x
		sta timers,x
		inx
		cpx #.sizeof(values)
	bne :-
	rts

; Reset only the first 3 bytes on each timer (handler, handlerBank)
fixTimerHandlers:
	ldx #0
	:
		ldy #3
		:
			lda values,x
			sta timers,x
			inx
			dey
		bne :-
		clc
		txa
		adc #.sizeof(Timer)-3
		tax
		cpx #.sizeof(values)
	bne :--
	rts
.endproc

.export fixTimerHandlers = defaultTimers::fixTimerHandlers

.segment "GAMESTATEM"
.export timers
.proc timers
	.export thrownTimer
	thrownTimer: 			.res .sizeof(Timer)*1

	.export monsterTimers
	monsterTimers:			.res .sizeof(Timer)*4

	.export partyMemberTimer0
	partyMemberTimer0:		.res .sizeof(Timer)*1

	.export partyMemberTimer1
	partyMemberTimer1:		.res .sizeof(Timer)*1

	.export partyMemberTimer2
	partyMemberTimer2:		.res .sizeof(Timer)*1

	.export partyMemberTimer3
	partyMemberTimer3:		.res .sizeof(Timer)*1

	.export partyMemberTimer4
	partyMemberTimer4:		.res .sizeof(Timer)*1

	.export partyMemberTimer5
	partyMemberTimer5:		.res .sizeof(Timer)*1

	.export doorTimer
	doorTimer:				.res .sizeof(Timer)*1

	teleporterTimerHandler:	.res .sizeof(Timer)*1
	foodTimerHandler:		.res .sizeof(Timer)*1

	.export swappingMemberTimer
	swappingMemberTimer:	.res .sizeof(Timer)*1
.endproc

.segment "MAIN"
.export resetTimer
.proc resetTimer
	clc
	lda tick+0
	ldy #Timer::timeout+0
	adc (CURRENT_TIMER),y
	ldy #Timer::absoluteTimeout+0
	sta (CURRENT_TIMER),y
	lda tick+1
	ldy #Timer::timeout+1
	adc (CURRENT_TIMER),y
	ldy #Timer::absoluteTimeout+1
	sta (CURRENT_TIMER),y
	lda tick+2
	ldy #Timer::timeout+2
	adc (CURRENT_TIMER),y
	ldy #Timer::absoluteTimeout+2
	sta (CURRENT_TIMER),y
	lda tick+3
	ldy #Timer::timeout+3
	adc (CURRENT_TIMER),y
	ldy #Timer::absoluteTimeout+3
	sta (CURRENT_TIMER),y
	rts
.endproc

.proc teleporterTimerHandler
	lda teleportFlarePhase
	eor #1
	sta teleportFlarePhase
	lda visibleTeleporters
	beq :+
		inc SHOULDRENDER
	:
	rts
.endproc

.segment "MEMCODE_RW"
.export timer_evaluate
.proc timer_evaluate
	.pushseg
	.segment "BSS"
		timerCnt: .res 1
	.popseg

	lda latchTick+0
	sta tick+0
	lda latchTick+1
	sta tick+1
	lda latchTick+2
	sta tick+2
	lda latchTick+3
	sta tick+3

	lda #<timers
	sta CURRENT_TIMER+0
	lda #>timers
	sta CURRENT_TIMER+1
	lda #(.sizeof(timers)/.sizeof(Timer))
	sta timerCnt
	loop:
		sec
		ldy #Timer::absoluteTimeout
		lda (CURRENT_TIMER),y
		sbc tick+0
		iny
		lda (CURRENT_TIMER),y
		sbc tick+1
		iny
		lda (CURRENT_TIMER),y
		sbc tick+2
		iny
		lda (CURRENT_TIMER),y
		sbc tick+3
		bcs :+
			ldy #Timer::active
			lda (CURRENT_TIMER),y
			beq :+

			ldy #Timer::argument
			lda (CURRENT_TIMER),y
			sta ARGS+0
			iny
			lda (CURRENT_TIMER),y
			sta ARGS+1
			ldy #Timer::handler
			lda (CURRENT_TIMER),y
			sta sm+1
			iny
			lda (CURRENT_TIMER),y
			sta sm+2

			ldy #Timer::handlerBank
			lda BANK
			pha
			lda (CURRENT_TIMER),y
			sta BANK
			sta $de00

			lda CURRENT_TIMER+0
			pha
			lda CURRENT_TIMER+1
			pha

			sm:jsr $dead

			pla
			sta CURRENT_TIMER+1
			pla
			sta CURRENT_TIMER+0

			pla
			sta BANK
			sta $de00
			jsr resetTimer
		:
		
		clc
		lda CURRENT_TIMER+0
		adc #<.sizeof(Timer)
		sta CURRENT_TIMER+0
		bcc :+
			inc CURRENT_TIMER+1
		:
		dec timerCnt
	bne loop

	lda #0
	sta SHOULDEVALUATETIMERS

	rts
.endproc
