TMP=$e0
TMP2=$e2
oldx=$e4
oldy=$e5
xp=$e6
yp=$e8

		*=$2000

		sei
;		lda #0
;		sta $d020
;		sta $d021
		lda #$c0
		sta $dc02
		lda #%10111111
		sta $dc00

		ldx #62
		lda #$ff
		abc:
			sta $0800,x
			dex
		bpl abc

		lda #1
		sta $d015
		lda #$80
		sta xp+0
		sta yp+0
		lda #$00
		sta xp+1
		sta yp+1

jmp skips
		ldx #0
		loop:
			lda #$01
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
			inx
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
			inx
			lda #$f
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
			inx
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
			inx
		bne loop
skips:

		lda #$00
		sta TMP+0
		lda #$04
		sta TMP+1

		ldx #0
		jsr read
		sta oldx
		ldx #1
		jsr read
		sta oldy

		lda #<($800/$40)
		sta $7f8

		poll:
			ldx #0
			jsr read
			bcc poll

			ldy oldx
			jsr delta
			sty oldx

			jsr accel

			clc
			lda xp+0
			adc TMP+0
			sta xp+0
			lda xp+1
			adc TMP+1
			sta xp+1

			ldx #1
			jsr read
			bcc poll

			ldy oldy
			jsr delta
			sty oldy

			jsr accel

			sec
			lda yp+0
			sbc TMP+0
			sta yp+0
			lda yp+1
			sbc TMP+1
			sta yp+1

			bit $d011
			bpl poll
			lda xp+0
			sta $d000
			lda xp+1
			and #1
			sta $d010
			lda yp+0
			sta $d001
		jmp poll

		accel:
			sta TMP+0
			stx TMP+1
			rts		

		delta:
			sty TMP2
			tay
			sec
			sbc TMP2
			pha
			lda #0
			sbc #0
			tax
			pla

			cmp #$20
			bmi aa
				sec
				sbc #$40
				ldx #$ff
				rts
			aa:
			cmp #<-$20
			bpl bb
				clc
				adc #$40
				ldx #$00
				rts
			bb:
		rts

		read:
			lda $d419,x
			cmp #$ff
			bne skip
				clc
				rts
			skip:

			; Fake jitter
			sta TMP
			lda $d012
			and #1
			clc
			adc TMP
			; -----------

			sec
			sbc #$40
			and #$7f
			lsr
			sec
		rts

		print:
			pha
			lsr
			lsr
			lsr
			lsr
			tax
			lda hex,x
			ora #$80
			ldy #0
			sta (TMP),y
			pla
			and #15
			tax
			lda hex,x
			ora #$80
			iny
			sta (TMP),y

			;bit $d011
			;bpl *-3
			;bit $d011
			;bmi *-3

			ldy #0
			lda (TMP),y
			and #$7f
			sta (TMP),y
			iny
			lda (TMP),y
			and #$7f
			sta (TMP),y

			clc
			lda TMP+0
			adc #2
			sta TMP+0
			lda TMP+1
			adc #0
			and #$03
			ora #$04
			sta TMP+1
		rts

		inc $d020
		jmp *-3

hex:	.byte $30,$31,$32,$33,$34,$35,$36,$37,$38,$39,1,2,3,4,5,6

; min:%0100000_
; max:%1011111_

; min:%00100000
; max:%01011111
