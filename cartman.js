const fs = require('fs');
const path = require('path');

// Install a little helper to iterate over objects
Object.prototype.map = function (func, object) {
    const iterate = object || this;
    const keys = Object.keys(iterate);
    const obj = {};
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        obj[key] = func(key, iterate[key]);
    }
    return obj;
}
Object.prototype.filter = function (func, object) {
    const iterate = object || this;
    const keys = Object.keys(iterate);
    const obj = {};
    for (let i = 0; i < keys.length; i++) {
        const key = keys[i];
        if (func(key, iterate[key]) === true)
	        obj[key] = iterate[key];
    }
    return obj;
}

// Process command line arguments
process.argv.shift();
process.argv.shift();
const prelink = require("./"+process.argv.shift());

// Find all segments and calculate total segment sizes by scanning all files
const segments = {};
process.argv.forEach(filename => {
	var objs = undefined;
	switch (path.extname(filename)) {
		case '.o':
			objs = [ObjectFile(filename)];
			break;
		case '.lib':
			objs =LibraryFile(filename).objects;
			break;
	}
	
	if (objs === undefined)
		return;

	objs.forEach(object => {
		object.segments.forEach(segmentFragment => {
			var segment = segments[segmentFragment.name];
			if (segment === undefined) {
				segment = {
					name: segmentFragment.name,
					align: segmentFragment.align,
					size: 0
				}
				segments[segment.name] = segment;
			}

			if (segmentFragment.align > segment.align)
				segment.align = segmentFragment.align;

			segment.size = ((segment.size+segmentFragment.align-1)/segmentFragment.align)*segmentFragment.align + segmentFragment.size;
		});
	});
});

// Define bins for all assignable memory areas
const bins = prelink.memory
	.filter((name, memory) => !(memory.unassignable || false))
	.map((name, memory) => {return {
		name: name,
		bank: memory.bank,
		segments: [],
		offset: 0,
		start: memory.start,
		size: memory.size
	}});

// Assign size to the static segments
prelink.segments.map((name, segment) => {
	segment.size = segments[name] === undefined ? 0 : segments[name].size;
});

// Go through all static segments and assign into the bins
prelink.segments
	.filter((name, segment) => bins[segment.load] !== undefined)
	.map((name, segment) => {
		segment.name = name;
		placeSegmentInBin(bins[segment.load], segment, segment.start);
	});

// Go through all dynamic segments an assign into the bins, start with the largest
Object.fromEntries(Object.entries(segments).sort(([,a],[,b]) => b.size-a.size))
	.filter((name, segment) => prelink.segments[name] === undefined)
	.map((name, segment) => {placeSegment(bins, segment)});

// Emit memory areas
console.log("MEMORY\n{");
prelink.memory.map(emitMemory);
prelink.otherMemory.map(emitMemory);
console.log("}");

console.log();

// Emit segments
console.log("SEGMENTS\n{");
console.log("# ----- FIXED ----------------------------------------------\n");
prelink.segments.map(emitSegment);
console.log("\n# ----- BIN PACKED ----------------------------------------\n");
bins.map((name,bin) => bin.segments.forEach(segment => emitSegment(segment.name, segment)));
console.log("}");

return;

function dec2hex(number,digits) {
	return "$"+number.toString(16).padStart(digits,'0');
}

function emitMemory(name, memory) {
	var str = "\t"+(name+":").padEnd(30);
	
	const optionActions = {
		start: value => dec2hex(value, 4),
		size: value => dec2hex(value, 4),
		bank: value => dec2hex(value, 2),
		file: value => value == "%O" ? value : '"'+value+'"',
		fillval: value => dec2hex(value, 2),
		unassignable: value => false
	};
	const defaultAction = value => value;

	var options = [];

	Object.keys(memory).forEach(option => {
		const action = optionActions[option] || defaultAction;
		const value = action(memory[option]);
		if (value !== false)
			options.push(option + "=" + value);
	});
	str += options.join(", ");
	str += ";";

	console.log(str);
}

function emitSegment(name, segment) {
	var str = "\t"+(name+":").padEnd(30);
	
	const optionActions = {
		start: value => dec2hex(value, 4),
		align: value => value === 1 ? false : value,
		size: value => false,
		name: value => false
	};
	const defaultAction = value => value;

	var options = [];

	Object.keys(segment).forEach(option => {
		const action = optionActions[option] || defaultAction;
		const value = action(segment[option]);
		if (value !== false)
			options.push(option + "=" + value);
	});
	str += options.join(", ");
	str += ";";

	console.log(str);
}

function nextOffsetInBin(bin, segment) {
	const alignment = segment.align || 1;
	var nextOffset = (bin.offset+(alignment-1))/alignment;
	nextOffset *= alignment;
	nextOffset += segment.size;
	return nextOffset;
}

function placeSegmentInBin(bin, segment, offset) {
	if (offset === undefined) {
		if (prelink.segments[segment.name] === undefined) // Don't add the static segments to the bins, just reserve the space
			bin.segments.push(segment);
		bin.offset = nextOffsetInBin(bin, segment);
	} else {
		const newSize = offset - bin.start;
		if (newSize < bin.size) {
			bin.size = newSize;
		}
	}
}

function placeSegment(bins, segment) {
	var bestFit = undefined;
	var bestSpaceLeft = 1000000;

	bins.map((name,bin) => {
		const spaceLeft = bin.size - nextOffsetInBin(bin, segment);
		if ((spaceLeft>=0) && (spaceLeft < bestSpaceLeft)) {
			bestFit = bin;
			bestSpaceLeft = spaceLeft;
		}
	});

	if (bestFit === undefined)
		return false;

	placeSegmentInBin(bestFit, segment);
	segment.load = bestFit.name;

	return true;
}

function File(filename, buffer) {
	return {
		data: buffer || fs.readFileSync(filename),
		offset: 0,
		readVar: function() {
			var C;
	    	var V = 0;
	    	var Shift = 0;
		    do {
		        /* Read one byte */
		        C = this.data.readUInt8(this.offset++);
		        /* Encode it into the target value */
		        V |= (C & 0x7F) << Shift;
		        /* Next value */
		        Shift += 7;
		    } while ((C & 0x80)!=0);

		    /* Return the value read */
		    return V;
		},
		readBuf: function() {
			var length = this.readVar();
			var string = this.data.toString("ascii", this.offset, this.offset+length);
			this.offset+=length;
			return string;
		}
	}
}

function LibraryFile(filename) {
	const file = File(filename);

	file.offset = file.data.readUInt32LE(8);
	const moduleCount = file.readVar();
	var modules = [];
	for (var i=0; i<moduleCount; i++) {
		const name = file.readBuf();
		const flags = file.data.readUInt16LE(file.offset); file.offset+=2;
		const mtime = file.data.readUInt32LE(file.offset); file.offset+=4;
		const start = file.data.readUInt32LE(file.offset); file.offset+=4;
		const size = file.data.readUInt32LE(file.offset); file.offset+=4;

		modules.push({
			name: name,
			flags: flags,
			mtime: mtime,
			start: start,
			size: size
		});
	}

	return {
		modules: modules,
		objects: modules.map(module => ObjectFile(module.name, File(module.name, file.data.subarray(module.start, module.start+module.size))))
	};
}

function ObjectFile(filename, buffer) {
	const file = buffer || File(filename);

	function readStringPool() {
		file.offset = header.sections.stringPool.offset;
		const nbrStrings = file.readVar();
		var pool = [];
		for (var i=0; i<nbrStrings; i++)
			pool.push(file.readBuf());
		return pool;
	}

	function readSegments() {
		file.offset = header.sections.segments.offset;
		const nbrSegments = file.readVar();
		var segments = [];
		for (var i=0; i<nbrSegments; i++) {
			const segmentSize = file.data.readUInt32LE(file.offset); file.offset+=4;
			const nextOffset = file.offset + segmentSize;

			const name = stringPool[file.readVar()];
			const flags = file.readVar();
			const size = file.readVar();
			const align = file.readVar();
			const addrSize = file.data.readUInt8(file.offset); file.offset++;

			if (size>0) {
				segments.push({
					name: name,
					flags: flags,
					size: size,
					align: align,
					addrSize: addrSize*8
				});
			}

			file.offset = nextOffset;
		}
		return segments;
	}

	const sections = [
		"options",
		"files",
		"segments",
		"imports",
		"exports",
		"symbols",
		"lineInfos",
		"stringPool",
		"asserts",
		"scopes",
		"spans"
	];

	const header = {
		magic: file.data.readUInt32LE(0),
		version: file.data.readUInt16LE(4),
		flags: file.data.readUInt16LE(6),
		sections: {}
	};

	sections.forEach((section, index) => {
		const offset = file.data.readUInt32LE(8+8*index);
		const size = file.data.readUInt32LE(12+8*index);
		header.sections[section] = {
			offset: offset,
			size: size
		}
	});

	const stringPool = readStringPool();
	const segments = readSegments();

	return {
		header:header,
		stringPool:stringPool,
		segments:segments
	}
}













/*
var binsizes = [
	{name:"BANK00L", size:0x2000},
	{name:"BANK00H", size:0},
	{name:"BANK01L", size:0x2000},
	{name:"BANK01H", size:0x2000},
	{name:"BANK02", size:0x4000},
	{name:"BANK03", size:0x4000},
	{name:"BANK04", size:0x4000},
	{name:"BANK05", size:0x4000},
	{name:"BANK06", size:0x4000},
	{name:"BANK07", size:0x4000},
	{name:"BANK08", size:0x4000},
	{name:"BANK09", size:0x4000},
	{name:"BANK0A", size:0x4000},
	{name:"BANK0B", size:0x4000},
	{name:"BANK0C", size:0x4000},
	{name:"BANK0D", size:0x4000},
	{name:"BANK0E", size:0x4000},
	{name:"BANK0F", size:0x4000},

	{name:"BANK10", size:0x4000},
	{name:"BANK11", size:0x4000},
	{name:"BANK12", size:0x4000},
	{name:"BANK13", size:0x4000},
	{name:"BANK14", size:0x4000},
	{name:"BANK15", size:0x4000},
	{name:"BANK16", size:0x4000},
	{name:"BANK17", size:0x4000},
	{name:"BANK18", size:0x4000},
	{name:"BANK19", size:0x4000},
	{name:"BANK1A", size:0x4000},
	{name:"BANK1B", size:0x4000},
	{name:"BANK1C", size:0x4000},
	{name:"BANK1D", size:0x4000},
	{name:"BANK1E", size:0x4000},
	{name:"BANK1F", size:0x4000},

	{name:"BANK20", size:0x4000},
	{name:"BANK21", size:0x4000},
	{name:"BANK22", size:0x4000},
	{name:"BANK23", size:0x4000},
	{name:"BANK24", size:0x4000},
	{name:"BANK25", size:0x4000},
	{name:"BANK26", size:0x4000},
	{name:"BANK27", size:0x4000},
	{name:"BANK28", size:0x4000},
	{name:"BANK29", size:0x4000},
	{name:"BANK2A", size:0x4000},
	{name:"BANK2B", size:0x4000},
	{name:"BANK2C", size:0x4000},
	{name:"BANK2D", size:0x4000},
	{name:"BANK2E", size:0x4000},
	{name:"BANK2F", size:0x4000},

	{name:"BANK30", size:0x4000},
	{name:"BANK31", size:0x4000},
	{name:"BANK32", size:0x4000},
	{name:"BANK33", size:0x4000},
	{name:"BANK34", size:0x4000},
	{name:"BANK35", size:0x4000},
	{name:"BANK36", size:0x4000},
	{name:"BANK37", size:0x4000},
	{name:"BANK38h", size:0x2000},
	{name:"BANK39h", size:0x2000},
	{name:"BANK3Ah", size:0x2000},
	{name:"BANK3Bh", size:0x2000},
	{name:"BANK3Ch", size:0x2000},
	{name:"BANK3Dh", size:0x2000},
	{name:"BANK3Eh", size:0x2000},
	{name:"BANK3Fh", size:0x2000},
];

var bins = binsizes.map(function(memory, index) { return {bank:memory.name, segments:[], offset:0, size:memory.size}; });

function placeSegmentInBin(bin, segment) {
	bin.segments.push(segment);

	var alignment = segment[2];
	var offset = (bin.offset+(alignment-1))/alignment;
	offset *= alignment;
	offset += segment[1];
	bin.offset = offset;
}

function placeSegment(bins, segment) {
	var bestFit = -1;
	var bestSpaceLeft = 1000000;
	bins.forEach(function(bin, index) {
		if ((segment[3] == true) && (bin.segments.length>0))
			return;

		var alignment = segment[2];
		var offset = (bin.offset+(alignment-1))/alignment;
		offset *= alignment;

		var nextOffset = offset + segment[1];
		var spaceLeft = bin.size-nextOffset;
		if ((spaceLeft>=0) && (spaceLeft < bestSpaceLeft)) {
			bestFit = index;
			bestSpaceLeft = spaceLeft;
		}
	});

	if (bestFit == -1)
		return false;

	placeSegmentInBin(bins[bestFit], segment);

	return true;
}

var hardcodedSegments = [
	["ZP",				"load=ZP, type=ZP"],
	["FONT",			"load=BANK00H"],
	["START",			"load=BANK00H, run=RAM, define=yes"],
	["BOOT",			"load=BANK00H"],
	["BSS",				"load=RAM, type=BSS, define=yes"],
	["EAPI", 			"load=BANK00H, start=$f800, define=yes"],
	["NAME", 			"load=BANK00H, start=$fb00"],
	["VECTORS",			"load=BANK00H, start=$fffa"],

	["MEMCODE_RO",		"load=BANK07bb0, run=RAM, type=RO, define=yes, align=256"],
	["MEMCODE_RW",		"load=BANK07bb1, run=RAM, type=RW, define=yes"],

	["FRAMEBUFFER",		"load=RAM, type=BSS, align=8, define=yes"],

	["GAMESTATEM", 		"load=RAM, type=BSS, define=yes"],
	["LEVELSTATE", 		"load=RAM, type=BSS, define=yes"],
	["ITEMDAT", 		"load=BANK07bb2, run=RAM, define=yes"],
	["QUICKSWAP", 		"load=RAM, type=BSS, define=yes"],
	["HIRAM", 			"load=BANK07bb3, run=RAM, type=RW, define=yes"],

	["SAVE0",			"load=BANK38l"],
	["SAVE1",			"load=BANK39l"],
	["SAVE2",			"load=BANK3Al"],

	["RENDERER_BSS",	"load=RRAM, type=BSS"],

	["INTRO_RAM_INIT",	"load=BANK36"],
	["INTRO_RAM",		"load=BANK36, run=INTRO_RAM, define=yes"],
	["INTRO_BSS",		"load=INTRO_RAM, align=256, type=BSS"],
	["ENDING_RAM_INIT",	"load=BANK36"],
	["ENDING_RAM",		"load=BANK36, run=ENDING_RAM, define=yes"],
	["ENDING_BSS",		"load=ENDING_RAM, align=256, type=BSS, define=yes"],

	["MUSIC_1_BSS", 	"load=MRAM1, type=BSS, define=YES"],
	["MUSIC_1_ZP", 		"load=MZP1, type=BSS"],
	["MUSIC_2_BSS", 	"load=MRAM2, type=BSS, define=YES"],
	["MUSIC_2_ZP", 		"load=MZP2, type=BSS"],
	["MUSIC_3_BSS", 	"load=MRAM3, type=BSS, define=YES"],
	["MUSIC_3_ZP", 		"load=MZP3, type=BSS"],
	["MUSIC_4_BSS", 	"load=MRAM4, type=BSS, define=YES"],
	["MUSIC_4_ZP", 		"load=MZP4, type=BSS"],
	["MUSIC_5_BSS", 	"load=MRAM5, type=BSS, define=YES"],
	["MUSIC_5_ZP", 		"load=MZP5, type=BSS"]
];

var hardcodedSegmentsList = hardcodedSegments.map(row => row[0]);
function readSegmentsFromMap() {
	const fs = require('fs');
	const mapfile = fs.readFileSync("map", "utf8");
	const lines = mapfile.split("\n");

	var index = lines.indexOf("Segment list:")+4;
	var segments = [];
	while(true) {
		const line = lines[index];
		if (line === "")
			break;

		const data = line.split(/(\s+)/).filter( function(e) { return e.trim().length > 0; } );
		const segment = [
			data[0],
			parseInt(data[3], 16),
			parseInt(data[4], 16)
		];
		segments.push(segment);

		index++;
	}

	return segments;
}

// Get all segments
var segments = readSegmentsFromMap();

// Filter out the hard coded segments
segments = segments.filter(segment => hardcodedSegmentsList.indexOf(segment[0]) === -1);

// Filter out the special segs and place them
segments = segments.filter(function(segment) {
	if (segment[0] == "MAIN") {
		placeSegmentInBin(bins[0], segment);
		return false;
	} else if (segment[0] == "ITEMFUNCS") {
		placeSegmentInBin(bins[3], segment);
		return false;
	} else if (segment[0] == "TEXTUTIL") {
		placeSegmentInBin(bins[3], segment);
		return false;
	}
	return true;
});

// Sort the remaining segments and place them
segments.sort(function(a,b) {
	return b[1]-a[1];
});
segments.forEach(segment => placeSegment(bins, segment));

// Dump hardcodeds
console.log("# ----- FIXED ".padEnd(60,"-")+"\n");
hardcodedSegments.forEach(function(row) {
	console.log("\t"+(row[0]+":").padEnd(30)+row[1]+";");
});
console.log("\n# ----- BIN PACKED ".padEnd(60,"-")+"\n");

// Dump bins
bins.forEach(function(bin) {
	bin.segments.forEach(function(segment) {
		var line = "\t"+(segment[0]+":").padEnd(30)+"load="+bin.bank;
		if (segment[2]>1)
			line += ", align="+segment[2];
		line += ";";

		console.log(line);
	});
});*/
