.macpack cbm

.segment "MAIN"
				jmp main

.proc printHex
				sta sm+1
				txa
				pha
				tya
				pha
				sm:lda #0
				ldx #0
				lsr
				lsr
				lsr
				lsr
				tay
				jsr write
				inx
				lda sm+1
				and #$f
				tay
				jsr write
				pla
				tay
				pla
				tax
				rts
write:			lda hex,y
dst:			sta $dead,x
				rts				
hex:			scrcode "0123456789ABCDEF"
.endproc

.macro printHexAt xpos,ypos
				lda #<(__SCREEN_RUN__+xpos+ypos*40)
				sta printHex::dst+1
				lda #>(__SCREEN_RUN__+xpos+ypos*40)
				sta printHex::dst+2
.endmacro

.proc drawString
				ldx #0
				:
src:				lda $dead,x
					beq :+
dst:				sta __SCREEN_RUN__,x
					inx
				bne :-
				:
				rts
.endproc

.macro drawStringAt string,xpos,ypos
				lda #<string
				sta drawString::src+1
				lda #>string
				sta drawString::src+2
				lda #<(__SCREEN_RUN__+(xpos)+(ypos)*40)
				sta drawString::dst+1
				lda #>(__SCREEN_RUN__+(xpos)+(ypos)*40)
				sta drawString::dst+2
				jsr drawString
.endmacro

.proc main
				sei
				ldx #0
				:
					.repeat 4,I
						lda __UMAX_LOAD__+I*256,x
						sta __UMAX_RUN__+I*256,x
						lda #1
						sta $d800+I*256,x
						lda #$20
						sta __SCREEN_RUN__+I*256,x
						lda #$31
						sta 1
						lda $d000+I*256,x
						sta __FONT_RUN__+I*256,x
						lda #$37
						sta 1
					.endrep
					inx
				bne :-
				:
					txa
					sta __XFER_RUN__,x
					inx
				bne :-
				cli

				jsr setupScreen

				lda #6
				sta $d020
				sta $d021
				lda #$32
				sta $d018
				lda #$3f
				sta $dd00
				lda #$1b
				sta $d011

				jsr getDeviceID
				printHexAt 0,24
				txa
				jsr printHex
				printHexAt 3,24
				tya
				jsr printHex

				jsr eraseFlash
				jsr openFile

				ldx #0
				sectorLoop:
					stx sector
					lda rom_lo,x
					sta dst+1
					lda rom_hi,x
					sta dst+2

					ldy #>$8000
					txa
					and #$40
					beq :+
						ldy #>$e000
					:
					sty hidst+1

					ldx #0
					blockLoop:
						stx count

						jsr readBlock

						lda count
dst:					sta __SCREEN_RUN__
hidst:					ora #>$8000
						tay
						lda sector
						and #$3f
						jsr writeBlock

						ldx count
						inx
						cpx #$20
					bne blockLoop

					ldx sector
					inx
					cpx #$80
				bne sectorLoop

				:
					inc __SCREEN_RUN__+39+24*40
				jmp :-

count:			.byte 0
sector:			.byte 0

rom_lo:			.repeat 2,J
					.repeat 64,I
						.scope
							sector = I/8
							block = I&7
							row = sector/4
							col = sector&3
							.byte <(__SCREEN_RUN__+ 5 + block + col*9 + (J*3+row)*40)
						.endscope
					.endrep
				.endrep

rom_hi:			.repeat 2,J
					.repeat 64,I
						.scope
							sector = I/8
							block = I&7
							row = sector/4
							col = sector&3
							.byte >(__SCREEN_RUN__+ 5 + block + col*9 + (0+row)*40)
						.endscope
					.endrep
				.endrep
.endproc

.proc openFile
				lda #fname_end-fname
				ldx #<fname
				ldy #>fname
				jsr $ffbd     ; call SETNAM

				lda #$02      ; file number 2
				ldx $BA       ; last used device number
				bne :+
					ldx #$08      ; default to device 8
				:
				ldy #$02      ; secondary address 2
				jsr $ffba     ; call SETLFS

				jsr $ffc0     ; call OPEN
				bcc :+	  ; if carry set, the file could not be opened
					sta $d021
					inc $d020
					jmp *-3
				:

				ldx #$02      ; filenumber 2
        		jsr $FFC6     ; call CHKIN (file 2 now used as input)

				rts

fname:			.byte "EOB.BIN"
fname_end:
.endproc

.proc readBlock
				ldy #0
				loop:
					jsr $ffcf
					sta __XFER_RUN__,y
					iny
				bne loop
				rts
.endproc

.proc eraseFlash
				.repeat 8,I
					.scope
					row = I/4
					col = I & 3

					drawStringAt eraseString, 5+col*9,0+row
					lda #I*8
					ldy #>$8000
					jsr eraseSector
					drawStringAt emptyString, 5+col*9,0+row

					drawStringAt eraseString, 5+col*9,3+row
					lda #I*8
					ldy #>$e000
					jsr eraseSector
					drawStringAt emptyString, 5+col*9,3+row
					.endscope
				.endrep
				rts
				
eraseString:	scrcode "EEEEEEEE"
				.byte 0
emptyString:	scrcode "--------"
				.byte 0
.endproc

.proc setupScreen
				drawStringAt roml,0,0
				drawStringAt romh,0,3

				ldx #7
				lda #'.'
				:
					.repeat 4,I
						sta __SCREEN_RUN__+5+0*40 + I*9,x
						sta __SCREEN_RUN__+5+1*40 + I*9,x
						sta __SCREEN_RUN__+5+3*40 + I*9,x
						sta __SCREEN_RUN__+5+4*40 + I*9,x
					.endrep
					dex
				bpl :-

				rts

roml:			scrcode "ROML"
				.byte 0
romh:			scrcode "ROMH"
				.byte 0
.endproc

.segment "UMAX"
.macro enterUmax
				sei
				lda #5
				sta $de02
.endmacro

.proc exitUmax
				lda #4
				sta $de02
				cli
				rts
.endproc

.if 0
.proc getDeviceID
				enterUmax
				lda #$aa
				sta $8555
				lda #$55
				sta $82aa
				lda #$90
				sta $8555
				ldx $8000
				ldy $8001
				lda #$f0
				sta $8000
				jmp exitUmax
.endproc

;a=bank
;y=hibyte
.proc eraseSector
				sta $de00
				enterUmax

				sty hi+2
				sty sm5+2
				sty sm6+2
				tya
				and #$f0
				ora #$5
				sta sm0+2
				sta sm2+2
				sta sm3+2
				eor #$5^2
				sta sm1+2
				sta sm4+2

				lda #$aa
sm0:			sta $8555
				lda #$55
sm1:			sta $82aa
				lda #$80
sm2:			sta $8555
				lda #$aa
sm3:			sta $8555
				lda #$55
sm4:			sta $82aa
				lda #$30
sm5:			sta $8000

				ldx #0
				check:
					lda #$ff
					:
						hi:and $8000,x
						inx
					bne :-
					cmp #$ff
				bne check

				lda #$f0
sm6:			sta $8000

				jmp exitUmax
.endproc

;a=bank
;y=hibyte
.proc writeBlock
				sta $de00
				enterUmax

				sty hi1+2
				sty hi2+2
				tya
				and #$f0
				ora #$5
				sta sm1+2
				sta sm3+2
				eor #$5^$2
				sta sm2+2

				ldx #0
				:
					lda #$aa
sm1:				sta $8555
					lda #$55
sm2:				sta $82aa
					lda #$a0
sm3:				sta $8555
					lda __XFER_RUN__,x
					sta __SCREEN_RUN__+10*40,x
hi1:				sta $8000,x
hi2:				cmp $8000,x
					bne hi2
					inx
				bne :-

				jmp exitUmax
.endproc

.else
.proc getDeviceID
				enterUmax
				lda #$aa
				sta $8aaa
				lda #$55
				sta $8555
				lda #$90
				sta $8aaa
				ldx $8000
				ldy $8002
				lda #$f0
				sta $8000
				jmp exitUmax
.endproc

;a=bank
;y=hibyte
.proc eraseSector
				sta $de00
				enterUmax

				sty hi+2
				sty sm5+2
				sty sm6+2
				tya
				and #$f0
				ora #$5
				sta sm1+2
				sta sm4+2
				eor #$5^$a
				sta sm0+2
				sta sm2+2
				sta sm3+2

				lda #$aa
sm0:			sta $8aaa
				lda #$55
sm1:			sta $8555
				lda #$80
sm2:			sta $8aaa
				lda #$aa
sm3:			sta $8aaa
				lda #$55
sm4:			sta $8555
				lda #$30
sm5:			sta $8000

				ldx #0
				check:
					lda #$ff
					:
						hi:and $8000,x
						inx
					bne :-
					cmp #$ff
				bne check

				lda #$f0
sm6:			sta $8000

				jmp exitUmax
.endproc

;a=bank
;y=hibyte
.proc writeBlock
				sta $de00
				enterUmax

				sty hi1+2
				sty hi2+2
				tya
				and #$f0
				ora #$5
				sta sm2+2
				eor #$5^$a
				sta sm1+2
				sta sm3+2

				ldx #0
				:
					lda #$aa
sm1:				sta $8aaa
					lda #$55
sm2:				sta $8555
					lda #$a0
sm3:				sta $8aaa
					lda __XFER_RUN__,x
hi1:				sta $8000,x
hi2:				cmp $8000,x
					bne hi2
					inx
				bne :-

				jmp exitUmax
.endproc

.endif

.segment "XFER"
				.res 256

.segment "FONT"
				.res 1024

.segment "SCREEN"
				.res 1024
