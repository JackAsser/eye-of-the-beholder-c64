import javax.swing.*;
import java.awt.image.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.*;
import java.util.function.*;
import java.io.*;

public class Editor extends JPanel {
	public interface Delegate {
		public void renderContext(BufferedImage dst, Asset asset);
		public void renderAsset(BufferedImage dst, Asset asset);
		default public void renderPC(BufferedImage dst, Asset asset) {}
		public Asset getAsset(int row, int column);
		public void reserveAssets() throws IOException, ClassNotFoundException;
		public boolean save();
		default public JPanel getExtraUI() {
			return null;
		}
	}

	private Point borderOffset = new Point(0,0);

	public Delegate delegate;

	private BufferedImage displayArea;
	private BufferedImage displayArea64;
	private BufferedImage displayAreaContext64;
	private BufferedImage displayAreaAsset64;
	private BufferedImage clashArea64;
	private BufferedImage crtImage;
	private BufferedImage zoomArea;
	private BufferedImage zoomedDisplayArea64;
	private Colodore colodore = new Colodore();

	private Point mousePosition = new Point(0,0);
	protected Point mousePixel = new Point(0,0);
	protected Point mouseSrcPixel = new Point(0,0);
	protected Point mouseChar = new Point(0,0);
	protected Point mouseSrcChar = new Point(0,0);

	protected int level=0;

	private JRadioButton radioButtons[][];
	private Actions actions[][];
	private Actions.Action currentAction;
	private int selectedRow = 0;
	private int selectedColumn = 0;

	private boolean pcGraphics = false;
	private boolean crtEmulation = true;
	private boolean hideZoom = false;
	private boolean mouseVisible = true;

	private boolean clashCells[][] = new boolean[22][15];

	private JPanel panel, leftBar, topBar;
	protected Asset currentAsset;

	private Point markBegin;
	private Point markEnd;
	private BufferedImage pasteImage;
	private Point pasteAnchor;

	private boolean needsSave=false;

	private static final int MODE_PLOT = 0;
	private static final int MODE_MARK = 1;
	private static final int MODE_PASTE = 2;
	private int mode = MODE_PLOT;

	protected void needsSave() {
		needsSave = true;
		EotBGfxEditor.sharedInstance.saveStateChanged();
	}

	public boolean saveNeeded() {
		return needsSave;
	}

	public Editor(int level) {
		super();
		this.level = level;

		setFocusTraversalKeysEnabled(false);

		displayArea64 = new BufferedImage(22*4, 15*8, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)EotBGfxEditor.sharedInstance.wallsetGraphics[level].getColorModel());
		displayAreaContext64 = new BufferedImage(22*4, 15*8, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)EotBGfxEditor.sharedInstance.wallsetGraphics[level].getColorModel());
		displayAreaAsset64 = new BufferedImage(22*4, 15*8, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)EotBGfxEditor.sharedInstance.wallsetGraphics[level].getColorModel());
		clashArea64 = new BufferedImage(22*4, 15*8, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)EotBGfxEditor.sharedInstance.wallsetGraphics[level].getColorModel());
		displayArea = new BufferedImage(22*8, 15*8, BufferedImage.TYPE_INT_ARGB);
		zoomArea = new BufferedImage(EotBGfxEditor.ZOOMW*EotBGfxEditor.ZOOMS*8, EotBGfxEditor.ZOOMH*EotBGfxEditor.ZOOMS*8, BufferedImage.TYPE_INT_ARGB);
		zoomedDisplayArea64 = new BufferedImage(22*8*EotBGfxEditor.ZOOMS, 15*8*EotBGfxEditor.ZOOMS, BufferedImage.TYPE_INT_ARGB);
		leftBar = new JPanel();

		panel = new JPanel() {
			public void paint(Graphics g) {
				Dimension preferredSize = new Dimension(displayArea.getWidth()*EotBGfxEditor.ZOOM, displayArea.getHeight()*EotBGfxEditor.ZOOM);
				borderOffset = new Point(
					(int)(panel.getWidth()-preferredSize.getWidth())/2,
					(int)(panel.getHeight()-preferredSize.getHeight())/2
				);

				g.setColor(Color.DARK_GRAY);
				g.fillRect(0,0,panel.getWidth(), panel.getHeight());

				g.translate(borderOffset.x, borderOffset.y);

				if (pcGraphics)
					g.drawImage(displayArea,0,0,displayArea.getWidth()*EotBGfxEditor.ZOOM, displayArea.getHeight()*EotBGfxEditor.ZOOM,null);
				else {
					if (crtEmulation) {
						g.drawImage(crtImage,0,0,displayArea64.getWidth()*EotBGfxEditor.ZOOM*2, displayArea64.getHeight()*EotBGfxEditor.ZOOM,null);
					} else {
						g.drawImage(displayArea64,0,0,displayArea64.getWidth()*EotBGfxEditor.ZOOM*2, displayArea64.getHeight()*EotBGfxEditor.ZOOM,null);						
					}

					if (!hideZoom) {
						g.setColor(Color.RED);
						for (int cy=0; cy<15; cy++) {
							for (int cx=0; cx<22; cx++) {
								if (clashCells[cx][cy]) {
									g.drawRect(cx*8*EotBGfxEditor.ZOOM, cy*8*EotBGfxEditor.ZOOM, 8*EotBGfxEditor.ZOOM, 8*EotBGfxEditor.ZOOM);
									g.drawRect(cx*8*EotBGfxEditor.ZOOM-1, cy*8*EotBGfxEditor.ZOOM-1, 8*EotBGfxEditor.ZOOM+2, 8*EotBGfxEditor.ZOOM+2);
								}
							}
						}

						g.setColor(Color.GREEN);
						drawBoundingBox(g);
					}

					if ((mode == MODE_MARK)||(mode == MODE_PASTE)) {
						g.setColor(Color.CYAN);
						Rectangle rect = getMarkRect();
						int x = rect.x*EotBGfxEditor.ZOOM*2;
						int y = rect.y*EotBGfxEditor.ZOOM;
						int w = rect.width*EotBGfxEditor.ZOOM*2;
						int h = rect.height*EotBGfxEditor.ZOOM;
						if (mode == MODE_PASTE)
							g.drawImage(pasteImage, x, y, w, h, null);
						g.drawRect(x,y,w,h);
					}

					if (!hideZoom && mouseVisible)
						g.drawImage(zoomArea, mousePosition.x-zoomArea.getWidth()/2-EotBGfxEditor.ZOOM/2, mousePosition.y-zoomArea.getHeight()/2-EotBGfxEditor.ZOOM/2, null);
				}
			}
		};

		addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				switch (e.getKeyCode()) {
					case KeyEvent.VK_SPACE: {
						hideZoom = false;
						renderZoom();
						repaint();
						break;
					}
					case KeyEvent.VK_TAB: {
						pcGraphics = false;
						panel.repaint();
						break;
					}
				}
			}

			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
					case KeyEvent.VK_P: {
						peekColor();
						break;
					}
					case KeyEvent.VK_BACK_SPACE: {
						EotBGfxEditor.sharedInstance.zoomatic.setSelected(!EotBGfxEditor.sharedInstance.zoomatic.isSelected());
						break;
					}
					case KeyEvent.VK_S: {
						if ((e.getModifiers() & (InputEvent.CTRL_MASK|InputEvent.META_MASK)) != 0) {
							if (delegate.save()) {
								needsSave = false;
								EotBGfxEditor.sharedInstance.saveStateChanged();
							}
						}
						break;
					}
					case KeyEvent.VK_ESCAPE: {
						mode = MODE_PLOT;
						renderZoom();
						repaint();
						break;
					}
					case KeyEvent.VK_ENTER: {
						if (mode == MODE_PASTE)
							paste();
						break;
					}
					case KeyEvent.VK_Z: {
						if ((e.getModifiers() & (InputEvent.CTRL_MASK|InputEvent.META_MASK)) != 0) {
							boolean didChange = false;
							if ((e.getModifiers() & (InputEvent.SHIFT_MASK)) == 0)
								didChange = actions[selectedRow][selectedColumn].undo();
							else
								didChange = actions[selectedRow][selectedColumn].redo();

							if (didChange) {
								needsSave();
								reloadAsset();
							}
						}
						break;
					}

					case KeyEvent.VK_X: {
						if ((mode == MODE_PASTE) && ((e.getModifiers() & InputEvent.SHIFT_MASK) != 0)) {
							PixelRect pixelRect = EotBGfxEditor.sharedInstance.clipboard;
							if (pixelRect!=null) {
								doActionOnCurrentAsset((currentAsset)->{
									pixelRect.xflip();
									renderPaste();
								});
							}
							break;
						}
						clearChar();
						break;
					}

					case KeyEvent.VK_R: {
						boolean replaceGlobally = (e.getModifiers() & InputEvent.SHIFT_MASK) != 0;
						replaceColor(replaceGlobally);
						break;
					}

					case KeyEvent.VK_SPACE: {
						hideZoom = true;
						repaint();
						break;
					}

					case KeyEvent.VK_0: {color(0);break;}
					case KeyEvent.VK_1: {color(1);break;}
					case KeyEvent.VK_2: {color(2);break;}
					case KeyEvent.VK_3: {color(3);break;}
					case KeyEvent.VK_4: {color(4);break;}
					case KeyEvent.VK_5: {color(5);break;}
					case KeyEvent.VK_6: {color(6);break;}
					case KeyEvent.VK_7: {color(7);break;}
					case KeyEvent.VK_8: {color(8);break;}
					case KeyEvent.VK_9: {color(9);break;}
					case KeyEvent.VK_A: {color(0xa);break;}
					case KeyEvent.VK_B: {color(0xb);break;}
					case KeyEvent.VK_C: {
						if ((e.getModifiers() & (InputEvent.CTRL_MASK|InputEvent.META_MASK)) != 0) {
							if ((e.getModifiers() & InputEvent.SHIFT_MASK) == 0)
								copy();
							break;
						}
						color(0xc);
						break;
					}
					case KeyEvent.VK_V: {
						if ((e.getModifiers() & (InputEvent.CTRL_MASK|InputEvent.META_MASK)) != 0) {
							if ((e.getModifiers() & InputEvent.SHIFT_MASK) == 0) {
								paste();
								render(true);
								renderZoom();
								repaint();
							}
						}
						break;
					}
					case KeyEvent.VK_D: {color(0xd);break;}
					case KeyEvent.VK_E: {color(0xe);break;}
					case KeyEvent.VK_F: {color(0xf);break;}

					case KeyEvent.VK_M: {
						crtEmulation = !crtEmulation;
						panel.repaint();
						break;
					}
					case KeyEvent.VK_TAB: {
						pcGraphics = true;
						panel.repaint();
						break;
					}
					case KeyEvent.VK_LEFT: {
						if (e.getModifiers()!=0) return;
						if (mode==MODE_PASTE) {
							PixelRect pixelRect = EotBGfxEditor.sharedInstance.clipboard;
							if ((pixelRect!=null) && (pixelRect.floating)) {
					 			markBegin.x--;
					 			markEnd.x--;
					 			loadAsset();
					 			break;
							}
						}
						if (selectedColumn>0) {
							selectedColumn--;
							radioButtons[selectedRow][selectedColumn].setSelected(true);
							loadAsset();
						}
						break;
					}
					case KeyEvent.VK_RIGHT: {
						if (e.getModifiers()!=0) return;
						if (mode==MODE_PASTE) {
							PixelRect pixelRect = EotBGfxEditor.sharedInstance.clipboard;
							if ((pixelRect!=null) && (pixelRect.floating)) {
					 			markBegin.x++;
					 			markEnd.x++;
					 			loadAsset();
					 			break;
							}
						}
						if (selectedColumn<radioButtons[0].length-1) {
							selectedColumn++;
							radioButtons[selectedRow][selectedColumn].setSelected(true);
							loadAsset();
						}
						break;
					}
					case KeyEvent.VK_UP: {
						if (e.getModifiers()!=0) return;
						if (mode==MODE_PASTE) {
							PixelRect pixelRect = EotBGfxEditor.sharedInstance.clipboard;
							if ((pixelRect!=null) && (pixelRect.floating)) {
					 			markBegin.y--;
					 			markEnd.y--;
					 			loadAsset();
					 			break;
							}
						}
						if (selectedRow>0) {
							selectedRow--;
							radioButtons[selectedRow][selectedColumn].setSelected(true);
							loadAsset();
						}
						break;
					}
					case KeyEvent.VK_DOWN: {
						if (e.getModifiers()!=0) return;
						if (mode==MODE_PASTE) {
							PixelRect pixelRect = EotBGfxEditor.sharedInstance.clipboard;
							if ((pixelRect!=null) && (pixelRect.floating)) {
					 			markBegin.y++;
					 			markEnd.y++;
					 			loadAsset();
					 			break;
							}
						}
						if (selectedRow<radioButtons.length-1) {
							selectedRow++;
							radioButtons[selectedRow][selectedColumn].setSelected(true);
							loadAsset();
						}
						break;
					}
				}
			}
		});

		panel.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				updateMouse(e.getX(), e.getY());

				if ((e.getModifiers() & KeyEvent.SHIFT_MASK)!=0)
					mode = MODE_MARK;
				else {
					if (mode == MODE_MARK) {
						currentAction = null;
						mode = MODE_PLOT;
						renderZoom();
						repaint();
						return;
					}
				}

				if (mode == MODE_PLOT) {
					if (SwingUtilities.isMiddleMouseButton(e)) {
						peekColor();
						reloadAsset();
						return;
					}
					currentAction = new DefaultAction(currentAsset.inPixels());

					if (SwingUtilities.isLeftMouseButton(e))
						plot(false);
					else if (SwingUtilities.isRightMouseButton(e))
						plot(true);

					reloadAsset();
				} else if (mode == MODE_MARK) {
					markBegin = (Point)mousePixel.clone();
					markEnd = (Point)mousePixel.clone();
				} else if (mode == MODE_PASTE) {
					pasteAnchor = new Point(mousePixel.x-markBegin.x, mousePixel.y-markBegin.y);
				}
			}

			public void mouseReleased(MouseEvent e) {
				updateMouse(e.getX(), e.getY());

				if ((mode == MODE_PLOT) && (currentAction!=null)) {
					if (actions[selectedRow][selectedColumn].add(currentAction)) {
						needsSave();
						reloadAsset();
					}
				} else if (mode == MODE_MARK) {
					markEnd = (Point)mousePixel.clone();
				} else if (mode == MODE_PASTE) {
					markBegin = new Point(mousePixel.x-pasteAnchor.x, mousePixel.y-pasteAnchor.y);
					markEnd = new Point(markBegin.x+pasteImage.getWidth()-1, markBegin.y+pasteImage.getHeight()-1);
					renderZoom();
					repaint();
				}
			}

			public void mouseEntered(MouseEvent e) {
				mouseVisible = true;
				requestFocus();
				repaint();				
			}

			public void mouseExited(MouseEvent e) {
				mouseVisible = false;
				repaint();				
			}
		});

		panel.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseDragged(MouseEvent e) {
				updateMouse(e.getX(), e.getY());

				if ((mode == MODE_PLOT) && (currentAction!=null)) {
					if (SwingUtilities.isLeftMouseButton(e))
						plot(false);
					else if (SwingUtilities.isRightMouseButton(e))
						plot(true);

					render(false);
				} else if (mode == MODE_MARK) {
					markEnd = (Point)mousePixel.clone();
				} else if (mode == MODE_PASTE) {
					markBegin = new Point(mousePixel.x-pasteAnchor.x, mousePixel.y-pasteAnchor.y);
					markEnd = new Point(markBegin.x+pasteImage.getWidth()-1, markBegin.y+pasteImage.getHeight()-1);
				}

				renderZoom();
				repaint();
			}

			public void mouseMoved(MouseEvent e) {
				requestFocus();
				updateMouse(e.getX(), e.getY());
				renderZoom();
				repaint();
			}
		});

		setLayout(new BorderLayout(20,20));

		add(panel, BorderLayout.CENTER);
		add(leftBar, BorderLayout.WEST);
	}

	private boolean assetContainsClashes(Asset asset) {
		clear(clashArea64, (byte)16);
		if (asset.mode != Asset.MODE_OVERLAY)
			delegate.renderContext(clashArea64, asset);
		delegate.renderAsset(clashArea64, asset);

		byte pixels[] = ((DataBufferByte)clashArea64.getRaster().getDataBuffer()).getData();
		Asset r = asset;
		for (int cy=r.dy; cy<r.dy+r.h; cy++) {
			for (int cx=r.dx; cx<r.dx+r.w; cx++) {
				if ((cx<0) || (cy<0) || (cx>22) || (cy>15))
					continue;
				if (charContainsClash(pixels, cx, cy, r.mode == Asset.MODE_OVERLAY))
					return true;
			}
		}

		return false;
	}

	private void render(boolean all) {
		clear(displayAreaContext64, (byte)0);
		clear(displayAreaAsset64, (byte)16);
		delegate.renderContext(displayAreaContext64, currentAsset);
		delegate.renderAsset(displayAreaAsset64, currentAsset);

		// Composite
		clear(displayArea64, (byte)0);
		blit(displayArea64, 0,0, displayAreaContext64, 0, 0, displayAreaContext64.getWidth(), displayAreaContext64.getHeight());
		blit(displayArea64, 0,0, displayAreaAsset64, 0, 0, displayAreaAsset64.getWidth(), displayAreaAsset64.getHeight());

		if (all) {
			delegate.renderPC(displayArea, currentAsset);
			generateCRTImage();	
		}

		BufferedImage tmp = currentAsset.mode == Asset.MODE_OVERLAY ? displayAreaAsset64 : displayArea64;

		// Calc clashes
		byte pixels[] = ((DataBufferByte)tmp.getRaster().getDataBuffer()).getData();
		Asset r = currentAsset;
		for (int cy=0; cy<15; cy++)
			for (int cx=0; cx<22; cx++)
				clashCells[cx][cy] = false;
		boolean anyClash = false;
		for (int cy=r.dy; cy<r.dy+r.h; cy++)
			for (int cx=r.dx; cx<r.dx+r.w; cx++) {
				if ((cx<0) || (cy<0) || (cx>22) || (cy>15))
					continue;
				if (charContainsClash(pixels, cx, cy, r.mode == Asset.MODE_OVERLAY)) {
					clashCells[cx][cy] = true;
					anyClash = true;
				}
			}

		// Update clash indicators
		if (anyClash) {
			radioButtons[selectedRow][selectedColumn].setBackground(Color.RED);
			radioButtons[selectedRow][selectedColumn].setOpaque(true);
		}
		else
			radioButtons[selectedRow][selectedColumn].setOpaque(false);

		// Render zoomer
		Graphics g = zoomedDisplayArea64.getGraphics();
		g.setColor(new Color(40,0,80));
		g.fillRect(0,0,zoomedDisplayArea64.getWidth(), zoomedDisplayArea64.getHeight());
		g.drawImage(tmp, 0, 0, zoomedDisplayArea64.getWidth(), zoomedDisplayArea64.getHeight(), null);

		g.setColor(Color.DARK_GRAY);
		for (int cy=0; cy<15; cy++)
			g.drawLine(0, cy*8*EotBGfxEditor.ZOOMS, 22*8*EotBGfxEditor.ZOOMS, cy*8*EotBGfxEditor.ZOOMS);
		for (int cx=0; cx<22; cx++)
			g.drawLine(cx*8*EotBGfxEditor.ZOOMS, 0, cx*8*EotBGfxEditor.ZOOMS, 15*8*EotBGfxEditor.ZOOMS);

		g.setColor(Color.RED);
		for (int cy=0; cy<15; cy++) {
			for (int cx=0; cx<22; cx++) {
				if (clashCells[cx][cy]) {
					g.drawRect(cx*8*EotBGfxEditor.ZOOMS, cy*8*EotBGfxEditor.ZOOMS, 8*EotBGfxEditor.ZOOMS-1, 8*EotBGfxEditor.ZOOMS-1);
				}
			}
		}

		g.setColor(Color.GREEN);
		g.drawRect(r.dx*8*EotBGfxEditor.ZOOMS-1, r.dy*8*EotBGfxEditor.ZOOMS-1, r.w*8*EotBGfxEditor.ZOOMS+2, r.h*8*EotBGfxEditor.ZOOMS+2);
		g.drawRect(r.dx*8*EotBGfxEditor.ZOOMS-2, r.dy*8*EotBGfxEditor.ZOOMS-2, r.w*8*EotBGfxEditor.ZOOMS+4, r.h*8*EotBGfxEditor.ZOOMS+4);
	}

	protected void redrawAll() {
		loadAsset();
		render(true);
		renderZoom();
		panel.repaint();
	}

	private void renderZoom() {
		Graphics g = zoomArea.getGraphics();
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0,0, zoomArea.getWidth(), zoomArea.getHeight());

		// Render zoom
		int sx = mousePosition.x*EotBGfxEditor.ZOOMS/EotBGfxEditor.ZOOM - zoomArea.getWidth()/2;
		int sy = mousePosition.y*EotBGfxEditor.ZOOMS/EotBGfxEditor.ZOOM - zoomArea.getHeight()/2;
		g.drawImage(zoomedDisplayArea64,
			0, 0, zoomArea.getWidth(), zoomArea.getHeight(), 
			sx, sy, sx+zoomArea.getWidth(), sy+zoomArea.getHeight(),
			null);

		// Render highlighted pixel
		if (!mouseOutsideAsset(currentAsset)) {
			int px = (mousePosition.x/EotBGfxEditor.ZOOM)&0xfffffffe;
			int py = mousePosition.y/EotBGfxEditor.ZOOM;
			int fracx = px*EotBGfxEditor.ZOOMS - mousePosition.x*EotBGfxEditor.ZOOMS/EotBGfxEditor.ZOOM;
			int fracy = py*EotBGfxEditor.ZOOMS - mousePosition.y*EotBGfxEditor.ZOOMS/EotBGfxEditor.ZOOM;
			int palette[] = new int[256];
			((IndexColorModel)currentAsset.src.getColorModel()).getRGBs(palette);
			g.setColor(new Color(palette[EotBGfxEditor.sharedInstance.selectedColor]));
			g.drawRect(zoomArea.getWidth()/2+fracx, zoomArea.getHeight()/2+fracy, EotBGfxEditor.ZOOMS*2-1, EotBGfxEditor.ZOOMS-1);

			g.setColor(Color.BLACK);
			g.drawRect(zoomArea.getWidth()/2+fracx-1, zoomArea.getHeight()/2+fracy-1, EotBGfxEditor.ZOOMS*2+1, EotBGfxEditor.ZOOMS+1);
			g.drawRect(zoomArea.getWidth()/2+fracx+1, zoomArea.getHeight()/2+fracy+1, EotBGfxEditor.ZOOMS*2-3, EotBGfxEditor.ZOOMS-3);
		}

		if ((mode == MODE_MARK)||(mode == MODE_PASTE)) {
			g.setColor(Color.CYAN);
			Rectangle rect = getMarkRect();
			int x = rect.x*EotBGfxEditor.ZOOMS*2;
			int y = rect.y*EotBGfxEditor.ZOOMS;
			int w = rect.width*EotBGfxEditor.ZOOMS*2;
			int h = rect.height*EotBGfxEditor.ZOOMS;
			if (mode == MODE_PASTE)
				g.drawImage(pasteImage, x-sx, y-sy, w, h, null);
			g.drawRect(x-sx-1,y-sy-1,w+1,h+1);
		}

		// Render border
		g.setColor(Color.BLACK);
		g.drawRect(0,0,zoomArea.getWidth()-1,zoomArea.getHeight()-1);
		g.drawRect(2,2,zoomArea.getWidth()-5,zoomArea.getHeight()-5);
		g.setColor(Color.WHITE);
		g.drawRect(1,1,zoomArea.getWidth()-3,zoomArea.getHeight()-3);
	}

	private Rectangle getMarkRect() {
		int x0 = Math.min(markBegin.x, markEnd.x);
		int y0 = Math.min(markBegin.y, markEnd.y);
		int x1 = Math.max(markBegin.x, markEnd.x);
		int y1 = Math.max(markBegin.y, markEnd.y);
		return new Rectangle(x0, y0, x1-x0+1, y1-y0+1);
	}

	public static int wallSrcOffsetX[] = {0,30,20,14,0,3,6,8,9,12,14};

	public static int wallSizes[][] = {
		{22,15},
		{16,12},
		{10,8},
		{6,5},
		{3,15},
		{3,12},
		{2,8},
		{1,5},
		{3,5},
		{2,6}
	};

	public static int wallDstOffset[][] = {
		{0,0},
		{3,1},
		{6,2},
		{8,3},
		{0,0},
		{3,1},
		{6,2},
		{8,3},
		{0,3},
		{2,3}
	};

	public static boolean wallHasRectangle[][] = {
		{true, 	false,	false,	false,	false,	false,	false,	false,	false,	false},
		{false,	true,	true,	true,	true,	true,	true,	true,	true,	true},
		{false,	true,	true,	true,	true,	true,	true,	true,	true,	true},
		{false,	true,	true,	true,	false,	false,	false,	false,	true,	false},
		{false,	true,	true,	true,	true,	true,	true,	true,	true,	true},
		{false,	true,	true,	true,	true,	true,	true,	true,	true,	true},
		{false,	true,	true,	true,	true,	true,	true,	true,	true,	true}		
	};

	protected void reserveAssets(int nbrRows, int nbrColumns) {
		if (actions != null)
			return;

		topBar = delegate.getExtraUI();
		if (topBar != null)
			add(topBar, BorderLayout.NORTH);

		actions = new Actions[nbrRows][nbrColumns];
		for (int i=0; i<nbrRows; i++)
			for (int j=0; j<nbrColumns; j++)
				actions[i][j] = new Actions();

		radioButtons = new JRadioButton[nbrRows][nbrColumns];
		leftBar.setLayout(new GridLayout(nbrRows, nbrColumns+1));
		ButtonGroup bg = new ButtonGroup();
		for (int i=0; i<nbrRows; i++) {
			leftBar.add(new JLabel(String.format("%02d:", i)));
			for (int j=0; j<nbrColumns; j++) {
				JRadioButton rb = new JRadioButton();
				rb.setFocusable(false);
				leftBar.add(rb);
				bg.add(rb);
				radioButtons[i][j] = rb;

				Asset asset = delegate.getAsset(i,j);
				if (asset==null)
					rb.setVisible(false);
				else {
					if (assetContainsClashes(asset)) {
						rb.setBackground(Color.RED);
						rb.setOpaque(true);
					}
				}

				final int _i = i;
				final int _j = j;
				rb.addActionListener((e) -> {
					currentAsset = delegate.getAsset(_i, _j);
					if (currentAsset != null) {
						selectedRow = _i;
						selectedColumn = _j;
						redrawAll();
					}
				});
			}
		}

		outer:for (int i=0; i<nbrRows; i++)
			for (int j=0; j<nbrColumns; j++) {
				currentAsset = delegate.getAsset(i, j);
				if (currentAsset != null) {
					selectedRow = i;
					selectedColumn = j;
					radioButtons[selectedRow][selectedColumn].setSelected(true);
					break outer;
				}
			}
	}

	protected void renderPCWall(BufferedImage dst, int wallType, int rectangleIndex) {
		renderPCWall(dst, wallType, rectangleIndex, 0, false);
	}

	protected void renderPCWall(BufferedImage dst, int wallType, int rectangleIndex, boolean flipped) {
		renderPCWall(dst, wallType, rectangleIndex, 0, flipped);
	}

	protected void renderPCWall(BufferedImage dst, int wallType, int rectangleIndex, int deltax) {
		renderPCWall(dst, wallType, rectangleIndex, deltax,  false);
	}

	protected void renderPCWall(BufferedImage dst, int wallType, int rectangleIndex, int deltax, boolean flipped) {
		if ((wallType!=0) && (rectangleIndex==0))
			return;

		int w = wallSizes[rectangleIndex][0]*8;
		int h = wallSizes[rectangleIndex][1]*8;
		int dx = wallDstOffset[rectangleIndex][0]*8+deltax*w;
		int dy = wallDstOffset[rectangleIndex][1]*8;
		int sx = wallSrcOffsetX[rectangleIndex]*8;
		int sy = wallType*15*8;

		Graphics g = dst.getGraphics();

		if (!flipped)
			g.drawImage(EotBGfxEditor.sharedInstance.wallsetPCGraphics[level],
				dx,dy, dx+w,dy+h,
				sx,sy, sx+w,sy+h,
				null);
		else {
			/* TODO */
		}
	}

	protected void renderWall(BufferedImage dst, int wallType, int rectangleIndex) {
		renderWall(dst, wallType, rectangleIndex, 0, false);
	}

	protected void renderWall(BufferedImage dst, int wallType, int rectangleIndex, boolean flipped) {
		renderWall(dst, wallType, rectangleIndex, 0, flipped);
	}

	protected void renderWall(BufferedImage dst, int wallType, int rectangleIndex, int deltax) {
		renderWall(dst, wallType, rectangleIndex, deltax,  false);
	}

	protected void renderWall(BufferedImage dst, int wallType, int rectangleIndex, int deltax, boolean flipped) {
		if ((wallType!=0) && (rectangleIndex==0))
			return;

		int w = wallSizes[rectangleIndex][0]*4;
		int h = wallSizes[rectangleIndex][1]*8;
		int dx = wallDstOffset[rectangleIndex][0]*4+deltax*w;
		int dy = wallDstOffset[rectangleIndex][1]*8;
		int sx = wallSrcOffsetX[rectangleIndex]*4;
		int sy = wallType*15*8;

		blit(dst, dx,dy, EotBGfxEditor.sharedInstance.wallsetGraphics[level], sx,sy, w,h, flipped);
	}

	protected void blit(BufferedImage dst, int dx, int dy, BufferedImage src, int sx, int sy, int w, int h) {
		blit(dst, dx, dy, src, sx, sy, w, h, false);
	}

	protected void blit(BufferedImage dst, int dx, int dy, BufferedImage src, int sx, int sy, int w, int h, boolean flipped) {
		byte srcPixels[] = ((DataBufferByte)src.getRaster().getDataBuffer()).getData();
		byte dstPixels[] = ((DataBufferByte)dst.getRaster().getDataBuffer()).getData();
		int srcWidth = src.getWidth();
		int srcHeight = src.getHeight();
		int dstWidth = dst.getWidth();
		int dstHeight = dst.getHeight();

		if (flipped)
			dx = 22*4-dx - w;
		for (int y=0; y<h; y++) {
			for (int x=0; x<w; x++) {
				if ((x+dx<0) || (y+dy<0) || (x+dx>=dstWidth) || (y+dy>=dstHeight))
					continue;
				int srcOffset = flipped ? (y+sy)*srcWidth + (w-1-x)+sx : (y+sy)*srcWidth + x+sx;
				int dstOffset = (y+dy)*dstWidth + x+dx;
				byte c = srcPixels[srcOffset];
				if ((c&0xff)<=15)
					dstPixels[dstOffset] = c;
			}
		}
	}

	protected void blit(BufferedImage dst, Asset asset) {
		Asset r = asset.inPixels();
		blit(dst, r.dx, r.dy, r.src, r.sx, r.sy, r.w, r.h);
	}

	protected void blit(BufferedImage dst, Asset asset, int deltax, int deltay) {
		Asset r = asset.inPixels();
		blit(dst, r.dx+deltax*4, r.dy+deltay*5, r.src, r.sx, r.sy, r.w, r.h);
	}

	protected void clear(BufferedImage dst, byte color) {
		byte pixels[] = ((DataBufferByte)dst.getRaster().getDataBuffer()).getData();
		for (int i=0; i<pixels.length; i++)
			pixels[i] = color;
	}

	private boolean charContainsClash(byte[] pixels, int cx, int cy, boolean renderAsOverlay) {
		boolean isOpaque = true;
		if (renderAsOverlay) {
			opaqueCheck:for (int py=0; py<8; py++) {
				for (int px=0; px<4; px++) {
					int offset = (cy*8+py)*22*4 + cx*4 + px;
					if ((pixels[offset]&0xff) >= 16) {
						isOpaque = false;
						break opaqueCheck;
					}
				}
			}
		}

		if (isOpaque) {
			HashSet<Byte> uniqueColors = new HashSet<Byte>();
			for (int py=0; py<8; py++) {
				for (int px=0; px<4; px++) {
					int offset = (cy*8+py)*22*4 + cx*4 + px;
					byte color = pixels[offset];
					if (color!=0)
						uniqueColors.add(color);
				}
			}
			return uniqueColors.size()>3;
		} else {
			for (int py=0; py<8; py++) {
				for (int px=0; px<4; px++) {
					int offset = (cy*8+py)*22*4 + cx*4 + px;
					byte color = pixels[offset];
					if ((color & 0xff) >= 16)
						continue;
					if ((color!=0)&&(color!=0x9)&&(color!=0xc)&&(color!=0xf))
						return true;
				}
			}
			return false;
		}
	}

	public static BufferedImage replacePalette(BufferedImage palette, BufferedImage src) {
		IndexColorModel srcCM = (IndexColorModel)palette.getColorModel();
		byte r[] = new byte[256];
		byte g[] = new byte[256];
		byte b[] = new byte[256];
		srcCM.getReds(r);
		srcCM.getGreens(g);
		srcCM.getBlues(b);
		for (int i=16; i<256; i++) {
			r[i]=40;
			g[i]=0;
			b[i]=80;
		}
		IndexColorModel icm = new IndexColorModel(8,256,r,g,b);

		BufferedImage dst = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_BYTE_INDEXED, icm);
		byte srcPixels[] = ((DataBufferByte)src.getRaster().getDataBuffer()).getData();
		byte dstPixels[] = ((DataBufferByte)dst.getRaster().getDataBuffer()).getData();
		for (int i=0; i<srcPixels.length; i++)
			dstPixels[i]=srcPixels[i];
		return dst;
	}

	private void generateCRTImage() {
		Graphics g = displayArea64.getGraphics();
		BufferedImage image = colodore.apply(displayArea64, false);
		crtImage = new BufferedImage(image.getWidth()*2, image.getHeight()*2, BufferedImage.TYPE_INT_ARGB);
		AffineTransform at = new AffineTransform();
		at.scale(2.0, 2.0);
		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BICUBIC);
		crtImage = scaleOp.filter(image, crtImage);
	}

	protected boolean mouseOutsideAsset(Asset asset) {
		Asset r = asset.inPixels();
		return (mouseChar.x<r.dx)||(mouseChar.y<r.dy)||(mouseChar.x>=r.dx+r.w)||(mouseChar.y>=r.dy+r.h);
	}

	protected void doActionOnCurrentAsset(Consumer<Asset> consumer) {
		if (mouseOutsideAsset(currentAsset)) return;

		DefaultAction action = new DefaultAction(currentAsset.inPixels());
		consumer.accept(currentAsset);
		if (actions[selectedRow][selectedColumn].add(action)) {
			needsSave();
			reloadAsset();
		}
	}

	private void clearChar() {
		doActionOnCurrentAsset((currentAsset)->{
			byte pixels[] = ((DataBufferByte)currentAsset.src.getRaster().getDataBuffer()).getData();

			for (int y=0; y<8; y++) {
				for (int x=0; x<4; x++) {
					int offset = mouseSrcChar.x + x + (mouseSrcChar.y + y)*currentAsset.src.getWidth();
					pixels[offset] = (byte)16;
				}
			}
		});
	}

	private void copy() {
		if (mouseOutsideAsset(currentAsset)) return;

		if (mode == MODE_PLOT) 
			EotBGfxEditor.sharedInstance.clipboard = new PixelRect.CharAligned(currentAsset, mousePixel, 4, 8);
		else if (mode == MODE_MARK) {
			Rectangle rect = getMarkRect();
			EotBGfxEditor.sharedInstance.clipboard = new PixelRect(currentAsset, new Point(rect.x,rect.y), rect.width, rect.height);
			mode = MODE_PLOT;
			renderZoom();
			repaint();
		}
	}

	private void renderPaste() {
		PixelRect pixelRect = EotBGfxEditor.sharedInstance.clipboard;
		pasteImage = new BufferedImage(pixelRect.w, pixelRect.h, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)EotBGfxEditor.sharedInstance.wallsetGraphics[level].getColorModel());
		byte pixels[] = ((DataBufferByte)pasteImage.getRaster().getDataBuffer()).getData();
		System.arraycopy(pixelRect.pixels, 0, pixels, 0, pixels.length);		
	}

	private void paste() {
		PixelRect pixelRect = EotBGfxEditor.sharedInstance.clipboard;
		if (pixelRect == null) return;

		if (pixelRect.floating) {
			if (mode == MODE_PLOT) {
				renderPaste();
				markBegin = (Point)mousePixel.clone();
				markEnd = new Point(markBegin.x+pixelRect.w-1, markBegin.y+pixelRect.h-1);
				mode = MODE_PASTE;
				repaint();
			} else if (mode == MODE_PASTE) {
				mode = MODE_PLOT;
				doActionOnCurrentAsset((currentAsset)->{
					pixelRect.paste(currentAsset, markBegin);
					repaint();
				});
			}
		} else {
			doActionOnCurrentAsset((currentAsset)->{
				pixelRect.paste(currentAsset, mousePixel);
			});
		}
	}

	private void replaceColor(boolean replaceGlobally) {
		doActionOnCurrentAsset((currentAsset)->{
			byte pixels[] = ((DataBufferByte)currentAsset.src.getRaster().getDataBuffer()).getData();

			byte color = pixels[mouseSrcPixel.x + mouseSrcPixel.y*currentAsset.src.getWidth()];

			if (replaceGlobally) {
				Asset r = currentAsset.inPixels();

				for (int y=0; y<r.h; y++) {
					for (int x=0; x<r.w; x++) {
						int offset = x + r.sx + (y + r.sy)*r.src.getWidth();
						if (pixels[offset] == color)
							pixels[offset] = (byte)EotBGfxEditor.sharedInstance.selectedColor;
					}
				}
			}
			else {
				for (int y=0; y<8; y++) {
					for (int x=0; x<4; x++) {
						int offset = mouseSrcChar.x + x + (mouseSrcChar.y + y)*currentAsset.src.getWidth();
						if (pixels[offset] == color)
							pixels[offset] = (byte)EotBGfxEditor.sharedInstance.selectedColor;
					}
				}
			}
		});
	}

	private void peekColor() {
		byte pixels[] = ((DataBufferByte)displayArea64.getRaster().getDataBuffer()).getData();
		EotBGfxEditor.sharedInstance.selectedColor = pixels[mousePixel.x + mousePixel.y*displayArea64.getWidth()];
		renderZoom();
		panel.repaint();
	}

	private void color(int colorIndex) {
		if (EotBGfxEditor.sharedInstance.zoomatic.isSelected()) {
			int backupColor = EotBGfxEditor.sharedInstance.selectedColor;
			EotBGfxEditor.sharedInstance.selectedColor = colorIndex;
			doActionOnCurrentAsset((currentAsset)->{plot(false);});
			EotBGfxEditor.sharedInstance.selectedColor = backupColor;
		}
		else
			EotBGfxEditor.sharedInstance.selectedColor = colorIndex;

		renderZoom();
		panel.repaint();
	}

	private void plot(boolean erase) {
		if (mouseOutsideAsset(currentAsset)) return;
		byte pixels[] = ((DataBufferByte)currentAsset.src.getRaster().getDataBuffer()).getData();

		int offset = mouseSrcPixel.x + mouseSrcPixel.y*currentAsset.src.getWidth();

		if (!erase)
			pixels[offset] = (byte)EotBGfxEditor.sharedInstance.selectedColor;
		else {
			pixels[offset] = (byte)16;
		}
	}

	private void drawBoundingBox(Graphics g) {
		Asset r = currentAsset.inPixels().hires();
		g.drawRect(r.dx*EotBGfxEditor.ZOOM-1, r.dy*EotBGfxEditor.ZOOM-1, r.w*EotBGfxEditor.ZOOM+2, r.h*EotBGfxEditor.ZOOM+2);
		g.drawRect(r.dx*EotBGfxEditor.ZOOM-2, r.dy*EotBGfxEditor.ZOOM-2, r.w*EotBGfxEditor.ZOOM+4, r.h*EotBGfxEditor.ZOOM+4);
	}

	private void updateMouse(int x, int y) {
		x-=borderOffset.x;
		y-=borderOffset.y;

		// Position on screen
		mousePosition.setLocation(x,y);

		// Position in c64 area
		x=((int)Math.floor(((float)x)/((float)EotBGfxEditor.ZOOM*2)));
		y=(int)Math.floor(((float)y)/((float)EotBGfxEditor.ZOOM));
		mousePixel.setLocation(x,y);

		// Position in src area
		if (currentAsset!=null) {
			Asset r = currentAsset.inPixels();
			int sx = x-r.dx+r.sx;
			int sy = y-r.dy+r.sy;
			mouseSrcPixel.setLocation(sx,sy);
		}

		// Position in c64 area clamped to nearest char
		x&=~3;
		y&=~7;
		mouseChar.setLocation(x,y);

		// Position in src area clamped to nearest char
		if (currentAsset!=null) {
			Asset r = currentAsset.inPixels();

			x-=r.dx;
			y-=r.dy;
			x+=r.sx;
			y+=r.sy;

			mouseSrcChar.setLocation(x,y);
		}
	}

	public void loadAsset() {
		Asset asset = delegate.getAsset(selectedRow, selectedColumn);
		if (asset!=null) {
			currentAsset = asset;
			render(true);
			renderZoom();
			panel.repaint();
		}
	}

	protected void reloadAsset() {
		if (currentAsset == null) return;
		selectedRow = currentAsset.row;
		selectedColumn = currentAsset.column;
		loadAsset();
	}
}