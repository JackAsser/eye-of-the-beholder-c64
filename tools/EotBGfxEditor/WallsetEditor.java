import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import javax.imageio.*;

public class WallsetEditor extends Editor {
	private static final int MAX_TYPES = 7;
	private static final int MAX_RECTS = 10;

	public WallsetEditor(int level) {
		super(level);

		delegate = new Editor.Delegate() {
			public void reserveAssets() {
				WallsetEditor.super.reserveAssets(MAX_TYPES, MAX_RECTS);
			}

			public void renderContext(BufferedImage dst, Asset asset) {
				int selectedWallType = asset.row;
				int selectedRectangleIndex = asset.column;

				renderWall(dst, 0, 0);
				if ((selectedWallType>0) && (selectedRectangleIndex>0)) {
					// Add context
					if ((selectedRectangleIndex>=4) && (selectedRectangleIndex<=7)) {
                        int t = selectedWallType>2 ? 1 : ((selectedWallType^selectedRectangleIndex)&1)+1;
                        renderWall(dst, t^3, 4);
                        renderWall(dst, t, 5);
                        renderWall(dst, t^3, 6);
                        renderWall(dst, t, 7);
                        renderWall(dst, selectedWallType, selectedRectangleIndex);
					} else if ((selectedRectangleIndex>=1) && (selectedRectangleIndex<=3)) {
						int t = selectedWallType>2 ? 1 : (selectedWallType&1)+1;
						renderWall(dst, t^3, selectedRectangleIndex, -2);
						renderWall(dst, t, selectedRectangleIndex, -1);
						renderWall(dst, selectedWallType, selectedRectangleIndex);
						renderWall(dst, t, selectedRectangleIndex, 1);
						renderWall(dst, t^3, selectedRectangleIndex, 2);
					} else {
						renderWall(dst, selectedWallType, selectedRectangleIndex);
					}
				}

			}

			public void renderAsset(BufferedImage dst, Asset asset) {
				;
			}

			public void renderPC(BufferedImage dst, Asset asset) {
				int selectedWallType = asset.row;
				int selectedRectangleIndex = asset.column;

				Graphics g = dst.getGraphics();
				g.drawImage(EotBGfxEditor.sharedInstance.wallsetPCGraphics[level], 0,0, 22*8,15*8, 0,0, 22*8,15*8, null);
				if ((selectedWallType>0) && (selectedRectangleIndex>0)) {
					Asset r = currentAsset.inPixels().hires();
					g.drawImage(EotBGfxEditor.sharedInstance.wallsetPCGraphics[level],
						r.dx,r.dy, r.dx+r.w,r.dy+r.h,
						r.sx,r.sy, r.sx+r.w,r.sy+r.h,
						null
					);
				}
			}

			public Asset getAsset(int wallType, int rectangleIndex) {
				if (!wallHasRectangle[wallType][rectangleIndex])
					return null;

				int w=22;
				int h=15;
				int dx=0;
				int dy=0;
				int sx=0;
				int sy=0;
				if ((wallType>0) && (rectangleIndex>0)) {
					w = Editor.wallSizes[rectangleIndex][0];
					h = Editor.wallSizes[rectangleIndex][1];
					dx = Editor.wallDstOffset[rectangleIndex][0];
					dy = Editor.wallDstOffset[rectangleIndex][1];
					sx = Editor.wallSrcOffsetX[rectangleIndex];
					sy = 15*wallType;
				}
				return new Asset(EotBGfxEditor.sharedInstance.wallsetGraphics[level],Asset.MODE_PREBLEND,w,h,dx,dy,sx,sy, wallType, rectangleIndex);
			}

			public boolean save() {
				try {
					optimize();
					ImageIO.write(EotBGfxEditor.sharedInstance.wallsetGraphics[level], "PNG", new File(EotBGfxEditor.sharedInstance.filenames[level][3]));
				} catch (IOException e2) {
					e2.printStackTrace();
					return false;
				}
				return true;
			}
		};
	}

	private void optimize() throws IOException {
		BufferedImage tmp = new BufferedImage(22*4, 15*8, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)EotBGfxEditor.sharedInstance.wallsetGraphics[level].getColorModel());
		BufferedImage reference = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStream(level, EotBGfxEditor.RES_WALL64, false));
		byte refPxels[] = ((DataBufferByte)reference.getRaster().getDataBuffer()).getData();

		Asset r = delegate.getAsset(0,0);
		byte dstPixels[] = ((DataBufferByte)r.src.getRaster().getDataBuffer()).getData();
		for (int y=0; y<15*8; y++)
			for (int x=0; x<22*4; x++) {
				int offset = x+y*r.src.getWidth();
				byte c = dstPixels[offset];
				if (c>=16)
					c=0;
				dstPixels[offset]=c;
			}

		for (int wallType=1; wallType<MAX_TYPES; wallType++) {
			for (int rectangleIndex=1; rectangleIndex<MAX_RECTS; rectangleIndex++) {
				r = delegate.getAsset(wallType, rectangleIndex);
				if (r==null)
					continue;

				renderWall(tmp, 0, 0);
				renderWall(tmp, wallType, rectangleIndex);

				byte pixels[] = ((DataBufferByte)tmp.getRaster().getDataBuffer()).getData();
				dstPixels = ((DataBufferByte)r.src.getRaster().getDataBuffer()).getData();

				for (int cy=r.dy; cy<r.dy+r.h; cy++) {
					for (int cx=r.dx; cx<r.dx+r.w; cx++) {
						boolean charShouldBeEmpty = false;
						for (int py=0; py<8; py++) {
							for (int px=0; px<4; px++) {
								int offset = cx*4+px + ((cy*8)+py) * tmp.getWidth();
								int dstOffset = (cx-r.dx+r.sx)*4+px + (((cy-r.dy+r.sy)*8)+py) * r.src.getWidth();

								if (refPxels[dstOffset]>=16)
									charShouldBeEmpty = true;

								if (charShouldBeEmpty)
									dstPixels[dstOffset] = (byte)16;
								else 
									dstPixels[dstOffset] = pixels[offset]>=16 ? 0 : pixels[offset];
							}
						}
					}
				}
			}
		}
	}
}
