import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

abstract public class AbstractItemsEditor extends Editor implements ActionListener {
	protected final static int POSITION_TL = 0;
	protected final static int POSITION_TR = 1;
	protected final static int POSITION_BL = 2;
	protected final static int POSITION_BR = 3;
	protected final static int POSITION_ALCOVE = 4;
	protected final static int POSITION_THROWN_LEFT = 5;
	protected final static int POSITION_THROWN_RIGHT = 6;

	protected int position = POSITION_TL;
	protected JPanel extraUI;

	private void addRadioButtons(String[] labels) {
		ButtonGroup group = new ButtonGroup();
		boolean first = true;
		for (String label : labels) {
			JRadioButton tmp = new JRadioButton(label);
			tmp.setForeground(new Color(90,90,90));
			tmp.setActionCommand(label);
			tmp.addActionListener(this);
			group.add(tmp);
			extraUI.add(tmp);
			if (first) {
				tmp.setSelected(true);
				first = false;
			}
		}
	}

	private void addLabel(String label) {
		JLabel tmp = new JLabel(label);
		tmp.setForeground(Color.BLACK);
		extraUI.add(tmp);
	}

	public AbstractItemsEditor(int level) {
		super(level);

		extraUI = new JPanel();
		extraUI.setBackground(new Color(180,180,180));
		extraUI.setLayout(new FlowLayout(FlowLayout.LEFT));

		addLabel("Position:");
		addRadioButtons(new String[]{"TL", "TR", "BL", "BR", "Alcove", "Thrown L", "Thrown R"});

		extraUI.add(new JSeparator(SwingConstants.HORIZONTAL));

		addLabel("Wallset:");
		addRadioButtons(new String[]{"Brick", "Blue", "Drow", "Green", "Xanatha"});
	}

	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
			case "TL": position = POSITION_TL; break;
			case "TR": position = POSITION_TR; break;
			case "BL": position = POSITION_BL; break;
			case "BR": position = POSITION_BR; break;
			case "Alcove": position = POSITION_ALCOVE; break;
			case "Thrown L": position = POSITION_THROWN_LEFT; break;
			case "Thrown R": position = POSITION_THROWN_RIGHT; break;
			case "Brick": level=0; break;
			case "Blue": level=3; break;
			case "Drow": level=4; break;
			case "Green": level=5; break;
			case "Xanatha": level=6; break;
		}
		redrawAll();
	}
}
