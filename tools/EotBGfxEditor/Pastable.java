import java.awt.*;
import java.awt.image.*;

interface Pastable {
	public void paste(Asset asset, Point p);
}
