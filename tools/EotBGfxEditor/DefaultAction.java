import java.awt.image.*;

public class DefaultAction implements Actions.Action {
	private byte[] before;
	private byte[] after;
	private Asset asset;

	private byte[] grab() {
		byte[] backupPixels = new byte[asset.w* asset.h];
		byte pixels[] = ((DataBufferByte)asset.src.getRaster().getDataBuffer()).getData();
		for (int y=0; y<asset.h; y++)
			for (int x=0; x<asset.w; x++)
				backupPixels[x+y*asset.w] = pixels[asset.sx+x + (asset.sy+y)*asset.src.getWidth()];
		return backupPixels;
	}

	private void ungrab(byte[] backupPixels) {
		byte pixels[] = ((DataBufferByte)asset.src.getRaster().getDataBuffer()).getData();
		for (int y=0; y<asset.h; y++)
			for (int x=0; x<asset.w; x++)
				pixels[asset.sx+x + (asset.sy+y)*asset.src.getWidth()] = backupPixels[x+y*asset.w];
	}

	public DefaultAction(Asset asset) {
		this.asset = asset;
		before = grab();
	}

	public boolean done() {
		after = grab();
		for (int i=0; i<before.length; i++)
			if (before[i]!=after[i])
				return true;
		return false;
	}

	public void perform() {
		ungrab(after);
	}

	public void undo() {
		ungrab(before);
	}
}
