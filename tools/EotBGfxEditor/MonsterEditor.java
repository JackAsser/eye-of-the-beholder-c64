import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import javax.imageio.*;

public class MonsterEditor extends Editor {
	private BufferedImage monsterGraphics;
	private BufferedImage monsterPCGraphics;

	public MonsterEditor(int level, String c64name, String pcname) throws IOException {
		super(level);

		delegate = new Editor.Delegate() {
			public void reserveAssets() throws IOException {
				if (monsterGraphics == null) {
					monsterGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName(c64name));
					monsterGraphics = replacePalette(monsterGraphics, monsterGraphics);
				}

				if (monsterPCGraphics == null) {
					monsterPCGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName(pcname));
				}

				MonsterEditor.super.reserveAssets(3,6);
			}

			public void renderContext(BufferedImage dst, Asset asset) {
				renderWall(dst, 0, 0);
				if (asset.row+2<4) {
					renderWall(dst, 1, asset.row+2);
					renderWall(dst, 2, asset.row+2, -1);
					renderWall(dst, 2, asset.row+2, 1);
					renderWall(dst, 1, asset.row+2, -2);
					renderWall(dst, 1, asset.row+2, 2);
				}
            }

			public void renderAsset(BufferedImage dst, Asset asset) {
				Asset r = asset.inPixels();
				blit(dst, r.dx, r.dy, monsterGraphics, r.sx, r.sy, r.w, r.h);
			}

			public void renderPC(BufferedImage dst, Asset asset) {
				if (asset == null)
					return;

				Graphics g = dst.getGraphics();
				g.drawImage(EotBGfxEditor.sharedInstance.wallsetPCGraphics[level], 0,0, 22*8,15*8, 0,0, 22*8,15*8, null);
				if (asset.row+2<4) {
					renderPCWall(dst, 1, asset.row+2);
					renderPCWall(dst, 2, asset.row+2, -1);
					renderPCWall(dst, 2, asset.row+2, 1);
					renderPCWall(dst, 1, asset.row+2, -2);
					renderPCWall(dst, 1, asset.row+2, 2);
				}

				Asset r = asset.inPixels().hires();
				g.drawImage(monsterPCGraphics,
					r.dx, r.dy, r.dx+r.w, r.dy+r.h,
					r.sx, r.sy, r.sx+r.w, r.sy+r.h,
					null);
			}

			public Asset getAsset(int zoom, int pos) {
				int objy[] = new int[]{0,3,5};
				int w=12;
				int h=12;
				int sx=w*pos;
				int sy=w*zoom;
				int dx=11-w/2;
				int dy=14-h-objy[zoom];
				return new Asset(monsterGraphics,Asset.MODE_OVERLAY,w,h,dx,dy,sx,sy, zoom, pos);
			}

			public boolean save() {
				try {
					ImageIO.write(monsterGraphics, "PNG", new File(c64name));
				} catch (IOException e2) {
					e2.printStackTrace();
					return false;
				}
				return true;
			}
		};
	}
}
