import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

public class EotBGfxEditor extends JFrame {
	public static final int ZOOM=5;	// Main window zoom
	public static final int ZOOMS=12; // Mouse zoom
	public static final int ZOOMW=5; // Characters wide
	public static final int ZOOMH=5; // Characters hight
	public static EotBGfxEditor sharedInstance;

	public static final int RES_PRECONV = 0;
	public static final int RES_DEC64 = 1;
	public static final int RES_DECPC = 2;
	public static final int RES_WALL64 = 3;
	public static final int RES_WALLPC = 4;
	public static final int RES_DOOR1 = 5;
	public static final int RES_DOOR2 = 6;

	public String[][] filenames;
	public BufferedImage wallsetGraphics[];
	public BufferedImage wallsetPCGraphics[];
	public int selectedColor = 4;
	public JCheckBox zoomatic;

	public PixelRect clipboard = null;
	private JTabbedPane mainTabbar;

	public void saveStateChanged() {
		for (int i=0; i<mainTabbar.getTabCount(); i++) {
			boolean toptabNeedsSave = false;

			if (mainTabbar.getComponentAt(i) instanceof JTabbedPane) {
				JTabbedPane toptab = (JTabbedPane)mainTabbar.getComponentAt(i);

				for (int j=0; j<toptab.getTabCount(); j++) {
					String title = toptab.getTitleAt(j).replace(" *", "");
					Editor editor = (Editor)toptab.getComponentAt(j);

					if (editor.saveNeeded()) {
						title+=" *";
						toptabNeedsSave = true;
					}
					toptab.setTitleAt(j, title);
				}
			} else if (mainTabbar.getComponentAt(i) instanceof Editor) {
				Editor editor = (Editor)mainTabbar.getComponentAt(i);
				if (editor.saveNeeded())
					toptabNeedsSave = true;
			}

			String title = mainTabbar.getTitleAt(i).replace(" *", "");
			if (toptabNeedsSave)
				title+=" *";
			mainTabbar.setTitleAt(i, title);
		}
	}

	public InputStream getResourceAsStreamByName(String name, boolean tryLocalFirst) throws IOException {
		File localFile = new File(name);
		if (localFile.exists() && tryLocalFirst)
			return new FileInputStream(localFile);
		return getClass().getResourceAsStream("assets/"+name);
	}

	public InputStream getResourceAsStream(int level, int resourceIndex, boolean tryLocalFirst) throws IOException {
		return getResourceAsStreamByName(filenames[level][resourceIndex], tryLocalFirst);
	}

	public InputStream getResourceAsStreamByName(String name) throws IOException {
		return getResourceAsStreamByName(name, true);
	}

	public InputStream getResourceAsStream(int level, int resourceIndex) throws IOException {
		return getResourceAsStream(level, resourceIndex, true);
	}

	private void setTheme() {
		UIManager.put( "background", new Color(0,0,0) );
		UIManager.put( "control", new Color( 80,80,80) );
		UIManager.put( "info", new Color(128,128,128) );
		UIManager.put( "nimbusBase", new Color( 18, 30, 49) );
		UIManager.put( "nimbusAlertYellow", new Color( 248, 187, 0) );
		UIManager.put( "nimbusDisabledText", new Color( 128, 128, 128) );
		UIManager.put( "nimbusFocus", new Color(115,164,209) );
		UIManager.put( "nimbusGreen", new Color(176,179,50) );
		UIManager.put( "nimbusInfoBlue", new Color( 66, 139, 221) );
		UIManager.put( "nimbusLightBackground", new Color( 18, 30, 49) );
		UIManager.put( "nimbusOrange", new Color(191,98,4) );
		UIManager.put( "nimbusRed", new Color(169,46,34) );
		UIManager.put( "nimbusSelectedText", new Color( 255, 255, 255) );
		UIManager.put( "nimbusSelectionBackground", new Color( 104, 93, 156) );
		UIManager.put( "text", new Color( 230, 230, 230) );
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
				  	break;
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (javax.swing.UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void newEditorSelected() {
		try {
			if (mainTabbar.getSelectedComponent() instanceof JTabbedPane) {
				JTabbedPane tabbar = (JTabbedPane)mainTabbar.getSelectedComponent();
				if (tabbar!=null) {
					Editor editor = (Editor)tabbar.getSelectedComponent();
					if (editor!=null) {
						editor.delegate.reserveAssets();
						editor.loadAsset();
					}
				}
			} else if (mainTabbar.getSelectedComponent() instanceof Editor) {
				Editor editor = (Editor)mainTabbar.getSelectedComponent();
				editor.delegate.reserveAssets();
				editor.loadAsset();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public EotBGfxEditor() throws IOException, ClassNotFoundException {
		super("Eye of the Pixel");

		setTheme();
		setLayout(new BorderLayout());
		EotBGfxEditor.sharedInstance = this;

		JPanel bottom = new JPanel();
			bottom.setLayout(new BorderLayout());

			JPanel statusPane = new JPanel();
				statusPane.setBackground(new Color(180,180,180));
				statusPane.setLayout(new FlowLayout(FlowLayout.LEFT));

				zoomatic = new JCheckBox("Zoomatic");
					zoomatic.setForeground(new Color(90,90,90));
				statusPane.add(zoomatic);

				JCheckBox floatingZoom = new JCheckBox("Floating zoom");
					floatingZoom.setForeground(new Color(90,90,90));
				statusPane.add(floatingZoom);
			bottom.add(statusPane, BorderLayout.SOUTH);

			JTextArea help = new JTextArea(
				"Arrows - Select gfx and perspective. (Left panel also selects, red=color clash).\n"+
				"0-F - Select color. LMB=Put pixel. RMB=Erase pixel. Red outline=color clash. Green outline=boundary.\n"+
				"x - Clear char. CMD+Z - undo. CMD+SHIFT+Z - redo. Space - Hide zoomer. Tab - See PC. m - toggle CRT emu.\n"+
				"SHIFT+Arrows - Shrink boundary - CMD+SHIFT+Arrows - Expand boundary.\n"+
				"CMD+S - Save. CMD+C - Copy char. CMD+V - Paste char. CMD+SHIFT+C - Copy whole wall. CMD+SHIFT+P - Paste whole wall.");
				help.setEditable(false);
				help.setFocusable(false);
		//	bottom.add(help, BorderLayout.CENTER);
		add(bottom, BorderLayout.SOUTH);

		filenames = new String[][] {
			{"brick1_decorations.preconv", "brick1_decorations64.png", "brick1_decorations_context.png", "brick64.png", "brick.png"},
			{"brick2_decorations.preconv", "brick2_decorations64.png", "brick2_decorations_context.png", "brick64.png", "brick.png"},
			{"brick3_decorations.preconv", "brick3_decorations64.png", "brick3_decorations_context.png", "brick64.png", "brick.png"},
			{"blue_decorations.preconv", "blue_decorations64.png", "blue_decorations_context.png", "blue64.png", "blue.png"},
			{"drow_decorations.preconv", "drow_decorations64.png", "drow_decorations_context.png", "drow64.png", "drow.png"},
			{"green_decorations.preconv", "green_decorations64.png", "green_decorations_context.png", "green64.png", "green.png"},
			{"xanatha_decorations.preconv", "xanatha_decorations64.png", "xanatha_decorations_context.png", "xanatha64.png", "xanatha.png"}
		};

		String monsters[][] = {
			{"0", "Kobold", "kobold.png", "koboldPC.png"},
			{"0", "Leech", "leech.png", "leechPC.png"},
			{"1", "Zombie", "zombie.png", "zombiePC.png"},
			{"1", "Skeleton", "skeleton.png", "skeletonPC.png"},
			{"2", "Kuotoa", "kuotoa.png", "kuotoaPC.png"},
			{"2", "Flind", "flind.png", "flindPC.png"},
			{"3", "Spider", "spider.png", "spiderPC.png"},
			{"3", "Dwarf", "dwarf.png", "dwarfPC.png"},
			{"3", "Kenku", "kenku.png", "kenkuPC.png"},
			{"3", "Mage", "mage.png", "magePC.png"},
			{"4", "Drowelf", "drowelf.png", "drowelfPC.png"},
			{"4", "Skelwar", "skelwar.png", "skelwarPC.png"},
			{"4", "Drider", "drider.png", "driderPC.png"},
			{"4", "Hellhnd", "hellhnd.png", "hellhndPC.png"},
			{"4", "Rust", "rust.png", "rustPC.png"},
			{"4", "Disbeast", "disbeast.png", "disbeastPC.png"},
			{"5", "Shindia", "shindia.png", "shindiaPC.png"},
			{"5", "Mantis", "mantis.png", "mantisPC.png"},
			{"5", "Xorn", "xorn.png", "xornPC.png"},
			{"5", "Mflayer", "mflayer.png", "mflayerPC.png"},
			{"6", "Golem", "golem.png", "golemPC.png"},
			{"6", "Xanath", "xanath.png", "xanathPC.png"},
		};

		wallsetGraphics = new BufferedImage[filenames.length];
		wallsetPCGraphics = new BufferedImage[filenames.length];

		for (int i=0; i<filenames.length; i++) {
			if ((i==1)||(i==2)) {
				wallsetGraphics[i] = wallsetGraphics[0];
				wallsetPCGraphics[i] = wallsetPCGraphics[0];
			}
			else {
				wallsetGraphics[i] = ImageIO.read(getResourceAsStream(i, RES_WALL64));
				wallsetGraphics[i] = Editor.replacePalette(wallsetGraphics[i], wallsetGraphics[i]);
				wallsetPCGraphics[i] = ImageIO.read(getResourceAsStream(i, RES_WALLPC));
			}
		}

		ChangeListener cl = new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				newEditorSelected();
			}
		};

		mainTabbar = new JTabbedPane();
		mainTabbar.addChangeListener(cl);

		JTabbedPane tabbar;

		tabbar = new JTabbedPane();
		tabbar.addChangeListener(cl);
		tabbar.addTab("Wallsets", new WallsetEditor(0));
		tabbar.addTab("Decorations 1", new DecorationEditor(0));
		tabbar.addTab("Decorations 2", new DecorationEditor(1));
		tabbar.addTab("Decorations 3", new DecorationEditor(2));
		tabbar.addTab("Door 0", new DoorEditor(0, "brick0_doors64.png", "brick0_doors.png", "buttons64.png"));
		tabbar.addTab("Door 1", new DoorEditor(0, "brick1_doors64.png", "brick1_doors.png", "buttons64.png"));
		mainTabbar.addTab("Brick", tabbar);

		tabbar = new JTabbedPane();
		tabbar.addChangeListener(cl);
		tabbar.addTab("Wallsets", new WallsetEditor(3));
		tabbar.addTab("Decorations", new DecorationEditor(3));
		tabbar.addTab("Door", new DoorEditor(3, "blue_doors64.png", "blue_doors.png", "buttons64.png"));
		mainTabbar.addTab("Blue", tabbar);

		tabbar = new JTabbedPane();
		tabbar.addChangeListener(cl);
		tabbar.addTab("Wallsets", new WallsetEditor(4));
		tabbar.addTab("Decorations", new DecorationEditor(4));
		tabbar.addTab("Door", new DoorEditor(4, "drow_doors64.png", "drow_doors.png", "buttons64.png"));
		mainTabbar.addTab("Drow", tabbar);

		tabbar = new JTabbedPane();
		tabbar.addChangeListener(cl);
		tabbar.addTab("Wallsets", new WallsetEditor(5));
		tabbar.addTab("Decorations", new DecorationEditor(5));
		tabbar.addTab("Door", new DoorEditor(5, "green_doors64.png", "green_doors.png", "buttons64.png"));
		mainTabbar.addTab("Green", tabbar);

		tabbar = new JTabbedPane();
		tabbar.addChangeListener(cl);
		tabbar.addTab("Wallsets", new WallsetEditor(6));
		tabbar.addTab("Decorations", new DecorationEditor(6));
		tabbar.addTab("Door", new DoorEditor(6, "xanatha_doors64.png", "xanatha_doors.png", "buttons64.png"));
		mainTabbar.addTab("Xanathar", tabbar);

		tabbar = new JTabbedPane();
		tabbar.addChangeListener(cl);
		for (int i=0; i<monsters.length; i++)
			tabbar.addTab(monsters[i][1], new MonsterEditor(Integer.parseInt(monsters[i][0]), monsters[i][2], monsters[i][3]));
		mainTabbar.addTab("Monsters", tabbar);

		mainTabbar.addTab("NPCs", new OuttakeEditor());

		tabbar = new JTabbedPane();
		tabbar.addChangeListener(cl);
		tabbar.addTab("Large", new LargeItemsEditor());
		tabbar.addTab("Small", new LargeItemsEditor());
		tabbar.addTab("Thrown", new LargeItemsEditor());
		mainTabbar.addTab("Items", tabbar);

		add(mainTabbar, BorderLayout.CENTER);

		setResizable(true);
//	    setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);

		setVisible(true);
		toFront();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		

		tabbar = (JTabbedPane)mainTabbar.getSelectedComponent();
		tabbar.getSelectedComponent().requestFocus();
		newEditorSelected();

		setSize(new Dimension(1280,760));
	}	

	public static void main(String[] args) {
		try {
			new EotBGfxEditor();
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}