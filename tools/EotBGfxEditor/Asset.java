import java.awt.image.*;

public class Asset {
	public static final int MODE_PREBLEND = 0;
	public static final int MODE_OVERLAY = 1;

	public int mode;
	public BufferedImage src;
	public int w,h,dx,dy,sx,sy;
	public int row,column;

	public Asset(BufferedImage src, int mode, int w, int h, int dx, int dy, int sx, int sy, int row, int column) {
		this.src = src;
		this.mode = mode;
		this.w = w;
		this.h = h;
		this.dx = dx;
		this.dy = dy;
		this.sx = sx;
		this.sy = sy;
		this.row = row;
		this.column = column;
	}

	public Asset inPixels() {
		return new Asset(src,mode,w*4,h*8,dx*4,dy*8,sx*4,sy*8,row,column);
	}

	public Asset hires() {
		return new Asset(src,mode,w*2,h,dx*2,dy,sx*2,sy,row,column);
	}
}
