import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import javax.imageio.*;

public class OuttakeEditor extends Editor {
	private BufferedImage dwarfsGraphics;
	private BufferedImage resurrectedGraphics;
	private BufferedImage shindaGraphics;
	private BufferedImage drowGraphics;
	private BufferedImage mageGraphics;

	public OuttakeEditor() throws IOException {
		super(1);

		delegate = new Editor.Delegate() {
			public void reserveAssets() throws IOException {
				if (dwarfsGraphics == null) {
					dwarfsGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName("dwarfs_all.png"));
					dwarfsGraphics = replacePalette(dwarfsGraphics, dwarfsGraphics);
					resurrectedGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName("resurrected.png"));
					resurrectedGraphics = replacePalette(resurrectedGraphics, resurrectedGraphics);
					shindaGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName("outtake_6_64.png"));
					shindaGraphics = replacePalette(shindaGraphics, shindaGraphics);
					drowGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName("outtake_7_64.png"));
					drowGraphics = replacePalette(drowGraphics, drowGraphics);
				}

				OuttakeEditor.super.reserveAssets(6, 10);
			}

			public void renderAsset(BufferedImage dst, Asset asset) {
				if (asset == null)
					return;

				blit(dst, asset);
			}

			public void renderContext(BufferedImage dst, Asset asset) {
				int[] bodyID = {0x1,0x1,0x2,0x2,0x2,0x3};

				switch (asset.row) {
					case 0:
						level=3;
						renderWall(dst, 0, 0, true);
						renderWall(dst, 2, 1, -1);
						renderWall(dst, 2, 1, 1);
						renderWall(dst, 2, 2);
						renderWall(dst, 2, 5, 0);
						renderWall(dst, 1, 5, 0,true);
						break;
					case 1:
						level=3;
						renderWall(dst, 0, 0, true);
						renderWall(dst, 1, 2, 0);
						renderWall(dst, 2, 2, 1);
						if (asset.column==1)
							blit(dst, getAsset(1,0), -3, 0);
						break;
					case 2:
						level=3;
						renderWall(dst, 0, 0, true);
						renderWall(dst, 2, 1, -1);
						renderWall(dst, 1, 5, 0);
						renderWall(dst, 1, 2);
						renderWall(dst, 2, 5, 0, true);
						renderWall(dst, 1, 4, 0, true);
						if (asset.column>0)
							blit(dst, getAsset(2,0), -3, 0);
						if (asset.column>=4)
							blit(dst, getAsset(2, bodyID[asset.column-4]));
						break;
					case 3:
						level=4;
						renderWall(dst, 0, 0, true);
						renderWall(dst, 2, 4);
						renderWall(dst, 1, 5);
						renderWall(dst, 2, 6);
						renderWall(dst, 1, 2, 1);
						renderWall(dst, 1, 4, true);
						renderWall(dst, 1, 6, true);
						break;
					case 4:
						level=5;
						renderWall(dst, 0, 0, true);
						renderWall(dst, 1, 4);
						renderWall(dst, 2, 4, true);
						break;
					case 5:
						level=5;
						renderWall(dst, 0, 0);
						renderWall(dst, 2, 1, -1);
						renderWall(dst, 2, 5);
						renderWall(dst, 1, 2);
						renderWall(dst, 1, 5, true);
						renderWall(dst, 2, 4, true);
						break;
				}
            }

			public Asset getAsset(int zoom, int pos) {
				switch (zoom) {
					case 0:
						// Drawf at level 4
						if (pos==0)
							return new Asset(
								dwarfsGraphics,
								Asset.MODE_OVERLAY,
								6,6,
								8,6,
								12,0,
								zoom, pos);						
						break;
					case 1:
						// Dwarven leader and king at level 5
						if (pos==0)
							return new Asset(
								dwarfsGraphics,
								Asset.MODE_OVERLAY,
								6,8,
								8,4,
								0,1,
								zoom, pos);
						else if (pos==1)
							return new Asset(
								dwarfsGraphics,
								Asset.MODE_OVERLAY,
								5,8,
								12,4,
								28,0,
								zoom, pos);
						break;
					case 2:
						// Dwarven cleric at level 5
						if (pos==0)
							return new Asset(
								dwarfsGraphics,
								Asset.MODE_OVERLAY,
								5,7,
								8,5,
								18,0,
								zoom, pos);

						// Dwarven cleric at level 5, male dresss
						else if (pos==1)
							return new Asset(
								resurrectedGraphics,
								Asset.MODE_OVERLAY,
								5,8,
								11,4,
								1,2,
								zoom, pos);
						// Dwarven cleric at level 5, female dresss
						else if (pos==2)
							return new Asset(
								resurrectedGraphics,
								Asset.MODE_OVERLAY,
								5,8,
								11,4,
								9,2,
								zoom, pos);
						// Dwarven cleric at level 5, dwarf dresss
						else if (pos==3)
							return new Asset(
								resurrectedGraphics,
								Asset.MODE_OVERLAY,
								5,8,
								11,4,
								16,2,
								zoom, pos);

						// Dwarven cleric at level 5, face 1
						else if (pos==4)
							return new Asset(
								resurrectedGraphics,
								Asset.MODE_OVERLAY,
								3,4,
								12,2,
								23,1,
								zoom, pos);
						// Dwarven cleric at level 5, face 2
						else if (pos==5)
							return new Asset(
								resurrectedGraphics,
								Asset.MODE_OVERLAY,
								3,4,
								12,2,
								27,1,
								zoom, pos);
						// Dwarven cleric at level 5, face 3
						else if (pos==6)
							return new Asset(
								resurrectedGraphics,
								Asset.MODE_OVERLAY,
								3,4,
								12,2,
								23,6,
								zoom, pos);
						// Dwarven cleric at level 5, face 4
						else if (pos==7)
							return new Asset(
								resurrectedGraphics,
								Asset.MODE_OVERLAY,
								3,3,
								12,3,
								27,7,
								zoom, pos);
						// Dwarven cleric at level 5, face 5
						else if (pos==8)
							return new Asset(
								resurrectedGraphics,
								Asset.MODE_OVERLAY,
								3,3,
								12,3,
								31,7,
								zoom, pos);
						// Dwarven cleric at level 5, face 6
						else if (pos==9)
							return new Asset(
								resurrectedGraphics,
								Asset.MODE_OVERLAY,
								3,3,
								12,5,
								31,2,
								zoom, pos);
						break;
					case 3:
						// Drow warriors at level 7
						if (pos==0)
							return new Asset(
								drowGraphics,
								Asset.MODE_OVERLAY,
								6,11,
								8,2,
								0,0,
								zoom, pos);
						break;
					case 4:
						// Shindia at level 10
						if (pos==0)
							return new Asset(
								shindaGraphics,
								Asset.MODE_OVERLAY,
								6,9,
								8,4,
								0,0,
								zoom, pos);
						break;
/*					case 5:
						// Xanathar encounter
						break;
					case 6:
						// Mage at level 6
						break;*/
					case 5:
						// Dwarven prince at level 10
						if (pos==0)
							return new Asset(
								dwarfsGraphics,
								Asset.MODE_OVERLAY,
								5,7,
								8,6,
								23,0,
								zoom, pos);
						break;
				}
				return null;
			}

			public boolean save() {
				try {
					ImageIO.write(dwarfsGraphics, "PNG", new File("dwarfs_all.png"));
					ImageIO.write(resurrectedGraphics, "PNG", new File("resurrected.png"));
					ImageIO.write(shindaGraphics, "PNG", new File("outtake_6_64.png"));
					ImageIO.write(drowGraphics, "PNG", new File("outtake_7_64.png"));
				} catch (IOException e2) {
					e2.printStackTrace();
					return false;
				}
				return true;
			}
		};
	}
}
