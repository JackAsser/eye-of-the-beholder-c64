import java.awt.*;
import java.awt.image.*;

public class PixelRect implements Pastable {
	int w;
	int h;
	byte pixels[];
	boolean floating;

	public PixelRect(Asset asset, Point p, int w, int h) {
		this.w = w;
		this.h = h;
		pixels = new byte[w*h];
		floating = true;

		asset = asset.inPixels();
		byte srcPixels[] = ((DataBufferByte)asset.src.getRaster().getDataBuffer()).getData();

		for (int y=0; y<h; y++)
			for (int x=0; x<w; x++) {
				int ax = p.x-asset.dx+x;
				int ay = p.y-asset.dy+y;
				byte c;
				if ((ax<0)||(ay<0)||(ax>=asset.w)||(ay>=asset.h))
					c = (byte)16;
				else {
					int sx = ax+asset.sx;
					int sy = ay+asset.sy;
					c = srcPixels[sx + sy*asset.src.getWidth()];
				}
				pixels[x+y*w] = c;
			}

	}

	public void paste(Asset asset, Point p) {
		asset = asset.inPixels();
		byte dstPixels[] = ((DataBufferByte)asset.src.getRaster().getDataBuffer()).getData();

		for (int y=0; y<h; y++)
			for (int x=0; x<w; x++) {
				int ax = p.x-asset.dx+x;
				int ay = p.y-asset.dy+y;
				if ((ax<0)||(ay<0)||(ax>=asset.w)||(ay>=asset.h))
					continue;
				int sx = ax+asset.sx;
				int sy = ay+asset.sy;
				dstPixels[sx + sy*asset.src.getWidth()] = pixels[x+y*w];
			}
	}

	public void xflip() {
		byte dst[] = new byte[w*h];
		for (int y=0; y<h; y++)
			for (int x=0; x<w; x++)
				dst[x+y*w] = pixels[(w-1-x)+y*w];
		pixels=dst;
	}

	static class CharAligned extends PixelRect {
		public CharAligned(Asset asset, Point p, int w, int h) {
			super(asset, new Point(p.x&~3, p.y&~7), w, h);
			floating = false;
		}

		@Override
		public void paste(Asset asset, Point p) {
			super.paste(asset, new Point(p.x&~3, p.y&~7));
		}
	}
}
