import java.io.*;

public class PrerenderedDecoration implements Serializable {
	public static final long serialVersionUID = -560266420679079161L;

	public transient byte[] pixels;
	public int flags;
	public int cx[],cy[],cw[],ch[],dx[];
	public int wallType;
	public transient boolean renderAsOverlay;

	public PrerenderedDecoration() {
		pixels = new byte[22*8*10*15*8];
		for (int i=0; i<pixels.length; i++)
			pixels[i] = -1;
		flags=0;
		cx = new int[10];
		cy = new int[10];
		cw = new int[10];
		ch = new int[10];
		dx = new int[10]; // Only used to compensate for the hacked x-coord of the cave-in on level1
	}

	public boolean equals(Object o) {
		if (o==null)
			return false;
		if (!(o instanceof PrerenderedDecoration))
			return false;
		PrerenderedDecoration other = (PrerenderedDecoration)o;
		for (int i=0; i<pixels.length; i++) {
			if (pixels[i]!=other.pixels[i])
				return false;
		}
		return true;
	}

	public long hashcode() {
		return 0;
	}
}
