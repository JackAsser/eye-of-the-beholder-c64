import java.util.*;

public class Actions {
	public interface Action {
		public void perform();
		public void undo();
		public boolean done();
	}

	private Stack<Action> undoStack = new Stack<>();
	private Stack<Action> redoStack = new Stack<>();

	public boolean add(Action action) {
		if (action.done()) {
			undoStack.push(action);
			redoStack.clear();
			return true;
		}
		return false;
	}

	public boolean canUndo() {
		return undoStack.size()>0;
	}

	public boolean canRedo() {
		return redoStack.size()>0;
	}

	public boolean undo() {
		if (!canUndo())
			return false;

		Action action = undoStack.pop();
		redoStack.push(action);
		action.undo();

		return true;
	}

	public boolean redo() {
		if (!canRedo())
			return false;

		Action action = redoStack.pop();
		undoStack.push(action);
		action.perform();

		return true;
	}
}
