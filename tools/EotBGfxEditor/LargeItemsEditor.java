import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import javax.imageio.*;

public class LargeItemsEditor extends AbstractItemsEditor {
	private BufferedImage largeItemsGraphics;

	private static int objectZoomPositionDeltasX[][] = new int[][] {
		{-8,    8,   -10,   10,   0},
		{-13,   13,  -16,   16,   0},
		{-22,   22,  -27,   27,   0},
		{-38,   38,  -38,   38,   0}
	};

	private static int objectZoomPositionDeltasY[][] = new int[][] {
		{-63, -63, -59, -59, -61},
		{-53, -53, -45, -45, -50},
		{-35, -35, -22, -22, -30},
		{-4,  -4,  -66, -66,  0}
	};

	private static int objectSubPosZoom[][] = new int[][] {
		{-1,-1,-1,-1},
		{ 2, 2, 2, 2},
		{ 1, 1, 1, 1},
		{ 0, 0,-1,-1}
	};

	private static int itemSizes[][] = {
		{8,3},
		{6,2},
		{4,2},
		{2,1}
	};

	private static int itemLocations[][] = {
		{0,0},
		{0,8},
		{0,16},
		{8,0},
		{8,8},
		{8,16},
		{16,0},
		{16,8},
		{16,16},
		{24,0},
		{24,8},
		{24,16},
		{32,0},
		{32,8},
		{32,16},
	};

	private static int itemDeltaYZoomStep[] = {0,3,5,7};

	public LargeItemsEditor() throws IOException {
		super(1);

		delegate = new Editor.Delegate() {
			public JPanel getExtraUI() {
				return extraUI;
			}

			public void reserveAssets() throws IOException {
				if (largeItemsGraphics == null) {
					largeItemsGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName("iteml164.png"));
					largeItemsGraphics = replacePalette(largeItemsGraphics, largeItemsGraphics);
				}

				LargeItemsEditor.super.reserveAssets(14, 3);
			}

			public void renderAsset(BufferedImage dst, Asset asset) {
				if (asset == null)
					return;

				blit(dst, asset);
			}

			public void renderContext(BufferedImage dst, Asset asset) {
				renderWall(dst, 0, 0, true);
				renderWall(dst, 2, 1, -1);
				renderWall(dst, 2, 1, 1);
				renderWall(dst, 2, 2);
				renderWall(dst, 2, 5, 0);
				renderWall(dst, 1, 5, 0,true);
            }

            private int round2chars(int pos) {
            	return (int)Math.round(((double)pos)/8.0);
            }

			public Asset getAsset(int row, int col) {
				int dx = 0;
				int dy = 0;
				int zoomIndex = 0;

				if (position <= 3) {
					zoomIndex = objectSubPosZoom[2-col+1][position];
					if (zoomIndex == -1) {
						dx = 100;
						dy = 100;
					}
					else {
						dx = round2chars(objectZoomPositionDeltasX[2-col][position])+11;
						dy = round2chars(objectZoomPositionDeltasY[2-col][position]+120);
					}
				}

				if (zoomIndex == -1)
					zoomIndex = 0;

				if (position <= 3) {
					int w = itemSizes[zoomIndex][0];
					int h = itemSizes[zoomIndex][1];
					dx -= (w+1)/2;
//					dy -= h;

					return new Asset(
						largeItemsGraphics,
						Asset.MODE_OVERLAY,
						w, h,
						dx, dy,
						itemLocations[row][0], itemLocations[row][1]+itemDeltaYZoomStep[zoomIndex],
						row, col);
				}

				return null;
			}

			public boolean save() {
				try {
					ImageIO.write(largeItemsGraphics, "PNG", new File("iteml164.png"));
				} catch (IOException e2) {
					e2.printStackTrace();
					return false;
				}
				return true;
			}
		};
	}
}
