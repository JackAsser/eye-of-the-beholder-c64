import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import javax.imageio.*;

public class DoorEditor extends Editor implements ActionListener {
	private static final int EDIT_DOOR = 0;
	private static final int EDIT_BUTTON = 1;
	private BufferedImage doorGraphics;
	private BufferedImage doorPCGraphics;
	private BufferedImage buttonGraphics;

	protected JPanel extraUI;
	private int editMode = EDIT_DOOR;

	private void addRadioButtons(String[] labels) {
		ButtonGroup group = new ButtonGroup();
		boolean first = true;
		for (String label : labels) {
			JRadioButton tmp = new JRadioButton(label);
			tmp.setForeground(new Color(90,90,90));
			tmp.setActionCommand(label);
			tmp.addActionListener(this);
			group.add(tmp);
			extraUI.add(tmp);
			if (first) {
				tmp.setSelected(true);
				first = false;
			}
		}
	}

	private void addLabel(String label) {
		JLabel tmp = new JLabel(label);
		tmp.setForeground(Color.BLACK);
		extraUI.add(tmp);
	}

	public DoorEditor(int level, String c64name, String pcname, String buttonsC64name) throws IOException {
		super(level);

		extraUI = new JPanel();
		extraUI.setBackground(new Color(180,180,180));
		extraUI.setLayout(new FlowLayout(FlowLayout.LEFT));

		addLabel("Edit:");
		addRadioButtons(new String[]{"Door", "Button"});

		delegate = new Editor.Delegate() {
			public JPanel getExtraUI() {
				return extraUI;
			}

			public void reserveAssets() throws IOException {
				if (doorGraphics == null) {
					doorGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName(c64name));
					doorGraphics = replacePalette(doorGraphics, doorGraphics);
					doorPCGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName(pcname));					
				}

				if (buttonGraphics == null) {
					buttonGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStreamByName(buttonsC64name));
					buttonGraphics = replacePalette(buttonGraphics, buttonGraphics);
				}

				DoorEditor.super.reserveAssets(3, 6);
			}

			public void renderAsset(BufferedImage dst, Asset asset) {
				if (asset == null)
					return;

				blit(dst, asset);
			}

			public void renderContext(BufferedImage dst, Asset asset) {
				renderWall(dst, 0, 0);
				renderWall(dst, 1, asset.row+1, -1);
				renderWall(dst, 1, asset.row+1, 1);
				renderWall(dst, 2, asset.row+1, -2);
				renderWall(dst, 2, asset.row+1, 2);
				renderWall(dst, 3, asset.row+1);

				if (editMode == EDIT_BUTTON) {
					Asset door = getDoorAsset(asset.row, asset.column);
					blit(dst, door);
				}
			}

			public void renderPC(BufferedImage dst, Asset asset) {
				Graphics g = dst.getGraphics();
				g.drawImage(EotBGfxEditor.sharedInstance.wallsetPCGraphics[level], 0,0, 22*8,15*8, 0,0, 22*8,15*8, null);

				Asset r = asset.inPixels().hires();
				g.drawImage(doorPCGraphics,
					r.dx, r.dy, r.dx+r.w, r.dy+r.h,
					r.sx, r.sy, r.sx+r.w, r.sy+r.h,
					null);
			}

			private Asset getDoorAsset(int zoom, int pos) {
				int coords[][] = new int[][] { // zoom
					// w,h,sy,dx,dy
					{12,11, 0, 5,1},
					{9,6, 11, 7,3},
					{6,5, 17, 8,3}
				};

				int w=coords[zoom][0];
				int h=coords[zoom][1];
				int sx=coords[zoom][0]*pos;
				int sy=coords[zoom][2];
				int dx=coords[zoom][3];
				int dy=coords[zoom][4];

				return new Asset(doorGraphics,Asset.MODE_OVERLAY,w,h,dx,dy,sx,sy, zoom, pos);
			}

			private Asset getButtonAsset(int zoom, int pos) {
				if (zoom>1)
					return null;

				int blueCoords[][][] = new int[][][] { // pos, zoom
					// w,h,sx,sy,dx,dy
					{
						{2,2, 0,8,11,6},
						{1,1, 0,6,11,6}
					},
					{
						{2,2, 4,8,12,6},
						{1,1, 4,6,12,6}
					},
					{
						{2,2, 8,8,13,6},
						{1,1, 8,6,12,6}
					},
					{
						{2,2,12,8,14,6},
						{1,1,12,6,13,6}
					},
					{
						{2,2,16,8,15,6},
						{1,1,16,6,14,6}
					},
					{
						{2,2,20,8,12,6},
						{1,1,20,6,11,6}
					}
				};

				int w=0;
				int h=0;
				int sx=0;
				int sy=0;
				int dx=0;
				int dy=0;

				switch (level) {
				case 0: // lvl 1..3
				case 1:
				case 2:
					if (zoom==0) {
						w=2; h=2; sx=0; sy=3; dx=16; dy=4; break;
					} else if (zoom==1) {
						w=1; h=2; sx=0; sy=0; dx=14; dy=4; break;
					}
					break;
				case 3: // lvl 4..6
					w = blueCoords[pos][zoom][0];
					h = blueCoords[pos][zoom][1];
					sx = blueCoords[pos][zoom][2];
					sy = blueCoords[pos][zoom][3];
					dx = blueCoords[pos][zoom][4];
					dy = blueCoords[pos][zoom][5];
					break;
				case 4: // lvl 7..9
					if (zoom==0) {
						w=2; h=3; sx=0; sy=14; dx=17; dy=4; break;
					} else if (zoom==1) {
						w=1; h=2; sx=0; sy=11; dx=14; dy=5; break;
					}
					break;
				case 5: // lvl 10..11
					if (zoom==0) {
						w=2; h=2; sx=0; sy=21; dx=15; dy=4; break;
					} else if (zoom==1) {
						w=1; h=2; sx=0; sy=18; dx=14; dy=4; break;
					}
					break;
				case 6: // lvl 12
					if (zoom==0) {
						w=1; h=2; sx=0; sy=26; dx=16; dy=5; break;
					} else if (zoom==1) {
						w=1; h=1; sx=0; sy=24; dx=14; dy=5; break;
					}
					break;
				}

				return new Asset(buttonGraphics,Asset.MODE_OVERLAY,w,h,dx,dy,sx,sy, zoom, pos);
			}

			public Asset getAsset(int zoom, int pos) {
				return editMode == EDIT_DOOR ? getDoorAsset(zoom, pos) : getButtonAsset(zoom, pos);
			}

			public boolean save() {
				try {
					ImageIO.write(doorGraphics, "PNG", new File(c64name));
					ImageIO.write(buttonGraphics, "PNG", new File(buttonsC64name));
				} catch (IOException e2) {
					e2.printStackTrace();
					return false;
				}
				return true;
			}
		};

		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
					case KeyEvent.VK_X: {
						if ((e.getModifiers() & (InputEvent.SHIFT_MASK)) != 0) {
							clearCharWithBackground();
						}
						break;
					}
				}
			}
		});
	}

	void clearCharWithBackground() {
		doActionOnCurrentAsset((currentAsset)->{
			byte pixels[] = ((DataBufferByte)currentAsset.src.getRaster().getDataBuffer()).getData();
			byte bgPixels[] = ((DataBufferByte)EotBGfxEditor.sharedInstance.wallsetGraphics[level].getRaster().getDataBuffer()).getData();

			for (int y=0; y<8; y++) {
				for (int x=0; x<4; x++) {
					int offset = mouseSrcChar.x + x + (mouseSrcChar.y + y)*currentAsset.src.getWidth();
					int bgOffset = mouseChar.x + x + (mouseChar.y + y)*EotBGfxEditor.sharedInstance.wallsetGraphics[level].getWidth();
					pixels[offset] = bgPixels[bgOffset];
				}
			}
		});
	}

	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
			case "Door": editMode = EDIT_DOOR; break;
			case "Button": editMode = EDIT_BUTTON; break;
		}
		redrawAll();
	}
}
