import java.io.*;
import java.awt.image.*;
import java.awt.*;
import java.awt.event.*;
import javax.imageio.*;
import javax.swing.*;
import java.util.*;

public class DecorationEditor extends Editor {
	private ArrayList<PrerenderedDecoration> uniqueDecorations;

	private BufferedImage decorationGraphics;
	private BufferedImage decorationPCWithContextGraphics;

	public DecorationEditor(int level) throws IOException, ClassNotFoundException {
		super(level);

		String filenames[][] = EotBGfxEditor.sharedInstance.filenames;

		delegate = new Editor.Delegate() {
			public void reserveAssets() throws IOException, ClassNotFoundException {
				if (uniqueDecorations == null) {
					ObjectInputStream ois = new ObjectInputStream(EotBGfxEditor.sharedInstance.getResourceAsStream(level, EotBGfxEditor.RES_PRECONV));
					uniqueDecorations = (ArrayList<PrerenderedDecoration>)ois.readObject();
					decorationGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStream(level, EotBGfxEditor.RES_DEC64));
					decorationGraphics = replacePalette(EotBGfxEditor.sharedInstance.wallsetGraphics[level], decorationGraphics);
					decorationPCWithContextGraphics = ImageIO.read(EotBGfxEditor.sharedInstance.getResourceAsStream(level, EotBGfxEditor.RES_DECPC));
					checkRenderTypes();
				}
				DecorationEditor.super.reserveAssets(uniqueDecorations.size(), 8);
			}

			public void renderContext(BufferedImage dst, Asset asset) {
				PrerenderedDecoration dec = uniqueDecorations.get(asset.row);

				renderWall(dst, 0, 0);

				blit(dst, 0,0, EotBGfxEditor.sharedInstance.wallsetGraphics[level], 0,0, 22*4,15*8);
				if ((dec.wallType>0) && (asset.column>0))
					renderWall(dst, dec.wallType, asset.column);
			}

			public void renderAsset(BufferedImage dst, Asset asset) {
				if (asset == null)
					return;
				Asset r = asset.inPixels();
				blit(dst, r.dx,r.dy, r.src, r.sx, r.sy, r.w, r.h);
			}

			public void renderPC(BufferedImage dst, Asset asset) {
				if (asset == null)
					return;

				Graphics g = dst.getGraphics();
				Asset r = asset.inPixels().hires();
				g.drawImage(EotBGfxEditor.sharedInstance.wallsetPCGraphics[level], 0,0, 22*8,15*8, 0,0, 22*8,15*8, null);

				int w = 22*8;
				int h = 15*8;
				int sx = 22*8*r.column;
				int sy = 15*8*r.row;

				g.drawImage(decorationPCWithContextGraphics,
				       0,0, w,h,
				       sx,sy, sx+w,sy+h,
				       null
				);
			}

			public Asset getAsset(int decorationIndex, int rectangleIndex) {
				PrerenderedDecoration dec = uniqueDecorations.get(decorationIndex);
				if ((dec.cw[rectangleIndex]==0) && (dec.ch[rectangleIndex]==0))
					return null;

				int w = dec.cw[rectangleIndex];
				int h = dec.ch[rectangleIndex];
				int dx = dec.cx[rectangleIndex];
				int dy = dec.cy[rectangleIndex];
				int sx = 22*rectangleIndex+dx;
				int sy = 15*decorationIndex+dy;

				return new Asset(decorationGraphics, dec.renderAsOverlay ? Asset.MODE_OVERLAY : Asset.MODE_PREBLEND, w,h,dx,dy,sx,sy, decorationIndex, rectangleIndex);
			}

			public boolean save() {
				try {
					optimize();
					ImageIO.write(decorationGraphics, "PNG", new File(filenames[level][1]));
					File preconv = new File(filenames[level][0]);
					ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(preconv));
					oos.writeObject(uniqueDecorations);
				} catch (IOException e2) {
					e2.printStackTrace();
					return false;
				}
				return true;
			}
		};

		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
					case KeyEvent.VK_LEFT: {
						if ((e.getModifiers() & (InputEvent.CTRL_MASK|InputEvent.META_MASK)) != 0) {
							expandLeft();
						} else if ((e.getModifiers() & InputEvent.SHIFT_MASK) != 0) {
							shrinkRight();
						}
						reloadAsset();
						break;
					}
					case KeyEvent.VK_RIGHT: {
						if ((e.getModifiers() & (InputEvent.CTRL_MASK|InputEvent.META_MASK)) != 0) {
							expandRight();
						} else if ((e.getModifiers() & InputEvent.SHIFT_MASK) != 0) {
							shrinkLeft();
						}
						reloadAsset();
						break;
					}
					case KeyEvent.VK_UP: {
						if ((e.getModifiers() & (InputEvent.CTRL_MASK|InputEvent.META_MASK)) != 0) {
							expandTop();
						} else if ((e.getModifiers() & InputEvent.SHIFT_MASK) != 0) {
							shrinkBottom();
						}
						reloadAsset();
						break;
					}
					case KeyEvent.VK_DOWN: {
						if ((e.getModifiers() & (InputEvent.CTRL_MASK|InputEvent.META_MASK)) != 0) {
							expandBottom();
						} else if ((e.getModifiers() & InputEvent.SHIFT_MASK) != 0) {
							shrinkTop();
						}
						reloadAsset();
						break;
					}
				}
			}
		});
	}

	private void optimize() {
		BufferedImage bg = new BufferedImage(22*4, 15*8, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)EotBGfxEditor.sharedInstance.wallsetGraphics[level].getColorModel());
		byte bgPixels[] = ((DataBufferByte)bg.getRaster().getDataBuffer()).getData();
		byte pixels[] = ((DataBufferByte)decorationGraphics.getRaster().getDataBuffer()).getData();

		for (int decorationIndex=0; decorationIndex<uniqueDecorations.size(); decorationIndex++) {
			PrerenderedDecoration dec = uniqueDecorations.get(decorationIndex);
			if (dec.renderAsOverlay)
				continue;

			for (int rectangleIndex=0; rectangleIndex<9; rectangleIndex++) {
				renderWall(bg, 0, 0);
				blit(bg, 0,0, EotBGfxEditor.sharedInstance.wallsetGraphics[level], 0,0, 22*4,15*8);
				if ((dec.wallType>0) && (rectangleIndex>0))
					renderWall(bg, dec.wallType, rectangleIndex);

				int sx = 22*4*rectangleIndex;
				int sy = 15*8*decorationIndex;

				for (int cy=0; cy<15; cy++) {
					for (int cx=0; cx<22; cx++) {
						boolean diffInChar = false;
						for (int y=0; y<8; y++) {
							for (int x=0; x<4; x++) {
								int bgOffset = (cy*8 + y)*bg.getWidth() + cx*4 + x;
								int dstOffset = (sy + cy*8 + y)*decorationGraphics.getWidth() + sx + cx*4 + x;
								if (pixels[dstOffset]>=16)
									pixels[dstOffset] = bgPixels[bgOffset];
								else if (pixels[dstOffset] != bgPixels[bgOffset])
									diffInChar = true;
							}
						}

						if (!diffInChar) {
							for (int y=0; y<8; y++) {
								for (int x=0; x<4; x++) {
									int dstOffset = (sy + cy*8 + y)*decorationGraphics.getWidth() + sx + cx*4 + x;
									pixels[dstOffset] = 16;
								}
							}							
						}
					}
				}
			}
		}
	}

	/* Local action handlers */
	private void expandLeft() {
		if (currentAsset == null) return;
		PrerenderedDecoration dec = uniqueDecorations.get(currentAsset.row);
		dec.cw[currentAsset.column]++;
		dec.cx[currentAsset.column]--;
	}
	private void expandRight() {
		if (currentAsset == null) return;
		PrerenderedDecoration dec = uniqueDecorations.get(currentAsset.row);
		dec.cw[currentAsset.column]++;
	}
	private void expandTop() {
		if (currentAsset == null) return;
		PrerenderedDecoration dec = uniqueDecorations.get(currentAsset.row);
		dec.ch[currentAsset.column]++;
		dec.cy[currentAsset.column]--;
	}
	private void expandBottom() {
		if (currentAsset == null) return;
		PrerenderedDecoration dec = uniqueDecorations.get(currentAsset.row);
		dec.ch[currentAsset.column]++;
	}

	private void shrinkLeft() {
		if (currentAsset == null) return;
		PrerenderedDecoration dec = uniqueDecorations.get(currentAsset.row);
		if (dec.cw[currentAsset.column]>1) {
			dec.cw[currentAsset.column]--;
			dec.cx[currentAsset.column]++;
		}
	}
	private void shrinkRight() {
		if (currentAsset == null) return;
		PrerenderedDecoration dec = uniqueDecorations.get(currentAsset.row);
		if (dec.cw[currentAsset.column]>1)
			dec.cw[currentAsset.column]--;
	}
	private void shrinkTop() {
		if (currentAsset == null) return;
		PrerenderedDecoration dec = uniqueDecorations.get(currentAsset.row);
		if (dec.ch[currentAsset.column]>1) {
			dec.ch[currentAsset.column]--;
			dec.cy[currentAsset.column]++;
		}
	}
	private void shrinkBottom() {
		if (currentAsset == null) return;
		PrerenderedDecoration dec = uniqueDecorations.get(currentAsset.row);
		if (dec.ch[currentAsset.column]>1)
			dec.ch[currentAsset.column]--;
	}

	private void checkRenderTypes() {
		byte pixels[] = ((DataBufferByte)decorationGraphics.getRaster().getDataBuffer()).getData();

		for (int i=0; i<uniqueDecorations.size(); i++) {
			PrerenderedDecoration dec = uniqueDecorations.get(i);
			int offset = 15*8*i*decorationGraphics.getWidth();
			dec.renderAsOverlay = pixels[offset]==0;
		}
	}
}
