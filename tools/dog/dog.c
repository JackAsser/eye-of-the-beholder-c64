#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#define MIN(a,b) (((a)<(b))?(a):(b))

int main(int argc, char** argv) {
	if (argc < 2) {
		fprintf(stderr, "Usage: dog <input files>\n");
		return 1;
	}

	off_t offset = 0;
	for (int i=1; i<argc; i++) {
		char* filename = argv[i];
		struct stat st;
		if (stat(filename, &st) != 0) {
			fprintf(stderr, "Failed to open %s (%d, '%s')\n", filename, errno, strerror(errno));
			return 2;
		}

		int fd = open(filename, O_RDONLY);
		unsigned char buf[65536];
		off_t left = st.st_size;
		while (left!=0) {
			ssize_t r = read(fd, buf, MIN(sizeof(buf), left));
			if (r == -1) {
				fprintf(stderr, "Failed to read from %s (%d, '%s')\n", filename, errno, strerror(errno));
				return 2;
			}
			if (write(fileno(stdout), buf, r) != r) {
				fprintf(stderr, "Didn't write the full buffer\n");
				return 3;
			}
			left -= r;
		}
		close(fd);

		for (char* c=filename; *c!='\0'; c++) {
			if (*c=='-' || *c==' ')
				*c = '_';
		}
		char* label = basename(filename);
		if (label == NULL) {
			fprintf(stderr, "Failed to generate a label from %s (%d, '%s')\n", filename, errno, strerror(errno));
			return 3;
		}
		char* dot = strchr(label, '.');
		if (dot != NULL)
			*dot = '\0';
		fprintf(stderr, "%s = %lld\n", label, offset);
		fprintf(stderr, "%s_size = %lld\n", label, st.st_size);

		offset += st.st_size;
	}

	return 0;
}
