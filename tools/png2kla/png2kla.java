import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class png2kla {
	public static void png2kla(String inName, String outName) throws Exception {
		C64Char.pixelWidth = 2;
		C64Char.defaultBackgroundColor = 0;
		C64Char.forceUnusedToBlack = true;
		C64Char.sortColors = true;
		BufferedImage bitmap = ImageIO.read(new File(inName));
		//Declash.declash(bitmap, C64Char.spriteColors);

		EOBObject obj = new EOBObject(bitmap, 0, 0, bitmap.getWidth()/8, bitmap.getHeight()/8);

		FileOutputStream fos = new FileOutputStream(outName);

		// Load address
		fos.write(0x00);
		fos.write(0x40);

		// Write bitmap
		for (int y=0; y<obj.height; y++) {
			for (int x=0; x<obj.width; x++) {
				C64Char c = obj.c64Chars[y+x*obj.height];
				for (int i=0; i<c.data.size(); i++)
					fos.write(c.data.get(i));
			}
		}

		// Screen ram
		for (int y=0; y<obj.height; y++) {
			for (int x=0; x<obj.width; x++) {
				C64Char c = obj.c64Chars[y+x*obj.height];
				fos.write(c.screen);
			}
		}

		// Color ram
		for (int y=0; y<obj.height; y++) {
			for (int x=0; x<obj.width; x++) {
				C64Char c = obj.c64Chars[y+x*obj.height];
				fos.write(c.d800&15);
			}
		}

		// Background color
		fos.write(0);

		fos.close();
	}

	public static void main(String[] args) {
		try {				
			png2kla.png2kla(args[0], args[1]);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}