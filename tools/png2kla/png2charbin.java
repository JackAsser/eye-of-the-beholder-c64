import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class png2charbin {
	public static void png2charbin(String inName, String outName) throws Exception {
		C64Char.pixelWidth = 2;
		C64Char.defaultBackgroundColor = 0;
		C64Char.forceUnusedToBlack = true;
		C64Char.sortColors = false;
		C64Char.spriteColors = new ArrayList<Byte>();
                C64Char.spriteColors.add((byte)0);
                C64Char.spriteColors.add((byte)1);
                C64Char.spriteColors.add((byte)2);
                C64Char.spriteColors.add((byte)3);
                C64Char.forcedType= C64Char.CharType.Sprite;
		BufferedImage bitmap = ImageIO.read(new File(inName));
		//Declash.declash(bitmap, C64Char.spriteColors);

		EOBObject obj = new EOBObject(bitmap, 0, 0, bitmap.getWidth()/8, bitmap.getHeight()/8);

		FileOutputStream fos = new FileOutputStream(outName);

		// Write bitmap
		for (int y=0; y<bitmap.getHeight(); y++) {
			for (int cx=0; cx<bitmap.getWidth()/8; cx++) {
				int cy = y/8;
				C64Char c = obj.c64Chars[cy+cx*obj.height];
				fos.write(c.data.get(y&7));
			}
		}

		fos.close();
	}

	public static void main(String[] args) {
		try {				
			png2charbin.png2charbin(args[0], args[1]);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}