import java.io.*;
import java.awt.image.*;
import javax.imageio.*;

public class kla2png {
	public static void main(String[] args) {
		if (args.length!=2) {
			System.out.println("Usage: kla2png input.kla output.png");
			System.exit(1);
		}

		try {
			FileInputStream fis = new FileInputStream(args[0]);
			fis.skip(2);
			byte bitmap[] = new byte[8000];
			fis.read(bitmap);
			byte screen[] = new byte[1000];
			fis.read(screen);
			byte d800[] = new byte[1000];
			fis.read(d800);
			byte bgColor = (byte)fis.read();
			if (bgColor==-1)
				bgColor=7;

			byte r[] = {(byte)0x00,(byte)0xFF,(byte)0x68,(byte)0x70,(byte)0x6F,(byte)0x58,(byte)0x35,(byte)0xB8,(byte)0x6F,(byte)0x43,(byte)0x9A,(byte)0x44,(byte)0x6C,(byte)0x9A,(byte)0x6C,(byte)0x95};
			byte g[] = {(byte)0x00,(byte)0xFF,(byte)0x37,(byte)0xA4,(byte)0x3D,(byte)0x8D,(byte)0x28,(byte)0xC7,(byte)0x4F,(byte)0x39,(byte)0x67,(byte)0x44,(byte)0x6C,(byte)0xD2,(byte)0x5E,(byte)0x95};
			byte b[] = {(byte)0x00,(byte)0xFF,(byte)0x2B,(byte)0xB2,(byte)0x86,(byte)0x43,(byte)0x79,(byte)0x6F,(byte)0x25,(byte)0x00,(byte)0x59,(byte)0x44,(byte)0x6C,(byte)0x84,(byte)0xB5,(byte)0x95};
			IndexColorModel icm = new IndexColorModel(8,16,r,g,b);
			BufferedImage image = new BufferedImage(320,200,BufferedImage.TYPE_BYTE_INDEXED,icm);
			byte pixels[] = ((DataBufferByte)image.getRaster().getDataBuffer()).getData();

			for (int cy=0; cy<25; cy++) {
				for (int cx=0; cx<40; cx++) {
					byte colors[] = {
						(byte)(bgColor&15),
						(byte)((screen[cx+cy*40]>>4)&15),
						(byte)(screen[cx+cy*40]&15),
						(byte)(d800[cx+cy*40]&15)
					};
					for (int y=0; y<8; y++) {
						byte bt = bitmap[cx*8+cy*320+y];
						int o = cx*8+(cy*8+y)*320;
						pixels[o+0] = pixels[o+1] = colors[(bt>>6)&3];
						pixels[o+2] = pixels[o+3] = colors[(bt>>4)&3];
						pixels[o+4] = pixels[o+5] = colors[(bt>>2)&3];
						pixels[o+6] = pixels[o+7] = colors[(bt>>0)&3];
					}
				}
			}

			ImageIO.write(image, "PNG", new File(args[1]));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
