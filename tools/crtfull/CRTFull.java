import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import java.awt.*;

public class CRTFull {
    public static void main(String[] args) {
        if (args.length!=2) {
            System.out.println("Usage CRTFull crt.bin crt.png");
            System.exit(1);
        }
        
        try {
            File inFile = new File(args[0]);
            byte[] inPixels = new byte[(int)inFile.length()];
            FileInputStream fis = new FileInputStream(inFile);
            fis.read(inPixels);
            fis.close();
            
            BufferedImage bi = new BufferedImage(1024,1024,BufferedImage.TYPE_INT_ARGB);
            int pixels[] = ((DataBufferInt)bi.getRaster().getDataBuffer()).getData();

            int bankFree[]=new int[64];
            int totalkbfree=0;
            for (int i=0; i<inPixels.length; i+=16384) {
                int bank = i/16384;
                int bankStart = i + ((bank<0x38) ? 0 : 8192);
                int bankSize = (bank<0x38) ? 16384 : 8192;
                int kbfree = 0;
                for (int j=0; j<bankSize; j+=1024) {
                    boolean free=true;
                    for (int k=bankStart+j; k<bankStart+j+1024; k++) {
                        if ((inPixels[k]&0xff)!=0xff) {
                            free=false;
                            break;
                        }
                    }
                    if (free)
                        kbfree++;
                }
                totalkbfree += kbfree;
                if (kbfree>0)
                    System.out.printf("%02x: %d\n", bank, kbfree);
                bankFree[bank] = kbfree;
            }
            System.out.printf("%dkb free\n", totalkbfree);

            for (int i=0; i<Math.min(pixels.length, inPixels.length); i++) {
                int bgcolor = 0xff000000;
                
                int color = (((i/16384)&1)==0) ? 0x010101 : 0x010001;
                
                pixels[i] = ((inPixels[i]&0xff)*color)|bgcolor;
            }
            Graphics g = bi.getGraphics();
            for (int i=0; i<64; i++) {
                int x = 30;
                int y = 13+i*16;
                String val = String.format("$%02x(%d)", i, bankFree[i]);
                g.setColor(Color.BLACK);
                for (int dy=-1; dy<=1; dy++)
                    for (int dx=-1; dx<=1; dx++)
                        g.drawString(val, x+dx, y+dy);
                g.setColor(Color.GREEN);
                g.drawString(val, x, y);
            }

            ImageIO.write(bi, "PNG", new File(args[1]));
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}