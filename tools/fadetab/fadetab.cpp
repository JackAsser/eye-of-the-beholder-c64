#include <stdio.h>

int main(void) {
	int colorsByLuma[][2] = {
		{0x0,0x0},
		{0x6,0x9},
		{0x2,0xb},
		{0x4,0x8},
		{0xc,0xe},
		{0x5,0xa},
		{0x3,0xf},
		{0x7,0xd},
		{0x1,0x1}
	};
	int lumaScale[] = {0,1,2,3,4,5,6,7,8,7,6,5,4,3,2,1};

	for (int srcColor=0; srcColor<16; srcColor++) {
		int srcLuma=0;
		int ramp=0;
		for (srcLuma=0; srcLuma<9; srcLuma++) {
			if (colorsByLuma[srcLuma][0] == srcColor) {
				ramp=0;
				break;
			} else if (colorsByLuma[srcLuma][1] == srcColor) {
				ramp=1;
				break;
			}
		}

		printf(".byte ");
		for (int fadeStep=0; fadeStep<16; fadeStep++) {
			int scale = lumaScale[fadeStep];
			int value = srcLuma + ((8-srcLuma)*scale+4)/8;
			if (fadeStep>0)
				printf(",");
			printf("$%0x", colorsByLuma[value][ramp]);
		}
		printf("\n");
	}

	return 0;
}