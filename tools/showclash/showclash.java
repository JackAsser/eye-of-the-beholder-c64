import java.io.*;
import java.awt.image.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.imageio.*;
import javax.swing.*;

public class showclash extends JFrame {
	private static int ZOOM=3;
	private BufferedImage image, bgImage;

	private void reload(String file) throws IOException {
		int retries=0;
		while(true) {
			try {
				image = ImageIO.read(new File(file));
				if (image!=null)
					break;
				else {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e2) {;}	
				}
			} catch (IOException e) {
				retries++;
				if (retries==10) {
					e.printStackTrace();
					System.exit(1);
				} else {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e2) {;}	
				}
			}
		}

		BufferedImage wideImage = new BufferedImage(image.getWidth()*2, image.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
		AffineTransform at = new AffineTransform();
		at.scale(2.0, 1.0);
		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		wideImage = scaleOp.filter(image, wideImage);

		Colodore colodore = new Colodore();
		BufferedImage filteredImage = colodore.apply(wideImage);

		bgImage = new BufferedImage(filteredImage.getWidth()*2, filteredImage.getHeight()*2, BufferedImage.TYPE_INT_ARGB);
		at = new AffineTransform();
		at.scale(2.0, 2.0);
		scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		bgImage = scaleOp.filter(filteredImage, bgImage);
	}

	public showclash(String file) throws IOException {
		super("showclash");

		reload(file);

		JPanel panel = new JPanel() {
			public void paint(Graphics g) {
				int ipixels[] = image.getSampleModel().getPixels(0,0,image.getWidth(),image.getHeight(),(int[])null, image.getRaster().getDataBuffer());
				g.drawImage(bgImage,0,0,bgImage.getWidth()/2, bgImage.getHeight()/2, null);

				g.setColor(Color.RED);
				for (int cy=0; cy<image.getHeight(); cy+=8) {
					for (int cx=0; cx<image.getWidth(); cx+=4*C64Char.pixelWidth) {
						C64Char.CharType type = C64Char.charType(ipixels, image.getWidth(), cx/(4*C64Char.pixelWidth), cy/8);
						if (type == C64Char.CharType.Bitmap) {
							try {
								new C64Char(image, ipixels, cx/4, cy/8);
							} catch (Exception e) {
								g.drawRect(cx*ZOOM*2+1, cy*ZOOM+1, 4*ZOOM*2-2,8*ZOOM-2);
							}
						}
					}
				}
				
				g.setColor(Color.DARK_GRAY);
				for (int cy=0; cy<image.getHeight(); cy+=8)
					g.drawLine(0,cy*ZOOM,image.getWidth()*ZOOM*2,cy*ZOOM);
				for (int cx=0; cx<image.getWidth(); cx+=4*C64Char.pixelWidth)
					g.drawLine(cx*ZOOM*2,0,cx*ZOOM*2,image.getHeight()*ZOOM);
			}
			
			public Dimension getPreferredSize() {
				return new Dimension(image.getWidth()*ZOOM*2, image.getHeight()*ZOOM);
			}
		};
		
		setLayout(new BorderLayout());
		
		JScrollPane scrollPane=new JScrollPane(panel);
		scrollPane.getVerticalScrollBar().setUnitIncrement(32);
		scrollPane.getHorizontalScrollBar().setUnitIncrement(32);
		
		add(scrollPane, BorderLayout.CENTER);
		pack();
		setResizable(true);
		setVisible(true);
		toFront();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		new Thread(()->{
			while(true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
				try {
					reload(file);
					panel.repaint();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		}).start();
	}

	public static void main(String[] args) {
		if (args.length!=1) {
			System.out.println("Usage: showclash image.png");
			System.exit(1);
		}
		
		try {
			new showclash(args[0]);
		} catch(Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
