var font = require('./font');

module.exports = (function() {
	this.brightness = 50;	// 0..100
	this.contrast = 100;		// 0..100
	this.saturation = 50;	// 0..100
	this.gammasrc = 2.8;		// 1.6 .. 2.8 PAL
	this.gammatgt = 2.2;		// 1.6 .. 2.8 sRGB
	this.phase = 0;			// -45 .. 45 color phase-offset
	this.odd = 360 / 16;		// 0..50 hanover bar phase-angle
	this.scanshade = 1 / 3;	// 0 .. 0.5 scanline transparency
	this.sub = 256 / 4;		// 0..255 scanline brightness-shift
	this.contrastBoost = 1 / 5;	// this helps contrast to work like the "1084s" crt
	this.crtBend = 0.07;
	this.scanLines = true;
	this.hanoverBars = true;
	this.delayLine = 0.5; // 0 .. 0.5 mix chroma with previous line
	this.chromaSubsampling = true;
	this.earlyLuma = false;
	var retina = false;
	var bilinearFiltering = true;
    var externalInterface = {};
    var eventListeners = {
        "mouseout":[],
        "mousemove":[],
        "click":[]
    };

	Math.clamp = function(value, min, max) {
		return Math.min(Math.max(value,min),max);
	}

	// internal state ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	var GL = undefined;
	var yuvFBO;
	var rgbFBO;
	var chromaSubSampleFBO;
	var resultyuv = [];
	var currentSize = "triple";

	// internal constants ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//	var border = [43, 36, 49, 48]; // t,r,b,l
	var border = [32,32,32,32]; // t,r,b,l
	var displayWidth = 320+border[1]+border[3];
	var displayHeight = 200+border[0]+border[2];
    var selection = undefined;

	var canvasSize = {
		"none": {
			"width":displayWidth,
			"height":displayHeight,
            "scale":1.0,
			"scanlineDelta":0,
			"scanlineModulus":0
		},
		"double": {
			"width":Math.round(displayWidth*0.9375*2),
			"height":displayHeight*2-1,
            "scale":2.0,
			"scanlineHeight":displayHeight*2-1,
			"scanlineDelta":0,
			"scanlineModulus":2
		},
		"triple": {
			"width":Math.round(displayWidth*0.9375*3),
			"height":displayHeight*3-2,
            "scale":3.0,
			"scanlineHeight":displayHeight*3,
			"scanlineDelta":-1,
			"scanlineModulus":3
		}
	};

	WebGLRenderingContext.prototype.loadShader = function(src, type, defines) {
	    var shader = this.createShader(type);

        src = "precision highp float;\n"+src;

        this.shaderSource(shader, src);
	    this.compileShader(shader);

	    if (!this.getShaderParameter(shader, this.COMPILE_STATUS)) {
	      console.log("Shader error:");
	      console.log(this.getShaderInfoLog(shader));
	      console.log(src);
	      return null;
	    }

	    return shader;
	}

	var Program = function(vs, fs) {
	    var p = GL.createProgram();
	    GL.attachShader(p, vs);
	    GL.attachShader(p, fs);
	    GL.linkProgram(p);

	    if (!GL.getProgramParameter(p, GL.LINK_STATUS)) {
	      alert("Could not initialize shader");
	      return {linked:false};
	    }

	    this.locations = {};
	    this.linked = true;

	    var attributes = [];
	    this.bindAttributes = function(attrs) {
	      for (var i in attrs) {
	        this.locations[attrs[i]] = GL.getAttribLocation(p, attrs[i]);
	        attributes.push(attrs[i]);
	      }
	    }

	    this.bindUniforms = function(attrs) {
	      for (var i in attrs)
	        this.locations[attrs[i]] = GL.getUniformLocation(p, attrs[i]);
	    }

	    this.use = function() {
	      GL.useProgram(p);
	      for (var i in attributes) {
	        var attr = attributes[i];
	        if (this.locations[attr]>=0) {
	          GL.enableVertexAttribArray(this.locations[attr]);
	        }
	      }

	      if (this.locations.iResolution)
	  	  GL.uniform2fv(this.locations.iResolution, new Float32Array([GL.viewportWidth, GL.viewportHeight]));
	      if (this.locations.iGlobalTime)
	  	  GL.uniform1f(this.locations.iGlobalTime, GL.time);
	    }
	};

	var FBO = function(width,height,mag,min,enableDepthStencil) {
		if ((width === undefined) && (height === undefined)) {
			width = GL.viewportWidth;
			height = GL.viewportHeight;
		}

		this.width = width;
		this.height = height;

		this.colorTexture = GL.createTexture();
		GL.bindTexture(GL.TEXTURE_2D, this.colorTexture);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, mag);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, min);
		GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, width, height, 0, GL.RGBA, GL.UNSIGNED_BYTE, null);

		this.fbo = GL.createFramebuffer();
		GL.bindFramebuffer(GL.FRAMEBUFFER, this.fbo);
		GL.framebufferTexture2D(GL.FRAMEBUFFER, GL.COLOR_ATTACHMENT0, GL.TEXTURE_2D, this.colorTexture, 0);

        if (enableDepthStencil === true) {
            var stencilBuffer = GL.createRenderbuffer();
            GL.bindRenderbuffer(GL.RENDERBUFFER, stencilBuffer);
            GL.renderbufferStorage(GL.RENDERBUFFER, GL.DEPTH_STENCIL, width, height);
            GL.framebufferRenderbuffer(GL.FRAMEBUFFER, GL.DEPTH_STENCIL_ATTACHMENT, GL.RENDERBUFFER, stencilBuffer);
        }

		this.status =  GL.checkFramebufferStatus(GL.FRAMEBUFFER);
		if (this.status !== GL.FRAMEBUFFER_COMPLETE) {
			console.log("Failed to create FBO: "+this.status);
		}

		this.dispose = function() {
			GL.bindFramebuffer(GL.FRAMEBUFFER, null);
			GL.deleteFramebuffer(this.fbo);
			this.fbo=undefined;
			GL.deleteTexture(this.colorTexture);
			this.colorTexture=undefined;
		};

		FBO.unbind();
	};

	FBO.prototype.bind = function() {
		GL.bindFramebuffer(GL.FRAMEBUFFER, this.fbo);
		GL.viewport(0,0, this.width, this.height);
	};

	FBO.unbind = function() {
		GL.bindFramebuffer(GL.FRAMEBUFFER, null);
		GL.viewport(0,0, GL.viewportWidth, GL.viewportHeight);
	};

	function createRenderFunctions() {
		var quadBuffer = GL.createBuffer();
        GL.bindBuffer(GL.ARRAY_BUFFER, quadBuffer);
        GL.bufferData(GL.ARRAY_BUFFER, new Float32Array(
        [   -1.0, 1.0, 0,0,
            -1.0,-1.0, 0,1,
             1.0, 1.0, 1,0,
             1.0,-1.0, 1,1
        ]), GL.STATIC_DRAW);

        var prepareBlit = function(program) {
			GL.bindBuffer(GL.ARRAY_BUFFER, quadBuffer);
            GL.vertexAttribPointer(program.locations.inVertex, 2, GL.FLOAT, false, 4*4, 0*4);
            GL.vertexAttribPointer(program.locations.inTexCoord, 2, GL.FLOAT, false, 4*4, 2*4);
        };

        var charVS = GL.loadShader([
        	"varying vec2 texCoord;",
        	"uniform vec2 position;",
        	"uniform float char;",
 			"attribute vec2 inVertex;",
 			"attribute vec2 inTexCoord;",
 			"",
 			"void main(void) {",
 			"	const vec2 borderScale = vec2(320.0/"+displayWidth+".0, 200.0/"+displayHeight+".0);",
 			"	const vec2 topLeft = vec2(-("+displayWidth+".0-320.0), "+displayHeight+".0-200.0);",
 			"	const vec2 windowSize = vec2("+displayWidth+".0, "+displayHeight+".0);",
 			"	vec2 tmp = (inVertex*0.5 + vec2(position.x-19.5, 12.0-position.y)) / vec2(20.0, 12.5);",
 			"	tmp *= borderScale;",
 			"	tmp += (topLeft/windowSize);",
 			"	tmp += vec2("+border[3]+".0, -"+border[0]+".0)/windowSize*2.0;",
 			"   gl_Position = vec4(tmp, 0.0, 1.0);",
 			"	vec2 chpos = vec2(mod(char,16.0), floor(char/16.0));",
 			"   texCoord = (inTexCoord+chpos)/vec2(16.0,32.0);",
 			"}"
        ].join("\n"), GL.VERTEX_SHADER);

        var charFS = GL.loadShader([
        	"varying vec2 texCoord;",
        	"uniform vec4 fgColor;",
        	"uniform vec4 bgColor;",
        	"uniform sampler2D texture;",
        	"",
        	"void main(void) {",
        	"	gl_FragColor = mix(bgColor, fgColor, texture2D(texture, texCoord).a);",
        	"}"
        ].join("\n"), GL.FRAGMENT_SHADER);



		var quadVS = GL.loadShader([
        	"varying vec2 texCoord;",
            "varying vec2 texCoordPreviousLine;",
 			"attribute vec2 inVertex;",
 			"attribute vec2 inTexCoord;",
 			"uniform vec2 scale;",
 			"uniform vec2 position;",
 			"uniform vec2 tscale;",
 			"uniform vec2 tposition;",
 			"",
 			"void main(void) {",
 			"   gl_Position = vec4(inVertex*scale+position, 0.0, 1.0);",
 			"   texCoord = vec2(inTexCoord.x, 1.0-inTexCoord.y)*tscale+tposition;",
            "   texCoordPreviousLine = texCoord;",
            "   texCoordPreviousLine.y += 1.0/"+displayHeight+".0;",
 			"}"
        ].join("\n"), GL.VERTEX_SHADER);

       	var copyFS = GL.loadShader([
        	"varying vec2 texCoord;",
        	"uniform sampler2D texture;",
        	"",
        	"void main(void) {",
        	"	gl_FragColor = texture2D(texture, texCoord);",
        	"}"
        ].join("\n"), GL.FRAGMENT_SHADER);

        var indexedFS = GL.loadShader([
            "varying vec2 texCoord;",
            "uniform sampler2D texture;",
            "uniform sampler2D palette;",
            "",
            "void main(void) {",
            "   vec2 index = vec2(texture2D(texture, texCoord).a, 0.5);",
            "   vec3 yuv = texture2D(palette, index).rgb;",
            "   gl_FragColor = vec4(yuv, 0.0);",
            "}"
        ].join("\n"), GL.FRAGMENT_SHADER);

       	var bendFS = GL.loadShader([
        	"varying vec2 texCoord;",
        	"uniform sampler2D texture;",
        	"uniform float curvature;",
        	"",
        	"void main(void) {",
        	"	float x = 1.0-distance(texCoord, vec2(0.5,0.5));",
        	"	vec2 g = vec2 (0.5, 0.5) - texCoord;",
        	"	vec2 uv = texCoord + g*x*curvature;",
        	"	gl_FragColor = texture2D(texture, uv);",
        	"}"
        ].join("\n"), GL.FRAGMENT_SHADER);

		var chromaSubSampleVS = GL.loadShader([
        	"varying vec2 texCoord[5];",
 			"attribute vec2 inVertex;",
 			"attribute vec2 inTexCoord;",
 			"uniform float delay;",
 			"",
 			"void main(void) {",
 			"   gl_Position = vec4(inVertex, 0.0, 1.0);",
 			"   texCoord[0] = vec2(inTexCoord.x-2.0/"+displayWidth+".0, 1.0-inTexCoord.y);",
            "   texCoord[1] = vec2(inTexCoord.x-1.0/"+displayWidth+".0, 1.0-inTexCoord.y);",
            "   texCoord[2] = vec2(inTexCoord.x,                        1.0-inTexCoord.y);",
            "   texCoord[3] = vec2(inTexCoord.x+1.0/"+displayWidth+".0, 1.0-inTexCoord.y);",
            "   texCoord[4] = vec2(inTexCoord.x+2.0/"+displayWidth+".0, 1.0-inTexCoord.y);",
 			"}"
        ].join("\n"), GL.VERTEX_SHADER);

       	var chromaSubSampleFS = GL.loadShader([
        	"varying vec2 texCoord[5];",
        	"uniform sampler2D texture;",
        	"",
        	"void main(void) {",
        	"	vec2 chroma = ",
            "       texture2D(texture, texCoord[0]).gb*0.06136 +",
            "       texture2D(texture, texCoord[1]).gb*0.24477 +",
            "       texture2D(texture, texCoord[2]).gb*0.38774 +",
            "       texture2D(texture, texCoord[3]).gb*0.24477 +",
            "       texture2D(texture, texCoord[4]).gb*0.06136;",
        	"	gl_FragColor = vec4(0.0, chroma.r, chroma.g, 1.0);",
        	"}"
        ].join("\n"), GL.FRAGMENT_SHADER);



        var yuv2rgbFS = GL.loadShader([
        	"varying vec2 texCoord;",
            "varying vec2 texCoordPreviousLine;",
        	"uniform sampler2D lumaTexture;",
        	"uniform sampler2D chromaTexture;",
        	"uniform float gammasrc;",
        	"uniform float gammatgt;",
        	"uniform vec2 odd;",
            "uniform float delayLine;",
        	"",
        	"void main(void) {",
        	// The yuv2rgb matrix
        	"	const mat3 yuv2rgb = mat3(vec3(1.000, 0.000, 1.140),",
        	"									  vec3(1.000,-0.396,-0.581),",
        	"                                     vec3(1.000, 2.029, 0.000));",
        	// Fetch luma & chroma into yuv
        	"	float luma = texture2D(lumaTexture, texCoord).r;",
        	"	vec2 chroma1 = texture2D(chromaTexture, texCoord).gb-0.5;",
            "   vec2 chroma2 = texture2D(chromaTexture, texCoordPreviousLine).gb-0.5;",
            "   vec2 chroma = mix(chroma1, chroma2, delayLine);",
        	"	vec3 yuv = vec3(luma, chroma.r, chroma.g);",
        	// Rotate v by odd every second line
        	"	yuv.b = mix(yuv.b, dot(yuv.gb,odd), mod(floor(texCoord.y*"+displayHeight+".0), 2.0));",
        	// Transform yuv 2 rgb
        	"	vec3 rgb = clamp(yuv*yuv2rgb, 0.0, 1.0);",
        	// Remove source gamma
        	"	rgb = clamp(pow(rgb, vec3(gammasrc)), 0.0, 1.0);",
        	// Apply destination gamma
        	"	rgb = clamp(pow(rgb, vec3(gammatgt)), 0.0, 1.0);",
        	"	gl_FragColor = vec4(rgb, 1.0);",
        	"}"
        ].join("\n"), GL.FRAGMENT_SHADER);



        var scanLinesFS = GL.loadShader([
        	"varying vec2 texCoord;",
        	"uniform sampler2D lumaTexture;",
        	"uniform float scanshade;",
        	"uniform float height;",
        	"uniform float modulus;",
        	"uniform float delta;",
        	"",
        	"void main(void) {",
        	"	float lumaInverted = texture2D(lumaTexture, texCoord).a;",
        	"	float a = scanshade * lumaInverted * min(mod(floor(texCoord.y*height)+delta, 3.0),1.0);",
        	"	gl_FragColor = vec4(0.0,0.0,0.0, a);",
        	"}"

        ].join("\n"), GL.FRAGMENT_SHADER);



        var selectFS = GL.loadShader([
            "void main(void) {",
            "   gl_FragColor = vec4(0.7, 0.7, 1.0, 0.5);",
            "}"

        ].join("\n"), GL.FRAGMENT_SHADER);



        var fontTexture = (function() {
        	var t = GL.createTexture();

	        var data = new Uint8Array(16*32*8*8);
	        for (var cy=0; cy<32; cy++) {
	        	for (var cx=0; cx<16; cx++) {
	        		var c = cx+cy*16;
	        		for (var y=0; y<8; y++) {
			        	for (var x=0; x<8; x++) {
		        			data[cx*8+x+(cy*8+y)*128] = (font[c*8+y]&(1<<(7-x))) == 0 ? 0 : 0xff;
		        		}
	        		}
	        	}
	        }

	        GL.bindTexture(GL.TEXTURE_2D, t);
	        GL.texImage2D(GL.TEXTURE_2D, 0, GL.ALPHA, 128, 256, 0, GL.ALPHA, GL.UNSIGNED_BYTE, data);
	        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
			GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);

			return t;
		})();

        var charProgram = new Program(charVS, charFS);
        charProgram.bindUniforms(["position", "char", "fgColor", "bgColor", "texture"]);
        charProgram.bindAttributes(["inVertex", "inTexCoord"]);
        charProgram.use();
		GL.uniform1i(charProgram.locations.texture, 0);

        var copyProgram = new Program(quadVS, copyFS);
        copyProgram.bindUniforms(["texture", "scale", "position", "tscale", "tposition"]);
        copyProgram.bindAttributes(["inVertex", "inTexCoord"]);
        copyProgram.use();
	    GL.uniform1i(copyProgram.locations.texture, 0);

        var indexedProgram = new Program(quadVS, copyFS);
        indexedProgram.bindUniforms(["texture", "scale", "position", "tscale", "tposition", "palette"]);
        indexedProgram.bindAttributes(["inVertex", "inTexCoord"]);
        indexedProgram.use();
        GL.uniform1i(indexedProgram.locations.texture, 0);

        var bendProgram = new Program(quadVS, bendFS);
        bendProgram.bindUniforms(["texture", "scale", "position", "tscale", "tposition", "curvature"]);
        bendProgram.bindAttributes(["inVertex", "inTexCoord"]);
        bendProgram.use();
	    GL.uniform1i(bendProgram.locations.texture, 0);

	    var chromaSubSampleProgram = new Program(chromaSubSampleVS, chromaSubSampleFS);
        chromaSubSampleProgram.bindUniforms(["texture"]);
        chromaSubSampleProgram.bindAttributes(["inVertex", "inTexCoord"]);
        chromaSubSampleProgram.use();
	    GL.uniform1i(chromaSubSampleProgram.locations.texture, 0);

	    var yuv2rgbProgram = new Program(quadVS, yuv2rgbFS)
        yuv2rgbProgram.bindUniforms(["lumaTexture","chromaTexture","gammasrc","gammatgt","odd","scale","position", "tscale", "tposition","delayLine"]);
        yuv2rgbProgram.bindAttributes(["inVertex", "inTexCoord"]);
        yuv2rgbProgram.use();
	    GL.uniform1i(yuv2rgbProgram.locations.lumaTexture, 0);
	    GL.uniform1i(yuv2rgbProgram.locations.chromaTexture, 1);

	    var scanLinesProgram = new Program(quadVS, scanLinesFS);
        scanLinesProgram.bindUniforms(["lumaTexture","scanshade","scale","position","tscale", "tposition","height","delta","modulus"]);
        scanLinesProgram.bindAttributes(["inVertex", "inTexCoord"]);
        scanLinesProgram.use();
	    GL.uniform1i(scanLinesProgram.locations.lumaTexture, 0);

        var selectProgram = new Program(quadVS, selectFS);
        selectProgram.bindUniforms(["scale","position"]);
        selectProgram.bindAttributes(["inVertex", "inTexCoord"]);
        selectProgram.use();

	    function resetQuadVS(program) {
			GL.uniform2f(program.locations.scale, 1,1);
		    GL.uniform2f(program.locations.position, 0,0);
			GL.uniform2f(program.locations.tscale, 1,1);
		    GL.uniform2f(program.locations.tposition, 0,0);
	    }

        return {
        	copy:function(texture) {
        		copyProgram.use();
        		resetQuadVS(copyProgram);
			    GL.bindTexture(GL.TEXTURE_2D, texture);
			    prepareBlit(copyProgram);
                GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
        	},
        	bend:function(texture, curvature) {
        		bendProgram.use();
        		resetQuadVS(bendProgram);
   			    GL.uniform1f(bendProgram.locations.curvature, curvature);
			    GL.bindTexture(GL.TEXTURE_2D, texture);
			    prepareBlit(bendProgram);
                GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
        	},
            selectRows:function(start, stop) {
                selectProgram.use();
                resetQuadVS(selectProgram);
                var selectionOffset = start;
                var selectionHeight = start-stop+1;
                GL.uniform2f(selectProgram.locations.scale, 320/displayWidth, (selectionHeight*8)/displayHeight);
                GL.uniform2f(selectProgram.locations.position, 0, 1.0-((border[0]+(selectionHeight/2+selectionOffset)*8-0.1)/displayHeight)*2);
                prepareBlit(selectProgram);
                GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);                
            },
        	chromaSubSample:function(yuvTexture) {
        		chromaSubSampleProgram.use();
        		GL.bindTexture(GL.TEXTURE_2D, yuvTexture);
        		prepareBlit(chromaSubSampleProgram);
                GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
        	},
        	yuv2rgb:function(lumaTexture, chromaTexture) {
        		yuv2rgbProgram.use();
        		resetQuadVS(yuv2rgbProgram);

                GL.uniform1f(yuv2rgbProgram.locations.delayLine, delayLine);

			    var shift = (currentSize == "double") ? 0 : 1;
			    var scaleY = (GL.viewportHeight+shift+1) / GL.viewportHeight;
			    GL.uniform2f(yuv2rgbProgram.locations.scale, 1,scaleY);
			    GL.uniform2f(yuv2rgbProgram.locations.position, 0,0);

        		if (hanoverBars)
	        		GL.uniform2f(yuv2rgbProgram.locations.odd, Math.sin( odd * Math.PI / 180 ), Math.cos( odd * Math.PI / 180 ));
	        	else
	        		GL.uniform2f(yuv2rgbProgram.locations.odd, 0,1);
        		GL.uniform1f(yuv2rgbProgram.locations.gammasrc, gammasrc);
        		GL.uniform1f(yuv2rgbProgram.locations.gammatgt, 1/gammatgt);

        		GL.activeTexture(GL.TEXTURE1);
        		GL.bindTexture(GL.TEXTURE_2D, chromaTexture);
        		GL.activeTexture(GL.TEXTURE0);
        		GL.bindTexture(GL.TEXTURE_2D, lumaTexture);
        		prepareBlit(yuv2rgbProgram);
                GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
        	},
        	applyScanLines:function(lumaTexture, scanshade) {
        		scanLinesProgram.use();
        		resetQuadVS(scanLinesProgram);

			    var shift = (currentSize == "double") ? 0 : 1;
			    var scaleY = (GL.viewportHeight+shift+1) / GL.viewportHeight;
    			GL.uniform2f(scanLinesProgram.locations.scale, 1,scaleY);
			    GL.uniform2f(scanLinesProgram.locations.position, 0,0);
			    GL.uniform1f(scanLinesProgram.locations.height, canvasSize[currentSize].scanlineHeight);
			    GL.uniform1f(scanLinesProgram.locations.delta, canvasSize[currentSize].scanlineDelta);
			    GL.uniform1f(scanLinesProgram.locations.modulus, canvasSize[currentSize].scanlineModulus);

        		GL.uniform1f(scanLinesProgram.locations.scanshade, scanshade);
        		GL.bindTexture(GL.TEXTURE_2D, lumaTexture);
        		prepareBlit(scanLinesProgram);
                GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
			},
        	beginDrawChars:function() {
        		charProgram.use();
			    GL.bindTexture(GL.TEXTURE_2D, fontTexture);
			    prepareBlit(charProgram);
			    yuvFBO.bind();
        	},
        	drawChar:function(ch, x, y, fgColor, bgColor) {
        		var f0 = Math.clamp(resultyuv[fgColor][0] ,0,255)/255.0;
        		var f1 = Math.clamp((resultyuv[fgColor][1]+128) ,0,255)/255.0;
        		var f2 = Math.clamp((resultyuv[fgColor][2]+128) ,0,255)/255.0;
        		var f3 = Math.clamp((255-resultyuv[fgColor][0] + sub) ,0,255)/255.0;
        		var b0 = Math.clamp(resultyuv[bgColor][0] ,0,255)/255.0;
        		var b1 = Math.clamp((resultyuv[bgColor][1]+128) ,0,255)/255.0;
        		var b2 = Math.clamp((resultyuv[bgColor][2]+128) ,0,255)/255.0;
        		var b3 = Math.clamp((255-resultyuv[bgColor][0] + sub) ,0,255)/255.0;

		        GL.uniform4f(charProgram.locations.fgColor, f0,f1,f2,f3);
		        GL.uniform4f(charProgram.locations.bgColor, b0,b1,b2,b3);
   		        GL.uniform1f(charProgram.locations.char, ch);
		        GL.uniform2f(charProgram.locations.position, x,y);
                GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
        	},
        	drawImage:function(image) {
			    yuvFBO.bind();
        		copyProgram.use();
        		resetQuadVS(copyProgram);

        		var scaleX = 320.0/displayWidth;
        		var scaleY = 200.0/displayHeight;
        		var positionX = scaleX-1 + border[3]/displayWidth*2;
        		var positionY = 1-scaleY - border[0]/displayHeight*2;
        		var tscaleX = image.width / image.textureWidth;
        		var tscaleY = image.height / image.textureHeight;

			    GL.uniform2f(copyProgram.locations.scale, scaleX,-scaleY);
			    GL.uniform2f(copyProgram.locations.tscale, tscaleX, tscaleY);
	 			GL.uniform2f(copyProgram.locations.position, positionX, positionY);
			    GL.bindTexture(GL.TEXTURE_2D, image);
			    prepareBlit(copyProgram);
                GL.drawArrays(GL.TRIANGLE_STRIP, 0, 4);
        	},
        	clear:function(color) {
        		var f0 = Math.clamp(resultyuv[color][0] ,0,255)/255.0;
        		var f1 = Math.clamp((resultyuv[color][1]+128) ,0,255)/255.0;
        		var f2 = Math.clamp((resultyuv[color][2]+128) ,0,255)/255.0;
        		var f3 = Math.clamp((255-resultyuv[color][0] + sub) ,0,255)/255.0;
        		yuvFBO.bind();
        		GL.clearColor(f0,f1,f2,f3);
        		GL.clear(GL.COLOR_BUFFER_BIT);
        	}
        };
	}

	function initialize(element) {
		var ctx = element.getContext("experimental-webgl", {antialias:false, alpha:false, depth:false, premultipliedAlpha:false, preserveDrawingBuffer:false, stencil:false});
		if (!ctx)
			window.alert("Oops! Can't initialize webGL\r\nTry restarting your browser!\r\n(or you have an outdated browser)");

        var devicePixelRatio = window.devicePixelRatio || 1;
        if ((devicePixelRatio>1) && retina) {
			element.style.width = (canvasSize[currentSize]["width"]/devicePixelRatio) + "px";
			element.style.height = (canvasSize[currentSize]["height"]/devicePixelRatio) + "px";
		} else {
			element.style.width = canvasSize[currentSize]["width"] + "px";
			element.style.height = canvasSize[currentSize]["height"]+ "px";
		}
		ctx.canvas.width  = canvasSize[currentSize]["width"];
		ctx.canvas.height = canvasSize[currentSize]["height"];

	    GL = ctx;
        GL.viewportWidth = ctx.canvas.width;
        GL.viewportHeight = ctx.canvas.height;

		yuvFBO = new FBO(displayWidth, displayHeight, bilinearFiltering?GL.LINEAR:GL.NEAREST, GL.LINEAR, true);
		chromaSubSampleFBO = new FBO(displayWidth, displayHeight, GL.LINEAR, GL.LINEAR);
		rgbFBO = new FBO(GL.viewportWidth, GL.viewportHeight, GL.LINEAR, GL.LINEAR);

        return ctx;
	}

	function convertImage(image) {
		var height = image.length;
		var width = image[0].length;

		var data = new Uint8Array(512*256*4);

		// create yuv image
		for (var i=0; i<height; i++) {
			for (var j=0; j<width; j++) {
				var color = resultyuv[image[i][j]];
				var pxl = (i*512 + j)*4;

				data[pxl+0] = Math.clamp(color[ 0 ], 0, 255);
				data[pxl+1] = Math.clamp(color[ 1 ] + 128, 0, 255);
				data[pxl+2] = Math.clamp(color[ 2 ] + 128, 0, 255);
				data[pxl+3] = Math.clamp(255-color[ 0 ] + sub, 0, 255);
			}
		}

		var t = GL.createTexture();
		t.width = width;
		t.height = height;
		t.textureWidth = 512;
		t.textureHeight = 256;
        GL.bindTexture(GL.TEXTURE_2D, t);
        GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, 512, 256, 0, GL.RGBA, GL.UNSIGNED_BYTE, data);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.CLAMP_TO_EDGE);
        GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.CLAMP_TO_EDGE);
        return t;
	}

	function postProcess() {
        GL.disable(GL.STENCIL_TEST);

        if (chromaSubsampling) {
    	   chromaSubSampleFBO.bind();
	   	   rf.chromaSubSample(yuvFBO.colorTexture);
           FBO.unbind();
        }

		rgbFBO.bind();
        GL.clearColor(0,0,0,1);
        GL.clear(GL.COLOR_BUFFER_BIT);
		rf.yuv2rgb(yuvFBO.colorTexture, chromaSubsampling ? chromaSubSampleFBO.colorTexture : yuvFBO.colorTexture);

        GL.enable(GL.BLEND);
        GL.blendFunc(GL.SRC_ALPHA, GL.ONE_MINUS_SRC_ALPHA);
		
        if (scanLines)
			rf.applyScanLines(yuvFBO.colorTexture, scanshade);

        // Apply selection
        if (selection !== undefined)
            rf.selectRows(selection.begin, selection.end);

        GL.disable(GL.BLEND);

		FBO.unbind();
        GL.clearColor(1,0,1,1);
        GL.clear(GL.COLOR_BUFFER_BIT);
        rf.bend(rgbFBO.colorTexture, crtBend);
//        rf.copy(rgbFBO.colorTexture);

		return;
	}

	function luma(input) {
		var factor = 256/32;
		return input * factor;
	}

	function angle(input, phs) {
		var factor = 360 / 16;
		var degree = Math.PI / 180;
		var rotate = factor / 2 + phs;
		return (input * factor + rotate) * degree;
	}

	function createPalette() {
		// convert percentage-values of sliders
		var con = contrast / 100 + contrastBoost;
		var sat = saturation / 1.25;	// max = 80
		var bri = brightness - 50;

		// init palette buffers
		resultyuv = [];

		// generate yuv colors
		var lumas = earlyLuma ?
			[0,32, 8,24,16,16,8,24,16,8,16, 8,16,24,16,24]:
			[0,32,10,20,12,16,8,24,12,8,16,10,15,24,15,20];
		var angles = [,,4, 4+8, 2, 2+8, 7+8, 7, 5, 6, 4, , , 2+8, 7+8, ];

		for (var i=0; i<16; i++) {
			var y = luma(lumas[i]);
			var u = 0;
			var v = 0;
			if (angles[i] !== undefined ) {
				u = sat * Math.cos(angle(angles[i], phase));
				v = sat * Math.sin(angle(angles[i], phase));
			}

			y *= con;
			u *= con;
			v *= con;
			y += bri;	// apply brightness and contrast

			resultyuv[i] = [y, u, v];
		}
	}

    function mouseToScreen(event) {
        // Same as the bendFS-shader but in JavaScript.
        // We need to convert display coordinates to texture coordinates through the bending
        var texCoord = {x:event.offsetX/GL.viewportWidth, y:event.offsetY/GL.viewportHeight};
        var x = 1.0 - Math.sqrt((texCoord.x-0.5)*(texCoord.x-0.5) + (texCoord.y-0.5)*(texCoord.y-0.5));
        var g = {x:0.5-texCoord.x, y:0.5-texCoord.y};
        var uv = {x:texCoord.x+g.x*x*crtBend, y:texCoord.y+g.y*x*crtBend};
        var pos = {x:Math.round(uv.x*displayWidth), y:Math.round(uv.y*displayHeight)};
        pos.x -= border[3];
        pos.y -= border[0];
        return pos;
    }

	var rf;
	function init(element) {
		initialize(element);
		createPalette();
		rf = createRenderFunctions();

        // Create a stencil for the char area
        rf.beginDrawChars();
        GL.clearStencil(0);
        GL.clear(GL.STENCIL_BUFFER_BIT);
        GL.enable(GL.STENCIL_TEST);
        GL.stencilFunc(GL.ALWAYS, 1, 0xff);
        GL.stencilOp(GL.REPLACE, GL.REPLACE, GL.REPLACE);
        for (var y=0; y<25; y++)
            for (var x=0; x<40; x++)
                rf.drawChar(0,x,y,0,0);
        GL.stencilFunc(GL.EQUAL, 1, 0xff);
        GL.stencilOp(GL.KEEP, GL.KEEP, GL.KEEP);
        GL.disable(GL.STENCIL_TEST);

        element.addEventListener("mousemove", (event) => {
            var pos = mouseToScreen(event);
            var row = Math.floor(pos.y/8);
            selection = {
                begin:row,
                end:row
            };
            postProcess();
        });
	};

	var that=this;
	externalInterface = {
		init:init,
		convertImage:convertImage,
        enableBorderMask:function(enable) {
            if (enable)
                GL.enable(GL.STENCIL_TEST);
            else
                GL.disable(GL.STENCIL_TEST);
        },
		beginDrawChars:function() {
			rf.beginDrawChars();
		},
		drawChar:function(ch,x,y,fg,bg) {
			rf.drawChar(ch,x,y,fg,bg);
		},
        drawString:function(str,x,y,fg,bg) {
            rf.beginDrawChars();
            for (var i=0; i<str.length; i++) {
                var c = str.charCodeAt(i);
                if ((c>=65) && (c<=90))
                    c-=64;
                else if ((c>=97) && (c<=122))
                    c+=256-96;
                rf.drawChar(c, x, y, fg, bg);
                x++;
            }
        },
		drawImage:function(image) {
			rf.drawImage(image);
		},
		clear:function(color) {
			rf.clear(color);
		},
		setProperty:function(name, value) {
			that[name]=value;
			createPalette();
		},
		repaint:function() {
			postProcess();
		},
        /* EventTarget implementation */
        addEventListener:function(type, listener) {
            if (eventListeners[type] === undefined)
                return false;
            if (eventListeners[type].indexOf(listener) != -1)
                return false;
            eventListeners[type].push(listener);
        },
        removeEventListener:function(type, listener) {
            if (eventListeners[type] === undefined)
                return false;
            var index = eventListeners[type].indexOf(listener);
            if (index == -1)
                return false;
            eventListeners[type].splice(index, 1);
        },
        dispatchEvent:function(event) {
            return false;
        }
	};
    return externalInterface;
})();
