import java.awt.image.*;
import java.awt.geom.*;
import static java.lang.Math.*;

public class Colodore {
	public double brightness = 50;	// 0..100
	public double contrast = 100;		// 0..100
	public double saturation = 50;	// 0..100
	public double contrastBoost = 1.0 / 5.0;	// this helps contrast to work like the "1084s" crt
	public boolean earlyLuma = false;
	public double phase = 0;			// -45 .. 45 color phase-offset
	public int sub = 256 / 4;		// 0..255 scanline brightness-shift
	public boolean chromaSubsampling = true;
	public double hanoverBarPhase = 360 / 16;		// 0..50 hanover bar phase-angle
	public boolean hanoverBars = true;
	public double delayLine	= 0.5; // 0 .. 0.5 mix chroma with previous line
	public double gammasrc = 2.8;		// 1.6 .. 2.8 PAL
	public double gammatgt = 2.2;		// 1.6 .. 2.8 sRGB
	public double scanshade = 1.0 / 3.0;	// 0 .. 0.5 scanline transparency

	private double resultyuv[][] = new double[16][3];

	private double luma(double input) {
		double factor = 256.0/32.0;
		return input * factor;
	}

	private double angle(double input, double phs) {
		double factor = 360.0 / 16.0;
		double degree = PI / 180.0;
		double rotate = factor / 2.0 + phs;
		return (input * factor + rotate) * degree;
	}

	private void createPalette() {
		double oldLumas[] = {0,32, 8,24,16,16,8,24,16,8,16, 8,16,24,16,24};
		double newLumas[] = {0,32,10,20,12,16,8,24,12,8,16,10,15,24,15,20};

		// convert percentage-values of sliders
		double con = contrast / 100.0 + contrastBoost;
		double sat = saturation / 1.25;	// max = 80
		double bri = brightness - 50.0;

		// init palette buffers
		resultyuv = new double[16][3];

		// generate yuv colors
		double lumas[] = earlyLuma ? oldLumas : newLumas;
		double angles[] = {-1, -1, 4, 4+8, 2, 2+8, 7+8, 7, 5, 6, 4, -1, -1, 2+8, 7+8, -1};

		for (int i=0; i<16; i++) {
			double y = luma(lumas[i]);
			double u = 0;
			double v = 0;
			if (angles[i] != -1) {
				u = sat * cos(angle(angles[i], phase));
				v = sat * sin(angle(angles[i], phase));
			}

			y *= con;
			u *= con;
			v *= con;
			y += bri;	// apply brightness and contrast

			resultyuv[i][0] = y;
			resultyuv[i][1] = u;
			resultyuv[i][2] = v;
		}
	}

	private int clamp(double value, double min, double max) {
		return (int)min(max(value,min),max);
	}

	private double clampd(double value, double min, double max) {
		return min(max(value,min),max);
	}

	/**
	 * Converts an indexed 16 color image to a YUVA image.
	 */
	private BufferedImage indexed2yuv(BufferedImage sourceImage) {
		BufferedImage destinationImage = new BufferedImage(sourceImage.getWidth(), sourceImage.getHeight(), BufferedImage.TYPE_INT_ARGB);

		byte sourcePixels[] = ((DataBufferByte)sourceImage.getRaster().getDataBuffer()).getData();
		int destinationPixels[] = ((DataBufferInt)destinationImage.getRaster().getDataBuffer()).getData();

		for (var i=0; i<sourceImage.getHeight(); i++) {
			for (var j=0; j<sourceImage.getWidth(); j++) {
				int offset = i*sourceImage.getWidth()+j;
				int index = sourcePixels[offset];
				if (index>=16)
					index=0;
				double color[] = resultyuv[index];

				destinationPixels[offset] = 
					(clamp(color[0], 0, 255)<<24) |
					(clamp(color[1]+128, 0, 255)<<16) |
					(clamp(color[2]+128, 0, 255)<<8) |
					clamp(255-color[ 0 ] + sub, 0, 255);
			}
		}

		return destinationImage;
	}

	private double yFromPixel(int pixel) {
		return (double)((pixel>>24)&0xff);
	}

	private double uFromPixel(int pixel) {
		return (double)((pixel>>16)&0xff);
	}

	private double vFromPixel(int pixel) {
		return (double)((pixel>>8)&0xff);
	}

	private double rFromPixel(int pixel) {
		return (double)((pixel>>16)&0xff);
	}

	private double gFromPixel(int pixel) {
		return (double)((pixel>>8)&0xff);
	}

	private double bFromPixel(int pixel) {
		return (double)((pixel>>0)&0xff);
	}

	private void chromaSubsample(BufferedImage image) {
		int blackU = clamp(resultyuv[0][1]+128, 0, 255);
		int blackV = clamp(resultyuv[0][2]+128, 0, 255);
		int pixels[] = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();

		for (var i=0; i<image.getHeight(); i++) {
			for (var j=0; j<image.getWidth(); j++) {
				int offset = i*image.getWidth()+j;

				int u =  clamp(
							(j-2>=0 ? uFromPixel(pixels[offset-2]) : blackU)*0.06136+
							(j-1>=0 ? uFromPixel(pixels[offset-1]) : blackU)*0.24477 +
							(uFromPixel(pixels[offset]))*.38774 +
							(j+1<image.getWidth() ? uFromPixel(pixels[offset+1]) : blackU)*0.24477 +
							(j+2<image.getWidth() ? uFromPixel(pixels[offset+2]) : blackU)*0.06136
						,0,255);

				int v =  clamp(
							(j-2>=0 ? vFromPixel(pixels[offset-2]) : blackV)*0.06136+
							(j-1>=0 ? vFromPixel(pixels[offset-1]) : blackV)*0.24477 +
							(vFromPixel(pixels[offset]))*.38774 +
							(j+1<image.getWidth() ? vFromPixel(pixels[offset+1]) : blackV)*0.24477 +
							(j+2<image.getWidth() ? vFromPixel(pixels[offset+2]) : blackV)*0.06136
						,0,255);

				pixels[offset] &= 0xff0000ff;
				pixels[offset] |= u<<16;
				pixels[offset] |= v<<8;
			}
		}
	}

	private double mix(double x0, double x1, double t) {
		return x0*t + (1.0-t)*x1;
	}

	private BufferedImage yuv2rgb(BufferedImage sourceImage) {
		int blackU = clamp(resultyuv[0][1]+128, 0, 255);
		int blackV = clamp(resultyuv[0][2]+128, 0, 255);

		BufferedImage destinationImage = new BufferedImage(sourceImage.getWidth(), sourceImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
		int sourcePixels[] = ((DataBufferInt)sourceImage.getRaster().getDataBuffer()).getData();
		int destinationPixels[] = ((DataBufferInt)destinationImage.getRaster().getDataBuffer()).getData();

		double rotV[] = new double[2];
		rotV[0] = sin(hanoverBarPhase*PI/180.0);
		rotV[1] = cos(hanoverBarPhase*PI/180.0);

		for (var i=0; i<sourceImage.getHeight(); i++) {
			for (var j=0; j<sourceImage.getWidth(); j++) {
				int offset = i*sourceImage.getWidth()+j;

				double y = yFromPixel(sourcePixels[offset])/255.0;
				double u1 = uFromPixel(sourcePixels[offset])/255.0 - 0.5;
				double u2 = (i==0 ? blackU : uFromPixel(sourcePixels[offset-sourceImage.getWidth()]))/255.0 - 0.5;
				double v1 = vFromPixel(sourcePixels[offset])/255.0 - 0.5;
				double v2 = (i==0 ? blackV : vFromPixel(sourcePixels[offset-sourceImage.getWidth()]))/255.0 - 0.5;
				double u = mix(u1, u2, delayLine);
				double v = mix(v1, v2, delayLine);

				if (hanoverBars)
					v = (i&1)==0 ? v : u*rotV[0]+v*rotV[1];

        		double r = y*1.000 + u*0.000 + v*1.140;
				double g = y*1.000 - u*0.396 - v*0.581;
        	    double b = y*1.000 + u*2.029 + v*0.000;

				r = clampd(pow(r, gammasrc), 0.0, 1.0);
				g = clampd(pow(g, gammasrc), 0.0, 1.0);
				b = clampd(pow(b, gammasrc), 0.0, 1.0);

				r = clampd(pow(r, 1.0/gammatgt), 0.0, 1.0);
				g = clampd(pow(g, 1.0/gammatgt), 0.0, 1.0);
				b = clampd(pow(b, 1.0/gammatgt), 0.0, 1.0);

        	    int ri = (int)Math.round(r*255.0);
        	    int gi = (int)Math.round(g*255.0);
        	    int bi = (int)Math.round(b*255.0);

        	    destinationPixels[offset] = (0xff<<24) | (ri<<16) | (gi<<8) | bi;
			}
		}

		return destinationImage;
	}

	private void applyScanlines(BufferedImage image, BufferedImage yuvImage) {
		int yuvPixels[] = ((DataBufferInt)yuvImage.getRaster().getDataBuffer()).getData();
		int pixels[] = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
		for (var i=0; i<image.getHeight(); i++) {
			for (var j=0; j<image.getWidth(); j++) {
				int offset = i*image.getWidth()+j;
				int yuvOffset = (i/3)*yuvImage.getWidth() + j/3;

				double r = rFromPixel(pixels[offset])/255.0;
				double g = gFromPixel(pixels[offset])/255.0;
				double b = bFromPixel(pixels[offset])/255.0;

				if ((i%3)!=1) {
					double lumaInverted = (yuvPixels[yuvOffset]&0xff)/255.0;
					double a = scanshade*lumaInverted;
					r -= r*a;
					g -= g*a;
					b -= b*a;
				}

        	    int ri = (int)Math.round(r*255.0);
        	    int gi = (int)Math.round(g*255.0);
        	    int bi = (int)Math.round(b*255.0);
        	    pixels[offset] = (0xff<<24) | (ri<<16) | (gi<<8) | bi;
			}
		}
	}	

	public BufferedImage apply(BufferedImage sourceImage) {
		createPalette();
		BufferedImage yuvImage = indexed2yuv(sourceImage);

		if (chromaSubsampling)
			chromaSubsample(yuvImage);

		BufferedImage rgbImage = yuv2rgb(yuvImage);

		BufferedImage scaled = new BufferedImage(rgbImage.getWidth()*3, rgbImage.getHeight()*3, BufferedImage.TYPE_INT_ARGB);
		AffineTransform at = new AffineTransform();
		at.scale(3.0, 3.0);
		AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		scaled = scaleOp.filter(rgbImage, scaled);

		applyScanlines(scaled, yuvImage);

		return scaled;
	}

	public Colodore() {
	}
}
