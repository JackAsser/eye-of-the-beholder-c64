#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, char** argv) {
	if (argc!=4) {
		fprintf(stderr, "Usage: frankensplit input.bin output.roml.bin output.romh.bin\n");
		exit(1);
	}

	unsigned char rom[1024*1024];
	unsigned char roml[1024*512];
	unsigned char romh[1024*512];

	FILE* f=fopen(argv[1],"rb");
	if (f==NULL) {
		fprintf(stderr, "Failed to open input (%d, %s)\n", errno, strerror(errno));
		exit(1);
	}
	if (fread(rom, sizeof(rom), 1, f) != 1) {
		fprintf(stderr, "Failed to read input (%d, %s)\n", errno, strerror(errno));
		exit(1);
	}
	fclose(f);

	for (int bank=0; bank<64; bank++) {
		int roml_offset = bank*16384 + 8192*0;
		int romh_offset = bank*16384 + 8192*1;
		int offset = bank*8192;//(bank^63)*8192;
		memcpy(roml+offset, rom+roml_offset, 8192);
		memcpy(romh+offset, rom+romh_offset, 8192);
	}

	f=fopen(argv[2],"wb");
	if (f==NULL) {
		fprintf(stderr, "Failed to open roml output (%d, %s)\n", errno, strerror(errno));
		exit(1);
	}
	if (fwrite(roml, sizeof(roml), 1, f) != 1) {
		fprintf(stderr, "Failed to write rom output (%d, %s)\n", errno, strerror(errno));
		exit(1);
	}
	fclose(f);

	f=fopen(argv[3],"wb");
	if (f==NULL) {
		fprintf(stderr, "Failed to open romh output (%d, %s)\n", errno, strerror(errno));
		exit(1);
	}
	if (fwrite(romh, sizeof(romh), 1, f) != 1) {
		fprintf(stderr, "Failed to write romh output (%d, %s)\n", errno, strerror(errno));
		exit(1);
	}
	fclose(f);

	return 0;
}