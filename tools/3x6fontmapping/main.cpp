#include <stdio.h>
#include <string.h>

int main(void) {
	const char font[] = {
		'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
		0x27,
		' ',
		'*','+',',','-','.','/',
		'0','1','2','3','4','5','6','7','8','9',
		':',';',
		'='};
/*
	const char font[] = {
		'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
		'0','1','2','3','4','5','6','7','8','9',
		' '};
*/
	unsigned char ascii[128];
	memset(ascii, 0, sizeof(ascii));
	for (int i=0; i<sizeof(font); i++) {
		char c = font[i];
		ascii[(int)c] = (unsigned char)i;
		if ((c>='A') && (c<='Z'))
			ascii[(int)(c-'A'+'a')] = (unsigned char)i;
	}
	for (int i=0; i<sizeof(font); i++) {
		if (ascii[i]==0)
			ascii[i]=ascii[' '];
	}

	for (int i=0; i<sizeof(ascii); i++) {
		if ((i&15) == 0)
			printf(".byte ");

		printf("$%02x", ascii[i]);

		if ((i&15) == 15)
			printf("\n");
		else
			printf(", ");
	}

	return 0;
}