.include "global.inc"

position_lo:
	.byte <position0
	.byte <position1
	.byte <position2
	.byte <position3
	.byte <position4
	.byte <position5
	.byte <position6
	.byte <position7
	.byte <position8
	.byte <position9

position_hi:
	.byte >position0
	.byte >position1
	.byte >position2
	.byte >position3
	.byte >position4
	.byte >position5
	.byte >position6
	.byte >position7
	.byte >position8
	.byte >position9

mul6:
	.repeat 40,I
		.byte I*6
	.endrep

.proc getDecorationFlags
	decorationIndex = ARGS+1 ;0..x
	ldx decorationIndex
	lda decorationFlags,x
	sta TMP
	rts
.endproc

.proc checkUpdateTriggerRect
	lda SCANINDEX
	cmp #13
	bne :+
		lda XPOS
		sta triggerRect+0
		lda YPOS
		sta triggerRect+1
		lda WIDTH
		sta triggerRect+2
		lda HEIGHT
		sta triggerRect+3
	:
	rts
.endproc

.proc prepare
	lda (TMP),y
	sta TMP2+0
	iny
	lda (TMP),y
	sta TMP2+1

	clc
	lda #<offsets_lo
	adc TMP2+0
	sta SRC+0
	lda #>offsets_lo
	adc TMP2+1
	sta SRC+1

	clc
	lda #<offsets_hi
	adc TMP2+0
	sta SRC2+0
	lda #>offsets_hi
	adc TMP2+1
	sta SRC2+1

	clc
	lda #<screen
	adc TMP2+0
	sta SRC3+0
	lda #>screen
	adc TMP2+1
	sta SRC3+1

	clc
	lda #<d800
	adc TMP2+0
	sta SRC4+0
	lda #>d800
	adc TMP2+1
	sta SRC4+1

	jsrf drawGfxObjectSetup

	rts
.endproc

.proc renderDecoration
	xpos = ARGS+0
	decorationIndex = ARGS+1 ;0..x
	position = ARGS+2 ;0..9

	; positionX: [width,height,xpos,ypos,offslo,offshi]*Y
	ldx position
	lda position_lo,x
	sta TMP+0
	lda position_hi,x
	sta TMP+1
	ldx decorationIndex
	ldy mul6,x
	lda (TMP),y
	bne :+
		rts
	:
	sta WIDTH
	iny
	lda (TMP),y
	sta HEIGHT
	iny
	clc
	lda (TMP),y
	adc xpos
	sta XPOS
	iny
	lda (TMP),y
	sta YPOS
	iny

	jsr checkUpdateTriggerRect

	ldx decorationIndex
	lda decorationFlags,x
	and #$80
	beq :+
		; Alternative rendering
		lda (TMP),y
		tax
		jsr loadAlternativeParameters
		jmp drawGfxObject
	:

	jsr prepare
	columnLoop:
		ldy XPOS
		cpy WALLX0
		bpl :+
			jmp doneRow
		:
		cpy WALLX1
		bmi :+
			rts
		:

		ldy #0
		lda DST_BITMAP+0
		sta DST_BITMAP_USE+0
		lda DST_BITMAP+1
		sta DST_BITMAP_USE+1
		rowLoop:
			lda (SRC2),y
			beq skipChar
				sta TMP+1
				lda (SRC),y
				sta TMP+0
				lda (SRC4),y
				sta (DST_D800),y
				lda (SRC3),y
				sta (DST_SCREEN),y
				sty YREG
				.repeat 8,I
					.if I=0
						ldy #0
					.else
						iny
					.endif
					lda (TMP),y
					sta (DST_BITMAP_USE),y
				.endrep
				ldy YREG
			skipChar:

			iny
			cpy HEIGHT
			beq doneRow

			clc
			lda DST_BITMAP_USE+0
			adc #8
			sta DST_BITMAP_USE+0
			bcc :+
				inc DST_BITMAP_USE+1
				clc
			:
		jmp rowLoop
		doneRow:
		inc XPOS
		dec WIDTH
		bne :+
			rts
		:

		clc
		lda SRC+0
		adc HEIGHT
		sta SRC+0
		bcc :+
			inc SRC+1
			clc
		:
		lda SRC2+0
		adc HEIGHT
		sta SRC2+0
		bcc :+
			inc SRC2+1
			clc
		:
		lda SRC3+0
		adc HEIGHT
		sta SRC3+0
		bcc :+
			inc SRC3+1
			clc
		:
		lda SRC4+0
		adc HEIGHT
		sta SRC4+0
		bcc :+
			inc SRC4+1
			clc
		:

		jsr forwardDestination
	jmp columnLoop
.endproc


.proc renderFlippedDecoration
	xpos = ARGS+0
	decorationIndex = ARGS+1 ;0..x
	position = ARGS+2 ;0..9

	; positionX: [width,height,xpos,ypos,offslo,offshi]*Y
	ldx position
	lda position_lo,x
	sta TMP+0
	lda position_hi,x
	sta TMP+1
	ldx decorationIndex
	ldy mul6,x
	lda (TMP),y
	bne :+
		rts
	:
	sta WIDTH
	iny
	lda (TMP),y
	sta HEIGHT
	iny
	
	sec
	lda #21
	sbc (TMP),y
	clc
	adc xpos
	sta XPOS
	pha
	iny
	lda (TMP),y
	sta YPOS
	iny

	sec
	lda XPOS
	sbc WIDTH
	clc
	adc #1
	sta XPOS
	jsr checkUpdateTriggerRect

	pla
	sta XPOS

	ldx decorationIndex
	lda decorationFlags,x
	and #$80
	beq :+
		; Alternative rendering
		lda (TMP),y
		tax
		jsr loadAlternativeParameters
		jmp drawFlippedGfxObject
	:

	jsr prepare
	columnLoop:
		ldy XPOS
		cpy WALLX0
		bpl :+
			rts
		:
		cpy WALLX1
		bmi :+
			jmp doneRow
			rts
		:

		ldy #0
		lda DST_BITMAP+0
		sta DST_BITMAP_USE+0
		lda DST_BITMAP+1
		sta DST_BITMAP_USE+1
		rowLoop:
			lda (SRC2),y
			beq skipChar
				sta TMP+1
				lda (SRC),y
				sta TMP+0
				lda (SRC4),y
				sta (DST_D800),y
				lda (SRC3),y
				sta (DST_SCREEN),y
				sty YREG
				.repeat 8,I
					.if I=0
						ldy #0
					.else
						iny
					.endif
					lax (TMP),y
					lda fliptab,x
					sta (DST_BITMAP_USE),y
				.endrep
				ldy YREG
			skipChar:

			iny
			cpy HEIGHT
			beq doneRow

			clc
			lda DST_BITMAP_USE+0
			adc #8
			sta DST_BITMAP_USE+0
			bcc :+
				inc DST_BITMAP_USE+1
				clc
			:
		jmp rowLoop
		doneRow:
		dec XPOS
		dec WIDTH
		bne :+
			rts
		:

		clc
		lda SRC+0
		adc HEIGHT
		sta SRC+0
		bcc :+
			inc SRC+1
			clc
		:
		lda SRC2+0
		adc HEIGHT
		sta SRC2+0
		bcc :+
			inc SRC2+1
			clc
		:
		lda SRC3+0
		adc HEIGHT
		sta SRC3+0
		bcc :+
			inc SRC3+1
			clc
		:
		lda SRC4+0
		adc HEIGHT
		sta SRC4+0
		bcc :+
			inc SRC4+1
		:

		jsr backwardDestination
	jmp columnLoop
.endproc

.proc loadAlternativeParameters
	lda _data_ptr_lo,x
	sta SRC_DATA+0	
	lda _data_ptr_hi,x
	sta SRC_DATA+1

	lda _char_offset_lo,x
	sta TMP+0
	lda _char_offset_hi,x
	sta TMP+1

	clc
	lda #<alt_screen
	adc TMP+0
	sta SRC_SCREEN+0
	lda #>alt_screen
	adc TMP+1
	sta SRC_SCREEN+1

	clc
	lda #<alt_d800
	adc TMP+0
	sta SRC_D800+0
	lda #>alt_d800
	adc TMP+1
	sta SRC_D800+1

	rts
.endproc
