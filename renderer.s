.include "global.inc"

.feature force_range

.segment "RENDERER_BSS"
.export wallsetBackgroundBank
wallsetBackgroundBank: .res 1
.export wallsetBackgroundOffset
wallsetBackgroundOffset: .res 2

.export decorationBank
decorationBank:		.res 1

.export decorationOffset
decorationOffset:	.res 6

.export doorBanks
doorBanks:		.res 2
.export doorOffsets
doorOffsets:	.res 2*2

flipBackground:		.res 1

.segment "BSS"
scannedMazeBlocks: 	.res 18*4
scannedMonsterCounts:	.res 18
.export scannedMazePositionsLo
scannedMazePositionsLo:	.res 18
.export scannedMazePositionsHi
scannedMazePositionsHi:	.res 18
scannedTopItemPtrsLo:	.res 18
scannedTopItemPtrsHi:	.res 18
scanIndexToWallSides:	.res 18*2

visibleWallXInterval0: .res 18
visibleWallXInterval1: .res 18
noMonsters: .res 1

.export relativeDirectionSouth
relativeDirectionSouth:	.res 1
relativeDirectionEast: .res 1
relativeDirectionWest: .res 1

.segment "MEMCODE_RW"
renderDecorations: .byte 1

; c=0:normal
; c=1:flipped
; x=picture 0/1
; DRAW_MONSTER_HIT
.proc drawMonster
	lda BANK
	pha

	lda monsterBanks,x
	sta BANK
	sta $de00

	bcc flippedMonster
		normalMonster:
		cpx #0
		bne :+
mon0normal:		jsr $dead
			jmp monsterDone
		:
mon1normal:		jsr $dead
			jmp monsterDone

		flippedMonster:
		cpx #0
		bne :+
mon0flipped:		jsr $dead+3
			jmp monsterDone
		:
mon1flipped:		jsr $dead+3

	monsterDone:
	pla
	sta BANK
	sta $de00
	rts
monsterBanks: .byte 0,0
.endproc
.export mon0flipped=drawMonster::mon0flipped
.export mon0normal=drawMonster::mon0normal
.export mon1flipped=drawMonster::mon1flipped
.export mon1normal=drawMonster::mon1normal
.export monsterBanks=drawMonster::monsterBanks

; c=0:normal
; c=1:flipped
; x=type
.proc drawWall
	lda BANK
	pha

	lda wallsetSet,x
	sta ARGS+2

	lda wallsetBank,x
	sta BANK
	sta $de00
	lda wallsetDrawLo,x
	bcc :+
		adc #3-1
	:
	sta sm+1
	lda wallsetDrawHi,x
	adc #0
	sta sm+2
	sm:jsr $dead

	pla
	sta BANK
	sta $de00
	rts

wallsetSet:   .byte 0,1,2,0,1,2,3
wallsetBank:  .byte 0,0,0,0,0,0,0
wallsetDrawLo:.byte 0,0,0,0,0,0,0
wallsetDrawHi:.byte 0,0,0,0,0,0,0
.endproc
.export wallsetBank=drawWall::wallsetBank
.export wallsetDrawLo=drawWall::wallsetDrawLo
.export wallsetDrawHi=drawWall::wallsetDrawHi

.segment "RENDERER"

relativeDirectionsSouth:
	.byte SOUTH, WEST,  NORTH, EAST
relativeDirectionsEast:
	.byte EAST,  SOUTH, WEST,  NORTH
relativeDirectionsWest:
	.byte WEST,  NORTH, EAST,  SOUTH


; Scan Indicdes (North)
; 00 01 02 03 04 05 06
;    07 08 09 10 11
;       12 13 14
;       15 16 17

; Scan Index Even/Odd table (used for floor flipped decorations)
; 01 00 01 00 01 00 01
;    01 00 01 00 01
;       01 00 01
;       00 01 00
scanIndexEvenOdd: .byte 1,0,1,0,1,0,1,  1,0,1,0,1,  1,0,1,  0,1,0

							;  00     01     02     03     04     05     06       07     08     09     10     11       12     13     14       15      16    17 (Scan Index)
mazeScanOffsetsNorth:	.word -99*4, -98*4, -97*4, -96*4, -95*4, -94*4, -93*4,   -66*4, -65*4, -64*4, -63*4, -62*4,   -33*4, -32*4, -31*4,    -1*4,   0*4,   1*4
mazeScanOffsetsEast:	.word -93*4, -61*4, -29*4,   3*4,  35*4,  67*4,  99*4,   -62*4, -30*4,   2*4,  34*4,  66*4,   -31*4,   1*4,  33*4,   -32*4,   0*4,  32*4
mazeScanOffsetsSouth:	.word  99*4,  98*4,  97*4,  96*4,  95*4,  94*4,  93*4,    66*4,  65*4,  64*4,  63*4,  62*4,    33*4,  32*4,  31*4,     1*4,   0*4,  -1*4
mazeScanOffsetsWest:	.word  93*4,  61*4,  29*4,  -3*4, -35*4, -67*4, -99*4,    62*4,  30*4,  -2*4, -34*4, -66*4,    31*4,  -1*4, -33*4,    32*4,   0*4, -32*4
mazeScanOffsets_lo:
	.byte <mazeScanOffsetsNorth, <mazeScanOffsetsEast, <mazeScanOffsetsSouth, <mazeScanOffsetsWest
mazeScanOffsets_hi:
	.byte >mazeScanOffsetsNorth, >mazeScanOffsetsEast, >mazeScanOffsetsSouth, >mazeScanOffsetsWest

backToFrontScanTable:
	.byte 0, 6, 1, 5, 2, 4, 3, 7, 11, 8, 10, 9, 12, 14, 13, 15, 17, 16

mul5:	.repeat 18,I
		.byte I*5
	.endrep

objectScanPositionDeltasX:
	.byte -111, -95,  -139, -117, -120
	.byte -76,  -60,  -95,  -74,  -80 
	.byte -43,  -27,  -53,  -31,  -40 
	.byte -8,    8,   -10,   10,   0  
	.byte  27,   43,   31,   53,   40 
	.byte  60,   76,   74,   95,   80 
	.byte  95,   111,  117,  139,  120
	.byte -118, -92,  -152, -120, -118
	.byte -66,  -40,  -84,  -51,  -59 
	.byte -13,   13,  -16,   16,   0  
	.byte  40,   66,   51,   84,   59 
	.byte  92,   118,  120, 152,   118
	.byte -110, -67,  -140, -83,  -98 
	.byte -22,   22,  -27,   27,   0  
	.byte  67,   110,  83,  140,   98 
	.byte -128,  128, -128, 128,  -128
	.byte -38,   38,  -38,   38,   0 
	.byte -128,  128, -128, 128,   128

objectScanPositionDeltasY:
	.byte -63, -63, -59, -59, -61
	.byte -63, -63, -59, -59, -61
	.byte -63, -63, -59, -59, -61
	.byte -63, -63, -59, -59, -61
	.byte -63, -63, -59, -59, -61
	.byte -63, -63, -59, -59, -61
	.byte -63, -63, -59, -59, -61
	.byte -53, -53, -45, -45, -50
	.byte -53, -53, -45, -45, -50
	.byte -53, -53, -45, -45, -50
	.byte -53, -53, -45, -45, -50
	.byte -53, -53, -45, -45, -50
	.byte -35, -35, -22, -22, -30
	.byte -35, -35, -22, -22, -30
	.byte -35, -35, -22, -22, -30
	.byte -4,  -4,  -66, -66,  0
	.byte -4,  -4,  -66, -66,  0
	.byte -4,  -4,  -66, -66,  0 

.export toggleRenderDecorations
.proc toggleRenderDecorations
	lda renderDecorations
	eor #1
	sta renderDecorations
	inc SHOULDRENDER
	jsr render
	rts
.endproc

.proc renderDungeon
	jsrf clearFrameBuffer

	lda #0
	sta WALLX0
	lda #22
	sta WALLX1

	jsr renderBackgroundAndWalls
	jsr renderObjectLayer

	rts
.endproc

.export render
.proc render
	lda #0
	sta SHOULDRENDER
	sta noMonsters
	lda #$ff
	sta triggerRect+0

	jsr renderDungeon

show:
	lda restoreFromPortalAnimation
	beq :+
		lda #0
		sta restoreFromPortalAnimation
		ldx #$18
		stx spriteXbase
		jsrf clearSprites
	:
	jsrf showFrameBuffer
	ldx #SPRITE_COL0
	stx dungeonSpriteColorD025
	ldx #SPRITE_COL2
	stx dungeonSpriteColorD026
	ldx #SPRITE_COL1
	lda visibleTeleporters
	beq :+
		ldx #4
	:
	stx dungeonSpriteColors

	rts
.if 0
	lda POINTER_ITEM+0
	ora POINTER_ITEM+1
	beq :+
		ldy #Item::picture
		;lda (POINTER_ITEM),y
		jsr lda_POINTER_ITEM_y
		sta ICONINDEX
		ldx #0
		ldy #0
		sty ARGS+0
		jsrf drawIcon
	:

	rts
.endif
.endproc
.export renderShow = render::show


.proc scanMaze
	lda #-1
	sta scanIndexToWallSides+0
	sta scanIndexToWallSides+1
	sta scanIndexToWallSides+7
	sta scanIndexToWallSides+12
	sta scanIndexToWallSides+13
	sta scanIndexToWallSides+19
	sta scanIndexToWallSides+27
	sta scanIndexToWallSides+30
	sta scanIndexToWallSides+33
	sta scanIndexToWallSides+34

	ldx partyDirection
	lda relativeDirectionsSouth,x
	sta relativeDirectionSouth
	sta scanIndexToWallSides+2
	sta scanIndexToWallSides+4
	sta scanIndexToWallSides+6
	sta scanIndexToWallSides+8
	sta scanIndexToWallSides+10
	sta scanIndexToWallSides+14
	sta scanIndexToWallSides+16
	sta scanIndexToWallSides+18
	sta scanIndexToWallSides+20
	sta scanIndexToWallSides+22
	sta scanIndexToWallSides+24
	sta scanIndexToWallSides+26
	sta scanIndexToWallSides+28
	sta scanIndexToWallSides+32
	lda relativeDirectionsEast,x
	sta relativeDirectionEast
	sta scanIndexToWallSides+3
	sta scanIndexToWallSides+5
	sta scanIndexToWallSides+15
	sta scanIndexToWallSides+17
	sta scanIndexToWallSides+25
	sta scanIndexToWallSides+31
	lda relativeDirectionsWest,x
	sta relativeDirectionWest
	sta scanIndexToWallSides+9
	sta scanIndexToWallSides+11
	sta scanIndexToWallSides+21
	sta scanIndexToWallSides+23
	sta scanIndexToWallSides+29
	sta scanIndexToWallSides+35

	lda mazeScanOffsets_lo,x
	sta SRC+0
	lda mazeScanOffsets_hi,x
	sta SRC+1

	; Scan walls

	; TMP = partyPosition*4
	lda partyPosition+1
	sta TMP+1
	lda partyPosition+0
	asl
	rol TMP+1
	asl
	rol TMP+1
	sta TMP+0

	ldx #0
	scanLoop:
		txa
		lsr
		tay

		clc
		lda (SRC),y
		adc TMP+0
		sta SRC2+0
		iny
		lda (SRC),y
		adc TMP+1
		and #$0f
		sta SRC2+1

		clc
		lda SRC2+0
		adc #<maze
		sta SRC2+0
		lda SRC2+1
		adc #>maze
		sta SRC2+1

		ldy #0
		lda (SRC2),y
		sta scannedMazeBlocks,x
		inx
		iny
		lda (SRC2),y
		sta scannedMazeBlocks,x
		inx
		iny
		lda (SRC2),y
		sta scannedMazeBlocks,x
		inx
		iny
		lda (SRC2),y
		sta scannedMazeBlocks,x

		inx
		cpx #18*4
	bne scanLoop

	; Scan monster counts
	ldx #0
	scanLoop_monsterCounts:
		txa
		asl
		tay

		; TMP = mazeScanOffsets[i*2] / 4
		lda (SRC),y
		sta TMP+0
		iny
		lda (SRC),y
		lsr
		ror TMP+0
		lsr
		ror TMP+0
		sta TMP+1

		; TMP += partyPosition
		; TMP &= $3ff
		clc
		lda TMP+0
		adc partyPosition+0
		sta TMP+0
		sta scannedMazePositionsLo,x
		lda TMP+1
		adc partyPosition+1
		and #$03
		sta TMP+1
		sta scannedMazePositionsHi,x

		; scannedTriggerIndices[x] = TMP+triggerIndices
		txa
		asl
		tay
		clc
		lda TMP+0
		adc #<triggerIndices
		sta SCANNED_TRIGGER_INCIDES,y
		iny
		lda TMP+1
		adc #>triggerIndices
		sta SCANNED_TRIGGER_INCIDES,y

		; SRC2 = TMP+triggerBitsAndMonsterCount
		clc
		lda TMP+0
		adc #<triggerBitsAndMonsterCount
		sta SRC2+0
		lda TMP+1
		adc #>triggerBitsAndMonsterCount
		sta SRC2+1

		ldy #0
		lda (SRC2),y
		and #7
		sta scannedMonsterCounts,x

		inx
		cpx #18
	bne scanLoop_monsterCounts

	; TMP2 = partyPosition*2
	lda partyPosition+1
	sta TMP2+1
	lda partyPosition+0
	asl
	rol TMP2+1
	sta TMP2+0

	; Scan topItemPtrs
	ldx #0
	scanLoop_topItemPtrs:
		txa
		asl
		tay

		; TMP = mazeScanOffsets[i*2] / 2
		lda (SRC),y
		sta TMP+0
		iny
		lda (SRC),y
		lsr
		ror TMP+0
		sta TMP+1

		; TMP += partyPosition{TMP2}
		; TMP &= $7ff
		clc
		lda TMP+0
		adc TMP2+0
		sta TMP+0
		lda TMP+1
		adc TMP2+1
		and #$07
		sta TMP+1

		; TMP += topItemPtrs
		clc
		lda TMP+0
		adc #<topItemPtrs
		sta TMP+0
		lda TMP+1
		adc #>topItemPtrs
		sta TMP+1

		ldy #0
		lda (TMP),y
		sta scannedTopItemPtrsLo,x
		iny
		lda (TMP),y
		sta scannedTopItemPtrsHi,x

		inx
		cpx #18
	bne scanLoop_topItemPtrs

	lda #<-1
	ldx #17
	:
		sta visibleWallXInterval0,x
		sta visibleWallXInterval1,x
		dex
	bpl :-

	rts
.endproc

; Impl in render_door.s
;	   xpos = ARGS+0
;	   ypos = ARGS+1
;	   zoom = ARGS+2 ;0..2
;	doorpos = ARGS+3 ;0..4 5=jammed
.proc drawDoor ;y = type 0/1
	lda #>(return-1)
	pha
	lda #<(return-1)
	pha
	lda BANK
	pha
	lda #>(jsrf_impl-1)
	pha
	lda #<(jsrf_impl-1)
	pha
	lda doorOffsets+2,y
	pha
	lda doorOffsets+0,y
	pha
	lda doorBanks,y
	pha
	jmp jsrf_impl
	return:
	rts
.endproc

; Impl in render_decoration.s
;	   xpos = ARGS+0
;	   decorationIndex = ARGS+1 ;0..x
;	   position = ARGS+2 ;0..9
.proc drawDecoration
	lda #>(return-1)
	pha
	lda #<(return-1)
	pha
	lda BANK
	pha
	lda #>(jsrf_impl-1)
	pha
	lda #<(jsrf_impl-1)
	pha
	lda decorationOffset+1
	pha
	lda decorationOffset+0
	pha
	lda decorationBank
	pha
	jmp jsrf_impl
	return:
	rts
.endproc

; Impl in render_decoration.s
;	   xpos = ARGS+0
;	   decorationIndex = ARGS+1 ;0..x
;	   position = ARGS+2 ;0..9
.proc drawFlippedDecoration
	lda #>(return-1)
	pha
	lda #<(return-1)
	pha
	lda BANK
	pha
	lda #>(jsrf_impl-1)
	pha
	lda #<(jsrf_impl-1)
	pha
	lda decorationOffset+3
	pha
	lda decorationOffset+2
	pha
	lda decorationBank
	pha
	jmp jsrf_impl
	return:
	rts
.endproc

; Impl in render_decoration.s
;	   decorationIndex = ARGS+1 ;0..x
.proc getDecorationFlags
	lda #>(return-1)
	pha
	lda #<(return-1)
	pha
	lda BANK
	pha
	lda #>(jsrf_impl-1)
	pha
	lda #<(jsrf_impl-1)
	pha
	lda decorationOffset+5
	pha
	lda decorationOffset+4
	pha
	lda decorationBank
	pha
	jmp jsrf_impl
	return:
	rts
.endproc

.pushseg
.segment "MEMCODE_RO"
.proc _drawBackground
	lda BANK
	pha
	lda wallsetBackgroundBank
	sta BANK
	sta $de00

	lda wallsetBackgroundOffset+0
	sta TMP+0
	lda wallsetBackgroundOffset+1
	sta TMP+1
	ldy #0

	lda flipBackground
	beq :+
		lda (TMP),y
		sta drawBackground_src_screen+0
		iny
		lda (TMP),y
		sta drawBackground_src_screen+1
		iny
		lda (TMP),y
		sta drawBackground_src_d800+0
		iny
		lda (TMP),y
		sta drawBackground_src_d800+1
		iny
		lda (TMP),y
		sta SRC_DATA+0
		iny
		lda (TMP),y
		sta SRC_DATA+1
		jsr drawBackground
		jmp :++
	:
		lda (TMP),y
		sta drawFlippedBackground_src_screen+0
		iny
		lda (TMP),y
		sta drawFlippedBackground_src_screen+1
		iny
		lda (TMP),y
		sta drawFlippedBackground_src_d800+0
		iny
		lda (TMP),y
		sta drawFlippedBackground_src_d800+1
		ldy #6
		lda (TMP),y
		sta SRC_DATA+0
		iny
		lda (TMP),y
		sta SRC_DATA+1
		jsr drawFlippedBackground
	:

	pla
	sta BANK
	sta $de00
	rts
.endproc
.popseg

; y = wallMappingIndex
.macro _drawWall xpos,ypos,index
.scope
	ldx wallType,y
	beq ___skip
		dex
		lda #xpos
		sta XPOS
		lda #ypos
		sta YPOS
		lda #index
		sta ARGS+3
		clc
		jsr drawWall
	___skip:
	.endscope
.endmacro

; y = wallMappingIndex
.macro _drawFlippedWall xpos,ypos,index
	.scope
	ldx wallType,y
	beq ___skip
		dex
		lda #xpos
		sta XPOS
		lda #ypos
		sta YPOS
		lda #index
		sta ARGS+3
		sec
		jsr drawWall
	___skip:
	.endscope
.endmacro

.macro isSpanVisible x0, x1
	.assert x1-x0<17,error,"Span must not be wider than 17"

;	lda frameBuffer_screen+15*x0+5
;	beq :+
;	lda frameBuffer_screen+15*x1+5
;	bne :++
;	:

	ldx #<(x1-x0)
	:
		ldy mul15,x
		lda frameBuffer_screen+15*x0+5,y
		beq :+
		dex
	bpl :-
	jmp :++
	:
.endmacro

mul15:
.repeat 17,I
	.byte I*15
.endrep

.proc renderBackgroundAndWalls
	jsr scanMaze

	; flipBackground = ((partyPosition/32)+(partyPosition&31)+partyDirection)&1;
	lda partyPosition
	lsr
	lsr
	lsr
	lsr
	lsr
	eor partyPosition
	eor partyDirection
	and #1
	sta flipBackground

	jsr draw0
	jsr draw1
	jsr draw2
	jsr draw3
	jsr draw4
	jsr draw5
	jsr draw6
	jsr draw7
	jmp _drawBackground

	; --------------------------------------------------
draw7:
	isSpanVisible 0,0
		ldx relativeDirectionEast
		ldy scannedMazeBlocks+0,x
		beq :+
			_drawWall -2,3,4
	:

	isSpanVisible 21,21
		ldx relativeDirectionWest
		ldy scannedMazeBlocks+$18,x
		beq :+
			_drawFlippedWall 21,3,4
	:

	ldx relativeDirectionEast
	lda scannedMazeBlocks+$04,x
	sta wmi_a
	ldx relativeDirectionSouth
	lda scannedMazeBlocks+$08,x
	sta wmi_b
	;if (!wallMappingIsDoor(wmi_a) && (GameState.inf.wallMappings.get(wmi_b).flags & 8) == 0)
	ldy wmi_a
	jsr wallMappingIsDoor
	bne :+
		ldy wmi_b
		lda wallFlags,y
		and #8
		bne :+
			ldy wmi_a
			_drawWall 2,3,4
			jmp :++
	:
	;else if ((wmi_a != 0) && ((GameState.inf.wallMappings.get(wmi_b).flags & 8)!=0))
		lda wmi_a
		beq :+
			ldy wmi_b
			lda wallFlags,y
			and #8
			beq :+
				_drawWall 2,3,4

	:

	ldx relativeDirectionWest
	lda scannedMazeBlocks+$14,x
	sta wmi_a
	ldx relativeDirectionSouth
	lda scannedMazeBlocks+$10,x
	sta wmi_b
	;if (!wallMappingIsDoor(wmi_a) && (GameState.inf.wallMappings.get(wmi_b).flags & 8) == 0)
	ldy wmi_a
	jsr wallMappingIsDoor
	bne :+
		ldy wmi_b
		lda wallFlags,y
		and #8
		bne :+
			ldy wmi_a
			_drawFlippedWall 17,3,4
			jmp :++
	:
	;else if ((wmi_a != 0) && ((GameState.inf.wallMappings.get(wmi_b).flags & 8)!=0))
		lda wmi_a
		beq :+
			ldy wmi_b
			lda wallFlags,y
			and #8
			beq :+
				_drawFlippedWall 17,3,4

	:
	rts

	; --------------------------------------------------
draw6:
	isSpanVisible 8,8
		ldx relativeDirectionEast
		ldy scannedMazeBlocks+$08,x
		beq :+
			_drawWall 8,3,3
	:

	isSpanVisible 13,13
		ldx relativeDirectionWest
		ldy scannedMazeBlocks+$10,x
		beq :+
			_drawFlippedWall 13,3,3
	:
	rts

	; --------------------------------------------------
draw5:
	isSpanVisible 0,1
		ldx #7*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$04,x
		jsr wallMappingIsDoor
		bne :+
			_drawWall -4,3,6
	:

	isSpanVisible 20,21
		ldx #11*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$14,x
		jsr wallMappingIsDoor
		bne :+
			_drawWall 20,3,6
	:

	isSpanVisible 2,7
		ldx #8*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$08,x
		jsr wallMappingIsDoor
		bne :+
			_drawWall 2,3,6
	:

	isSpanVisible 14,19
		ldx #10*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$10,x
		jsr wallMappingIsDoor
		bne :+
			_drawWall 14,3,6
	:

	isSpanVisible 8,13
		ldx #9*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$0c,x
		beq :+
			_drawWall 8,3,6
	:
	rts

	; --------------------------------------------------
draw4:
	isSpanVisible 0,1
		ldx relativeDirectionEast
		ldy scannedMazeBlocks+$1c,x
		beq :+
			_drawWall 0,3,5
	:

	isSpanVisible 20,21
		ldx relativeDirectionWest
		ldy scannedMazeBlocks+$2c,x
		beq :+
			_drawFlippedWall 20,3,5
	:

	isSpanVisible 6,7
		ldx relativeDirectionEast
		ldy scannedMazeBlocks+$20,x
		beq :+
			_drawWall 6,2,2
	:

	isSpanVisible 14,15
		ldx relativeDirectionWest
		ldy scannedMazeBlocks+$28,x
		beq :+
			_drawFlippedWall 14,2,2
	:
	rts

	; --------------------------------------------------
draw3:
	isSpanVisible 0,5
		ldx #12*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$20,x
		jsr wallMappingIsDoor
		bne :+
			_drawWall -4,2,7
	:

	isSpanVisible 16,21
		ldx #14*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$28,x
		jsr wallMappingIsDoor
		bne :+
			_drawWall 16,2,7
	:

	isSpanVisible 6,15
		ldx #13*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$24,x
		beq :+
			_drawWall 6,2,7
	:
	rts

	; --------------------------------------------------
draw2:
	isSpanVisible 3,5
		ldx relativeDirectionEast
		ldy scannedMazeBlocks+$30,x
		beq :+
			_drawWall 3,1,1
	:

	isSpanVisible 16,18
		ldx relativeDirectionWest
		ldy scannedMazeBlocks+$38,x
		beq :+
			_drawFlippedWall 16,1,1
	:
	rts

	; --------------------------------------------------
draw1:
	isSpanVisible 0,2
		ldx #15*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$30,x
		lda wallFlags,y
		and #8
		bne :+
			_drawWall -13,1,8
	:

	isSpanVisible 19,21
		ldx #17*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$38,x
		lda wallFlags,y
		and #8
		bne :+
			_drawWall 19,1,8 ;19..21
	:

	isSpanVisible 3,18
		ldx #16*2
		lda (SCANNED_TRIGGER_INCIDES,x)
		ora #$80
		sta (SCANNED_TRIGGER_INCIDES,x)

		ldx relativeDirectionSouth
		ldy scannedMazeBlocks+$34,x
		beq :+
			_drawWall 3,1,8 ;3..18
	:
	rts

	; --------------------------------------------------
draw0:
	ldx relativeDirectionEast
	lda scannedMazeBlocks+$3c,x
	sta wmi_a
	ldx relativeDirectionWest
	lda scannedMazeBlocks+$44,x
	sta wmi_b
	
	lda levelIndex
	;if ((GameState.levelIndex<=6) || (GameState.levelIndex>=10))
	cmp #7
	bcc notDrow ;<7
	cmp #10
	bcs notDrow ;>=10
	jmp drow
	notDrow:
		;if (GameState.levelIndex>3)
		cmp #4
		bcc doneLevelFix
			; Level 4,5,6,10,11,12
			ldx #0
			lda wmi_a
			cmp #$18
			bne :+
				stx wmi_a
			:
			lda wmi_b
			cmp #$17
			bne :+
				stx wmi_b
			:
		jmp doneLevelFix
	drow:
		; Level 7,8,9
		ldx #0
		lda wmi_a
		cmp #$17
		bne :+
			stx wmi_a
		:
		lda wmi_b
		cmp #$18
		bne :+
			stx wmi_b
		:		
	doneLevelFix:

	ldy wmi_a
	beq :+
		_drawWall 0,0,0
	:
	ldy wmi_b
	beq :+
		_drawFlippedWall 19,0,0
	:

	rts
.endproc

.proc wallMappingIsDoor
	cpy #0
	beq :+
		lda wallFlags,y
		and #8
	:
	rts
.endproc

; x = scanIndex (SCANINDEX)
.proc drawWallDecorationsOnPosition
	txa
	asl
	sta wallFrontScanIndex
	sta wallSideScanIndex
	inc wallSideScanIndex

	ldy wallSideScanIndex
	lda scanIndexToWallSides,y
	bmi skipSideDecoration
		sta TMP
		txa
		asl
		asl
		clc
		adc TMP
		tay
		ldx scannedMazeBlocks,y
		lda wallDecoration,x
		sta ARGS+1
		bmi skipSideDecoration
			ldy wallSideScanIndex
			ldx scanIndexAndSideToDecorationRectangleIndex,y
			beq skipSideDecoration
				bmi flippedSideDecoration
				normalSideDecoration:
					dex
					stx ARGS+2
					lda #0
					sta ARGS+0
					jsr drawDecoration
					jmp skipSideDecoration
				flippedSideDecoration:
					txa
					eor #$ff
					tax
					stx ARGS+2 ;rectangleIndex / position
					lda #0
					sta ARGS+0
					jsr drawFlippedDecoration

	skipSideDecoration:

	ldy wallFrontScanIndex
	lda scanIndexToWallSides,y
	bpl :+
		rts
	:
	sta TMP
	lda SCANINDEX
	asl
	asl
	clc
	adc TMP
	tay
	ldx scannedMazeBlocks,y
	lda wallDecoration,x
	sta ARGS+1
	bpl :+
		rts
	:

	jsr getDecorationFlags
	lda TMP 
	and #1
	bne flipped
	lda TMP 
	and #2
	beq normal
	lda flipBackground
	and #1
	bne flipped
	normal:
		; Normal
		ldy wallFrontScanIndex
		lda scanIndexAndSideToDecorationRectangleIndex,y
		jmp :+
	flipped:
		; Flipped
		ldy wallFrontScanIndex
		lda scanIndexAndSideToDecorationRectangleIndex,y
		eor #$ff
		clc
		adc #1
	:
	tax ; rectangleIndexWithMode
	bmi flippedFrontDecoration
	normalFrontDecoration:
		dex
		stx ARGS+2 ;rectangleIndex / position
		ldx SCANINDEX
		lda TMP
		and #4
		beq :+
			ldy mul5,x
			lda scanIndexEvenOdd,x
			ldx objectScanPositionDeltasX+4,y
			eor flipBackground
			php
			lda roundToChars,x
			plp
			beq counterFlip1 ; We are here if the decoration is flagged as flip with floor. It has already been taken care of above, but if we draw at x!=0 we need to counter flip again since we pre-blend with the floor graphics.
			jmp :++
		:
			lda decorationDeltaXBasedOnScanPosition2,x
		:
counterFlip2:
		sta ARGS+0
		jmp drawDecoration

	flippedFrontDecoration:
		txa
		eor #$ff
		tax
		stx ARGS+2 ;rectangleIndex / position
		ldx SCANINDEX
		lda TMP
		and #4
		beq :+
			ldy mul5,x
			lda scanIndexEvenOdd,x
			ldx objectScanPositionDeltasX+4,y
			eor flipBackground
			php
			lda roundToChars,x
			plp
			beq counterFlip2 ; See above
			jmp :++
		:
			lda decorationDeltaXBasedOnScanPosition2,x
		:
counterFlip1:
		sta ARGS+0
		jmp drawFlippedDecoration

scanIndexAndSideToDecorationRectangleIndex:
	.byte 1, 1, 4, 10, 4, 8, 4, 1, 4, -8, 4, -10, 0, 0, 3, 9, 3, 7, 3, 1, 3, -7, 3, -9, 2, 6, 2, 1, 2, -6, 1, 5, 1, 1, 1, -5
decorationDeltaXBasedOnScanPosition2:
	.byte -144/8, -96/8, -48/8, 0, 48/8, 96/8, 144/8, -160/8, -80/8, 0, 80/8, 160/8, -128/8, 0, 128/8, 0, 0, 0

.endproc

.pushseg
.segment "MEMCODE_RO"
.export roundToChars
roundToChars:
	; 0..127
	.repeat 128,I
		.byte (I+7)>>3
	.endrep
	; -128..-1
	.repeat 128,I
	.scope
		val=(-128+I-7)/8
		.byte <val
	.endscope
	.endrep
.popseg

; indexed by otherSubPos*4 + partyDirection
relativeSubPositions:
	.byte 0,2,2,1
	.byte 1,0,3,3
	.byte 2,3,0,0
	.byte 3,1,1,2
	.byte 4,4,4,4

; x = scanIndex
.proc drawThrownOnPosition
	.pushseg
	.segment "BSS"
		thrownPtrsLo: .res 5
		thrownPtrsHi: .res 5
		flip: .res 1
		var_8: .res 1
		tdItemIndex: .res 1
		relativeSubPosition: .res 1
	.popseg

	xpos = ARGS+0
	ypos = ARGS+1
	zoom = ARGS+2

	lda #1
	sta ARGS+4 ;let item_renderer know that we are dealing with thrown items

	lda #0
	ldy #4
	:
		sta thrownPtrsLo,y
		sta thrownPtrsHi,y
		dey
	bpl :-

	; frontWallMappingIndex = scannedMazeBlocks[scanIndex].walls[relativeDirectionSouth];
	txa
	asl
	asl
	clc
	adc relativeDirectionSouth
	tay
	ldx scannedMazeBlocks,y
	lda wallType,x
	beq :+
	lda wallFlags,x
	and WALLFLAG_SHOWITEMS
	beq :+
		rts
	:

	lda #<thrown
	sta TMP+0
	lda #>thrown
	sta TMP+1
	thrownMatchLoop:
		ldy #Thrown::active
		lda (TMP),y
		beq :+
		ldy #Thrown::position
		lda (TMP),y
		ldx SCANINDEX
		cmp scannedMazePositionsLo,x
		bne :+
		iny
		lda (TMP),y
		cmp scannedMazePositionsHi,x
		bne :+
			; Found matching thrown on scan position
			ldy #Thrown::subpos
			lda (TMP),y
			and #3
			asl
			asl
			ora partyDirection
			tax
			ldy relativeSubPositions,x
			lda TMP+0
			sta thrownPtrsLo,y
			lda TMP+1
			sta thrownPtrsHi,y
		:
		clc
		lda TMP+0
		adc #.sizeof(Thrown)
		tax
		sta TMP+0
		lda TMP+1
		adc #0
		sta TMP+1

		cmp #>(thrown+.sizeof(Thrown)*10)
		bne thrownMatchLoop
		cpx #<(thrown+.sizeof(Thrown)*10)
	bne thrownMatchLoop	

	ldy #0
	thrownRenderLoop:
		tya
		pha

		lda thrownPtrsHi,y
		bne renderThrown
		jmp continue
		renderThrown:
			sta CURRENT_THROWN+1
			lda thrownPtrsLo,y
			sta CURRENT_THROWN+0

			;int relativeSubPosition = objectRelativeSubPosition[GameState.partyDirection][currentThrown.subpos&3];
			ldy #Thrown::subpos
			lda (CURRENT_THROWN),y
			and #3
			asl
			asl
			ora partyDirection
			tax
			ldy objectRelativeSubPositions,x
			sty relativeSubPosition

			;int xpos = thrownXoffsets[scanIndex][relativeSubPosition];
			lda SCANINDEX
			asl
			asl
			ora relativeSubPosition
			tax
			lda thrownXoffsets,x
			clc
			adc #20
			sta xpos

			;int zoom = thrownZoom[scanIndex][relativeSubPosition];
			lda thrownZoom,x
			;if (zoom<0) conitnue
			bpl :+
				jmp continue
			:
			sta zoom

			;int ypos = thrownYoffsets[objectScanPositionZoom[scanIndex]][relativeSubPosition];
			ldx SCANINDEX
			ldy objectScanPositionZoom,x
			lda thrownYoffsets,y
			sta ypos

			lda #0
			sta flip

			ldy #Thrown::active
			lda (CURRENT_THROWN),y
			cmp #1
			beq isItem
			jmp isSpell

			isItem:
				;int tdItemIndex = itemPictureTo3dItemIndex[GameState.items.get(currentThrown.itemOrSpellIndex).picture];
				ldy #Thrown::itemOrSpellIndex
				lda (CURRENT_THROWN),y
				sta CUR_ITEM+0
				iny
				lda (CURRENT_THROWN),y
				sta CUR_ITEM+1
				ldy #Item::picture
				;lax (CUR_ITEM),y
				jsr lax_CUR_ITEM_y
				lda itemPictureTo3dItemIndex,x
				sta tdItemIndex

				lda #0
				sta var_8
				ldy #Thrown::direction
				lda partyDirection
				cmp (CURRENT_THROWN),y
				beq :++
					eor #2
					cmp (CURRENT_THROWN),y
					bne :+
						inc var_8
						jmp :++
					:
						dec var_8
				:

				; if ((var_8!=-1) && (tdItemIndexToThrownIndex[var_8][tdItemIndex]!=-1))
				ldx var_8
				bmi stdGfx
				clc
				lda mul45,x
				adc tdItemIndex
				tay
				lda tdItemIndexToThrownIndex,y
				bmi stdGfx
				
				thrownGfx:
				.scope
					sta ARGS+3

					;flip = thrownFlip[currentThrown.direction][currentThrown.subpos&3]==1;
					ldy #Thrown::subpos
					lda (CURRENT_THROWN),y
					and #3
					sta TMP
					ldy #Thrown::direction
					lda (CURRENT_THROWN),y
					asl
					asl
					ora TMP
					tax
					lda thrownFlip,x
					cmp #1
					bne noFlip
					flip:
						jsrf item_thrown+3
						jmp continue
					noFlip:
						jsrf item_thrown
						jmp continue
				.endscope

				stdGfx:
				.scope
					;flip = ((GameState.partyDirection+1)&3) == currentThrown.direction;
					clc
					lda partyDirection
					adc #1
					and #3
					ldy #Thrown::direction
					cmp (CURRENT_THROWN),y
					bne noFlip
					flip:
						lda tdItemIndex
						cmp #15
						bcs :+
							sta ARGS+3 ;item index
							jsrf item_iteml1+3
							jmp continue
						:
							sbc #15
							sta ARGS+3
							jsrf item_items1+3
							jmp continue
					noFlip:
						lda tdItemIndex
						cmp #15
						bcs :+
							sta ARGS+3 ;item index
							jsrf item_iteml1+0
							jmp continue
						:
							sbc #15
							sta ARGS+3
							jsrf item_items1+0
							jmp continue
				.endscope
			isSpell:
			.scope
				ldy #Thrown::spellGfxIndexOrItemType
				lda (CURRENT_THROWN),y
				sta ARGS+3

				ldy #Thrown::flags
				lda (CURRENT_THROWN),y
				and #THROWNFLAGS_CENTERED
				beq :+
					ldx SCANINDEX
					ldy mul5,x
					clc
					lda objectScanPositionDeltasX+4,y
					adc #88
					sta xpos
					lda #44
					sta ypos
					lda #2
					sta ARGS+4 ;to render centered spells
				:

				;flip = thrownFlip[currentThrown.direction][currentThrown.subpos&3]==1;
				ldy #Thrown::subpos
				lda (CURRENT_THROWN),y
				and #3
				sta TMP
				ldy #Thrown::direction
				lda (CURRENT_THROWN),y
				asl
				asl
				ora TMP
				tax
				lda thrownFlip,x
				cmp #1
				bne noFlip
				flip:
					jsrf item_thrown+3
					jmp continue
				noFlip:
					jsrf item_thrown
					jmp continue
			.endscope
		continue:
		pla
		tay
		iny
		cpy #4
		bne :+
			rts
		:
	jmp thrownRenderLoop
mul45:	.byte 0,45

thrownXoffsets:
	.byte 0, 0, 0, 0
	.byte 0, 0, 0,15
	.byte 0, 0,33,52
	.byte 0, 0,71,90
	.byte 0, 0,109,127
	.byte 0, 0,-110, 0
	.byte 0, 0, 0, 0
	.byte 0, 0, 0, 0
	.byte-3,25,-23,15
	.byte 60,88,53,91
	.byte 123,-105,-127,-89
	.byte 0, 0, 0, 0
	.byte 0,-9, 0,-33
	.byte 40,89,31,94
	.byte-118, 0,-99, 0
	.byte 0, 0, 0, 0
	.byte 16,96, 0, 0
	.byte 0, 0, 0, 0

thrownYoffsets:
	.byte 32,32,31,24
	.byte 22,22,21,21
	.byte 21,21,20,20
	.byte 12,12,100,100

thrownZoom:
	.byte -1,-1,-1,-1
	.byte -1,-1,-1,-1;-1, 3
	.byte -1,-1,-1,-1; 3, 3
	.byte -1,-1,-1,-1; 3, 3
	.byte -1,-1,-1,-1; 3, 3
	.byte -1,-1,-1,-1; 3,-1
	.byte -1,-1,-1,-1
	.byte -1,-1,-1,-1
	.byte  2, 2, 2, 2
	.byte  2, 2, 2, 2
	.byte  2, 2, 2, 2
	.byte -1,-1,-1,-1
	.byte -1, 1,-1, 1
	.byte  1, 1, 1, 1
	.byte  1,-1, 1,-1
	.byte -1,-1,-1,-1
	.byte  0, 0,-1,-1
	.byte -1,-1,-1,-1

tdItemIndexToThrownIndex:
	.byte -1, -1, -1,  0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  1, -1, -1,  3, -1,  5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
	.byte -1, -1, -1,  4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1

thrownFlip:
	.byte 0,  1,  0,  1
	.byte 0,  0,  1,  1
	.byte 1,  0,  1,  0
	.byte 1,  1,  0,  0
.endproc

; x = scanIndex
.proc drawTeleportOnPosition
	inc visibleTeleporters

	ldx SCANINDEX
	sec
	lda #2
	sbc objectScanPositionZoom,x
	bpl :+
		rts
	:
	tay
	sty YREG
	lda width,y
	sta WIDTH

	sec
	lda objectScanPositionDeltaXForCompartment,x 
	sbc teleportDeltaX,y
	sta XPOS

	jsrf getSpriteColumn

	ldx #0
	columnLoop:
		ldy XPOS
		cpy WALLX0
		bpl :+
			jmp skipColumn
		:
		cpy WALLX1
		bmi :+
			rts
		:

		lda #1
		sta affectedSpriteColumns,y

		ldy YREG
		lda teleportFlarePhase
		beq :+
			jsrf teleporter_draw_even
			jmp :++
		:
			jsrf teleporter_draw_odd
		:

		skipColumn:
		clc
		lda DST_SPRITES_USE+0
		adc #<120
		sta DST_SPRITES_USE+0
		bcc :+
			inc DST_SPRITES_USE+1
		:

		inc XPOS
		inx
		dec WIDTH
	bne columnLoop

	rts
teleportDeltaX: .byte 40/8, 28/8, 18/8
width:		.byte 10, 7, 4
.endproc


; x = scanIndex
.proc drawMonstersOnPosition
	.pushseg
	.segment "BSS"
		monsterPtrs: .res 5
		flip: .res 1
	.popseg

	; Clear monster pointers
	lda #$ff
	ldy #4
	:
		sta monsterPtrs,y
		dey
	bpl :-

	; Scan monster list for match on position and assign into monsterPtrs
	ldy #29
	monsterMatchLoop:
		lda monster_position_lo,y
		ldx SCANINDEX
		cmp scannedMazePositionsLo,x
		bne nextMonster
		lda monster_position_hi,y
		cmp scannedMazePositionsHi,x
		bne nextMonster
			sty TMP
			; Found matching monster on scan position
			lda monster_subpos,y
			asl
			asl
			ora partyDirection
			tax
			ldy relativeSubPositions,x
			lda TMP
			sta monsterPtrs,y
			tay
		nextMonster:
		dey
	bpl monsterMatchLoop

	; zoomIndex -> ARGS+2
	ldx SCANINDEX
	ldy objectScanPositionZoom,x
	lda zoomToMonsterScale,y
	sta ARGS+2

	ldy mul5,x
	tya
	clc
	adc #<objectScanPositionDeltasX
	sta DXTAB+0
	lda #0
	adc #>objectScanPositionDeltasX
	sta DXTAB+1
	tya
	clc
	adc #<objectScanPositionDeltasY
	sta DYTAB+0
	lda #0
	adc #>objectScanPositionDeltasY
	sta DYTAB+1

	; Render monsters on monster pointers
	ldx #0
	renderNextMonster:
		ldy monsterPtrs,x
		bpl monstersOnSubPos
		jmp noMonsterOnSubPos
		monstersOnSubPos:
			txa
			pha

			; Calculate gfxIndex based on monster size, relative rotation and leg state
			lda monster_legState,y
			asl
			asl
			ora partyDirection
			asl
			asl
			ora monster_direction,y
			sta TMP

			ldx monster_typeIndex,y
			lda monsterType_typeBits_lo,x
			and #MONSTER_TYPEBITS_LO_SMALLSIZE
			beq :+
				; Small monster
				ldx TMP
				lda smallMonsterGfxIndex,x
				sta ARGS+3
				lda smallMonsterFlip,x
				sta flip
				jmp :++
			:
				; Quarter or big monster
				ldx TMP
				lda bigMonsterGfxIndex,x
				sta ARGS+3
				lda bigMonsterFlip,x
				sta flip
			:

			; If close we must override the PREPARE_ATTACK and ATTACK images if needed because they are not in the gfxIndex-tables
			lda ARGS+2
			bne :++
				lda monster_state,y
				cmp #MONSTER_STATE_PREPARE_ATTACK
				bne :+
					lda #4
					sta ARGS+3
					lda #1
					sta flip
				:
				cmp #MONSTER_STATE_ATTACK
				bne :+
					lda #5
					sta ARGS+3
					lda #1
					sta flip
			:

			; Fetch position based on relative sub position
			lda monster_subpos,y
			asl
			asl
			ora partyDirection
			tax
			sty TMP
			ldy objectRelativeSubPositions,x

			lax (DXTAB),y
			lda roundToChars,x
			clc
			adc #11
			sta ARGS+0

			lda (DYTAB),y
			clc
			adc #127
			sta ARGS+1

			;if (currentMonster.type.specialYoffset==1)
			ldy TMP
			ldx monster_typeIndex,y
			lda monsterType_specialYoffset,x
			cmp #1
			bne :++
				;if ((gfxIndex==1) || (gfxIndex>3))
				;changed to gfxIndex==0 || gfxIndex>=3 due to c64 conversion having other bounding boxes
				lda ARGS+3
				;cmp #1
				beq :+
				cmp #3
				bcs :+
				jmp :++
				:
					;ypos += specialMonsterYoffset[zoomIndex];
					ldx ARGS+2
					clc
					lda ARGS+1
					adc specialMonsterYoffset,x
					sta ARGS+1
			:

			ldx ARGS+1
			lda roundToChars,x
			sta ARGS+1

			; Move Xanathar one char up
			ldy TMP
			ldx monster_typeIndex,y
			cpx #21
			bne :+
				dec ARGS+1
			:

			ldy TMP
			lda #0
			sta DRAW_MONSTER_HIT
			lda monster_flags,y
			and #MONSTER_FLAGS_HIT
			beq :+
				inc DRAW_MONSTER_HIT
				lda monster_flags,y
				and #MONSTER_FLAGS_HIT^$ff
				sta monster_flags,y
				inc SHOULDRENDER
			:

			ldx monster_picture,y
			lsr flip
			jsr drawMonster

			pla
			tax
		noMonsterOnSubPos:
		inx
		cpx #5
		bne :+
			rts
		:
	jmp renderNextMonster

; Indexed by monster.legState*16 + monster.direction*4 + partyDirection
smallMonsterGfxIndex:
	.byte 3, 1, 0, 2
	.byte 2, 3, 1, 0
	.byte 0, 2, 3, 1
	.byte 1, 0, 2, 3 ;
	.byte 3, 2, 0, 1
	.byte 1, 3, 2, 0
	.byte 0, 1, 3, 2
	.byte 2, 0, 1, 3
smallMonsterFlip:
	.byte 1, 0, 1, 1
	.byte 1, 1, 0, 1
	.byte 1, 1, 1, 0
	.byte 0, 1, 1, 1 ;
	.byte 0, 0, 0, 1
	.byte 1, 0, 0, 0
	.byte 0, 1, 0, 0
	.byte 0, 0, 1, 0
bigMonsterGfxIndex:
	.byte 3, 1, 0, 2
	.byte 2, 3, 1, 0
	.byte 0, 2, 3, 1
	.byte 1, 0, 2, 3 ;
	.byte 3, 2, 0, 1
	.byte 1, 3, 2, 0
	.byte 0, 1, 3, 2
	.byte 2, 0, 1, 3
bigMonsterFlip:
	.byte 1, 0, 1, 1
	.byte 1, 1, 0, 1
	.byte 1, 1, 1, 0
	.byte 0, 1, 1, 1 ;
	.byte 1, 0, 1, 1
	.byte 1, 1, 0, 1
	.byte 1, 1, 1, 0
	.byte 0, 1, 1, 1

specialMonsterYoffset: .byte 12,8,4,4,2,0,0

zoomToMonsterScale:
	.byte 2,1,0
.endproc

.pushseg
.segment "MEMCODE_RO"
.export objectRelativeSubPositions
objectRelativeSubPositions:
;	.byte 0, 1, 2, 3
;	.byte 2, 0, 3, 1
;	.byte 3, 2, 1, 0
;	.byte 1, 3, 0, 2

	.byte 0, 2, 3, 1
	.byte 1, 0, 2, 3
	.byte 2, 3, 1, 0
	.byte 3, 1, 0, 2
	.byte 4, 4, 4, 4
.popseg

.export renderer_drawBurningHands 
.proc renderer_drawBurningHands
	; Make sure the frame buffer is in OK condition (shared by the scratch area!)
	jsrf render

	lda #3
	sta ARGS+4
	ldx #0
	loop:
		stx XREG
		lda burningHandsGfxX,x
		sta ARGS+0
		lda burningHandsGfxY,x
		sta ARGS+1
		lda burningHandsGfxIndex,x
		sta ARGS+3
		lda burningHandsGfxFlip,x
		beq :+
			jsrf item_thrown+3
			jmp :++
		:
			jsrf item_thrown
		:
		ldx XREG
		inx
		cpx #6
	bne loop
	jsrf showFrameBuffer
	inc SHOULDRENDER
	rts

burningHandsGfxFlip:  .byte 0,1,0,1,0,1
burningHandsGfxIndex: .byte 38,38,37,37,36,36
burningHandsGfxX:     .byte 0,19,3,15,7,11
burningHandsGfxY:     .byte 8,8,7,7,7,7

.endproc

; x = scanIndex
; TOP_ITEM = topItem
.proc drawItemsOnPosition
	.pushseg
	.segment "BSS"
		_onFirstItem: 		.byte 0
		_isCompartmentItem: 	.byte 0
		_wallFlags: 		.byte 0
		_relativeSubPos: 	.byte 0
		_zoomIndex: 		.byte 0
		_doorClipScanIndex:	.byte 0
	.popseg

	topItem = TOP_ITEM

	lda #0
	sta ARGS+4 ;let item_renderer know that we are dealing with normal items

	; frontWallMappingIndex = scannedMazeBlocks[scanIndex].walls[relativeDirectionSouth];
	txa
	asl
	asl
	clc
	adc relativeDirectionSouth
	tay
	ldx scannedMazeBlocks,y

	; wallFlags = scanIndex==16 ? WallMapping.SHOWITEMS : GameState.inf.wallMappings.get(frontWallMappingIndex).flags;
	lda #WALLFLAG_SHOWITEMS
	ldy SCANINDEX
	cpy #16
	beq :+
		lda wallFlags,x
	:
	sta _wallFlags

	; if (((GameState.inf.wallMappings.get(frontWallMappingIndex).wallType) != 0) && ((wallFlags&WallMapping.SHOWITEMS)==0))
	ldy wallType,x
	beq :+
		and #WALLFLAG_SHOWITEMS
		bne :+
			rts
	:

	; curItem,endItem = topItem.previous
	ldy #Item::previous
	;lda (topItem),y
	jsr lda_TOP_ITEM_y
	sta END_ITEM+0
	sta CUR_ITEM+0
	iny
	;lda (topItem),y
	jsr lda_TOP_ITEM_y
	sta END_ITEM+1
	sta CUR_ITEM+1

	lda #1
	sta _onFirstItem
	lda #-1
	sta _doorClipScanIndex

	; DXTAB,DYTAB = &objectScanPositionDeltas[scanIndex]
	ldx SCANINDEX
	ldy mul5,x
	tya
	clc
	adc #<objectScanPositionDeltasX
	sta DXTAB+0
	lda #0
	adc #>objectScanPositionDeltasX
	sta DXTAB+1
	tya
	clc
	adc #<objectScanPositionDeltasY
	sta DYTAB+0
	lda #0
	adc #>objectScanPositionDeltasY
	sta DYTAB+1

	previousItemInPile:
		; boolean isCompartmentItem = (currentItem.subpos == 8);
		ldx #0
		ldy #Item::subpos
		;lda (CUR_ITEM),y
		jsr lda_CUR_ITEM_y
		cmp #8
		bne :+
			inx
		:
		stx _isCompartmentItem

		; if (isCompartmentItem || (currentItem.subpos<4))
		cpx #1
		beq renderItem
		cmp #4
		bcc renderItem
		jmp skipItem
renderItem:		
			ldy #-1
			sty _doorClipScanIndex

			; y = relativeSubPosition = isCompartmentItem ? 4 : objectRelativeSubPosition[GameState.partyDirection][currentItem.subpos];
			ldy _isCompartmentItem
			beq :+
				ldy #4
				jmp :++
			:
				; a = subpos
				asl
				asl
				ora partyDirection
				tax
				ldy objectRelativeSubPositions,x
			:

			lax (DXTAB),y
			lda roundToChars,x
			clc
			adc #11
			sta ARGS+0
			sty _relativeSubPos

			lda _isCompartmentItem
			beq :+
				;deltaX = objectScanPositionDeltaXForCompartment[scanIndex];
				ldx SCANINDEX
				lda objectScanPositionDeltaXForCompartment,x
				sta ARGS+0

				;deltaY = compartmentYpos[objectScanPositionZoom[scanIndex]];
				ldy objectScanPositionZoom,x
				lda compartmentYpos,y

				;relativeSubPosition = 0;
				ldy #0
				sty _relativeSubPos
				jmp :++
			:
				lda (DYTAB),y
				clc
				adc #120
			:
			tax
			lda roundToChars,x
			sta ARGS+1

			;int zoomIndex = objectSubPosZoom[objectScanPositionZoom[scanIndex]][relativeSubPosition];
			ldx SCANINDEX
			lda objectScanPositionZoom,x
			asl
			asl
			clc
			adc _relativeSubPos
			tay
			lda objectSubPosZoom,y
			sta _zoomIndex
			
			;if ((wallFlags == WallMapping.ISDOOR) && (relativeSubPosition<2) && (zoomIndex!=0))

			ldx _wallFlags
			cpx #WALLFLAG_ISDOOR
			bne :+
				ldx _relativeSubPos
				cpx #2
				bcs :+
					cmp #0
					beq :+
						dec ARGS+1
						ldy SCANINDEX
						ldx enabledDoorClipScanIndices,y
						bmi :+
							jsr calculateWallClip
			:

			lda _zoomIndex
			bmi skipItem
				sta ARGS+2 ;zoom

				ldy #Item::picture
				;lax (CUR_ITEM),y
				jsr lax_CUR_ITEM_y
				lda itemPictureTo3dItemIndex,x
				cmp #15
				bcs :+
					sta ARGS+3 ;item index
					jsrf item_iteml1+0
					jmp skipItem
				:
					sbc #15
					sta ARGS+3
					jsrf item_items1+0

skipItem:
		; curItem = curItem.previous
		ldy #Item::previous
		;lda (CUR_ITEM),y
		jsr lda_CUR_ITEM_y
		pha
		iny
		;lda (CUR_ITEM),y
		jsr lda_CUR_ITEM_y
		sta CUR_ITEM+1
		pla
		sta CUR_ITEM+0

		; rts if (CUR_ITEM == END_ITEM) && (_onFirstItem==0)
		cmp END_ITEM+0
		bne :+
		lda CUR_ITEM+1
		cmp END_ITEM+1
		bne :+
		lda _onFirstItem
		bne :+
			rts
		:
		lda #0
		sta _onFirstItem

		ldx _doorClipScanIndex
		bmi :+
			jsr calculateWallClip
		:
	jmp previousItemInPile

objectSubPosZoom:
	.byte -1,-1,-1,-1
	.byte  2, 2, 2, 2
	.byte  1, 1, 1, 1
	.byte  0, 0,-1,-1

.pushseg
.segment "MEMCODE_RO"
.export itemPictureTo3dItemIndex
itemPictureTo3dItemIndex:
	;0..13 = large
	;14..  = small
	.byte  0,   0,   0,   4,   1,   2,   3,   4,   5,   6,   7
	.byte  7,  18,   6,  15,  18,  20,  23,  19,  17,  21,  13
	.byte  9,   9,  22,  31,  12,  35,  12,  12,  12,  11,  10
	.byte 28,  28,  26,  27,  33,  29,  29,  34,  34,  34,   0
	.byte  8,   0,  37,  24,  22,  23,  22,  23,  22,  23,  25
	.byte 35,  16,  30,  36,  24,  28,  32,  18,  33,  30,  35
	.byte 28,   0,  32,  17,  17,  13,  31,   9,   9,  21,  21
	.byte 15,   5,  12,  30,  30,  -1,  -1,  -1,  -1,  -1,  27
	.byte 27
.popseg

enabledDoorClipScanIndices:
	.byte -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,    3,   -1,   -1,   -1,    9,   -1,  $B,  $C,  $D
.endproc
objectScanPositionDeltaXForCompartment:
	.byte -56/8, -8/8, 40/8, 88/8, 136/8, 184/8, 232/8, -72/8, 8/8, 88/8, 168/8, 248/8, -40/8, 88/8, 216/8, -88/8, 88/8, 264/8

compartmentYpos: .byte 37, 48, 56, 0

; x = scanIndex
.proc drawDoorOnPosition
	ldy objectScanPositionZoom,x
	cpy #3
	bne :+
		rts
	:
	sty ARGS+2
	sec
	lda #2
	sbc ARGS+2
	sta ARGS+2

	lda doorScanPositionDeltaX,x
	sta ARGS+0
	lda doorBottomY,y
	sta ARGS+1

	lda wmi_a
	cmp #30
	bcs :+
		; Not jammed, calculate door open step
		sec
		tax
		sbc wallMappingDoorStartIndex,x
		jmp :++
	:
		; Jammed
		lda #5
	:
	sta ARGS+3

	ldx wmi_a
	ldy wallMappingToDoorType,x
	jsr drawDoor

	ldx wmi_a
	lda wallMappingHasDoorKnob,x
	beq :+
		jsrf button_graphics
	:

	rts
wallMappingToDoorType:
	.byte 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1

wallMappingHasDoorKnob:
	.byte 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

wallMappingDoorStartIndex:
	.byte 0, 0, 0, 3, 3, 3, 3, 3, 8, 8, 8, 8, 8,13,13,13,13,13,18,18,18,18,18, 0, 0, 0, 0, 0, 0, 0,30,31
doorScanPositionDeltaX:
	.byte 11-15, 11-10, 11-5, 11, 11+5, 11+10, 11+15
	.byte 11-14, 11-7, 11, 11+7, 11+14
	.byte 11-12, 11, 11+12
	.byte 11-16, 11, 11+16
doorBottomY:
	.byte 8,9,12
.endproc

objectScanPositionZoom: .byte 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3

; x = scanIndex (SCANINDEX)
; return position hidden in c (set = hidden, clr = visible)
.export calculateWallClip
.proc calculateWallClip
	.pushseg
	.segment "BSS"
		_scanIndex:	.res 1
	.popseg
	stx _scanIndex

	lda visibleWallXInterval0,x
	bmi notCached
		sta WALLX0
		lda visibleWallXInterval1,x
		sta WALLX1

		lda WALLX0
		cmp WALLX1
		rts
	notCached:

	lda #0
	sta WALLX0
	lda #22
	sta WALLX1

	ldx #0
	nextScan:
		stx INNERSCAN
		txa
		asl
		asl
		clc
		adc relativeDirectionSouth
		tay
		ldx scannedMazeBlocks,y
		lda wallFlags,x
		and #WALLFLAG_ISDOOR
		bne isDoor
		isNotDoor:
			lda wallType,x
			beq continue

			ldx _scanIndex
			lda wallClipTable_lo,x
			sta TMP+0
			lda wallClipTable_hi,x
			sta TMP+1
			ldy INNERSCAN

			lda (TMP),y
			cmp #-40
			beq continue
			cmp #-41
			bne :+
				lda #22
				sta WALLX0
				lda #0
				sta WALLX1
				jmp checkBreak
			:

			lda (TMP),y
			beq :+
			bmi :+
				cmp WALLX1
				bpl :+
					sta WALLX1
			:

			lda (TMP),y
			bpl :+
				eor #$ff
				clc
				adc #1
				cmp WALLX0
				bmi :+
					sta WALLX0
			:

			jmp checkBreak
		isDoor:
			ldx _scanIndex
			lda wallClipDoorTable_lo,x
			sta TMP+0
			lda wallClipDoorTable_hi,x
			sta TMP+1
			lda INNERSCAN
			asl
			tay
			lda (TMP),y
			cmp WALLX0
			bmi :+
				sta WALLX0
			:
			iny
			lda (TMP),y
			cmp WALLX1
			bpl :+
				sta WALLX1
			:

		checkBreak:
		lda WALLX1
		cmp WALLX0
		bpl :+
			sec	; set carry when position is hidden
			rts
		:

		continue:
		ldx INNERSCAN
		inx
		cpx #18
		beq return
	jmp nextScan
return:	clc ; clear carry when position might be visible
	rts

wallClipTable:
	.byte -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -40, -41
	.byte -40, -40,   2, -40, -40, -40, -40,  -2, -41, -40, -40, -40, -41,   3, -40,  -3, -40, -40
	.byte -40,  -2, -40,   8, -40, -40, -40,  -2, -41,   6, -40, -40,  -6,   3, -40,  -3, -40, -40
	.byte -40, -40, -40, -40, -40, -40, -40, -40,  -6, -41,  16, -40,  -3, -41,  19, -40, -40, -40
	.byte -40, -40, -40, -14, -40,  20, -40, -40, -40, -16, -41,  20, -40, -19,  16, -40, -40,  19
	.byte -40, -40, -40, -40, -20, -40, -40, -40, -40, -40, -41,  20, -40, -19, -41, -40, -40,  19
	.byte -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -40, -41
	.byte -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -40, -41
	.byte -40, -40, -40, -40, -40, -40, -40, -40, -40,   6, -40, -40,  -6,   3, -40,  -3, -40, -40
	.byte -40, -40, -40, -40, -40, -40, -40, -40,  -6, -40,  16, -40,  -3, -41,  19,  -3, -40,  19
	.byte -40, -40, -40, -40, -40, -40, -40, -40, -40, -16, -40, -40, -40, -19,  16, -40, -40,  19
	.byte -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -41, -40, -41
	.byte -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40,   3, -40,  -3, -40, -40
	.byte -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40,  -3, -40,  19,  -3, -40,  19
	.byte -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -19, -40, -40, -40,  19
	.byte -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40
	.byte -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40
	.byte -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40, -40

wallClipTable_lo:
	.repeat 18,I
		.byte <(wallClipTable+I*18)
	.endrep

wallClipTable_hi:
	.repeat 18,I
		.byte >(wallClipTable+I*18)
	.endrep

wallClipDoorTable:
	.byte 22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,   0,  22,  22,   0
	.byte  0,  22,   0,  22,   0,   2,   0,  22,   0,  22,   0,  22,   0,  22,   2,  22,   0,   4,   0,  22,   0,  22,   0,  22,  22,   0,   0,   3,   0,  22,   3,  22,   0,  22,   0,  22
 	.byte  0,  22,   2,  22,   0,  22,   0,   8,   0,  22,   0,  22,   0,  22,   0,   2,  22,   0,   0,   6,   0,  22,   0,  22,   6,  22,   0,   3,   0,  22,   3,   0,   0,  22,   0,  22
 	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   6,  22,   8,  14,   0,  16,   0,  22,   3,  22,   6,  16,   0,  19,   0,  22,   0,  22,   0,  22
	.byte  0,  22,   0,  22,   0,  22,  14,  22,   0,  22,   0,  20,   0,  22,   0,  22,   0,  22,  16,  22,  22,   0,   0,  20,   0,  22,  19,  22,   0,  16,   0,  22,   0,  22,   0,  19
	.byte  0,  22,   0,  22,   0,  22,   0,  22,  20,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,  18,  22,  20,  22,   0,  22,  19,  22,  22,   0,   0,  22,   0,  22,   0,  19
	.byte 22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,   0,  22,  22,   0
	.byte 22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,   0,  22,  22,   0
	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,   6,   0,  22,   0,  22,   6,  22,   0,   3,   0,  22,   3,  22,   0,  22,   0,  22
	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   6,  22,   0,  22,   0,  16,   0,  22,   3,  22,   7,  15,   0,  19,   3,  22,   0,  22,   0,  19
	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,  16,  22,   0,  22,   0,  22,   0,  22,  19,  22,   0,  16,   0,  22,   0,  22,   0,  19
	.byte 22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,   0,  22,  22,   0
	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,   3,   0,  22,   3,  22,   0,  22,   0,  22
	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   3,  22,   0,  22,   0,  19,   3,  22,   0,  22,   0,  19
	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,  19,  22,   0,  22,   0,  22,   0,  22,   0,  19
	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22
	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22
	.byte  0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22,   0,  22

wallClipDoorTable_lo:
	.repeat 18,I
		.byte <(wallClipDoorTable+I*18*2)
	.endrep

wallClipDoorTable_hi:
	.repeat 18,I
		.byte >(wallClipDoorTable+I*18*2)
	.endrep

.endproc

.proc renderObjectLayer
	lda #0
	sta visibleTeleporters

	ldy #17
	sty SCAN
	nextPosition:
		ldy SCAN
		ldx backToFrontScanTable,y
		stx SCANINDEX

		; Calculate wallclip
		jsr calculateWallClip
		ldx SCANINDEX
		lda WALLX0
		sta visibleWallXInterval0,x
		lda WALLX1
		sta visibleWallXInterval1,x
		bcs positionHidden

		; Draw decorations on position
		lda renderDecorations
		beq :+
			jsr drawWallDecorationsOnPosition
		:

		; Draw items on position
		ldx SCANINDEX
		lda scannedTopItemPtrsLo,x
		sta TOP_ITEM+0
		lda scannedTopItemPtrsHi,x
		sta TOP_ITEM+1
		ora TOP_ITEM+0
		beq :+
			jsr drawItemsOnPosition
		:

		; Get front wallmapping
		ldx SCANINDEX
		txa
		asl
		asl
		clc
		adc relativeDirectionSouth
		tax
		ldy scannedMazeBlocks,x
		sty wmi_a

		; Draw monsters and doors
		ldx SCANINDEX
		cpx #15
		bcs skipDoorsAndMonsters
			lda wallFlags,y
			and #WALLFLAG_ISDOOR
			beq :+
				ldx SCANINDEX
				jsr drawDoorOnPosition
			:

			; Draw monsters
			ldx SCANINDEX
			lda scannedMonsterCounts,x
			beq :+
				lda noMonsters
				bne :+
					jsr drawMonstersOnPosition
			:
		skipDoorsAndMonsters:

		; Draw thrown on position
		jsr drawThrownOnPosition

		; Draw teleport on position
		lda #$34
		cmp wmi_a
		bne :+
			jsr drawTeleportOnPosition
		:

		positionHidden:
		dec SCAN
	bpl nextPosition
	rts

backToFrontScanTable: .byte 16, 17, 15, 13, 14, 12, 9, 10, 8, 11, 7, 3, 4, 2, 5, 1, 6, 0
.endproc

.export render_bestiary
.proc render_bestiary
		lda #0
		sta WALLX0
		lda #22
		sta WALLX1
		jsr drawMonster
		jsrf showFrameBuffer
		rts
.endproc

.export render_just_dungeon
.proc render_just_dungeon
	lda #1
	sta noMonsters
	jsr renderDungeon
	rts
.endproc

.export render_npcScene
.proc render_npcScene
	lda #1
	sta noMonsters
	txa
	pha
	jsr renderDungeon
	pla
	tax

	cpx #0
	bne :+
		lda #(88+4)/8
		sta ARGS+0
		lda #(104-4)/8
		sta ARGS+1
		lda #2
		sta ARGS+2
		jsr draw
		jmp done
	:
	cpx #1
	bne :++
		lda npcSubSequence
		bpl :+
			lda #(88+4)/8
			sta ARGS+0
			lda #(104-4)/8
			sta ARGS+1
			lda #0
			sta ARGS+2
			jsr draw
			jmp done
		:
		lda #(60+4)/8
		sta ARGS+0
		lda #(104-4)/8
		sta ARGS+1
		lda #0
		sta ARGS+2
		jsr draw
		lda #(116+4)/8
		sta ARGS+0
		lda #(104-4)/8
		sta ARGS+1
		lda #5
		sta ARGS+2
		jsr draw
		jmp done
	:
	cpx #2
	bne :++
		lda npcSubSequence
		bpl :+
			lda #(88+4)/8
			sta ARGS+0
			lda #(104-4)/8
			sta ARGS+1
			lda #3
			sta ARGS+2
			jsr draw
			jmp done
		:

		lda #(60+4)/8
		sta ARGS+0
		lda #(104-4)/8
		sta ARGS+1
		lda #3
		sta ARGS+2
		jsr draw

		lda #(116+4)/8
		sta ARGS+0
		lda #(104-4)/8
		sta ARGS+1
		ldx npcSubSequence
		lda bodyID,x
		sta ARGS+2
		jsr draw

		lda #(116+4)/8
		sta ARGS+0
		ldx npcSubSequence
		lda faceY,x
		sta ARGS+1
		lda faceID,x
		sta ARGS+2
		jsr draw
		jmp done

		bodyID: .byte $9,$8,$8,$9,$9,$a
		faceID: .byte $d,$b,$c,$e,$f,$10
		faceY:  .byte $6,$6,$6,$6,$6,$8
	:
	cpx #3
	bne :+
		lda #(88+4)/8
		sta ARGS+0
		lda #(104)/8
		sta ARGS+1
		lda #7
		sta ARGS+2
		jsr draw
		jmp done
	:
	cpx #4
	bne :+
		lda #(88+4)/8
		sta ARGS+0
		lda #(104)/8
		sta ARGS+1
		lda #6
		sta ARGS+2
		jsr draw
		jmp done
	:
	cpx #5
	bne :+
		jsr draw_xanathar
		jmp done
	:
	cpx #6
	bne :+
		lda #(88+4)/8
		sta ARGS+0
		lda #(104)/8+1
		sta ARGS+1
		ldx #0
		stx ARGS+2
		stx ARGS+3
		stx DRAW_MONSTER_HIT
		inx
		sec
		jsr drawMonster
		jmp done
	:
	cpx #7
	bne :+
		lda #(88+4)/8
		sta ARGS+0
		lda #(104)/8
		sta ARGS+1
		lda #4
		sta ARGS+2
		jsr draw
		jmp done
	:

done:
	jsrf showFrameBuffer

	rts

draw:
	jsrf outtake_graphics
	rts
.endproc

.export draw_xanathar
.proc draw_xanathar
		lda #(88+4)/8
		sta ARGS+0
		lda #(88)/8+1
		sta ARGS+1
		ldx #0
		stx ARGS+2
		stx ARGS+3
		stx DRAW_MONSTER_HIT
		sec
		jmp drawMonster
.endproc
