.define VDCADR $d600
.define VDCDAT $d601

.macro WriteVDC
.scope
	stx VDCADR
	lp:bit VDCADR
	bpl lp
	sta VDCDAT
.endscope
.endmacro

.macro ReadVDC
.scope
	stx VDCADR
	lp:bit VDCADR
	bpl lp
	lda VDCDAT
.endscope
.endmacro

.define VDC_HT		$00 ;127
.define VDC_HD	 	$01 ;80
.define VDC_HP 		$02 ;102
.define VDC_VWHW 	$03 ;73
.define VDC_VT 		$04 ;38
.define VDC_VA 		$05 ;224
.define VDC_VD		$06 ;25
.define VDC_VP		$07 ;32
.define VDC_IM		$08 ;252
.define VDC_CTV		$09 ;231
.define VDC_CMCS	$0a ;160
.define VDC_CE		$0b ;231
.define VDC_DShi	$0c ;0
.define VDC_DSlo	$0d ;0
.define VDC_CPhi	$0e ;0
.define VDC_CPlo	$0f ;0
.define VDC_LPV		$10 ;0
.define VDC_LPH		$11 ;0
.define VDC_UAhi	$12 ;15
.define VDC_UAlo	$13 ;208
.define VDC_AAhi	$14 ;8
.define VDC_AAlo	$15 ;0
.define VDC_CDH		$16 ;120
.define VDC_CDV		$17 ;232
.define VDC_VSS		$18 ;32
.define VDC_HSS		$19 ;71
.define VDC_FGBG	$1a ;240
.define VDC_AI 		$1b ;0
.define VDC_CB		$1c ;47
.define VDC_UL 		$1d ;231
.define VDC_WC		$1e ;79
.define VDC_DA		$1f ;255
.define VDC_BAhi	$20 ;15
.define VDC_BAlo	$21 ;208
.define VDC_DEB		$22 ;125
.define VDC_DEE		$23 ;100
.define VDC_DRR		$24 ;245
.define VDC_SYNC	$25

.define VDC_COPY	$80
.define VDC_FILL	$00

.macro defineMode name, width, height, attributeHeight, interlaced
.proc name
charWidth = width/8
charHeight = height/attributeHeight
bitmapSize = charWidth*charHeight*attributeHeight
attributeSize = charWidth*charHeight
fieldSize = bitmapSize+attributeSize
verticalTotal = 38*(8/attributeHeight)
bitmap0 = 0
attributes0 = bitmapSize
bitmap1 = fieldSize
attributes1 = (fieldSize+bitmapSize)
.assert fieldSize*(interlaced+1) < 16384, error, "Mode will not fit in 16KB VDC"
			.byte VDC_HSS,	$c7
			.byte VDC_VD,	charHeight
			.byte VDC_HD,	charWidth
			.byte VDC_HT,	$7f
			.byte VDC_VT,	verticalTotal
			.byte VDC_HP,	($7f+width/8)/2
			.byte VDC_VP,	(verticalTotal+charHeight)/2
			.byte VDC_CTV,	attributeHeight-1
.proc even
			.byte VDC_DSlo,	<bitmap0
			.byte VDC_DShi,	>bitmap0
			.byte VDC_AAlo,	<attributes0
			.byte VDC_AAhi,	>attributes0
.endproc
.if interlaced = 1
.proc odd
			.byte VDC_DSlo,	<bitmap1
			.byte VDC_DShi,	>bitmap1
			.byte VDC_AAlo,	<attributes1
			.byte VDC_AAhi,	>attributes1
.endproc
.endif
.endproc
.endmacro

.macro defineTextMode name, width, height
.proc name
attributeSize = width*height
screen = (0+4*width)
attributes = (screen+attributeSize+4*width)
font = $2000
alternateFont = font + $1000
verticalTotal = 38
.assert attributeSize*2 < font, error, "Mode will not fit in below the font"
			.byte VDC_HT,	$3f
			.byte VDC_HD,	width
			.byte VDC_HP,	($3f+width+9)/2
			.byte VDC_VWHW,	$45
			.byte VDC_VT,	verticalTotal
			.byte VDC_VA,	0
			.byte VDC_VD,	height
			.byte VDC_VP,	(verticalTotal+height)/2+1
			.byte VDC_IM,	0
			.byte VDC_CTV,	7
			.byte VDC_CMCS,	$40
			.byte VDC_CE,	8
			.byte VDC_DSlo,	<screen
			.byte VDC_DShi,	>screen
			.byte VDC_CPlo, $ff
			.byte VDC_CPhi,	$ff
			.byte VDC_AAlo,	<attributes
			.byte VDC_AAhi,	>attributes
			.byte VDC_CDH,	$89
			.byte VDC_CDV,	7
			.byte VDC_VSS,	$20
			.byte VDC_HSS,	$57
			.byte VDC_FGBG,	$00
			.byte VDC_AI,	0
			.byte VDC_CB, 	<(font>>8)
			.byte VDC_UL,	7
			.byte VDC_DEB,	$3f
			.byte VDC_DEE,	$32
			.byte VDC_DRR,	$05
.endproc
.endmacro

.macro defineTextModeNTSC name, width, height
.proc name
attributeSize = width*height
screen = (0+4*width)
attributes = (screen+attributeSize+4*width)
font = $2000
alternateFont = font + $1000
verticalTotal = 38
.assert attributeSize*2 < font, error, "Mode will not fit in below the font"
			.byte VDC_HT,	$3f
			.byte VDC_HD,	width
			.byte VDC_HP,	($3f+width+9)/2
			.byte VDC_VWHW,	$45
			.byte VDC_VT,	verticalTotal
			.byte VDC_VA,	0
			.byte VDC_VD,	height
			.byte VDC_VP,	(verticalTotal+height)/2+1
			.byte VDC_IM,	0
			.byte VDC_CTV,	7
			.byte VDC_CMCS,	$40
			.byte VDC_CE,	8
			.byte VDC_DSlo,	<screen
			.byte VDC_DShi,	>screen
			.byte VDC_CPlo, $ff
			.byte VDC_CPhi,	$ff
			.byte VDC_AAlo,	<attributes
			.byte VDC_AAhi,	>attributes
			.byte VDC_CDH,	$89
			.byte VDC_CDV,	7
			.byte VDC_VSS,	$20
			.byte VDC_HSS,	$57
			.byte VDC_FGBG,	$00
			.byte VDC_AI,	0
			.byte VDC_CB, 	<(font>>8)
			.byte VDC_UL,	7
			.byte VDC_DEB,	$3f
			.byte VDC_DEE,	$32
			.byte VDC_DRR,	$05
.endproc
.endmacro
