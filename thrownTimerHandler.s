.include "global.inc"

.segment "MAIN"

newPositionAndSubposTable:
	.byte $82, $83, $00, $01
	.byte $01, $80, $03, $82
	.byte $02, $03, $80, $81
	.byte $81, $00, $83, $02

forwardDeltaLo:
	.byte <-32, <1, <32, <-1
forwardDeltaHi:
	.byte >-32, >1, >32, >-1

.pushseg
.segment "BSS"
	endOfRangeReached:	.res 1
	newSubpos:		.res 1
	newPosition:		.res 2
	_wallFlags:		.res 1
.popseg

.proc moveToNewPosition
	; int frontWallMappingIndex = mb.walls[thrown.direction^2];
	lda newPosition+0
	asl
	sta TMP2+0
	sta TMP+0
	lda newPosition+1
	rol
	sta TMP2+1
	asl TMP+0
	rol
	sta TMP+1

	clc
	lda #<maze
	adc TMP+0
	sta TMP+0
	lda #>maze
	adc TMP+1
	sta TMP+1

	ldy #Thrown::direction
	lda (CURRENT_THROWN),y
	eor #2
	tay
	lax (TMP),y

	; int wallFlags = frontWallMapping.flags;
	lda wallFlags,x
	sta _wallFlags

	ldy #Thrown::active
	lda (CURRENT_THROWN),y
	cmp #1
	beq isItem
	jmp isSpell
	isItem:
	.scope
		; Normal item
		;int itemIndex = thrown.itemOrSpellIndex;
		ldy #Thrown::itemOrSpellIndex
		lda (CURRENT_THROWN),y
		sta ARGS+0
		iny
		lda (CURRENT_THROWN),y
		sta ARGS+1

		; boolean isSmallItem = (tdItemIndex>=0x0f);
		; if ((wallFlags&WallMapping.PASSPARTY)!=0) || (thrown.movedOnlyWithinSubpos!=0) || 
		; (((wallFlags&WallMapping.PASSSMALL)!=0) && isSmallItem);
		; true for now

		; (thrown.movedOnlyWithinSubpos!=0)
		ldy #Thrown::movedOnlyWithinSubpos
		lda (CURRENT_THROWN),y
		bne pass

		; ((wallFlags&WallMapping.PASSPARTY)!=0)
		lda _wallFlags
		and #WALLFLAG_PASSPARTY
		bne pass

		;int tdItemIndex = Renderer.itemPictureTo3dItemIndex[item.picture];
		ldy #Item::picture
		;lax (ARGS+0),y
		jsr lax_ARGS0_y
		lda itemPictureTo3dItemIndex,x

		; (((wallFlags&WallMapping.PASSSMALL)!=0) && isSmallItem)
		cmp #15 ;<15 = largeitem
		bcc noPass

		lda _wallFlags
		and #WALLFLAG_PASSSMALL
		bne pass

		noPass:
			lda #TRIGGER_THROWNHITSWALL
			sta TRIGGER_MASK
			jmp return

		pass:
			;int topItemIndexPtr[] = {GameState.inf.maze.mazeBlocks[thrown.position].topItemIndex};
			ldy #Thrown::position
			lda (CURRENT_THROWN),y
			asl
			sta TMP+0
			iny
			lda (CURRENT_THROWN),y
			rol
			sta TMP+1

			clc
			lda #<topItemPtrs
			adc TMP+0
			sta TMP+0
			lda #>topItemPtrs
			adc TMP+1
			sta TMP+1

			ldy #0
			lda (TMP),y
			sta TOP_ITEM+0
			iny
			lda (TMP),y
			sta TOP_ITEM+1

			;GameState.unlinkItem(topItemIndexPtr, 0, itemIndex);

			jsrf items_unlinkItemByItemIndex

			;GameState.inf.maze.mazeBlocks[thrown.position].topItemIndex = topItemIndexPtr[0];
			ldy #0
			lda TOP_ITEM+0
			sta (TMP),y
			iny
			lda TOP_ITEM+1
			sta (TMP),y

			;topItemIndexPtr[0] = mb.topItemIndex;
			clc
			lda #<topItemPtrs
			adc TMP2+0
			sta TMP2+0
			lda #>topItemPtrs
			adc TMP2+1
			sta TMP2+1

			ldy #0
			lda (TMP2),y
			sta TOP_ITEM+0
			iny
			lda (TMP2),y
			sta TOP_ITEM+1

			ldy #Thrown::itemOrSpellIndex
			lda (CURRENT_THROWN),y
			sta CUR_ITEM+0
			iny
			lda (CURRENT_THROWN),y
			sta CUR_ITEM+1

			lda newPosition+0
			sta ARGS+0
			lda newPosition+1
			sta ARGS+1

			lda newSubpos
			ora #FLYING
			sta ARGS+2

			;GameState.linkItem(topItemIndexPtr, newPosition, itemIndex, newSubpos|4);
			jsrf items_linkItem

			;mb.topItemIndex = topItemIndexPtr[0];
			ldy #0
			lda TOP_ITEM+0
			sta (TMP2),y
			iny
			lda TOP_ITEM+1
			sta (TMP2),y

			;thrown.position = newPosition;
			ldy #Thrown::position
			lda newPosition+0
			sta (CURRENT_THROWN),y
			iny
			lda newPosition+1
			sta (CURRENT_THROWN),y

			;thrown.subpos = newSubpos;
			ldy #Thrown::subpos
			lda newSubpos
			sta (CURRENT_THROWN),y

			;thrown.range--;
			ldy #Thrown::range
			sec
			lda (CURRENT_THROWN),y
			sbc #1
			sta (CURRENT_THROWN),y

			;return true;
			sec
			rts
	.endscope

	isSpell:
	.scope
		;boolean mayPass = (((wallFlags&WallMapping.PASSPARTY)!=0) || (thrown.position==newPosition));

		; ((wallFlags&WallMapping.PASSPARTY)!=0)
		lda _wallFlags
		and #WALLFLAG_PASSPARTY
		bne pass

		; (thrown.position==newPosition));
		ldy #Thrown::position
		lda (CURRENT_THROWN),y
		cmp newPosition+0
		bne noPass
		iny
		lda (CURRENT_THROWN),y
		cmp newPosition+1
		beq pass

		; Spell
		noPass:
			lda #TRIGGER_PLAYERENTER
			sta TRIGGER_MASK
			jmp return

		pass:
			;thrown.position = newPosition;
			ldy #Thrown::position
			lda newPosition+0
			sta (CURRENT_THROWN),y
			iny
			lda newPosition+1
			sta (CURRENT_THROWN),y

			;thrown.subpos = newSubpos;
			ldy #Thrown::subpos
			lda newSubpos
			sta (CURRENT_THROWN),y

			;thrown.range--;
			ldy #Thrown::range
			sec
			lda (CURRENT_THROWN),y
			bmi :+
				sbc #1
				sta (CURRENT_THROWN),y
			:

			;return true;
			sec
			rts
	.endscope

return:
	lda newPosition+0
	sta ARGS+0
	lda newPosition+1
	sta ARGS+1
	ldy #Thrown::direction
	lda (CURRENT_THROWN),y
	sta ARGS+2
	lda #0
	sta triggerRect+0
	sta triggerRect+1
	lda #22
	sta triggerRect+2
	lda #15
	sta triggerRect+3
	jsrf scriptParser_doHandleWallEvent

	; return false
	clc
	rts
.endproc

.export checkAllThrownHit 
.proc checkAllThrownHit
	lda #<thrown
	sta CURRENT_THROWN+0
	lda #>thrown
	sta CURRENT_THROWN+1
	loop:
		ldy #Thrown::active
		lda (CURRENT_THROWN),y
		beq :++
			ldy #Thrown::position
			lda (CURRENT_THROWN),y
			pha
			iny
			lax (CURRENT_THROWN),y
			ldy #Thrown::subpos
			lda (CURRENT_THROWN),y
			tay
			pla
			jsr checkThrownHit
			bcc :+
				jsr terminate
			:
		:
		clc
		lda CURRENT_THROWN+0
		adc #.sizeof(Thrown)
		sta CURRENT_THROWN+0
		tax
		lda CURRENT_THROWN+1
		adc #0
		sta CURRENT_THROWN+1
		cmp #>(thrown+.sizeof(Thrown)*10)
		bne loop
		cpx #<(thrown+.sizeof(Thrown)*10)
	bne loop
	rts
.endproc

.proc checkThrownHit
	.pushseg
	.segment "BSS"
		position: .word 0
		subpos: .byte 0
	.popseg

	sta position+0
	stx position+1
	sty subpos

	ldy #Thrown::movedOnlyWithinSubpos
	lda (CURRENT_THROWN),y
	beq :+
		jmp false
	:

	ldy #Thrown::active
	lda (CURRENT_THROWN),y
	cmp #2
	bne :++
		ldy #Thrown::spellHandler
		lda (CURRENT_THROWN),y
		sta INDJMP+0
		iny
		lda (CURRENT_THROWN),y
		sta INDJMP+1
		ora INDJMP+0
		clc
		beq :+
			ldx #<.bank(spellBank)
			jsr indirectJsr
		:
		rts
	:

	clc
	lda position+0
	adc #<triggerBitsAndMonsterCount
	sta TMP+0
	lda position+1
	adc #>triggerBitsAndMonsterCount
	sta TMP+1
	ldy #0
	lda (TMP),y
	and #7
	beq noMonstersOnLocation
		ldx #0
		:
			lda position+0
			cmp monster_position_lo,x
			bne continue
			lda position+1
			cmp monster_position_hi,x
			bne continue
				lda subpos
				cmp monster_subpos,x
				beq monsterHit
				lda #4
				cmp monster_subpos,x
				beq monsterHit
				jmp continue
				monsterHit:
					stx ARGS+0
					jsrf gameState_thrownHitMonster
					lsr ARGS+0
					rts
			continue:
			inx
			cpx #30
		bne :-
	noMonstersOnLocation:

	lda position+0
	cmp partyPosition+0
	bne :+
	lda position+1
	cmp partyPosition+1
	bne :+
		jsrf gameState_thrownHitParty
		lsr ARGS+0
		rts
	:

	; return false
false:	clc
	rts
.endproc

.proc terminate
	ldy #Thrown::active
	lda (CURRENT_THROWN),y
	beq :+
		ldy #Thrown::itemOrSpellIndex
		lda (CURRENT_THROWN),y
		sta TMP+0
		iny
		lda (CURRENT_THROWN),y
		sta TMP+1
		ldy #Item::subpos
		jsr lda_TMP_y
		and #3
		jsr sta_TMP_y

		ldy #Thrown::position
		lda (CURRENT_THROWN),y
		iny
		sta ARGS+0
		lda (CURRENT_THROWN),y
		sta ARGS+1
		lda #TRIGGER_ITEMDROPPED
		sta ARGS+2
		jsrf scriptParser_executeTrigger

		lda #$12
		sta ARGS+0
		jsrf audio_playSoundAtPartyPosition
	:

	lda #0
	ldy #Thrown::active
	sta (CURRENT_THROWN),y
	ldy #Thrown::range
	sta (CURRENT_THROWN),y
	ldy #Thrown::direction
	sta (CURRENT_THROWN),y
	ldy #Thrown::position
	sta (CURRENT_THROWN),y
	iny
	sta (CURRENT_THROWN),y
	ldy #Thrown::caster
	sta (CURRENT_THROWN),y
	ldy #Thrown::spellGfxIndexOrItemType
	sta (CURRENT_THROWN),y
	ldy #Thrown::itemOrSpellIndex
	sta (CURRENT_THROWN),y
	iny
	sta (CURRENT_THROWN),y

	rts
.endproc

.proc handleThrown
	.pushseg
	.segment "BSS"
		newPositionNeeded: .byte 0
	.popseg

	;boolean endOfRangeReached = (currentThrown.range == 0);
	lda #0
	sta endOfRangeReached
	ldy #Thrown::range
	lda (CURRENT_THROWN),y
	bne :+
		inc endOfRangeReached
	:

	ldy #Thrown::position
	lda (CURRENT_THROWN),y
	sta newPosition+0
	iny
	lda (CURRENT_THROWN),y
	sta newPosition+1

	ldy #Thrown::direction
	lda (CURRENT_THROWN),y
	asl
	asl
	ldy #Thrown::subpos
	ora (CURRENT_THROWN),y
	tax
	lda newPositionAndSubposTable,x
	sta newPositionNeeded

	bpl :+
		and #$7f
		sta newSubpos

		; New position needed
		;newPosition = GameState.getForwardPosition(currentThrown.position, currentThrown.direction);
		ldy #Thrown::direction
		lax (CURRENT_THROWN),y
		clc
		lda newPosition+0
		adc forwardDeltaLo,x
		sta newPosition+0
		iny
		lda newPosition+1
		adc forwardDeltaHi,x
		and #$03
		sta newPosition+1

		; currentThrown.movedOnlyWithinSubpos = 0;
		lda #0
		ldy #Thrown::movedOnlyWithinSubpos
		sta (CURRENT_THROWN),y
		jmp :++
	:
		; No new position needed
		sta newSubpos
	:

	jsr moveToNewPosition
	bcc didNotMoveToNewPosition
	didMoveToNewPosition:
		lda newPositionNeeded
		bpl :+
			lda newPosition+0
			sta ARGS+0
			lda newPosition+1
			sta ARGS+1
			lda #TRIGGER_THROWNHITSWALL
			sta ARGS+2
			jsrf scriptParser_executeTrigger
		:

		lda newPosition+0
		ldx newPosition+1
		ldy newSubpos
		jsr checkThrownHit
		bcc :+
			inc endOfRangeReached
		:
		jmp checkTerminate
	didNotMoveToNewPosition:
		ldy #Thrown::flags
		lda (CURRENT_THROWN),y
		and SPELLFLAGS_EXPLOSION
		beq :+
			ldy #Thrown::position
			lda (CURRENT_THROWN),y
			pha
			iny
			lax (CURRENT_THROWN),y
			ldy #Thrown::subpos
			lda (CURRENT_THROWN),y
			tay
			pla
			jsr checkThrownHit
			bcs :+
				lda CURRENT_THROWN+0
				sta ARGS+0
				lda CURRENT_THROWN+1
				sta ARGS+1
				jsrf gameState_handleExplosion
		:
		inc endOfRangeReached

checkTerminate:
	lda endOfRangeReached
	beq :+
		jsr terminate
	:

	inc SHOULDRENDER

	rts
.endproc

.export thrownTimerHandler
.proc thrownTimerHandler
	lda #<thrown
	sta CURRENT_THROWN+0
	lda #>thrown
	sta CURRENT_THROWN+1
	loop:
		ldy #Thrown::active
		lda (CURRENT_THROWN),y
		beq :+
			jsr handleThrown
		:
		clc
		lda CURRENT_THROWN+0
		adc #.sizeof(Thrown)
		sta CURRENT_THROWN+0
		tax
		lda CURRENT_THROWN+1
		adc #0
		sta CURRENT_THROWN+1
		cmp #>(thrown+.sizeof(Thrown)*10)
		bne loop
		cpx #<(thrown+.sizeof(Thrown)*10)
	bne loop
	rts
.endproc
