.include "global.inc"

.segment "BSS"
runPos:	.res 2

; Normal implementation
; Standard				0162
; No spr				0146	1c
; No bg					0117	4b
; No spr,bg				00fc	66

; Render optimizations
; Standard				012f
; No spr				
; No bg					
; No spr,bg	

; Render optimizations with culling
; Standard				0100

; No walls				00ee
; No wallrender			00f1
; draw0					00f9
; draw1					010a
; draw2					0111
; draw3					011b
; draw4					0120
; draw5					0129
; draw6					012b
; draw7					012f
;
; No showFB				00dd 52
; No showSpr			010d 22
;
; Total time: 		12f 303
; Of which blit is: 052  82 = 27%
; Of which sprites; 030  48 = 58%
;
; 012f -> 010e -> 0100
; c64 1mhz: $104 260
; c128 2mhz: $a8 168
;
; 2020-04-23:
; Baseline: $f1
;
; 2022-05-02
; Baseline: $f2
;
; 2022-05-03
; Baseline: $d7

.segment "BENCHMARK"

.export benchmark
.proc benchmark
	ldx #1
	jsrf gameState_loadLevel

	jsr resetTimer
	lda #<run
	sta runPos+0
	lda #>run
	sta runPos+1
	loop:
		lda runPos+0
		sta TMP+0
		lda runPos+1
		sta TMP+1

		ldy #0
		lda (TMP),y
		bpl :+
			jsr measure
			jmp benchmark
		:
		sta partyPosition+1
		iny
		lda (TMP),y
		sta partyPosition+0
		iny
		lda (TMP),y
		sta partyDirection
		inc SHOULDRENDER
		jsrf render

		clc
		lda runPos+0
		adc #3
		sta runPos+0
		bcc :+
			inc runPos+1
		:
	jmp loop
.endproc

.proc measure
	lda #0
	sta timersEnabled
	lda _tick+3
	jsr printByte
	lda _tick+2
	jsr printByte
	lda _tick+1
	jsr printByte
	lda _tick+0
	jsr printByte
	jsrf text_newLine
	rts
printByte:
	sta ARGS+0
	jsrf text_writeHexByteNoDollar
	rts
.endproc

.proc resetTimer
	lda #0
	sta _tick+0
	sta _tick+1
	sta _tick+2
	sta _tick+3
	inc timersEnabled
	rts
.endproc

run:
.byte $01,$ea,0
.byte $01,$ea,1
.byte $01,$eb,1
.byte $01,$ec,1
.byte $01,$ed,1
.byte $01,$ef,1
.byte $01,$ee,1
.byte $01,$ee,0
.byte $01,$ce,0
.byte $01,$ae,0
.byte $01,$8e,0
.byte $01,$8e,1
.byte $01,$8f,1
.byte $01,$90,1
.byte $01,$91,1
.byte $01,$92,1
.byte $01,$92,2
.byte $01,$b2,2
.byte $01,$d2,2
.byte $01,$d1,2
.byte $01,$f2,2
.byte $02,$11,2
.byte $02,$31,2
.byte $02,$51,2
.byte $02,$51,3
.byte $02,$50,3
.byte $02,$4f,3
.byte $02,$4f,3
.byte $02,$4d,3
.byte $02,$4c,3
.byte $02,$4b,3
.byte $02,$4a,3
.byte $02,$49,3
.byte $02,$48,3
.byte $02,$48,2
.byte $02,$68,2
.byte $02,$88,2
.byte $02,$a8,2
.byte $02,$a8,1
.byte $02,$a9,1
.byte $02,$aa,1
.byte $02,$ab,1
.byte $02,$ac,1
.byte $02,$ad,1
.byte $02,$ae,1
.byte $02,$af,1
.byte $02,$b0,1
.byte $02,$b0,0
.byte $02,$90,0
.byte $02,$91,0
.byte $02,$92,0
.byte $02,$72,0
.byte $02,$73,0
.byte $ff
