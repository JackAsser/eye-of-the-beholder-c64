import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Convert {
	private static void convert(String inFile, String name) throws Exception {
		BufferedImage bitmap = ImageIO.read(new File(inFile));
		byte pixels[] = ((DataBufferByte)bitmap.getRaster().getDataBuffer()).getData();
		Declash.declash(bitmap, C64Char.spriteColors);
		ImageIO.write(bitmap, "PNG", new File(name+"-auto.png"));

		FileOutputStream f0 = new FileOutputStream(name+"-bitmap.bin");
		FileOutputStream f1 = new FileOutputStream(name+"-screen.bin");
		FileOutputStream f2 = new FileOutputStream(name+"-d800.bin");

		int cw = bitmap.getWidth()/4;
		int ch = bitmap.getHeight()/8;
		EOBObject obj = new EOBObject(bitmap, 0,0, cw,ch);
		for (int cy=0; cy<ch; cy++) {
			for (int cx=0; cx<cw; cx++) {
				C64Char c = obj.c64Chars[cx*ch+cy];
				if (c.type != C64Char.CharType.Bitmap)
					throw new Exception("WTF!");
				for (int i=0; i<8; i++)
					f0.write(c.data.get(i)&0xff);
				f1.write(c.screen&0xff);
				f2.write(c.d800&0xff);
			}
		}
		f0.close();
		f1.close();
		f2.close();
	}

	private static void convertSpellBook(String inFile, String name) throws Exception {
		BufferedImage bitmap = ImageIO.read(new File(inFile));
		byte pixels[] = ((DataBufferByte)bitmap.getRaster().getDataBuffer()).getData();
		Declash.declash(bitmap, C64Char.spriteColors);
		ImageIO.write(bitmap, "PNG", new File(name+"-auto.png"));

		FileOutputStream f0 = new FileOutputStream(name+"-bitmap.bin");
		FileOutputStream f1 = new FileOutputStream(name+"-screen.bin");
		FileOutputStream f2 = new FileOutputStream(name+"-d800.bin");

		int cw = bitmap.getWidth()/4;
		int ch = bitmap.getHeight()/8;
		EOBObject obj = new EOBObject(bitmap, 0,0, cw,ch);
		for (int cy=0; cy<ch; cy++) {
			for (int cx=0; cx<cw; cx++) {
				C64Char c = obj.c64Chars[cx*ch+cy];
				if (c.type != C64Char.CharType.Bitmap)
					throw new Exception("WTF!");

				if ((cx>0) && (cy>=1)) {
					int r = (cy<(ch-1)) ? 8:1;
					for (int i=0; i<r; i++) {
						c.data.set(i, (byte)0xff);
					}
					c.screen=(byte)0xcd;
					c.d800=(byte)0xf;
				}

				if ((cy==ch-1) && ((cx==6)||(cx==12)||(cx==13))) {
					for (int i=2; i<8; i++) {
						c.data.set(i,(byte)0x55);
					}
				}

				if (cy==0)
					c.data.set(0,(byte)0);

				for (int i=0; i<8; i++)
					f0.write(c.data.get(i)&0xff);
				f1.write(c.screen&0xff);
				f2.write(c.d800&0xff);
			}
		}
		f0.close();
		f1.close();
		f2.close();
	}

	private static void convert(BufferedImage bitmap, int cx, int cy, int cw, int ch, byte[] colors, String name) throws Exception {
		C64Char.fixedColors = colors;
		convert(bitmap, cx,cy,cw,ch, name);
		C64Char.fixedColors = null;
	}

	private static void convert(BufferedImage bitmap, int cx, int cy, int cw, int ch, String name) throws Exception {
		FileOutputStream f0 = new FileOutputStream(name+"-bitmap.bin");
		FileOutputStream f1 = new FileOutputStream(name+"-screen.bin");
		FileOutputStream f2 = new FileOutputStream(name+"-d800.bin");

		EOBObject obj = new EOBObject(bitmap, cx,cy,cw,ch);
		for (cy=0; cy<ch; cy++) {
			for (cx=0; cx<cw; cx++) {
				C64Char c = obj.c64Chars[cx*ch+cy];
				if (c.type != C64Char.CharType.Bitmap)
					throw new Exception("WTF!");
				for (int i=0; i<8; i++)
					f0.write(c.data.get(i)&0xff);
				f1.write(c.screen&0xff);
				f2.write(c.d800&0xff);
			}
		}
		f0.close();
		f1.close();
		f2.close();
	}

	private static void convertOverlays() throws Exception {
		BufferedImage bitmap = ImageIO.read(new File("converted/overlays.png"));
		byte pixels[] = ((DataBufferByte)bitmap.getRaster().getDataBuffer()).getData();

		convert(bitmap, 0,0,10,1, new byte[]{0,13,1,5}, "digits");
		convert(bitmap, 0,1,4,2, new byte[]{0,10,1,2}, "miss");
		convert(bitmap, 0,3,4,2, new byte[]{0,13,1,5}, "hack");
		convert(bitmap, 0,5,4,2, new byte[]{0,10,1,2}, "cantreach");
		convert(bitmap, 0,7,4,2, new byte[]{0,10,1,2}, "noammo");
		convert(bitmap, 0,9,4,2, new byte[]{0,13,1,5}, "green");
		convert(bitmap, 0,11,4,2, new byte[]{0,10,1,2}, "red");
		convert(bitmap, 0,13,4,2, new byte[]{0,12,1,0xf}, "default");
	}

	private static void grabSpriteRow(BufferedImage bitmap, int nbrSprites, int height, int ypos, FileOutputStream fos) throws Exception {
		byte pixels[] = ((DataBufferByte)bitmap.getRaster().getDataBuffer()).getData();
		for (int s=0; s<nbrSprites; s++) {
			for (int y=0; y<height; y++) {
				for (int x=0;x<3; x++) {
					byte ch = 0;
					for (int p=0; p<8; p++) {
						byte c = pixels[s*24+(y+ypos)*bitmap.getWidth()+x*8+p];
						if (c==1)
							ch|=1<<(7-p);
					}
					fos.write(ch&0xff);
				}
			}
		}
	}

	private static void convertChargen() throws Exception {
		BufferedImage bitmap;
		FileOutputStream fos;

		bitmap = ImageIO.read(new File("converted/chargen_menu.png"));
		convert(bitmap, 0,0,10,5, "chargen_menu");
		bitmap = ImageIO.read(new File("converted/chargen_menu_sprites.png"));
		fos = new FileOutputStream("chargen_menu-sprites.bin");
		grabSpriteRow(bitmap, 3,  7, 0, fos);
		grabSpriteRow(bitmap, 3, 20, 7, fos);
		fos.close();

		bitmap = ImageIO.read(new File("converted/chargen_back.png"));
		convert(bitmap, 0,0,5,3, "chargen_back");
		bitmap = ImageIO.read(new File("converted/chargen_back_sprites.png"));
		fos = new FileOutputStream("chargen_back-sprites.bin");
		grabSpriteRow(bitmap, 2, 11, 0, fos);
		fos.close();

		bitmap = ImageIO.read(new File("converted/chargen_arrows.png"));
		convert(bitmap, 0,0,4,4, "chargen_arrows");

		bitmap = ImageIO.read(new File("converted/chargen_delete.png"));
		convert(bitmap, 0,0,10,3, "chargen_delete");
		bitmap = ImageIO.read(new File("converted/chargen_delete_sprites.png"));
		fos = new FileOutputStream("chargen_delete-sprites.bin");
		grabSpriteRow(bitmap, 3, 11, 0, fos);

		bitmap = ImageIO.read(new File("converted/chargen_modify.png"));
		convert(bitmap, 0,0,11,3, "chargen_modify");
		bitmap = ImageIO.read(new File("converted/chargen_modify_sprites.png"));
		fos = new FileOutputStream("chargen_modify-sprites.bin");
		grabSpriteRow(bitmap, 4, 11, 0, fos);

		bitmap = ImageIO.read(new File("converted/chargen_play.png"));
		convert(bitmap, 0,0,11,3, "chargen_play");

		fos.close();
	}

	private static void convertCamp() throws Exception {
		BufferedImage bitmap = ImageIO.read(new File("converted/camp.png"));

		C64SpriteSheet sheet = new C64SpriteSheet(bitmap, 0, 0, 3, 1, new int[]{0x00,0x09,0x04}, new int[] {
			0x0a, 0x0a, 0x87
		});

		sheet.save(String.format("camp.bin"));
	}

	public static void main(String[] args) {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);
		C64Char.forceUnusedToBlack = true;
		C64Char.sortColors = true;
		C64Char.forcedType = C64Char.CharType.Bitmap;

		try {
			convert("converted/north_left.png", "north_left");
			convert("converted/north_center.png", "north_center");
			convert("converted/north_right.png", "north_right");

			convert("converted/east_left.png", "east_left");
			convert("converted/east_center.png", "east_center");
			convert("converted/east_right.png", "east_right");

			convert("converted/south_left.png", "south_left");
			convert("converted/south_center.png", "south_center");
			convert("converted/south_right.png", "south_right");

			convert("converted/west_left.png", "west_left");
			convert("converted/west_center.png", "west_center");
			convert("converted/west_right.png", "west_right");

			convert("converted/controls.png", "controls");

			C64Char.forceUnusedToBlack = false;
			C64Char.forceColorToIndex = Map.of(
				0x0f, 1,
				0x0c, 2
			);
			convert("converted/inventory.png", "inventory");

			C64Char.forceColorToIndex = Map.of(
				0x0f, 1,
				0x01, 2
			);
			convert("converted/stats.png", "stats");
			C64Char.forceColorToIndex = null;
			C64Char.forceUnusedToBlack = true;

			convert("converted/characters.png", "characters");
			convert("converted/character5.png", "character5");
			convert("converted/character6.png", "character6");

			C64Char.fixedColors = new byte[]{0,0,0,1};
			convert("converted/font.png", "font");
			C64Char.fixedColors = null;
			C64Char.forceUnusedToBlack = false;
			convertSpellBook("converted/spellbook.png", "spellbook");
			C64Char.forceUnusedToBlack = true;
			convertChargen();
			convertOverlays();
			convertCamp();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
