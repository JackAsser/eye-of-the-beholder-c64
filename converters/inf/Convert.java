import java.io.*;
import java.nio.*;
import java.util.*;

public class Convert {
	private static void dumpInf(PrintStream out, int startLvl, int stopLvl) throws IOException {
		ExtractScript.stringPool = "";
		for (int levelIndex=startLvl; levelIndex<=stopLvl; levelIndex++) {
			Inf inf = new Inf(levelIndex);
/*
			for (int i=0; i<4; i++)
				System.out.printf("%d ", inf.monsterTimeouts[i]);
			System.out.println();
*/			
			Optional<Integer> maxWmi = inf.wallMappings.keySet().stream().reduce(Integer::max);

			int wallType[] = new int[maxWmi.get()+1];
			int wallEventMask[] = new int[maxWmi.get()+1];
			int wallFlags[] = new int[maxWmi.get()+1];

			for (int wmi : inf.wallMappings.keySet()) {
				WallMapping wm = inf.wallMappings.get(wmi);
				wallType[wmi] = wm.wallType;
				wallEventMask[wmi] = wm.event;
				wallFlags[wmi] = wm.flags;
			}

			out.printf("; -------------------------------- Level %d -------------------------------------\n", levelIndex);

			AsmUtil.dumpByteArray(out, String.format("lvl%dWallType", levelIndex), wallType);
			AsmUtil.dumpByteArray(out, String.format("lvl%dWallEventMask", levelIndex), wallEventMask);
			AsmUtil.dumpByteArray(out, String.format("lvl%dWallFlags", levelIndex), wallFlags);

			out.printf(".export lvl%dTimer\n", levelIndex);
			out.printf("lvl%dTimer:\n", levelIndex);
			out.printf("\t.byte %d ;timerMode\n", inf.timerMode);
			out.printf("\t.word %d ;timerTicks\n", inf.timerTicks);
			out.printf("\t.word %d ;stepsUntilScriptCall\n", inf.stepsUntilScriptCall);

			ExtractScript.extract(out, levelIndex, inf);

			out.printf(".export lvl%dMonsterCreate\n", levelIndex);
			out.printf("lvl%dMonsterCreate:\n", levelIndex);
			for (int i=0; i<inf.monsterCreates.length; i++) {
			MonsterCreate mc = inf.monsterCreates[i];
			if (mc.index==0)
			  break;
			out.printf("\t.byte $%02x,<$%04x,>$%04x,$%02x,$%02x,$%02x,$%02x,$%02x,$%02x,$%02x,$%02x\n",
			  mc.levelType,
			  mc.position, mc.position,
			  mc.subpos,
			  mc.direction,
			  mc.type,
			  mc.picture,
			  mc.phase,
			  mc.pause,
			  mc.weapon,
			  mc.pocketItem
			  );
			}
			out.println("\t.byte $ff");
		}

		AsmUtil.dumpLocalName(out, "messagePool");
		for (int i=0; i<ExtractScript.stringPool.length(); i+=32) {
			String subString = ExtractScript.stringPool.substring(i,Math.min(ExtractScript.stringPool.length(), i+32));
			out.printf("\t .byte \"%s\"\n", subString);
		}
	}

	private static void dumpMonsterTypes(PrintStream out) {
		ArrayList<Integer> c;
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.ac);
		AsmUtil.dumpByteArray(out, "monsterType_ac", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.thac0);
		AsmUtil.dumpByteArray(out, "monsterType_tach0", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.hpDices);
		AsmUtil.dumpByteArray(out, "monsterType_hpDices", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackCount);
		AsmUtil.dumpByteArray(out, "monsterType_attackCount", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackDices[0].rolls);
		AsmUtil.dumpByteArray(out, "monsterType_attackDice0Rolls", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackDices[0].sides);
		AsmUtil.dumpByteArray(out, "monsterType_attackDice0Sides", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackDices[0].base);
		AsmUtil.dumpByteArray(out, "monsterType_attackDice0Base", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackDices[1].rolls);
		AsmUtil.dumpByteArray(out, "monsterType_attackDice1Rolls", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackDices[1].sides);
		AsmUtil.dumpByteArray(out, "monsterType_attackDice1Sides", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackDices[1].base);
		AsmUtil.dumpByteArray(out, "monsterType_attackDice1Base", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackDices[2].rolls);
		AsmUtil.dumpByteArray(out, "monsterType_attackDice2Rolls", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackDices[2].sides);
		AsmUtil.dumpByteArray(out, "monsterType_attackDice2Sides", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackDices[2].base);
		AsmUtil.dumpByteArray(out, "monsterType_attackDice2Base", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.typeBits);
		AsmUtil.dumpShortLoArray(out, "monsterType_typeBits_lo", c);
		AsmUtil.dumpShortHiArray(out, "monsterType_typeBits_hi", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackBits);
		AsmUtil.dumpByteArray(out, "monsterType_attackBits", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.size);
		AsmUtil.dumpByteArray(out, "monsterType_size", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.experience);
		AsmUtil.dumpShortLoArray(out, "monsterType_experience_lo", c);
		AsmUtil.dumpShortHiArray(out, "monsterType_experience_hi", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.attackSound);
		AsmUtil.dumpByteArray(out, "monsterType_attackSound", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.walkSound);
		AsmUtil.dumpByteArray(out, "monsterType_walkSound", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.thcount);
		AsmUtil.dumpByteArray(out, "monsterType_thCount", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.spellImmunity);
		AsmUtil.dumpByteArray(out, "monsterType_spellImmunity", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.tuResist);
		AsmUtil.dumpByteArray(out, "monsterType_tuResist", c);
		c = new ArrayList<Integer>(); for (MonsterType mt : MonsterType.monsterTypes) c.add(mt.specialYoffset);
		AsmUtil.dumpByteArray(out, "monsterType_specialYoffset", c);
	}

    public static void main(String[] args) {
    	try {
			Global.BASE = "../../pcdata/";
			Global.byteOrder = ByteOrder.LITTLE_ENDIAN;

			int lvls[][] = {
				{1,3},
				{4,5},
				{6,8},
				{9,12}
			};
			for (int i=0; i<lvls.length; i++) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				PrintStream out = new PrintStream(baos);
				out.printf(".segment \"INF%d\"\n", i);
				out.println();
				dumpInf(out, lvls[i][0], lvls[i][1]);

				FileOutputStream fos = new FileOutputStream(new File(String.format("inf_%d.s",i)));
				baos.writeTo(fos);
				fos.close();
			}

			PrintStream out = new PrintStream(new File("monsterTypes.s"));
			out.println(".segment \"MEMCODE_RO\"");
			out.println();
			dumpMonsterTypes(out);
			out.close();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
