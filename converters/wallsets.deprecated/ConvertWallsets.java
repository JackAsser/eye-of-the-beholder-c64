import java.io.*;
import java.util.*;
import java.awt.image.*;
import javax.imageio.*;

// VCN - Charset
// VMP - Screen data

public class ConvertWallsets {
	public ConvertWallsets(String wallsetName) throws Exception {
		BufferedImage bi = ImageIO.read(new File("converted/"+wallsetName+"64.png"));

		if ((bi.getWidth()!=184))// || (bi.getHeight()!=720))
			throw new Exception(String.format("Wrong image size. Expected 184x720, got %dx%d", bi.getWidth(), bi.getHeight()));

		Declash.declash(bi);

		int HEIGHT = bi.getHeight();

		ColorModel cm = bi.getColorModel();
		if (!(cm instanceof IndexColorModel))
			throw new Exception(String.format("Expected indexed imaged"));

		byte pixels[] = ((DataBufferByte)bi.getRaster().getDataBuffer()).getData();

		// Convert to koala
		byte bitmap[] = new byte[pixels.length/4];
		ArrayList<Long> vcn = new ArrayList();
		byte screen[] = new byte[bitmap.length/8];
		byte d800[] = new byte[screen.length];
		int vmp[] = new int[screen.length];
		for (int i=0; i<vmp.length; i++) {
			vmp[i] = 0;
		}

		for (int cy=0; cy<HEIGHT/8; cy++) {
			nextchar:for (int cx=0; cx<184/4; cx++) {
				byte colors[] = new byte[]{0,-1,-1,-1};

				// Assign color usage
				for (int py=0; py<8; py++) {
					for (int px=0; px<4; px++) {
						byte c = pixels[cx*4+px + (cy*8+py)*184];
						if (c>=16) {
							continue nextchar;
						}
						int ci=0;
						for (ci=0; ci<4; ci++) {
							if (colors[ci]==c)
								break;
							if (colors[ci]==-1) {
								colors[ci]=c;
								break;
							}
						}
					}
				}
				Arrays.sort(colors);

				long ch=0;
				for (int py=0; py<8; py++) {
					byte b = 0;
					for (int px=0; px<4; px++) {
						byte c = pixels[cx*4+px + (cy*8+py)*184];
						if (c>=16) {
							continue nextchar;
						}
						int ci=0;
						for (ci=0; ci<4; ci++) {
							if (colors[ci]==c)
								break;
						}
						if (ci==4) {
							System.out.printf("%d,%d\n", cx,cy);
							throw new Exception("Expected koala restrictions on image.");
						}
						b |= (byte)(ci<<((3-px)*2));
					}
					bitmap[cx*8 + py + cy*(184/4)*8] = b;
					ch |= ((long)(b&0xff)) << (py*8);
				}
				screen[cx+cy*184/4] = (byte)(((colors[1]&15)<<4) | (colors[2]&15));
				d800[cx+cy*184/4] = (byte)(colors[3]&15);

				int vcnIndex = vcn.indexOf(ch);
				if (vcnIndex == -1) {
					vcnIndex = vcn.size();
				//	System.out.printf("%04d: %016x\n", vcnIndex, ch);
					vcn.add(ch);
				}
				vmp[cx+cy*184/4] = vcnIndex+1;
			}
		}

		System.out.printf("Found %d unique chars (%d bytes)\n", vcn.size(), vcn.size()*8);

		FileOutputStream fos = new FileOutputStream(String.format("%s.vcn.bin", wallsetName));
		// One char initial padding since vmp=0 == transparent
		for (int i=0; i<8; i++)
			fos.write(0);
		for (int i=0; i<vcn.size(); i++) {
			for (int j=0; j<8; j++) {
				int b = (int)((vcn.get(i)>>(j*8))&0xff);
				fos.write(b);
			}
		}
		fos.close();

		int sizes[][] = {
			{22,15},
			{3 ,15},
			{3 ,12},
			{2 ,8 },
			{1 ,5 },
			{3 ,5 },
			{2 ,6 },
			{6 ,5 },
			{10,8 },
			{16,12},
		};
		FileOutputStream fos1 = new FileOutputStream(String.format("%s.vmp-lo.bin", wallsetName));
		FileOutputStream fos2 = new FileOutputStream(String.format("%s.vmp-hi.bin", wallsetName));
		FileOutputStream fos3 = new FileOutputStream(String.format("%s.vmp-screen.bin", wallsetName));
		FileOutputStream fos4 = new FileOutputStream(String.format("%s.vmp-d800.bin", wallsetName));

		int vmpSize=0;
		int yp=0;
		
		for (int n=0; n<HEIGHT/(15*8); n++) {
			int xp=0;
			for (int i=(n==0?0:1); i<(n==0?1:sizes.length); i++) {
				int w = sizes[i][0];
				int h = sizes[i][1];
				for (int x=0; x<w; x++) {
					for (int y=0; y<h; y++) {
						int offset = xp+x+(yp+y)*184/4;
						int index = 0;
						if (vmp[offset]>0) {
							index = (vmp[offset])*8 + 0x8000;
						}
						fos1.write(index&0xff);
						fos2.write((index>>8)&0xff);
						fos3.write(screen[offset]);
						fos4.write(d800[offset]);
						vmpSize++;
					}
				}
				xp+=w;
			}
			yp+=15;
		}
		fos1.close();
		fos2.close();
		fos3.close();
		fos4.close();
		System.out.printf("VMP-size is %d\n", vmpSize);
	}

	public static void main(String[] args) {
		try {
			new ConvertWallsets(args[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
