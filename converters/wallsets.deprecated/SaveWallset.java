import java.io.*;
import java.awt.image.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.*;
import javax.imageio.*;

class VCN
{
	public static final int FROM_OCS=0;
	public static final int FROM_EGA=1;
	public static final int FROM_VGA=2;
	
	private byte[] m_rawData;
	private int m_numberOfTiles;
	private byte[][] m_tiles;
	private byte[][] m_palette;
	private int m_fromMode;
	
	// VGA only
	private byte backgroundColors[];
	private byte wallColors[];

	public VCN(String wallsetName, int fromMode)
		throws IOException
	{
		String filename="";
		if (fromMode==VCN.FROM_OCS)
			filename=wallsetName+".ocn";
		else if (fromMode==VCN.FROM_EGA)
			filename=wallsetName+".ecn";
		else
			filename=wallsetName+".vcn";
		
		m_fromMode=fromMode;
		
		backgroundColors=new byte[16];
		wallColors=new byte[16];
		
		File f=new File(filename);
		long fileSize=f.length();
		m_rawData=new byte[(int)fileSize];

		FileInputStream fis=new FileInputStream(filename);
		int tr=fis.read(m_rawData);
		fis.close();

		System.out.println("Read "+tr+"/"+fileSize+" bytes from "+filename+".");

		m_numberOfTiles=((int)m_rawData[0]&0xff) | ((int)m_rawData[1]&0xff)<<8;
		m_tiles=new byte[m_numberOfTiles][64];

		System.out.println("Loaded "+m_numberOfTiles+" 8x8 tiles");

		if (fromMode==FROM_OCS)
		{
			m_palette=new byte[16][3];
			for (int i=0;i<16;i++)
			{
				int b1=m_rawData[2+i*2+0];
				int b2=m_rawData[2+i*2+1];
				int r=b1&0x0f;
				int g=(b2&0xf0)>>4;
				int b=b2&0x0f;
				r*=17;
				g*=17;
				b*=17;
				m_palette[i][0]=(byte)r;
				m_palette[i][1]=(byte)g;
				m_palette[i][2]=(byte)b;
			}
			
			for (int i=0;i<m_numberOfTiles;i++)
			{
				int offset=2+32+i*40;
				for (int y=0;y<8;y++)
				{
					for (int x=0;x<8;x++)
					{
						int bit=1<<(7-x);
						byte data=0;
						for (int plane=0;plane<5;plane++)
							data|=(m_rawData[offset+plane+y*5]&bit)==0?0:(1<<plane);
							m_tiles[i][x+y*8]=data;
					}
				}			
			}
		}
		else if (fromMode==FROM_EGA)
		{
			for (int i=0;i<m_numberOfTiles;i++)
			{
				int offset=2+32+i*32;
				for (int j=0;j<32;j++)
				{
					byte twoPixels=m_rawData[offset+j];
					m_tiles[i][j*2+0]=(byte)(((twoPixels>>4)&0x0f));
					m_tiles[i][j*2+1]=(byte)((twoPixels&0x0f));
				}
			}
		}
		else if (fromMode==FROM_VGA)
		{
			for (int i=0;i<16;i++)
			{
				backgroundColors[i] = m_rawData[2+i];
				wallColors[i] = m_rawData[2+16+i];
			}

			for (int i=0;i<m_numberOfTiles;i++)
			{
				int offset=2+32+i*32;
				for (int j=0;j<32;j++)
				{
					byte twoPixels=m_rawData[offset+j];
					m_tiles[i][j*2+0]=(byte)(((twoPixels>>4)&0x0f));
					m_tiles[i][j*2+1]=(byte)((twoPixels&0x0f));
				}
			}
		}
	}

	public int getNumberOfTiles()
	{
		return m_numberOfTiles;
	}

	public byte[] getTile(int index)
	{
		return m_tiles[index];
	}

	public byte[][] getPalette()
	{
		return m_palette;
	}
	
	public int getPaletteIndex(int i, boolean isBackground)
	{
		if (m_fromMode==FROM_VGA)
		{
			if (isBackground)
				return backgroundColors[i];
			return wallColors[i];
		}
		return i;
	}
}

class SaveWallset
{
	private static final int ZOOM=3;
	
	private VCN m_vcn;
	private byte[] m_vmp;
	private ColorModel m_palette;
	private Image[] tiles;
	private boolean[] m_partialTiles;
	private String m_wallsetName;
	
	private int m_fromMode;

	private IndexColorModel loadVGAPalette(String fileName)
	{
		byte pr[],pg[],pb[];
		pr=new byte[256];
		pg=new byte[256];
		pb=new byte[256];
		
		try
		{
			FileInputStream fis=new FileInputStream(fileName);
			
			for (int i=0;i<256;i++)
			{
				pr[i]=(byte)((fis.read()&0x3f)*255/63);
				pg[i]=(byte)((fis.read()&0x3f)*255/63);
				pb[i]=(byte)((fis.read()&0x3f)*255/63);
			}
			fis.close();
		}
		catch (IOException e)
		{
			return null;
		}		

		IndexColorModel palette=new IndexColorModel(8,256,pr,pg,pb,0);
		return palette;
	}
	
	private IndexColorModel loadEGAPalette()
	{
		byte pr[],pg[],pb[];
		pr=new byte[256];
		pg=new byte[256];
		pb=new byte[256];
		
		try
		{
			FileInputStream fis=new FileInputStream("palette.col");
			
			for (int i=0;i<256;i++)
			{
				pr[i]=(byte)((fis.read()<<2)&0xc0);
				pg[i]=(byte)((fis.read()<<2)&0xc0);
				pb[i]=(byte)((fis.read()<<2)&0xc0);
			}
			fis.close();
		}
		catch (IOException e)
		{
			return null;
		}		

		IndexColorModel palette=new IndexColorModel(8,256,pr,pg,pb,0);
		return palette;
	}

	private IndexColorModel loadDefaultPalette(byte[][] vcnPalette)
	{
		byte rawData[]=new byte[320*200/8*5+64];
		byte pr[],pg[],pb[],pa[];
		pr=new byte[256];
		pg=new byte[256];
		pb=new byte[256];
		pa=new byte[256];

		try
		{
			FileInputStream fis=new FileInputStream("invent.cp_");
			fis.read(rawData);
			fis.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

		// Create palette
		for (int i=0;i<32;i++)
		{
			int r,g,b;
			if ((i>=1) && (i<=16) && ((vcnPalette[i-1][0]!=0x00) || (vcnPalette[i-1][1]!=0x00) || (vcnPalette[i-1][2]!=0x00)))
			{
				r=(int)vcnPalette[i-1][0];
				g=(int)vcnPalette[i-1][1];
				b=(int)vcnPalette[i-1][2];
			}
			else
			{
				int b1,b2;
				b1=rawData[40000+i*2+0];
				b2=rawData[40000+i*2+1];
				r=b1&0x0f;
				g=(b2&0xf0)>>4;
				b=b2&0x0f;
				r*=17;
				g*=17;
				b*=17;
			}
			pr[i]=(byte)r;
			pg[i]=(byte)g;
			pb[i]=(byte)b;
		}

		IndexColorModel palette=new IndexColorModel(8,256,pr,pg,pb,0);
		return palette;
	}
	
	private byte[] renderWall(int baseOffset, int width, int height, int wallset, int bufx, int bufy)
	{
		byte rawPixels[]=new byte[width*8*height*8];
		for (int i=0; i<rawPixels.length; i++)
			rawPixels[i] = 0;
		
		boolean background=(wallset==0);
		boolean bgFlip=(wallset==2);

		for (int y=0;y<height;y++)
		{
			for (int x=0;x<width;x++)
			{
				int offset=2*(baseOffset+x+y*width);
				int tileIndex=((int)m_vmp[offset+1]&0xff) | ((int)m_vmp[offset+0]&0xff)<<8;

				byte pix[] = getRawTile(tileIndex, wallset==0);

				if (!background)
				{
					if (m_partialTiles[tileIndex&0x3fff])
					{
						// Fetch background block
						int bgOffset=0;
						if (bgFlip)
							bgOffset=2*(1+(21-(bufx+x))+(bufy+y)*22);
						else
							bgOffset=2*(1+(bufx+x)+(bufy+y)*22);
							
						int bgTileIndex=((int)m_vmp[bgOffset+1]&0xff) | ((int)m_vmp[bgOffset+0]&0xff)<<8;

						if (bgFlip)
							bgTileIndex^=0x4000;
						
						byte bgPix[] = getRawTile(bgTileIndex, true);

						for (int py=0;py<8;py++)
							for (int px=0;px<8;px++)
								rawPixels[(x*8+px)+(y*8+py)*width*8] = bgPix[px+py*8];
					}
				}

				for (int py=0;py<8;py++)
					for (int px=0;px<8;px++)
					{
						if (pix[px+py*8]!=0x00000000)
							rawPixels[(x*8+px)+(y*8+py)*width*8] = pix[px+py*8];
					}
			}
		}
		
		return rawPixels;
	}

	public byte[] getRawTile(int tile, boolean isBackground)
	{
		int index=tile&0x3fff;
		boolean flip=(tile&0x4000)!=0;
	
		byte raw[] = m_vcn.getTile(index);
		byte pix[] = new byte[64];
		if (flip)
		{
			for (int y=0;y<8;y++)
				for (int x=0;x<8;x++)
					pix[(7-x)+y*8] = (byte)m_vcn.getPaletteIndex(raw[x+y*8], isBackground);
		}
		else
		{
			for (int j=0;j<64;j++)
				pix[j] = (byte)m_vcn.getPaletteIndex(raw[j], isBackground);
		}
		return pix;
	}

	
	private void drawWall(BufferedImage dstImage, int bx, int by, int baseOffset, int width, int height, byte[] vmp, int wallset, int bufx, int bufy)
	{
		byte[] pixels = renderWall(baseOffset, width, height, wallset, bufx, bufy);
		byte dstPixels[] = ((DataBufferByte)dstImage.getRaster().getDataBuffer()).getData();

		for (int y=0; y<height*8; y++) {
			for (int x=0; x<width*8; x++) {
				dstPixels[x+bx*8+(y+by*8)*dstImage.getWidth()] = pixels[x+y*width*8];
			}
		}
	}
	
	private int m_tilesInVMP;
	private int m_nrWallSets;
	
	private static final int offsetTable[][]=
	{
		{331, 3,15},
		{376, 3,12},
		{412, 2,8 },
		{428, 1,5 },
		{433, 3,5 },
		{448, 2,6 },
		{460, 6,5 },
		{490,10,8 },
		{570,16,12},
	};
	
	public SaveWallset(String wallsetName, String fromMode)
		throws IOException
	{
		m_wallsetName=wallsetName;
		
		if (fromMode.equals("ocs"))
			m_fromMode=VCN.FROM_OCS;
		else if (fromMode.equals("ega"))
			m_fromMode=VCN.FROM_EGA;
		else
			m_fromMode=VCN.FROM_VGA;

		// Depack the vcn file
		m_vcn=new VCN(wallsetName, m_fromMode);

		// Load default palette but replace color indices 1-17 of the default palette from the palette in the VCN if non black
		if (m_fromMode==VCN.FROM_OCS)
			m_palette=loadDefaultPalette(m_vcn.getPalette());
		else if (m_fromMode==VCN.FROM_EGA)
			m_palette=loadEGAPalette();
		else
			m_palette=loadVGAPalette(wallsetName+".pal");

		// Load the vmp table
		String vmpName="";
		if (m_fromMode==VCN.FROM_OCS)
			vmpName=wallsetName+".omp";
		else if (m_fromMode==VCN.FROM_EGA)
			vmpName=wallsetName+".emp";
		else
			vmpName=wallsetName+".vmp";
			
		File f=new File(vmpName);
		m_vmp=new byte[(int)f.length()];
		FileInputStream fis=new FileInputStream(f);
		fis.read(m_vmp);
		fis.close();
		
		// Flip all shorts
		if (m_fromMode!=VCN.FROM_OCS)
		{
			for (int i=0;i<m_vmp.length;i+=2)
			{
				byte tmp=m_vmp[i+0];
				m_vmp[i+0]=m_vmp[i+1];
				m_vmp[i+1]=tmp;
			}
		}
		
		// Find out which tiles are partially transparent.
		m_partialTiles=new boolean[m_vcn.getNumberOfTiles()];

		for (int i=0; i<m_partialTiles.length; i++)
		{
			byte pix[] = getRawTile(i,false);
			
			int nrTransparent=0;
			for (int j=0; j<64; j++)
			{
				if (pix[j]==0)
					nrTransparent++;
			}
			m_partialTiles[i]=(nrTransparent>0)&&(nrTransparent<64);
		}
		
		// | |_  E D G
		// | |_  F C H
		//   |_    B I
		//   |     A
		// Backdrop [        1,22,15]
		// A        [331+n*431, 3,15]
		// B        [376+n*431, 3,12]
		// C        [412+n*431, 2,8 ]
		// D        [428+n*431, 1,5 ]
		// E        [433+n*431, 3,5 ]
		// F        [448+n*431, 2,6 ]
		// G        [460+n*431, 6,5 ]
		// H        [490+n*431,10,8 ]
		// I        [570+n*431,16,12]
		//
		// n = walltype-1

		m_tilesInVMP=((m_vmp[0]&0xff)<<8) | (m_vmp[1]&0xff);
		System.out.printf("%d tiles in VMP\n", m_tilesInVMP);
		m_nrWallSets=(m_tilesInVMP-22*15)/431+1;
		System.out.println("Found "+m_nrWallSets+" wall sets + background");

		final BufferedImage bi = new BufferedImage(8*46,(15*m_nrWallSets)*8, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)m_palette);
		
		int wpos[]={0,25,50,74,66,66,74,50,25};
		
		int x=0;
		int y=0;
		drawWall(bi,x,y,1,22,15,m_vmp,0,0,0);
		y+=15;
		for (int n=0; n<6; n++) {
			x=0;
			for (int i=0; i<offsetTable.length; i++) {
				int wallX=wpos[i]%22;
				int wallY=wpos[i]/22;
				drawWall(bi, x, y, offsetTable[i][0]+n*431, offsetTable[i][1], offsetTable[i][2], m_vmp, n+1, wallX, wallY);
				x+=offsetTable[i][1];							
			}
			y+=15;
		}	

		try {
			ImageIO.write(bi, "png", new File(wallsetName+".png"));
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public static void main(String args[])
	{
		if (args.length!=2)
		{
			System.out.println("Usage: java SaveWallset [brick|blue|drow|green|xanatha] [ocs|ega|vga]");
			System.exit(0);
		}
		
		try
		{
			new SaveWallset(args[0], args[1]);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
