import java.io.*;
import java.nio.*;
import java.util.*;

public class ItemDat {
	private Items items;
	public ItemDat() throws IOException {
		items = new Items();

		PrintStream out = new PrintStream(new File("itemdat.s"));
		out.println(".segment \"ITEMDAT\"");
		out.println();
/*
		ArrayList<Integer> c = new ArrayList<Integer>();
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.inventoryBits);
		AsmUtil.dumpShortLoArray(out, "itemType_inventoryBits_lo", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.inventoryBits);
		AsmUtil.dumpShortHiArray(out, "itemType_inventoryBits_hi", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.handBits);
		AsmUtil.dumpShortLoArray(out, "itemType_handBits_lo", c);
		AsmUtil.dumpShortHiArray(out, "itemType_handBits_hi", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.armourClassModifier);
		AsmUtil.dumpByteArray(out, "itemType_armourClassModifier", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.classBits);
		AsmUtil.dumpByteArray(out, "itemType_classBits", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.doubleHanded);
		AsmUtil.dumpByteArray(out, "itemType_doubleHanded", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.damageVsSmall.rolls);
		AsmUtil.dumpByteArray(out, "itemType_damageVsSmall_rolls", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.damageVsSmall.sides);
		AsmUtil.dumpByteArray(out, "itemType_damageVsSmall_sides", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.damageVsSmall.base);
		AsmUtil.dumpByteArray(out, "itemType_damageVsSmall_base", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.damageVsBig.rolls);
		AsmUtil.dumpByteArray(out, "itemType_damageVsBig_rolls", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.damageVsBig.sides);
		AsmUtil.dumpByteArray(out, "itemType_damageVsBig_sides", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.damageVsBig.base);
		AsmUtil.dumpByteArray(out, "itemType_damageVsBig_base", c);
		c.clear(); for (ItemType it : items.itemTypes) c.add(it.usage);
		AsmUtil.dumpByteArray(out, "itemType_usage", c);
*/
		AsmUtil.dumpName(out, "items");
		int cnt=0;
		for (Item item : items) {
			if (item.nameUnidentified == 0xff)
				break;
			cnt++;
				
			out.print("\t .byte ");
			
			String previous = "<$0000,>$0000";
			String next = "<$0000,>$0000";
			if (item.previous != 0)
				previous = String.format("<(items+$%03x*14), >(items+$%03x*14)", item.previous, item.previous);
			if (item.next != 0)
				previous = String.format("<(items+$%03x*14), >(items+$%03x*14)", item.next, item.next);

			out.printf("$%02x, $%02x, $%02x, $%02x, $%02x, $%02x, <$%04x, >$%04x, $%02x, $%02x, %s, %s ;%s\n",
				item.nameUnidentified&0xff,
				item.nameIdentified&0xff,
				item.flags&0xff,
				item.picture&0xff,
				item.type&0xff,
				item.subpos&0xff,
				item.position&0xffff,item.position&0xffff,
				item.level&0xff,
				item.value&0xff,
				previous,
				next,
				getItemString(item)
			);
		}

		out.println(".repeat 152");
	 	out.println("\t.byte $0c, $01, $00, $00, $00, $00, <$ffff, >$ffff, $00, $00, <$0000,>$0000, <$0000,>$0000");
	 	out.println(".endrep");

		AsmUtil.dumpName(out, "lastItem");

		out.println();
		out.println(".export itemsBaseInSave");
		out.println("itemsBaseInSave: .word items");
		out.println();

		out.println(".assert *<=$a000, error, \"itemdat must reside in $8000-$9fff\"\n");
		System.out.println(cnt);

		out.println(".segment \"ITEMFUNCS\"");

		for (int i=0; i<items.names.length; i++)
			out.printf("_n%02x: .byte \"%s\",0\n", i, items.names[i]);
		out.println(".export itemName_lo");
		out.println("itemName_lo:");
		for (int i=0; i<items.names.length; i++)
				out.printf("\t .byte <_n%02x\n", i);
		out.println(".export itemName_hi");
		out.println("itemName_hi:");
		for (int i=0; i<items.names.length; i++)
				out.printf("\t .byte >_n%02x\n", i);
		
		out.close();

		dumpItemsByUsage("Armor", 0);
		dumpItemsByUsage("Melee Weapons", 1);
		dumpItemsByUsage("Throwables", 2);
		dumpItemsByUsage("Range Weapons", 3);
		dumpItemsByUsage("Special", 4);
		dumpItemsByUsage("Spellbooks", 5);
		dumpItemsByUsage("Holy Symbols", 6);
		dumpItemsByUsage("Food", 7);
		dumpItemsByUsage("Bones", 8);
		dumpItemsByUsage("Mage Scrolls", 9);
		dumpItemsByUsage("Cleric Scrolls", 10);
		dumpItemsByUsage("Notes", 11);
		dumpItemsByUsage("Stone Items", 12);
		dumpItemsByUsage("Keys", 13);
		dumpItemsByUsage("Potions", 14);
		dumpItemsByUsage("Gems", 15);
		dumpItemsByUsage("Rings", 16);
//		dumpItemsByUsage("Not used", 17);
		dumpItemsByUsage("Wands", 18);
	}

	private String getItemString(Item item) {
		String name = items.getName(item, true);	
		return String.format("\"%s\" (%d, %d) @ %d:%d,%d:%d. Flags:$%02x", name, item.value, item.type, item.level, item.position&31, item.position/32, item.subpos, item.flags&0xff);
	}

	private void dumpItemsByUsage(String type, int usage) {
		int i=0;
		System.out.printf("; %s:\n", type);
		for (Item item : items) {
			if (((items.itemTypes[item.type].usage&0x7f) == usage) && (i!=0)) {
				String name = items.getName(item, true);
				if ((name.length()>0) && !name.equals("NULL"))
					System.out.printf(";   $%04x %s\n", i, getItemString(item));
			}
			i++;
		}
		System.out.println();
	}

	public static void main(String[] args) {
    	Global.BASE = "../../pcdata/";
		Global.byteOrder = ByteOrder.LITTLE_ENDIAN;

		try {
			new ItemDat();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
