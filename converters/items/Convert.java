import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Convert {
	private static int largeItemSizes[][] = {
		{8,3},
		{6,2},
		{4,2},
		{2,1}
	};
	
	private static int smallItemSizes[][] = {
		{4,3},
		{3,2},
		{2,2},
		{1,1}
	};
	
	private static int largeItemLocations[][] = {
		{0,0},
		{0,8},
		{0,16},
		{8,0},
		{8,8},
		{8,16},
		{16,0},
		{16,8},
		{16,16},
		{24,0},
		{24,8},
		{24,16},
		{32,0},
		{32,8},
		{32,16},
	};
	
	private static int smallItemLocations[][] = {
		{0,0},
		{0,8},
		{0,16},
		{4,0},
		{4,8},
		{4,16},
		{8,0},
		{8,8},
		{8,16},
		{12,0},
		{12,8},
		{12,16},
		{16,0},
		{16,8},
		{16,16},
		{20,0},
		{20,8},
		{20,16},
		{24,0},
		{24,8},
		{24,16},
		{28,0},
		{28,8},
		{28,16}
	};

	public static EOBBank convert(String name, int method) throws Exception {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		BufferedImage bitmap = ImageIO.read(new File(String.format("converted/%s64.png", name)));
	//
		if (method!=2)
			Declash.declash(bitmap, C64Char.spriteColors);

		ArrayList<EOBObject> eobObjects = new ArrayList<EOBObject>();

		int itemDeltaYZoomStep[] = {0,3,5,7};

		// Large
		if (method==0) {
			for (int i=0; i<14; i++) {
				for (int j=0; j<3; j++) {
					int cw = largeItemSizes[j][0];
					int ch = largeItemSizes[j][1];
					int cy = largeItemLocations[i][1]+itemDeltaYZoomStep[j];
					int cx = largeItemLocations[i][0];
					eobObjects.add(new EOBObject(bitmap, cx,cy, cw,ch));
				}
			}
		}
		// Small 
		else if (method==1) {
			for (int i=0; i<23; i++) {
				for (int j=0; j<3; j++) {
					int cw = smallItemSizes[j][0];
					int ch = smallItemSizes[j][1];
					int cy = smallItemLocations[i][1]+itemDeltaYZoomStep[j];
					int cx = smallItemLocations[i][0];
					eobObjects.add(new EOBObject(bitmap, cx,cy, cw,ch));
				}
			}
		}
		// Thrown
		else {
			for (int i=0; i<12; i++) {
				for (int j=0; j<3; j++) {
					int cw = smallItemSizes[j][0];
					int ch = smallItemSizes[j][1];
					int cy = smallItemLocations[i][1]+itemDeltaYZoomStep[j];
					int cx = smallItemLocations[i][0];
					eobObjects.add(new EOBObject(bitmap, cx,cy, cw,ch));
				}
			}

			// Burning hands
			//System.out.println("Burning hands gfx starting at index: "+eobObjects.size());
			eobObjects.add(new EOBObject(bitmap, 16,0/8,4,24/8));
			eobObjects.add(new EOBObject(bitmap, 16,24/8,4,24/8));
			eobObjects.add(new EOBObject(bitmap, 16,48/8,3,24/8));
		}

		//ImageIO.write(bitmap, "PNG", new File(String.format("%s-auto.png",name)));

		return new EOBBank(name, eobObjects);
	}

	public static void main(String[] args) {
		try {				
			EOBBank largeItems = Convert.convert("iteml1", 0);
			largeItems.dump("item_");
			
			EOBBank smallItems = Convert.convert("items1", 1);
			smallItems.dump("item_");

			EOBBank thrown = Convert.convert("thrown", 2);
			thrown.dump("item_");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}