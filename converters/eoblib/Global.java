import java.nio.*;

public class Global
{
	public static String BASE = "data/";
	public static ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;
	public static int hitColor = 31;
	public static int characterBoxesBgColor = 0x84;
	public static int characterBoxesBrightColor = 0x82;
	public static int characterBoxesDarkColor = 0x87;
	public static int characterBoxesInfoColor = 0xb4;
	public static int missBrightColor = 0x11;
	public static int missDarkColor = 0x17;
	public static int missBgColor = 0x14;
	public static int unselectedBrightColor = 0xb1;
	public static int unselectedDarkColor = 0xb8;
}
