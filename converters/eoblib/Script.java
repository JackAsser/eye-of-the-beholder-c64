import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;

class Trigger
{
  Position pos;
  int rawPos;
  int flags;
  int collapseFlags;
  int address;
  
  private static HashMap<Integer, Integer> collapseTriggerFlags;
  
  static
  {
    collapseTriggerFlags=new HashMap<Integer,Integer>();
    collapseTriggerFlags.put(0x00,0);
    collapseTriggerFlags.put(0x08,1);
    collapseTriggerFlags.put(0x18,2);
    collapseTriggerFlags.put(0x20,3);
    collapseTriggerFlags.put(0x28,4);
    collapseTriggerFlags.put(0x40,5);
    collapseTriggerFlags.put(0x48,6);
    collapseTriggerFlags.put(0x60,7);
    collapseTriggerFlags.put(0x78,8);
    collapseTriggerFlags.put(0x80,9);
    collapseTriggerFlags.put(0x88,10);
    collapseTriggerFlags.put(0xa8,11);
  }
  
  public Trigger(int rawPos, int flags, int address)
  {
    this.rawPos=rawPos;
	this.pos = new Position(this.rawPos);
    this.flags=flags;
    this.address=address;
    this.collapseFlags=collapseTriggerFlags.get(flags);
  }
  
  public Trigger(InputStream is)
    throws IOException
  {
	rawPos = (is.read() | (is.read()<<8)) & 0xffff;
	pos = new Position(rawPos);
    flags = is.read();
    address = (is.read() | (is.read()<<8)) & 0xffff;
  }
}


abstract class AbstractToken
{
  private static final int MAX_TOKEN_SIZE = 2048;

  public static final int TOKEN_SET_WALL         = 0xff;
  public static final int TOKEN_CHANGE_WALL      = 0xfe;
  public static final int TOKEN_OPEN_DOOR        = 0xfd;
  public static final int TOKEN_CLOSE_DOOR       = 0xfc;
  public static final int TOKEN_CREATE_MONSTER   = 0xfb;
  public static final int TOKEN_TELEPORT         = 0xfa;
  public static final int TOKEN_STEAL_SMALL_ITEMS= 0xf9;
  public static final int TOKEN_MESSAGE          = 0xf8;
  public static final int TOKEN_SET_FLAG         = 0xf7;
  public static final int TOKEN_SOUND            = 0xf6;
  public static final int TOKEN_CLEAR_FLAG       = 0xf5;
  public static final int TOKEN_HEAL             = 0xf4; // Not used in EOB I
  public static final int TOKEN_DAMAGE           = 0xf3;
  public static final int TOKEN_JUMP             = 0xf2;
  public static final int TOKEN_END              = 0xf1;
  public static final int TOKEN_RETURN           = 0xf0;
  public static final int TOKEN_CALL             = 0xef;
  public static final int TOKEN_CONDITIONAL      = 0xee;
  public static final int TOKEN_CONSUME          = 0xed;
  public static final int TOKEN_CHANGE_LEVEL     = 0xec;
  public static final int TOKEN_GIVE_XP          = 0xeb;
  public static final int TOKEN_NEW_ITEM         = 0xea;
  public static final int TOKEN_LAUNCHER         = 0xe9;
  public static final int TOKEN_TURN             = 0xe8;
  public static final int TOKEN_IDENTIFY_ITEMS   = 0xe7;
  public static final int TOKEN_ENCOUNTER        = 0xe6;
  public static final int TOKEN_WAIT             = 0xe5;

  public int tokenID;
  public int offset;
  public byte[] rawData;

  public static AbstractToken fromStream(ReReadInputStream rris)
    throws IOException
  {
    rris.mark(MAX_TOKEN_SIZE);

    AbstractToken token = null;

    int tokenID = rris.read();

    switch (tokenID)
    {
    case TOKEN_SET_WALL:			token = new SetWallToken(rris); break;
    case TOKEN_CHANGE_WALL:      	token = new ChangeWallToken(rris); break;
    case TOKEN_OPEN_DOOR:        	token = new OpenDoorToken(rris); break;
    case TOKEN_CLOSE_DOOR:       	token = new CloseDoorToken(rris); break;
    case TOKEN_CREATE_MONSTER:   	token = new CreateMonsterToken(rris); break;
    case TOKEN_TELEPORT:         	token = new TeleportToken(rris); break;
    case TOKEN_STEAL_SMALL_ITEMS:	token = new StealSmallItemsToken(rris); break;
    case TOKEN_MESSAGE:          	token = new MessageToken(rris); break;
    case TOKEN_SET_FLAG:         	token = new SetFlagToken(rris); break;
    case TOKEN_SOUND:            	token = new SoundToken(rris); break;
    case TOKEN_CLEAR_FLAG:       	token = new ClearFlagToken(rris); break;
//    case TOKEN_HEAL:             	token = new HealToken(rris); break;
    case TOKEN_DAMAGE:           	token = new DamageToken(rris); break;
    case TOKEN_JUMP:             	token = new JumpToken(rris); break;
    case TOKEN_END:              	token = new EndToken(rris); break;
    case TOKEN_RETURN:           	token = new ReturnToken(rris); break;
    case TOKEN_CALL:             	token = new CallToken(rris); break;
    case TOKEN_CONDITIONAL:      	token = new ConditionalToken(rris); break;
    case TOKEN_CONSUME:          	token = new ConsumeToken(rris); break;
    case TOKEN_CHANGE_LEVEL:     	token = new ChangeLevelToken(rris); break;
    case TOKEN_GIVE_XP:          	token = new GiveXPToken(rris); break;
    case TOKEN_NEW_ITEM:         	token = new NewItemToken(rris); break;
    case TOKEN_LAUNCHER:         	token = new LauncherToken(rris); break;
    case TOKEN_TURN:             	token = new TurnToken(rris); break;
    case TOKEN_IDENTIFY_ITEMS:   	token = new IdentifyItemsToken(rris); break;
    case TOKEN_ENCOUNTER:        	token = new EncounterToken(rris); break;
    case TOKEN_WAIT:             	token = new WaitToken(rris); break;
    default:
      throw new IOException(String.format("Illegal tokenID 0x%02x", tokenID));
    }

    token.tokenID = tokenID;
    token.rawData = new byte[rris.getSizeAfterMark()];

    rris.reset();
    rris.read(token.rawData);

    return token;    
  }
  
  protected static int rb(InputStream is)
    throws IOException
  {
    return is.read() & 0xff;
  }
  
  protected static int rw(InputStream is)
    throws IOException
  {
    return (is.read() | (is.read()<<8)) & 0xffff;
  }
  
  protected static String readString(InputStream is)
    throws IOException
  {
    StringBuilder sb = new StringBuilder();
    while (true)
    {
      int ch = is.read();
      if (ch==-1)
        throw new EOFException("Reached end of file while reading a null terminated string.");
      if (ch==0)
        break;
      sb.append((char)ch);
    }
    return sb.toString();
  }
  
  protected static Dice readDice(InputStream is)
    throws IOException
  {
    return new Dice(rb(is), rb(is), rb(is));
  }
}

class Position
{
  public int x;
  public int y;
  
  public Position(int pos)
  {
    if (pos==0xffff)
    {
      x=-1;
      y=-1;
    }
    else
    {  
      x=pos&31;
      y=pos/32;
    }
  }
  
  public boolean equals(Object other)
  {
    if (other==null)
      return false;
    if (!(other instanceof Position))
      return false;
    Position op = (Position)other;
    return (op.x==x) && (op.y==y);
  }
}

class Dice
{
  // AD&D dice: XtY+Z

  public int rolls; // X
  public int sides; // Y
  public int base;  // Z

  public Dice(int rolls, int sides, int base)
  {
    this.rolls = rolls;
    this.sides = sides;
    this.base = base;
  }
  
  public Dice()
  {
	this.rolls=0;
	this.sides=0;
	this.base=0;
  }
  
  public int roll()
  {
	Random r=new Random();
	int value=base;
	for (int i=0; i<rolls; i++)
		value += r.nextInt(sides)+1;

	return value;
  }
  
  public Dice(InputStream is)
	throws IOException
  {
	this.rolls = is.read();
	this.sides = is.read();
	this.base = is.read();
  }
  
  public Dice(ByteBuffer bb)
	throws IOException
  {
	this.rolls = bb.get();
	this.sides = bb.get();
	this.base = bb.get();
  }
  
  public String toString()
  {
	if (base==0)
		return String.format("%dT%d", rolls, sides);
	else if (base>0)
		return String.format("%dT%d+%d", rolls, sides, base);
	else
		return String.format("%dT%d%d", rolls, sides, base);
  }
}

class SetWallToken extends AbstractToken
{
  public static final int TYPE_ALL_SIDES				= 0xf7;
  public static final int TYPE_ONE_SIDE					= 0xe9;
  public static final int TYPE_CHANGE_PARTY_DIRECTION	= 0xed;
  
  int type = -1;
  Position pos = null;
  int to = -1;
  int side = -1;
  int direction = -1;
  
  SetWallToken(InputStream is)
    throws IOException
  {
    type = is.read();

    switch(type)
    {
    case TYPE_ALL_SIDES:
      pos = new Position(rw(is));
      to = rb(is);
      break;

    case TYPE_ONE_SIDE:
      pos = new Position(rw(is));
      side = rb(is);
      to = rb(is);
      break;

    case TYPE_CHANGE_PARTY_DIRECTION:
      direction = rb(is);
      break;

    default:
      throw new IOException(String.format("Illegal type 0x%02x", type));
    }
  }
}

class ChangeWallToken extends AbstractToken
{
  public static final int TYPE_ALL_SIDES				= 0xf7;
  public static final int TYPE_ONE_SIDE					= 0xe9;
  public static final int TYPE_OPEN_DOOR				= 0xea;
  
  int type = -1;
  Position pos = null;
  int to = -1;
  int from = -1;
  int side = -1;
  
  ChangeWallToken(InputStream is)
    throws IOException
  {
    type = is.read();

    switch(type)
    {
    case TYPE_ALL_SIDES:
      pos = new Position(rw(is));
      to = rb(is);
      from = rb(is);
      break;

    case TYPE_ONE_SIDE:
      pos = new Position(rw(is));
      side = rb(is);
      to = rb(is);
      from = rb(is);
      break;

    case TYPE_OPEN_DOOR:
      pos = new Position(rw(is));
      break;

    default:
      throw new IOException(String.format("Illegal type 0x%02x", type));
    }
  }
}

class OpenDoorToken extends AbstractToken
{
  Position pos = null;

  OpenDoorToken(InputStream is)
    throws IOException
  {
    pos = new Position(rw(is));
  }
}

class CloseDoorToken extends AbstractToken
{
  Position pos = null;

  CloseDoorToken(InputStream is)
    throws IOException
  {
    pos = new Position(rw(is));
  }
}

class CreateMonsterToken extends AbstractToken
{
  MonsterCreate monster = null;

  CreateMonsterToken(InputStream is)
    throws IOException
  {
    monster = new MonsterCreate();
    
    monster.index = rb(is);//0
    monster.levelType = rb(is);//1
	monster.position = (short)rw(is);
    monster.subpos = rb(is);//4
    monster.direction = rb(is);//5
    monster.type = rb(is);//6
    monster.picture = rb(is);//7
    monster.phase = rb(is);//8
    monster.pause = rb(is);//9
    monster.pocketItem = rw(is);//10
    monster.weapon = rw(is);//12
  }
}

class TeleportToken extends AbstractToken
{
  public static final int TYPE_PARTY	= 0xe8;
  public static final int TYPE_MONSTER	= 0xf3;
  public static final int TYPE_ITEM		= 0xf5;

  public int type = -1;
  public Position source = null;
  public Position destination = null;
  

  TeleportToken(InputStream is)
    throws IOException
  {
    type = rb(is);
    
    switch(type)
    {
    case TYPE_PARTY:
      rw(is); // TODO
      destination = new Position(rw(is));
      break;

    case TYPE_MONSTER:
    case TYPE_ITEM:    
      source = new Position(rw(is));
      destination = new Position(rw(is));
      break;

    default:
      throw new IOException(String.format("Illegal type 0x%02x", type));
    }
  }
}

class StealSmallItemsToken extends AbstractToken
{
  public static final int WHOM_RANDOM = 0xff;

  int whom = -1;
  Position pos = null;
  int subpos = -1;

  StealSmallItemsToken(InputStream is)
    throws IOException
  {
    whom = rb(is);
    pos = new Position(rw(is));
    subpos = rb(is);
  }
}

class MessageToken extends AbstractToken
{
  public String message;
  public int color;

  MessageToken(InputStream is)
    throws IOException
  {
    message = readString(is);
    color = rb(is);
    rb(is); // TODO
  }
}

class SetFlagToken extends AbstractToken
{
  public static final int TARGET_LEVEL   = 0xef;
  public static final int TARGET_GLOBAL  = 0xf0;
  public static final int TARGET_MONSTER = 0xf3;

  public int target = -1;
  public int id = -1;
  public int flag = -1;

  SetFlagToken(InputStream is)
    throws IOException
  {
    int target = rb(is);
    
    switch(target)
    {
    case TARGET_MONSTER:
      id = rb(is);

    case TARGET_LEVEL:
    case TARGET_GLOBAL:    
      flag = rb(is);
      break;
      
    default:
      throw new IOException(String.format("Illegal target 0x%02x", target));
    }
  }
}

class SoundToken extends AbstractToken
{
  int id = -1;
  Position pos = null;

  SoundToken(InputStream is)
    throws IOException
  {
    id = rb(is);
    pos = new Position(rw(is));
  }
}

class ClearFlagToken extends AbstractToken
{
  public static final int TARGET_LEVEL   = 0xef;
  public static final int TARGET_GLOBAL  = 0xf0;
  public static final int TARGET_MONSTER = 0xf3;

  public int target = -1;
  public int id = -1;
  public int flag = -1;

  ClearFlagToken(InputStream is)
    throws IOException
  {
    int target = rb(is);
    
    switch(target)
    {
    case TARGET_MONSTER:
      id = rb(is);

    case TARGET_LEVEL:
    case TARGET_GLOBAL:    
      flag = rb(is);
      break;
      
    default:
      throw new IOException(String.format("Illegal target 0x%02x", target));
    }
  }
}

class DamageToken extends AbstractToken
{
  public static final int WHOM_ALL = 0xff;

  int whom = -1;
  Dice damage = null;

  DamageToken(InputStream is)
    throws IOException
  {
    whom = rb(is);
    damage = readDice(is);
  }
}

class JumpToken extends AbstractToken
{
  int address;
  AbstractToken resolvedToken = null;

  JumpToken(InputStream is)
    throws IOException
  {
    address = rw(is);
  }
}

class EndToken extends AbstractToken
{
  EndToken(InputStream is)
    throws IOException
  {
    ;
  }
}

class ReturnToken extends AbstractToken
{
  ReturnToken(InputStream is)
    throws IOException
  {
    ;
  }
}

class CallToken extends AbstractToken
{
  int address;
  AbstractToken resolvedToken = null;

  CallToken(InputStream is)
    throws IOException
  {
    address = rw(is);
  }
}

class ConditionalOperator extends AbstractToken
{
  public static final int OPERATOR_EQ 	= 0xff;
  public static final int OPERATOR_NEQ	= 0xfe;
  public static final int OPERATOR_LT	= 0xfd;
  public static final int OPERATOR_LTE	= 0xfc;
  public static final int OPERATOR_GT	= 0xfb;
  public static final int OPERATOR_GTE	= 0xfa;
  public static final int OPERATOR_AND	= 0xf9;
  public static final int OPERATOR_OR	= 0xf8;

  public int operator = -1;
  
  public ConditionalOperator(int operator)
  {
    this.operator = operator;
  }
}

abstract class ConditionalValue extends AbstractToken
{
  public static final int VALUE_GET_WALL_NUMBER			= 0xf7;
  public static final int VALUE_COUNT_ITEMS        	 	= 0xf5;
  public static final int VALUE_COUNT_MONSTERS			= 0xf3;
  public static final int VALUE_CHECK_PARTY_POSITION 	= 0xf1;
  public static final int VALUE_GET_GLOBAL_FLAG			= 0xf0;
  public static final int VALUE_GET_LEVEL_FLAG			= 0xef;
  public static final int VALUE_END_CONDITIONAL			= 0xee;
  public static final int VALUE_GET_PARTY_DIRECTION 	= 0xed;
  public static final int VALUE_GET_WALL_SIDE			= 0xe9;
  public static final int VALUE_GET_POINTER_ITEM		= 0xe7;
  public static final int VALUE_GET_TRIGGER_FLAG		= 0xe0;
  public static final int VALUE_CONTAINS_RACE			= 0xdd;
  public static final int VALUE_CONTAINS_CLASS			= 0xdc;
  public static final int VALUE_ROLL_DICE				= 0xdb;
  public static final int VALUE_IS_PARTY_VISIBLE		= 0xda;
  public static final int VALUE_CONTAINS_ALIGNMENT		= 0xce;
  
}

class ConditionalLiteral extends AbstractToken
{
  public static final int LITERAL_FALSE = 0;
  public static final int LITERAL_TRUE	= 1;

  public int literal = -1;
  
  public ConditionalLiteral(int literal)
  {
    this.literal = literal;
  }
}

class ConditionalGetWallNumber extends ConditionalValue
{
  public Position pos = null;

  public ConditionalGetWallNumber(InputStream is)
    throws IOException
  {
    pos = new Position(rw(is));  
  }
}

class ConditionalCountItems extends ConditionalValue
{
  public int itemType = -1;
  public Position pos = null;

  public ConditionalCountItems(InputStream is)
    throws IOException
  {
    itemType = rb(is);
    pos = new Position(rw(is));
  }
}

class ConditionalCountMonsters extends ConditionalValue
{
  public Position pos = null;

  public ConditionalCountMonsters(InputStream is)
    throws IOException 
  {
    pos = new Position(rw(is));
  }
}

class ConditionalCheckPartyPosition extends ConditionalValue
{
  public Position pos = null;

  public ConditionalCheckPartyPosition(InputStream is)
    throws IOException
  {
    pos = new Position(rw(is));
  }
}

class ConditionalGetGlobalFlag extends ConditionalValue
{
  public int flag = -1;

  public ConditionalGetGlobalFlag(InputStream is)
    throws IOException
  {
     flag = rb(is); 
  }
}

class ConditionalEnd extends AbstractToken
{
  public ConditionalEnd(InputStream is)
  {
    ;
  }
}

class ConditionalGetPartyDirection extends ConditionalValue
{
  public ConditionalGetPartyDirection(InputStream is)
  {
    ;
  }
}

class ConditionalGetWallSide extends ConditionalValue
{
  int side = -1;
  Position pos = null;

  public ConditionalGetWallSide(InputStream is)
    throws IOException
  {
    side = rb(is);
    pos = new Position(rw(is));  
  }
}

class ConditionalGetPointerItem extends ConditionalValue
{
  public static final int ITEM_UNKNOWN 	= 0xf5;
  public static final int ITEM_VALUE	= 0xf6;
  public static final int ITEM_TYPE 	= 0xe1;

  public int queryType = -1;

  public ConditionalGetPointerItem(InputStream is)
    throws IOException
  {
    queryType = rb(is);
  }
}

class ConditionalGetTriggerFlag extends ConditionalValue
{
  public ConditionalGetTriggerFlag(InputStream is)
  {
    ;
  }
}

class ConditionalContainsRace extends ConditionalValue
{
  int race = -1;

  public ConditionalContainsRace(InputStream is)
    throws IOException
  {
    race = rb(is);
  }
}

class ConditionalContainsClass extends ConditionalValue
{
  int clazz = -1;

  public ConditionalContainsClass(InputStream is)
    throws IOException
  {
    clazz = rb(is);
  }
}

class ConditionalRollDice extends ConditionalValue
{
  Dice dice = null;

  public ConditionalRollDice(InputStream is)
    throws IOException
  {
    dice = readDice(is);
  }
}

class ConditionalIsPartyVisible extends ConditionalValue
{
  public ConditionalIsPartyVisible(InputStream is)
  {
    ;
  }
}

class ConditionalContainsAlignment extends ConditionalValue
{
  int alignment = -1;

  public ConditionalContainsAlignment(InputStream is)
    throws IOException
  {
    alignment = rb(is);
  }
}

class ConditionalGetLevelFlag extends ConditionalValue
{
  int flag = -1;

  public ConditionalGetLevelFlag(InputStream is)
    throws IOException
  {
    flag = rb(is);  
  }
}

class ConditionalToken extends AbstractToken
{
  public ArrayList<AbstractToken> operations = null;
  public int falseAddress = -1;
  public AbstractToken resolvedFalseToken = null;

  ConditionalToken(InputStream is)
    throws IOException
  {
    operations = new ArrayList<AbstractToken>();

    while (true)
    {
      AbstractToken operation = null;
    
      int operationID = rb(is);

      switch (operationID)
      {
        case ConditionalOperator.OPERATOR_EQ:			operation = new ConditionalOperator(operationID); break;
        case ConditionalOperator.OPERATOR_NEQ:			operation = new ConditionalOperator(operationID); break;
        case ConditionalOperator.OPERATOR_LT:			operation = new ConditionalOperator(operationID); break;
        case ConditionalOperator.OPERATOR_LTE:			operation = new ConditionalOperator(operationID); break;
        case ConditionalOperator.OPERATOR_GT:			operation = new ConditionalOperator(operationID); break;
        case ConditionalOperator.OPERATOR_GTE:			operation = new ConditionalOperator(operationID); break;
        case ConditionalOperator.OPERATOR_AND:			operation = new ConditionalOperator(operationID); break;
        case ConditionalOperator.OPERATOR_OR:			operation = new ConditionalOperator(operationID); break;
        case ConditionalValue.VALUE_GET_WALL_NUMBER:	operation = new ConditionalGetWallNumber(is); break;
        case ConditionalValue.VALUE_COUNT_ITEMS:		operation = new ConditionalCountItems(is); break;
        case ConditionalValue.VALUE_COUNT_MONSTERS:		operation = new ConditionalCountMonsters(is); break;
        case ConditionalValue.VALUE_CHECK_PARTY_POSITION:operation = new ConditionalCheckPartyPosition(is); break;
        case ConditionalValue.VALUE_GET_GLOBAL_FLAG:	operation = new ConditionalGetGlobalFlag(is); break;
        case ConditionalValue.VALUE_END_CONDITIONAL:	operation = new ConditionalEnd(is); break;
        case ConditionalValue.VALUE_GET_PARTY_DIRECTION:operation = new ConditionalGetPartyDirection(is); break;
        case ConditionalValue.VALUE_GET_WALL_SIDE:		operation = new ConditionalGetWallSide(is); break;
        case ConditionalValue.VALUE_GET_POINTER_ITEM:	operation = new ConditionalGetPointerItem(is); break;
        case ConditionalValue.VALUE_GET_TRIGGER_FLAG:	operation = new ConditionalGetTriggerFlag(is); break;
        case ConditionalValue.VALUE_CONTAINS_RACE:		operation = new ConditionalContainsRace(is); break;
        case ConditionalValue.VALUE_CONTAINS_CLASS:		operation = new ConditionalContainsClass(is); break;
        case ConditionalValue.VALUE_ROLL_DICE:			operation = new ConditionalRollDice(is); break;
        case ConditionalValue.VALUE_IS_PARTY_VISIBLE:	operation = new ConditionalIsPartyVisible(is); break;
        case ConditionalValue.VALUE_CONTAINS_ALIGNMENT:	operation = new ConditionalContainsAlignment(is); break;
        case ConditionalValue.VALUE_GET_LEVEL_FLAG:     operation = new ConditionalGetLevelFlag(is); break;

      default:
        if (operationID>=0x80)
          throw new IOException(String.format("Unknown operationID 0x%02x",operationID));
        else
           operation = new ConditionalLiteral(operationID);
      }

      operations.add(operation);

      if (operation instanceof ConditionalEnd)
        break;
    }
    
    falseAddress = rw(is);
  }
}

class ConsumeToken extends AbstractToken
{
  public static final int LOCATION_MOUSE_POINTER		= 0xff;
  public static final int LOCATION_AT_POSITION			= 0xfe;
  public static final int LOCATION_AT_POSITION_BY_TYPE	= 0;

  public int location = -1;
  public Position pos = null;
  public int type = -1;

  ConsumeToken(InputStream is)
    throws IOException
  {
    int tmp = rb(is);
    switch (tmp)
    {
    case LOCATION_MOUSE_POINTER:
      location = tmp;
      break;
      
    case LOCATION_AT_POSITION:
      location = tmp;
      pos = new Position(rw(is));
      break;
      
    default:
      location = LOCATION_AT_POSITION_BY_TYPE;
      type = tmp;
      pos = new Position(rw(is));
      break;
    }
  }
}

class ChangeLevelToken extends AbstractToken
{
  public static final int TYPE_INTER_LEVEL		= 0xe5;
  public static final int TYPE_INTRA_LEVEL		= 0x00;
  
  public int type = -1;
  public int level = -1;
  public Position pos = null;
  public int direction = -1;

  ChangeLevelToken(InputStream is)
    throws IOException
  {
    type = rb(is);
    
    if (type == TYPE_INTER_LEVEL)
    {
      level = rb(is);
      pos = new Position(rw(is));
      direction = rb(is);
    }
    else
    {
      type = TYPE_INTRA_LEVEL;
      direction = rb(is);
      pos = new Position(rw(is));
    }
  }
}

class GiveXPToken extends AbstractToken
{
  public static final int TYPE_TO_ALL = 0xe2;

  int type = -1;
  int amount = -1;

  GiveXPToken(InputStream is)
    throws IOException
  {
    type=rb(is);
  
    if (type == TYPE_TO_ALL)
      amount=rw(is);
    else
      throw new IOException(String.format("Unknown type %02x", type));
  }
}

class NewItemToken extends AbstractToken
{
  int itemIndex = -1;
  Position pos = null;
  int subpos = -1;

  NewItemToken(InputStream is)
    throws IOException
  {
    itemIndex = rw(is);
    pos = new Position(rw(is));
    subpos = rb(is);
  }
}

class LauncherToken extends AbstractToken
{
  public static final int KIND_SPELL	= 0xdf;
  public static final int KIND_ITEM		= 0xec;

  int kind = -1;
  int itemIndex = -1;
  Position pos = null;
  int direction = -1;
  int subpos = -1;

  LauncherToken(InputStream is)
    throws IOException
  {
    kind = rb(is);
    itemIndex = rw(is);
    pos = new Position(rw(is));
    direction = rb(is);
    subpos = rb(is);
    
    switch(kind)
    {
    case KIND_SPELL:
    case KIND_ITEM:
      break;
    
    default:
      throw new IOException(String.format("Unknown kind %02x", kind));
    }
  }
}

class TurnToken extends AbstractToken
{
  public static final int TYPE_PARTY	= 0xf1;
  public static final int TYPE_ITEM		= 0xf5;

  int type = -1;
  int turnAmount = -1;

  TurnToken(InputStream is)
    throws IOException
  {
    type = rb(is);
    turnAmount = rb(is);
    
    switch (type)
    {
    case TYPE_PARTY:
    case TYPE_ITEM:
      break;
    default:
      throw new IOException(String.format("Unknown type %02x", type));
    }
  }
}

class IdentifyItemsToken extends AbstractToken
{
  Position pos = null;

  IdentifyItemsToken(InputStream is)
    throws IOException
  {
    pos = new Position(rw(is));
  }
}

class EncounterToken extends AbstractToken
{
  int index = -1;

  EncounterToken (InputStream is)
    throws IOException
  {
    index = rb(is);
  }
}

class WaitToken extends AbstractToken
{
  int ticks = -1;

  WaitToken (InputStream is)
    throws IOException
  {
    ticks = rw(is);
  }
}

public class Script
{
  public ArrayList<AbstractToken> tokens;
  public Trigger triggers[];

  public Script(Inf inf)
    throws IOException
  {
    tokens = new ArrayList<AbstractToken>();
    
    ByteArrayInputStream bais = new ByteArrayInputStream(inf.rawScript);
    ReReadInputStream rris = new ReReadInputStream(bais);

    int totalRead=0;
    while (totalRead < inf.rawScript.length)
    {
      AbstractToken token = AbstractToken.fromStream(rris);
      token.offset=totalRead;
      tokens.add(token);
      totalRead += token.rawData.length; 
    }
    bais.close();
      
    if (totalRead != inf.rawScript.length)
      throw new IOException(String.format("Error: read %d too much\n", inf.rawScript.length - totalRead));

    bais = new ByteArrayInputStream(inf.rawTriggers);

    triggers=new Trigger[(bais.read() | (bais.read()<<8)) & 0xffff];
    for (int i=0; i<triggers.length; i++)
      triggers[i]=new Trigger(bais);

	Arrays.sort(triggers, new Comparator<Trigger>()
	{
		public int compare(Trigger ta, Trigger tb)
		{
			if (ta.pos.y < tb.pos.y)
				return -1;
			else if (ta.pos.y > tb.pos.y)
				return 1;
			else if (ta.pos.x < tb.pos.x)
				return -1;
			else if (ta.pos.x > tb.pos.x)
				return 1;
			return 0;
		}
	});    
    bais.close();
  }
  
  public static void main(String[] arg)
  {
    Global.BASE="../data/";
  
    try
    {
      Inf inf = new Inf(Integer.parseInt(arg[0]));
      Script script = new Script(inf);
    
      System.out.printf("%d tokens found.\n", script.tokens.size());

      for (AbstractToken token : script.tokens)
      {
        System.out.printf("%04x: %s\n", token.offset, token.getClass().getName());
        if (token instanceof ConditionalToken)
        {
          ConditionalToken ct = (ConditionalToken)token;
          int stackSize = 0;
          for (AbstractToken operation : ct.operations)
          {
            System.out.printf("     >");
            for (int i=0; i<stackSize; i++)
              System.out.print("   ");
            System.out.printf("%s\n", operation.getClass().getName());
            if (operation instanceof ConditionalOperator)
              stackSize--;
            else if (operation instanceof ConditionalValue)
              stackSize++;
            else if (operation instanceof ConditionalLiteral)
              stackSize++;
            else if (operation instanceof ConditionalEnd)
              stackSize--;
          }
          
          if (stackSize!=0)
          {
            System.err.println("Stack Error! "+stackSize);
            System.exit(1);
          }
        }
      }
      
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
}
