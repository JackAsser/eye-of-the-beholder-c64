import java.io.*;
import java.util.*;

class StaticItemData implements Comparable
{
	byte nameUnidentified;
	byte nameIdentified;
	byte picture;
	byte type;
	byte value;

	public StaticItemData(Item i)
	{
		nameUnidentified = (byte)i.nameUnidentified;
		nameIdentified = (byte)i.nameIdentified;
		picture = (byte)i.picture;
		type = (byte)i.type;
		value = (byte)i.value;
	}
	
	public boolean equals(Object o)
	{
		if (o==null)
			return false;
		if (!(o instanceof StaticItemData))
			return false;
			
		StaticItemData other = (StaticItemData)o;
			
		return (other.nameUnidentified==nameUnidentified) && (other.nameIdentified==nameIdentified) && (other.picture==picture) && (other.type==type) && (other.value==value);
	}
	
	public int hashCode()
	{
		return (nameUnidentified<<24) | (nameIdentified<<16) | (picture<<8) | type + value*0x12345678;
	}
	
	public int compareTo(Object o)
	{
		if (o==null)
			return 0;
		if (!(o instanceof StaticItemData))
			return 0;

		StaticItemData other = (StaticItemData)o;
		
		return String.format("%02x %02x %02x %02x %02x", nameUnidentified, nameIdentified, picture, type, value).compareTo(
					 String.format("%02x %02x %02x %02x %02x", other.nameUnidentified, other.nameIdentified, other.picture, other.type, other.value));
	}
}

public class ItemTest
{
	PrintStream out;
	
	private void compressStrings(ArrayList<String> strings)
	{
			//	00000000001111111111222222222233
			//	01234567890123456789012345678901
			// " ABCDEFGHIJKLMNOPQRSTUVWXYZ,.'! "
			String xlate="\0ABCDEFGHIJKLMNOPQRSTUVWXYZ,.'! ";
		int input[]=new int[8];
			int output[]=new int[5];

			StringBuilder sb=new StringBuilder();
			int starts[]=new int[strings.size()];
			for (int i=0; i<strings.size(); i++)
			{
				String s = strings.get(i).toUpperCase();
				starts[i] = sb.length();
				sb.append(s);
				sb.append('\0');
//				System.out.printf("%4d: +$%04x %s\n", i, starts[i], s);
			}
		String s = sb.toString();
		
		int rlen=(s.length()+1+7)&0xfffffff8;

		out.printf(".export itemStringLo\nitemStringLo:\n");
		for (int i=0; i<strings.size(); i++)
		{
			int offset = starts[i];
			int boundary = offset&0xfffffff8;
			int subOffset = 7-(offset-boundary);
			boundary/=8;
			boundary*=5;
			
			if (boundary>4095)
				System.err.println("ERROR: Too big string buffer!");
			
			int address = boundary | (subOffset<<12);
				
				if ((i&15)==0)
					out.printf("\t .byte ");
				
				out.printf("<$%04x", address);
				
				if ( (i==strings.size()-1) || ((i&15)==15) )
					out.println();
				else
					out.printf(",");				
		}
		out.println();
		
		out.printf(".export itemStringHi\nitemStringHi:\n");
		for (int i=0; i<strings.size(); i++)
		{
			int offset = starts[i];
			int boundary = offset&0xfffffff8;
			int subOffset = 7-(offset-boundary);
			boundary/=8;
			boundary*=5;
			int address = boundary | (subOffset<<12);
				
				if ((i&15)==0)
					out.printf("\t .byte ");
				
				out.printf(">$%04x", address);
				
				if ( (i==strings.size()-1) || ((i&15)==15) )
					out.println();
				else
					out.printf(",");
		}
		out.println();

		out.printf(".export itemStringBuffer\nitemStringBuffer:\n");
			for (int off=0; off<rlen; off+=8)
			{
					if ((off%32)==0)
							out.printf("\t .byte ");

					for (int i=0; i<8; i++)
					{
							int pos=0;
				if (off+i<s.length())
				{
				for (;pos<32;pos++)
					if (s.charAt(off+i) == xlate.charAt(pos))
											break;
				}
							input[i]=pos;
					}

			Arrays.fill(output,0);
					for (int outbit=0; outbit<5; outbit++)
					{
							for (int inbit=0; inbit<8; inbit++)
							{
				int i = (input[7-inbit]&(1<<(4-outbit))) == 0 ? 0:1;
									output[outbit] |= i << inbit;
							}
					}
					for (int i=0; i<5; i++)
					{
							out.printf("$%02x", output[i]);
							if (i<4)
									out.printf(",");
					}
					
					if ((off%32)==24)
						out.println();
					else
					{
						if (off<rlen-8)
							out.printf(",");
						else
							out.println();
					}
			}
			out.println();
	}

	public String bitstring(int value, int nrbits)
	{
		StringBuffer sb=new StringBuffer();
		for (int i=0; i<nrbits; i++)
		{
			int bit = value&(1<<(nrbits-1));
			if (bit!=0)
				bit=1;
			sb.append(bit);
			value<<=1;
		}
		return sb.toString();
	}
	
	public ItemTest(int level)
		throws IOException
	{
		Items items = new Items();

		ArrayList<Item> itemsOnLevel = new ArrayList<Item>();
		for (int i=0; i<items.size(); i++)
//				if (items.get(i).level==level)
				itemsOnLevel.add(items.get(i));
				
		System.out.printf("%d/%d items located on level %d:\n", itemsOnLevel.size(), items.size(), level);
		System.out.printf("\t#### [ x,y ]-sp Fl Ch Tp Va Hand bits                     Unidentified name | Identified name\n");
		System.out.printf("\t--------------------------------------------------------------------------------------------------\n");
		for (Item item : itemsOnLevel)
			System.out.printf("\t%04x [%2d,%2d]-%s %02x %02x %02x %s (%d) %35s | %s (%s %s)\n",
				item.index,
				item.position&0x1f, item.position/0x20, item.subposText[item.subpos],
				item.flags, item.type, item.value,
				bitstring(items.itemTypes[item.type].classBits, 16),
				(byte)items.itemTypes[item.type].classBits,
				items.getName(item, false), items.getName(item, true),				
				items.itemTypes[item.type].damageVsSmall,
				items.itemTypes[item.type].damageVsBig);

		FileOutputStream fos=new FileOutputStream("items.s");
		out=new PrintStream(fos);
		out.printf(".segment \"ITEM_DAT\"\n\n");
		
		// Filter out all the used strings and update the items accordingly to point into this instead
		ArrayList<String> usedStrings = new ArrayList<String>();

		usedStrings.add("");
		usedStrings.add("Mouse Pointer");
		usedStrings.add("NULL");

		for (Item item : items)
		{
			int i;
			String s;
			
			s = items.names[item.nameUnidentified];
			i = usedStrings.indexOf(s);
			
			if (i==-1)
			{
				i=usedStrings.size();
				usedStrings.add(s);
			}
			i-=3;
			if (i<0)
				i=0xff;
			item.nameUnidentified = i;

			s = items.names[item.nameIdentified];
			if (s.equals("Ring of Protection +2"))
				s = "Ring of Extra Protection";

			i = usedStrings.indexOf(s);
			
			if (i==-1)
			{
				i=usedStrings.size();
				usedStrings.add(s);
			}
			i-=3;
			if (i<0)
				i=0xff;
			item.nameIdentified = i;
		}

		usedStrings.remove(2);
		usedStrings.remove(1);
		usedStrings.remove(0);

		int rings=usedStrings.size();
		for (String s : items.rings)
			usedStrings.add(s);

		int clericScrolls=usedStrings.size();
		for (String s : items.clericScrolls)
			usedStrings.add(s);
			
		int mageScrolls=usedStrings.size();
		for (String s : items.mageScrolls)
			usedStrings.add(s);
			
		int potions=usedStrings.size();
		for (String s : items.potions)
			usedStrings.add(s);			
			
		int wands=usedStrings.size();
		for (String s : items.wands)
			usedStrings.add(s);						

		int of=usedStrings.size();
		usedStrings.add(" OF ");
		
		int taken=usedStrings.size();
		usedStrings.add(" TAKEN");

		int potionOf=usedStrings.size();
		usedStrings.add("POTION OF ");
		
		int mageScrollOf=usedStrings.size();
		usedStrings.add("MAGE SCROLL OF ");
		
		int clericScrollOf=usedStrings.size();
		usedStrings.add("CLERIC SCROLL OF ");		
		
		int ringOf=usedStrings.size();
		usedStrings.add("RING OF ");				
		
		int wandOf=usedStrings.size();
		usedStrings.add("WAND OF ");
		
		int youCantPut=usedStrings.size();
		usedStrings.add("YOU CAN'T PUT THAT ITEM THERE.");
		
		System.out.printf(".define MSG_RINGS					$%02x\n", rings);
		System.out.printf(".define MSG_CLERIC_SCROLLS $%02x\n", clericScrolls);
		System.out.printf(".define MSG_MAGE_SCROLLS	 $%02x\n", mageScrolls);
		System.out.printf(".define MSG_POTIONS				$%02x\n", potions);
		System.out.printf(".define MSG_WANDS					$%02x\n", wands);
		System.out.printf(".define MSG_OF						 $%02x\n", of);
		System.out.printf(".define MSG_TAKEN					$%02x\n", taken);
		System.out.printf(".define MSG_POTION_OF			$%02x\n", potionOf);
		System.out.printf(".define MSG_MAGE_SCROLL_OF $%02x\n", mageScrollOf);
		System.out.printf(".define MSG_CLERIC_SCROLL_OF $%02x\n", clericScrollOf);
		System.out.printf(".define MSG_RING_OF				$%02x\n", ringOf);
		System.out.printf(".define MSG_WAND_OF				$%02x\n", wandOf);
		System.out.printf(".define MSG_YOUCANTPUT			$%02x\n", youCantPut);
		
		compressStrings(usedStrings);
		
		while (items.size()!=512)
			items.add(new Item());
		
		// Filter out all static item data
		ArrayList<StaticItemData> sids=new ArrayList<StaticItemData>();
		HashMap<Item, Integer> item2sidIndex=new HashMap<Item, Integer>();
		for (Item item : items)
		{
			if ((item.nameUnidentified==0xff) && (item.index>0))
			{
				item2sidIndex.put(item,0xff);
				continue;
			}
			
			StaticItemData sid = new StaticItemData(item);
			int index=sids.indexOf(sid);
			if (index==-1)
			{
				index=sids.size();
				sids.add(sid);
			}
			item2sidIndex.put(item,index);
		}
		
		out.printf(".export item_static_name_unidentified\nitem_static_name_unidentified:\n");
		for (int i=0; i<sids.size(); i++)
		{
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", sids.get(i).nameUnidentified);
			if (((i&15)==15) || (i==sids.size()-1))
				out.println();
			else
				out.print(","); 
		}
		out.println();
		
		out.printf(".export item_static_name_identified\nitem_static_name_identified:\n");
		for (int i=0; i<sids.size(); i++)
		{
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", sids.get(i).nameIdentified);
			if (((i&15)==15) || (i==sids.size()-1))
				out.println();
			else
				out.print(","); 
		}
		out.println();
		
		out.printf(".export item_static_picture\nitem_static_picture:\n");
		for (int i=0; i<sids.size(); i++)
		{
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", sids.get(i).picture);
			if (((i&15)==15) || (i==sids.size()-1))
				out.println();
			else
				out.print(","); 
		}
		out.println(); 
		
		out.printf(".export item_static_type\nitem_static_type:\n");
		for (int i=0; i<sids.size(); i++)
		{
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", sids.get(i).type);
			if (((i&15)==15) || (i==sids.size()-1))
				out.println();
			else
				out.print(","); 
		}
		out.println(); 
		
		out.printf(".export item_static_value\nitem_static_value:\n");
		for (int i=0; i<sids.size(); i++)
		{
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", sids.get(i).value);
			if (((i&15)==15) || (i==sids.size()-1))
				out.println();
			else
				out.print(","); 
		}
		out.println(); 
		
		out.printf(".export item_static\nitem_static:\n");
		for (int i=0; i<items.size(); i++)
		{
			Item item=items.get(i);
			int sidIndex=item2sidIndex.get(item).intValue();
			
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", sidIndex);
			if (((i&15)==15) || (i==items.size()-1))
				out.println();
			else
				out.print(","); 
		}
		out.println();

		out.printf(".export item_type_to_inventory_usage\nitem_type_to_inventory_usage:\n");
		out.print("\t.byte ");
		for (int i=0; i<items.itemTypes.length; i++)
		{
			int value = items.itemTypes[i].inventoryBits;
			if (value==8)
				value=0xff; // Handle backpack-only bits separatly
			else
			{
				value &= ~8; // Remove backpack-bit, it's quite obvious all items can be put there....		
				if ((value&0x100)==0x100) // Reuse backpack-bit as ring-bit instead to make it fit into 8-bits
				{
					value&=~0x100;
					value|=8;
				}
			}
			
			out.printf("$%02x", value);
		
			if (i<items.itemTypes.length-1)
				out.print(",");
			else
				out.println();
		}
		out.println();
		
		out.println(".segment \"SAVEGAME\"");

		out.printf(".export item_flags\nitem_flags:\n");
		for (int i=0; i<items.size(); i++)
		{
			Item item=items.get(i);
		
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", item.flags);
			if (((i&15)==15) || (i==items.size()-1))
				out.println();
			else
				out.print(","); 
		}
		out.println();
		
		/*
		out.printf(".export item_subpos\nitem_subpos:\n");
		for (int i=0; i<items.size(); i++)
		{
			Item item=items.get(i);
		
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", (item.level<<4)|item.subpos);
			if (((i&15)==15) || (i==items.size()-1))
				out.println();
			else
				out.print(","); 
		}
		out.println();
		*/
		
		// Now create linked lists for all items per level
		out.printf(".export item_level_start\nitem_level_start:\n");
		int nextLink[] = new int[items.size()];
		for (int l=1; l<13; l++)
		{	
			ArrayList<Integer> list=new ArrayList<Integer>();
			for (Item item : items)
				if (item.level==l)
					list.add(item.index);
			out.printf("\t.word $%04x; size=%d\n", list.get(0), list.size());
			
			for (int i=0; i<list.size()-1; i++)
			{
				int current = list.get(i);
				int next = list.get(i+1);
				nextLink[current] = next;
			}
			nextLink[list.get(list.size()-1)] = 0;
		}
		out.println();
		
		out.printf(".export item_maze_x\nitem_maze_x:\n");
		for (int i=0; i<items.size(); i++)
		{
			Item item=items.get(i);
		
			int subpos=item.subpos;
			if (subpos==8)
				subpos=4;
		
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x|$%02x", item.position&0x1f, subpos<<5);
			if (((i&15)==15) || (i==items.size()-1))
				out.println();
			else
				out.print(", "); 
		}
		out.println();
		
		out.printf(".export item_maze_y\nitem_maze_y:\n");
		for (int i=0; i<items.size(); i++)
		{
			Item item=items.get(i);
		
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x|$%02x", (item.position/0x20)&0x1f, (nextLink[i]&0x100)>>1);
			if (((i&15)==15) || (i==items.size()-1))
				out.println();
			else
				out.print(", "); 
		}
		out.println();
		
		out.printf(".export item_next\nitem_next:\n");
		for (int i=0; i<items.size(); i++)
		{
			Item item=items.get(i);
		
			if ((i&15)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", nextLink[i]&0xff);
			if (((i&15)==15) || (i==items.size()-1))
				out.println();
			else
				out.print(","); 
		}
		out.println();
		
		out.close();
		fos.close();

/*
		// Game start: Level 1: Xpos=10, Ypos=15, Dir=North
		
		HashSet<Byte> values = new HashSet<Byte>();
		for (Item item :	items)
			values.add(item.value);
		System.out.printf("Possible values (%d):\n", values.size());
//		for (Integer value : values)
//			System.out.printf("\t%02x\n", value);
			
		HashSet<Integer> flags = new HashSet<Integer>();
		for (Item item :	items)
			flags.add(item.flags);
		System.out.printf("Possible flags (%d):\n", flags.size());
//		for (Integer flag : flags)
//			System.out.printf("\t%02x\n", flag);
			
		HashSet<Integer> subposes = new HashSet<Integer>();
		for (Item item :	items)
			subposes.add(item.subpos);
		System.out.printf("Possible subposes (%d):\n", subposes.size());
//		for (Integer subpos : subposes)
//			System.out.printf("\t%02x\n", subpos);

		HashSet<Integer> chargeses = new HashSet<Integer>();
		for (Item item :	items)
			chargeses.add(item.charges);
		System.out.printf("Possible chargeses (%d):\n", chargeses.size());
		for (Integer charges : chargeses)
			System.out.printf("\t%02x\n", charges);
			
		int xposes = 32;
		int yposes = 32;
		int levels = 12+1;
		int nexts = items.size();

		int bits=0;
		bits += (int)Math.ceil(Math.log(values.size())/Math.log(2.0));
		bits += (int)Math.ceil(Math.log(flags.size())/Math.log(2.0));
		bits += (int)Math.ceil(Math.log((double)xposes)/Math.log(2.0));
		bits += (int)Math.ceil(Math.log((double)yposes)/Math.log(2.0));
		bits += (int)Math.ceil(Math.log((double)levels)/Math.log(2.0));
		bits += (int)Math.ceil(Math.log((double)nexts)/Math.log(2.0));		
		System.out.printf(bits+" bits needed\n");

		int flagBits=0;
		for (Item item : items)
			flagBits|=item.flags;
		System.out.printf("Flag bits used: %%");
		for (int i=7; i>=0; i--)
			System.out.print((flagBits&(1<<i))==0?"0":"1");
		System.out.println();
*/
	}

	public static void main(String[] args)
	{
		Global.BASE="../data/";
	
		try
		{
			new ItemTest(Integer.parseInt(args[0]));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
