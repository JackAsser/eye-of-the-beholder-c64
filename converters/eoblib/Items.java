import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.*;

/*
  String expansion based on type and value:

  Type
  00 name +item.value
  01 name +item.value
  02 name +item.value
  03 name +item.value
  04 name +item.value
  05 name +item.value
  06 Potion of potions[item.value]
  07 name +item.value
  08 name
  09 name +item.value
  0a name +item.value
  0b name +item.value
  0c name +item.value
  0d name +item.value
  0e name +item.value
  0f name +item.value
  10 name +item.value
  11 name +item.value
  12 name +item.value
  13 name +item.value
  14 name +item.value
  15 name +item.value
  16 name +item.value
  17 name +item.value
  18 name +item.value
  19 name +item.value
  1a name +item.value
  1b name +item.value
  1c name
  1d name
  1e name
  1f name
  20 name +item.value
  21 name
  22 Mage scroll of mageScrolls[item.value]
  23 Cleric scroll of clericScrolls[item.value]
  24 name
  25 name
  26 name
  27 Potion of potions[item.value]
  28 name
  29 name +item.value
  2a name +item.value
  2b name +item.value
  2c name
  2d name +item.value
  2e name
  2f Ring of rings[item.value]
  30 Wand of wands[item.value]
  31 name

  rings[]
  00 = Adornment
  01 = Wizardry
  02 = Sustenance
  03 = Feather Fall
  
  clericScrolls[]
  00 = 
  01 = Bless
  02 = Cure Light Wnds
  03 = Cause Light Wnds
  04 = Detect Magic
  05 = Protect-Evil
  06 = Aid
  07 = Flame Blade
  08 = Hold Person
  09 = Slow Poison
  0a = Create Food
  0b = Dispel Magic
  0c = Magical Vestment
  0d = Prayer
  0e = Remove Paralysis
  0f = Cure Serious
  10 = Cause Serious
  11 = Neutral-Poison
  12 = Protect-Evil 10'
  13 = Protect-Lightning
  14 = Cure Critical
  15 = Cause Critical
  16 = Flame Strike
  17 = Raise Dead
  18 = Lay on Hands
  
  mageScrolls[]
  00 = 
  01 = Armor
  02 = Burning Hands
  03 = Detect Magic
  04 = Magic Missile
  05 = Read Magic
  06 = Shield
  07 = Shocking Grasp
  08 = Invisibility
  09 = Knock
  0a = M's Acid Arrow
  0b = Stinking Cloud
  0c = Dispel Magic
  0d = Fireball
  0e = Flame Arrow
  0f = Haste
  10 = Hold Person
  11 = Invisibility 10'
  12 = Lightning Bolt
  13 = Vampiric Touch
  14 = Fear
  15 = Ice Storm
  16 = Stoneskin
  17 = Cloudkill
  18 = Cone of Cold
  19 = Hold Monster

  potions[]
  00 =
  01 = Giant Strength
  02 = Healing
  03 = Extra Healing
  04 = Poison
  05 = Vitality
  06 = Speed
  07 = Invisibility
  08 = Cure Poison
  
  wands[]
  00 = 
  01 = Lightning
  02 = Frost
  03 = Curing
  04 = Fireball
  05 = Silvias
  06 = Magic Missile
*/

public class Items extends ArrayList<Item>
{
  public String[] names;

  public static final String[] rings =
  {
    "Adornment",
    "Wizardry",
    "Sustenance",
    "Feather Fall"
  };
  
  public static final String[] clericScrolls =
  {
  	"",
  	"Bless",
  	"Cure Light Wnds",
  	"Cause Light Wnds",
  	"Detect Magic",
  	"Protect Evil",
  	"Aid",
  	"Flame Blade",
  	"Hold Person",
  	"Slow Poison",
  	"Create Food",
  	"Dispel Magic",
  	"Magical Vestment",
  	"Prayer",
  	"Remove Paralysis",
  	"Cure Serious",
  	"Cause Serious",
  	"Neutral Poison",
  	"Protect Evil Ten'",
  	"Protect Lightning",
  	"Cure Critical",
  	"Cause Critical",
  	"Flame Strike",
  	"Raise Dead",
  	"Lay on Hands"
  };
  
  public static final String[] mageScrolls =
  {
  	"",
  	"Armor",
  	"Burning Hands",
  	"Detect Magic",
  	"Magic Missile",
  	"Read Magic",
  	"Shield",
  	"Shocking Grasp",
  	"Invisibility",
  	"Knock",
  	"M's Acid Arrow",
  	"Stinking Cloud",
  	"Dispel Magic",
  	"Fireball",
  	"Flame Arrow",
  	"Haste",
  	"Hold Person",
  	"Invisibility Ten'",
  	"Lightning Bolt",
  	"Vampiric Touch",
  	"Fear",
  	"Ice Storm",
  	"Stoneskin",
  	"Cloudkill",
  	"Cone of Cold",
  	"Hold Monster"
  };

  public static final String[] potions =
  {
  	"",
  	"Giant Strength",
  	"Healing",
  	"Extra Healing",
  	"Poison",
  	"Vitality",
  	"Speed",
  	"Invisibility",
  	"Cure Poison"
  };

  public static final String[] wands =
  {
    "",
  	"Lightning",
  	"Frost",
  	"Curing",
  	"Fireball",
  	"Silvias",
  	"Magic Missile"
  };

  public ItemType[] itemTypes;
  
  public Items()
    throws IOException
  {
    super();
  
	FileInputStream fis=new FileInputStream((Global.BASE+"itemtype.dat").toLowerCase());
	FileChannel fc = fis.getChannel();
	MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
	mbb.order(Global.byteOrder);
		
	int nrItemTypes = mbb.getShort()&0xffff;
	itemTypes = new ItemType[nrItemTypes];
	for (int i=0; i<nrItemTypes; i++)
		itemTypes[i] = new ItemType(mbb);
  
	fis.close();
	
	fis=new FileInputStream((Global.BASE+"item.dat").toLowerCase());
    fc = fis.getChannel();
	mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
	mbb.order(Global.byteOrder);

    int nrItems = mbb.getShort()&0xffff;
	//Item[] items = new Item[nrItems];
    for (int i=0; i<500; i++)
	{
		if (i<nrItems)
			add(new Item(mbb));
		else
			add(new Item());
	}

    int nrStrings = mbb.getShort()&0xffff;
    names = new String[nrStrings];
    
    byte[] buffer=new byte[35];
    for (int i=0; i<nrStrings; i++)
    {
      mbb.get(buffer);
      int len=0;
      for (;len<35;len++)
        if (buffer[len]==0)
          break;
      names[i] = new String(buffer, 0, len);
    }

    fis.close();    
  }
  
  public int createItem(int baseItemIndex)
  {
	Item baseItem = get(baseItemIndex);
	if (baseItem.position==-1)
		return 0;
		
	for (int i=1; i<500; i++)
	{
		Item item = get(i);
		if (item.position==-1)
		{
			Item newItem = (Item)baseItem.clone();
			newItem.index=i;
			set(i, newItem);
			return i;
		}
	}

	return 0;
  }
  
	public int createCustomItemType(int inventoryBits, int handBits, int armourClassModifier, int classBits, int damageDiceRolls, int damageDiceSides, int damageDiceBase, int usage)
	{
		int itemTypeIndex=51;
		for (;itemTypeIndex<57; itemTypeIndex++)
		{
			if ((itemTypes[itemTypeIndex].armourClassModifier&0xff) == 0xe2)
				break;
		}

		ItemType itemType = itemTypes[itemTypeIndex];
		itemType.inventoryBits = inventoryBits;
		itemType.handBits = handBits;
		itemType.armourClassModifier = armourClassModifier;
		itemType.classBits = classBits;
		itemType.damageVsBig.rolls = damageDiceRolls;
		itemType.damageVsSmall.rolls = damageDiceRolls;
		itemType.damageVsBig.sides = damageDiceSides;
		itemType.damageVsSmall.sides = damageDiceSides;
		itemType.damageVsBig.base = damageDiceBase;
		itemType.damageVsSmall.base = damageDiceBase;
		itemType.usage = usage;

		return itemTypeIndex;
	}
	
	public int createCustomItem(int flags, int picture, int value, int type)
	{
		int itemIndex=11;
		for (;itemIndex<17;itemIndex++)
		{
			if ((get(itemIndex).position&0xffff)==0xfffe)
				break;
		}

		Item item = get(itemIndex);
		item.flags = flags|0x20;
		item.picture = picture;
		item.type = type;
		item.value = value;
		item.subpos = 0;
		item.position = 0;
		item.nameIdentified = 0;
		item.nameUnidentified = 0;
		item.next = 0;
		item.previous = 0;

		return itemIndex;
	}
	
	public void freeCustomItem(int itemIndex)
	{
		Item item = get(itemIndex);
		item.position = -2;
		item.level = -1;
		
		ItemType itemType = itemTypes[item.type];
		itemType.armourClassModifier = 0xe2;
	}
  
  private static final int NAMETYPE_PLAIN = 0;
  private static final int NAMETYPE_PLUS  = 1;
  private static final int NAMETYPE_POTION= 2;
  private static final int NAMETYPE_MAGE  = 3;
  private static final int NAMETYPE_CLERIC= 4;
  private static final int NAMETYPE_RING  = 5;
  private static final int NAMETYPE_WAND  = 6;
  private static int nameTypes[] = {1,1,1,1,1,1,2,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,0,3,4,0,0,0,2,0,1,5,1,0,1,0,5,6,0};
  
  public String getName(Item item, boolean identified)
  {
	if (item.nameUnidentified==0xff)
		return "";
	if (identified && item.nameIdentified==0xff)
		return "";
  
    if (!identified)
      return names[item.nameUnidentified];
      
    if (item.nameIdentified!=1)
      return names[item.nameIdentified];
    else
    {
      switch (nameTypes[item.type])
      {
      case NAMETYPE_PLAIN:
        return names[item.nameUnidentified];
      case NAMETYPE_PLUS:
      {
        if (item.value>0)
          return names[item.nameUnidentified]+" +"+item.value;
        else if (item.value<0)
          return names[item.nameUnidentified]+" "+item.value;
        else
          return names[item.nameUnidentified];
      }
      case NAMETYPE_POTION:
        return "Potion of "+potions[item.value];
      case NAMETYPE_MAGE:
        return "Mage Scroll of "+mageScrolls[item.value];
      case NAMETYPE_CLERIC:
        return "Cleric Scroll of "+clericScrolls[item.value];
      case NAMETYPE_RING:
        return "Ring of "+rings[item.value];
      case NAMETYPE_WAND:
        if (item.value>0)
          return "Wand of "+wands[item.value];
        else
          return "Wand";
      }  
    }
    
    return "???"; 
  }
}
