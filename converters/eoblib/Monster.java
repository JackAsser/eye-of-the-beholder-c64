import java.io.*;

public class Monster
{
	public static final int FLAGS_ACTIVE = 1;
	public static final int FLAGS_HIT = 2;
	public static final int FLAGS_FLIP = 4;
	public static final int FLAGS_TURNED = 8;
	public static final int FLAGS_FLEE = 16;
	public static final int FLAGS_STONE = 32;
	public static final int FLAGS_INACT = 64;

	public static final int STATE_PREPARE_ATTACK = -2;
	public static final int STATE_ATTACK = -1;
	public static final int STATE_FORWARD = 0;
	public static final int STATE_BACKWARD = 1;
	public static final int STATE_LEFT = 2;
	public static final int STATE_RIGHT = 3;
	public static final int STATE_ADJTURN = 4;
	public static final int STATE_TURNLEFT = 5;
	public static final int STATE_TURNRIGHT = 6;
	public static final int STATE_READYHIT = 7;
	public static final int STATE_INACTIVE = 8;
	public static final int STATE_LEGWALK = 9;
	public static final int STATE_ISHIT = 10;
	
	MonsterType type;
	int typeIndex;
	int levelType;
	int position;
	int subpos;
	int direction;
	int legState;
	int picture;
	int phase;
	int turnDirection;
	int state;
	int pause;
	int hpMax;
	int hp;
	int goalPosition;
	int weapon;
	int pocketItem;
	int flags;
	int scatterOffset;
	int ticksUntilMove;
	int thcount;
	int field_1A;
	int field_1B;

	public void clear()
	{
		type = MonsterType.monsterTypes[0];
		levelType=0;
		position=0;
		subpos=0;
		direction=0;
		legState=0;
		picture=0;
		phase=0;
		turnDirection=0;
		state=0;
		pause=0;
		hpMax=0;
		hp=0;
		goalPosition=0;
		weapon=0;
		pocketItem=0;
		flags=0;
		scatterOffset=0;
		ticksUntilMove=0;
		thcount=0;
		field_1A=0;
		field_1B=0;
	}

	public Monster(Maze maze, MonsterCreate monsterCreate)
	{
		if (monsterCreate.position == 0)
			return;
			
		type = MonsterType.monsterTypes[monsterCreate.type];
		typeIndex = monsterCreate.type;
//		System.out.printf("levelType:%d monsterType:%d  Snd:%d/%d Phase:%d\n", monsterCreate.levelType/2, monsterCreate.type, type.walkSound, type.attackSound, monsterCreate.phase);
		
		thcount = type.thcount;
		levelType = monsterCreate.levelType*2 + (monsterCreate.index&1);
		subpos = monsterCreate.subpos;
		picture = monsterCreate.picture;
		phase = monsterCreate.phase;
		pause = monsterCreate.pause;
		direction = monsterCreate.direction;
		
		Dice hpDice;
		if (type.hpDices!=-1)
			hpDice = new Dice(type.hpDices, 8, 0);
		else
			hpDice = new Dice(1,4,0);
		
		hpMax = hpDice.roll();
		hp = hpMax;
		weapon = monsterCreate.weapon;
		pocketItem = monsterCreate.pocketItem;
		updatePositionAndDirection(maze, monsterCreate.position, monsterCreate.direction);
	}
	
	public void updatePositionAndDirection(Maze maze, int newPosition, int newDirection)
	{
		if (newPosition!=-1)
		{
			//GameState.isPositionVisible(position);
		
			if ((maze.mazeBlocks[position].triggerBitsAndMonsterCount&7) != 0)
			{
				maze.mazeBlocks[position].triggerBitsAndMonsterCount--;
			}
			position = newPosition;
			maze.mazeBlocks[position].triggerBitsAndMonsterCount++;
		}

		if (newDirection!=-1)
		{
			direction = newDirection;
		}

		//GameState.isPositionVisible(position);
	}
	
	public boolean occupiesPosition(int position, int subpos)
	{
		if (this.position != position)
			return false;
			
		if (this.subpos == subpos)
			return true;
			
		if (this.subpos == 4)
			return true;

		return false;
	}
}

class MonsterType
{
	public static final int TYPEBITS_QUARTERSIZE 	  = 0x0000;
	public static final int TYPEBITS_BIGSIZE 		  = 0x0001;
	public static final int TYPEBITS_SMALLSIZE 		  = 0x0002;
	public static final int TYPEBITS_REDUCEDAMAGE     = 0x0004;
	public static final int TYPEBITS_TWOATTACKS       = 0x0008;
	public static final int TYPEBITS_POISONUS         = 0x0010;
	public static final int TYPEBITS_MINDBLAST        = 0x0020;
	public static final int TYPEBITS_RANGEATTACK      = 0x0040;
	public static final int TYPEBITS_STEAL            = 0x0080;
	public static final int TYPEBITS_INVISIBLE        = 0x0100;
	public static final int TYPEBITS_BADSIGHT         = 0x0200;
	public static final int TYPEBITS_UNKNOWN2         = 0x0400;
	public static final int TYPEBITS_EYESFRONT        = 0x0800;
	public static final int TYPEBITS_CANOPENDOOR      = 0x1000;
	public static final int TYPEBITS_NOSPELLKILL      = 0x2000;
	public static final int TYPEBITS_MULTIATTACK      = 0x4000;
	public static final int TYPEBITS_TURNTOSTONE      = 0x8000;

	public static final int ATTACKBITS_ISPERSON 	  = 0x0001;
	public static final int ATTACKBITS_ISMONSTER 	  = 0x0002;
	
	public static MonsterType[] monsterTypes;
	public int ac;
	public int thac0;
	public int hpDices;
	public int attackCount;
	public Dice attackDices[];
	public int typeBits;
	public int attackBits;
	public int size;
	public int experience;
	public int specialYoffset;
	public int attackSound;
	public int walkSound;
	public int thcount;
	public int tuResist;
	public int spellImmunity;
 
	public MonsterType(int ac, int thac0, int hpDices, int attackCount, Dice attackDice0, Dice attackDice1, Dice attackDice2, int typeBits, int attackBits, int size, int experience, int specialYoffset, int attackSound, int walkSound, int thcount, int tuResist, int spellImmunity)
	{
		this.ac = ac;
		this.thac0 = thac0;
		this.hpDices = hpDices;
		this.attackCount = attackCount;
		this.attackDices = new Dice[3];
		this.attackDices[0] = attackDice0;
		this.attackDices[1] = attackDice1;
		this.attackDices[2] = attackDice2;
		this.typeBits = typeBits;
		this.attackBits = attackBits;
		this.size = size;
		this.experience = experience;
		this.specialYoffset = specialYoffset;
		this.attackSound = attackSound;
		this.walkSound = walkSound;
		this.thcount = thcount;
		this.tuResist = tuResist;
		this.spellImmunity = spellImmunity;
	}
	
	static
	{
		monsterTypes = new MonsterType[]
		{
			new MonsterType( 7, 18, -1,  1, new Dice(1,   4, 0),new Dice(),     new Dice(),      0x0000, 3, 0,   15,  0,  2,   75,  2, -1, 0x00),
			new MonsterType( 7, 17,  2,  1, new Dice(1,   4, 0),new Dice(),     new Dice(),      0x0002, 2, 0,  120,  1,  56,  57,  0, -1, 0x00),
			new MonsterType( 5, 17,  1,  1, new Dice(1,   6, 0),new Dice(),     new Dice(),      0x010C, 4, 0,   65,  0,  66,  67,  0,  2, 0x00),
			new MonsterType( 8, 15,  2,  1, new Dice(1,   8, 0),new Dice(),     new Dice(),      0x0000, 4, 0,   65,  0,  73,  74,  0,  3, 0x00),
			new MonsterType( 4, 16,  2,  2, new Dice(1,   4, 1),new Dice(1,4,1),new Dice(),      0x0042, 0, 0,  175,  2,  54,  55,  4, -1, 0x14),
			new MonsterType( 5, 17,  3,  1, new Dice(1,   8, 0),new Dice(),     new Dice(),      0x0008, 3, 0,   65,  0,  47,  48,  0, -1, 0x00),
			new MonsterType( 6, 19,  2,  1, new Dice(1,   8, 0),new Dice(),     new Dice(),      0x0012, 2, 0,  650,  2,  49,  38,  0, -1, 0x00),
			new MonsterType( 3, 12,  7,  1, new Dice(1,   8, 0),new Dice(),     new Dice(),      0x0000, 3, 0,  730,  0,  45,  46,  0, -1, 0x00),
			new MonsterType( 5,  7, 10,  1, new Dice(1,   8, 6),new Dice(),     new Dice(),      0x000C, 4, 0, 1500,  0,  64,  65,  0,  7, 0x00),
			new MonsterType( 4, 15,  6,  2, new Dice(1,   8, 0),new Dice(1,8,0),new Dice(),      0x0002, 3, 0,  975,  1,  39,  40,  0, -1, 0x00),
			new MonsterType( 5, 13,  6,  3, new Dice(1,   8, 0),new Dice(1,4,0),new Dice(1,4,0), 0x0028, 3, 0, 1400,  2,  37,  36,  0, -1, 0x00),
			new MonsterType( 4, 13,  5,  2, new Dice(1,   4, 0),new Dice(1,4,0),new Dice(),      0x0000, 1, 0, 1250,  0,  62,  63,  0, -1, 0x00),
			new MonsterType( 4, 15,  4,  1, new Dice(1,   8, 4),new Dice(),     new Dice(),      0x0020, 1, 0,  650,  0,  43,  44,  0, -1, 0x3C),
			new MonsterType(-1,  7, 10,  1, new Dice(3,   8, 0),new Dice(),     new Dice(),      0x0002, 2, 0,10000,  2,  68,  69,  0, -1, 0x64),
			new MonsterType(-1,  7,  9,  1, new Dice(3,   4, 0),new Dice(),     new Dice(),      0x0042, 3, 0, 2000,  0,  31, -1,   8, -1, 0x00),
			new MonsterType( 3, 13,  7,  1, new Dice(1,   6, 0),new Dice(),     new Dice(),      0x0040, 2, 0, 5000,  2,  41,  42,  2, -1, 0x3C),
			new MonsterType( 5, 13,  7,  1, new Dice(1,   6, 0),new Dice(),     new Dice(),      0x0040, 2, 0, 5000,  0,  52,  53,  2, -1, 0x00),
			new MonsterType( 5, 11,  8,  1, new Dice(5,  20, 0),new Dice(),     new Dice(),      0x0042, 0, 0, 8000,  0,  58,  59, -1, -1, 0x5A),
			new MonsterType( 2, 15,  5,  1, new Dice(1,   1, 0),new Dice(),     new Dice(),      0x0082, 2, 0,  270,  2,  60,  61,  0, -1, 0x00),
			new MonsterType(-2, 13,  8,  1, new Dice(6,   4, 0),new Dice(),     new Dice(),      0x0002, 2, 0, 4000,  2,  72,  71,  0, -1, 0x32),
			new MonsterType( 4, 13,  7,  1, new Dice(1,  10, 0),new Dice(),     new Dice(),      0x0002, 2, 0,  975,  1,  51,  50,  0, -1, 0x28),
			new MonsterType( 0,  5, 35,  1, new Dice(1, 100, 0),new Dice(),     new Dice(),      0x0042, 0, 0,14000,  2, -1,  -1,  -1, -1, 0x64)
		};


/*
   LVL  NAME       TYPE SOUND
	1:	kobold		0	75/2
		leech		1	57/56
	2:	zombie		3	74/73
		skeleton	2	67/66
	3:	kuotoa		4	55/54
		flind		5	48/47
	4:	spider		6	38/49
	5:	dwarf		7	46/45
		spider		6	38/49
	6:	kenku		16	53/52
		mage		14  31/-1
	7:	drowelf		12	44/43
		skelwar		8	65/64
	8:	drider		15	42/41
		hellhnd		20	50/51
	9:	rust		18	61/60
		disbeast	9	40/39
	10:	shindia		11	62/63
		mantis		10	36/37
	11:	xorn		19	71/72
		mflayer		17	59/58
	12:	xanath		21	-1/-1
		golem		13	69/58
*/
	
	}
}

class MonsterCreate
{
	public int index;
	public int levelType;
	public short position;
	public int subpos;
	public int direction;
	public int type;
	public int picture;
	public int phase;
	public int pause;
	public int weapon;
	public int pocketItem;

	public MonsterCreate()
	{
		;
	}
	
	public MonsterCreate(RandomAccessFile is)
		throws IOException
	{
		index = is.read();
		levelType = is.read();
		position = (short)((is.read() | (is.read()<<8)) & 0xffff);
		subpos = is.read();
		direction = is.read();
		type = is.read();
		picture = is.read();
		phase = is.read();
		pause = is.read();
		weapon = is.read() | (is.read()<<8);
		pocketItem = is.read() | (is.read()<<8);
	}
}
