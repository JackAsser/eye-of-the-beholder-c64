import java.io.*;
import java.nio.*;
import java.nio.channels.*;

public class VCN
{
	public static int MODE_OCS = 0;
	public static int MODE_VGA = 1;
	public int mode;
	
	private byte[] rawData;
	private int numberOfTiles;
	private byte[][] ocsPalette;
	private byte[] fgPalette;
	private byte[] bgPalette;

	public VCN(String filename)
		throws IOException
	{
//		System.out.println("VCN: "+filename);
		FileInputStream fis=new FileInputStream(filename.toLowerCase());
		FileChannel fc = fis.getChannel();
		MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
		mbb.order(ByteOrder.LITTLE_ENDIAN);
		
		numberOfTiles = mbb.getShort()&0xffff;

		rawData=new byte[(int)(fc.size()-2)];
		mbb.get(rawData);

		if (fc.size() == 2+16+16+numberOfTiles*4*8)
			mode = MODE_VGA;
		else
			mode = MODE_OCS;
		
		if (mode == MODE_OCS)
		{
			ocsPalette=new byte[16][3];
			for (int i=0;i<16;i++)
			{
				int b1=rawData[i*2+0];
				int b2=rawData[i*2+1];
				int r=b1&0x0f;
				int g=(b2&0xf0)>>4;
				int b=b2&0x0f;
				r*=17;
				g*=17;
				b*=17;
				ocsPalette[i][0]=(byte)r;
				ocsPalette[i][1]=(byte)g;
				ocsPalette[i][2]=(byte)b;
			}
		}
		else
		{
			bgPalette = new byte[16];
			System.arraycopy(rawData, 0, bgPalette, 0, 16);
			fgPalette = new byte[16];
			System.arraycopy(rawData, 16, fgPalette, 0, 16);
		}
		
		fis.close();
	}

	public int getNumberOfTiles()
	{
		return numberOfTiles;
	}

	public byte[] getTile(int index, boolean foregroundTile)
	{
		byte tile[] = new byte[8*8];
		
		if (mode == MODE_OCS)
		{
			int offset=32+index*40;
			for (int y=0;y<8;y++)
			{
				for (int x=0;x<8;x++)
				{
					int bit=1<<(7-x);
					byte data=0;
					for (int plane=0;plane<5;plane++)
						data|=(rawData[offset+plane+y*5]&bit)==0?0:(1<<plane);
						tile[x+y*8]=data;
				}
			}			
		}
		else
		{
			byte[] palette = foregroundTile?fgPalette:bgPalette;
			int offset=32+index*32;
			for (int y=0;y<8;y++)
			{
				for (int x=0;x<4;x++)
				{
					byte c = rawData[offset+x+y*4];
					byte c0 = (byte)((c&0xf0)>>4);
					byte c1 = (byte)(c&0x0f);
					tile[x*2+y*8+0] = palette[c0];
					tile[x*2+y*8+1] = palette[c1];
				}
			}
		}
		return tile;
	}

	public byte[][] getPalette()
	{
		return ocsPalette;
	}
}
