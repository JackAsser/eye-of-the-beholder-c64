public class GfxObject
{
	int width;
	int height;
	byte[] data;
	
	public GfxObject(int width, int height, byte[] data)
	{
		this.width = width;
		this.height = height;
		this.data = data;
	}
	
	public GfxObject scaleDown()
	{
		int scaledWidth = width*2/3;
		int scaledHeight = height*2/3;
		byte[] scaledData = new byte[scaledWidth*scaledHeight];

		int sy=0,sx=0;
		for (int y=0; y<height && sy<scaledHeight;)
		{
			sx=0;
			for (int x=0; x<width && sx<scaledWidth;)
			{
				scaledData[sx+sy*scaledWidth] = data[x+y*width];
				sx++;
				x++;
				if ((x>=width) || (sx>=scaledWidth))
					break;

				scaledData[sx+sy*scaledWidth] = data[x+y*width];
				sx++;
				x+=2;
			}
			sy++;
			y++;
			if ((y>=height) || (sy>=scaledHeight))
				break;
			
			sx=0;
			for (int x=0; x<width && sx<scaledWidth;)
			{
				scaledData[sx+sy*scaledWidth] = data[x+y*width];
				sx++;
				x++;
				if ((x>=width) || (sx>=scaledWidth))
					break;
				
				scaledData[sx+sy*scaledWidth] = data[x+y*width];
				sx++;
				x+=2;
			}
			sy++;
			y+=2;
		}
		
		return new GfxObject(scaledWidth, scaledHeight, scaledData);
	}
}

class GfxOverlay
{
	public GfxObject gfxObject;
	int deltaX;
	int deltaY;
	
	public GfxOverlay()
	{
	}
}
