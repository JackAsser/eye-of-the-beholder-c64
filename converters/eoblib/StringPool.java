import java.util.*;

public class StringPool
{
  private int nextOffset;
  private ArrayList<String> strings;
  private ArrayList<Integer> offsets;
  
  public StringPool()
  {
    strings = new ArrayList<String>();
    offsets = new ArrayList<Integer>();
    nextOffset = 0;
  }
  
  public int add(String s)
  {
    int i = strings.indexOf(s);
    if (i==-1)
    {
      int returnOffset = nextOffset;
      strings.add(s);
      offsets.add(nextOffset);
      nextOffset += s.length()+1;	// +1 to allow for terminating null bytes
      return returnOffset;
    }
    return offsets.get(i);
  }
  
  public byte[] compress()
  {
    String xlate="\0ABCDEFGHIJKLMNOPQRSTUVWXYZ,.'! ";    

    int input[]=new int[8];

    StringBuilder sb=new StringBuilder();
    for (String string : strings)
	{
	  sb.append(string.toUpperCase());
	  sb.append('\0');
	}
	String s = sb.toString();
		
	int rlen=(s.length()+1+7)&0xfffffff8;
	int slen=rlen/8;
	int soff=0;
	slen*=5;
	
	byte output[] = new byte[slen];
	
	for (int off=0; off<rlen; off+=8, soff+=5)
	{
	  for (int i=0; i<8; i++)
	  {
	    int pos=0;
	    if (off+i<s.length())
	    {
	      for (;pos<32;pos++)
            if (s.charAt(off+i) == xlate.charAt(pos))
              break;
    	}
		input[i]=pos;
      }

      for (int outbit=0; outbit<5; outbit++)
      {
        for (int inbit=0; inbit<8; inbit++)
        {
          int i = (input[7-inbit]&(1<<(4-outbit))) == 0 ? 0:1;
          output[soff+outbit] |= (byte)(i << inbit);
		}
      }
    }
     
    return output;
  }  
}
