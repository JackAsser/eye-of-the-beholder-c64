import java.io.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;

public class ViewMaze extends JFrame
{
  private Inf inf;

  public ViewMaze(int level)
  {    
    super("ViewMaze");

    try
    {
      inf=new Inf(level);
    }
    catch (IOException e)
    {
      e.printStackTrace();
      System.exit(1);
    }

    setLayout(new BorderLayout());

    Canvas c = new Canvas()
    {
      public void paint(Graphics g)
      {
        for (int y=0; y<32; y++)
        {
          for (int x=0; x<32; x++)
          {
            g.setColor(Color.white);
            g.fillRect(x*9,y*9,9,9);
  
            g.setColor(Color.black);

			if (inf.wallMappings.get(inf.maze.mazeBlocks[x+y*32].walls[Maze.N]).isPlainWall())
              g.drawLine(x*9,y*9,x*9+8,y*9);
			if (inf.wallMappings.get(inf.maze.mazeBlocks[x+y*32].walls[Maze.S]).isPlainWall())
              g.drawLine(x*9,y*9+8,x*9+8,y*9+8);
            if (inf.wallMappings.get(inf.maze.mazeBlocks[x+y*32].walls[Maze.W]).isPlainWall())
              g.drawLine(x*9,y*9,x*9,y*9+8);
            if (inf.wallMappings.get(inf.maze.mazeBlocks[x+y*32].walls[Maze.E]).isPlainWall())
              g.drawLine(x*9+8,y*9,x*9+8,y*9+8);              
          }
        }
      }
      
      public void update(Graphics g)
      {
        paint(g);
      }
      
      public Dimension getPreferredSize()
      {
        return new Dimension(32*9,32*9);
      }
    };
    
    add(c, BorderLayout.CENTER);
    pack();
    setResizable(false);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setVisible(true);
    toFront();
  }

  public static void main(String[] args)
  {
    Global.BASE = "../data/";

    if (args.length==0)
    {
      System.err.println("Usage: java ViewMaze <level>");
      System.exit(1);
    }

    new ViewMaze(Integer.parseInt(args[0]));

  }
}
