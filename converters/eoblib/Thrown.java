interface SpellHandler
{
	public void spellCast();
	public boolean spellHit(Thrown thrown);
	public void spellExpired(PartyMember member);
}

public class Thrown
{
	public static final int CENTERED=0x40;

	public int active; // 0=inactive, 1=item, 2=spell
	public int unknown;
	public int movedOnlyWithinSubpos;
	public int position;
	public int subpos;
	public int direction;
	public int itemOrSpellIndex;
	public int spellGfxIndexOrItemTypeIndex;
	public int caster;
	public int flags;
	public int range;
	public SpellHandler spellHandler = null;
}
