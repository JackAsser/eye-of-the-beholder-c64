import java.io.*;
import java.nio.*;
import java.nio.channels.*;

class ItemType
{
	public static final int INVENTORY_USAGE_QUIVER	= 0x01;
	public static final int INVENTORY_USAGE_ARMOUR	= 0x02;
	public static final int INVENTORY_USAGE_BRACERS = 0x04;
	public static final int INVENTORY_USAGE_BACKPACK= 0x08;
	public static final int INVENTORY_USAGE_BOOTS	= 0x10;
	public static final int INVENTORY_USAGE_HELMET	= 0x20;
	public static final int INVENTORY_USAGE_NECKLACE= 0x40;
	public static final int INVENTORY_USAGE_BELT	= 0x80;
	public static final int INVENTORY_USAGE_RING	= 0x100;

	public static final int CLASS_USAGE_FIGHTER		= 0x01;
	public static final int CLASS_USAGE_MAGE        = 0x02;
	public static final int CLASS_USAGE_CLERIC      = 0x04;
	public static final int CLASS_USAGE_PALADIN     = 0x05;
	public static final int CLASS_USAGE_THIEF       = 0x08;
	
	int inventoryBits;
	int handBits;		
	int armourClassModifier;
	int classBits;
	int doubleHanded;
	Dice damageVsSmall;
	Dice damageVsBig;
	int unknown1;
	int usage;
	
	public ItemType(ByteBuffer bb)
		throws IOException
	{
		inventoryBits = bb.getShort()&0xffff;
		handBits = bb.getShort()&0xffff;
		armourClassModifier = bb.get();
		classBits = bb.get();
		doubleHanded = bb.get();
		damageVsSmall = new Dice(bb);
		damageVsBig = new Dice(bb);
		unknown1 = bb.get();
		usage = bb.getShort()&0xffff;
	}
}

class Item implements Cloneable
{
  public static final int SUBPOS_FLOOR_NW    = 0;
  public static final int SUBPOS_FLOOR_NE    = 1;
  public static final int SUBPOS_FLOOR_SW    = 2;
  public static final int SUBPOS_FLOOR_SE    = 3;
  public static final int SUBPOS_COMPARTMENT = 8;

  public static final int FLAGS_MAGICAL    = 0x80;
  public static final int FLAGS_IDENTIFIED = 0x40;
  public static final int FLAGS_CURSED     = 0x20;
  public static final int FLAGS_DESTROYWHENUSED = 0x10;
  public static final int FLAGS_REGAINHP   = 0x08;
  public static final int FLAGS_CHARGES	   = 0x00; // 0x00..0x1f

  // <14 == Large item
  // >=14 == Small item
  public static final int pictureInMaze[] = {
     0, 0, 0, 6, 5,10, 1, 6,11, 2, 7, 7,15, 2,14,15,31,24,23,19,
    16, 9, 3, 3,24,27, 4,36, 4, 4, 4,13, 8,26,26,33,18,20,34,34,
    28,28,28, 0,12, 0,17,29,24,24,24,24,24,24,25,36,22,19,21,17,
    26,35,15,20,19,36,26, 0,35,19,19, 9,27, 3, 3,16,16,14,11, 4,
    19,19};

  public static final String[] subposText =
  {
    "NW","NE","SW","SE","?","?","?","?","C "
  };

  int nameUnidentified;
  int nameIdentified;
  int flags;
  int picture;
  int type;
  int subpos;
  int position;
  int level;
  int value;
  int index;
  int previous;
  int next;
  
  public static int maxIndex=0;
  
  public Item(ByteBuffer bb)
    throws IOException
  {
    nameUnidentified = bb.get();
    nameIdentified = bb.get();
    flags = bb.get();
    picture = bb.get();
    type = bb.get();
    subpos = bb.get();
	position = bb.getShort();
    previous = bb.getShort()&0xffff;
	next = bb.getShort()&0xffff;
    level = bb.get();
    value = bb.get();
    index = maxIndex;
    maxIndex++;
  }
  
  public Item()
  {
	nameUnidentified=0xff;
	nameIdentified=0xff;
	flags=0;
	picture=0;
	type=0;
	subpos=0;
	position=-1;
	level=0;
	value=0;
	previous=0;
	next=0;
	index=maxIndex;
	maxIndex++;
  }
  
  public Object clone()
  {
	Item item = new Item();
	item.nameUnidentified=nameUnidentified;
	item.nameIdentified=nameIdentified;
	item.flags=flags;
	item.picture=picture;
	item.type=type;
	item.subpos=subpos;
	item.position=position;
	item.level=level;
	item.value=value;
	item.previous=previous;
	item.next=next;
	item.index=-1;
	return item;
  }
}
