import java.io.*;
import java.util.*;
import java.awt.image.*;

public class Inf
{
	public VCN vcn;
	public VMP vmp;
	public String mazName;
	public String VCNVMPName;
	public String paletteName;
	public int timerMode;
	public int timerTicks;
	public int stepsUntilScriptCall;
	public HashMap<String, Dat> decorationMaps;
	public HashMap<String, CPSImage> decorationGraphics;
	public HashMap<Integer, WallMapping> wallMappings;
	public Maze maze;
	public int scriptOffset;
	public byte[] rawScript;
	public int triggersOffset;
	public byte[] rawTriggers;
	public int monsterTimeouts[] = {20,20,20,20};
	public Monster monsters[];
	public MonsterCreate monsterCreates[];
	public Script script;
	public GfxObject monsterGfx[];
	public String monsterGfxNames[];
	public GfxObject[] doorGfx;
	public GfxOverlay[] doorKnobs;
	public byte palette_r[];
	public byte palette_g[];
	public byte palette_b[];
	
	private String getString(byte[] arr)
	{
		String s=new String(arr);
		int i=0;
		while (arr[i++]!=0);
		return s.substring(0,i-1);
	}
	
	public void loadMonsterGraphics(String name, int monsterPlacementMethodIndex, int gfxOffset)
		throws IOException
	{
		int monsterRect[][][][] =
		{
			{
				{
					 {0, 0, 7, 96},
	                 {7, 40, 12, 56},
	                 {19, 40, 12, 56},
	                 {31, 0, 7, 96},
	                 {0, 96, 7, 96},
	                 {7, 96, 7, 96}
				},
				{
					 {14, 96, 5, 56},
	                 {7, 0, 7, 40},
	                 {14, 0, 7, 40},
	                 {19, 96, 5, 56},
	                 {24, 96, 5, 56},
	                 {29, 96, 5, 56}
				},
				{
					 {14, 152, 3, 32},
	                 {17, 160, 4, 24},
	                 {21, 160, 4, 24},
	                 {25, 152, 3, 32},
	                 {28, 152, 3, 32},
	                 {31, 152, 3, 32}
				}
			},
			{
				{
					{0, 0, 7, 96},
					{7, 0, 7, 96},
					{14, 0, 7, 96},
					{21, 0, 7, 96},
					{28, 0, 7, 96},
					{0, 96, 7, 96}
				},
				{
					{7, 96, 5, 56},
					{12, 96, 5, 56},
					{17, 96, 5, 56},
					{22, 96, 5, 56},
					{27, 96, 5, 56},
					{32, 96, 5, 56}
				},
				{
					{7, 152, 3, 32},
					{10, 152, 3, 32},
					{13, 152, 3, 32},
					{16, 152, 3, 32},
					{19, 152, 3, 32},
					{22, 152, 3, 32}
				}
			},
			{
				{
					{0, 0, 10, 88},
					{10, 0, 10, 88},
					{20, 0, 10, 88},
					{30, 0, 10, 88},
					{0, 88, 10, 88},
					{10, 88, 10, 88}
				}
			}
		};
	
		CPSImage cps;
		if (monsterPlacementMethodIndex!=2)
			cps = new CPSImage((Global.BASE+name+".cp_").toLowerCase());
		else
			cps = new CPSImage((Global.BASE+name+"1.cp_").toLowerCase());
			
		for (int i=0; i<6; i++)
		{
			monsterGfx[gfxOffset+i] = cps.getGfxObject(
				monsterRect[monsterPlacementMethodIndex][0][i][0]*8,
				monsterRect[monsterPlacementMethodIndex][0][i][1],
				monsterRect[monsterPlacementMethodIndex][0][i][2]*8,
				monsterRect[monsterPlacementMethodIndex][0][i][3]);
		}
		//System.out.printf("Monster graphics %s loaded using method %d...\n", name, monsterPlacementMethodIndex);
	}

	public void loadDoorGraphics(int doorType0, int doorGfxPointerIndex0, int doorType1, int doorGfxPointerIndex1)
		throws IOException
	{
		doorGfx = new GfxObject[6];
		doorKnobs = new GfxOverlay[6];
		
		int doorRects[][][] = 
		{
			{
				{0, 0, 10, 72},
				{17, 152, 7, 47},
				{25, 72, 4, 29},
			},
			{
				{10, 0, 10, 72},
				{29, 72, 7, 47},
				{25, 104, 4, 29},
			},
			{
				{20, 0, 6, 72},
				{36, 72, 4, 47},
				{30, 120, 3, 29},
			},
			{
				{26, 0, 12, 72},
				{0, 144, 7, 41},
				{33, 120, 4, 27},
			},
			{
				{0, 188, 10, 12},
				{10, 188, 6, 8},
				{10, 196, 4, 2},
			},
			{
				{7, 74, 9, 55},
				{0, 74, 7, 37},
				{7, 160, 5, 23},
			},
			{
				{7, 130, 9, 28},
				{0, 112, 7, 19},
				{12, 160, 5, 11},
			},
			{
				{16, 72, 9, 75},
				{24, 152, 7, 46},
				{31, 152, 4, 29}
			}
		};

		int doorKnobRects[][][] =
		{
			{
				{37, 120, 1, 7},
				{37, 136, 1, 3},
				{37, 144, 1, 1}
			},
			{
				{37, 120, 1, 7},
				{37, 136, 1, 3},
				{37, 144, 1, 1}
			},
			{
				{38, 120, 1, 11},
				{38, 136, 1, 6},
				{38, 144, 1, 4}
			},
			{
				{39, 120, 1, 14},
				{39, 136, 1, 8},
				{37, 144, 1, 1}
			},
			{
				{37, 120, 1, 7},
				{37, 136, 1, 3},
				{37, 144, 1, 1}
			},
			{
				{37, 152, 1, 11},
				{37, 168, 1, 6},
				{37, 176, 1, 4}
			},
			{
				{37, 152, 1, 11},
				{37, 168, 1, 5},
				{37, 176, 1, 4}
			},
			{
				{38, 152, 1, 10},
				{38, 168, 1, 4},
				{37, 144, 1, 1}
			}
		};

		int doorKnobOffsets[][][] =
		{
			{
				{130, 36},
				{116, 39},
				{0, 0}
			},
			{
				{130, 36},
				{116, 39},
				{0, 0}
			},
			{
				{91, 54},
				{90, 51},
				{90, 45}
			},
			{
				{136, 38},
				{116, 41},
				{0, 0}
			},
			{
				{0, 0},
				{0, 0},
				{0, 0}
			},
			{
				{126, 36},
				{115, 38},
				{0, 0}
			},
			{
				{0, 0},
				{0, 0},
				{0, 0}
			},
			{
				{128, 43},
				{116, 43},
				{0, 0}
			}
		};

		//System.out.printf("# type slot\n");
		//System.out.printf("0 %02x   %02x\n", doorType0, doorGfxPointerIndex0);
		//System.out.printf("1 %02x   %02x\n", doorType1, doorGfxPointerIndex1);
		
		// TODO: Work around to handle doorType1 on level 3, which really shouldn't be there at all...
		if (doorType0==0xff)
			doorType0=0;
		if (doorType1==0xff)
			doorType1=1;

		CPSImage cps = new CPSImage((Global.BASE+"door.cp_").toLowerCase());
		if (doorType0!=0xff)
		{
			for (int i=0; i<3; i++)
			{
				int[] rect = doorRects[doorType0][i];			
				doorGfx[i+doorGfxPointerIndex0] = cps.getGfxObject(rect[0]*8, rect[1], rect[2]*8, rect[3]);
				GfxOverlay ovl = new GfxOverlay();
				rect = doorKnobRects[doorType0][i];
				ovl.gfxObject = cps.getGfxObject(rect[0]*8, rect[1], rect[2]*8, rect[3]);
				ovl.deltaX = doorKnobOffsets[doorType0][i][0];
				ovl.deltaY = doorKnobOffsets[doorType0][i][1];
				doorKnobs[i+doorGfxPointerIndex0] = ovl;
			}
		}
		
		if (doorType1!=0xff)
		{
			for (int i=0; i<3; i++)
			{
				int[] rect = doorRects[doorType1][i];
				doorGfx[i+doorGfxPointerIndex1] = cps.getGfxObject(rect[0]*8, rect[1], rect[2]*8, rect[3]);
				GfxOverlay ovl = new GfxOverlay();
				rect = doorKnobRects[doorType1][i];
				ovl.gfxObject = cps.getGfxObject(rect[0]*8, rect[1], rect[2]*8, rect[3]);
				ovl.deltaX = doorKnobOffsets[doorType1][i][0];
				ovl.deltaY = doorKnobOffsets[doorType1][i][1];
				doorKnobs[i+doorGfxPointerIndex1] = ovl;
			}
		}
	}

	public Inf(int level)
		throws IOException
	{
		byte tmp[]=new byte[1024];
		RandomAccessFile fis=new RandomAccessFile((Global.BASE+"level"+level+".inf.bin").toLowerCase(), "r");
		
		triggersOffset=fis.read()+(fis.read()<<8);

		fis.read(tmp,0,12);		
		mazName=getString(tmp);
		
		maze=new Maze((Global.BASE+mazName).toLowerCase());

		fis.read(tmp,0,12);
		VCNVMPName=getString(tmp);
		
		// Depack the vcn file
		if (new File((Global.BASE+VCNVMPName+".vc_").toLowerCase()).exists())
			vcn=new VCN((Global.BASE+VCNVMPName+".vc_").toLowerCase());
		else
			vcn=new VCN((Global.BASE+VCNVMPName+".oc_").toLowerCase());

		// Depack the vmp file
		vmp=new VMP((Global.BASE+VCNVMPName+".vmp").toLowerCase());

		fis.read(tmp,0,12);
		paletteName=getString(tmp);
		File paletteFile = new File((Global.BASE+paletteName+".PAL").toLowerCase());
		palette_r=new byte[256];
		palette_g=new byte[256];
		palette_b=new byte[256];
		if (paletteFile.exists())
		{
			byte tmp2[] = new byte[768];
			FileInputStream fis2=new FileInputStream(paletteFile);
			fis2.read(tmp2);
			fis2.close();
			for (int i=0; i<256; i++)
			{
				palette_r[i] = (byte)((((int)tmp2[i*3+0])&0x3f)<<2);
				palette_g[i] = (byte)((((int)tmp2[i*3+1])&0x3f)<<2);
				palette_b[i] = (byte)((((int)tmp2[i*3+2])&0x3f)<<2);
			}
		}
		else
		{
			CPSImage invent = new CPSImage((Global.BASE+"invent.cp_").toLowerCase());
			invent.replacePalette(vcn);
			System.arraycopy(invent.r, 0, palette_r, 0, 256);
			System.arraycopy(invent.g, 0, palette_g, 0, 256);
			System.arraycopy(invent.b, 0, palette_b, 0, 256);
		}

		loadDoorGraphics(fis.read(), fis.read(), fis.read(), fis.read());
		
		timerMode = fis.read();
		timerTicks = fis.read()+(fis.read()<<8);
		stepsUntilScriptCall = fis.read()+(fis.read()<<8);

		monsterGfx = new GfxObject[36];
		monsterGfxNames = new String[2];
		
		int methodIndex = fis.read();
		fis.read(tmp,0,12);
		if (methodIndex != 0xff) {
			monsterGfxNames[0] = getString(tmp); 
			loadMonsterGraphics(monsterGfxNames[0], methodIndex, 0);
		}
		methodIndex = fis.read();
		fis.read(tmp,0,12);
		if (methodIndex != 0xff) {
			monsterGfxNames[1] = getString(tmp); 
			loadMonsterGraphics(monsterGfxNames[1], methodIndex, 18);
		}

		while(true)
		{
			int monsterTimerIndex = fis.readByte();
			if (monsterTimerIndex == -1)
				break;
	
			int timeout=fis.readByte();
			monsterTimeouts[monsterTimerIndex*2+0] = timeout;
			monsterTimeouts[monsterTimerIndex*2+1] = timeout;
		}

		monsters = new Monster[30];
		monsterCreates = new MonsterCreate[30];
		for (int i=0; i<30; i++)
		{
			monsterCreates[i] = new MonsterCreate(fis);			
			monsters[i] = new Monster(maze, monsterCreates[i]);
		}
		
		int nbrECFBCommands=fis.read()+(fis.read()<<8);
		
		Dat currentDecorationMap=null;
		CPSImage currentDecorationGraphics=null;
		decorationMaps=new HashMap<String, Dat>();
		decorationGraphics=new HashMap<String, CPSImage>();
		
		wallMappings=new HashMap<Integer, WallMapping>();

		// Prefill wallmappings with standard walls
		int wallTypeInit[]={0, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		int wallEventMaskInit[]={0, 0, 0, 1, 1, 1, 1, 1, 6, 0, 0, 0, 0, 1, 1, 1, 1, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		int wallFlagsInit[]={7, 0, 0x40, 0xA8, 0x88, 0x88, 0x88, 0x9F, 0xA8, 0x88, 0x88, 0x88, 0x9F, 0xAA, 0x8A, 0x8A, 0x8A, 0x9F, 0xAA, 0x8A, 0x8A, 0x8A, 0x9F, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		for (int i=0; i<wallTypeInit.length; i++)
		{
			WallMapping wm = new WallMapping(i, wallTypeInit[i], 0xff, wallEventMaskInit[i], wallFlagsInit[i], null, null);
			wallMappings.put(i, wm);
		}

		for (int i=0; i<nbrECFBCommands; i++)
		{
			int command=fis.read();
			if (command==0xec)
			{
				fis.read(tmp,0,12);
				String cpsName=getString(tmp);
				if (decorationGraphics.containsKey(cpsName))
					currentDecorationGraphics=(CPSImage)decorationGraphics.get(cpsName);
				else
				{
					currentDecorationGraphics=new CPSImage((Global.BASE+cpsName+".cp_").toLowerCase());
					//currentDecorationGraphics.replacePalette(vcn);
					decorationGraphics.put(cpsName, currentDecorationGraphics);
				}

				fis.read(tmp,0,12);
				String datName=getString(tmp);		
				if (decorationMaps.containsKey(datName))
					currentDecorationMap=(Dat)decorationMaps.get(datName);
				else								
				{
					currentDecorationMap=new Dat((Global.BASE+datName).toLowerCase());
					decorationMaps.put(datName, currentDecorationMap);
				}
			}
			else if (command==0xfb)
			{
				int wallMappingIndex = fis.read();
				
				int wallType = fis.read();
				int decorationID = fis.read();
				int eventMask = fis.read();
				int flags = fis.read()^4;
				
				WallMapping wm = new WallMapping(wallMappingIndex, wallType, decorationID, eventMask, flags, currentDecorationMap, currentDecorationGraphics);
				wallMappings.put(new Integer(wallMappingIndex), wm);
			}
			else
				System.err.println("Parse error in inf-file.");
		}

		// Fetch the script and triggers as raw data
		scriptOffset = (int)fis.getFilePointer();
		
		rawScript = new byte[triggersOffset-scriptOffset];
		fis.read(rawScript);
		
		rawTriggers = new byte[(int)fis.length()-triggersOffset];
		fis.read(rawTriggers);

		script = new Script(this);
		
		fis.close();
	}
}
