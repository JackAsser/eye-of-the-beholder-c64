import java.io.*;
import java.awt.*;
import java.awt.image.*;

public class CPSImage
{
	private static int MODE_OCS = 0;
	private static int MODE_VGA = 1;
	private int mode;
	private boolean containsPalette;

	public byte pixels[];
	public byte r[], g[], b[];

	public CPSImage(String filename)
		throws IOException
	{
		pixels=new byte[320*200];
		r=new byte[256];
		g=new byte[256];
		b=new byte[256];

		File f=new File(filename.toLowerCase());
		byte rawData[] = new byte[(int)f.length()];
		
		FileInputStream fis=new FileInputStream(f);
		fis.read(rawData);
		
		if (rawData.length==40000)
		{
			mode = MODE_OCS;
			containsPalette = false;
		}
		else if (rawData.length<64000)
		{
			mode = MODE_OCS;
			containsPalette = true;
		}
		else if (rawData.length==64000)
		{
			mode = MODE_VGA;
			containsPalette = false;
		}
		else
		{
			mode = MODE_VGA;
			containsPalette = true;
		}

		if (containsPalette)
		{
			if (mode == MODE_OCS)
			{
				for (int i=0;i<32;i++)
				{
					int b1=rawData[40000+i*2+0];
					int b2=rawData[40000+i*2+1];
					r[i]=(byte)((b1&0x0f)<<4);
					g[i]=(byte)(b2&0xf0);
					b[i]=(byte)((b2&0x0f)<<4);
				}
			}
			else
			{
				for (int i=0;i<256;i++)
				{
					r[i]=(byte)(rawData[64000+i*3+0]<<2);
					g[i]=(byte)(rawData[64000+i*3+1]<<2);
					b[i]=(byte)(rawData[64000+i*3+2]<<2);
				}
			}
		}
		else
		{
			if (mode == MODE_OCS)
			{
				replacePalette(new CPSImage((Global.BASE+"invent.cp_").toLowerCase()));
			}
			else
			{
				byte tmp2[] = new byte[768];
				FileInputStream fis2=new FileInputStream((Global.BASE+"BRICK.PAL").toLowerCase());
				fis2.read(tmp2);
				fis2.close();
				for (int i=0; i<256; i++)
				{
					r[i] = (byte)((((int)tmp2[i*3+0])&0x3f)<<2);
					g[i] = (byte)((((int)tmp2[i*3+1])&0x3f)<<2);
					b[i] = (byte)((((int)tmp2[i*3+2])&0x3f)<<2);
				}
			}
		}

		fis.close();
		
		if (mode == MODE_OCS)
		{
			for (int y=0;y<200;y++)
			{
				for (int x=0;x<320;x++)
				{
					int byt=(x+y*320)/8;
					int bit=1<<(7-(x&7));
					int data=0;
					for (int plane=0;plane<5;plane++)
						data+=(rawData[8000*plane+byt]&bit)==0?0:(1<<plane);
					pixels[x+y*320]=(byte)data;
				}
			}
		}
		else
		{
			System.arraycopy(rawData, 0, pixels, 0, 64000);
		}
	}
	
	public void replacePalette(byte[] other_r, byte[] other_g, byte[] other_b)
	{
		System.arraycopy(other_r, 0, r, 0, r.length);
		System.arraycopy(other_g, 0, g, 0, g.length);
		System.arraycopy(other_b, 0, b, 0, b.length);
	}
	
	public void replacePalette(CPSImage other)
	{
		System.arraycopy(other.r, 0, r, 0, r.length);
		System.arraycopy(other.g, 0, g, 0, g.length);
		System.arraycopy(other.b, 0, b, 0, b.length);
	}
	
	public void replacePalette(VCN other)
	{
		byte[][] palette=other.getPalette();
		for (int i=0; i<palette.length; i++)
		{
			byte r=palette[i][0];
			byte g=palette[i][1];
			byte b=palette[i][2];
			if ((r!=0) || (g!=0) || (b!=0))
			{
				this.r[i+1]=r;
				this.g[i+1]=g;
				this.b[i+1]=b;
			}
		}
	}
	
	public BufferedImage getImage()
	{
		IndexColorModel cm = new IndexColorModel(8,256,r,g,b,0);
		MemoryImageSource mis = new MemoryImageSource(320,200,cm,pixels,0,320);
		SampleModel sm = cm.createCompatibleSampleModel(320,200);
		DataBufferByte dbb = new DataBufferByte(pixels, 320*200);
		WritableRaster wr = WritableRaster.createWritableRaster(sm, dbb, new Point(0,0));
		return new BufferedImage(cm,wr,false,null);
	}
	
	public GfxObject getGfxObject(int x, int y, int w, int h)
	{
		byte[] data = new byte[w*h];
		for (int sy=0; sy<h; sy++)
		{
			for (int sx=0; sx<w; sx++)
			{
				data[sx+sy*w] = pixels[x+sx+(y+sy)*320];
			}
		}
		return new GfxObject(w,h,data);
	}
	
	public Color getColor(int index)
	{
		index&=0xff;
		return new Color(r[index]&0xff,g[index]&0xff,b[index]&0xff);
	}
}
