public class WallMapping
{
	public static final int PASSPARTY = 1;
	public static final int PASSSMALL = 2;
	public static final int BLOCKMONSTER = 4;
	public static final int ISDOOR = 8;
	public static final int DOOROPEN = 16;
	public static final int DOORCLOSED = 32;
	public static final int SHOWITEMS = 128;

	public int index;
	public int wallType;
	public int decorationID;
	public Dat.Decoration decoration;
	public int event;
	public int flags;

	public WallMapping(int index, int wallType, int decorationID, int event, int flags, Dat currentDecorationMap, CPSImage decorationGraphics)
	{
		this.index=index;
		this.wallType=wallType;
		this.decorationID=decorationID;
		if (decorationID==0xff)
			decoration=null;
		else
		{
			decoration=currentDecorationMap.decorations[decorationID];
			
			Dat.Decoration d = decoration;
			while(d!=null)
			{
				for (int i=0; i<10; i++)
				{
					Dat.DecorationRectangle dr = d.srcRectangles[i];
					if (dr==null)
						continue;
						
					dr.grabGraphicsFrom(decorationGraphics);
				}
				d=d.next;
			}
		}
		this.event=event;
		this.flags=flags;
	}
	
	public boolean isPlainWall()
	{
		return (wallType==1) || (wallType==2);
	}
}
