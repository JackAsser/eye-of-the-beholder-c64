import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.awt.*;

public class VMP
{
	public int codes[];
	
	private static final int offsetTable[][]=
	{
		{330, 3,15},
		{375, 3,12},
		{411, 2,8 },
		{427, 1,5 },
		{432, 3,5 },
		{447, 2,6 },
		{459, 6,5 },
		{489,10,8 },
		{569,16,12},
	};	

	public VMP(String filename)
		throws IOException
	{
		FileInputStream fis=new FileInputStream(filename.toLowerCase());
		FileChannel fc = fis.getChannel();
		MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
		mbb.order(Global.byteOrder);
		
		int nrCodes = mbb.getShort()&0xffff;
		codes=new int[nrCodes];
		
		for (int i=0; i<nrCodes; i++)
			codes[i] = mbb.getShort()&0xffff;

		fis.close();
	}
	
	public int getNbrOfWallSets()
	{
		return (codes.length-22*15)/431;
	}
	
	private byte[] render(VCN vcn, int baseOffset, int width, int height)
	{
		byte rawPixels[]=new byte[width*8*height*8];

		for (int y=0;y<height;y++)
		{
			for (int x=0;x<width;x++)
			{
				int offset=baseOffset+x+y*width;
				int vmpCode=codes[offset];
				int tileIndex=vmpCode&0x3fff;
				boolean tileFlipped=(vmpCode&0x4000)==0x4000;
				byte tile[]=vcn.getTile(tileIndex, baseOffset!=0);

				if (!tileFlipped)
				{
					for (int py=0;py<8;py++)
						for (int px=0;px<8;px++)
							rawPixels[(x*8+px)+(y*8+py)*width*8] = tile[px+py*8];
				}
				else
				{
					for (int py=0;py<8;py++)
						for (int px=0;px<8;px++)
							rawPixels[(x*8+px)+(y*8+py)*width*8] = tile[7-px+py*8];
				}
			}
		}
		
		return rawPixels;
	}
	
	public byte[] renderBackground(VCN vcn)
	{
		return render(vcn,0,22,15);
	}
	
	public Dimension getBackgroundSize()
	{
		return new Dimension(22,15);
	}
	
	public byte[] renderWall(VCN vcn, int wallset, int wall)
	{
		int base=offsetTable[wall][0]+wallset*431;
		int wallW=offsetTable[wall][1];
		int wallH=offsetTable[wall][2];
		return render(vcn,base,wallW,wallH);
	}
	
	public Dimension getWallSize(int wall)
	{
		int wallW=offsetTable[wall][1];
		int wallH=offsetTable[wall][2];
		return new Dimension(wallW,wallH);
	}
}
