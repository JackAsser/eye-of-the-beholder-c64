import java.io.*;
import java.util.*;

class MazeBlock
{
	public int walls[];
	public int topItemIndex;
	public int triggerBitsAndMonsterCount;
	public int triggerOffset;
	
	public MazeBlock()
	{
		walls=new int[4];
		topItemIndex=0;
		triggerBitsAndMonsterCount=0;
	}
}

public class Maze
{
	public static final int N=0,E=1,S=2,W=3;
	public MazeBlock[] mazeBlocks;
	
	public Maze(String filename)
		throws IOException	
	{
		mazeBlocks = new MazeBlock[32*32];

		FileInputStream fis=new FileInputStream(filename.toLowerCase());
		fis.skip(6);
		for (int y=0;y<32;y++)
			for (int x=0;x<32;x++)
			{
				mazeBlocks[x+y*32] = new MazeBlock();
				for (int d=0;d<4;d++)
				{
					int wmi = fis.read()&0xff;
					mazeBlocks[x+y*32].walls[d] = wmi;
				}
			}
		fis.close();		
	}
	
	public boolean isSolidWall(int x, int y)
	{
		for (int d=0; d<4; d++)
			if ((mazeBlocks[x+y*32].walls[d]<1) || (mazeBlocks[x+y*32].walls[d]>2))
				return false;
		return true;
	}
	
	public boolean isAir(int x, int y)
	{
		for (int d=0; d<4; d++)
			if (mazeBlocks[x+y*32].walls[d]!=0)
				return false;
		return true;
	}
}
