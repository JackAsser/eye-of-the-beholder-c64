import java.io.*;

public class Dat
{
	public Decoration[] decorations;

	class DecorationRectangle
	{
		public GfxObject gfxObject;
		public int x,y,w,h;
		
		DecorationRectangle(FileInputStream fis)
			throws IOException
		{
			x=8*(fis.read()+(fis.read()<<8));
			y=(fis.read()+(fis.read()<<8));
			w=8*(fis.read()+(fis.read()<<8));
			h=(fis.read()+(fis.read()<<8));
		}
		
		public void grabGraphicsFrom(CPSImage cps)
		{
			gfxObject = cps.getGfxObject(x,y,w,h);
		}
	}

	public class Decoration
	{
		public DecorationRectangle[] srcRectangles;
		private int srcRectangleIndices[];
		
		public int nextIndex;
		public Decoration next;
		
		public int flags;
		
		public int destX[];
		public int destY[];
		
		Decoration(FileInputStream fis)
			throws IOException
		{
			srcRectangleIndices=new int[10];
			srcRectangles=new DecorationRectangle[10];
			destX=new int[10];
			destY=new int[10];
			
			for (int i=0; i<10; i++)
				srcRectangleIndices[i]=fis.read();
				
			nextIndex=fis.read();
			
			flags=fis.read();
			
			for (int i=0; i<10; i++)
				destX[i]=(fis.read()+(fis.read()<<8));
			
			for (int i=0; i<10; i++)
				destY[i]=(fis.read()+(fis.read()<<8));
		}		
	}

	Dat(String fileName)
		throws IOException
	{
		FileInputStream fis=new FileInputStream(fileName.toLowerCase());
		
		int nbrDecorations=fis.read()+(fis.read()<<8);
		decorations=new Decoration[nbrDecorations];
		for (int i=0; i<nbrDecorations; i++)
			decorations[i]=new Decoration(fis);

		int nbrRectangles=fis.read()+(fis.read()<<8);
		//System.out.printf("%d rectangles\n", nbrRectangles);
		DecorationRectangle[] rectangles=new DecorationRectangle[nbrRectangles];
		for (int i=0; i<nbrRectangles; i++)
			rectangles[i]=new DecorationRectangle(fis);

		// Resolve dependencies
		for (int i=0; i<nbrDecorations; i++)
		{
			if (decorations[i].nextIndex==0)
				decorations[i].next=null;
			else
				decorations[i].next=decorations[decorations[i].nextIndex];

			for (int j=0; j<10; j++)
			{
				if (decorations[i].srcRectangleIndices[j]==0xff)
					decorations[i].srcRectangles[j]=null;
				else
					decorations[i].srcRectangles[j]=rectangles[decorations[i].srcRectangleIndices[j]];
			}
		}
		
		fis.close();
	}
}
