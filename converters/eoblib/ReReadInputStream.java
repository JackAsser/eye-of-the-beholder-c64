import java.io.*;

public class ReReadInputStream extends BufferedInputStream
{
  public ReReadInputStream(InputStream is)
  {
    super(is);
  }
  
  public ReReadInputStream(InputStream is, int size)
  {
    super(is, size);
  }

  public int available()
    throws IOException
  {
    return super.available();
  }
  
  public void close()
    throws IOException
  {
    super.close();
  }
  
  public void mark(int readLimit)
  {
    super.mark(readLimit);
  }
  
  public boolean markSupported()
  {
    return super.markSupported();
  }
  
  public int read()
    throws IOException
  {
    return super.read();
  }
  
  public int read(byte[] b, int off, int len)
    throws IOException
  {
    return super.read(b, off, len);
  }
  
  public void reset()
    throws IOException
  {
    super.reset();
  }
  
  public long skip(long n)
    throws IOException
  {
    return super.skip(n);
  }
  
  public int getSizeAfterMark()
  {
    if (markpos==-1)
      return 0;
    else
      return pos-markpos;
  }
}
