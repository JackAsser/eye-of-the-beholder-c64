import java.io.*;
import java.nio.*;
import java.nio.channels.*;

public class PartyMember
{
	public static final int CLASS_FIGHTER 	= 0;
	public static final int CLASS_MAGE 		= 1;
	public static final int CLASS_CLERIC 	= 2;
	public static final int CLASS_THIEF 	= 3;
	public static final int CLASS_PALADIN 	= 4;

	// RACE * 2 + gender == Member.race
	public static final int RACE_HUMAN		= 0;
	public static final int RACE_ELF		= 1;
	public static final int RACE_HALFELF	= 2;
	public static final int RACE_DWARF		= 3;
	public static final int RACE_GNOME		= 4;
	public static final int RACE_HALFLING	= 5;

	public static final int SPELL_SHIELD				= 0x02;
	public static final int SPELL_INVISIBLE				= 0x04;
	public static final int SPELL_HASTE 				= 0x08;
	public static final int SPELL_AID	 				= 0x40;
	public static final int SPELL_SLOW_POISON 			= 0x80;
	public static final int SPELL_MAGICAL_VESTMENT		= 0x100;
	public static final int SPELL_PROTECTION_FROM_EVIL 	= 0x200;
	
	public static final int STATUS_ACTIVE 		= 1;
	public static final int STATUS_POISONED 	= 2;
	public static final int STATUS_PARALYZED 	= 4;

	public int position;
	public int status;
	public String name;
	public int strengthCurrent;
	public int strength;
	public int extraStrengthCurrent;
	public int extraStrength;
	public int intelligenceCurrent;
	public int intelligence;
	public int wisdomCurrent;
	public int wisdom;
	public int dexterityCurrent;
	public int dexterity;
	public int constitutionCurrent;
	public int constitution;
	public int charismaCurrent;
	public int charisma;
	public int hpCurrent;
	public int hp;
	public int ac;
	public int disabledHands;
	public int race;
	public int clazz;
	public int alignment;
	public int picture;
	public int food;
	public int level[] = new int[3];
	public int experience[] = new int[3];
	public int unknown;
	public int mageSpells[][] = new int[5][6];	// 5 levels, 6 spells per level max. 0 = empty slot. <0 = -spellIndex used. >0 = spellIndex ready to use.
	public int clericSpells[][] = new int[5][6];
	public int scribedScrolls;
	public int inventory[] = new int[28];
	public int timeouts[] = new int[10];
	public int timeoutActions[] = new int[10];
	public int field_DF;
	public int attackSaves;
	public int field_E1;
	public int temporaryHP;
	public int activeSpells;
	public int damageInfo;
	public int hitInfo[] = new int[2];
	public int magicalGlimmer = -1;
	public byte flags[] = new byte[7];
	
	private String getString(byte[] arr)
	{
		String s=new String(arr);
		int i=0;
		while (arr[i++]!=0);
		return s.substring(0,i-1);
	}
	
	public PartyMember(ByteBuffer bb)
	{
		position = bb.get();
		status = bb.get();
		
		byte[] tmp = new byte[11];
		bb.get(tmp);
		name = getString(tmp);

		strengthCurrent = bb.get();
		strength = bb.get();
		extraStrengthCurrent = bb.get();
		extraStrength = bb.get();
		intelligenceCurrent = bb.get();
		intelligence = bb.get();
		wisdomCurrent = bb.get();
		wisdom = bb.get();
		dexterityCurrent = bb.get();
		dexterity = bb.get();
		constitutionCurrent = bb.get();
		constitution = bb.get();
		charismaCurrent = bb.get();
		charisma = bb.get();
		hpCurrent = bb.get();
		hp = bb.get();
		ac = bb.get();
		disabledHands = bb.get();
		race = bb.get();
		clazz = bb.get();
		alignment = bb.get();
		picture = bb.get();
		food = bb.get();

		for (int i=0; i<3; i++)
			level[i] = bb.get();
		
		for (int i=0; i<3; i++)
			experience[i] = bb.getInt();
		
		unknown = bb.getInt();
		
		for (int lvl=0; lvl<5; lvl++)
			for (int slot=0; slot<6; slot++)
				mageSpells[lvl][slot] = bb.get();
		
		for (int lvl=0; lvl<5; lvl++)
			for (int slot=0; slot<6; slot++)
				clericSpells[lvl][slot] = bb.get();

		scribedScrolls = bb.getInt();

		for (int i=0; i<28; i++)
			inventory[i] = bb.getShort();

		for (int i=0; i<10; i++)
			timeouts[i] = bb.getInt();

		for (int i=0; i<10; i++)
			timeoutActions[i] = bb.get();
			
		field_DF = bb.get();
		attackSaves = bb.get();
		field_E1 = bb.get();
		temporaryHP = bb.get();
		activeSpells = bb.getInt();
		damageInfo = bb.get();
		
		for (int i=0; i<2; i++)
			hitInfo[i] = bb.get();
			
		bb.get(flags);
	}
	
	public String toString()
	{
		return String.format("%s\nSTR: %d/%d\nINT: %d\nWIS: %d\nDEX: %d\nCON: %d\nCHA: %d\n HP: %d/%d\n AC: %d\n",
			name,
			strength, extraStrength,
			intelligence,
			wisdom,
			dexterity,
			constitution,
			charisma,
			hpCurrent, hp,
			ac);
	}
}
