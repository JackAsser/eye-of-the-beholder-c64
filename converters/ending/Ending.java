import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Ending {
	static void convertCouncil() throws Exception {
		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 8;
		C64Char.pixelWidth = 2;

		BufferedImage bitmap = ImageIO.read(new File("veto/council_frames_magenta_blackmask.png"));
		new C64Bitmap(bitmap, 0,0, 40,9).save("converted/council_table", true, true, true);
		System.out.println("* council_table");

		int frames[][][] = new int[][][] {
			{
				{ 0, 9,3,3},
				{ 0,16,3,3},
				{ 0,23,3,3}
			},
			{
				{ 7, 9,3,5},
				{ 7,16,3,5},
				{ 7,23,3,5}
			},
			{
				{12, 9,4,7},
				{12,16,4,7},
				{12,23,4,7}
			},
			{
				{17, 9,7,2},
				{17,16,7,2},
				{17,23,7,2},
				{17,30,7,2}
			},
			{
				{25, 9,3,3},
				{25,16,3,3},
				{25,23,3,3}
			},
			{
				{31, 9,3,3},
				{31,16,3,3},
				{31,23,3,3}
			},
			{
				{37, 9,3,4},
				{37,16,3,4},
				{37,23,3,4}
			}
		};

		for (int character=0; character<frames.length; character++) {
			for (int frame=0; frame<frames[character].length; frame++) {
				int cx=frames[character][frame][0];
				int cy=frames[character][frame][1];
				int cw=frames[character][frame][2];
				int ch=frames[character][frame][3];
				String name = String.format("converted/council_frames_magenta_%d_%d", character, frame);
				new C64Bitmap(bitmap, cx,cy, cw,ch).save(name, true, true, true);
				System.out.println("* "+name);
			}
		}

		bitmap = ImageIO.read(new File("veto/council_frames_yellow_blackmask.png"));
		frames = new int[][][] {
			{
				{ 0, 9,4,1},
				{ 0,12,4,1},
				{ 0,15,4,1}
			},
			{
				{ 7, 9,3,2},
				{ 7,12,3,2},
				{ 7,15,3,2},
			},
			{
				{18, 9,5,3},
				{18,12,5,3},
				{18,15,5,3},
				{18,18,5,3}
			},
			{
				{25, 9,3,2},
				{25,12,3,2},
				{25,15,3,2}
			},
			{
				{33, 9,2,1},
				{33,12,2,1},
				{33,15,2,1}
			},
			{
				{37, 9,2,2},
				{37,12,2,2},
				{37,15,2,2}
			}
		};

		for (int character=0; character<frames.length; character++) {
			for (int frame=0; frame<frames[character].length; frame++) {
				int cx=frames[character][frame][0];
				int cy=frames[character][frame][1];
				int cw=frames[character][frame][2];
				int ch=frames[character][frame][3];
				String name = String.format("converted/council_frames_yellow_%d_%d", character, frame);
				new C64Bitmap(bitmap, cx,cy, cw,ch).save(name, true, true, true);
				System.out.println("* "+name);
			}
		}

		bitmap = ImageIO.read(new File("veto/council_sprites.png"));
		C64SpriteSheet sheet = new C64SpriteSheet(bitmap, 0, 0, 5, 8, new int[]{0x04,0x09,0x08}, new int[] {
			0xa,0xc, -1, -1, -1,
			0xa,0xc,0x0,0xf,0x7,
			0xa,0xc, -1, -1, -1,
			0xa,0xc,0x0,0xf,0x7,
			0xa,0xc, -1, -1, -1,
			0xa,0xc,0x0,0xf,0x7,
			 -1,0xc, -1, -1, -1,
			0xa,0xc,0x0, -1, -1
		});

		sheet.save(String.format("converted/council_sprites.bin"));
		System.out.printf("* council_sprites uses %d(%d) sprites.\n", sheet.sprites.size(), sheet.sprites.size());
	}

	static void convertText() throws Exception {
		BufferedImage bitmap = ImageIO.read(new File("veto/ending_text.png"));

		C64Char.defaults();
		C64Char.multicolor = false;
		C64Char.defaultBackgroundColor = 0;
		C64Char.pixelWidth = 1;

		C64CharObject cobj = new C64CharObject(bitmap, 0,0, 40,25);
		System.out.printf("* ending_text_screen uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/ending_text", true, true, false);
	}

	static void convertEndScroller() throws Exception {
		C64Char.defaults();
		C64Char.multicolor = false;
		C64Char.defaultBackgroundColor = 0;
		C64Char.pixelWidth = 1;

		BufferedImage bitmap = ImageIO.read(new File("veto/ending_credits_charset1.png"));
		C64CharObject cobj = new C64CharObject(bitmap, 0,0, 40,75);
		System.out.printf("* ending_credits_charset1 uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/ending_credits_charset1", true, true, false);

		bitmap = ImageIO.read(new File("veto/ending_credits_charset2.png"));
		cobj = new C64CharObject(bitmap, 0,0, 40,75);
		System.out.printf("* ending_credits_charset2 uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/ending_credits_charset2", true, true, false);

		C64SpriteSheet sheet = new C64SpriteSheet(
			ImageIO.read(new File("veto/ending_theend_sprites.png")),
			0,0, 4,2,
			new int[]{0x0},
			new int[]{
				0x8e, 0x8e, 0x8e, 0x8e,
				-1,	 0x8e, 0x8e, 0x8e
			}
		);
		sheet.save(String.format("converted/ending_theend_sprites.bin"));
		System.out.printf("* ending_theend_sprites uses %d sprites.\n", sheet.sprites.size());
	}

	static void convertHighPriest() throws Exception {
		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 8;
		C64Char.pixelWidth = 2;

		BufferedImage bitmap = ImageIO.read(new File("veto/ending_highpriest_bitmap.png"));

		new C64Bitmap(bitmap, 24,1, 16,5).save("converted/highpriest_eyes", true, true, true);
		System.out.println("* highpriest_eyes");

		new C64Bitmap(bitmap, 24,7, 16,9).save("converted/eye_stalk", true, true, true);
		System.out.println("* eye_stalk");

		new C64Bitmap(bitmap, 0,0, 12,15).save("converted/highpriest", true, true, true);
		System.out.println("* highpriest");

		new C64Bitmap(bitmap, 12,0, 12,9).save("converted/ending_right_hand", true, true, true);
		System.out.println("* ending_right_hand");

		new C64Bitmap(bitmap, 9,16, 15,8).save("converted/ending_shake_hands", true, true, true);
		System.out.println("* ending_shake_hands");

		C64SpriteSheet sheet = new C64SpriteSheet(
			ImageIO.read(new File("veto/ending_highpriest_hand_sprites.png")),
			0,0, 7,4,
			new int[]{0xe,0x8,0x9},
			new int[]{
				 -1, -1,0xa, -1,0x6, -1, -1,
				0xa,0xa,0xa, -1,0x6,0x0, -1,
				0x0,0x0,0xa,0xa,0x6,0x0,0x0,
				 -1,0x0,0x0,0xa, -1, -1,0x0
			}
		);
		sheet.save(String.format("converted/ending_left_hand_sprites.bin"));
		System.out.printf("* ending_left_hand_sprites uses %d sprites.\n", sheet.sprites.size());

		sheet = new C64SpriteSheet();
		sheet.append(
			ImageIO.read(new File("veto/ending_highpriest_eye_closed_sprites.png")),
			0,0, 4,1,
			new int[]{0xc,0x0,0x9},
			new int[]{0x8,0x8,0x8,0x8}
		);
		sheet.append(
			ImageIO.read(new File("veto/ending_highpriest_eye_left_sprites.png")),
			0,0, 4,1,
			new int[]{0xc,0x0,0x6},
			new int[]{0xe,0xe,0xe,0xe}
		);
		sheet.append(
			ImageIO.read(new File("veto/ending_highpriest_eye_right_sprites.png")),
			0,0, 4,1,
			new int[]{0xc,0x0,0x6},
			new int[]{0xe,0xe,0xe,0xe}
		);
		sheet.save(String.format("converted/highpriest_eyes_sprites.bin"));
		System.out.printf("* highpriest_eyes_sprites uses %d sprites.\n", sheet.sprites.size());
	}

	public static void main(String[] args) {
		try {
			convertText();
			convertCouncil();
			convertHighPriest();
			convertEndScroller();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
