import javax.imageio.*;
import java.io.*;

public class Convert {
	public static void main(String[] args) {
		try {
			new C64SpriteSheet(
				ImageIO.read(new File("gfx/portala.png")),
				0, 0, 5, 5,
				new int[]{0x10,0x00,0x0b},
				new int[] {
					0x0c,0x0c,0x0c,0x0c,0x0c,
					0x0c,0x0c,0x0c,0x0c,0x0c,
					0x0c,0x0c,0x0c,0x0c,0x0c,
					0x0c,0x0c,0x0c,0x0c,0x0c,
					0x0c,0x0c,0x0c,0x0c,0x0c
			}).save("converted/portal_sprites.bin");

			var bitmap = ImageIO.read(new File("gfx/portalb.png"));
			Declash.declash(bitmap);
			new C64Bitmap(bitmap, 0,0, 88,10).save("converted/portal", true, true, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
