import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;

public class Portal extends JFrame {
	private static final int ZOOM = 1;
	private byte pixels[] = new byte[22*8*15*8];

	class EobShape {
		public BufferedImage src;
		public int x;
		public int y;
		public int w;
		public int h;

		public EobShape(BufferedImage src, int cx, int y, int cw, int h) {
			this.src = src;
			this.x = cx*8;
			this.y = y;
			this.w = cw*8;
			this.h = h;
		}

		public void draw(Graphics g, int dx, int dy) {
			g.drawImage(src, dx,dy,dx+w,dy+h, x,y,x+w,y+h, null);
		}
	}

	private void delay(int ticks) {
		try {
			Thread.sleep(ticks*55);
		} catch (InterruptedException e) {
			;
		}
	}

	private void copyRegion(BufferedImage src, int x1, int y1, int x2, int y2, int w, int h, Graphics g) {
		g.drawImage(src, x2,y2,x2+w,y2+h, x1,y1,x1+w,y1+h, null);
	}

	public Portal() throws IOException {
		super("Portal");

		EobShape shapes1[] = new EobShape[5];
		EobShape shapes2[] = new EobShape[5];
		EobShape shapes3[] = new EobShape[5];
		EobShape shape0;

		BufferedImage portalA = ImageIO.read(new File("original/portalA.gif"));
		for (int i=0; i<5; i++) {
			shapes1[i] = new EobShape(portalA, i * 3, 0, 3, 75);
			shapes2[i] = new EobShape(portalA, i * 3, 80, 3, 75);
			shapes3[i] = new EobShape(portalA, 15, i * 18, 15, 18);
		}
		shape0 = new EobShape(portalA, 30, 0, 8, 77);

		BufferedImage portalB = ImageIO.read(new File("original/portalB.gif"));

		BufferedImage offscreen = new BufferedImage(22*8, 15*8, BufferedImage.TYPE_INT_RGB);

		JPanel panel = new JPanel() {
			public void paint(Graphics g) {
				g.drawImage(offscreen, 0,0, offscreen.getWidth()*ZOOM, offscreen.getHeight()*ZOOM, null);
				g.setColor(Color.darkGray);
/*				for (int x=0; x<offscreen.getWidth()*ZOOM; x+=8*ZOOM)
					g.drawLine(x,0,x,offscreen.getHeight()*ZOOM);
				for (int y=0; y<offscreen.getHeight()*ZOOM; y+=8*ZOOM)
					g.drawLine(0,y,offscreen.getWidth()*ZOOM,y);
*/			}

			public Dimension getPreferredSize() {
				return new Dimension(offscreen.getWidth()*ZOOM, offscreen.getHeight()*ZOOM);
			}
		};

		int portalSeq[] = new int[] {
		    0x1,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
	        0x0,0x0,0x1,0x0,0x2,0x0,0x1,0x0,
	        0x0,0x0,0x1,0x0,0x2,0x0,0x3,0x0,
	        0x2,0x0,0x1,0x0,0x2,0x0,0x3,0x0,
	        0x4,0x0,0x3,0x0,0x2,0x0,0x1,0x0,
	        0x0,0x0,0x0,0x0,0x0,0x1,0x0,0x0,
	        0x0,0x1,0x0,0x2,0x0,0x1,0x0,0x0,
	        0x0,0x1,0x0,0x2,0x0,0x3,0x0,0x4,
	        0x0,0x1,0x1,0x0,0x2,0x1,0x3,0x2,
	        0x2,0x3,0x1,0x4,0x0,0x2,0x1,0x3,
	        0x2,0x4,0x3,0x2,0x4,0x3,0x3,0x4,
	        0x2,0x2,0x1,0x1,0x0,0x0,0x0,0x0,
	        0x0,0x0,0x1,0x1,0x2,0x2,0x3,0x3,
	        0x4,0x4,0x4,0x5,0x4,0x6,0x3,0x7,
	        0x2,0x8,0x2,0x8,0x1,0x9,0x1,0x9,
	        0x1,0x9,0x0,0xA, -1, -1
	    };

		Graphics g = offscreen.getGraphics();
		new Thread(() -> {
			g.setColor(Color.darkGray);
			g.fillRect(0,0, offscreen.getWidth()*ZOOM, offscreen.getHeight()*ZOOM);
			shapes3[0].draw(g, 28, 9);
			shapes1[0].draw(g, 34, 28);
			shapes2[0].draw(g, 120, 28);
			shape0.draw(g, 56, 27);
			panel.repaint();
/*
			delay(30);

			for (int pos=0; portalSeq[pos]>-1;) {
				int s = portalSeq[pos++];
				shapes3[s].draw(g, 28, 9);
				shapes1[s].draw(g, 34, 28);
				shapes2[s].draw(g, 120, 28);

				s = portalSeq[pos++];
				if (s == 0) {
					shape0.draw(g, 56, 27);
				} else {
					s--;
					copyRegion(portalB, (s%5)*64,s/5*77, 56,27, 64,77, g);
				}

				panel.repaint();
				delay(2);
			}*/
		}).start();

		setLayout(new BorderLayout());
		add(panel, BorderLayout.CENTER);
		setResizable(false);
		setVisible(true);
		pack();
		toFront();
	}

	public static void main(String[] args) {
		try {
			new Portal();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}