import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Convert {
	public static void main(String[] args) {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		try {			
			BufferedImage bitmap = ImageIO.read(new File("converted/portraits-64.png"));
			byte pixels[] = ((DataBufferByte)bitmap.getRaster().getDataBuffer()).getData();
			for (int i=0; i<pixels.length; i++)
				if (pixels[i]>=16)
					pixels[i]=0;
			for (int i=0; i<bitmap.getWidth(); i++)
				pixels[31*bitmap.getWidth()+i]=0;
			Declash.declash(bitmap, C64Char.spriteColors);
			ImageIO.write(bitmap, "PNG", new File("portraits.png"));

			FileOutputStream f0 = new FileOutputStream("portraits-bitmap.bin");
			FileOutputStream f1 = new FileOutputStream("portraits-screen.bin");
			FileOutputStream f2 = new FileOutputStream("portraits-d800.bin");

			System.out.printf("%d portraits found.\n", bitmap.getWidth()/16);

			for (int bcx=0; bcx<bitmap.getWidth()/4; bcx+=4) {
				EOBObject obj = new EOBObject(bitmap,bcx,0,4,4);
				for (int cy=0; cy<4; cy++) {
					for (int cx=0; cx<4; cx++) {
						C64Char ch = obj.c64Chars[cx*4+cy];
						if (ch.type != C64Char.CharType.Bitmap)
							throw new Exception("WTF!");
						for (int i=0; i<8; i++)
							f0.write(ch.data.get(i)&0xff);
						f1.write(ch.screen&0xff);
						f2.write(ch.d800&0xff);
					}
				}
			}
			f0.close();
			f1.close();
			f2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}