import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Buttons {
	public static EOBBank convert() throws Exception {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		BufferedImage bitmap = ImageIO.read(new File("converted/buttons64.png"));
		Declash.declash(bitmap, C64Char.spriteColors);

		ArrayList<EOBObject> eobObjects = new ArrayList<EOBObject>();

		// Brick 1
		eobObjects.add(new EOBObject(bitmap, 0,3, 2,2));

		// Brick 2
		eobObjects.add(new EOBObject(bitmap, 0,0, 1,2));

		// Blue 1
		eobObjects.add(new EOBObject(bitmap, 0,8, 2,2));
		eobObjects.add(new EOBObject(bitmap, 4,8, 2,2));
		eobObjects.add(new EOBObject(bitmap, 8,8, 2,2));
		eobObjects.add(new EOBObject(bitmap,12,8, 2,2));
		eobObjects.add(new EOBObject(bitmap,16,8, 2,2));

		// Blue 2
		eobObjects.add(new EOBObject(bitmap, 0,6, 1,1));
		eobObjects.add(new EOBObject(bitmap, 4,6, 1,1));
		eobObjects.add(new EOBObject(bitmap, 8,6, 2,1));
		eobObjects.add(new EOBObject(bitmap,12,6, 2,1));
		eobObjects.add(new EOBObject(bitmap,16,6, 1,1));
		
		// Drow 1
		eobObjects.add(new EOBObject(bitmap, 0,14, 1,3));

		// Drow 2
		eobObjects.add(new EOBObject(bitmap, 0,11, 1,2));

		// Green 1
		eobObjects.add(new EOBObject(bitmap, 0,21, 2,2));

		// Green 2
		eobObjects.add(new EOBObject(bitmap, 0,18, 1,2));

		// Xanatha 1
		eobObjects.add(new EOBObject(bitmap, 0,26, 1,2));

		// Xanatha 2
		eobObjects.add(new EOBObject(bitmap, 0,24, 1,1));

		ImageIO.write(bitmap, "PNG", new File("buttons-auto.png"));

		return new EOBBank("graphics", eobObjects);
	}

	public static void main(String[] args) {
		try {				
			EOBBank buttons = Buttons.convert();
			buttons.dump("button_");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
