import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Convert {
	public Convert(String wallsetName) throws Exception {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		BufferedImage bitmap = ImageIO.read(new File(String.format("converted/%s_doors64.png", wallsetName)));
		if (wallsetName.equals("drow"))
			Declash.colorBias[7] = 100000;
		Declash.declash(bitmap, C64Char.spriteColors);

		System.out.printf("Read %dx%d\n", bitmap.getWidth(), bitmap.getHeight());
		int grab[][] = new int[][]{
			{5,1,12,11},
			{7,3,9,6},
			{8,3,6,5}
		};

		ImageIO.write(bitmap, "PNG", new File(String.format("%s-auto.png", wallsetName)));

		ArrayList<Byte> bank = new ArrayList<>();
		ArrayList<Short> offsets = new ArrayList<>();
		ArrayList<Byte> screenData = new ArrayList<>();
		ArrayList<Byte> d800Data = new ArrayList<>();

		int dy=0;
		for (int zoom=0; zoom<3; zoom++) {
			int dx=0;
			int cw=grab[zoom][2];
			int ch=grab[zoom][3];
			for (int pos=0; pos<6; pos++) {
				for (int cx=dx; cx<dx+cw; cx++) {
					for (int cy=dy; cy<dy+ch; cy++) {
						C64Char c = new C64Char(bitmap, cx, cy);

						screenData.add(c.screen);
						d800Data.add(c.d800);

						short offset=(short)0x8000; // Negative offset = skipped char, see row 79 in render_door.s
						switch(c.type) {
							case Empty:
								break;
							case Bitmap: {
								boolean match = false;
								n:for (int i=0; i<bank.size()-8; i++) {
									for (int j=0; j<8; j++) {
										if (bank.get(i+j) != c.data.get(j))
											continue n;
									}
									match = true;
									offset = (short)(i);
								}
								if (!match) {
									offset = (short)(bank.size());
									for (int j=0; j<8; j++)
										bank.add(c.data.get(j));
								}
								break;
							}
							case Sprite: {
								boolean match = false;
								n:for (int i=0; i<bank.size()-12; i++) {
									for (int j=0; j<4; j++) {
										if (bank.get(i+j) != c.mask.get(j))
											continue n;
									}
									for (int j=0; j<8; j++) {
										if (bank.get(i+j+4) != c.data.get(j))
											continue n;
									}
									match = true;
									offset = (short)(i);
								}
								if (!match) {
									offset = (short)(bank.size());
									for (int j=0; j<4; j++)
										bank.add(c.mask.get(j));
									for (int j=0; j<8; j++)
										bank.add(c.data.get(j));
								}
								break;
							}
						}
						offsets.add(offset);
					}
				}

				dx+=cw;
			}
			dy+=ch;
		}

		// Calculate all offsets and sizes
		ArrayList<Integer> objOffsets = new ArrayList();
		ArrayList<Integer> objWidths = new ArrayList();
		ArrayList<Integer> objHeights = new ArrayList();
		int offset = 0;
		for (int zoom=0; zoom<3; zoom++) {
			int cw=grab[zoom][2];
			int ch=grab[zoom][3];
			for (int pos=0; pos<6; pos++) {
				objOffsets.add(offset);
				objWidths.add(cw);
				objHeights.add(ch);
				offset+=cw*ch;
			}
		}

		// Emit binary data
		String[] arrayNames = new String[]{
			"doorGraphicsOffsetsLo",
			"doorGraphicsOffsetsHi",
			"doorGraphicsOffsetsScreen",
			"doorGraphicsOffsetsD800"
		};
		String[] referenceNames = new String[]{
			"offsets_lo",
			"offsets_hi",
			"screen",
			"d800"
		};

		FileOutputStream fos;
		fos = new FileOutputStream(wallsetName+"_data.bin");
		for (Byte b : bank) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[0]+".bin");
		for (Short s : offsets) {
			int i = (int)(s&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[1]+".bin");
		for (Short s : offsets) {
			int i = (int)((s>>8)&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[2]+".bin");
		for (Byte b : screenData) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[3]+".bin");
		for (Byte b : d800Data) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		// Emit source
		PrintStream ps = new PrintStream(new File(String.format("../../%s_doors.s", wallsetName)));

		ps.printf(".segment \"%s_DOORS\"\n", wallsetName.toUpperCase());
		ps.printf(".export %s_doors\n", wallsetName);
		ps.printf("%s_doors:\n\n", wallsetName);

		ps.printf("jmp renderDoor\n\n");

		ps.println("doorGraphicsBitmap:");
		ps.printf("\t.incbin \"converters/doors/%s_data.bin\"\n\n", wallsetName);
		for (int i=0; i<referenceNames.length; i++) {
			ps.printf("%s:\n", referenceNames[i]);
			ps.printf("\t.incbin \"converters/doors/%s_%s.bin\"\n", wallsetName, referenceNames[i]);
			ps.println();
		}

		ps.println("doorGraphicsWidth:");
		ps.printf("\t.byte ");
		for (int i=0; i<objWidths.size(); i++) {
			if (i>0)
				ps.printf(",");
			ps.printf("$%02x", objWidths.get(i));
		}
		ps.println();
		ps.println();

		ps.println("doorGraphicsHeight:");
		ps.printf("\t.byte ");
		for (int i=0; i<objHeights.size(); i++) {
			if (i>0)
				ps.printf(",");
			ps.printf("$%02x", objHeights.get(i));
		}
		ps.println();
		ps.println();

		for (int j=0; j<arrayNames.length; j++) {
			ps.printf("%s_lo:", arrayNames[j]);
			ps.printf("\n\t.byte ");
			for (int i=0; i<objOffsets.size(); i++) {
				if (i>0)
					ps.printf(",");
				ps.printf("<(%s+$%04x)", referenceNames[j], objOffsets.get(i));
			}
			ps.println();
			ps.println();

			ps.printf("%s_hi:", arrayNames[j]);
			ps.printf("\n\t.byte ");
			for (int i=0; i<objOffsets.size(); i++) {
				if (i>0)
					ps.printf(",");
				ps.printf(">(%s+$%04x)", referenceNames[j], objOffsets.get(i));
			}
			ps.println();
			ps.println();
		}

		ps.printf(".include \"render_door.s\"\n");
	}

	public static void main(String[] args) {
		try {
			new Convert(args[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
