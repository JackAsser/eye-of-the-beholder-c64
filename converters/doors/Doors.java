import java.io.*;
import java.nio.*;
import java.awt.image.*;
import javax.imageio.*;

public class Doors {
	private byte frameBuffer[];
	private int tiles[];
	private int levelIndex;
	private Inf inf;

	public void calculateDoorClip(int scanIndex, int clipY[]) {
		int doorClipYcoords[][] = {
			{ 31,  24,  15,   0},
			{  0,   0,   0,   0},
			{ 32,  32,  24,   0},
			{ 30,  24,  16,   0},
			{ 58,  72,  96, 120},
			{ 58,  70,  86,   0},
			{120, 120, 120, 120},
			{ 30,  24,  15}
		};
		int doorClipY0indices[] = {0,   0,   0,   0,   4,   4,   4,   8,   8,   8,  12,  12, 28};
		int doorClipY1indices[] = {28,  16,  16,  16,  24,  24,  24,  16,  16,  16,  20,  20,  24};
		int doorClipY0index = doorClipY0indices[levelIndex]/4;
		int zoom = objectScanPositionZoom[scanIndex];
		clipY[0] = doorClipYcoords[doorClipY0index][zoom];
		int doorClipY1index =doorClipY1indices[levelIndex]/4;
		clipY[1] = doorClipYcoords[doorClipY1index][zoom];
		setClipFine(wallX0*8,clipY[0],wallX1*8,clipY[1]);
	}

	int wallX0, wallX1;
	int clipX=0;
	int clipY=0;
	int clipW=22*8;
	int clipH=15*8;
	void setClip(int x0, int y0, int x1, int y1) {
		clipX=x0*8;
		clipY=y0*8;
		clipW=(x1-x0)*8;
		clipH=(y1-y0)*8;
	}
	
	void setClipFine(int x0, int y0, int x1, int y1) {
		clipX=x0;
		clipY=y0;
		clipW=x1-x0;
		clipH=y1-y0;
	}

	public static int wallMappingToDoorTypeOffset[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 3};
	public static int wallMappingDoorStartIndex[] = {0, 0, 0, 3, 3, 3, 3, 3, 8, 8, 8, 8, 8,13,13,13,13,13,18,18,18,18,18, 0, 0, 0, 0, 0, 0, 0,30,31};
	public static int objectScanPositionZoom[] = {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3};
	public static int objectScanPositionDeltas[][][] = {
		{{-111, -63}, {-95,  -63}, {-139, -59}, {-117, -59}, {-120, -61}},
		{{-76,  -63}, {-60,  -63}, {-95,  -59}, {-74,  -59}, {-80,  -61}},
		{{-43,  -63}, {-27,  -63}, {-53,  -59}, {-31,  -59}, {-40,  -61}},
		{{-8,   -63}, { 8,   -63}, {-10,  -59}, { 10,  -59}, { 0,   -61}},
		{{ 27,  -63}, { 43,  -63}, { 31,  -59}, { 53,  -59}, { 40,  -61}},
		{{ 60,  -63}, { 76,  -63}, { 74,  -59}, { 95,  -59}, { 80,  -61}},
		{{ 95,  -63}, { 111, -63}, { 117, -59}, { 139, -59}, { 120, -61}},
		{{-118, -53}, {-92,  -53}, {-152, -45}, {-120, -45}, {-118, -50}},
		{{-66,  -53}, {-40,  -53}, {-84,  -45}, {-51,  -45}, {-59,  -50}},
		{{-13,  -53}, { 13,  -53}, {-16,  -45}, { 16,  -45}, { 0,   -50}},
		{{ 40,  -53}, { 66,  -53}, { 51,  -45}, { 84,  -45}, { 59,  -50}},
		{{ 92,  -53}, { 118, -53}, { 120, -45}, {152,  -45}, { 118, -50}},
		{{-110, -35}, {-67,  -35}, {-140, -22}, {-83,  -22}, {-98,  -30}},
		{{-22,  -35}, { 22,  -35}, {-27,  -22}, { 27,  -22}, { 0,   -30}},
		{{ 67,  -35}, { 110, -35}, { 83,  -22}, {140,  -22}, { 98,  -30}},
		{{-128, -4 }, { 128, -4 }, {-128, -66}, {128,  -66}, {-128,  0 }},
		{{-38,  -4 }, { 38,  -4 }, {-38,  -66}, { 38,  -66}, { 0,    0 }},
		{{-128, -4 }, { 128, -4 }, {-128, -66}, {128,  -66}, { 128,  0 }}
	};

	public void drawDoorOnPosition(int scanIndex, int frontWallMappingIndex) {
		int wallMappingHasDoorKnob[] = {0, 0, 0, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, -1, -1, -1, -1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		int lvl1_2_3doorBottomY[] = {58,71,86,0};
		int lvl4_5_6doorBottomY[] = {59,71,88,0};
		int lvl7_8_9doorBottomY[] = {32,32,24,0};
		int lvl10_11doorBottomY[] = {30,24,16};
		int lvl10_11doorTopY[] = {48,52,59};
		int lvl4_5_6doorX[] = {0, 0,-24,16,32,64,72,104,112,144,160,200, 0, 0, 0, 0,-24,48,56,120,128,200, 0, 0,-88,32,40,136,144,264, 0, 0, 0, 0, 0, 0};
		int lvl12doorBottomY[] = {58,69,90,0};
		int lvl1_2_3_12doorOpenStepY[] = {8,12,18, 0};
		int lvl1_2_3_12doorJammedDeltaY[] = {0, 2, 4, 0};
		int lvl4_5_6doorOpenStepX[] = {4, 6, 9, 0};
		int lvl4_5_6doorJammedDeltaX[] = {0, 2, 4, 0};
		int lvl10_11doorOpenStepY[] = {7,11,14, 0};
		int lvl10_11doorJammedDeltaY[] = {0, 2, 4, 0};

		int clipY[] = new int[2];
		int zoom = objectScanPositionZoom[scanIndex];
		int doorGfxOffset = wallMappingToDoorTypeOffset[frontWallMappingIndex]+2-zoom;
		if (doorGfxOffset<0)
			return;
		
		GfxObject doorGfx = inf.doorGfx[doorGfxOffset];
		
		if (doorGfx==null) {
			System.out.printf("WTF is this door?!? wmi was %d\n", frontWallMappingIndex);
			return;
		}
		
		int deltaX = objectScanPositionDeltas[scanIndex][4][0];
		int xpos = deltaX+88;
		if ((levelIndex<7) || (levelIndex>9)) {
			// lvl 1..6, 10..12 (i.e. drow levels excluded)
			calculateDoorClip(scanIndex, clipY);			
		}
		
		switch (levelIndex)
		{
			case 1:
			case 2:
			case 3:
			case 12:
			{
				int ypos = 0;
				if (levelIndex==12)
					ypos = lvl12doorBottomY[zoom];
				else
					ypos = lvl1_2_3doorBottomY[zoom];
				ypos-=doorGfx.height;
				
				xpos-=doorGfx.width/2;
				int doorOpenDeltaY=0;
				if (frontWallMappingIndex < 30)
				{
					int doorOpenStep = frontWallMappingIndex - wallMappingDoorStartIndex[frontWallMappingIndex];
					doorOpenDeltaY = doorOpenStep * lvl1_2_3_12doorOpenStepY[zoom]; 
				}
				else
				{
					doorOpenDeltaY = lvl1_2_3_12doorJammedDeltaY[zoom];
				}
				ypos-=doorOpenDeltaY;
				drawMaskedGfxObject(doorGfx,xpos,ypos);
				
				if (wallMappingHasDoorKnob[frontWallMappingIndex]==-1)
				{
					GfxOverlay knob = inf.doorKnobs[doorGfxOffset];
					drawMaskedGfxObject(knob.gfxObject, knob.deltaX, knob.deltaY);
				}
				
				break;
			}
			case 4:
			case 5:
			case 6:
			{
				int ypos = lvl4_5_6doorBottomY[zoom] - doorGfx.height;
				int x0 = lvl4_5_6doorX[scanIndex*2+0]/8;
				int x1 = lvl4_5_6doorX[scanIndex*2+1]/8;
				if (wallX0>x0)
					x0=wallX0;
				if (wallX1<x1)
					x1=wallX1;
				setClip(x0,0,x1,15);
				
				int doorOpenDeltaX=0;
				if (frontWallMappingIndex < 30)
				{
					int doorOpenStep = frontWallMappingIndex - wallMappingDoorStartIndex[frontWallMappingIndex];
					doorOpenDeltaX = doorOpenStep * lvl4_5_6doorOpenStepX[zoom];
				}
				else
				{
					doorOpenDeltaX = lvl4_5_6doorJammedDeltaX[zoom];
				}
				doorOpenDeltaX =- doorOpenDeltaX;				

				doorOpenDeltaX -= doorGfx.width;
				drawMaskedGfxObject(doorGfx,xpos+doorOpenDeltaX,ypos);

				doorOpenDeltaX += doorGfx.width;
				drawMaskedFlippedGfxObject(doorGfx,xpos-doorOpenDeltaX,ypos);

				if (wallMappingHasDoorKnob[frontWallMappingIndex]==-1)
				{
					GfxOverlay knob = inf.doorKnobs[doorGfxOffset];
					drawMaskedGfxObject(knob.gfxObject, knob.deltaX+deltaX-doorOpenDeltaX, knob.deltaY);
				}

				break;
			}
			case 7:
			case 8:
			case 9:
			{
				GfxObject topBar = inf.doorGfx[3+doorGfxOffset];

				int ypos = lvl7_8_9doorBottomY[zoom];
				int topBarXpos = xpos-topBar.width/2;
				xpos-=doorGfx.width/2;
				ypos-=topBar.height;
				drawMaskedGfxObject(topBar,topBarXpos,ypos);

				calculateDoorClip(scanIndex, clipY);

				ypos = lvl7_8_9doorBottomY[zoom];
				int doorOpenDeltaY=0;
				if (frontWallMappingIndex<30)
				{
					int doorOpenStep = frontWallMappingIndex - wallMappingDoorStartIndex[frontWallMappingIndex];
					doorOpenDeltaY = doorOpenStep * lvl1_2_3_12doorOpenStepY[zoom];
				}
				else
				{
					doorOpenDeltaY = lvl1_2_3_12doorJammedDeltaY[zoom];
				}
				ypos-=doorOpenDeltaY;
				drawMaskedGfxObject(doorGfx,xpos,ypos);
				
				if (wallMappingHasDoorKnob[frontWallMappingIndex]==-1)
				{
					GfxOverlay knob = inf.doorKnobs[doorGfxOffset];
					drawMaskedGfxObject(knob.gfxObject, knob.deltaX, knob.deltaY);
				}

				break;
			}
			case 10:
			case 11:
			{
				int doorOpenDeltaY=0;
				if (frontWallMappingIndex<30)
				{
					int doorOpenStep = frontWallMappingIndex - wallMappingDoorStartIndex[frontWallMappingIndex];
					doorOpenDeltaY = doorOpenStep * lvl10_11doorOpenStepY[zoom];
				}
				else
				{
					doorOpenDeltaY = lvl10_11doorJammedDeltaY[zoom];
				}
				doorOpenDeltaY=-doorOpenDeltaY;
				
				xpos-=doorGfx.width/2;
				int ypos = lvl10_11doorBottomY[zoom] + doorOpenDeltaY;
				drawMaskedGfxObject(doorGfx,xpos,ypos);
				
				doorOpenDeltaY = doorOpenDeltaY/4 + doorOpenDeltaY/8;
				ypos = lvl10_11doorTopY[zoom] - doorOpenDeltaY;
				drawMaskedGfxObject(inf.doorGfx[doorGfxOffset+3], xpos, ypos);

				if (wallMappingHasDoorKnob[frontWallMappingIndex]==-1)
				{
					GfxOverlay knob = inf.doorKnobs[doorGfxOffset];
					drawMaskedGfxObject(knob.gfxObject, knob.deltaX, knob.deltaY);
				}

				break;
			}
		};
		
		setClip(wallX0,0,wallX1,15);
	}


	void drawMaskedGfxObject(GfxObject gfxObject, int xpos, int ypos)
	{
		for (int y=0;y<gfxObject.height;y++)
			for (int x=0;x<gfxObject.width;x++)
			{
				byte c = gfxObject.data[x+y*gfxObject.width];
				if (c==0)
					continue;
				if ((xpos+x<clipX) || (xpos+x>=clipX+clipW) || (ypos+y<clipY) || (ypos+y>=clipY+clipH))
					continue;
				frameBuffer[xpos+x+(ypos+y)*22*8]=c;
			}
	}
	
	void drawMaskedFlippedGfxObject(GfxObject gfxObject, int xpos, int ypos)
	{
		for (int y=0;y<gfxObject.height;y++)
			for (int x=0;x<gfxObject.width;x++)
			{
				byte c = gfxObject.data[gfxObject.width-1-x+y*gfxObject.width];
				if (c==0)
					continue;
				if ((xpos+x<clipX) || (xpos+x>=clipX+clipW) || (ypos+y<clipY) || (ypos+y>=clipY+clipH))
					continue;
				frameBuffer[xpos+x+(ypos+y)*22*8]=c;
			}
	}

	private void drawWall(int xpos, int ypos, int wallType, int vmpOffset, int width, int height) {
		vmpOffset += (wallType-1)*431 + 330;
		for (int y=0; y<height; y++)
		{
			int wallTileOffset = xpos+(ypos+y)*22;
			for (int x=0; x<width; x++)
			{
				if ((xpos+x>=0) && (xpos+x<22))
				{
					int code = inf.vmp.codes[vmpOffset];
					if (code>0)
					{
						tiles[wallTileOffset] = code;
					}
				}
				
				wallTileOffset++;
				vmpOffset++;
			}
		}
	}

	private void renderTilesToFrameBuffer() {
		int tileOffset=0;
		for (int y=0; y<15; y++)
		{
			for (int x=0; x<22; x++)
			{
				int vmpCode = tiles[tileOffset];
				boolean tileIsBackground = false;
				boolean tileIsFlipped = false;
				int maskedTileValue = 0;
				
				if ((vmpCode&0x8000)==0x8000)
				{
					maskedTileValue=vmpCode-0x8000;
					vmpCode=0;
				}
				
				if ((vmpCode&0x4000)==0x4000)
				{
					tileIsFlipped = true;							
					vmpCode&=0x3fff;
				}
				
				if (vmpCode==0)
				{
					tileIsBackground = true;
					vmpCode = tiles[x+y*22+330];
					if ((vmpCode&0x4000)==0x4000)
					{
						tileIsFlipped = true;
						vmpCode&=0x3fff;
					}
				}
				
				tileOffset++;
				
				byte tile[] = inf.vcn.getTile(vmpCode, !tileIsBackground);
				if (!tileIsFlipped)
				{							
					for (int py=0;py<8;py++)
						for (int px=0;px<8;px++) {
							byte c = tile[px+py*8];
								if (c==0)
									continue;
							frameBuffer[(x*8+px)+(y*8+py)*22*8] = tile[px+py*8];
						}
				}
				else
				{
					for (int py=0;py<8;py++)
						for (int px=0;px<8;px++) {
							byte c = tile[px+py*8];
								if (c==0)
									continue;
							frameBuffer[(x*8+px)+(y*8+py)*22*8] = tile[7-px+py*8];
						}
				}
				
				if (maskedTileValue!=0)
				{
					vmpCode = maskedTileValue;
					
					tileIsFlipped = false;
					if ((vmpCode&0x4000)==0x4000)
					{
						tileIsFlipped = true;
						vmpCode&=0x3fff;
					}
					
					tile = inf.vcn.getTile(vmpCode, true);
					if (!tileIsFlipped)
					{							
						for (int py=0;py<8;py++)
							for (int px=0;px<8;px++)
							{
								byte c = tile[px+py*8];
								if (c==0)
									continue;
								frameBuffer[(x*8+px)+(y*8+py)*22*8] = c;
							}
					}
					else
					{
						for (int py=0;py<8;py++)
							for (int px=0;px<8;px++)
							{
								byte c = tile[7-px+py*8];
								if (c==0)
									continue;
								frameBuffer[(x*8+px)+(y*8+py)*22*8] = c;
							}
					}
				}
			}
		}
	}
	
	private BufferedImage precalcDoors(int lvl, int type) throws IOException {
		byte[] result = new byte[12*6*8 * 22*8];
		for (int i=0; i<result.length; i++)
			result[i]=-1;

		levelIndex = lvl;
    	inf = new Inf(levelIndex);

		int scales[] = new int[]{13,9,3};
		int scale   = 0;	// 0=near, 1=mid, 2=far
		int pos 	= 0; 	// 0..4. 0=closed, 4=opened
		int button	= 1;	// 0=with button, 1=without button
		//int type 	= 0;	// 0=type1, 1=type2 (only on lvl 1..3)
		int jammed 	= 0;	// 0=not jammed, 1=jammed

		int grab[][] = new int[][]{
			{5,1,12,11},
			{7,3,9,6},
			{8,3,6,5}
		};

		int cury=0;
		for (scale=0; scale<3; scale++) {
			int curx=0;
			for (pos=0; pos<6; pos++) {
				jammed = pos==5 ? 1 : 0;

				frameBuffer = new byte[22*8*15*8];
				for (int cy=grab[scale][1]; cy<grab[scale][1]+grab[scale][3]; cy++) {
					for (int cx=grab[scale][0]; cx<grab[scale][0]+grab[scale][2]; cx++) {
						for (int y=0; y<8; y++) {
							for (int x=0; x<8; x++) {
								frameBuffer[cx*8+x+(cy*8+y)*22*8] = (byte)-1;
							}
						}
					}
				}

				tiles = new int[22*15*2];
				/*drawWall(0,15,1,-330,22,15);
				for (int cy=0; cy<15; cy++) {
					for (int cx=0; cx<22; cx++) {
						if ((cx>=grab[scale][0]) && (cx<grab[scale][0]+grab[scale][2]) && (cy==grab[scale][1])) {
							;
						} else
						{
							tiles[cx+cy*22+330] = 0;
						}
					}
				}*/

				if (scale==0) {
					drawWall(3,1,3,0xef,16,12);
				} else if (scale==1) {
					drawWall(6,2,3,0x9f,10,8);
				} else {
					drawWall(8,3,3,0x81,6,5);
				}
				renderTilesToFrameBuffer();

				wallX0 = 0;
				wallX1 = 22;
				
		    	drawDoorOnPosition(scales[scale], (jammed==0) ? 3+pos+10*type+5*button : 30+type);

				for (int cy=grab[scale][1]; cy<grab[scale][1]+grab[scale][3]; cy++) {
					for (int cx=grab[scale][0]; cx<grab[scale][0]+grab[scale][2]; cx++) {
						int dx = cx-grab[scale][0];
						int dy = cy-grab[scale][1];
						for (int y=0; y<8; y++) {
							for (int x=0; x<8; x++) {
								result[dx*8+x+curx + (dy*8+y+cury)*12*6*8] = frameBuffer[cx*8+x+(cy*8+y)*22*8];
							}
						}
					}
				}
				curx += grab[scale][2]*8;
		    }
		    cury += grab[scale][3]*8;
		}

    	CPSImage playfld = new CPSImage(Global.BASE+"playfld.cp_");
        playfld.replacePalette(inf.palette_r, inf.palette_g, inf.palette_b);
        IndexColorModel cm = new IndexColorModel(8,256,playfld.r,playfld.g,playfld.b,255);
		WritableRaster raster = Raster.createPackedRaster(DataBuffer.TYPE_BYTE, 12*6*8, 22*8, 1, 8, null);
		BufferedImage bi = new BufferedImage(12*6*8, 22*8, BufferedImage.TYPE_BYTE_INDEXED, cm);
		raster.setDataElements(0,0,12*6*8, 22*8,result);
		bi.setData(raster);	

		return bi;	
	}
	
	public Doors() throws IOException {
    	Global.BASE = "../../../EotB/c64/data/";
		Global.byteOrder = ByteOrder.BIG_ENDIAN;

		ImageIO.write(precalcDoors(1,0), "PNG", new File("brick0_doors.png"));
		ImageIO.write(precalcDoors(1,1), "PNG", new File("brick1_doors.png"));
		ImageIO.write(precalcDoors(4,0), "PNG", new File("blue_doors.png"));
		ImageIO.write(precalcDoors(7,0), "PNG", new File("drow_doors.png"));
		ImageIO.write(precalcDoors(10,0), "PNG", new File("green_doors.png"));
		ImageIO.write(precalcDoors(12,0), "PNG", new File("xanatha_doors.png"));
	}

    public static void main(String[] args) {
    	try {
    		new Doors();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
