import java.io.*;
import java.nio.*;
import java.awt.image.*;
import javax.imageio.*;
import java.util.*;

public class Decorations {
	private byte frameBuffer[];
	private byte overlayBuffer[];
	private int tiles[];
	private int levelIndex;
	private Inf inf;

	int wallX0, wallX1;
	int clipX=0;
	int clipY=0;
	int clipW=22*8;
	int clipH=15*8;
	void setClip(int x0, int y0, int x1, int y1) {
		clipX=x0*8;
		clipY=y0*8;
		clipW=(x1-x0)*8;
		clipH=(y1-y0)*8;
	}
	
	void setClipFine(int x0, int y0, int x1, int y1) {
		clipX=x0;
		clipY=y0;
		clipW=x1-x0;
		clipH=y1-y0;
	}

	public static int wallMappingToDoorTypeOffset[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 3};
	public static int wallMappingDoorStartIndex[] = {0, 0, 0, 3, 3, 3, 3, 3, 8, 8, 8, 8, 8,13,13,13,13,13,18,18,18,18,18, 0, 0, 0, 0, 0, 0, 0,30,31};
	public static int objectScanPositionZoom[] = {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3};
	public static int objectScanPositionDeltas[][][] = {
		{{-111, -63}, {-95,  -63}, {-139, -59}, {-117, -59}, {-120, -61}},
		{{-76,  -63}, {-60,  -63}, {-95,  -59}, {-74,  -59}, {-80,  -61}},
		{{-43,  -63}, {-27,  -63}, {-53,  -59}, {-31,  -59}, {-40,  -61}},
		{{-8,   -63}, { 8,   -63}, {-10,  -59}, { 10,  -59}, { 0,   -61}},
		{{ 27,  -63}, { 43,  -63}, { 31,  -59}, { 53,  -59}, { 40,  -61}},
		{{ 60,  -63}, { 76,  -63}, { 74,  -59}, { 95,  -59}, { 80,  -61}},
		{{ 95,  -63}, { 111, -63}, { 117, -59}, { 139, -59}, { 120, -61}},
		{{-118, -53}, {-92,  -53}, {-152, -45}, {-120, -45}, {-118, -50}},
		{{-66,  -53}, {-40,  -53}, {-84,  -45}, {-51,  -45}, {-59,  -50}},
		{{-13,  -53}, { 13,  -53}, {-16,  -45}, { 16,  -45}, { 0,   -50}},
		{{ 40,  -53}, { 66,  -53}, { 51,  -45}, { 84,  -45}, { 59,  -50}},
		{{ 92,  -53}, { 118, -53}, { 120, -45}, {152,  -45}, { 118, -50}},
		{{-110, -35}, {-67,  -35}, {-140, -22}, {-83,  -22}, {-98,  -30}},
		{{-22,  -35}, { 22,  -35}, {-27,  -22}, { 27,  -22}, { 0,   -30}},
		{{ 67,  -35}, { 110, -35}, { 83,  -22}, {140,  -22}, { 98,  -30}},
		{{-128, -4 }, { 128, -4 }, {-128, -66}, {128,  -66}, {-128,  0 }},
		{{-38,  -4 }, { 38,  -4 }, {-38,  -66}, { 38,  -66}, { 0,    0 }},
		{{-128, -4 }, { 128, -4 }, {-128, -66}, {128,  -66}, { 128,  0 }}
	};

	int minX,minY,maxX,maxY;
	void drawMaskedGfxObject(GfxObject gfxObject, int xpos, int ypos)
	{
		for (int y=0;y<gfxObject.height;y++)
			for (int x=0;x<gfxObject.width;x++)
			{
				byte c = gfxObject.data[x+y*gfxObject.width];
				if (c==0)
					continue;
				if ((xpos+x<clipX) || (xpos+x>=clipX+clipW) || (ypos+y<clipY) || (ypos+y>=clipY+clipH))
					continue;

				if (xpos+x < minX)
					minX = xpos+x;
				if (xpos+x > maxX)
					maxX = xpos+x;
				if (ypos+y < minY)
					minY = ypos+y;
				if (ypos+y > maxY)
					maxY = ypos+y;

				overlayBuffer[xpos+x+(ypos+y)*22*8]=c;
			}
	}
	
	void drawMaskedFlippedGfxObject(GfxObject gfxObject, int xpos, int ypos)
	{
		for (int y=0;y<gfxObject.height;y++)
			for (int x=0;x<gfxObject.width;x++)
			{
				byte c = gfxObject.data[gfxObject.width-1-x+y*gfxObject.width];
				if (c==0)
					continue;
				if ((xpos+x<clipX) || (xpos+x>=clipX+clipW) || (ypos+y<clipY) || (ypos+y>=clipY+clipH))
					continue;

				if (xpos+x < minX)
					minX = xpos+x;
				if (xpos+x > maxX)
					maxX = xpos+x;
				if (ypos+y < minY)
					minY = ypos+y;
				if (ypos+y > maxY)
					maxY = ypos+y;

				overlayBuffer[xpos+x+(ypos+y)*22*8]=c;
			}
	}

	private void drawWall(int xpos, int ypos, int wallType, int vmpOffset, int width, int height) {
		vmpOffset += (wallType-1)*431 + 330;
		for (int y=0; y<height; y++)
		{
			int wallTileOffset = xpos+(ypos+y)*22;
			for (int x=0; x<width; x++)
			{
				if ((xpos+x>=0) && (xpos+x<22))
				{
					int code = inf.vmp.codes[vmpOffset];
					if (code>0)
					{
						tiles[wallTileOffset] = code;
					}
				}
				
				wallTileOffset++;
				vmpOffset++;
			}
		}
	}

	private void drawFlippedWall(int xpos, int ypos, int wallType, int vmpOffset, int width, int height) {
		vmpOffset += (wallType-1)*431 + 330;
		for (int y=0; y<height; y++)
		{
			int wallTileOffset = xpos+width-1+(ypos+y)*22;
			for (int x=0; x<width; x++)
			{
				if ((xpos+x>=0) && (xpos+x<22))
				{
					int code = inf.vmp.codes[vmpOffset];
					if (code>0)
					{
						tiles[wallTileOffset] = code|0x4000;
					}
				}
				
				wallTileOffset--;
				vmpOffset++;
			}
		}
	}

	private void renderTilesToFrameBuffer() {
		int tileOffset=0;
		for (int y=0; y<15; y++)
		{
			for (int x=0; x<22; x++)
			{
				int vmpCode = tiles[tileOffset];
				boolean tileIsBackground = false;
				boolean tileIsFlipped = false;
				int maskedTileValue = 0;
				
				if ((vmpCode&0x8000)==0x8000)
				{
					maskedTileValue=vmpCode-0x8000;
					vmpCode=0;
				}
				
				if ((vmpCode&0x4000)==0x4000)
				{
					tileIsFlipped = true;							
					vmpCode&=0x3fff;
				}
				
				if (vmpCode==0)
				{
					tileIsBackground = true;
					vmpCode = tiles[x+y*22+330];
					if ((vmpCode&0x4000)==0x4000)
					{
						tileIsFlipped = true;
						vmpCode&=0x3fff;
					}
				}
				
				tileOffset++;
				
				byte tile[] = inf.vcn.getTile(vmpCode, !tileIsBackground);
				if (!tileIsFlipped)
				{							
					for (int py=0;py<8;py++)
						for (int px=0;px<8;px++) {
							byte c = tile[px+py*8];
								if (c==0)
									continue;
							frameBuffer[(x*8+px)+(y*8+py)*22*8] = tile[px+py*8];
						}
				}
				else
				{
					for (int py=0;py<8;py++)
						for (int px=0;px<8;px++) {
							byte c = tile[px+py*8];
								if (c==0)
									continue;
							frameBuffer[(x*8+px)+(y*8+py)*22*8] = tile[7-px+py*8];
						}
				}
				
				if (maskedTileValue!=0)
				{
					vmpCode = maskedTileValue;
					
					tileIsFlipped = false;
					if ((vmpCode&0x4000)==0x4000)
					{
						tileIsFlipped = true;
						vmpCode&=0x3fff;
					}
					
					tile = inf.vcn.getTile(vmpCode, true);
					if (!tileIsFlipped)
					{							
						for (int py=0;py<8;py++)
							for (int px=0;px<8;px++)
							{
								byte c = tile[px+py*8];
								if (c==0)
									continue;
								frameBuffer[(x*8+px)+(y*8+py)*22*8] = c;
							}
					}
					else
					{
						for (int py=0;py<8;py++)
							for (int px=0;px<8;px++)
							{
								byte c = tile[7-px+py*8];
								if (c==0)
									continue;
								frameBuffer[(x*8+px)+(y*8+py)*22*8] = c;
							}
					}
				}
			}
		}
	}

	private ArrayList<PrerenderedDecoration> uniqueDecorations;
	private HashMap<Integer, HashMap<Integer, Integer>> wmiToUniqueDecorationIndexMap = new HashMap<>();
	private ArrayList<HashMap<Integer, WallMapping>> wallMappingList;
	private BufferedImage precalcDecorations(boolean showContext, int[] lvls) throws IOException {
		frameBuffer = new byte[22*8*15*8];
		overlayBuffer = new byte[22*8*15*8];
		tiles = new int[22*15*2];
		uniqueDecorations = new ArrayList<PrerenderedDecoration>();
		wallMappingList = new ArrayList<HashMap<Integer, WallMapping>>();

		for (int li=0; li<lvls.length; li++) {
			HashMap<Integer, Integer> wmiToUniqueDecorationIndex = new HashMap<Integer, Integer>();
			wmiToUniqueDecorationIndexMap.put(lvls[li], wmiToUniqueDecorationIndex);

			levelIndex = lvls[li];
			System.out.printf("Level %d\n", levelIndex);
			inf = new Inf(levelIndex);
			wallMappingList.add(inf.wallMappings);

			for (int wmi : inf.wallMappings.keySet()) {
				WallMapping wm = inf.wallMappings.get(wmi);
				Dat.Decoration decoration = wm.decoration;
				if (decoration == null)
					continue;

				boolean skipPreblending = false;
				if ((levelIndex>=4) && (levelIndex<=6)) {
					if (wmi==0x28||wmi==0x29||wmi==0x39) {
						skipPreblending = true;
					}					
				}
				else if (levelIndex==12) {
					if (wmi==0x16||wmi==0x1a||wmi==0x27) {
						skipPreblending = true;
					}
					if (wmi==0x30)
						continue;
//					if (wmi==0x31)
//						continue;
//					if (wmi==0x2e)
//						continue;
				}

				PrerenderedDecoration prerenderedDecoration = new PrerenderedDecoration();
				prerenderedDecoration.flags = decoration.flags;
				prerenderedDecoration.wallType = wm.wallType;

				for (int rectangleIndex=0; rectangleIndex<10; rectangleIndex++) {
					Dat.Decoration curDecoration = decoration;
					boolean anyThingDrawn = false;
					while(curDecoration != null) {
						Dat.DecorationRectangle decorationRectangle = curDecoration.srcRectangles[rectangleIndex];
						if (decorationRectangle != null) {
							anyThingDrawn = true;
							break;
						}
						curDecoration = curDecoration.next;
					}

					if (!anyThingDrawn)
						continue;

					for (int i=0; i<frameBuffer.length; i++) {
						frameBuffer[i] = -1;
						overlayBuffer[i] = -1;
					}

					if (!skipPreblending) {
						for (int i=0; i<tiles.length; i++)
							tiles[i] = 0;
						drawWall(0,15,1,-330,22,15);
						// 0 Center
						// 1 Front near
						// 2 Front middle
						// 3 Front far
						// 4 Side near
						// 5 Side middle
						// 6 Side far
						// 7 Side very far
						// 8 Side-Side far
						// 9 Side-Side very far

						if (wm.wallType>0) {
							switch (rectangleIndex) {
								case 1:
									drawWall(3,1,wm.wallType,0xef,16,12);
									break;
								case 2:
									drawWall(6,2,wm.wallType,0x9f,10,8);
									break;
								case 3:
									drawWall(8,3,wm.wallType,0x81,6,5);
									break;
								case 4:
									drawWall(0,0,wm.wallType,0,3,15);
									break;
								case 5:
									drawWall(3,1,wm.wallType,0x2d,3,12);
									break;
								case 6:
									drawWall(6,2,wm.wallType,0x51,2,8);
									break;
								case 7:
									drawWall(8,3,wm.wallType,0x61,1,5);
									break;
								case 8:
									drawWall(-2,3,wm.wallType,0x66,3,5);
									break;
								case 9:
									drawWall(2,3,wm.wallType,0x66,3,5);
									break;
							}
						}

						clipX = 0;
						clipW = 22*8;
						if ((levelIndex == 1) && (wmi==0x31) && (rectangleIndex==1)) {
							clipX = 0*8;
							clipW = 3*8;
							drawWall(2,3,2,0x66,3,5);
							drawWall(3,1,1,0xef,16,12);
						} else if ((levelIndex == 1) && (wmi==0x45) && (rectangleIndex==1)) {
							clipX = 0*8;
							clipW = 19*8;
							drawWall(0,0,1,0,3,15);
							drawWall(3,1,2,0x2d,3,12);
							drawWall(6,2,1,0x51,2,8);
							drawWall(8,3,2,0x61,1,5);
							drawFlippedWall(16,1,1,0x2d,3,12);
							drawFlippedWall(14,2,2,0x51,2,8);
							drawFlippedWall(12,3,1,0x61,1,5);
							drawWall(19,1,2,0xef,16,12);
						} 

	/*
						if (!showContext) {
							for (int y=0; y<15; y++) {
								for (int x=0; x<22; x++) {
									if ((x<clipX/8) || (x>=clipX/8+(clipW/8)))
										tiles[x+y*22+22*15] = 0;
								}
							}
						}*/

						renderTilesToFrameBuffer();
					}

					minX = 1000;
					minY = 1000;
					maxX = -1000;
					maxY = -1000;
					curDecoration = decoration;
					while(curDecoration != null) {
						Dat.DecorationRectangle decorationRectangle = curDecoration.srcRectangles[rectangleIndex];
						if (decorationRectangle != null) {
							int xpos = curDecoration.destX[rectangleIndex];
							if ((levelIndex == 1) && (wmi==0x31)) {
								xpos-=16*8;
							}
							int ypos = curDecoration.destY[rectangleIndex];
							if ((curDecoration.flags&1)==0)
								drawMaskedGfxObject(decorationRectangle.gfxObject, xpos, ypos);
							else {
								xpos=176-xpos;
								xpos-=decorationRectangle.w;
								drawMaskedFlippedGfxObject(decorationRectangle.gfxObject, xpos, ypos);
							}
						}
						curDecoration = curDecoration.next;
					}

					if ((levelIndex == 1) && (wmi==0x31))
						prerenderedDecoration.dx[rectangleIndex] = 16;

					if ((maxX>minX) && (maxY>minY)) {
						maxX +=7;
						maxY +=7;
						minX /= 8;
						maxX /= 8;
						minY /= 8;
						maxY /= 8;
						prerenderedDecoration.cx[rectangleIndex] = minX;
						prerenderedDecoration.cy[rectangleIndex] = minY;
						prerenderedDecoration.cw[rectangleIndex] = maxX-minX;
						prerenderedDecoration.ch[rectangleIndex] = maxY-minY;

						if (!showContext) {
							for (int cy=0; cy<15; cy++) {
								for (int cx=0; cx<22; cx++) {
									if ((cx<minX) || (cy<minY) || (cx>=maxX) || (cy>=maxY))
										continue;

									boolean overlayVisibleInChar = false;
									out:
									for (int py=0; py<8; py++) {
										for (int px=0; px<8; px++) {
											if (overlayBuffer[cx*8+px + (cy*8+py)*22*8] != -1) {
												overlayVisibleInChar = true;
												break out;
											}
										}
									}

									if (!overlayVisibleInChar) 
										continue;

									for (int py=0; py<8; py++)
										for (int px=0; px<8; px++)
											prerenderedDecoration.pixels[rectangleIndex*22*8 + cx*8 + px + (cy*8+py)*22*8*10] = frameBuffer[cx*8+px + (cy*8+py)*22*8];

									for (int py=0; py<8; py++)
										for (int px=0; px<8; px++) {
											byte p = overlayBuffer[cx*8+px + (cy*8+py)*22*8];
											if (p == -1)
												continue;
											prerenderedDecoration.pixels[rectangleIndex*22*8 + cx*8 + px + (cy*8+py)*22*8*10] = p;
										}
								}
							}
						} else {
							for (int y=0; y<15*8; y++) {
								for (int x=0; x<22*8; x++) {
									byte bp = frameBuffer[x + y*22*8];
									byte pp = overlayBuffer[x + y*22*8];
									byte p = pp!=-1 ? pp : bp;
									prerenderedDecoration.pixels[rectangleIndex*22*8 + x + y*22*8*10] = p;
								}
							}
						}
					}
				}

				int index = uniqueDecorations.indexOf(prerenderedDecoration);
				if (index==-1) {
					index = uniqueDecorations.size();
					uniqueDecorations.add(prerenderedDecoration);
				}
				wmiToUniqueDecorationIndex.put(wmi, index);
				System.out.printf("   0x%02x => %d\n", wmi, index);
			}
		}

		if (!showContext)
			System.out.printf("   %d decorations found.\n", uniqueDecorations.size());

		for (int i=0; i<uniqueDecorations.size(); i++) {
			System.out.printf("   %02d: %dx%d\n", i, uniqueDecorations.get(i).cw[1], uniqueDecorations.get(i).ch[1]);
		}

		int WIDTH = 22*8*10;
		int HEIGHT = 15*8*uniqueDecorations.size();
		byte[] result = new byte[WIDTH*HEIGHT];
		for (int i=0; i<uniqueDecorations.size(); i++)
			System.arraycopy(uniqueDecorations.get(i).pixels, 0, result, i*22*8*10*15*8, 22*8*10*15*8);

    	CPSImage playfld = new CPSImage(Global.BASE+"playfld.cp_");
    	playfld.replacePalette(inf.palette_r, inf.palette_g, inf.palette_b);
        IndexColorModel cm = new IndexColorModel(8, 256 ,playfld.r, playfld.g, playfld.b, 255);
		WritableRaster raster = Raster.createPackedRaster(DataBuffer.TYPE_BYTE, WIDTH, HEIGHT, 1, 8, null);
		BufferedImage bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_INDEXED, cm);
		raster.setDataElements(0,0,WIDTH,HEIGHT,result);
		bi.setData(raster);	

		return bi;	
	}

	private void dumpWmiMapping(int lvlStart, int lvlStop, PrintStream out) throws IOException {
		int li=0;
		for (HashMap<Integer,WallMapping> wallMappings : wallMappingList) {
			int lvl = lvlStart+li;
			if (lvl>lvlStop)
				break;
			Optional<Integer> maxWmi = wallMappings.keySet().stream().reduce(Integer::max);

			int wallDecoration[] = new int[maxWmi.get()+1];
			for (int i=0; i<wallDecoration.length; i++)
				wallDecoration[i]=0xff;

			HashMap<Integer, Integer> decorationMap = wmiToUniqueDecorationIndexMap.get(lvl);
			for (int wmi : decorationMap.keySet()) {
				int decorationIndex = decorationMap.get(wmi);
				wallDecoration[wmi] = decorationIndex;
			}

			out.printf(".export lvl%dWallDecoration\n", lvl);
			out.printf("lvl%dWallDecoration:\n\t.byte ", lvl);
			for (int i=0; i<wallDecoration.length; i++) {
				if (i>0)
					out.print(",");
				out.printf("$%02x", wallDecoration[i]);
			}
			out.println();

			li++;
		}
	}

	private void exportDecorations(File outputFile) throws IOException {
		outputFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(outputFile);
		ObjectOutputStream dos = new ObjectOutputStream(fos);
		dos.writeObject(uniqueDecorations);
		dos.close();
		fos.close();
	}
	
	public Decorations() throws IOException {
	    Global.BASE = "../../pcdata/";
		Global.byteOrder = ByteOrder.LITTLE_ENDIAN;

		PrintStream wmiMappingOut = new PrintStream(new File("wmiToDecoration.s"));

		wmiMappingOut.println(".segment \"INF0\"");
		
		ImageIO.write(precalcDecorations(false, new int[]{1}), "PNG", new File("brick1_decorations.png"));
		exportDecorations(new File("brick1_decorations.preconv"));
		dumpWmiMapping(1,1, wmiMappingOut);
		
		ImageIO.write(precalcDecorations(false, new int[]{2}), "PNG", new File("brick2_decorations.png"));
		exportDecorations(new File("brick2_decorations.preconv"));
		dumpWmiMapping(2,2, wmiMappingOut);
		
		ImageIO.write(precalcDecorations(false, new int[]{3}), "PNG", new File("brick3_decorations.png"));
		exportDecorations(new File("brick3_decorations.preconv"));
		dumpWmiMapping(3,3, wmiMappingOut);

		wmiMappingOut.println(".segment \"INF1\"");

		ImageIO.write(precalcDecorations(false, new int[]{4,5,6}), "PNG", new File("blue_decorations.png"));
		exportDecorations(new File("blue_decorations.preconv"));
		dumpWmiMapping(4,5, wmiMappingOut);
		
		wmiMappingOut.println(".segment \"INF2\"");
		dumpWmiMapping(6,6, wmiMappingOut);

		ImageIO.write(precalcDecorations(false, new int[]{7,8,9}), "PNG", new File("drow_decorations.png"));
		exportDecorations(new File("drow_decorations.preconv"));

		dumpWmiMapping(7,8, wmiMappingOut);
		
		wmiMappingOut.println(".segment \"INF3\"");

		dumpWmiMapping(9,9, wmiMappingOut);

		ImageIO.write(precalcDecorations(false, new int[]{10,11}), "PNG", new File("green_decorations.png"));
		exportDecorations(new File("green_decorations.preconv"));
		dumpWmiMapping(10,11, wmiMappingOut);

		ImageIO.write(precalcDecorations(false, new int[]{12}),    "PNG", new File("xanatha_decorations.png"));
		exportDecorations(new File("xanatha_decorations.preconv"));

		dumpWmiMapping(12,12, wmiMappingOut);

/*
		ImageIO.write(precalcDecorations(true, new int[]{1}), "PNG", new File("brick1_decorations_context.png"));
		ImageIO.write(precalcDecorations(true, new int[]{2}), "PNG", new File("brick2_decorations_context.png"));
		ImageIO.write(precalcDecorations(true, new int[]{3}), "PNG", new File("brick3_decorations_context.png"));
		ImageIO.write(precalcDecorations(true, new int[]{4,5,6}), "PNG", new File("blue_decorations_context.png"));
		ImageIO.write(precalcDecorations(true, new int[]{7,8,9}), "PNG", new File("drow_decorations_context.png"));
		ImageIO.write(precalcDecorations(true, new int[]{10,11}), "PNG", new File("green_decorations_context.png"));
		ImageIO.write(precalcDecorations(true, new int[]{12}),    "PNG", new File("xanatha_decorations_context.png"));*/
	}

    public static void main(String[] args) {
    	try {
    		new Decorations();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
