import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

enum CharType {
        Empty,
        Bitmap,
}

public class Convert {
	public Convert(String wallsetName) throws Exception {
		System.out.printf("Converting %s...\n", wallsetName);
		BufferedImage bitmap = ImageIO.read(new File("converted/"+wallsetName+"_decorations64.png"));
		Declash.declash(bitmap);
		byte bitmapPixels[] = ((DataBufferByte)bitmap.getRaster().getDataBuffer()).getData();

		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(wallsetName+"_decorations.preconv"));
		
		@SuppressWarnings("unchecked")
		ArrayList<PrerenderedDecoration> uniqueDecorations = (ArrayList<PrerenderedDecoration>)ois.readObject();

		System.out.printf("Got %d unique decorations\n", uniqueDecorations.size());

		if (bitmap.getHeight() != 15*8*uniqueDecorations.size()) {
			throw new Exception(String.format("Inconsistent height in png image. Got %d, expected %d.",
				bitmap.getHeight(), 15*8*uniqueDecorations.size()));
		}

		int WIDTH = 22*8*10 / 2;
		if (bitmap.getWidth() != WIDTH) {
			throw new Exception(String.format("Inconsistent width in png image. Got %d, expected %d.",
				bitmap.getWidth(), WIDTH));			
		}

		int bankOffset = 0x8009;

		ArrayList<Byte> bank = new ArrayList<Byte>();
		ArrayList<Short> offsets = new ArrayList<Short>();
		ArrayList<Byte> screenData = new ArrayList<Byte>();
		ArrayList<Byte> d800Data = new ArrayList<Byte>();

		int objOffsets[][] = new int[uniqueDecorations.size()][10];
		int objXpos[][] = new int[uniqueDecorations.size()][10];
		int objYpos[][] = new int[uniqueDecorations.size()][10];
		int objWidths[][] = new int[uniqueDecorations.size()][10];
		int objHeights[][] = new int[uniqueDecorations.size()][10];

		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		ArrayList<EOBObject> alternativeDecorations = new ArrayList<>();

		for (int decorationIndex=0; decorationIndex<uniqueDecorations.size(); decorationIndex++) {
			System.out.printf("   %d\n", decorationIndex);
			PrerenderedDecoration decoration = uniqueDecorations.get(decorationIndex);
			int baseY = decorationIndex*15*8;

			for (int position=0; position<10; position++) {
				int baseX = position*22*8/2;
				int dx = decoration.cx[position];
				int dy = decoration.cy[position];
				int cw = decoration.cw[position];
				int ch = decoration.ch[position];

				objXpos[decorationIndex][position] = dx+decoration.dx[position];
				objYpos[decorationIndex][position] = dy;
				objWidths[decorationIndex][position] = cw;
				objHeights[decorationIndex][position] = ch;

				if ((cw==0) || (ch==0)) {
					objOffsets[decorationIndex][position] = 0;
					continue;
				}

				if (bitmapPixels[baseY*WIDTH]==0) {
					// Alternative 'monster'-mode
					decoration.flags |= 0x80;
					objOffsets[decorationIndex][position] = alternativeDecorations.size();
					try {
						alternativeDecorations.add(new EOBObject(bitmap, baseX/4+dx,baseY/8+dy, cw,ch));
						continue;
					} catch (Exception e) {
						System.out.println("I Suck! Switching to koala.");
						decoration.flags &= ~0x80; 
					}
				}

				objOffsets[decorationIndex][position] = offsets.size();

				for (int cx=dx; cx<dx+cw; cx++) {
					for (int cy=dy; cy<dy+ch; cy++) {
						CharType type = CharType.Empty;
						byte colors[] = new byte[]{0,-1,-1,-1};

						// Assign color usage
						abortBitmap:for (int py=0; py<8; py++) {
							for (int px=0; px<4; px++) {
								byte c = bitmapPixels[baseX+cx*4+px + (baseY+cy*8+py)*WIDTH];
								if (c>=16) {
									break abortBitmap;
								}
								int ci=0;
								for (ci=0; ci<4; ci++) {
									if (colors[ci]==c)
										break;
									if (colors[ci]==-1) {
										colors[ci]=c;
										break;
									}
								}
								if (ci==4) {
									throw new Exception(String.format("Not koala restrictions at char %d,%d on decoration %d at position %d", cx,cy, decorationIndex, position));
								}
							}
							type = CharType.Bitmap;
						}

						byte d800 = 0;
						byte screen = 0;
						byte data[] = new byte[8];

						if (type == CharType.Bitmap) {
							Arrays.sort(colors);
							// Generate data array for the bitmap
							for (int py=0; py<8; py++) {
								byte cb = 0;
								for (int px=0; px<4; px++) {
									byte c = bitmapPixels[baseX+cx*4+px + (baseY+cy*8+py)*WIDTH];
									int ci = 0;
									for (ci=0; ci<4; ci++) {
										if (colors[ci]==c)
											break;
									}
									if (ci==4) {
										// This will happen if a decoration was tagged as alternative rendering but do not adhere to sprite restrictions.
										// The code will switch to koala mode instead but those parts have not been declashed.
										//throw new Exception(String.format("Sanity check. I fucked up in the code! %d,%d on decoration %d at position %d", cx, cy, decorationIndex, position));
										ci=0; // TODO, remove this later, but force to black for now
									}
									cb |= (byte)(ci<<((3-px)*2));
								}
 								data[py] = cb;
							}
							screen = (byte)(((colors[1]&15)<<4) | (colors[2]&15));
							d800 = (byte)(colors[3]&15);
						}

						screenData.add(screen);
						d800Data.add(d800);

						short offset=0;
						switch(type) {
							case Empty:
								break;
							case Bitmap: {
								boolean match = false;
								n:for (int i=0; i<bank.size()-8; i++) {
									for (int j=0; j<8; j++) {
										if (bank.get(i+j) != data[j])
											continue n;
									}
									match = true;
									offset = (short)(i+bankOffset);
								}
								if (!match) {
									offset = (short)(bank.size()+bankOffset);
									for (int j=0; j<8; j++)
										bank.add(data[j]);									
								}
								break;
							}
						}

						offsets.add(offset);
					}
				}
			}
		}

		EOBBank alternativeBank = new EOBBank("alternative", alternativeDecorations);

		System.out.printf("Bank size is $%04x\n", bank.size());
		System.out.printf("Screen size is $%04x\n", screenData.size());
		System.out.printf("D800 size is $%04x\n", d800Data.size());
		System.out.printf("Alt Bank size is $%04x\n", alternativeBank.size());

		// Emit binary data
		String[] arrayNames = new String[]{
			"decoratonOffsetsLo",
			"decoratonOffsetsHi",
			"decoratonOffsetsScreen",
			"decoratonOffsetsD800"
		};
		String[] referenceNames = new String[]{
			"offsets_lo",
			"offsets_hi",
			"screen",
			"d800",
			"alt_data",
			"alt_screen",
			"alt_d800"
		};

		FileOutputStream fos;
		fos = new FileOutputStream(wallsetName+"_data.bin");
		for (Byte b : bank) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[0]+".bin");
		for (Short s : offsets) {
			int i = (int)(s&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[1]+".bin");
		for (Short s : offsets) {
			int i = (int)((s>>8)&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[2]+".bin");
		for (Byte b : screenData) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[3]+".bin");
		for (Byte b : d800Data) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[4]+".bin");
		for (Byte b : alternativeBank.bank) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[5]+".bin");
		for (Byte b : alternativeBank.screenData) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(wallsetName+"_"+referenceNames[6]+".bin");
		for (Byte b : alternativeBank.d800Data) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		// Emit source
		PrintStream ps = new PrintStream(new File(String.format("dec_%s.s", wallsetName)));

		ps.printf(".segment \"DEC_%s\"\n", wallsetName.toUpperCase());
		ps.printf(".export dec_%s\n", wallsetName);
		ps.printf("dec_%s:\n\n", wallsetName);
		ps.println("jmp renderDecoration");
		ps.println("jmp renderFlippedDecoration");
		ps.println("jmp getDecorationFlags");
		ps.println();

		ps.printf(".incbin \"%s_data.bin\"\n\n", wallsetName);
		for (int i=0; i<referenceNames.length; i++) {
			ps.printf("%s:\n", referenceNames[i]);
			ps.printf("\t.incbin \"%s_%s.bin\"\n", wallsetName, referenceNames[i]);
			ps.println();
		}

		ps.print("decorationFlags: .byte ");
		for (int i=0; i<uniqueDecorations.size(); i++) {
			if (i>0)
				ps.print(",");
			ps.printf("$%02x", uniqueDecorations.get(i).flags);
		}
		ps.println();

		for (int p=0; p<10; p++) {
			ps.printf("position%d: .byte ", p);
			for (int i=0; i<uniqueDecorations.size(); i++) {
				if (i>0)
					ps.printf(", ");
				ps.printf("$%02x,$%02x,$%02x,$%02x,<$%04x,>$%04x",
					objWidths[i][p],
					objHeights[i][p],
					objXpos[i][p],
					objYpos[i][p],
					objOffsets[i][p],
					objOffsets[i][p]);
			}
			ps.println();
		}
		ps.println();

		ps.println("_char_offset_lo:");
		if (alternativeBank.objCharOffsets.size()>0)
			ps.printf("\t.byte ");
		for (int i=0; i<alternativeBank.objCharOffsets.size(); i++) {
			if (i>0)
				ps.printf(",");
			ps.printf("<$%04x", alternativeBank.objCharOffsets.get(i));
		}
		ps.println();

		ps.println("_char_offset_hi:");
		if (alternativeBank.objCharOffsets.size()>0)
			ps.printf("\t.byte ");
		for (int i=0; i<alternativeBank.objBankOffsets.size(); i++) {
			if (i>0)
				ps.printf(",");
			ps.printf(">$%04x", alternativeBank.objCharOffsets.get(i));
		}
		ps.println();

		ps.println("_data_ptr_lo:");
		if (alternativeBank.objCharOffsets.size()>0)
			ps.printf("\t.byte ");
		for (int i=0; i<alternativeBank.objBankOffsets.size(); i++) {
			if (i>0)
				ps.printf(",");
			ps.printf("<(alt_data+$%04x)", alternativeBank.objBankOffsets.get(i));
		}
		ps.println();

		ps.println("_data_ptr_hi:");
		if (alternativeBank.objCharOffsets.size()>0)
			ps.printf("\t.byte ");
		for (int i=0; i<alternativeBank.objBankOffsets.size(); i++) {
			if (i>0)
				ps.printf(",");
			ps.printf(">(alt_data+$%04x)", alternativeBank.objBankOffsets.get(i));
		}
		ps.println();

		ps.println();

		ps.printf(".include \"../../dec_renderer.s\"\n");
	}

	public static void main(String[] args) {
		try {
			new Convert(args[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
