import java.io.*;
import java.awt.image.*;
import javax.imageio.*;

public class Extract {
	private int shapes[][] = new int[][] {
		{0x00, 0x00, 0x06, 0x40},
		{0x06, 0x00, 0x07, 0x48},
        {0x0D, 0x00, 0x06, 0x30},
        {0x13, 0x00, 0x05, 0x38},
        {0x18, 0x00, 0x05, 0x38},
        {0x1D, 0x00, 0x05, 0x38},
        {0x22, 0x00, 0x06, 0x48},
        {0x00, 0x48, 0x06, 0x58},
        {0x06, 0x48, 0x05, 0x40},
        {0x0B, 0x48, 0x05, 0x40},
        {0x10, 0x48, 0x05, 0x30},
        {0x06, 0x88, 0x03, 0x18},
        {0x09, 0x88, 0x03, 0x18},
        {0x0C, 0x88, 0x03, 0x18},
        {0x0F, 0x88, 0x03, 0x18},
        {0x12, 0x88, 0x03, 0x18},
        {0x15, 0x88, 0x03, 0x18},
        {0x18, 0x48, 0x06, 0x58},
        {0x1E, 0x48, 0x0A, 0x48}
	};

	private static final int FB_WIDTH=22*8;
	private static final int FB_HEIGHT=15*8;
	private BufferedImage outtake;
	private byte[] frameBuffer;

	private void clearFrameBuffer() {
		frameBuffer = new byte[FB_WIDTH * FB_HEIGHT];
	}

	private void clear(int x, int y, int w, int h) {
		for (int py=y; py<y+h; py++)
			for (int px=x; px<x+w; px++)
				frameBuffer[px+py*FB_WIDTH] = 0;
	}

	private void clearExcept(int x, int y, int w, int h) {
		for (int py=0; py<FB_HEIGHT; py++)
			for (int px=0; px<FB_WIDTH; px++)
				if ((px>=x) && (py>=y) && (px<x+w) && (py<y+h))
					continue;
				else
					frameBuffer[px+py*FB_WIDTH] = 0;
	}

	private void dumpFrameBuffer(String name) throws IOException {
		BufferedImage output = new BufferedImage(FB_WIDTH, FB_HEIGHT, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)outtake.getColorModel());
		byte[] outputPixels = ((DataBufferByte)output.getRaster().getDataBuffer()).getData();
		System.arraycopy(frameBuffer, 0, outputPixels, 0, frameBuffer.length);
		ImageIO.write(output, "PNG", new File(name));
	}

	private void drawShape(int index, int dx, int dy) {
		byte[] src = ((DataBufferByte)outtake.getRaster().getDataBuffer()).getData();
		int shape[] = shapes[index];
		int sx = shape[0]*8;
		int sy = shape[1];
		int sw = shape[2]*8;
		int sh = shape[3];
		dx-=sw/2;
		dy-=sh;
		for (int y=0; y<sh; y++) {
			for (int x=0; x<sw; x++) {
				byte c = src[sx+x + (sy+y)*320];
				if (c==0)
					continue;
				frameBuffer[dx+x + (dy+y)*FB_WIDTH] = c;
			}
		}
	}

	private void dumpObject(String name) throws IOException {
		int minX = FB_WIDTH;
		int maxX = 0;
		int minY = FB_HEIGHT;
		int maxY = 0;
		
		// Find bounding box
		minYfound:for (minY=0; minY<FB_HEIGHT; minY++) {
			for (int x=0; x<FB_WIDTH; x++) {
				if (frameBuffer[x+minY*FB_WIDTH] != 0)
					break minYfound;
			}
		}

		maxYfound:for (maxY=FB_HEIGHT-1; maxY>=0; maxY--) {
			for (int x=0; x<FB_WIDTH; x++) {
				if (frameBuffer[x+maxY*FB_WIDTH] != 0)
					break maxYfound;
			}
		}

		minXfound: for (minX=0; minX<FB_WIDTH; minX++) {
			for (int y=0; y<FB_HEIGHT; y++) {
				if (frameBuffer[minX+y*FB_WIDTH] != 0)
					break minXfound;
			}
		}

		maxXfound: for (maxX=FB_WIDTH-1; maxX>=0; maxX--) {
			for (int y=0; y<FB_HEIGHT; y++) {
				if (frameBuffer[maxX+y*FB_WIDTH] != 0)
					break maxXfound;
			}
		}

		// Round to chars
		minX&=0xf8;
		minY&=0xf8;
		maxX = (maxX+7)&0xf8;
		maxY = (maxY+7)&0xf8;

		// Save object
		int objWidth = maxX-minX;
		int objHeight = maxY-minY;
		BufferedImage output = new BufferedImage(objWidth, objHeight, BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)outtake.getColorModel());
		byte[] outputPixels = ((DataBufferByte)output.getRaster().getDataBuffer()).getData();
		for (int x=minX; x<maxX; x++) {
			for (int y=minY; y<maxY; y++) {
				byte c = frameBuffer[x+y*FB_WIDTH];
				outputPixels[x-minX+(y-minY)*objWidth] = c;
			}
		}
		ImageIO.write(output, "PNG", new File(name));		
	}

	public Extract() throws IOException {
		outtake = ImageIO.read(new File("originals/outtake.gif"));

		clearFrameBuffer();
		drawShape(2,88,104);
		dumpObject("outtake_2.png");

		clearFrameBuffer();
		drawShape(0,88,104);
		dumpObject("outtake_0.png");

		clearFrameBuffer();
		drawShape(5,88,120);
		dumpObject("outtake_5.png");

		clearFrameBuffer();
		drawShape(3,88,104);
		dumpObject("outtake_3.png");

		clearFrameBuffer();
		drawShape(8,116,104);
		clear(13*8,3*8, 3*8,4*8);
		dumpObject("outtake_8.png");

		clearFrameBuffer();
		drawShape(9,116,104);
		clear(13*8,3*8, 3*8,4*8);
		dumpObject("outtake_9.png");

		clearFrameBuffer();
		drawShape(10,116,104);
		clear(13*8,6*8, 3*8,3*8);
		dumpObject("outtake_10.png");

		clearFrameBuffer();
		drawShape(8,116,104);
		drawShape(11,115,0x31);
		clearExcept(13*8,3*8, 3*8,4*8);
		dumpObject("outtake_11.png");

		clearFrameBuffer();
		drawShape(8,116,104);
		drawShape(12,115,0x31);
		clearExcept(13*8,3*8, 3*8,4*8);
		dumpObject("outtake_12.png");

		clearFrameBuffer();
		drawShape(9,116,104);
		drawShape(13,115,0x34);
		clearExcept(13*8,3*8, 3*8,4*8);
		dumpObject("outtake_13.png");

		clearFrameBuffer();
		drawShape(9,116,104);
		drawShape(14,116,0x37);
		clearExcept(13*8,3*8, 3*8,4*8);
		dumpObject("outtake_14.png");

		clearFrameBuffer();
		drawShape(9,116,104);
		drawShape(15,118,0x37);
		clearExcept(13*8,3*8, 3*8,4*8);
		dumpObject("outtake_15.png");

		clearFrameBuffer();
		drawShape(10,116,104);
		drawShape(16,115,0x45);
		clearExcept(13*8,6*8, 3*8,3*8);
		dumpObject("outtake_16.png");

		clearFrameBuffer();
		drawShape(7,88,106);
		dumpObject("outtake_7.png");

		clearFrameBuffer();
		drawShape(6,88,104);
		dumpObject("outtake_6.png");

		clearFrameBuffer();
		drawShape(18,88,88);
		dumpObject("outtake_18.png");

		clearFrameBuffer();
		drawShape(17,88,104);
		dumpObject("outtake_17.png");

		clearFrameBuffer();
		drawShape(4,88,104);
		dumpObject("outtake_4.png");

		clearFrameBuffer();
		drawShape(1,88,104);
		dumpObject("outtake_1.png");
	}

	public static void main(String[] args) {
		try {
			new Extract();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}