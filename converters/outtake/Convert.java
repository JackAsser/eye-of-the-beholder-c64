import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Convert {
	public static void main(String[] args) {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		try {			
			ArrayList<EOBObject> objects = new ArrayList<EOBObject>();

			BufferedImage bitmap;

			bitmap = ImageIO.read(new File("converted/dwarfs_all.png"));
			objects.add(new EOBObject(bitmap, 0, 1, 6, 8)); //0
			objects.add(new EOBObject(bitmap,12, 0, 6, 6)); //2
			objects.add(new EOBObject(bitmap,18, 0, 5, 7)); //3
			objects.add(new EOBObject(bitmap,23, 0, 5, 7)); //4
			objects.add(new EOBObject(bitmap,28, 0, 5, 8)); //5

			for (int i=6; i<=7; i++) {
				bitmap = ImageIO.read(new File(String.format("converted/outtake_%d_64.png", i)));
				Declash.declash(bitmap, C64Char.spriteColors);
				objects.add(new EOBObject(bitmap, 0, 0, bitmap.getWidth()/4, bitmap.getHeight()/8));
			}

			bitmap = ImageIO.read(new File("converted/resurrected.png"));
			objects.add(new EOBObject(bitmap, 1, 2, 5, 8)); //8
			objects.add(new EOBObject(bitmap, 9, 2, 5, 8)); //9
			objects.add(new EOBObject(bitmap,16, 2, 5, 8)); //10

			objects.add(new EOBObject(bitmap,23, 1, 3, 4)); //11
			objects.add(new EOBObject(bitmap,27, 1, 3, 4)); //12
			objects.add(new EOBObject(bitmap,23, 6, 3, 4)); //13
			objects.add(new EOBObject(bitmap,27, 7, 3, 3)); //14
			objects.add(new EOBObject(bitmap,31, 7, 3, 3)); //15
			objects.add(new EOBObject(bitmap,31, 2, 3, 3)); //16

			bitmap = ImageIO.read(new File(String.format("converted/outtake_%d_64.png", 17)));
			Declash.declash(bitmap, C64Char.spriteColors);
			objects.add(new EOBObject(bitmap, 0, 0, bitmap.getWidth()/4, bitmap.getHeight()/8));

			EOBBank outtake = new EOBBank("graphics", objects);
			outtake.dump("outtake_");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}