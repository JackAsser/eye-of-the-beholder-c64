.segment "WALLSET_BLUE_BACKGROUND"
.export wallset_blue_background
wallset_blue_background:
.word _screen
.word _d800
.word _data
.word _flippedData
_flippedData:
	.incbin "wallset_blue_background_flippedData.bin"
_data:
	.incbin "wallset_blue_background_data.bin"
_screen:
	.incbin "wallset_blue_background_screen.bin"
_d800:
	.incbin "wallset_blue_background_d800.bin"
