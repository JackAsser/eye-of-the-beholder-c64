.segment "WALLSET_DROW_WALLS2"
.export wallset_drow_walls2
wallset_drow_walls2:
jmp wallset_draw_normal
jmp wallset_draw_flipped

_width:
	.byte $03,$03,$02,$01,$03,$02,$06,$0a,$10,$03,$03,$02,$01,$03,$02,$06,$0a,$10,$03,$03,$02,$01,$03,$02,$06,$0a,$10
_height:
	.byte $0f,$0c,$08,$05,$05,$06,$05,$08,$0c,$0f,$0c,$08,$05,$05,$06,$05,$08,$0c,$0f,$0c,$08,$05,$05,$06,$05,$08,$0c

_char_offset_lo:
	.byte <$0000,<$002d,<$0051,<$0061,<$0066,<$0075,<$0081,<$009f,<$00ef,<$01af,<$01dc,<$0200,<$0210,<$0215,<$0224,<$0230,<$024e,<$029e,<$035e,<$038b,<$03af,<$03bf,<$03c4,<$03d3,<$03df,<$03fd,<$044d
_char_offset_hi:
	.byte >$0000,>$002d,>$0051,>$0061,>$0066,>$0075,>$0081,>$009f,>$00ef,>$01af,>$01dc,>$0200,>$0210,>$0215,>$0224,>$0230,>$024e,>$029e,>$035e,>$038b,>$03af,>$03bf,>$03c4,>$03d3,>$03df,>$03fd,>$044d
_data_ptr_lo:
	.byte <(_data+$0000),<(_data+$0163),<(_data+$0266),<(_data+$02d8),<(_data+$0301),<(_data+$037c),<(_data+$03de),<(_data+$04d4),<(_data+$075e),<(_data+$0d6e),<(_data+$0eb9),<(_data+$0fbc),<(_data+$102e),<(_data+$1057),<(_data+$10d2),<(_data+$1134),<(_data+$120a),<(_data+$1454),<(_data+$1a04),<(_data+$1b6f),<(_data+$1c72),<(_data+$1ce4),<(_data+$1d0d),<(_data+$1d88),<(_data+$1dea),<(_data+$1ee0),<(_data+$216a)
_data_ptr_hi:
	.byte >(_data+$0000),>(_data+$0163),>(_data+$0266),>(_data+$02d8),>(_data+$0301),>(_data+$037c),>(_data+$03de),>(_data+$04d4),>(_data+$075e),>(_data+$0d6e),>(_data+$0eb9),>(_data+$0fbc),>(_data+$102e),>(_data+$1057),>(_data+$10d2),>(_data+$1134),>(_data+$120a),>(_data+$1454),>(_data+$1a04),>(_data+$1b6f),>(_data+$1c72),>(_data+$1ce4),>(_data+$1d0d),>(_data+$1d88),>(_data+$1dea),>(_data+$1ee0),>(_data+$216a)
_data:
	.incbin "wallset_drow_walls2_data.bin"
_screen:
	.incbin "wallset_drow_walls2_screen.bin"
_d800:
	.incbin "wallset_drow_walls2_d800.bin"

.include "../../wallset_renderer.s"
