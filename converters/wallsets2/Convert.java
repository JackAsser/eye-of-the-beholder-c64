import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Convert {
	public static EOBBank[] convert(String name) throws Exception {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		System.out.printf("Converting wallset %s\n", name);

		BufferedImage bitmap = ImageIO.read(new File(String.format("converted/%s64.png", name)));
		Declash.declash(bitmap, C64Char.spriteColors);

		// Make completly black background chars to be empty instead
        byte pixels[] = ((DataBufferByte)bitmap.getRaster().getDataBuffer()).getData();
		for (int cy=0; cy<15; cy++) {
			for (int cx=0; cx<22; cx++) {
				int blackCount=0;
				for (int py=0; py<8; py++) {
                    for (int px=0; px<4; px++) {
                    	int c = pixels[cx*4+px + (cy*8+py)*bitmap.getWidth()] & 0xff;
                        if ((c == 0) || (c >= 16))
                        	blackCount++;
                    }
                }

                if (blackCount==32) {
					for (int py=0; py<8; py++) {
                        for (int px=0; px<4; px++) {
                            pixels[cx*4+px + (cy*8+py)*bitmap.getWidth()] = 16;
                        }
                    }
				}
			}
		}

		int sizes[][] = {
			{3 ,15},
			{3 ,12},
			{2 ,8 },
			{1 ,5 },
			{3 ,5 },
			{2 ,6 },
			{6 ,5 },
			{10,8 },
			{16,12},
		};

		ArrayList<EOBObject> eobObjectsBG = new ArrayList<EOBObject>();
		ArrayList<EOBObject> eobObjects1 = new ArrayList<EOBObject>();
		ArrayList<EOBObject> eobObjects2 = new ArrayList<EOBObject>();

		eobObjectsBG.add(new EOBObject(bitmap, 0,0, 22,15));

		int nbrTypes = bitmap.getHeight()/(15*8)-1;
		for (int type=0; type<nbrTypes; type++) {
			int cy = 15+15*type;
			int cx = 0;
			for (int s=0; s<sizes.length; s++) {
				int cw = sizes[s][0];
				int ch = sizes[s][1];

				if (type<3)
					eobObjects1.add(new EOBObject(bitmap, cx,cy, cw,ch));
				else
					eobObjects2.add(new EOBObject(bitmap, cx,cy, cw,ch));
				cx += cw;
			}
		}

		ImageIO.write(bitmap, "PNG", new File(String.format("%s-auto.png",name)));

		return new EOBBank[]{
			new EOBBank(name+"_background", eobObjectsBG),
			new EOBBank(name+"_walls1", eobObjects1),
			new EOBBank(name+"_walls2", eobObjects2)};
	}

	public static void main(String[] args) {
		String[] names = {
			"brick",
			"blue",
			"drow",
			"green",
			"xanatha"
		};

		try {
			ArrayList<EOBBank> banks = new ArrayList<EOBBank>();
			for (String name : names) {
				EOBBank wallbanks[] = Convert.convert(name);
				banks.add(wallbanks[0]);
				banks.add(wallbanks[1]);
				banks.add(wallbanks[2]);
			}
			Collections.sort(banks, (b1,b2) -> b1.size() - b2.size());
			for (EOBBank bank : banks) {
				System.out.printf("Size for %s will be $%04x\n", bank.name, bank.size());
				bank.dump("wallset_", bank.name.indexOf("_background")==-1 ? EOBBank.DumpMode.Normal : EOBBank.DumpMode.Background);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}