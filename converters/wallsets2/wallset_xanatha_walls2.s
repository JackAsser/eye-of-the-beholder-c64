.segment "WALLSET_XANATHA_WALLS2"
.export wallset_xanatha_walls2
wallset_xanatha_walls2:
jmp wallset_draw_normal
jmp wallset_draw_flipped

_width:
	.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$03,$03,$02,$01,$03,$02,$06,$0a,$10
_height:
	.byte $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$0f,$0c,$08,$05,$05,$06,$05,$08,$0c

_char_offset_lo:
	.byte <$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$0000,<$002d,<$0051,<$0061,<$0066,<$0075,<$0081,<$009f,<$00ef
_char_offset_hi:
	.byte >$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$0000,>$002d,>$0051,>$0061,>$0066,>$0075,>$0081,>$009f,>$00ef
_data_ptr_lo:
	.byte <(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$0000),<(_data+$016b),<(_data+$026e),<(_data+$02e0),<(_data+$0309),<(_data+$0384),<(_data+$03e6),<(_data+$04dc),<(_data+$0766)
_data_ptr_hi:
	.byte >(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$0000),>(_data+$016b),>(_data+$026e),>(_data+$02e0),>(_data+$0309),>(_data+$0384),>(_data+$03e6),>(_data+$04dc),>(_data+$0766)
_data:
	.incbin "wallset_xanatha_walls2_data.bin"
_screen:
	.incbin "wallset_xanatha_walls2_screen.bin"
_d800:
	.incbin "wallset_xanatha_walls2_d800.bin"

.include "../../wallset_renderer.s"
