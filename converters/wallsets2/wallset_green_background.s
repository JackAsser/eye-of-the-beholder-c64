.segment "WALLSET_GREEN_BACKGROUND"
.export wallset_green_background
wallset_green_background:
.word _screen
.word _d800
.word _data
.word _flippedData
_flippedData:
	.incbin "wallset_green_background_flippedData.bin"
_data:
	.incbin "wallset_green_background_data.bin"
_screen:
	.incbin "wallset_green_background_screen.bin"
_d800:
	.incbin "wallset_green_background_d800.bin"
