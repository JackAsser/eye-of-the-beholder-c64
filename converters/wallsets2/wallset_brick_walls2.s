.segment "WALLSET_BRICK_WALLS2"
.export wallset_brick_walls2
wallset_brick_walls2:
jmp wallset_draw_normal
jmp wallset_draw_flipped

_width:
	.byte $03,$03,$02,$01,$03,$02,$06,$0a,$10,$03,$03,$02,$01,$03,$02,$06,$0a,$10,$00,$00,$00,$00,$00,$00,$00,$00,$00
_height:
	.byte $0f,$0c,$08,$05,$05,$06,$05,$08,$0c,$0f,$0c,$08,$05,$05,$06,$05,$08,$0c,$00,$00,$00,$00,$00,$00,$00,$00,$00

_char_offset_lo:
	.byte <$0000,<$002d,<$0051,<$0061,<$0066,<$0075,<$0081,<$009f,<$00ef,<$01af,<$01dc,<$0200,<$0210,<$0215,<$0224,<$0230,<$024e,<$029e,<$035e,<$035e,<$035e,<$035e,<$035e,<$035e,<$035e,<$035e,<$035e
_char_offset_hi:
	.byte >$0000,>$002d,>$0051,>$0061,>$0066,>$0075,>$0081,>$009f,>$00ef,>$01af,>$01dc,>$0200,>$0210,>$0215,>$0224,>$0230,>$024e,>$029e,>$035e,>$035e,>$035e,>$035e,>$035e,>$035e,>$035e,>$035e,>$035e
_data_ptr_lo:
	.byte <(_data+$0000),<(_data+$0163),<(_data+$0266),<(_data+$02d8),<(_data+$0301),<(_data+$037c),<(_data+$03de),<(_data+$04d4),<(_data+$074e),<(_data+$0d3e),<(_data+$0ea1),<(_data+$0fa4),<(_data+$1016),<(_data+$103f),<(_data+$10ba),<(_data+$111c),<(_data+$1212),<(_data+$149c),<(_data+$1aac),<(_data+$1aac),<(_data+$1aac),<(_data+$1aac),<(_data+$1aac),<(_data+$1aac),<(_data+$1aac),<(_data+$1aac),<(_data+$1aac)
_data_ptr_hi:
	.byte >(_data+$0000),>(_data+$0163),>(_data+$0266),>(_data+$02d8),>(_data+$0301),>(_data+$037c),>(_data+$03de),>(_data+$04d4),>(_data+$074e),>(_data+$0d3e),>(_data+$0ea1),>(_data+$0fa4),>(_data+$1016),>(_data+$103f),>(_data+$10ba),>(_data+$111c),>(_data+$1212),>(_data+$149c),>(_data+$1aac),>(_data+$1aac),>(_data+$1aac),>(_data+$1aac),>(_data+$1aac),>(_data+$1aac),>(_data+$1aac),>(_data+$1aac),>(_data+$1aac)
_data:
	.incbin "wallset_brick_walls2_data.bin"
_screen:
	.incbin "wallset_brick_walls2_screen.bin"
_d800:
	.incbin "wallset_brick_walls2_d800.bin"

.include "../../wallset_renderer.s"
