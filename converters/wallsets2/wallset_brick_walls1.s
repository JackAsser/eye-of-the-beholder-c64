.segment "WALLSET_BRICK_WALLS1"
.export wallset_brick_walls1
wallset_brick_walls1:
jmp wallset_draw_normal
jmp wallset_draw_flipped

_width:
	.byte $03,$03,$02,$01,$03,$02,$06,$0a,$10,$03,$03,$02,$01,$03,$02,$06,$0a,$10,$00,$00,$00,$00,$03,$00,$06,$0a,$10
_height:
	.byte $0f,$0c,$08,$05,$05,$06,$05,$08,$0c,$0f,$0c,$08,$05,$05,$06,$05,$08,$0c,$00,$00,$00,$00,$05,$00,$05,$08,$0c

_char_offset_lo:
	.byte <$0000,<$002d,<$0051,<$0061,<$0066,<$0075,<$0081,<$009f,<$00ef,<$01af,<$01dc,<$0200,<$0210,<$0215,<$0224,<$0230,<$024e,<$029e,<$035e,<$035e,<$035e,<$035e,<$035e,<$036d,<$036d,<$038b,<$03db
_char_offset_hi:
	.byte >$0000,>$002d,>$0051,>$0061,>$0066,>$0075,>$0081,>$009f,>$00ef,>$01af,>$01dc,>$0200,>$0210,>$0215,>$0224,>$0230,>$024e,>$029e,>$035e,>$035e,>$035e,>$035e,>$035e,>$036d,>$036d,>$038b,>$03db
_data_ptr_lo:
	.byte <(_data+$0000),<(_data+$0163),<(_data+$0266),<(_data+$02d8),<(_data+$0301),<(_data+$037c),<(_data+$03de),<(_data+$04d4),<(_data+$075e),<(_data+$0d6e),<(_data+$0ed1),<(_data+$0fd4),<(_data+$1046),<(_data+$106f),<(_data+$10ea),<(_data+$114c),<(_data+$1242),<(_data+$14cc),<(_data+$1adc),<(_data+$1adc),<(_data+$1adc),<(_data+$1adc),<(_data+$1adc),<(_data+$1b57),<(_data+$1b57),<(_data+$1bcd),<(_data+$1d07)
_data_ptr_hi:
	.byte >(_data+$0000),>(_data+$0163),>(_data+$0266),>(_data+$02d8),>(_data+$0301),>(_data+$037c),>(_data+$03de),>(_data+$04d4),>(_data+$075e),>(_data+$0d6e),>(_data+$0ed1),>(_data+$0fd4),>(_data+$1046),>(_data+$106f),>(_data+$10ea),>(_data+$114c),>(_data+$1242),>(_data+$14cc),>(_data+$1adc),>(_data+$1adc),>(_data+$1adc),>(_data+$1adc),>(_data+$1adc),>(_data+$1b57),>(_data+$1b57),>(_data+$1bcd),>(_data+$1d07)
_data:
	.incbin "wallset_brick_walls1_data.bin"
_screen:
	.incbin "wallset_brick_walls1_screen.bin"
_d800:
	.incbin "wallset_brick_walls1_d800.bin"

.include "../../wallset_renderer.s"
