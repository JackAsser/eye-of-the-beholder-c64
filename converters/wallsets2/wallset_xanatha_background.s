.segment "WALLSET_XANATHA_BACKGROUND"
.export wallset_xanatha_background
wallset_xanatha_background:
.word _screen
.word _d800
.word _data
.word _flippedData
_flippedData:
	.incbin "wallset_xanatha_background_flippedData.bin"
_data:
	.incbin "wallset_xanatha_background_data.bin"
_screen:
	.incbin "wallset_xanatha_background_screen.bin"
_d800:
	.incbin "wallset_xanatha_background_d800.bin"
