.segment "WALLSET_BRICK_BACKGROUND"
.export wallset_brick_background
wallset_brick_background:
.word _screen
.word _d800
.word _data
.word _flippedData
_flippedData:
	.incbin "wallset_brick_background_flippedData.bin"
_data:
	.incbin "wallset_brick_background_data.bin"
_screen:
	.incbin "wallset_brick_background_screen.bin"
_d800:
	.incbin "wallset_brick_background_d800.bin"
