.segment "WALLSET_DROW_BACKGROUND"
.export wallset_drow_background
wallset_drow_background:
.word _screen
.word _d800
.word _data
.word _flippedData
_flippedData:
	.incbin "wallset_drow_background_flippedData.bin"
_data:
	.incbin "wallset_drow_background_data.bin"
_screen:
	.incbin "wallset_drow_background_screen.bin"
_d800:
	.incbin "wallset_drow_background_d800.bin"
