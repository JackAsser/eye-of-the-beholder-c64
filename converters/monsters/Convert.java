import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Convert {
	public static EOBBank convert(String name) throws Exception {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		BufferedImage bitmap = ImageIO.read(new File(String.format("converted/%s.png", name)));
		Declash.declash(bitmap, C64Char.spriteColors);

		ArrayList<EOBObject> eobObjects = new ArrayList<EOBObject>();

		int iPixels[] = bitmap.getSampleModel().getPixels(0,0,bitmap.getWidth(),bitmap.getHeight(),(int[])null, bitmap.getRaster().getDataBuffer());

		int sectionWidth = 12;
		int sectionHeight = 12;
		for (int zoom=0; zoom<3; zoom++) {
			int baseCy = zoom*sectionHeight;
			for (int i=0; i<((zoom==0) ? 6 : 4); i++) {
				int baseCx = i*sectionWidth;

				// Find bounding box
				int top=0;
				topFound:for (top=0; top<sectionHeight; top++)
					for (int cx=0; cx<sectionWidth; cx++)
						if (C64Char.charType(iPixels, bitmap.getWidth(), baseCx+cx, baseCy+top) != C64Char.CharType.Empty)
							break topFound;
				int left=0;
				leftFound:for (left=0; left<sectionWidth; left++)
					for (int cy=top; cy<sectionHeight; cy++)
						if (C64Char.charType(iPixels, bitmap.getWidth(), baseCx+left, baseCy+cy) != C64Char.CharType.Empty)
							break leftFound;
				int right=0;
				rightFound:for (right=sectionWidth-1; right>=0; right--)
					for (int cy=top; cy<sectionHeight; cy++)
						if (C64Char.charType(iPixels, bitmap.getWidth(), baseCx+right, baseCy+cy) != C64Char.CharType.Empty)
							break rightFound;

				eobObjects.add(new EOBObject(bitmap, baseCx+left, baseCy+top, right-left+1, sectionHeight-top));
			}
		}

		ImageIO.write(bitmap, "PNG", new File(String.format("%s-auto.png",name)));

		return new EOBBank(name, eobObjects);
	}

	public static void main(String[] args) {
		String[] names = {
			"disbeast",
			"drider",
			"drowelf",
			"dwarf",
			"flind",
			"golem",
			"hellhnd",
			"kenku",
			"kobold",
			"kuotoa",
			"leech",
			"mage",
			"mantis",
			"mflayer",
			"rust",
			"shindia",
			"skeleton",
			"skelwar",
			"spider",
			"xanath",
			"xorn",
			"zombie"
		};

		try {
			ArrayList<EOBBank> banks = new ArrayList<EOBBank>();
			for (String name : names) {
				System.out.printf("Converting %s\n", name);
				banks.add(Convert.convert(name));
			}
			Collections.sort(banks, (b1,b2) -> b1.size() - b2.size());
			for (EOBBank bank : banks) {
				System.out.printf("Size for %s will be $%04x\n", bank.name, bank.size());
				bank.dump("mon_");
			}

			for (int i=0; i<banks.size(); i+=2) {
				int j = banks.size()-1-i;
				EOBBank a = banks.get(i);
				EOBBank b = banks.get(j);
				System.out.printf("$%04x+$%04x = $%04x (%s+%s)\n", a.size(), b.size(), a.size()+b.size(), a.name, b.name);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}