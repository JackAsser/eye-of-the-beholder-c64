import java.io.*;
import java.nio.*;
import java.awt.image.*;
import javax.imageio.*;
import java.util.*;

public class Monsters {	
	private class ExtractedMonsterGraphics {
		public Inf inf;
		public GfxObject graphics[][];

		public ExtractedMonsterGraphics(Inf inf, int index) {
			this.inf = inf;
			graphics = new GfxObject[3][6];
			for (int zoom=0; zoom<3; zoom++) {
				for (int i=0; i<6; i++) {
					GfxObject original = inf.monsterGfx[i+index*18];
					graphics[zoom][i] = original;
					for (int scale=0; scale<zoom; scale++)
						graphics[zoom][i] = graphics[zoom][i].scaleDown();
				}
			}
		}

		public BufferedImage render() throws IOException {
			int WIDTH=0;
			int HEIGHT=0;

			int sectionWidth = 12*8;
			int sectionHeight = 12*8;
			WIDTH = sectionWidth*6;
			HEIGHT = sectionHeight*3;

			byte[] result = new byte[WIDTH*HEIGHT];
			for (int i=0; i<result.length; i++)
				result[i]=-1;

			for (int zoom=0; zoom<3; zoom++) {
				int baseY = zoom*sectionHeight;
				for (int i=0; i<6; i++) {
					int baseX = i*sectionWidth;
					GfxObject g = graphics[zoom][i];
					if ((g.width>sectionWidth) || (g.height>sectionHeight)) {
						System.err.printf("%dx%d needed", (g.width+7)/8, (g.height+7)/8);
						System.exit(-1);
					}

					int cx = (sectionWidth-g.width)/2;
					int cy = sectionHeight-g.height;

					for (int y=0; y<g.height; y++) {
						for (int x=0; x<g.width; x++) {
							byte b = g.data[x+y*g.width];
							if (b==0)
								continue;
							result[baseX+cx+x+(baseY+cy+y)*WIDTH] = b;
						}
					}
				}
			}

		   	CPSImage playfld = new CPSImage(Global.BASE+"playfld.cp_");
	        playfld.replacePalette(inf.palette_r, inf.palette_g, inf.palette_b);
	        IndexColorModel cm = new IndexColorModel(8,256,playfld.r,playfld.g,playfld.b,255);
			WritableRaster raster = Raster.createPackedRaster(DataBuffer.TYPE_BYTE, WIDTH, HEIGHT, 1, 8, null);
			BufferedImage bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_INDEXED, cm);
			raster.setDataElements(0,0, WIDTH,HEIGHT, result);
			bi.setData(raster);
			return bi;
		}
	}

	public Monsters() throws IOException {
    	Global.BASE = "../../pcdata/";
		Global.byteOrder = ByteOrder.LITTLE_ENDIAN;

		HashMap<String, ExtractedMonsterGraphics> monsters = new HashMap<String, ExtractedMonsterGraphics>();

		for (int levelIndex=1; levelIndex<=12; levelIndex++) {
	    	Inf inf = new Inf(levelIndex);
	    	String name;

	    	for (int i=0; i<2; i++) {
		    	name = inf.monsterGfxNames[i];
		    	if (name!=null) {
		    		if (!monsters.containsKey(name)) {
			    		monsters.put(name, new ExtractedMonsterGraphics(inf, i));
			    	}
		    	}
		    }
	    }

	    for (String name : monsters.keySet()) {
    		System.out.println(name);
	    	ExtractedMonsterGraphics emg = monsters.get(name);
	    	ImageIO.write(emg.render(), "PNG", new File("extract/"+name+"PC.png"));
	    }
	}

    public static void main(String[] args) {
    	try {
    		new Monsters();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
