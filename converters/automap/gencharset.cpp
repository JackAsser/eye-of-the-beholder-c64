#include <stdio.h>
#include <stdint.h>
#include <map>

using namespace std;

int main(void) {
	map<uint64_t, unsigned char> uniqueChars;
	unsigned char mapping[256];

	for (int i=0; i<256; i++) {
		uint64_t ch = 0;

		if (i&0x01) // N
			ch |= 0x000000000000ffff;
		if (i&0x02) // NE
			ch |= 0x000000000000c0c0;
		if (i&0x04) // E
			ch |= 0xc0c0c0c0c0c0c0c0;
		if (i&0x08) // SE
			ch |= 0xc0c0000000000000;
		if (i&0x10) // S
			ch |= 0xffff000000000000;
		if (i&0x20) // SW
			ch |= 0x0303000000000000;
		if (i&0x40) // W
			ch |= 0x0303030303030303;
		if (i&0x80) // NW
			ch |= 0x0000000000000303;

		auto it = uniqueChars.find(ch);
		if (it != uniqueChars.end())
			mapping[i] = it->second;
		else {
			uniqueChars[ch] = i;
			mapping[i] = uniqueChars.size();
		}
	}

	printf("%ld\n", uniqueChars.size());
	for (auto ch : uniqueChars) {
		printf("%02x: %016llx\n", ch.second, ch.first);
	}

	return 0;
}