import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Combiner {
	private static BufferedImage mcPixels(BufferedImage input) {
		BufferedImage output = new BufferedImage(input.getWidth()/2, input.getHeight(), BufferedImage.TYPE_BYTE_INDEXED, (IndexColorModel)input.getColorModel());
		byte inputPixels[] = ((DataBufferByte)input.getRaster().getDataBuffer()).getData();
		byte outputPixels[] = ((DataBufferByte)output.getRaster().getDataBuffer()).getData();
		for (int i=0; i<outputPixels.length; i++)
			outputPixels[i] = inputPixels[i*2];
		return output;
	}

	public static void combiner(String maskName, String mcName, String outName) throws Exception {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		BufferedImage maskBitmap = ImageIO.read(new File(maskName));
		byte maskPixels[] = ((DataBufferByte)maskBitmap.getRaster().getDataBuffer()).getData();
		int iMaskPixels[] = maskBitmap.getSampleModel().getPixels(0,0,maskBitmap.getWidth(),maskBitmap.getHeight(),(int[])null, maskBitmap.getRaster().getDataBuffer());
		BufferedImage mcBitmap = ImageIO.read(new File(mcName));
		byte mcPixels[] = ((DataBufferByte)mcBitmap.getRaster().getDataBuffer()).getData();

		for (int cy=0; cy<maskBitmap.getHeight()/8; cy++) {
			for (int cx=0; cx<maskBitmap.getWidth()/4; cx++) {
				C64Char.CharType maskChar = C64Char.charType(iMaskPixels, maskBitmap.getWidth(), cx, cy);
				if (maskChar == C64Char.CharType.Sprite) {
					for (int py=0; py<8; py++) {
						for (int px=0; px<4; px++) {
							int offset = cx*4+px+(cy*8+py)*maskBitmap.getWidth();
							mcPixels[offset] = maskPixels[offset];
						}
					}
				}
			}
		}
		
		Declash.declash(mcBitmap, C64Char.spriteColors);
		ImageIO.write(mcBitmap, "PNG", new File(outName));
	}

	public static void main(String[] args) {
		try {
			Combiner.combiner(args[0], args[1], args[2]);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
