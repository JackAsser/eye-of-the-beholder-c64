import java.io.*;
import java.nio.*;
import java.awt.image.*;
import javax.imageio.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;

public class ConeOfColdDOS {
	private byte frameBuffer[];

	public static int getRandomValue(int minVal, int maxVal)
	{
		Random r = new Random();
		return minVal + r.nextInt(maxVal-minVal);
	}

	public static int pseudoRandom()
	{
		Random r = new Random();
		return r.nextInt()&0xff;
	}

	int xpos_rom[] = new int[150];
	int ypos_rom[] = new int[150];
	int vx_rom[] = new int[150];
	int vy_rom[] = new int[150];
	int fadeSpeed_rom[] = new int[150];
	int delayUntilStart_rom[] = new int[150];

	void prepRomTables() {
		int numberOfParticles = 150;
		for (int si=0; si<numberOfParticles; si++)
		{
			int var38 = getRandomValue(50*256/4, 50*256);
			int di=0;
			int var3A = 0;
			do
			{
				var3A+=40;
				di+=var3A;
			}
			while (di<var38);
			
			int quadrant = pseudoRandom()&3;
			switch(quadrant)
			{
				case 0:
				{
					xpos_rom[si]=32*4;
					ypos_rom[si]=di;
					vx_rom[si]=var3A;
					vy_rom[si]=0;
					break;
				}
				case 1:
				{
					xpos_rom[si]=di;
					ypos_rom[si]=32*4;
					vx_rom[si]=0;
					vy_rom[si]=var3A;
					break;
				}
				case 2:
				{
					xpos_rom[si]=32*4;
					ypos_rom[si]=-di;
					vx_rom[si]=var3A;
					vy_rom[si]=0;
					break;
				}
				case 3:
				{
					xpos_rom[si]=-di;
					ypos_rom[si]=32*4;
					vx_rom[si]=0;
					vy_rom[si]=var3A;
					break;
				}
			}
			
			if ((pseudoRandom()&1)!=0)
			{
				vx_rom[si]*=-1;
				vy_rom[si]*=-1;
			}
			
			fadeSpeed_rom[si] = getRandomValue(0x400/100, 0x800/100);
			delayUntilStart_rom[si] = getRandomValue(0, 100/4);
		}

		System.out.printf("coc_fadeSpeed_init: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf("$%02x", fadeSpeed_rom[i]);
		}
		System.out.println();

		System.out.printf("coc_delayUntilStart_init: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf("$%02x", delayUntilStart_rom[i]);
		}
		System.out.println();

		System.out.printf("coc_vx_init_lo: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf("<$%04x", vx_rom[i]&0xffff);
		}
		System.out.println();
		System.out.printf("coc_vx_init_hi: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf(">$%04x", vx_rom[i]&0xffff);
		}
		System.out.println();

		System.out.printf("coc_vy_init_lo: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf("<$%04x", vy_rom[i]&0xffff);
		}
		System.out.println();
		System.out.printf("coc_vy_init_hi: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf(">$%04x", vy_rom[i]&0xffff);
		}
		System.out.println();

		System.out.printf("coc_xpos_init_lo: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf("<$%04x", xpos_rom[i]&0xffff);
		}
		System.out.println();
		System.out.printf("coc_xpos_init_hi: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf(">$%04x", xpos_rom[i]&0xffff);
		}
		System.out.println();

		System.out.printf("coc_ypos_init_lo: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf("<$%04x", ypos_rom[i]&0xffff);
		}
		System.out.println();
		System.out.printf("coc_ypos_init_hi: .byte ");
		for (int i=0; i<numberOfParticles; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf(">$%04x", ypos_rom[i]&0xffff);
		}
		System.out.println();
	}
	
	public void renderConeOfCold()
	{
		int nbrParticles = 150;
		int colorTable[] = new int[]{15,9,15,9,2,10,2,0};

		int xpos[] = new int[150];
		int ypos[] = new int[150];
		int vx[] = new int[150];
		int vy[] = new int[150];
		int fadeSpeed[] = new int[150];
		int darkness[] = new int[150];
		int delayUntilStart[] = new int[150];
	
		for (int si=0; si<nbrParticles; si++) {
			xpos[si] = xpos_rom[si];
			ypos[si] = ypos_rom[si];
			vx[si] = vx_rom[si];
			vy[si] = vy_rom[si];
			fadeSpeed[si] = fadeSpeed_rom[si];
			darkness[si] = 0;
			delayUntilStart[si] = delayUntilStart_rom[si];
		}
		
		int frameCount = 0;
		boolean didPlot = true;
		while(didPlot)
		{
			frameCount++;
			for (int i=0; i<frameBuffer.length; i++)
				frameBuffer[i] = 0;
			
			didPlot = false;
			for (int si=0; si<nbrParticles; si++)
			{
				if (delayUntilStart[si]!=0) {
					didPlot = true;
					delayUntilStart[si]--;
					continue;
				}

				if (xpos[si]<=0)
					if (vx[si]>=0)
						vx[si]+=35;
					else
						vx[si]+=40;
				else
					if (vx[si]<=0)
						vx[si]-=35;
					else
						vx[si]-=40;
				
				if (ypos[si]<=0)
					if (vy[si]>=0)
						vy[si]+=35;
					else
						vy[si]+=40;
				else
					if (vy[si]<=0)
						vy[si]-=35;
					else
						vy[si]-=40;

				xpos[si]+=vx[si];
				ypos[si]+=vy[si];
				darkness[si]+=fadeSpeed[si];

				int xp = xpos[si]/256+88;
				int yp = ypos[si]/256+53;
				byte color = (byte)colorTable[darkness[si]/256];
				if (color==0)
					fadeSpeed[si]=0;
				else {
					didPlot = true;
					frameBuffer[xp+yp*22*8] = color;
				}
			}
			
			panel.repaint();

			try
			{
				Thread.sleep(1000/60);
			}
			catch (InterruptedException e)
			{
				;
			}
		}
		System.out.println(frameCount);
	}

	
	JPanel panel;
	public ConeOfColdDOS() throws IOException {
    	Global.BASE = "../../../java/pcdata/";
		Global.byteOrder = ByteOrder.LITTLE_ENDIAN;

    	CPSImage playfld = new CPSImage(Global.BASE+"playfld.cp_");
        IndexColorModel cm = new IndexColorModel(8,256,playfld.r,playfld.g,playfld.b,255);
		WritableRaster raster = Raster.createPackedRaster(DataBuffer.TYPE_BYTE, 22*8, 15*8, 1, 8, null);
		BufferedImage bi = new BufferedImage(22*8, 15*8, BufferedImage.TYPE_BYTE_INDEXED, cm);
		frameBuffer = ((DataBufferByte)bi.getRaster().getDataBuffer()).getData();

		prepRomTables();

		JFrame f = new JFrame("Explosion");
		f.setLayout(new BorderLayout());
		panel = new JPanel() {
			public void paint(Graphics g) {
				g.drawImage(bi, 0, 0, 22*8*4, 15*8*4, null);
			}

			public Dimension getPreferredSize() {
				return new Dimension(22*8*4, 15*8*4);
			}
		};
		f.add(panel, BorderLayout.CENTER);
		f.pack();
		f.setVisible(true);
		f.toFront();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		new Thread(() -> {
			while(true) {
				renderConeOfCold();
			}
		}).start();
	}

    public static void main(String[] args) {
    	try {
    		new ConeOfColdDOS();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
