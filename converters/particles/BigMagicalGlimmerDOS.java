import java.io.*;
import java.nio.*;
import java.awt.image.*;
import javax.imageio.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;

public class BigMagicalGlimmerDOS {
	private byte frameBuffer[];

	void drawMaskedGfxObject(GfxObject gfxObject, int xpos, int ypos) {
		for (int y=0;y<gfxObject.height;y++)
			for (int x=0;x<gfxObject.width;x++)
			{
				byte c = gfxObject.data[x+y*gfxObject.width];
				if (c==0)
					continue;
				frameBuffer[xpos+x+(ypos+y)*22*8]=c;
			}
	}

	void clear() {
		for (int i=0; i<frameBuffer.length; i++)
			frameBuffer[i] = 0;
	}
	
	void drawMagicalGlimmer(int di) {
		int gfxIndices[][] = new int[][] {
			{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
			{2,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0},
			{3,2,2,2,1,1,1,1,0,0,0,0,0,0,0,0},
			{2,3,3,3,2,2,2,2,1,1,1,0,0,0,0,0},
			{1,2,2,2,3,3,3,3,2,2,2,1,1,1,1,0},
			{0,1,1,1,2,2,2,2,3,3,3,2,2,2,2,1},
			{0,0,0,0,1,1,1,1,2,2,2,3,3,3,3,2},
			{0,0,0,0,0,0,0,0,1,1,1,2,2,2,2,3},
			{0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,2},
			{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}
		};

		int xpos[] = {10, 14,  6, 13,  4, 12,  7, 15, 16,  9, 11,  5, 12,  8, 14,  9};
		int ypos[] = {49, 43, 72, 23, 22, 72, 53, 27, 67, 46, 36, 40, 56, 28, 22, 68};

		for (int si=0; si<16; si++) {
			int gfxIndex = gfxIndices[di][si];
			if (gfxIndex !=0 )
				drawMaskedGfxObject(GfxResources.flareGfx[gfxIndex-1], xpos[si]*8, ypos[si]);
		}
	}

	void dumpFlares(IndexColorModel cm) throws IOException {
		for (int i=0; i<GfxResources.flareGfx.length; i++) {
			GfxObject g = GfxResources.flareGfx[i];
			BufferedImage bi = new BufferedImage(g.width, g.height, BufferedImage.TYPE_BYTE_INDEXED, cm);
			byte pixels[] = ((DataBufferByte)bi.getRaster().getDataBuffer()).getData();
			System.arraycopy(g.data, 0, pixels, 0, g.data.length);
			ImageIO.write(bi, "png", new File(String.format("flare%d.png", i)));
		}		
	}

	void genSpeedCode() throws Exception {
		int xpos[] = {10, 14,  6, 13,  4, 12,  7, 15, 16,  9, 11,  5, 12,  8, 14,  9};
		int ypos[] = {49, 43, 72, 23, 22, 72, 53, 27, 67, 46, 36, 40, 56, 28, 22, 68};

		for (int si=0; si<16; si++) {
			System.out.printf ("ldy #$%02x\n", si);
			System.out.println("lda (SRC),y");
			System.out.println("beq :+");
			System.out.println("\ttax");
			System.out.println("\tlda jumptab_lo,x");
			System.out.println("\tsta INDJMP+0");
			System.out.println("\tlda jumptab_hi,x");
			System.out.println("\tsta INDJMP+1");
			System.out.printf ("\tlda #<(frameBuffer_sprites+%d*15*8)\n", xpos[si]);
			System.out.println("\tsta DST+0");
			System.out.printf ("\tlda #>(frameBuffer_sprites+%d*15*8)\n", xpos[si]);
			System.out.println("\tsta DST+1");
			System.out.printf ("\tldy #%d\n", ypos[si]+4);
			System.out.println("\tjsr INDJMPOP");
			System.out.println(":");
		}

		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);
		C64Char.forcedType = C64Char.CharType.Sprite;
		BufferedImage bitmap = ImageIO.read(new File("converted/bigflares.png"));
		for (int i=0; i<3; i++) {
			System.out.printf("drawFlare%d:\n", i);
			EOBObject o = new EOBObject(bitmap, i*2, 0, 2, 2);
			boolean ychanged = false;
			for (int cx=0; cx<2; cx++) {
				if (ychanged) {
					System.out.println("\tldy YREG");
					ychanged = false;
				}
				for (int y=4; y<16; y++) {
					C64Char ch = o.c64Chars[cx*2+y/8];
					int data = ch.data.get(y&7)&0xff;
					if (data != 0) {
						int mask = ((data&0x55) | ((data&0x55)<<1) | (data&0xaa) | ((data&0xaa)>>1)) ^ 0xff;
						System.out.println("\tlda (DST),y");
						System.out.printf("\tand #$%02x\n", mask);
						System.out.printf("\tora #$%02x\n", data);
						System.out.println("\tsta (DST),y");
					}

					boolean stop=true;
					for (int y2=y+1; y2<16; y2++) {
						ch = o.c64Chars[cx*2+y2/8];
						data = ch.data.get(y2&7)&0xff;
						if (data!=0) {
							stop=false;
							break;
						}
					}

					if (stop)
						break;

					if ((!ychanged) && (cx==0)) {
						System.out.println("\tsty YREG");
						ychanged = true;
					}

					System.out.println("\tiny");
				}

				if (cx==0) {
					System.out.println("\tlda DST+0");
					System.out.println("\tadc #15*8");
					System.out.println("\tsta DST+0");
					System.out.println("\tbcc :+");
					System.out.println("\t\tinc DST+1");
					System.out.println("\t:");
				}
			}
			System.out.println("\trts");
			System.out.println();
		}
	}
	
	public BigMagicalGlimmerDOS() throws Exception {
		genSpeedCode();
		System.exit(0);

    	Global.BASE = "../../../java/pcdata/";
		Global.byteOrder = ByteOrder.LITTLE_ENDIAN;

    	CPSImage playfld = new CPSImage(Global.BASE+"playfld.cp_");
        IndexColorModel cm = new IndexColorModel(8,256,playfld.r,playfld.g,playfld.b,255);
		WritableRaster raster = Raster.createPackedRaster(DataBuffer.TYPE_BYTE, 22*8, 15*8, 1, 8, null);
		BufferedImage bi = new BufferedImage(22*8, 15*8, BufferedImage.TYPE_BYTE_INDEXED, cm);
		frameBuffer = ((DataBufferByte)bi.getRaster().getDataBuffer()).getData();

		dumpFlares(cm);

		JFrame f = new JFrame("Particles");
		f.setLayout(new BorderLayout());
		final JPanel p = new JPanel() {
			public void paint(Graphics g) {
				g.drawImage(bi, 0, 0, 22*8*4, 15*8*4, null);
			}

			public Dimension getPreferredSize() {
				return new Dimension(22*8*4, 15*8*4);
			}
		};
		f.add(p, BorderLayout.CENTER);
		f.pack();
		f.setVisible(true);
		f.toFront();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		new Thread(() -> {
			while(true) {
				for (int frame=0; frame<10; frame++) {
					clear();
					drawMagicalGlimmer(frame);
					p.repaint();
					try {
						Thread.sleep(55*2);
					} catch (InterruptedException e) {
						;
					}
				}
			}
		}).start();
	}

    public static void main(String[] args) {
    	try {
    		new BigMagicalGlimmerDOS();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
