import java.io.*;
import java.nio.*;
import java.awt.image.*;
import javax.imageio.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;

public class ExplosionDOS {
	private byte frameBuffer[];

	int clipX=0;
	int clipY=0;
	int clipW=22*8;
	int clipH=15*8;
	void setClip(int x0, int y0, int x1, int y1) {
		clipX=x0*8;
		clipY=y0*8;
		clipW=(x1-x0)*8;
		clipH=(y1-y0)*8;
	}
	
	void setClipFine(int x0, int y0, int x1, int y1) {
		clipX=x0;
		clipY=y0;
		clipW=x1-x0;
		clipH=y1-y0;
	}

	public static int objectScanPositionZoom[] = {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3};
	public static int objectScanPositionDeltaXForCompartment[] = {-56, -8, 40, 88, 136, 184, 232, -72, 8, 88, 168, 248, -40, 88, 216, -88, 88, 264};

	int vx_fixpoint_rom[][] = new int[2][50];
	int vy_fixpoint_rom[][] = new int[2][50];
	void prepRomTables() {
		for (int j=0; j<2; j++) {
			int numberOfParticles = j==0 ? 35 : 50;
			int maxInitialVelocity = j==0 ? 147 : 460;
			int arg8 = j==0 ? 7 : 4;
			for (int i=0; i<numberOfParticles; i++) {
				vx_fixpoint_rom[j][i]=(getRandomValue(0,maxInitialVelocity)-maxInitialVelocity/2)*4;
				vy_fixpoint_rom[j][i]=(getRandomValue(0,maxInitialVelocity)-maxInitialVelocity/2 - (maxInitialVelocity>>(8-arg8)))*4;
			}
		}
	}

	public void renderExplosion(int zoomLevel, int mode, int[] colorTable, int deltaX, int deltaY)
	{
		int GRAVITY = 5*4;
		int xpos_fixpoint[] = new int[50];
		int ypos_fixpoint[] = new int[50];
		int vx_fixpoint[] = new int[50];
		int vy_fixpoint[] = new int[50];
		int darkness[] = new int[50];
		int fadeSpeed[] = new int[50];
		int floorLevelByZoom[] = {119, 103, 79, 63};

		int floorLevel = floorLevelByZoom[zoomLevel];
		
		if (zoomLevel!=0)
			zoomLevel--;
						
		int numberOfParticles = mode==0 ? 35 : 50;
		for (int i=0; i<50; i++)
		{
			xpos_fixpoint[i]=0;
			ypos_fixpoint[i]=0;

			vx_fixpoint[i] = vx_fixpoint_rom[mode][i];
			vy_fixpoint[i] = vy_fixpoint_rom[mode][i];

			fadeSpeed[i]=getRandomValue(0x400/20, 0x800/20);
			darkness[i]=zoomLevel*256;
		}
		
		boolean particleDrawn=true;
		while(particleDrawn) {
			for (int i=0; i<frameBuffer.length; i++)
				frameBuffer[i] = 0;

			for (int i=0; i<colorTable.length; i++)
				for (int y=0; y<8; y++)
					for (int x=0; x<8; x++)
						frameBuffer[i*8+x+y*22*8] = (byte)colorTable[i];

			particleDrawn=false;
			for (int i=0; i<numberOfParticles; i++) {
				// Deceleration in the x direction
				if (vx_fixpoint[i]<=0)
					vx_fixpoint[i]+=4;
				else
					vx_fixpoint[i]-=4;
				
				// Acceleration due to gravity
				vy_fixpoint[i]+=GRAVITY;

				// Apply velocity to position
				xpos_fixpoint[i]+=vx_fixpoint[i];
				ypos_fixpoint[i]+=vy_fixpoint[i];

				darkness[i]+=fadeSpeed[i];
				
				int xpos = xpos_fixpoint[i]/256 + deltaX;
				int ypos = ypos_fixpoint[i]/256 + deltaY;
				
				// Inelastic floor bounce. Speed halved
				if (ypos>=floorLevel)
					vy_fixpoint[i] = -vy_fixpoint[i]/2;

				if (darkness[i]/256 < colorTable.length) {
					int color = colorTable[darkness[i]/256];
						particleDrawn=true;
						if ((xpos>=0) && (xpos<22*8) && (ypos>=0) && (ypos<15*8))
							frameBuffer[xpos+ypos*22*8] = (byte)color;
				}
			}
		
			panel.repaint();

			try
			{
				Thread.sleep(1000/30);
			}
			catch (InterruptedException e)
			{
				;
			}		
		}	
	}

	public static int getRandomValue(int minVal, int maxVal)
	{
		Random r = new Random();
		return minVal + r.nextInt(maxVal-minVal);
	}

	public static int pseudoRandom()
	{
		Random r = new Random();
		return r.nextInt()&0xff;
	}

	public void handleExplosion() {
	//	int coneOfColdColors[] = {15,9,15,9,2,10,2,0};
	//	renderConeOfCold(150,50,10,1,100,coneOfColdColors);
		
		int explosionColorTable0[] = {15,   5,  15,   5,   6,   5,   6,   8,   6,   8,   6,   8};//,   0};
		int explosionColorTable1[] = {15,   9,  15,   9,   2,  10,  11,  10,  11};//,   0};
		int explosionColorTable2[] = { 5,   3,   5,   3,   3,   3,   3,  11,   3,  11};//,   0};
		int[] colorTable = explosionColorTable0;

/*
		renderer.calculateWallClip(si);
		if (renderer.wallX1 < renderer.wallX0)
			return;
*/

		renderExplosion(3,0,colorTable,88,48);
		renderExplosion(2,0,colorTable,88,48);
		renderExplosion(1,0,colorTable,88,48);
		renderExplosion(0,1,colorTable,88,48);
	}
	
	public void renderConeOfCold(int arg0, int arg2, int arg4, int arg6, int arg8, int[] argA)
	{
		int data0[] = new int[300];
		int data1[] = new int[300];
		int data2[] = new int[300];
		int data3[] = new int[300];
		int data4[] = new int[300];
		int data5[] = new int[300];
		int data6[] = new int[300];
		int data7[] = new int[300];
		int data8[] = new int[300];
	
		arg2<<=6;
		if (arg0>150)
			arg0=150;
		
		int var6=88;
		int var8=48;
		for (int si=0; si<arg0; si++)
		{
			int var38 = getRandomValue(arg2/4, arg2);
			int di=0;
			int var3A = 0;
			do
			{
				int ax = arg4;
				var3A+=ax;
				di+=var3A;
			}
			while (di<var38);
			
			int bx = pseudoRandom()&3;
			switch(bx)
			{
				case 0:
				{
					data0[si]=32;
					data1[si]=di;
					data2[si]=var3A;
					data3[si]=0;
					break;
				}
				case 1:
				{
					data0[si]=di;
					data1[si]=32;
					data2[si]=0;
					data3[si]=var3A;
					break;
				}
				case 2:
				{
					data0[si]=32;
					data1[si]=-di;
					data2[si]=var3A;
					data3[si]=0;
					break;
				}
				case 3:
				{
					data0[si]=-di;
					data1[si]=32;
					data2[si]=0;
					data3[si]=var3A;
					break;
				}
			}
			
			if ((pseudoRandom()&1)!=0)
			{
				data2[si]*=-1;
				data3[si]*=-1;
			}
			
			data5[si] = getRandomValue(0x400/arg8, 0x800/arg8);
			data6[si] = 0;
			data8[si] = getRandomValue(0, arg8/4);
		}
		
		int var4=2;
		int var12=0;
		for (; var4!=0; var12++)
		{
			for (int i=0; i<frameBuffer.length; i++)
				frameBuffer[i] = 0;
			
			var4=0;
			int var3C = arg4/2 + arg4/4 + arg4/8;
			for (int si=0; si<arg0; si++)
			{
				if (data8[si]!=0)
				{
					data8[si]--;
				}
				else
				{
					if (data0[si]<=0)
					{							
						if (data2[si]>=0)
						{
							data2[si]+=var3C;
						}
						else
						{
							data2[si]+=arg4;
						}							
					}
					else
					{
						if (data2[si]<=0)
						{
							data2[si]-=var3C;
						}
						else
						{
							data2[si]-=arg4;
						}
					}						
					
					if (data1[si]<=0)
					{
						if (data3[si]>=0)
						{
							data3[si]+=var3C;
						}
						else
						{
							data3[si]+=arg4;
						}
					}
					else
					{
						if (data3[si]<=0)
						{
							data3[si]-=var3C;
						}
						else
						{
							data3[si]-=arg4;
						}
					}
					
					data0[si]+=data2[si];
					data1[si]+=data3[si];
					data6[si]+=data5[si];
				}
				
				int xpos = data0[si]/64+var6;
				int ypos = data1[si]/64+var8;
				int var10=0;
				int var2 = argA[data6[si]/256];
				if (var2==0)
				{
					data5[si]=0;
				}
				else
				{
					var4=1;
					if ((var10==0) && (data8[si]==0))
					{
						if ((xpos>=0) && (xpos<22*8) && (ypos>=0) && (ypos<15*8))
							frameBuffer[xpos+ypos*22*8] = (byte)var2;
					}						
				}
			}
			
			panel.repaint();

			try
			{
				Thread.sleep(1000/60);
			}
			catch (InterruptedException e)
			{
				;
			}
		}
	}

	
	JPanel panel;
	public ExplosionDOS() throws IOException {
    	Global.BASE = "../../../java/pcdata/";
		Global.byteOrder = ByteOrder.LITTLE_ENDIAN;

    	CPSImage playfld = new CPSImage(Global.BASE+"playfld.cp_");
        IndexColorModel cm = new IndexColorModel(8,256,playfld.r,playfld.g,playfld.b,255);
		WritableRaster raster = Raster.createPackedRaster(DataBuffer.TYPE_BYTE, 22*8, 15*8, 1, 8, null);
		BufferedImage bi = new BufferedImage(22*8, 15*8, BufferedImage.TYPE_BYTE_INDEXED, cm);
		frameBuffer = ((DataBufferByte)bi.getRaster().getDataBuffer()).getData();

		prepRomTables();

		JFrame f = new JFrame("Explosion");
		f.setLayout(new BorderLayout());
		panel = new JPanel() {
			public void paint(Graphics g) {
				g.drawImage(bi, 0, 0, 22*8*4, 15*8*4, null);
			}

			public Dimension getPreferredSize() {
				return new Dimension(22*8*4, 15*8*4);
			}
		};
		f.add(panel, BorderLayout.CENTER);
		f.pack();
		f.setVisible(true);
		f.toFront();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		new Thread(() -> {
			while(true)
				handleExplosion();
		}).start();
	}

    public static void main(String[] args) {
    	try {
    		new ExplosionDOS();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
