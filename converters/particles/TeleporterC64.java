import java.io.*;
import java.nio.*;
import java.awt.image.*;
import javax.imageio.*;
import java.awt.*;
import javax.swing.*;

public class TeleporterC64 {
	private byte frameBuffer[];

	int teleportFlarePhase = 0;
	
	public static int objectScanPositionZoom[] = {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3};
	public static int objectScanPositionDeltaXForCompartment[] = {-56, -8, 40, 88, 136, 184, 232, -72, 8, 88, 168, 248, -40, 88, 216, -88, 88, 264};

	private byte flareGraphics[];

	void drawFlare(int flareIndex, int xpos, int ypos)
	{
		for (int y=0; y<16; y++) {
			for (int x=0; x<8; x++) {
				byte c = flareGraphics[flareIndex*8 + x + y*48];
				if (c==0)
					continue;
//				if (c==12)
//					c=4;
				frameBuffer[xpos+x+(ypos+y)*22*4] = c;
			}
		}
	}
	
	void drawTeleportOnPosition(int scanIndex)
	{
		int teleportFlareDeltas[][][] =
		{
			{
				{   12,     7},
				{   26,     1},
				{   62,     3},
				{   12,    26},
				{   42,    19},
				{   64,    24},
				{    2,    45},
				{   22,    37},
				{   40,    50},
				{   54,    39},
				{   10,    62},
				{   22,    73},
				{   62,    68}
			},
			{				
				{    6,     6},
				{   42,     4},
				{   55,    10},
				{    4,    27},
				{   26,    22},
				{   55,    29},
				{   14,    42},
				{   27,    53},
				{   46,    40},
				{   66,    48},
				{    6,    71},
				{    6,    71},
				{   45,    76}
			},
			{		
				{   10,     4},
				{   20,     0},
				{   46,     1},
				{   12,    16},
				{   31,    16},
				{   47,    16},
				{   18,    24},
				{   40,    29},
				{    1,    33},
				{    8,    42},
				{   17,    50},
				{   47,    46},
				{   31,    37}
			},
			{
				{    2,     2},
				{    1,    17},
				{    1,    47},
				{    8,    30},
				{   17,    14},
				{   17,    38},
				{   28,     1},
				{   30,    25},
				{   31,    51},
				{   36,    17},
				{   38,     5},
				{   40,    43},
				{   47,    34}
			},
			{
				{    0,    19},
				{    5,     1},
				{    6,     8},
				{    9,    12},
				{    4,    26},
				{    8,    31},
				{   18,     5},
				{   18,    21},
				{   22,    16},
				{   26,     8},
				{   26,    29},
				{   10,     0},
				{   10,     0}
			},
			{
				{    0,     9},
				{    0,    30},
				{    4,    17},
				{    8,    22},
				{    8,     6},
				{   16,     0},
				{   17,    13},
				{   18,    32},
				{   21,     2},
				{   20,     9},
				{   22,    27},
				{   26,    20},
				{   26,    20}
			}
		};

		int teleportDeltaX[] = {40,  28,  18};
		int teleportDeltaY[] = {13,  21,  26};
		int zoom = 2-objectScanPositionZoom[scanIndex];
		if (zoom<0)
			return;

		int deltaX = objectScanPositionDeltaXForCompartment[scanIndex] - teleportDeltaX[zoom];
		int deltaY = teleportDeltaY[zoom];
		for (int flareSize=0; flareSize<2; flareSize++)
		{
			int scaledFlareSize = zoom*2+flareSize;
			int flareIndex = scaledFlareSize^teleportFlarePhase;
			for (int i=0; i<13; i++)
			{
				int xpos = teleportFlareDeltas[scaledFlareSize][i][0] + deltaX;
				int ypos = teleportFlareDeltas[scaledFlareSize][i][1] + deltaY;
				drawFlare(flareIndex, (xpos-1)/2, ypos+1);
			}
		}
	}

	private void render() {
		for (int i=0; i<frameBuffer.length; i++)
			frameBuffer[i] = 0;

		int x0[] = new int[]{6,8,9};
		int y0[] = new int[]{16,26,32};
		int x1[] = new int[]{15,14,12};
		int y1[] = new int[]{103,83,68};
		int sp[] = new int[]{13,9,3};

		int f = 2;
		for (int y=y0[f]; y<=y1[f]; y++) {
			for (int cx=x0[f]; cx<=x1[f]; cx++) {
				for (int x=0; x<4; x++) {
					frameBuffer[cx*4+x + y*22*4] = 0;
				}
			}
		}
		drawTeleportOnPosition(sp[f]);
	}

	private void precalcTeleporter() throws IOException {
		int x0[] = new int[]{6,8,9};
		int y0[] = new int[]{16,26,32};
		int x1[] = new int[]{15,14,12};
		int y1[] = new int[]{103,83,68};
		int sp[] = new int[]{13,9,3};

		PrintWriter out = new PrintWriter(new File("teleporter.s"));

		int size=0;
		for (int f=0; f<3; f++) {
			for (teleportFlarePhase=0; teleportFlarePhase<2; teleportFlarePhase++) {
				for (int i=0; i<frameBuffer.length; i++)
					frameBuffer[i] = 0;

				drawTeleportOnPosition(sp[f]);

				for (int cx=x0[f]; cx<=x1[f]; cx++) {
					System.out.printf(".byte <teleportZ%dP%dC%d\n", f, teleportFlarePhase, cx-x0[f]);
					out.printf("teleportZ%dP%dC%d:\n", f, teleportFlarePhase, cx-x0[f]);
					int lastY=0;
					for (int y=y0[f]; y<=y1[f]; y++) {
						int b=0;
						int mask=0xff;
						for (int x=0; x<4; x++) {
							byte c = frameBuffer[cx*4+x + y*22*4];
							int p=0;
							if (c==0)
								p=0;
							else if (c==9)
								p=1;
							else if (c==12)
								p=2;
							else if (c==15)
								p=3;
							else {
								System.err.printf("Illegal color %d\n", c);
								System.exit(1);
							}
							b |= p<<((3-x)*2);
							if (c!=0)
								mask &= (3<<((3-x)*2))^0xff;
						}
						if (b==0)
							continue;

						mask=0;

						if (y-lastY==1) {
							out.printf("\tiny\n");
							size++;
						} else {
							out.printf("\tldy #$%02x\n", y);
							size+=2;
						}

						if (mask!=0) {
							out.printf("\tlda (DST_SPRITES_USE),y\n");
							out.printf("\tand #$%02x\n", mask);
							out.printf("\tora #$%02x\n", b);
							size+=6;
						} else {
							out.printf("\tlda #$%02x\n", b);
							size+=2;
						}
						out.printf("\tsta (DST_SPRITES_USE),y\n");
						size+=2;

						lastY=y;
					}
					out.println("\trts");
					size++;
				}
			}
		}
		out.printf("; Total code size: $%04x\n", size);
		out.close();
	}
	
	public TeleporterC64() throws IOException {
		BufferedImage tmp = ImageIO.read(new File("converted/flares.png"));
		flareGraphics = ((DataBufferByte)tmp.getRaster().getDataBuffer()).getData();

		byte r[] = {(byte)0x00,(byte)0xFF,(byte)0x68,(byte)0x70,(byte)0x6F,(byte)0x58,(byte)0x35,(byte)0xB8,(byte)0x6F,(byte)0x43,(byte)0x9A,(byte)0x44,(byte)0x6C,(byte)0x9A,(byte)0x6C,(byte)0x95};
		byte g[] = {(byte)0x00,(byte)0xFF,(byte)0x37,(byte)0xA4,(byte)0x3D,(byte)0x8D,(byte)0x28,(byte)0xC7,(byte)0x4F,(byte)0x39,(byte)0x67,(byte)0x44,(byte)0x6C,(byte)0xD2,(byte)0x5E,(byte)0x95};
		byte b[] = {(byte)0x00,(byte)0xFF,(byte)0x2B,(byte)0xB2,(byte)0x86,(byte)0x43,(byte)0x79,(byte)0x6F,(byte)0x25,(byte)0x00,(byte)0x59,(byte)0x44,(byte)0x6C,(byte)0x84,(byte)0xB5,(byte)0x95};
		IndexColorModel icm = new IndexColorModel(8,16,r,g,b);
		BufferedImage image = new BufferedImage(22*4,15*8,BufferedImage.TYPE_BYTE_INDEXED,icm);
		frameBuffer = ((DataBufferByte)image.getRaster().getDataBuffer()).getData();

		precalcTeleporter();
		System.exit(0);

		JFrame f = new JFrame("Particles");
		f.setLayout(new BorderLayout());
		final JPanel p = new JPanel() {
			public void paint(Graphics g) {
				g.drawImage(image, 0, 0, 22*8*4, 15*8*4, null);
			}

			public Dimension getPreferredSize() {
				return new Dimension(22*8*4, 15*8*4);
			}
		};
		f.add(p, BorderLayout.CENTER);
		f.pack();
		f.setVisible(true);
		f.toFront();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		new Thread(() -> {
			while(true) {
				try {
					Thread.sleep(55*10);
					teleportFlarePhase ^= 1;
					render();
					p.repaint();
				} catch (InterruptedException e) {
					;
				} 
			}
		}).start();
	}

    public static void main(String[] args) {
    	try {
    		new TeleporterC64();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
