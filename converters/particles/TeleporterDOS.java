import java.io.*;
import java.nio.*;
import java.awt.image.*;
import javax.imageio.*;
import java.awt.*;
import javax.swing.*;

public class TeleporterDOS {
	private byte frameBuffer[];

	int teleportFlarePhase = 0;
	int clipX=0;
	int clipY=0;
	int clipW=22*8;
	int clipH=15*8;
	void setClip(int x0, int y0, int x1, int y1) {
		clipX=x0*8;
		clipY=y0*8;
		clipW=(x1-x0)*8;
		clipH=(y1-y0)*8;
	}
	
	void setClipFine(int x0, int y0, int x1, int y1) {
		clipX=x0;
		clipY=y0;
		clipW=x1-x0;
		clipH=y1-y0;
	}

	public static int objectScanPositionZoom[] = {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3};
	public static int objectScanPositionDeltaXForCompartment[] = {-56, -8, 40, 88, 136, 184, 232, -72, 8, 88, 168, 248, -40, 88, 216, -88, 88, 264};

	void drawMaskedGfxObject(GfxObject gfxObject, int xpos, int ypos)
	{
		for (int y=0;y<gfxObject.height;y++)
			for (int x=0;x<gfxObject.width;x++)
			{
				byte c = gfxObject.data[x+y*gfxObject.width];
				if (c==0)
					continue;
				if ((xpos+x<clipX) || (xpos+x>=clipX+clipW) || (ypos+y<clipY) || (ypos+y>=clipY+clipH))
					continue;
				frameBuffer[xpos+x+(ypos+y)*22*8]=c;
			}
	}
	
	void drawMaskedFlippedGfxObject(GfxObject gfxObject, int xpos, int ypos)
	{
		for (int y=0;y<gfxObject.height;y++)
			for (int x=0;x<gfxObject.width;x++)
			{
				byte c = gfxObject.data[gfxObject.width-1-x+y*gfxObject.width];
				if (c==0)
					continue;
				if ((xpos+x<clipX) || (xpos+x>=clipX+clipW) || (ypos+y<clipY) || (ypos+y>=clipY+clipH))
					continue;
				frameBuffer[xpos+x+(ypos+y)*22*8]=c;
			}
	}

	void drawTeleportOnPosition(int scanIndex)
	{
		int teleportFlareDeltas[][][] =
		{
			{
				{   12,     7},
				{   26,     1},
				{   62,     3},
				{   12,    26},
				{   42,    19},
				{   64,    24},
				{    2,    45},
				{   22,    37},
				{   40,    50},
				{   54,    39},
				{   10,    62},
				{   22,    73},
				{   62,    68}
			},
			{				
				{    6,     6},
				{   42,     4},
				{   55,    10},
				{    4,    27},
				{   26,    22},
				{   55,    29},
				{   14,    42},
				{   27,    53},
				{   46,    40},
				{   66,    48},
				{    6,    71},
				{    6,    71},
				{   45,    76}
			},
			{		
				{   10,     4},
				{   20,     0},
				{   46,     1},
				{   12,    16},
				{   31,    16},
				{   47,    16},
				{   18,    24},
				{   40,    29},
				{    1,    33},
				{    8,    42},
				{   17,    50},
				{   47,    46},
				{   31,    37}
			},
			{
				{    2,     2},
				{    1,    17},
				{    1,    47},
				{    8,    30},
				{   17,    14},
				{   17,    38},
				{   28,     1},
				{   30,    25},
				{   31,    51},
				{   36,    17},
				{   38,     5},
				{   40,    43},
				{   47,    34}
			},
			{
				{    0,    19},
				{    5,     1},
				{    6,     8},
				{    9,    12},
				{    4,    26},
				{    8,    31},
				{   18,     5},
				{   18,    21},
				{   22,    16},
				{   26,     8},
				{   26,    29},
				{   10,     0},
				{   10,     0}
			},
			{
				{    0,     9},
				{    0,    30},
				{    4,    17},
				{    8,    22},
				{    8,     6},
				{   16,     0},
				{   17,    13},
				{   18,    32},
				{   21,     2},
				{   20,     9},
				{   22,    27},
				{   26,    20},
				{   26,    20}
			}
		};

		int teleportDeltaX[] = {40,  28,  18};
		int teleportDeltaY[] = {13,  21,  26};
		int zoom = 2-objectScanPositionZoom[scanIndex];
		if (zoom<0)
			return;
			
		int deltaX = objectScanPositionDeltaXForCompartment[scanIndex] - teleportDeltaX[zoom];
		int deltaY = teleportDeltaY[zoom];
		for (int flareSize=0; flareSize<2; flareSize++)
		{
			int scaledFlareSize = zoom*2+flareSize;
			int adjustDeltaY = 0;
			int adjustDeltaX = 0;
			if (scaledFlareSize==0)
			{
				adjustDeltaX = -4;
				adjustDeltaY = -4;
			}
			GfxObject gfx = GfxResources.teleportFlareGfx[scaledFlareSize^teleportFlarePhase];
			
			for (int i=0; i<13; i++)
			{
				int xpos = teleportFlareDeltas[scaledFlareSize][i][0] + adjustDeltaX + deltaX;
				int ypos = teleportFlareDeltas[scaledFlareSize][i][1] + adjustDeltaY + deltaY;
				drawMaskedGfxObject(gfx,xpos,ypos);
			}
		}
	}

	private void render() {
		for (int i=0; i<frameBuffer.length; i++)
			frameBuffer[i] = 0;

//		drawTeleportOnPosition(3);
//		drawTeleportOnPosition(9);
		drawTeleportOnPosition(13);
	}
	
	public TeleporterDOS() throws IOException {
    	Global.BASE = "../../../java/pcdata/";
		Global.byteOrder = ByteOrder.LITTLE_ENDIAN;

    	CPSImage playfld = new CPSImage(Global.BASE+"playfld.cp_");
        IndexColorModel cm = new IndexColorModel(8,256,playfld.r,playfld.g,playfld.b,255);
		WritableRaster raster = Raster.createPackedRaster(DataBuffer.TYPE_BYTE, 22*8, 15*8, 1, 8, null);
		BufferedImage bi = new BufferedImage(22*8, 15*8, BufferedImage.TYPE_BYTE_INDEXED, cm);
		frameBuffer = ((DataBufferByte)bi.getRaster().getDataBuffer()).getData();

		JFrame f = new JFrame("Particles");
		f.setLayout(new BorderLayout());
		final JPanel p = new JPanel() {
			public void paint(Graphics g) {
				g.drawImage(bi, 0, 0, 22*8*4, 15*8*4, null);
			}

			public Dimension getPreferredSize() {
				return new Dimension(22*8*4, 15*8*4);
			}
		};
		f.add(p, BorderLayout.CENTER);
		f.pack();
		f.setVisible(true);
		f.toFront();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		new Thread(() -> {
			while(true) {
				try {
					Thread.sleep(55*10);
					teleportFlarePhase ^= 1;
					render();
					p.repaint();
				} catch (InterruptedException e) {
					;
				} 
			}
		}).start();
	}

    public static void main(String[] args) {
    	try {
    		new TeleporterDOS();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
