import java.io.*;
import java.nio.*;
import java.awt.image.*;
import javax.imageio.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;

public class ExplosionC64 {
	private byte frameBuffer[];

	public static int getRandomValue(int minVal, int maxVal)
	{
		Random r = new Random();
		return minVal + r.nextInt(maxVal-minVal);
	}

	public static int pseudoRandom()
	{
		Random r = new Random();
		return r.nextInt()&0xff;
	}

	int vx_fixpoint_rom[][] = new int[2][50];
	int vy_fixpoint_rom[][] = new int[2][50];
	int fadeSpeed[] = new int[50];
	void prepRomTables() {
		for (int j=0; j<2; j++) {
			int numberOfParticles = j==0 ? 35 : 50;
			int maxInitialVelocity = j==0 ? 147 : 460;
			int arg8 = j==0 ? 7 : 4;
			for (int i=0; i<numberOfParticles; i++) {
				vx_fixpoint_rom[j][i]=(getRandomValue(0,maxInitialVelocity)-maxInitialVelocity/2)*4;
				vy_fixpoint_rom[j][i]=(getRandomValue(0,maxInitialVelocity)-maxInitialVelocity/2 - (maxInitialVelocity>>(8-arg8)))*4;
			}
		}

		for (int i=0; i<50; i++)
			fadeSpeed[i]=getRandomValue(51, 102);

		System.out.printf("fadeSpeed: .byte ");
		for (int i=0; i<50; i++) { 
			if (i>0) System.out.printf(",");
			System.out.printf("$%02x", fadeSpeed[i]);
		}
		System.out.println();

		for (int mode=0; mode<2; mode++) {
			int numberOfParticles = mode==0 ? 35 : 50;
			System.out.printf("vx_fixpoint_%d_lo: .byte ", mode);
			for (int i=0; i<numberOfParticles; i++) { 
				if (i>0) System.out.printf(",");
				System.out.printf("<$%04x", vx_fixpoint_rom[mode][i]&0xffff);
			}
			System.out.println();
			System.out.printf("vx_fixpoint_%d_hi: .byte ", mode);
			for (int i=0; i<numberOfParticles; i++) { 
				if (i>0) System.out.printf(",");
				System.out.printf(">$%04x", vx_fixpoint_rom[mode][i]&0xffff);
			}
			System.out.println();
			System.out.printf("vy_fixpoint_%d_lo: .byte ", mode);
			for (int i=0; i<numberOfParticles; i++) { 
				if (i>0) System.out.printf(",");
				System.out.printf("<$%04x", vy_fixpoint_rom[mode][i]&0xffff);
			}
			System.out.println();
			System.out.printf("vy_fixpoint_%d_hi: .byte ", mode);
			for (int i=0; i<numberOfParticles; i++) { 
				if (i>0) System.out.printf(",");
				System.out.printf(">$%04x", vy_fixpoint_rom[mode][i]&0xffff);
			}
			System.out.println();
		}
	}

	public void renderExplosion(int zoomLevel, int mode, int[] colorTable, int deltaX, int deltaY)
	{
		int zl = zoomLevel;
		int GRAVITY = 5*4;
		int xpos_fixpoint[] = new int[50];
		int ypos_fixpoint[] = new int[50];
		int vx_fixpoint[] = new int[50];
		int vy_fixpoint[] = new int[50];
		int darkness[] = new int[50];
		int floorLevelByZoom[] = {119, 103, 79, 63};

		int floorLevel = floorLevelByZoom[zoomLevel];
		
		if (zoomLevel!=0)
			zoomLevel--;
						
		int numberOfParticles = mode==0 ? 35 : 50;
		for (int i=0; i<50; i++)
		{
			xpos_fixpoint[i]=0;
			ypos_fixpoint[i]=0;

			vx_fixpoint[i] = vx_fixpoint_rom[mode][i];
			vy_fixpoint[i] = vy_fixpoint_rom[mode][i];

			darkness[i]=zoomLevel*256;
		}
		
		int frameCount=0;
		boolean particleDrawn=true;
		while(particleDrawn) {
			for (int i=0; i<frameBuffer.length; i++)
				frameBuffer[i] = 0;

			for (int i=0; i<colorTable.length; i++)
				for (int y=0; y<8; y++)
					for (int x=0; x<4; x++)
						frameBuffer[i*4+x+y*22*4] = (byte)colorTable[i];

			particleDrawn=false;
			for (int i=0; i<numberOfParticles; i++) {
				// Deceleration in the x direction
				if (vx_fixpoint[i]<=0)
					vx_fixpoint[i]+=4;
				else
					vx_fixpoint[i]-=4;
				
				// Acceleration due to gravity
				vy_fixpoint[i]+=GRAVITY;

				// Apply velocity to position
				xpos_fixpoint[i]+=vx_fixpoint[i];
				ypos_fixpoint[i]+=vy_fixpoint[i];

				darkness[i]+=fadeSpeed[i];
				
				int xpos = xpos_fixpoint[i]/256 + deltaX;
				int ypos = ypos_fixpoint[i]/256 + deltaY;
				
				// Inelastic floor bounce. Speed halved
				if (ypos>=floorLevel)
					vy_fixpoint[i] = -vy_fixpoint[i]/2;

				if (darkness[i]/256 < colorTable.length) {
					int color = colorTable[darkness[i]/256];
						particleDrawn=true;
						if ((xpos>=0) && (xpos<22*8) && (ypos>=0) && (ypos<15*8))
							frameBuffer[xpos/2+ypos*22*4] = (byte)(color&15);
				}
			}
		
			frameCount++;
			panel.repaint();

			try
			{
				Thread.sleep(1000/30);
			}
			catch (InterruptedException e)
			{
				;
			}		
		}	
		System.out.printf("%d frames on zoom level %d\n", frameCount, zl);
	}

	public void handleExplosion() {		
		int explosionColorTable0[] = {15,   5,  15,   5,   6,   5,   6,   8,   6,   8,   6,   8};//,   0};
		int explosionColorTable1[] = {15,   9,  15,   9,   2,  10,  11,  10,  11};//,   0};
		int explosionColorTable2[] = { 5,   3,   5,   3,   3,   3,   3,  11,   3,  11};//,   0};
		int[] colorTable = {15,15,12,15,12,12,9,12,9,9};

		renderExplosion(3,0,colorTable,88,48);
		renderExplosion(2,0,colorTable,88,48);
		renderExplosion(1,0,colorTable,88,48);
		renderExplosion(0,1,colorTable,88,48);
	}
	
	JPanel panel;
	public ExplosionC64() throws IOException {
		byte r[] = {(byte)0x00,(byte)0xFF,(byte)0x68,(byte)0x70,(byte)0x6F,(byte)0x58,(byte)0x35,(byte)0xB8,(byte)0x6F,(byte)0x43,(byte)0x9A,(byte)0x44,(byte)0x6C,(byte)0x9A,(byte)0x6C,(byte)0x95};
		byte g[] = {(byte)0x00,(byte)0xFF,(byte)0x37,(byte)0xA4,(byte)0x3D,(byte)0x8D,(byte)0x28,(byte)0xC7,(byte)0x4F,(byte)0x39,(byte)0x67,(byte)0x44,(byte)0x6C,(byte)0xD2,(byte)0x5E,(byte)0x95};
		byte b[] = {(byte)0x00,(byte)0xFF,(byte)0x2B,(byte)0xB2,(byte)0x86,(byte)0x43,(byte)0x79,(byte)0x6F,(byte)0x25,(byte)0x00,(byte)0x59,(byte)0x44,(byte)0x6C,(byte)0x84,(byte)0xB5,(byte)0x95};
		IndexColorModel icm = new IndexColorModel(8,16,r,g,b);
		BufferedImage image = new BufferedImage(22*4,15*8,BufferedImage.TYPE_BYTE_INDEXED,icm);
		frameBuffer = ((DataBufferByte)image.getRaster().getDataBuffer()).getData();

		prepRomTables();

		JFrame f = new JFrame("Explosion");
		f.setLayout(new BorderLayout());
		panel = new JPanel() {
			public void paint(Graphics g) {
				g.drawImage(image, 0, 0, 22*8*4, 15*8*4, null);
			}

			public Dimension getPreferredSize() {
				return new Dimension(22*8*4, 15*8*4);
			}
		};
		f.add(panel, BorderLayout.CENTER);
		f.pack();
		f.setVisible(true);
		f.toFront();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		new Thread(() -> {
			while(true)
				handleExplosion();
		}).start();
	}

    public static void main(String[] args) {
    	try {
    		new ExplosionC64();
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
