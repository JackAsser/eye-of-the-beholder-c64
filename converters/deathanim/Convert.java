import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Convert {
	public static EOBBank convert(int y) throws Exception {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)0);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)12);
		C64Char.spriteColors.add((byte)15);

		ArrayList<EOBObject> eobObjects = new ArrayList<EOBObject>();

		BufferedImage bitmap = ImageIO.read(new File("converted/ingame_xanathar_death.png"));
		int iPixels[] = bitmap.getSampleModel().getPixels(0,0,bitmap.getWidth(),bitmap.getHeight(),(int[])null, bitmap.getRaster().getDataBuffer());

		int sectionWidth = 14;
		int sectionHeight = 12;
		int baseCy = y*12;
		for (int x=0; x<3; x++) {
			int baseCx = x*14;
			if (x==0 && y==0)
				continue;

			// Find bounding box
			int top=0;
			topFound:for (top=0; top<sectionHeight; top++)
				for (int cx=0; cx<sectionWidth; cx++)
					if (C64Char.charType(iPixels, bitmap.getWidth(), baseCx+cx, baseCy+top) != C64Char.CharType.Empty)
						break topFound;
			int left=0;
			leftFound:for (left=0; left<sectionWidth; left++)
				for (int cy=top; cy<sectionHeight; cy++)
					if (C64Char.charType(iPixels, bitmap.getWidth(), baseCx+left, baseCy+cy) != C64Char.CharType.Empty)
						break leftFound;
			int right=0;
			rightFound:for (right=sectionWidth-1; right>=0; right--)
				for (int cy=top; cy<sectionHeight; cy++)
					if (C64Char.charType(iPixels, bitmap.getWidth(), baseCx+right, baseCy+cy) != C64Char.CharType.Empty)
						break rightFound;

			EOBObject obj = new EOBObject(bitmap, baseCx+left, baseCy+top, right-left+1, sectionHeight-top);
			obj.dx = (byte)(left+4);
			obj.dy = (byte)(top+1);
			eobObjects.add(obj);
		}

		if (y==3)
			eobObjects.add(new EOBObject(ImageIO.read(new File("converted/ingame_xanathar_glassdoor.png")), 0,0, 16, 12));
		return new EOBBank(String.format("xanathar%d",y), eobObjects);
	}

	public static void main(String[] args) {
		try {
			for (int y=0; y<4; y++) {
				EOBBank bank = Convert.convert(y);
				System.out.printf("Size for deathanim_xanathar%d will be $%04x\n", y, bank.size());
				bank.dump("deathanim_");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}