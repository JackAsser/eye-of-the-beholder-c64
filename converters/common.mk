.SUFFIXES: .class .java
JAVAC = javac
JAVA  = java

EOBLIB = ../eoblib/eoblib.jar
COMMON = ../common/
CLASSPATH = ./:$(EOBLIB):$(COMMON)
RUN_OPTIONS = -Djava.awt.headless=true
BUILD_OPTIONS = -Werror -Xlint:unchecked

RUN = @$(JAVA) $(RUN_OPTIONS) -cp $(CLASSPATH)

.java.class:
	@$(JAVAC) $(BUILD_OPTIONS) -cp $(CLASSPATH) *.java
