import java.util.*;
import java.io.*;

public class EOBBank {
	public enum DumpMode {
		Normal,
		Background
	}

	public ArrayList<Byte> bank;
	public ArrayList<Byte> flippedBank;
	public ArrayList<Byte> screenData;
	public ArrayList<Byte> d800Data;
	public ArrayList<Short> objCharOffsets;
	public ArrayList<Short> objBankOffsets;
	public ArrayList<Byte> objWidths;
	public ArrayList<Byte> objHeights;
	public ArrayList<Byte> objDx;
	public ArrayList<Byte> objDy;
	public String name;
	
	private boolean shouldEmitDx;
	private boolean shouldEmitDy;

	public int size() {
		int size = bank.size() + screenData.size() + d800Data.size();
		size += objCharOffsets.size()*2 + objBankOffsets.size()*2;
		size += objWidths.size() + objHeights.size();
		if (shouldEmitDx)
			size += objDx.size();
		if (shouldEmitDy)
			size += objDy.size();
		return size;
	}

	public PrintStream dump(String prefix) throws IOException {
		return dump(prefix, DumpMode.Normal);
	}

	public PrintStream dump(String prefix, DumpMode dumpMode) throws IOException {
		String combinedName = prefix+name;

		FileOutputStream fos;
		fos = new FileOutputStream(combinedName+"_data.bin");
		for (Byte b : bank) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		if (dumpMode == DumpMode.Background) {
			fos = new FileOutputStream(combinedName+"_flippedData.bin");
			for (Byte b : flippedBank) {
				int i = (int)(b&0xff);
				fos.write(i);
			}
			fos.close();
		}

		fos = new FileOutputStream(combinedName+"_screen.bin");
		for (Byte b : screenData) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		fos = new FileOutputStream(combinedName+"_d800.bin");
		for (Byte b : d800Data) {
			int i = (int)(b&0xff);
			fos.write(i);
		}
		fos.close();

		// Emit source
		PrintStream ps = new PrintStream(new File(combinedName+".s"));

		ps.printf(".segment \"%s\"\n", combinedName.toUpperCase());
		ps.printf(".export %s\n", combinedName);
		ps.printf("%s:\n", combinedName);

		if (dumpMode == DumpMode.Normal) {
			ps.printf("jmp %sdraw_normal\n", prefix);
			ps.printf("jmp %sdraw_flipped\n", prefix);
			ps.println();

			ps.println("_width:");
			ps.printf("\t.byte ");
			for (int i=0; i<objWidths.size(); i++) {
				if (i>0)
					ps.printf(",");
				ps.printf("$%02x", objWidths.get(i));
			}
			ps.println();

			ps.println("_height:");
			ps.printf("\t.byte ");
			for (int i=0; i<objHeights.size(); i++) {
				if (i>0)
					ps.printf(",");
				ps.printf("$%02x", objHeights.get(i));
			}
			ps.println();

			if (shouldEmitDx) {
				ps.println("_dx:");
				ps.printf("\t.byte ");
				for (int i=0; i<objDx.size(); i++) {
					if (i>0)
						ps.printf(",");
					ps.printf("$%02x", objDx.get(i));
				}
				ps.println();
			}
			
			if (shouldEmitDy) {
				ps.println("_dy:");
				ps.printf("\t.byte ");
				for (int i=0; i<objDy.size(); i++) {
					if (i>0)
						ps.printf(",");
					ps.printf("$%02x", objDy.get(i));
				}
				ps.println();
			}
			
			ps.println();

			ps.println("_char_offset_lo:");
			ps.printf("\t.byte ");
			for (int i=0; i<objCharOffsets.size(); i++) {
				if (i>0)
					ps.printf(",");
				ps.printf("<$%04x", objCharOffsets.get(i));
			}
			ps.println();

			ps.println("_char_offset_hi:");
			ps.printf("\t.byte ");
			for (int i=0; i<objBankOffsets.size(); i++) {
				if (i>0)
					ps.printf(",");
				ps.printf(">$%04x", objCharOffsets.get(i));
			}
			ps.println();

			ps.println("_data_ptr_lo:");
			ps.printf("\t.byte ");
			for (int i=0; i<objBankOffsets.size(); i++) {
				if (i>0)
					ps.printf(",");
				ps.printf("<(_data+$%04x)", objBankOffsets.get(i));
			}
			ps.println();

			ps.println("_data_ptr_hi:");
			ps.printf("\t.byte ");
			for (int i=0; i<objBankOffsets.size(); i++) {
				if (i>0)
					ps.printf(",");
				ps.printf(">(_data+$%04x)", objBankOffsets.get(i));
			}
			ps.println();
		}
		else if (dumpMode == DumpMode.Background) {
			ps.println(".word _screen");
			ps.println(".word _d800");
			ps.println(".word _data");
			ps.println(".word _flippedData");
			ps.println("_flippedData:");
			ps.printf("\t.incbin \"%s_flippedData.bin\"\n", combinedName);
		}

		ps.println("_data:");
		ps.printf("\t.incbin \"%s_data.bin\"\n", combinedName);
		ps.println("_screen:");
		ps.printf("\t.incbin \"%s_screen.bin\"\n", combinedName);
		ps.println("_d800:");
		ps.printf("\t.incbin \"%s_d800.bin\"\n", combinedName);
		
		if (dumpMode == DumpMode.Normal) {
			ps.println();
			ps.printf(".include \"../../%srenderer.s\"\n", prefix);
		}

		return ps;
	}

	public EOBBank(String name, List<EOBObject> eobObjects) {
		this.name = name;
		bank = new ArrayList<>();
		flippedBank = new ArrayList<>();
		screenData = new ArrayList<>();
		d800Data = new ArrayList<>();
		objCharOffsets = new ArrayList<>();
		objBankOffsets = new ArrayList<>();
		objWidths = new ArrayList<>();
		objHeights = new ArrayList<>();
		objDx = new ArrayList<>();
		objDy = new ArrayList<>();
		shouldEmitDx = false;
		shouldEmitDy = false;

		for (EOBObject eobObject : eobObjects) {
			objWidths.add(eobObject.width);
			objHeights.add(eobObject.height);
			objDx.add(eobObject.dx);
			objDy.add(eobObject.dy);
			if (eobObject.dx != 0)
				shouldEmitDx = true;
			if (eobObject.dy != 0)
				shouldEmitDy = true;
			objCharOffsets.add((short)screenData.size());
			objBankOffsets.add((short)bank.size());

			for (int x=0; x<eobObject.width; x++) {
				ArrayList<Byte> columnData = new ArrayList<>();
				for (int y=0; y<eobObject.height; y++) {
					C64Char c64Char = eobObject.c64Chars[y+x*eobObject.height];
					screenData.add(c64Char.screen);
					d800Data.add(c64Char.d800);
					switch (c64Char.type) {
						case Bitmap:
							columnData.addAll(c64Char.data);
							break;
						case Sprite:
							columnData.addAll(c64Char.mask);
							columnData.addAll(c64Char.data);
							break;
					}
				}
				
				if (columnData.size()>255) {
					System.err.println("EOBObject column is >255 bytes. Can't add skip marker!!!");
					System.exit(1);
				}

				bank.add((byte)columnData.size());
				bank.addAll(columnData);

				// This will only be correct for pure Bitmap stuff
				flippedBank.add((byte)columnData.size());
				for (byte b : columnData)
					flippedBank.add((byte)(((b&0x03)<<6) | ((b&0x0c)<<2) | ((b&0x30)>>2) | ((b&0xc0)>>6)));
			}
		}		
	}
}
