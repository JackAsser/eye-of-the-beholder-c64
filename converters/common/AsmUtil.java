import java.io.*;
import java.util.*;

public class AsmUtil {
	public static void dumpName(PrintStream out, String name) {
		if (name==null)
			return;
		out.printf(".export %s\n", name);
		out.printf("%s:\n", name);		
	}

	public static void dumpLocalName(PrintStream out, String name) {
		if (name==null)
			return;
		out.printf("%s:\n", name);		
	}

	public static void dumpByteArray(PrintStream out, String name, List<Integer> c) {
		dumpName(out, name);
		for (int i=0; i<c.size(); i++) {
			if ((i&31)==0)
				out.printf("\t.byte ");
			out.printf("$%02x", c.get(i)&0xff);
			if (((i&31)<31) && (i<c.size()-1))
				out.printf(",");
			else
				out.println();
		}
	}

	public static void dumpByteArray(PrintStream out, String name, int[] c) {
		dumpByteArray(out, name, new ArrayList<Integer>() {{ for (int i : c) add(i); }});
	}

	public static void dumpByteArray(PrintStream out, String name, byte[] c) {
		dumpByteArray(out, name, new ArrayList<Integer>() {{ for (byte i : c) add(i&0xff); }});
	}

	public static void dumpShortLoArray(PrintStream out, String name, List<Integer> c) {
		dumpName(out, name);
		for (int i=0; i<c.size(); i++) {
			if ((i&31)==0)
				out.printf("\t.byte ");
			out.printf("<$%04x", c.get(i)&0xffff);
			if (((i&31)<31) && (i<c.size()-1))
				out.printf(",");
			else
				out.println();
		}
	}

	public static void dumpShortHiArray(PrintStream out, String name, List<Integer> c) {
		dumpName(out, name);
		for (int i=0; i<c.size(); i++) {
			if ((i&31)==0)
				out.printf("\t.byte ");
			out.printf(">$%04x", c.get(i)&0xffff);
			if (((i&31)<31) && (i<c.size()-1))
				out.printf(",");
			else
				out.println();
		}
	}

	public static void dumpPtrLoArray(PrintStream out, String name, String ref, boolean allowNull, int scale, List<Integer> c) {
		dumpName(out, name);
		for (int i=0; i<c.size(); i++) {
			if ((i&31)==0)
				out.printf("\t.byte ");
			if ((c.get(i)==0) && (!allowNull))
				out.printf("$0000");
			else {
				if (scale==0)
					out.printf("<(%s+$%04x)", ref, c.get(i)&0xffff);
				else
					out.printf("<(%s+$%04x*%d)", ref, c.get(i)&0xffff, scale);
			}
			if (((i&31)<31) && (i<c.size()-1))
				out.printf(",");
			else
				out.println();
		}
	}

	public static void dumpPtrHiArray(PrintStream out, String name, String ref, boolean allowNull, int scale, List<Integer> c) {
		dumpName(out, name);
		for (int i=0; i<c.size(); i++) {
			if ((i&31)==0)
				out.printf("\t.byte ");
			if ((c.get(i)==0) && (!allowNull))
				out.printf("$0000");
			else {
				if (scale==0)
					out.printf(">(%s+$%04x)", ref, c.get(i)&0xffff);
				else
					out.printf(">(%s+$%04x*%d)", ref, c.get(i)&0xffff, scale);
			}		
			if (((i&31)<31) && (i<c.size()-1))
				out.printf(",");
			else
				out.println();
		}
	}
}
