import java.awt.image.*;
import java.util.*;

class C64Char {
	public enum CharType {
		Empty,
		Bitmap,
		Sprite,
		UndeterminedChar,
		HiresChar,
		LoresChar
	}

	public static boolean multicolor = true;
	public static int pixelWidth = 1;
	public static ArrayList<Byte> spriteColors;
	public static byte fixedColors[] = null;
	public static byte defaultBackgroundColor = 0;
	public static boolean sortColors = true;
	public static boolean forceUnusedToBlack = false;
	public static CharType forcedType = CharType.Empty;
	public static Map<Integer, Integer> forceColorToIndex = null;

	public static void defaults() {
		multicolor = true;
		pixelWidth = 1;
		spriteColors = null;
		fixedColors = null;
		defaultBackgroundColor = 0;
		sortColors = true;
		forceUnusedToBlack = false;
		forcedType = CharType.Empty;
	}

	public CharType type;
	public byte d800;
	public byte screen;
	public ArrayList<Byte> data;
	public ArrayList<Byte> mask;

	public int hashCode() {
		return 0;
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof C64Char))
			return false;
		C64Char other = (C64Char)o;
		if ((data == null) && (other.data == null))
			return true;
		if (data == null)
			return false;
		if (other.data == null)
			return false;
		if (data.size() != other.data.size())
			return false;
		for (int i=0; i<data.size(); i++) {
			if (data.get(i) != other.data.get(i))
				return false;
		}
		return true;
	}

	public static CharType charType(int[] pixels, int stride, int cx, int cy) {
		// Determine type
		int emptyCount=0;
		int nonEmptyCount=0;
		for (int py=0; py<8; py++) {
			for (int px=0; px<4; px++) {
				int c = pixels[(cx*4+px)*pixelWidth + (cy*8+py)*stride];
				if (c>=16)
					emptyCount++;
				else
					nonEmptyCount++;
			}
		}

		if ((emptyCount==32) && (nonEmptyCount==0))
			return CharType.Empty;
		else if ((emptyCount==0) && (nonEmptyCount==32))
			return CharType.Bitmap;
		else
			return CharType.Sprite;
	}

	public static CharType charType(BufferedImage bitmap, int cx, int cy) {
		int pixels[] = bitmap.getSampleModel().getPixels(0,0,bitmap.getWidth(),bitmap.getHeight(),(int[])null, bitmap.getRaster().getDataBuffer());
		return charType(pixels, bitmap.getWidth(), cx, cy);
	}

	public C64Char(byte fillPattern) {
		data = new ArrayList<>();
		for (int i=0; i<8; i++)
			data.add(fillPattern);
	}
	
	public C64Char(BufferedImage bitmap, int cx, int cy) throws Exception {
		int pixels[] = bitmap.getSampleModel().getPixels(0,0,bitmap.getWidth(),bitmap.getHeight(),(int[])null, bitmap.getRaster().getDataBuffer());
		helper(bitmap, pixels, cx, cy);		
	}
	
	public C64Char(BufferedImage bitmap, int pixels[], int cx, int cy) throws Exception {
		helper(bitmap, pixels, cx, cy);
	}
	
	private void helper(BufferedImage bitmap, int pixels[], int cx, int cy) throws Exception {
		d800 = 0;
		screen = 0;
		data = new ArrayList<>();
		mask = new ArrayList<>();

		// Determine type
		type = forcedType == CharType.Empty ? charType(pixels, bitmap.getWidth(), cx, cy) : forcedType;
		if (type == CharType.UndeterminedChar) {
			// - Default char to lowres
			type = multicolor ? CharType.LoresChar : CharType.HiresChar;

			// - Scan each 2x1 to determine if colors are equal, if no on any pair => turn char hires
			if ((pixelWidth==1) && multicolor) {
				HashSet<Integer> colorUsage = new HashSet<>();
				outer:for (int py=0; py<8; py++) {
					for (int px=0; px<8; px+=2) {
						int c0 = pixels[cx*8 + px + 0 + (cy*8+py)*bitmap.getWidth()];
						int c1 = pixels[cx*8 + px + 1 + (cy*8+py)*bitmap.getWidth()];
						colorUsage.add(c0);
						colorUsage.add(c1);
						if (c0 != c1) {
							type = CharType.HiresChar;
							break outer;
						}
					}
				}

				// - If lores: If two colors only and match char restrictions => then turn char hires
				if ((type == CharType.LoresChar) && (colorUsage.size()<=2)) {
					Integer[] colors = colorUsage.toArray(new Integer[colorUsage.size()]);
					if (colors.length == 1) {
						if ((colors[0] == defaultBackgroundColor) || (colors[0]<8))
							type = CharType.HiresChar;
					} else {
						if (((colors[0] == defaultBackgroundColor) && (colors[1]<8)) ||
							((colors[1] == defaultBackgroundColor) && (colors[0]<8)))
							type = CharType.HiresChar;
					}
				}
			}

			// Generate color map
			HashMap<Integer,Integer> cmap = new HashMap<>();
			HashMap<Integer,Integer> rcmap = new HashMap<>();

			// Find $d800-color
			if (type == CharType.LoresChar) {
				if (fixedColors == null) {
					throw new Exception("A char has been determined multicolor. C64Char.fixedColors are assumed to be set for type CharType.LoresChar");
				}
				for (int i=0; i<fixedColors.length; i++) {
					cmap.put(fixedColors[i]&0xff, i);
					rcmap.put(i, fixedColors[i]&0xff);
				}

				outer:for (int py=0; py<8; py++) {
					for (int px=0; px<4; px++) {
						int c = pixels[(cx*4+px)*2 + (cy*8+py)*bitmap.getWidth()]&0xff;
						if (cmap.get(c)==null) {
							if (c<8) {
								cmap.put(c, 3);
								rcmap.put(3, c);
							}
							else {
								throw new Exception(String.format("$d800-color in char mode must be < 8, was %d (type=%s) @ %d,%d", c&0xff, type.name(), cx, cy));
							}
							break outer;
						}
					}
				}
			} else {
				if (fixedColors == null) {
					cmap.put(defaultBackgroundColor&0xff, 0);
					rcmap.put(0, defaultBackgroundColor&0xff);
				} else {
					cmap.put(fixedColors[0]&0xff, 0);
					rcmap.put(0, fixedColors[0]&0xff);					
				}

				outer:for (int py=0; py<8; py++) {
					for (int px=0; px<8; px++) {
						int c = pixels[(cx*8+px) + (cy*8+py)*bitmap.getWidth()]&0xff;
						if (cmap.get(c)==null) {
							if (c < (multicolor ? 8 : 16)) {
								cmap.put(c, 1);
								rcmap.put(1, c);
							}
							else {
								throw new Exception(String.format("$d800-color in char mode must be < %d, was %d (type=%s) @ %d,%d", multicolor ? 8 : 16, c&0xff, type.name(), cx, cy));
							}
						}
					}
				}
			}

			// Type and color map established. Generate the data
			if ( ((type == CharType.HiresChar) && (cmap.size()>2)) ||
				 ((type == CharType.LoresChar) && (cmap.size()>4)) ){
				System.out.println("Color map is: "+Arrays.toString(cmap.keySet().toArray(new Integer[cmap.size()])));
				throw new Exception(String.format("Too many colors at char %d,%d (type=%s)", cx,cy,type.name()));
			}
					
			// Fetch data
			if (type == CharType.HiresChar) {
				for (int py=0; py<8; py++) {
					byte cb = 0;
					for (int px=0; px<8; px++) {
						int c = pixels[(cx*8+px) + (cy*8+py)*bitmap.getWidth()];
						Integer ci = cmap.get(c);
						if (ci == null) {
							System.out.println("Color map is: "+Arrays.toString(cmap.keySet().toArray(new Integer[cmap.size()])));
							throw new Exception(String.format("Color %d not found in cmap @ %d,%d (type=%s).", c&0xff, cx, cy, type.name()));
						}
						cb |= (byte)(ci<<(7-px));
					}
					data.add(cb);
				}

				d800 = (byte)(rcmap.get(1) == null ? 0 : rcmap.get(1));
			} else if (type == CharType.LoresChar) {
				for (int py=0; py<8; py++) {
					byte cb = 0;
					for (int px=0; px<4; px++) {
						int c = pixels[(cx*4+px)*2 + (cy*8+py)*bitmap.getWidth()];
						Integer ci = cmap.get(c&0xff);
						if (ci == null) {
							System.out.println("Color map is: "+Arrays.toString(cmap.keySet().toArray(new Integer[cmap.size()])));
							throw new Exception(String.format("Color %d not found in cmap @ %d,%d (type=%s).", c&0xff, cx, cy, type.name()));
						}
						cb |= (byte)(ci<<((3-px)*2));
					}
					data.add(cb);
				}

				d800 = (byte)((rcmap.get(3) == null ? 8 : rcmap.get(3))|8);
			}
		}
		else if (type == CharType.Bitmap) {
			// Assign color usage
			int colors[] = new int[]{defaultBackgroundColor,-1,-1,-1};

			if (fixedColors != null) {
				for (int i=0; i<fixedColors.length; i++)
					colors[i] = fixedColors[i];
			} else {
				if (forceColorToIndex != null) {
					for (int py=0; py<8; py++) {
						for (int px=0; px<4; px++) {
							int c = pixels[(cx*4+px)*pixelWidth + (cy*8+py)*bitmap.getWidth()];
							Integer index = forceColorToIndex.get(c);
							if (index != null)
								colors[index] = c;
						}
					}	
				}

				abort:for (int py=0; py<8; py++) {
					for (int px=0; px<4; px++) {
						int c = pixels[(cx*4+px)*pixelWidth + (cy*8+py)*bitmap.getWidth()];
						if (c>=16) {
							break abort;
						}

						if ((forceColorToIndex == null) || (forceColorToIndex.get(c)==null)) {
							int ci=0;
							for (ci=0; ci<4; ci++) {
								if (colors[ci]==c)
									break;
								if (colors[ci]==-1) {
									colors[ci]=c;
									break;
								}
							}
							if (ci==4) {
								throw new Exception(String.format("Not koala restrictions. %d not in (%d,%d,%d,%d) in the bitmap layer at char %d,%d", c, colors[0], colors[1], colors[2], colors[3], cx,cy));
							}
						}
					}
					type = CharType.Bitmap;
				}

				if (forceUnusedToBlack) {
					for (int ci=0; ci<4; ci++)
						if (colors[ci]==-1)
							colors[ci]=127;
				}

				if (sortColors && (forceColorToIndex==null)) {
					int o[] = new int[]{colors[1], colors[2], colors[3]};
					Arrays.sort(o);
					for (int i=1; i<4; i++)
						colors[i] = o[i-1];
				}

				for (int ci=0; ci<4; ci++)
					if (colors[ci]==127)
						colors[ci]=0;
			}
	
			// Generate data array for the bitmap
			for (int py=0; py<8; py++) {
				byte cb = 0;
				for (int px=0; px<4; px++) {
					int c = pixels[(cx*4+px)*pixelWidth + (cy*8+py)*bitmap.getWidth()];
					int ci = 0;
					for (ci=0; ci<4; ci++) {
						if (colors[ci]==c)
							break;
					}
					if (ci==4) {
						throw new Exception("Sanity check. I fucked up in the code!");
					}
					cb |= (byte)(ci<<((3-px)*2));
				}
				data.add(cb);
			}

			screen = (byte)(((colors[1]&15)<<4) | (colors[2]&15));
			d800 = (byte)(colors[3]&15);
		} else if (type == CharType.Sprite) {
			byte cm=0;
			for (int py=0; py<8; py++) {
				byte cb=0;
				if ((py&1)==0)
					cm=(byte)255;
				for (int px=0; px<4; px++) {
					int ci = pixels[(cx*4+px)*pixelWidth + (cy*8+py)*bitmap.getWidth()];
					if (ci>=16) {
						; // Transparent, do nothing
						continue;
					}
					int c = spriteColors.indexOf((byte)ci);
					if (c==-1) {
						throw new Exception(String.format("Illegal sprite color %d in the sprite layer at char %d,%d", ci, cx,cy));
					}

					cb |= (byte)(c<<((3-px)*2));
					cm &= (byte)(1<<(7-(px+(py&1)*4))^0xff);
					type = CharType.Sprite;
				}
				data.add(cb);

				if ((py&1)==1)
					mask.add(cm);
			}
			screen = (byte)0x80;
			d800 = (byte)0x80;
		}
/*
		byte c=0;
		switch (type) {
		case Empty: c=0; break;
		case Bitmap:c=1; break;
		case Sprite:c=2; break;
		}
		for (int y=0; y<8; y++) {
			for (int x=0; x<4; x++) {
				if (((x+y)&1) == 0)
					pixels[cx*4+x + (cy*8+y)*bitmap.getWidth()] = c;
			}
		}*/
	}
}
