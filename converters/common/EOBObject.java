import java.awt.image.*;

class EOBObject{
	public byte width;
	public byte height;
	public byte dx;
	public byte dy;
	public C64Char c64Chars[];

	public EOBObject(BufferedImage bitmap, int cx0, int cy0, int cw, int ch) throws Exception {
		dx = 0;
		dy = 0;
		width = (byte)cw;
		height = (byte)ch;
		c64Chars = new C64Char[width*height];

		int count = 0;
		int emptyCount = 0;
		for (int cx=cx0; cx<cx0+cw; cx++)
			for (int cy=cy0; cy<cy0+ch; cy++) {
				c64Chars[count] = new C64Char(bitmap, cx,cy);
				if (c64Chars[count].type == C64Char.CharType.Empty)
					emptyCount++;
				count++;
			}

		if (emptyCount == count) {
			width = 0;
			height = 0;
			c64Chars = new C64Char[0];
		}
	}
}
