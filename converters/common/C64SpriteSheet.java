import java.io.*;
import java.awt.image.*;
import java.util.*;

public class C64SpriteSheet {
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;

	int direction = HORIZONTAL;
	ArrayList<C64Sprite> sprites = new ArrayList<>();
	ArrayList<Integer> mapping = new ArrayList<>();

	public C64SpriteSheet() {
		direction = HORIZONTAL;
	}

	public C64SpriteSheet(int direction) {
		this.direction = direction;
	}

	public C64SpriteSheet(BufferedImage bitmap, int dx, int dy, int sw, int sh, int[] commonColors, int[] uniqueColors) throws Exception {
		direction = HORIZONTAL;
		append(bitmap, dx, dy, sw, sh, commonColors, uniqueColors);
	}

	public void append(C64SpriteSheet other) {
		sprites.addAll(other.sprites);
		mapping.addAll(other.mapping);
	}

	public void append(BufferedImage bitmap, int dx, int dy, int sw, int sh, int[] commonColors, int[] uniqueColors) throws Exception {
		if (direction == HORIZONTAL) {
			for (int sy=0; sy<sh; sy++)
				for (int sx=0; sx<sw; sx++)
					appendHelper(bitmap, dx, dy, sw, sx, sy, commonColors, uniqueColors);
		} else {
			for (int sx=0; sx<sw; sx++)
				for (int sy=0; sy<sh; sy++)
					appendHelper(bitmap, dx, dy, sw, sx, sy, commonColors, uniqueColors);
		}
	}

	private boolean appendHelper(BufferedImage bitmap, int dx, int dy, int sw, int sx, int sy, int[] commonColors, int[] uniqueColors) throws Exception {
		int color = uniqueColors[sx+sy*sw];
		if (color < 0)
			return false;

		mapping.add(sprites.size());
		if ((color&0x80) == 0)
			sprites.add(new C64Sprite(bitmap, dx+sx*24, dy+sy*21, new int[]{commonColors[0], commonColors[1], commonColors[2], color}, true, false, false));	
		else
			sprites.add(new C64Sprite(bitmap, dx+sx*24, dy+sy*21, new int[]{commonColors[0], color&0x7f}, false, false, false));	

		return true;
	}

	public C64SpriteSheet withIntermediateImages(int sw, int sh) {
		if (direction != VERTICAL)
			return null;

		C64SpriteSheet sheet = new C64SpriteSheet(VERTICAL);

		for (int sx=0; sx<sw; sx++) {
			for (int sy=0; sy<sh; sy++) {
				C64Sprite sprite = sprites.get(sx*sh+sy);

				// Always emit the full sprite
				sheet.mapping.add(sprites.size());
				sheet.sprites.add(sprite);

				// If not last, then emit an intermediary
				if (sy==sh-1)
					continue;
				C64Sprite nextSprite = sprites.get(sx*sh+sy+1);
				C64Sprite indermediate = C64Sprite.intermediateFrom(sprite, nextSprite);
				sheet.mapping.add(sprites.size());
				sheet.sprites.add(indermediate);				
			}
		}
		return sheet;
	}

	public C64SpriteSheet optimize() {
		C64SpriteSheet newSheet = new C64SpriteSheet();
		HashMap<C64Sprite, Integer> uniqueSprites = new HashMap<>();

		for (int i=0; i<sprites.size(); i++) {
			C64Sprite sprite = sprites.get(i);
			Integer index = uniqueSprites.get(sprite);
			if (index == null) {
				index = uniqueSprites.size();
				uniqueSprites.put(sprite, index);
				newSheet.sprites.add(sprite);
			}
			newSheet.mapping.add(index);
		}

		return newSheet;
	}

	public void save(String filename) throws Exception {
		FileOutputStream fos = new FileOutputStream(filename);
		for (C64Sprite sprite : sprites)
			fos.write(sprite.data);
		fos.close();		
	}
}
