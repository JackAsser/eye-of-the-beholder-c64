import java.awt.image.*;
import java.util.*;
import java.io.*;

public class C64Bitmap {
	int width;
	int height;
	byte[] data;
	byte[] screen;
	byte[] d800;

	public C64Bitmap(BufferedImage bitmap, int cx, int cy, int cw, int ch) throws Exception {
		width = cw;
		height = ch;
		data = new byte[width*height*8];
		screen = new byte[width*height];
		d800 = new byte[width*height];

		C64Char.forcedType = C64Char.CharType.Bitmap;

		for (int y=0; y<ch; y++) {
			for (int x=0; x<cw; x++) {
				C64Char c = new C64Char(bitmap, x+cx, y+cy);
				for (int i=0; i<8; i++)
					data[x*8+y*cw*8+i] = c.data.get(i);
				screen[x+y*cw] = c.screen;
				d800[x+y*cw] = c.d800;
			}
		}
	}

	public void save(String baseName, boolean saveBitmap, boolean saveScreen, boolean saveD800) throws Exception {
		FileOutputStream fos;

		if (saveBitmap) {
			fos = new FileOutputStream(String.format("%s_bitmap.bin", baseName));
			fos.write(data);
			fos.close();
		}

		if (saveScreen) {
			fos = new FileOutputStream(String.format("%s_screen.bin", baseName));
			fos.write(screen);
			fos.close();
		}

		if (saveD800) {
			fos = new FileOutputStream(String.format("%s_d800.bin", baseName));
			fos.write(d800);
			fos.close();
		}	
	}
}
