import java.awt.image.*;
import java.util.*;

public class C64Sprite {
	byte data[];

	public C64Sprite() {
		data = new byte[64];
	}

	public static C64Sprite intermediateFrom(C64Sprite a, C64Sprite b) {
		C64Sprite intermediate = new C64Sprite();
		for (int y=0; y<21; y++) {
			C64Sprite src = y<10 ? b : a;
			for (int x=0; x<3; x++)
				intermediate.data[x+y*3] = src.data[x+y*3];		
			
		}
		return intermediate;
	}

	public C64Sprite(BufferedImage bitmap, int xp, int yp, int[] colors, boolean multicolor, boolean xExpanded, boolean yExpanded) throws Exception {
		int pixels[] = bitmap.getSampleModel().getPixels(0,0,bitmap.getWidth(),bitmap.getHeight(),(int[])null, bitmap.getRaster().getDataBuffer());

		data = new byte[64];

		HashMap<Integer, Integer> cmap = new HashMap<>();
		int bits[] = new int[]{0,1,3,2};
		for (int i=0; i<colors.length; i++)
			cmap.put(colors[i], bits[i]);

		for (int y=0; y<21; y++) {
			for (int cx=0; cx<3; cx++) {
				byte b = 0;

				if (multicolor) {
					for (int x=0; x<4; x++) {
						int sx = xp + (x*2+cx*8) * (xExpanded ? 2 : 1);
						int sy = yp + y * (yExpanded ? 2 : 1);
						int c = pixels[sx+sy*bitmap.getWidth()];
						Integer ci = cmap.get(c);
						if (ci == null)
							throw new Exception(String.format("Invalid color %d @ %d,%d", c, sx, sy));
						b |= ci<<((3-x)*2);
					}
				} else {
					for (int x=0; x<8; x++) {
						int sx = xp + (x+cx*8) * (xExpanded ? 2 : 1);
						int sy = yp + y * (yExpanded ? 2 : 1);
						int c = pixels[sx+sy*bitmap.getWidth()];
						Integer ci = cmap.get(c);
						if (ci == null)
							throw new Exception(String.format("Invalid color %d @ %d,%d", c, sx, sy));
						b |= ci<<(7-x);
					}
				}

				data[y*3+cx] = b;
			}
		}
	}

	public int hashCode() {
		return 0;
	}

	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof C64Sprite))
			return false;
		C64Sprite other = (C64Sprite)o;
		if ((data == null) && (other.data == null))
			return true;
		if (data == null)
			return false;
		if (other.data == null)
			return false;
		if (data.length != other.data.length)
			return false;
		for (int i=0; i<data.length; i++) {
			if (data[i] != other.data[i])
				return false;
		}
		return true;
	}
}
