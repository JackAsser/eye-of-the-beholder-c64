import java.awt.image.*;
import java.util.*;

public class C64Charset {
	private HashMap<C64Char, Byte> data;
	private HashMap<Integer, C64Char> rdata;

	public C64Charset() {
		data = new HashMap<>();
		rdata = new HashMap<>();
	}

	public byte add(C64Char ch) throws Exception {
		Byte b = data.get(ch);
		if (b==null) {
			int code = data.size();
			if (code >= 256) {
				throw new Exception("Charset limit exceeded");
			}
			data.put(ch, (byte)code);
			rdata.put(code, ch);
			return (byte)code;
		}
		return b;
	}

	public int size() {
		return data.size();
	}

	public byte[] data() {
		byte data[] = new byte[size()*8];
		for (int i=0; i<size(); i++) {
			ArrayList<Byte> ch = rdata.get(i).data;
			for (int j=0; j<8; j++)
				data[i*8+j] = ch.get(j);
		}
		return data;
	}
}
