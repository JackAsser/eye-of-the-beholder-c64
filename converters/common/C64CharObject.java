import java.awt.image.*;
import java.util.*;
import java.io.*;

public class C64CharObject {
	int width;
	int height;
	byte[] charset;
	byte[] screen;
	byte[] d800;

	private C64Charset cset = new C64Charset();

	public C64CharObject(int width, int height) {
		this.width = width;
		this.height = height;
		screen = new byte[width*height];
		d800 = new byte[width*height];
	}

	public void reserveChar(C64Char c) throws Exception {
		cset.add(c);
	}

	public void append(BufferedImage bitmap, int cx, int cy, int cw, int ch, int dx, int dy) throws Exception {
		C64Char.forcedType = C64Char.CharType.UndeterminedChar;

		for (int y=0; y<ch; y++) {
			for (int x=0; x<cw; x++) {
				C64Char c = new C64Char(bitmap, x+cx, y+cy);
				byte screenCode = cset.add(c);
				screen[dx+x+(dy+y)*width] = screenCode;
				d800[dx+x+(dy+y)*width] = c.d800;
			}
		}
		charset = cset.data();
	}

	public C64CharObject(BufferedImage bitmap, int cx, int cy, int cw, int ch) throws Exception {
		width = cw;
		height = ch;
		screen = new byte[width*height];
		d800 = new byte[width*height];
		append(bitmap, cx, cy, cw, ch, 0, 0);
	}

	public void save(String baseName, boolean saveCharset, boolean saveScreen, boolean saveD800) throws Exception {
		FileOutputStream fos;

		if (saveCharset) {
			fos = new FileOutputStream(String.format("%s_charset.bin", baseName));
			fos.write(charset);
			fos.close();
		}

		if (saveScreen) {
			fos = new FileOutputStream(String.format("%s_screen.bin", baseName));
			fos.write(screen);
			fos.close();
		}

		if (saveD800) {
			fos = new FileOutputStream(String.format("%s_d800.bin", baseName));
			fos.write(d800);
			fos.close();
		}	
	}
}
