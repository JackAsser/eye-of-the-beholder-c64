import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Declash {
	public static int colorBias[] = new int[]{
		100, //0
		100, //1
		100, //2
		100, //3
		100, //4
		100, //5
		100, //6
		100, //7
		100, //8
		100, //9
		100, //10
		100, //11
		100, //12
		100, //13
		100, //14
		100  //15
	};

   	public static boolean onlySprites = false;

	public static void declash(BufferedImage image) throws Exception {
		ArrayList<Byte> spriteColors = new ArrayList<>();
		spriteColors.add((byte)0);
		spriteColors.add((byte)9);
		spriteColors.add((byte)12);
		spriteColors.add((byte)15);
		Declash.declash(image, spriteColors);
	}

	public static void resetBias() {
		for (int i=0; i<colorBias.length; i++)
			colorBias[i] = 100;
	}

	public static void declash(BufferedImage image, ArrayList<Byte> spriteColors) throws Exception {
		int luma[] = new int[]{0,32,10,20,12,16,8,24,12,8,16,10,15,24,15,20};
		byte pixels[] = ((DataBufferByte)image.getRaster().getDataBuffer()).getData();
		int ipixels[] = image.getSampleModel().getPixels(0,0,image.getWidth(),image.getHeight(),(int[])null, image.getRaster().getDataBuffer());

		if (image.getSampleModel().getPixels(0,0,image.getWidth(),image.getHeight(),(int[])null, image.getRaster().getDataBuffer()).length != pixels.length)
			throw new Exception("Declash only supports one pixel per byte sample model.");

		for (int cy=0; cy<image.getHeight(); cy+=8) {
			for (int cx=0; cx<image.getWidth(); cx+=4*C64Char.pixelWidth) {
				C64Char.CharType type = C64Char.charType(ipixels, image.getWidth(), cx/(4*C64Char.pixelWidth), cy/8);

				if (onlySprites && (type==C64Char.CharType.Bitmap))
					type = C64Char.CharType.Sprite;

               	ArrayList<Byte> colors = new ArrayList<>();
               	if (type == C64Char.CharType.Sprite) {
               		for (int i=0; i<4; i++)
               			colors.add(spriteColors.get(i));
               	}
               	else if (type == C64Char.CharType.Bitmap) {
               		HashSet<Byte> uniqueColors = new HashSet<>();
               		uniqueColors.add(C64Char.defaultBackgroundColor);
                	for (int y=0; y<8; y++) {
						for (int x=0; x<4; x++) {
							byte c = pixels[cx+x*C64Char.pixelWidth+(cy+y)*image.getWidth()];
							uniqueColors.add(c);
						}
					}
					if (uniqueColors.size() > 4)
						System.out.printf("Declash warning: bitmap char at %dx%d contains more than four colors.\n", cx/(4*C64Char.pixelWidth), cy/8);
					else
						continue;

                	ArrayList<Map.Entry<Byte,Integer>> histogram = new ArrayList<>();
                	for (byte i=0; i<16; i++)
                		histogram.add(new AbstractMap.SimpleEntry<Byte,Integer>(i,(i==0)?1000:0));
                	for (int y=0; y<8; y++) {
						for (int x=0; x<4; x++) {
							byte c = pixels[cx+x*C64Char.pixelWidth+(cy+y)*image.getWidth()];
							histogram.get(c).setValue(histogram.get(c).getValue()+colorBias[c]);
						}
					}
					histogram.get(C64Char.defaultBackgroundColor).setValue(10000000);
					Collections.sort(histogram, (e1,e2) -> e2.getValue().compareTo(e1.getValue()));
					for (int i=0; i<4; i++)
						colors.add(histogram.get(i).getKey());
                }

				for (int y=0; y<8; y++) {
					for (int x=0; x<4; x++) {
						int c = pixels[cx+x*C64Char.pixelWidth+(cy+y)*image.getWidth()] & 0xff;
						if (colors.indexOf(c) != -1)
							continue;
						if (c>=16)
							continue;

						int targetLuma = luma[c];
						int bestC1=0;
						int bestC2=0;
						int bestDist=10000;
						for (int c1=0; c1<4; c1++) {
							for (int c2=0; c2<4; c2++) {
								int luma1 = luma[colors.get(c1)];
								int luma2 = luma[colors.get(c2)];
								if (Math.abs(luma1-luma2)>10)
									continue;
								int curLuma = (luma1+luma2)/2;
								int curDist = curLuma-targetLuma;
								curDist*=curDist;
								if (curDist < bestDist) {
									bestDist = curDist;
									bestC1 = c1;
									bestC2 = c2;
								}
							}
						}

						if ((x+y&1)==0)
							c = colors.get(bestC1);
						else
							c = colors.get(bestC2);

						for (int p=0; p<C64Char.pixelWidth; p++)
							pixels[cx+x*C64Char.pixelWidth+p+(cy+y)*image.getWidth()] = (byte)c;
					}
                }
			}
		}
	}

	public Declash(String inputName, String outputName, ArrayList<Byte> spriteColors) throws Exception {
		BufferedImage image = ImageIO.read(new File(inputName));
		Declash.declash(image, spriteColors);
		ImageIO.write(image, "PNG", new File(outputName));
	}

	public static void main(String[] args) {
		try {
			if (args.length < 7) {
				System.err.println("Usage: declash [-sprites] input.png output.png backgroundcol sprcol0 sprcol1 sprcol2 sprcol3");
				System.exit(1);
			}
			
			if (args[0].equals("-sprites")) {
				args = Arrays.copyOfRange(args, 1, args.length);
				onlySprites = true;
			}

			C64Char.defaultBackgroundColor = Byte.parseByte(args[2]);

			ArrayList<Byte> spriteColors = new ArrayList<>();
			for (int i=0; i<4; i++)
				spriteColors.add(Byte.parseByte(args[3+i]));

			new Declash(args[0], args[1], spriteColors);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
