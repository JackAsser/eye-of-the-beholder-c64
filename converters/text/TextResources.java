import java.io.*;
import java.util.*;

public class TextResources
{
	public static ArrayList<String> messages = new ArrayList<String>();
	
	public TextResources() throws IOException
	{
		File f = new File(Global.BASE+"text.dat");
		FileInputStream fis = new FileInputStream(f);
		byte data[] = new byte[(int)f.length()];
		fis.read(data);
		fis.close();
			
		int firstTextOffset=-1;
		int directoryOffset=0;
		while(directoryOffset!=firstTextOffset)
		{
			int textOffset = (((int)(data[directoryOffset++]))&0xff) | (((int)(data[directoryOffset++]<<8))&0xff00);

			if (firstTextOffset==-1)
				firstTextOffset = textOffset;
				
			StringBuilder sb = new StringBuilder();
			while(true)
			{
				char ch = (char)(data[textOffset++]&0xff);
				if (ch=='\0')
					break;
				if (ch=='\r')
					ch='\n';
				sb.append(ch);
			}
			messages.add(sb.toString());
		}
		messages.add("All of your party has been defeated.\n  The minions of evil will be able to carry out their plans unhindered!");

		PrintWriter out = new PrintWriter(new File("text.s"));
		out.println(".segment \"TEXTRESOURCES\"");
		out.println();

		int i=0;
		ArrayList<String> labels = new ArrayList<String>();
		for (String txt : messages) {
			txt = txt.replace("\"", "\",$22,\"");
			txt = txt.replace("\n", "\",$a,\"");
			String label = String.format("_%02d", i);
			labels.add(label);
			out.printf("%s: .byte \"%s\",0\n", label, txt);
			i++;
		}

		out.println();
		out.print("messages_lo: .byte ");
		for (i=0; i<labels.size(); i++) {
			if (i>0)
				out.printf(",");
			out.printf("<%s", labels.get(i));
		}

		out.println();
		out.print("messages_hi: .byte ");
                for (i=0; i<labels.size(); i++) {
                        if (i>0)
                                out.printf(",");
                        out.printf(">%s", labels.get(i));
                }
		out.println();
		out.println(".include \"../../text_resources.s\"");
		out.close();
	}

	public static void main(String[] args) {
		Global.BASE = "../../pcdata/";
		try {
			new TextResources();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
