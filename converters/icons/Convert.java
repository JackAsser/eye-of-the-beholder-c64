import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Convert {
	public static void convert() throws Exception {
		C64Char.spriteColors = new ArrayList<Byte>();
		C64Char.spriteColors.add((byte)16);
		C64Char.spriteColors.add((byte)9);
		C64Char.spriteColors.add((byte)1);
		C64Char.spriteColors.add((byte)15);
		C64Char.forcedType = C64Char.CharType.Sprite;

		BufferedImage bitmap = ImageIO.read(new File("maskedicons_spr.png"));

		FileOutputStream fos = new FileOutputStream("sprite_icons.bin");
		for (int i=0; i<89; i++) {
			int sy = i/20;
			int sx = i%20;
			for (int cy=0; cy<2; cy++) {
				for (int cx=0; cx<2; cx++) {
					C64Char ch = new C64Char(bitmap, sx*2+cx, sy*2+cy);
					for (byte b : ch.data)
						fos.write(b&0xff);
				}
			}
		}
		fos.close();
	}

	public static void main(String[] args) {
		try {
			convert();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
