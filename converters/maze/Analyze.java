import java.io.*;
import java.util.*;
import java.nio.*;

public class Analyze {
	private static int SAFE_MARGIN = 10;

	private static void analyze(int level) throws IOException {
		Inf inf = new Inf(level);

		byte[] maze = new byte[4096];
		FileInputStream fis = new FileInputStream(String.format("../../resources/LEVEL%d.MAZ", level));
		fis.read(maze);
		fis.close();

		HashSet<Integer> dynamicOffsets = new HashSet<>();

		for (int i=0; i<maze.length; i++) {
			int wallMappingIndex = maze[i]&0xff;
			WallMapping wm = inf.wallMappings.get(wallMappingIndex);
			if ((wm.flags&WallMapping.ISDOOR)!=0)
				dynamicOffsets.add(i);
			else if (wm.event == 3)
				dynamicOffsets.add(i);
			else if (wm.event == 4)
				dynamicOffsets.add(i);
		}

		for (AbstractToken token : inf.script.tokens) {
			if (token instanceof SetWallToken) {
				SetWallToken t = (SetWallToken)token;
				if (t.type == SetWallToken.TYPE_ALL_SIDES) {
					for (int i=0; i<4; i++)
						dynamicOffsets.add((t.pos.y*32+t.pos.x)*4+i);
				} else if (t.type == SetWallToken.TYPE_ONE_SIDE) {
					dynamicOffsets.add((t.pos.y*32+t.pos.x)*4+t.side);
				}
			} else if (token instanceof ChangeWallToken) {
				ChangeWallToken t = (ChangeWallToken)token;
				if (t.type == ChangeWallToken.TYPE_ALL_SIDES) {
					for (int i=0; i<4; i++)
						dynamicOffsets.add((t.pos.y*32+t.pos.x)*4+i);
				} else if (t.type == ChangeWallToken.TYPE_ONE_SIDE) {
					dynamicOffsets.add((t.pos.y*32+t.pos.x)*4+t.side);
				}
			}
		}

		List<Integer> sortedOffsets = new ArrayList<Integer>(dynamicOffsets);
		Collections.sort(sortedOffsets);

		int map[] = new int[4096];
		for (int offset : sortedOffsets)
			map[offset] = 1;

		ArrayList<Integer> rle = new ArrayList<>();
		int rleCount = 0;
		for (int i=0; i<4096; i++) {
			if (map[i] == 0) {
				rleCount++;
				if (rleCount==128) {
					rle.add(rleCount-1);
					rleCount=0;
				}
			}
			else {
				if (rleCount>0) {
					rle.add(rleCount-1);
					rleCount=0;
				}
				rle.add(0x80|map[i]);
			}
		}
		if (rleCount>0)
			rle.add(rleCount-1);
//007C39 -> 7039
		System.out.printf(".export storedMaze%02d\n", level);
		System.out.printf("storedMaze%02d:\n\t.res $%03x, $7f\n", level, rle.size()+SAFE_MARGIN);
		//System.out.printf("Level %2d contains %3d dynamic offsets. Max 0RLE size will be $%04x.\n", level, sortedOffsets.size(), rle.size());
	}

    public static void main(String[] args) {
    	try {
			Global.BASE = "../../../java/pcdata/";
			Global.byteOrder = ByteOrder.LITTLE_ENDIAN;

			for (int i=1; i<=12; i++)
				analyze(i);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
