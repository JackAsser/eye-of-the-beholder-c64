import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

class Shape {

}

class Screen {
	private int curPage = 0;
	private BufferedImage frameBuffer;
	private byte frameBufferPixels[];
	private byte pixels[][];

	public Screen(String palette) throws Exception {
		frameBuffer = ImageIO.read(new File(palette));
		frameBufferPixels = ((DataBufferByte)frameBuffer.getRaster().getDataBuffer()).getData();

		if (frameBuffer.getSampleModel().getPixels(0,0,frameBuffer.getWidth(),frameBuffer.getHeight(),(int[])null, frameBuffer.getRaster().getDataBuffer()).length != frameBufferPixels.length)
			throw new Exception("Not one pixel per byte sample model.");

		pixels = new byte[10][frameBufferPixels.length];
	}

	public void loadBitmap(String filename, int tempPage, int dstPage) throws Exception {
		BufferedImage img = ImageIO.read(new File(filename));
		byte[] tmp = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
		if (img.getSampleModel().getPixels(0,0,img.getWidth(),img.getHeight(),(int[])null, img.getRaster().getDataBuffer()).length != tmp.length)
			throw new Exception("Not one pixel per byte sample model.");
		System.arraycopy(tmp, 0 , pixels[dstPage], 0, tmp.length);
	}

	public void setCurPage(int page) {
		this.curPage = page;
	}

	public Shape encodeShape(int x, int y, int w, int h, boolean flags) {
		return null;
	}

	public BufferedImage updateScreen() {
		System.arraycopy(pixels[curPage], 0, frameBufferPixels, 0, frameBufferPixels.length);
		return frameBuffer;
	}

	public void fillRect(int x1, int y1, int x2, int y2, int color, int pageNum) {
		for (int y=y1; y<=y2; y++) {
			for (int x=x1; x<=x2; x++) {
				pixels[pageNum][x+y*320] = (byte)(color&0xff);
			}
		}
	}

	public void drawShape(int pageNum, Shape shapeData, int x, int y, int sd) {

	}
}

public class Player extends JFrame {
	private static final int ZOOM = 3;

	public Player() throws Exception {
		super("Player");

		Screen _screen = new Screen("originals/wtrdp2.gif");
		_screen.setCurPage(2);
		Shape shp1 = _screen.encodeShape(0, 140, 21, 60, true);
		Shape shp2 = _screen.encodeShape(21, 140, 12, 60, true);
		_screen.loadBitmap("originals/hands.gif", 3, 5);
		_screen.fillRect(0, 160, 319, 199, 12, 0);
		_screen.fillRect(0, 0, 191, 63, 157, 2);
		_screen.drawShape(2, shp1, 0, 4, 0);
		_screen.drawShape(2, shp2, 151, 4, 0);

		JPanel output = new JPanel() {
			public void paint(Graphics g) {
				g.drawImage(_screen.updateScreen(), 0, 0, 320*ZOOM, 200*ZOOM, this);
			}

			public Dimension getPreferredSize() {
				return new Dimension(320*ZOOM, 200*ZOOM);
			}
		};

		setLayout(new BorderLayout());
		setResizable(false);
		add(output, BorderLayout.CENTER);
		pack();
		setVisible(true);
		setDefaultCloseOperation(this.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		try {
			new Player();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
