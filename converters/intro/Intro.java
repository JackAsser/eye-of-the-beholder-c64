import javax.imageio.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;

public class Intro {
	static void convertLogo() throws Exception {
		BufferedImage bitmap;
                C64CharObject cobj;

                C64Char.defaults();
                C64Char.multicolor = true;
                C64Char.defaultBackgroundColor = 0xc;
                C64Char.pixelWidth = 2;
                C64Char.fixedColors = new byte[]{0xc, 0xf, 0x1};
                bitmap = ImageIO.read(new File("veto/eotb_logo_fadeframe.png"));
                cobj = new C64CharObject(bitmap, 0, 5, 40, 14);
                System.out.printf("* eotb_logo_fadeframe_top is 40x14 and uses %d unique chars.\n", cobj.charset.length/8);
                cobj.save("converted/eotb_logo_fadeframe_top", true, true, false);
                cobj = new C64CharObject(bitmap, 0, 5+14, 40, 2);
                System.out.printf("* eotb_logo_fadeframe_bottom is 40x2 and uses %d unique chars.\n", cobj.charset.length/8);
                cobj.save("converted/eotb_logo_fadeframe_bottom", true, true, false);

                bitmap = ImageIO.read(new File("veto/eotb_logo.png"));
                C64Char.defaults();
                C64Char.multicolor = true;
                C64Char.defaultBackgroundColor = 7;
                C64Char.pixelWidth = 2;

                new C64Bitmap(bitmap, 0,0, 40,25).save("converted/eotb_logo", true, true, true);
                System.out.printf("* eotb_logo is %dx%d.\n", bitmap.getWidth(), bitmap.getHeight());

		bitmap = ImageIO.read(new File("veto/eotb_logo_fade_sprites.png"));
		C64SpriteSheet sheet = new C64SpriteSheet(bitmap, 21, 0, 5, 7, new int[]{0x06,0x00,0x00}, new int[] {
			0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,
		});
		
		C64SpriteSheet optimizedSheet = sheet.optimize();
                optimizedSheet.save(String.format("converted/eotb_logo_fade_sprites.bin"));
                System.out.printf("* eotb_logo_fade_sprites uses %d(%d) sprites.\n", optimizedSheet.sprites.size(), sheet.sprites.size());

		FileOutputStream mapping = new FileOutputStream("converted/eotb_logo_fade_spritesmap.bin");
		for (Integer i : optimizedSheet.mapping)
			mapping.write((byte)((0xa800/0x40+i)&0xff));
		mapping.close();

		bitmap = ImageIO.read(new File("veto/eotb_logo_sprites.png"));
		sheet = new C64SpriteSheet(bitmap, 0, 0, 5, 6*3, new int[]{0x06,0x00,0x00}, new int[] {
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,

			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,

			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a,
			0x8a,0x8a,0x8a,0x8a,0x8a
		});

		optimizedSheet = sheet.optimize();
		optimizedSheet.save(String.format("converted/eotb_logo_sprites.bin"));
		System.out.printf("* eotb_logo_sprites uses %d(%d) sprites.\n", optimizedSheet.sprites.size(), sheet.sprites.size());

		int fadeSequence[] = new int[] {
			6,7,8,9,10,
			5,6,7,8,9,
			4,5,6,7,8,
			3,4,5,6,7,
			2,3,4,5,6,
			1,2,3,4,5
		};

		int empty = 4;

		int fadePointers[][]=new int[22][5*6];

		int f=0;
		for (int i=0; i<11; i++) {
			for (int j=0; j<5*6; j++) {
				if (f<fadeSequence[j])
					fadePointers[i][j] = empty;
				else if (f==fadeSequence[j]+0)
					fadePointers[i][j] = 5*6+j;
				else if (f==fadeSequence[j]+1)
					fadePointers[i][j] = j;
				else if (f==fadeSequence[j]+2)
					fadePointers[i][j] = j;
				else if (f==fadeSequence[j]+3)
					fadePointers[i][j] = 2*5*6+j;
				else 
					fadePointers[i][j] = empty;
			}
			f++;
		}

		f=0;
		for (int i=11; i<22; i++) {
			for (int j=0; j<5*6; j++) {
				if (f<fadeSequence[j])
					fadePointers[i][j] = empty;
				else if (f==fadeSequence[j]+0)
					fadePointers[i][j] = 5*6+j;
				else if (f==fadeSequence[j]+1)
					fadePointers[i][j] = j;
				else if (f==fadeSequence[j]+2)
					fadePointers[i][j] = 2*5*6+j;
				else 
					fadePointers[i][j] = empty;
			}
			f++;
		}

		mapping = new FileOutputStream("converted/eotb_logo_spritesmap.bin");
		for (int i=0; i<fadePointers.length; i++) {
		//	System.out.printf("%d:\n",i);
			for (int j=0; j<5*6; j++) {
				int ptr = optimizedSheet.mapping.get(fadePointers[i][j]);
		//		System.out.printf("%02x ", ptr);
		//		if ((j%5)==4)
		//			System.out.println();
				mapping.write((byte)((ptr+0x40)&0xff));
			}
			for (int j=0; j<5; j++) {
				int ptr = optimizedSheet.mapping.get(fadePointers[4][j]);
				mapping.write((byte)((ptr+0x40)&0xff));
			}
		}
		mapping.close();

/*		FileOutputStream mapping = new FileOutputStream("converted/intro_logo_sprites.map");
		for (Integer i : optimizedSheet.mapping)
			mapping.write((byte)(i&0xff));
		mapping.close();	
*/
	}

	static void convertCredits() throws Exception {
		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 6;
		C64Char.pixelWidth = 2;

		BufferedImage bitmap = ImageIO.read(new File("veto/intro_credits.png"));
		C64Bitmap tmp = new C64Bitmap(bitmap, 0,0, 40,25);

		int ramp[][] = new int[][] {
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x06,0x06, 0x06,0x0b,0x07,0x0f,0x07,0x01, 0x0d,0x03,0x0e,0x06},
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x06,0x06, 0x06,0x0b,0x07,0x0f,0x0f,0x07, 0x0f,0x0e,0x06,0x06},
			{0x06,0x06, 0x06,0x0b,0x07,0x0f,0x0a,0x08, 0x09,0x06,0x06,0x06},
			{0x06,0x06, 0x06,0x0b,0x0f,0x0a,0x08,0x09, 0x06,0x06,0x06,0x06},
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x06,0x06, 0x06,0x0b,0x07,0x0f,0x0f,0x0c, 0x06,0x06,0x06,0x06},
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x00,0x00, 0x00,0x00,0x00,0x00,0x00,0x00, 0x00,0x00,0x00,0x00},
			{0x06,0x06, 0x06,0x0b,0x07,0x0f,0x0f,0x0f, 0x0e,0x06,0x06,0x06}
		};

		HashMap<Integer, Integer> uniqueColors = new HashMap<>();
		for (int i=0; i<tmp.width*tmp.height; i++) {
			int d800 = tmp.d800[i]&0xf;
			int screen = tmp.screen[i]&0xff;
			if (!uniqueColors.containsKey(d800))
				uniqueColors.put(d800, uniqueColors.size());
			if (!uniqueColors.containsKey(screen))
				uniqueColors.put(screen, uniqueColors.size());
		}

		for (int i=0; i<1000; i++) {
			tmp.d800[i] = (byte)((int)uniqueColors.get(tmp.d800[i]&0xf)*12);
			tmp.screen[i] = (byte)((int)uniqueColors.get(tmp.screen[i]&0xff)*12);
		}
		tmp.save("converted/intro_credits", true, true, true);
		System.out.printf("* intro_credits is %dx%d.\n", bitmap.getWidth(), bitmap.getHeight());

		bitmap = ImageIO.read(new File("veto/intro_credits_cover_original.png"));
		tmp = new C64Bitmap(bitmap, 0,10,40,7);

		for (int i=0; i<tmp.width*tmp.height; i++) {
			int d800 = tmp.d800[i]&0xf;
			int screen = tmp.screen[i]&0xff;
			if (!uniqueColors.containsKey(d800))
				uniqueColors.put(d800, uniqueColors.size());
			if (!uniqueColors.containsKey(screen))
				uniqueColors.put(screen, uniqueColors.size());
		}

		for (int i=0; i<40*7; i++) {
			tmp.d800[i] = (byte)((int)uniqueColors.get(tmp.d800[i]&0xf)*12);
			tmp.screen[i] = (byte)((int)uniqueColors.get(tmp.screen[i]&0xff)*12);
		}
		tmp.save("converted/intro_credits_cover", true, true, true);
		System.out.printf("* intro_credits_cover is %dx%d.\n", tmp.width*8, tmp.height*8);

		int ramps[][] = new int[uniqueColors.size()][12];
		for (int r=0; r<12; r++) {
			for (HashMap.Entry<Integer, Integer> i : uniqueColors.entrySet()) {
				int colorA = (i.getKey()>>4)&15;
				int colorB = i.getKey()&15;
				colorA = ramp[colorA][r];
				colorB = ramp[colorB][r];
				int color = (colorA<<4)|colorB;
				ramps[i.getValue()][r] = color;
			}
		}

		for (int i=0; i<ramps.length; i++) {
			int t[] = ramps[i];
			System.out.printf(".byte ", i);
			for (int j=0; j<t.length; j++) {
				if (j!=0)
					System.out.printf(",");
				System.out.printf("$%02x", t[j]); 
			}
			System.out.printf(";%x\n",i);
		}

		bitmap = ImageIO.read(new File("veto/intro_credits_sprites.png"));
		C64SpriteSheet sheet = new C64SpriteSheet(bitmap, 0, 0, 8, 7, new int[]{0x06,0x00,0x00}, new int[] {
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80
		});

		bitmap = ImageIO.read(new File("veto/intro_credits_cover_sprites.png"));
		sheet.append(bitmap, 0, 0, 8, 2, new int[]{0x06,0x00,0x00}, new int[] {
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80,
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80
		});

		C64SpriteSheet optimizedSheet = sheet.optimize();
		optimizedSheet.save(String.format("converted/intro_credits_sprites.bin"));
		System.out.printf("* intro_credtis_sprites uses %d(%d) sprites.\n", optimizedSheet.sprites.size(), sheet.sprites.size());

		FileOutputStream mapping = new FileOutputStream("converted/intro_credits_spritesmap.bin");
		for (Integer i : optimizedSheet.mapping)
			mapping.write((byte)(i&0xff));
		mapping.close();
	}

	static void convertText() throws Exception {
		BufferedImage bitmap = ImageIO.read(new File("veto/intro_text.png"));

		C64Char.defaults();
		C64Char.multicolor = false;
		C64Char.defaultBackgroundColor = 0;
		C64Char.pixelWidth = 1;

		C64CharObject cobj = new C64CharObject(bitmap, 0,0, 40,25);
		System.out.printf("* intro_text_screen uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_text", true, true, false);
	}

	static void convertThroneRoom() throws Exception {
		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 8;
		C64Char.pixelWidth = 2;
	
		BufferedImage bitmap = ImageIO.read(new File("veto/intro_throneroom.png"));
		//Declash.declash(bitmap, null);
		
		new C64Bitmap(bitmap, 6, 0, 28, 23).save("converted/intro_throneroom", true, true, true);
		System.out.println("* intro_throneroom is 224x184.");

		bitmap = ImageIO.read(new File("veto/intro_throneroom_sprites.png"));

		C64SpriteSheet sheet = new C64SpriteSheet(C64SpriteSheet.VERTICAL);
		sheet.append(bitmap, 0, 0, 8, 5, new int[]{0x04,0x00,0x0c}, new int[] {
			0x06,0x09,0x06,0x0b,0x06,0x06,0x06,0x09,
			0x06,0x09,0x06,0x0b,0x06,0x06,0x06,0x09,
			0x06,0x09,0x06,0x0b,0x06,0x06,0x06,0x09,
			0x06,0x09,0x06,0x0b,0x06,0x06,0x06,0x09,
			0x06,0x09,0x06,0x0b,0x06,0x06,0x06,0x09
		});

		sheet = sheet.withIntermediateImages(8,5);

		C64SpriteSheet optimizedSheet = sheet.optimize();
		optimizedSheet.save(String.format("converted/intro_throneroom_sprites.bin"));
		System.out.printf("* intro_throneroom_sprites uses %d(%d) sprites.\n", optimizedSheet.sprites.size(), sheet.sprites.size());
		FileOutputStream mapping = new FileOutputStream("converted/intro_throneroom_sprites.map");
		for (Integer i : optimizedSheet.mapping)
			mapping.write((byte)(i&0xff));
		mapping.close();	
	}

	static void convertMageCircle() throws Exception {
		BufferedImage bitmap = ImageIO.read(new File("veto/intro_tower_magecircle.png"));

		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 0;
		C64Char.fixedColors = new byte[]{0x00, 0x08, 0x0e};
		C64Char.pixelWidth = 1;

		C64CharObject cobj = new C64CharObject(bitmap, 0,1, 18,12);
		System.out.printf("* intro_tower_magecircle is 18x12 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_tower_magecircle", true, true, true);
	}

	static void convertCityZoom() throws Exception {
		BufferedImage bitmap;
		C64CharObject cobj;

		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 0;
		C64Char.pixelWidth = 1;

		bitmap = ImageIO.read(new File("veto/intro_cityzoom_01-02.png"));
		cobj = new C64CharObject(20,17*2);
		C64Char.fixedColors = new byte[]{0x0b, 0x08, 0x0e};
		cobj.append(bitmap, 0,4,20,17, 0,0);
		cobj.append(bitmap, 20,4,20,17, 0,17);
		System.out.printf("* intro_cityzoom_01-02 is 20x34 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_cityzoom_01-02", true, true, true);

		bitmap = ImageIO.read(new File("veto/intro_cityzoom_03-04.png"));
		cobj = new C64CharObject(20,17*2);
		C64Char.fixedColors = new byte[]{0x09, 0x08, 0x0e};
		cobj.append(bitmap, 0,4,20,17, 0,0);
		C64Char.fixedColors = new byte[]{0x09, 0x08, 0x0e};
		cobj.append(bitmap, 20,4,20,17, 0,17);
		System.out.printf("* intro_cityzoom_03-04 is 20x34 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_cityzoom_03-04", true, true, true);

		bitmap = ImageIO.read(new File("veto/intro_cityzoom_05.png"));
		cobj = new C64CharObject(20,17);
		C64Char.fixedColors = new byte[]{0x09, 0x08, 0x0f};
		cobj.append(bitmap, 0,4,20,17, 0,0);
		System.out.printf("* intro_cityzoom_05 is 20x34 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_cityzoom_05", true, true, true);

		bitmap = ImageIO.read(new File("veto/intro_cityzoom_06-07.png"));
		cobj = new C64CharObject(20,17*2);
		C64Char.fixedColors = new byte[]{0x09, 0x08, 0x0f};
		cobj.append(bitmap, 0,4,20,17, 0,0);
		cobj.append(bitmap, 20,4,20,17, 0,17);
		System.out.printf("* intro_cityzoom_06-07 is 20x34 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_cityzoom_06-07", true, true, true);

		bitmap = ImageIO.read(new File("veto/intro_cityzoom_08-09.png"));
		cobj = new C64CharObject(20,17*2);
		C64Char.fixedColors = new byte[]{0x09, 0x08, 0x0f};
		cobj.append(bitmap, 0,0,1,1, 0,0);
		cobj.append(bitmap, 0,4,20,17, 0,0);
		cobj.append(bitmap, 20,4,20,17, 0,17);
		System.out.printf("* intro_cityzoom_08-09 is 20x34 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_cityzoom_08-09", true, true, true);

		bitmap = ImageIO.read(new File("veto/intro_cityzoom_overlaysprites.png"));
		C64Sprite cookieCutter = new C64Sprite(bitmap, 24, 0, new int[] {0xf,0x8}, false, false, false);
		FileOutputStream fos = new FileOutputStream("converted/intro_cityzoom_overlaysprites.bin");
		fos.write(cookieCutter.data);
		fos.close();

		bitmap = ImageIO.read(new File("veto/intro_castle_entrance_heroes_enter.png"));
		C64SpriteSheet sheet = new C64SpriteSheet(bitmap, 0, 0, 5, 3, new int[] {0x8,0x1,0xa}, new int[] {
			0x05,0x80,0x8e,0x89,0x89,
			0x05,0x80,0x8e,0x89,0x89,
			0x05,0x80,0x8e,0x89,0x89
		});
		sheet.save("converted/intro_castle_entrance_heroes_enter.bin");

		bitmap = ImageIO.read(new File("veto/intro_castle_entrance_heroes_exit.png"));
		sheet = new C64SpriteSheet(bitmap, 0, 0, 5, 3, new int[] {0x8,0x1,0xa}, new int[] {
			0x05,0x80,0x8e,0x89,0x89,
			0x05,0x80,0x8e,0x89,0x89,
			0x05,0x80,0x8e,0x89,0x89
		});
		sheet.save("converted/intro_castle_entrance_heroes_exit.bin");
	}

	static void convertTowerSprites() throws Exception {
		BufferedImage bitmap = ImageIO.read(new File("veto/intro_tower_spritesheet.png"));

		C64SpriteSheet sheet = new C64SpriteSheet(bitmap, 0, 0, 8, 11, new int[]{0x04,0x0c,0x0f}, new int[] {
			0x0b,0x01,0x01,0x01,0x01,0x01,  -1,  -1,
			0x0b,0x01,0x0b,0x0b,0x01,0x01,  -1,  -1,
			0x0b,0x01,0x01,0x01,0x01,0x01,  -1,  -1,
			0x0b,0x01,0x01,0x01,0x01,0x01,  -1,  -1,
			0x0b,0x01,0x01,0x0b,0x01,0x01,  -1,  -1,
			0x01,0x06,0x86,0x86,0x00,0x01,  -1,  -1,
			0x01,0x0e,  -1,  -1,0x86,0x01,  -1,  -1,
			0x01,0x0e,  -1,  -1,0x86,0x01,  -1,  -1,
			0x01,0x0e,0x0e,0x06,0x06,0x01,0x80,0x80,
			0x01,0x01,0x01,0x01,0x01,0x01,0x80,  -1,
			0x0b,0x0b,0x0b,0x0b,0x01,0x01,0x80,  -1
		});
		sheet.save("converted/intro_tower_spritesheet.bin");

		System.out.printf("* intro_tower_spritesheet uses %d sprites.\n", sheet.sprites.size());
	}

	static void convertIntroHandsKnife() throws Exception {
		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 8;
		C64Char.pixelWidth = 2;

		BufferedImage bitmap = ImageIO.read(new File("veto/intro_hands_knife_bitmap.png"));
		new C64Bitmap(bitmap, 1, 1, 168/8,64/8).save("converted/intro_hands_knife", true, true, true);
		System.out.println("* intro_hands_knife_bitmap is 168x64.");

		bitmap = ImageIO.read(new File("veto/intro_hands_knife_sprites.png"));

		C64SpriteSheet sheet = new C64SpriteSheet(C64SpriteSheet.VERTICAL);
		sheet.append(bitmap, 0,0, 4,3, new int[]{0x4,0x8,0x9}, new int[] {
			0x00,0x0a,0x0a,0x0a,
			0x00,0x0a,0x0a,0x0a,
			0x00,0x00,0x00,0x00
		});

		sheet = sheet.withIntermediateImages(4,3);

		sheet.append(bitmap, 24*4,0, 2,3, new int[]{0x4,0x8,0x9}, new int[] {
			0x0f,0x0f,
			0x0f,0x00,
			0x0b,  -1
		});
		sheet.save("converted/intro_hands_knife_sprites.bin");

		System.out.printf("* intro_hands_knife_sprites uses %d sprites.\n", sheet.sprites.size());
	}

	static void convertIntroHandsSword() throws Exception {
		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 8;
		C64Char.pixelWidth = 2;

		BufferedImage bitmap = ImageIO.read(new File("veto/intro_hands_sword_bitmap.png"));
		new C64Bitmap(bitmap, 0, 0, 144/8,136/8).save("converted/intro_hands_sword", true, true, true);
		System.out.println("* intro_hands_sword_bitmap is 144x136.");

		bitmap = ImageIO.read(new File("veto/intro_hands_sword_sprites.png"));

		C64SpriteSheet sheet = new C64SpriteSheet(C64SpriteSheet.VERTICAL);
		sheet.append(bitmap, 0,0, 4,3, new int[]{0x4,0x7,0x8}, new int[] {
			0x0f,0x0f,0x0c,0x0c,
			0x0c,0x0f,0x0c,0x0c,
			0x0c,0x09,0x0c,0x0c
		});

		sheet = sheet.withIntermediateImages(4,3);

		sheet.append(bitmap, 24*4,21, 1,2, new int[]{0x4,0x7,0x8}, new int[] {
			0x01,
			0x01
		});

		sheet.save("converted/intro_hands_sword_sprites.bin");

		System.out.printf("* intro_hands_sword_sprites uses %d sprites.\n", sheet.sprites.size());
	}

	static void convertIntroHandsPray() throws Exception {
		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 8;
		C64Char.pixelWidth = 2;

		BufferedImage bitmap = ImageIO.read(new File("veto/intro_hands_pray_bitmap.png"));
		new C64Bitmap(bitmap, 0, 0, 80/8,160/8).save("converted/intro_hands_pray", true, true, true);
		System.out.println("* intro_hands_pray_bitmap is 80x16.");

		bitmap = ImageIO.read(new File("veto/intro_hands_pray_sprites.png"));

		C64SpriteSheet sheet = new C64SpriteSheet(C64SpriteSheet.VERTICAL);
		sheet.append(bitmap, 0,0, 4,2, new int[]{0x4,0xc,0xf}, new int[] {
			0x0b,0x01,0x01,0x01,
			0x0b,0x01,0x01,0x01
		});
		sheet = sheet.withIntermediateImages(4,2);
		sheet.save("converted/intro_hands_pray_sprites.bin");

		System.out.printf("* intro_hands_pray_sprites uses %d sprites.\n", sheet.sprites.size());
	}

	static void convertIntroHandsSpellbook() throws Exception {
		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 8;
		C64Char.pixelWidth = 2;

		BufferedImage bitmap = ImageIO.read(new File("veto/intro_hands_spellbook_bitmap.png"));
		new C64Bitmap(bitmap, 0, 0, 128/8,200/8).save("converted/intro_hands_spellbook", true, true, true);
		System.out.println("* intro_hands_spellbook is 128x200.");
	}

	static void convertIntroSphere() throws Exception {
		int[] commonColors = new int[]{
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
		};

		int spriteBase[] = new int[]{
			0xf200, 0xe000, 0xac00, 0x6400
		};
		int spriteBaseTunnel[] = new int[]{
			0xc000, 0xe000, 0xa800, 0x6000
		};
		for (int i=0; i<=3; i++) {
			C64SpriteSheet sheet = new C64SpriteSheet(C64SpriteSheet.VERTICAL);
			BufferedImage bitmap = ImageIO.read(new File(String.format("veto/intro_sphere_%d_sprites.png",i)));
			sheet.append(bitmap, 0, 0, 8, 9, new int[]{0x04,0x06,0x0e}, commonColors);
			sheet = sheet.withIntermediateImages(8,9);
			C64SpriteSheet optimizedSheet = sheet.optimize();

			optimizedSheet.save(String.format("converted/intro_sphere_%d_sprites.bin", i));
			System.out.printf("* intro_sphere_%d_sprites uses %d(%d) sprites $%04x.\n", i, optimizedSheet.sprites.size(), sheet.sprites.size(), optimizedSheet.sprites.size()*64);
			FileOutputStream mapping = new FileOutputStream(String.format("converted/intro_sphere_%d_sprites.map",i));
			for (Integer j : optimizedSheet.mapping) {
				int offset = j + spriteBase[i]/0x40;
				mapping.write((byte)(offset&0xff));
			}
			mapping.close();
			mapping = new FileOutputStream(String.format("converted/intro_sphere_%db_sprites.map",i));
			for (Integer j : optimizedSheet.mapping) {
				int offset = j + spriteBaseTunnel[i]/0x40;
				mapping.write((byte)(offset&0xff));
			}
			mapping.close();
		}
	}

	static void convertTunnel() throws Exception {
		BufferedImage bitmap;
		C64CharObject cobj;

		C64Char.defaults();
		C64Char.multicolor = true;
		C64Char.defaultBackgroundColor = 9;
		C64Char.pixelWidth = 1;

		C64Char emptyChar = new C64Char((byte)0xff);
		C64Char grayChar = new C64Char((byte)0xaa);

		bitmap = ImageIO.read(new File("veto/intro_tunnel_x1_move_01_04.png"));
		cobj = new C64CharObject(20,15*2);
		cobj.reserveChar(emptyChar);
		C64Char.fixedColors = new byte[]{0x09, 0x00, 0x0c};
		cobj.append(bitmap,  0,5,20,15, 0,0);
		cobj.append(bitmap, 20,5,20,15, 0,15);
		System.out.printf("* intro_tunnel_x1_move_01_04 is 2x20x15 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_tunnel_x1_move_01_04", true, true, true);

		bitmap = ImageIO.read(new File("veto/intro_tunnel_x1_move_02_05.png"));
		cobj = new C64CharObject(20,15*2);
		cobj.reserveChar(emptyChar);
		C64Char.fixedColors = new byte[]{0x09, 0x00, 0x0c};
		cobj.append(bitmap,  0,5,20,15, 0,0);
		cobj.append(bitmap, 20,5,20,15, 0,15);
		System.out.printf("* intro_tunnel_x1_move_02_05 is 2x20x15 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_tunnel_x1_move_02_05", true, true, true);

		bitmap = ImageIO.read(new File("veto/intro_tunnel_x1_move_03_06.png"));
		cobj = new C64CharObject(20,15*2);
		cobj.reserveChar(emptyChar);
		C64Char.fixedColors = new byte[]{0x09, 0x00, 0x0c};
		cobj.append(bitmap,  0,5,20,15, 0,0);
		cobj.append(bitmap, 20,5,20,15, 0,15);
		System.out.printf("* intro_tunnel_x1_move_03_06 is 2x20x15 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_tunnel_x1_move_03_06", true, true, true);

		for (int i=7; i<=18; i++) {
			bitmap = ImageIO.read(new File(String.format("veto/intro_tunnel_x1_move_%02d.png", i)));
			cobj = new C64CharObject(20,15);
			cobj.reserveChar(emptyChar);
			C64Char.fixedColors = new byte[]{0x09, 0x00, 0x0c};
			cobj.append(bitmap,  0,5,20,15, 0,0);
			System.out.printf("* intro_tunnel_x1_move_%02d is 20x15 and uses %d unique chars.\n", i, cobj.charset.length/8);
			cobj.save(String.format("converted/intro_tunnel_x1_move_%02d", i), true, true, true);
		}

		for (int i=1; i<=10; i++) {
			try {
				bitmap = ImageIO.read(new File(String.format("veto/intro_tunnel_x2_rotation_%02d.png", i)));
				cobj = new C64CharObject(20,15);
				cobj.reserveChar(emptyChar);
				C64Char.fixedColors = new byte[]{0x09, 0x00, 0x0c};
				cobj.append(bitmap,  0,5,20,15, 0,0);
				System.out.printf("* intro_tunnel_x2_rotation_%02d is 20x15 and uses %d unique chars.\n", i, cobj.charset.length/8);
				cobj.save(String.format("converted/intro_tunnel_x2_rotation_%02d", i), true, true, true);
			} catch (Exception e) {
				System.out.printf("* Skipping intro_tunnel_x2_rotation_%02d: %s\n", i, e.getMessage());
			}
		}

		bitmap = ImageIO.read(new File("veto/intro_tunnel_x3_door.png"));
		cobj = new C64CharObject(20,15*2);
		cobj.reserveChar(emptyChar);
		C64Char.fixedColors = new byte[]{0x09, 0x00, 0x0c};
		cobj.append(bitmap,  0,5,20,15, 0,0);
		cobj.append(bitmap, 20,5,20,15, 0,15);
		System.out.printf("* intro_tunnel_x3_door is 2x20x15 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_tunnel_x3_door", true, true, true);

		for (int i=1; i<=4; i++) {
			try {
				bitmap = ImageIO.read(new File(String.format("veto/intro_tunnel_x4_rockfall_%02d.png", i)));
				cobj = new C64CharObject(20,15);
				cobj.reserveChar(emptyChar);
				C64Char.fixedColors = new byte[]{0x09, 0x00, 0x0c};
				cobj.append(bitmap,  0,5,20,15, 0,0);
				System.out.printf("* intro_tunnel_x4_rockfall_%02d is 20x15 and uses %d unique chars.\n", i, cobj.charset.length/8);
				cobj.save(String.format("converted/intro_tunnel_x4_rockfall_%02d", i), true, true, true);
			} catch (Exception e) {
				System.out.printf("* Skipping intro_tunnel_x4_rockfall_%02d: %s\n", i, e.getMessage());
			}
		}

		bitmap = ImageIO.read(new File("veto/intro_tunnel_x4_rockfall_05_06.png"));
		cobj = new C64CharObject(20,15*2);
		cobj.reserveChar(emptyChar);
		cobj.reserveChar(grayChar);
		C64Char.fixedColors = new byte[]{0x09, 0x00, 0x0c};
		cobj.append(bitmap,  0,5,20,15, 0,0);
		cobj.append(bitmap, 20,5,20,15, 0,15);
		System.out.printf("* intro_tunnel_x4_rockfall_05_06 is 2x20x15 and uses %d unique chars.\n", cobj.charset.length/8);
		cobj.save("converted/intro_tunnel_x4_rockfall_05_06", true, true, true);

		bitmap = ImageIO.read(new File("veto/intro_tunnel_sprites.png"));
		C64SpriteSheet sheet = new C64SpriteSheet(bitmap, 0, 0, 8, 2, new int[]{0x4,0x9,0xc}, new int[] {
			0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
			0x80,0x80,0x80,0x80,0x80,0x80,0x80,0x80
		});
		sheet.save("converted/intro_tunnel_sprites.bin");
		System.out.printf("* intro_tunnel_sprites uses %d sprites.\n", sheet.sprites.size());
	}

	public static void main(String[] args) {
		try {
			convertLogo();
			convertCredits();
			convertText();
			convertThroneRoom();
			convertMageCircle();
			convertCityZoom();
			convertIntroSphere();
			convertTowerSprites();
			convertIntroHandsKnife();
			convertIntroHandsSword();
			convertIntroHandsPray();
			convertIntroHandsSpellbook();
			convertTunnel();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
