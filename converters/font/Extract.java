import java.io.*;

public class Extract {
	public Extract() throws IOException {
		EobFont font = new EobFont("../../../java/pcdata/font8.fnt");
		
		FileOutputStream fos = new FileOutputStream("font8.bin");
		fos.write(font.data);
		fos.close();
		
		PrintStream out = new PrintStream(new FileOutputStream("font8.inc"));
		for (int i=0; i<128; i++)
			out.printf(".charmap $%02x, $%02x\n", i, font.offsets[i]/8);
		out.close();
	}

	public static void main(String[] args) {
	    	try {
    			new Extract();
	    	} catch (Exception e) {
    			e.printStackTrace();
	    	}
	}
}
