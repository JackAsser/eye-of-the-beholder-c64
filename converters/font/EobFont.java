import java.io.*;
import java.nio.*;
import java.nio.channels.*;

class EobFont {
	public int offsets[];
	public int width;
	public int height;
	public byte data[];

	public EobFont(String fileName) throws IOException {
		FileInputStream fis = new FileInputStream(fileName);
		FileChannel fc = fis.getChannel();
		MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
		mbb.order(ByteOrder.LITTLE_ENDIAN);
		
		int filesize = mbb.getShort()&0xffff;
		offsets = new int[128];
		for (int i=0; i<128; i++)
			offsets[i] = (mbb.getShort()&0xffff) - 260;
		
		width = mbb.get()&0xff;
		height = mbb.get()&0xff;
		
		data = new byte[128*height];
		mbb.get(data);
		
		fis.close();
	}
}
