.proc mouse_joy
		lda #1
		sta mouseConnected
		lda $dc00
		sta tmp

		lda #0
		sta dx+0
		sta dy+0
		sta dx+1
		sta dy+1

		lsr tmp
		bcs :+
			;up
			lda #<4
			sta dy+0
			lda #>4
			sta dy+1
		:

		lsr tmp
		bcs :+
			;down
			lda #<-4
			sta dy+0
			lda #>-4
			sta dy+1
		:

		lsr tmp
		bcs :+
			;right
			lda #<-4
			sta dx+0
			lda #>-4
			sta dx+1
		:

		lsr tmp
		bcs :+
			;left
			lda #<4
			sta dx+0
			lda #>4
			sta dx+1
		:

		rts
.endproc

.proc buttons_joy
		lda #$00
		sta tmp
		lda $dc00
		and #$10
		bne :+
			lda tmp
			ora #1
			sta tmp
		:

		lda $d419 ;c0=released, ff=pressed
		cmp #$ff
		bne :+
			lda tmp
			ora #2
			sta tmp
		:

		lda tmp
		rts
.endproc
