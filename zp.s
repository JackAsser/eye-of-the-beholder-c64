; Most ZP variables are temporary and none is used in IRQ
; Those marked as resident must be restored after kernel usage

.segment "ZP": zeropage

BANK: 			.res 1
.exportzp BANK

SRC:			.res 2
.exportzp SRC
.exportzp SRC_DATA = SRC
.exportzp SRC_DATA_hi = SRC+1
.exportzp SRC_BITMAP = SRC

DST:			.res 2
.exportzp DST
.exportzp DST_BITMAP = DST
.exportzp DST_BITMAP_hi = DST+1

SRC2:			.res 2
.exportzp SRC2
.exportzp SRC2_hi = SRC2+1

DST2:			.res 2
.exportzp DST2
.exportzp DST_BITMAP_USE = DST2
.exportzp DST_BITMAP_USE_hi = DST2+1

MSRC:			.res 2
.exportzp MSRC
.exportzp MSRC_hi = MSRC+1

MDST:			.res 2
.exportzp MDST
.exportzp MDST_hi = MDST+1

TMP:			.res 2
.exportzp TMP
.exportzp TMP_hi = TMP+1

TMP2:			.res 2
.exportzp TMP2
.exportzp TMP2_hi = TMP2+1

TMP3:			.res 2
.exportzp TMP3
.exportzp TMP3_hi = TMP3+1
.exportzp DAMAGE = TMP3

DST3:			.res 2
.exportzp DST3
.exportzp DST_SCREEN = DST3
.exportzp DST_SCREEN_hi = DST3+1

DST4:			.res 2
.exportzp DST4
.exportzp DST_D800 = DST4
.exportzp DST_D800_hi = DST4+1

SRC3:			.res 2
.exportzp SRC3
.exportzp SRC_SCREEN = SRC3
.exportzp SRC_SCREEN_hi = SRC3+1


SRC4:			.res 2
.exportzp SRC4
.exportzp SRC_D800 = SRC4
.exportzp SRC_D800_hi = SRC4+1

INDJMPOP:		.res 1			; Resident
.exportzp INDJMPOP
INDJMP:			.res 2
.exportzp INDJMP
.exportzp INDJMP_hi = INDJMP+1

ICONINDEX:		.res 1
.exportzp ICONINDEX

WIDTH:			.res 1
.exportzp WIDTH

HEIGHT:			.res 1
.exportzp HEIGHT

SCAN:			.res 1
.exportzp SCAN

ROW:
WALLX0:			.res 1
.exportzp ROW
.exportzp WALLX0

COL:
WALLX1:			.res 1
.exportzp COL
.exportzp WALLX1

INNERSCAN:		.res 1
.exportzp INNERSCAN

SCANINDEX:		.res 1
.exportzp SCANINDEX

RASTER_IRQ:		.res 2			; Resident
.exportzp RASTER_IRQ
.exportzp RASTER_IRQ_hi = RASTER_IRQ+1

YREG:			.res 1
.exportzp YREG

DST5:			.res 2
.exportzp DST5
.exportzp DST_SPRITES = DST5
.exportzp DST_SPRITES_hi = DST5+1

DST6:			.res 2
.exportzp DST6
.exportzp DST_SPRITES_USE = DST6
.exportzp DST_SPRITES_USE_hi = DST6+1
 
SHOULDRENDER:		.res 1		; Resident
.exportzp SHOULDRENDER

SHOULDEVALUATETIMERS:	.res 1	; Resident
.exportzp SHOULDEVALUATETIMERS

DIDTELEPORT:		.res 1
.exportzp DIDTELEPORT

INTRO_IRQ_TMP:
.exportzp INTRO_IRQ_TMP

YPOS:			.res 1
.exportzp YPOS

wallFrontScanIndex:	.res 1
.exportzp wallFrontScanIndex

wallSideScanIndex:	.res 1
.exportzp wallSideScanIndex

wallSideDirectionSideIndex:	.res 1
.exportzp wallSideDirectionSideIndex

XPOS:			.res 1
.exportzp XPOS

DICE_ROLLS:		.res 1
.exportzp DICE_ROLLS

DICE_SIDES:		.res 1
.exportzp DICE_SIDES

DICE_BASE:		.res 1
.exportzp DICE_BASE

DICE_RESULT:		.res 2
.exportzp DICE_RESULT
.exportzp DICE_RESULT_hi = DICE_RESULT+1

NUM1:			.res 2
.exportzp NUM1
.exportzp NUM1_hi = NUM1+1

NUM2:			.res 1
.exportzp NUM2

DXTAB:			.res 2
.exportzp DXTAB
.exportzp DXTAB_hi = DXTAB+1

DYTAB:			.res 2
.exportzp DYTAB
.exportzp DYTAB_hi = DYTAB+1

CUR_ITEM:		.res 2
.exportzp CUR_ITEM
.exportzp CUR_ITEM_hi = CUR_ITEM+1

TOP_ITEM:		.res 2
.exportzp TOP_ITEM
.exportzp TOP_ITEM_hi = TOP_ITEM+1

PREV_ITEM:		.res 2
.exportzp PREV_ITEM
.exportzp PREV_ITEM_hi = PREV_ITEM+1

LEFT_ITEM:		.res 2
.exportzp LEFT_ITEM
.exportzp LEFT_ITEM_hi = LEFT_ITEM+1

END_ITEM:		.res 2
.exportzp END_ITEM
.exportzp END_ITEM_hi = END_ITEM+1

CUR_REGION:		.res 2
.exportzp CUR_REGION
.exportzp CUR_REGION_hi = CUR_REGION+1

POINTER_ITEM:		.res 2
.exportzp POINTER_ITEM
.exportzp POINTER_ITEM_hi = POINTER_ITEM+1

NEXT_ITEM:		.res 2
.exportzp NEXT_ITEM
.exportzp NEXT_ITEM_hi = NEXT_ITEM+1

RIGHT_ITEM:		.res 2
.exportzp RIGHT_ITEM
.exportzp RIGHT_ITEM_hi = RIGHT_ITEM+1

CURRENT_THROWN:		.res 2
.exportzp CURRENT_THROWN
.exportzp CURRENT_THROWN_hi = CURRENT_THROWN+1

CURRENT_TIMER:		.res 2
.exportzp CURRENT_TIMER
.exportzp CURRENT_TIMER_hi = CURRENT_TIMER+1

CURRENT_PARTYMEMBER:	.res 2
.exportzp CURRENT_PARTYMEMBER
.exportzp CURRENT_PARTYMEMBER_hi = CURRENT_PARTYMEMBER+1

CURRENT_PARTYMEMBER_INDEX:	.res 1
.exportzp CURRENT_PARTYMEMBER_INDEX

INV_ITEM:		.res 2
.exportzp INV_ITEM
.exportzp INV_ITEM_hi = INV_ITEM+1

TRIGGER_POSITION:	.res 2
.exportzp TRIGGER_POSITION
.exportzp TRIGGER_POSITION_hi = TRIGGER_POSITION+1

TRIGGER_MASK:		.res 1
.exportzp TRIGGER_MASK

SCRIPT_DONE:		.res 1
.exportzp SCRIPT_DONE

INF_POINTER:		.res 2
.exportzp INF_POINTER
.exportzp INF_POINTER_hi = INF_POINTER+1

CUR_CHAR:		.res 1
.exportzp CUR_CHAR

REGION_X0:		.res 2
.exportzp REGION_X0
.exportzp REGION_X0_hi = REGION_X0+1

REGION_X1:		.res 2
.exportzp REGION_X1
.exportzp REGION_X1_hi = REGION_X1+1

REGION_Y0:		.res 1
.exportzp REGION_Y0

REGION_Y1:		.res 1
.exportzp REGION_Y1

SP_ENTRY:		.res 1
.exportzp SP_ENTRY

CALLSTACK_PTR:		.res 1
.exportzp CALLSTACK_PTR

LAST_INF_POINTER:	.res 2
.exportzp LAST_INF_POINTER
.exportzp LAST_INF_POINTER_hi = LAST_INF_POINTER+1

MAZEPOSITION:		.res 2
.exportzp MAZEPOSITION
.exportzp MAZEPOSITION_hi = MAZEPOSITION+1

XREG:			.res 1
.exportzp XREG

ATTACKING_PARTYMEMBER_INDEX:	.res 1
.exportzp ATTACKING_PARTYMEMBER_INDEX

ATTACKING_HAND_INDEX:	.res 1
.exportzp ATTACKING_HAND_INDEX

MONSTERS_ON_LOC:	.res 5
.exportzp MONSTERS_ON_LOC

DRAW_MONSTER_HIT:	.res 1
.exportzp DRAW_MONSTER_HIT

INFBANK:		.res 1
.exportzp INFBANK					; Resident

TARGETED_MONSTER_INDEX:	.res 1
.exportzp TARGETED_MONSTER_INDEX
.exportzp CURRENT_MONSTER_INDEX = TARGETED_MONSTER_INDEX

REMAINDER32:
.exportzp REMAINDER32
DIVISOR:		.res 2
.exportzp DIVISOR
.exportzp DIVISOR_hi = DIVISOR+1

DIVIDEND:		.res 2
.exportzp DIVIDEND
.exportzp DIVIDEND_hi = DIVIDEND+1
.exportzp QUOTIENT = DIVIDEND

REMAINDER:		.res 2
.exportzp REMAINDER
.exportzp REMAINDER_hi = REMAINDER+1

QUOTIENT32:
NUMERATOR32:	.res 4
DENOMINATOR32:	.res 4
.exportzp QUOTIENT32
.exportzp NUMERATOR32
.exportzp DENOMINATOR32

.exportzp BB_DST = NUMERATOR32
.exportzp BB_BITS = NUMERATOR32+2

.exportzp wmi_a = NUMERATOR32+0
.exportzp wmi_b = NUMERATOR32+1

MEMBER_GETTING_XP:	.res 1
.exportzp MEMBER_GETTING_XP

MEMBER_INVENTORY_OPEN:	.res 1
.exportzp MEMBER_INVENTORY_OPEN

SWAPPING_MEMBER_INDEX:	.res 1
.exportzp SWAPPING_MEMBER_INDEX

SWAPPING_IN_INVENTORY_MEMBER:	.res 1
.exportzp SWAPPING_IN_INVENTORY_MEMBER

SPELLBOOK_MEMBER_INDEX:	.res 1
.exportzp SPELLBOOK_MEMBER_INDEX

BACKUP_SPELLBOOK_MEMBER_INDEX:	.res 1
.exportzp BACKUP_SPELLBOOK_MEMBER_INDEX

SPELLBOOK_SELECTED_ROW:	.res 1
.exportzp SPELLBOOK_SELECTED_ROW

SPELLBOOK_TYPE: .res 1
.exportzp SPELLBOOK_TYPE

BACKUP_SPELLBOOK_TYPE: .res 1
.exportzp BACKUP_SPELLBOOK_TYPE

SPELLCASTER_USING_SCROLL: .res 1
.exportzp SPELLCASTER_USING_SCROLL

SELECTED_SPELL: .res 2
.exportzp SELECTED_SPELL

CURRENT_SPELLTYPE: .res 2
.exportzp CURRENT_SPELLTYPE

TEXT2_SCREEN: .res 2
.exportzp TEXT2_SCREEN

TEXT2_D800: .res 2
.exportzp TEXT2_D800

ASK_FOR_SPELL_RECEIVER_GUI: .res 1
.exportzp ASK_FOR_SPELL_RECEIVER_GUI

AFFECTED_MEMBER_INDEX: .res 1
.exportzp AFFECTED_MEMBER_INDEX

AFFECTED_MEMBER: .res 2
.exportzp AFFECTED_MEMBER

SPELLCASTER_SUBPOS: .res 1
.exportzp SPELLCASTER_SUBPOS

COUNT: .res 1
.exportzp COUNT

KEYEVENT: .res 4
.exportzp KEYEVENT

MOUSEEVENT: .res 4
.exportzp MOUSEEVENT

INF_READ_BYTE: .res 1
.exportzp INF_READ_BYTE

MAY_ITEM: .res 2
.exportzp MAY_ITEM

MONSTER_ATTACK_TARGET_ORDER: .res 2
.exportzp MONSTER_ATTACK_TARGET_ORDER

CURRENT_CAMP_MENU: .res 2
.exportzp CURRENT_CAMP_MENU

SCREENMODE: .res 2
.exportzp SCREENMODE

SCREENMODE_PENDING: .res 2
.exportzp SCREENMODE_PENDING

ROWSKIP:		.res 1
.exportzp ROWSKIP

ARGS:			.res 10
.exportzp ARGS
.exportzp ARGS_1 = ARGS+1
.exportzp ARGS_2 = ARGS+2
.exportzp ARGS_3 = ARGS+3
.exportzp ARGS_4 = ARGS+4
.exportzp ARGS_5 = ARGS+5
.exportzp ARGS_6 = ARGS+6
.exportzp ARGS_7 = ARGS+7
.exportzp ARGS_8 = ARGS+8
.exportzp ARGS_9 = ARGS+9

RESULT:			.res 18*2
.exportzp RESULT
.exportzp SCANNED_TRIGGER_INCIDES = RESULT

