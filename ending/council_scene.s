.include "../global.inc"
.include "bitblt.inc"

OFF_SCREEN = $5c00
OFF_BITMAP = $6000
OFF_D800 = $5800

ENDING_SCREEN1 = $5000
ENDING_SCREEN2 = $5400
ENDING_FONT1 = $5800
ENDING_FONT2 = $6000
ENDING_SPRITES = $6800

.segment "ENDING_RAM"
.proc screenModeEndScroller
	.word init
	.word teardown
nbrSplits: .byte 1
	.word $10, top
splitPos:
    .word $38+580, split

buffer: .byte 0
bufferMutex: .byte 0
fineScroll: .byte 7
frame: .byte 0
spritePos: .word $80+500
startScroll: .byte 0

	.proc init
		lda #$17
		sta $d011
		ldx #7
		ldy #14
		:
			lda #$e
			sta $d027,x
			lda spritex,x
			sta $d000,y
			txa
			clc
			adc #<(ENDING_SPRITES/$40)
			sta ENDING_SCREEN1+$3f8,x
			sta ENDING_SCREEN2+$3f8,x
			dey
			dey
			dex
		bpl :-
		lda #$7f
		sta $d01b
		lda #0
		sta $d015
		sta $d010
		sta $d017
		sta $d01d
		sta $d01c

		lda #$08
		sta $d016
		rts
spritex:
		.byte 112+24+0*24
		.byte 112+24+1*24
		.byte 112+24+2*24
		.byte 112+24+3*24
		.byte 112+24+1*24
		.byte 112+24+2*24
		.byte 112+24+3*24
	.endproc

	.proc teardown
		rts
	.endproc

	.proc split
		lda buffer
		bne :+
			SET_D018 ENDING_FONT2, ENDING_SCREEN1
			SET_DD00 ENDING_FONT2
			jmp :++
		:
			SET_D018 ENDING_FONT2, ENDING_SCREEN2
			SET_DD00 ENDING_FONT2
		:
		jmp exitIrq_quick
	.endproc 

	.proc top
		lda #1
		sta nbrSplits
		lda startScroll
		beq noScroll
			inc frame

			lda frame
			and #1
			bne :+
				jsr doScroll
			:

			lda frame
			and #3
			bne :+
				jsr doSprites
			:
		noScroll:

		; Initial state
		lda buffer
		bne :+
			SET_D018 ENDING_FONT1, ENDING_SCREEN1
			SET_DD00 ENDING_FONT1
			jmp :++
		:
			SET_D018 ENDING_FONT1, ENDING_SCREEN2
			SET_DD00 ENDING_FONT1
		:

		; Clamp lower bound
		sec
		lda #<$f8
		sbc splitPos+0
		sta INTRO_IRQ_TMP+0
		lda #>$f8
		sbc splitPos+1
		bpl :+
			jmp exitIrq
		:

		; Clamp upper bound
		sec
		lda splitPos+0
		sbc #<$38
		lda splitPos+1
		sbc #>$38
		bmi negative
			lda #2
			sta nbrSplits
			jmp exitIrq
		negative:
			lda buffer
			bne :+
				SET_D018 ENDING_FONT2, ENDING_SCREEN1
				SET_DD00 ENDING_FONT2
				jmp exitIrq
			:
				SET_D018 ENDING_FONT2, ENDING_SCREEN2
				SET_DD00 ENDING_FONT2
			:
		jmp exitIrq

doSprites:
		sec
		lda spritePos+0
		sbc #<1
		sta spritePos+0
		lda spritePos+1
		sbc #>1
		sta spritePos+1

		; Clamp upper bound
		sec
		lda spritePos+0
		sbc #<$f8
		lda spritePos+1
		sbc #>$f8
		bmi :+
			lda #0
			sta $d015
			rts
		:

		lda #$7f
		sta $d015
		clc
		lda spritePos
		sta $d001
		sta $d003
		sta $d005
		sta $d007
		adc #21
		sta $d009
		sta $d00b
		sta $d00d
		rts

doScroll:
		ldx fineScroll
		dex
		bpl :+
			inc bufferMutex
			lda buffer
			eor #1
			sta buffer
			ldx #7
		:
		stx fineScroll

		sec
		lda splitPos+0
		sbc #<1
		sta splitPos+0
		lda splitPos+1
		sbc #>1
		sta splitPos+1
		txa
		ora #$10
		sta $d011

		lda splitPos+1
		cmp #>-392
		bne :+
			lda splitPos+0
			cmp #<-392
			bne :+
				lda #0
				sta startScroll
		:

		rts
	.endproc
.endproc

.proc screenMode
	.word init
	.word teardown
	.byte 3
	.word $20, top
	.word $a9, stopCouncilSprites
	.word $e8, bottom

	scrollInEyeStalk:
	.word init
	.word teardown
	.byte 6
	.word $20, top
	.word $72, scrollSplit
	.word $79, showBitmap
	.word $c9, hideBitmap
	.word $da, unScrollSplit
	.word $e8, bottom

	shakeHands:
	.word init
	.word teardown
	.byte 9
	.word $20, top
	.word $30, resetHandSprites
sm1:.word $53+21*0, forwardSprites
sm2:.word $53+21*1-1, split0
sm3:.word $53+21*1+10, forwardSprites
sm4:.word $53+21*2-3, split1
sm5:.word $53+21*2+10, forwardSprites
sm6:.word $53+21*3-2, split2
	.word $e8, bottom

	finalShake:
	.word init
	.word teardown
	.byte 4
	.word $20, top
	.word $52, scrollSplit2
	.word $da, unScrollSplit
	.word $e8, bottom

	chars:
	.word init
	.word teardown
	.byte 1
	.word $20, bottom

	frame: .byte 0

	.proc resetHandSprites
		; Init
		ldx #0
		jsr spriteIndex
		lda #0
		sta $d027
		sta $d028
		sta $d02a
		lda #6
		sta $d029
		lda #$a
		sta $d02b
		sta $d02c
		sta $d02d
		sta $d02e

ypos:	lda #$53
		jsr spriteYpos
		clc
		sta sm1
		adc #(21*1-2)-(21*0)
		sta sm2
		adc #(21*1+10)-(21*1-2)
		sta sm3
		adc #(21*2-3)-(21*1+10)
		sta sm4
		adc #(21*2+10)-(21*2-3)
		sta sm5
		adc #(21*3-2)-(21*2+10)
		sta sm6

		lda #$ff
		sta $d015
		jmp exitIrq_quick
	.endproc

	.proc split0
		ldx #4
		dex
		bne *-1
		nop
		nop
		ldx #8
		jsr spriteIndex
		jmp exitIrq_quick
	.endproc

	.proc split1
		pha
		pla
		pha
		pla
		pha
		pla
		pha
		pla
		lda #0
		sta $d02d
		sta $d02e
		ldx #16
		jsr spriteIndex
		jmp exitIrq_quick
	.endproc

	.proc split2
		pha
		pla
		pha
		pla
		nop
		ldx #24
		jsr spriteIndex
		lda #0
		sta $d02c
		jmp exitIrq_quick
	.endproc

	.proc forwardSprites
		lda $d001
		clc
		adc #21
		jsr spriteYpos
		jmp exitIrq_quick
	.endproc


spriteYpos:
		.repeat 8,I
			sta $d001+I*2
		.endrep
		rts

spriteIndex:
		.repeat 8,I
			lda spr_index+I,x
			sta vic_screen+$3ff-I
		.endrep
		rts

_CLEAR = <((vic_sprites+ending_left_hand_sprites_size)/64)
SPR = (<(vic_sprites/64))
spr_index: ;  Hand(4),                          Mag(1),   Cya(1),   Yel(2)
		.byte _CLEAR, _CLEAR, SPR+00, _CLEAR,   _CLEAR,   SPR+01,   _CLEAR, _CLEAR
		.byte SPR+02, SPR+03, SPR+04, _CLEAR,   SPR+06,   SPR+05,   _CLEAR, _CLEAR
		.byte SPR+07, SPR+08, SPR+09, SPR+10,   _CLEAR,   SPR+11,   SPR+12, SPR+13
		.byte _CLEAR, SPR+14, SPR+15, SPR+16,   _CLEAR,   _CLEAR,   _CLEAR, SPR+17

	.proc scrollSplit
		nop
		nop
;		bit $ff
		scrollY:lda #0
		and #7
		ora #$78
		sta $d011
		lda $dd00
		and #$fc
		_dd00:ora #$00
		sta $dd00
		_d018:lda #$00
		sta $d018
		jmp exitIrq_quick
	.endproc

	.proc scrollSplit2
		nop
		bit $ff
		scrollY:lda #3
		and #7
		ora #$38
		sta $d011
		jmp exitIrq_quick
	.endproc

	.proc showBitmap
		nop
		lda system
		cmp #SYSTEM_PAL
		bne :+
		:

		lda $d011
		and #$3f
		sta $d011
		jmp exitIrq_quick
	.endproc

	.proc hideBitmap
		pha
		pla
		nop
		lda $d011
		ora #$70
		ldx $d012
		:cpx $d012
		beq :-
		sta $d011
		jmp exitIrq_quick
	.endproc

	.proc unScrollSplit
		ldx frame
		inx
		stx frame
		txa
		and #1
		bne :+
			dec scrollSplit::scrollY+1
		:
		lda #$7b
		sta $d011
		jmp exitIrq_quick
	.endproc

	.proc top
_d021:	lda #8
		sta $d021
		lda #$3b
		sta $d011
		lda #$18
		sta $d016
		lda #(((vic_screen/$400)&15)<<4) | ((vic_bitmap/$400)&8)
		sta $d018
		lda $dd00
		and #$fc
		sta $dd00
		jmp exitIrq_quick
	.endproc

	.proc stopCouncilSprites
		lda #0
		beq :+
			lda #(((vic_screen2/$400)&15)<<4) | ((vic_bitmap/$400)&8)
			sta $d018
		:
		jmp exitIrq_quick
	.endproc

	.proc bottom
		lda #0
		sta $d021
		lda #$1b
		sta $d011
		lda $dd00
		and #$fc
		ora #$01
		sta $dd00
		lda #$02
		sta $d018
		lda #$08
		sta $d016
		jmp exitIrq
	.endproc

	.proc init
		lda #0
		sta $d020
		sta $d015
		rts
	.endproc

	.proc teardown
		lda #0
		sta $d011
		sta $d015
		lda $dd00
		and #$fc
		sta $dd00
		rts
	.endproc
.endproc
scrollY = screenMode::scrollSplit::scrollY+1

_d018 = screenMode::scrollSplit::_d018+1
_dd00 = screenMode::scrollSplit::_dd00+1
_d021 = screenMode::top::_d021+1

.segment "COUNCIL"
council_frames_gfx:
		.incbin  "../converters/ending/compressed/council_frames.prg.b2",2
		.include "../converters/ending/compressed/council_frames.inc"

.segment "HIGHPRIEST_EYES"
highpriest_eyes_gfx:
		.incbin  "../converters/ending/compressed/highpriest_eyes.prg.b2",2
		.include "../converters/ending/compressed/highpriest_eyes.inc"

.segment "EYE_STALK"
eye_stalk_gfx:
		.incbin  "../converters/ending/compressed/eye_stalk.prg.b2",2
		.include "../converters/ending/compressed/eye_stalk.inc"

.segment "HIGHPRIEST"
highpriest_gfx:
		.incbin  "../converters/ending/compressed/highpriest.prg.b2",2
		.include "../converters/ending/compressed/highpriest.inc"

.segment "SHAKEHANDS"
shakehands_gfx:
		.incbin  "../converters/ending/compressed/shakehands.prg.b2",2
		.include "../converters/ending/compressed/shakehands.inc"

.segment "ENDING_CREDITS"
ending_credits_gfx:
		.incbin  "../converters/ending/compressed/ending_credits.prg.b2",2
		.include "../converters/ending/compressed/ending_credits.inc"

.segment "ENDING"

.define BASEY 6

.macro TALK ticks
		sec
		ldx #ticks
		jsr animateTableSubtle
.endmacro
.macro NOTALK ticks
		clc
		ldx #ticks
		jsr animateTableSubtle
.endmacro
.macro SHRUGS
		jsr animateTableShrug
.endmacro
.macro AGITATED ticks
		ldx #ticks
		jsr animateTableAgitated
.endmacro
.macro TEXT line,color
		ldx #line
		ldy #1
		lda #color
		jsr showTextExtEnding
.endmacro
.macro NOTEXT
		jsr clearText
.endmacro
.define FRAMES_PER_TICK 8

.proc clearOffscreen
		memset OFF_BITMAP,  $aa, 8000
		memset OFF_SCREEN,  $00, 1000
		memset OFF_D800,  $00, 1000
		rts
.endproc

.proc clearVicScreen
		memset vic_screen,  $00, 1000
		memset $d800,       $00, 1000
		memset vic_bitmap,  $aa, 8000
		rts
.endproc

.export council_scene
.proc council_scene
		lda #<.bank(council_frames_gfx)
		ldy #<council_frames_gfx
		ldx #>council_frames_gfx
		jsr decrunchFarToFreeRamEnding

		memcpy vic_sprites, freeRamEnding+council_sprites, council_sprites_size
		ldx #62
		lda #0
		:
			sta vic_sprites+council_sprites_size,x
			dex
		bpl :-

		jsr clearVicScreen
		jsr clearOffscreen
		jsr renderTableOffscreen

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

		jsr transitionTable

		NOTALK 10

		TEXT 0,$f ;Who dares...
		TALK 12
		NOTALK 3

		TEXT 1,$f ;So you have returned...
		TALK 12
		NOTALK 3

		TEXT 2,$e ;Lords of Waterdeep...
		NOTALK 12

		SHRUGS
		NOTALK 2

		TEXT 3,$f ;A gift? What is it?
		TALK 12
		NOTALK 3

		TEXT 4,$f ;Perhaps you have captured...
		TALK 12

		jsr animateCouncilSprites

		AGITATED 10

		NOTEXT
		jsr transitionHighPriestEyes

		jsr scrollInEyeStalk
		jsr eyeStalkBlink

		NOTEXT
		lda #<.bank(council_frames_gfx)
		ldy #<council_frames_gfx
		ldx #>council_frames_gfx
		jsr decrunchFarToFreeRamEnding
		jsr renderTableOffscreen
		jsr transitionTable2

		AGITATED 10

		TEXT 6,$f ;I apologize for my..
		TALK 12
		NOTALK 10

		jsr fadeoutTable

		TEXT 7,$f ;You are worthy of...

		jsr transitionHighPriest

		lda #200
		jsr wait

		jsr fadeoutHighPriest

		jsr shakeHands

		jsr fadeoutTable

		jsr endScroller

		jmp *
.endproc

.proc clearText
		lda #0
		ldx #79
		:
			sta $d800+40*23,x
			dex
		bpl :-
		rts
.endproc

.proc endScroller
		lda #<.bank(ending_credits_gfx)
		ldy #<ending_credits_gfx
		ldx #>ending_credits_gfx
		jsr decrunchFarToFreeRamEnding

		lda #<(freeRamEnding+ending_credits_charset1_screen+25*40)
		sta SRC_SCREEN+0
		lda #>(freeRamEnding+ending_credits_charset1_screen+25*40)
		sta SRC_SCREEN+1

		memcpy ENDING_FONT1, freeRamEnding+ending_credits_charset1_charset, ending_credits_charset1_charset_size
		memcpy ENDING_FONT2, freeRamEnding+ending_credits_charset2_charset, ending_credits_charset2_charset_size
		memcpy ENDING_SPRITES, freeRamEnding+ending_theend_sprites, ending_theend_sprites_size
		memcpy ENDING_SCREEN1, freeRamEnding+ending_credits_charset1_screen, 1000
		memset freeRamEnding+ending_credits_charset2_screen+ending_credits_charset2_screen_size, 0, 10*40

		clc
		ldx #<screenModeEndScroller
		ldy #>screenModeEndScroller
		jsr setScreenMode

		jsr showThusBeginALegend
		memset $d800, 1 ,25*40

		lda #50
		jsr wait

		inc screenModeEndScroller::startScroll

		coarseScroll:
			ldx #0
			:
				.repeat 3,I
					lda ENDING_SCREEN1+I*$100+40,x
					sta ENDING_SCREEN2+I*$100,x
				.endrep
				inx
			bne :-
			ldx #192
			:
				lda ENDING_SCREEN1+3*$100+40-1,x
				sta ENDING_SCREEN2+3*$100-1,x
				dex
			bne :-
			ldy #39
			:
				lda (SRC_SCREEN),y
				sta ENDING_SCREEN2+24*40,y
				dey
			bpl :-

			jsr waitMutex
			lda screenModeEndScroller::startScroll
			beq done

			ldx #0
			:
				.repeat 3,I
					lda ENDING_SCREEN2+I*$100+40,x
					sta ENDING_SCREEN1+I*$100,x
				.endrep
				inx
			bne :-
			ldx #192
			:
				lda ENDING_SCREEN2+3*$100+40-1,x
				sta ENDING_SCREEN1+3*$100-1,x
				dex
			bne :-
			ldy #39
			:
				lda (SRC_SCREEN),y
				sta ENDING_SCREEN1+24*40,y
				dey
			bpl :-

			jsr waitMutex
		jmp coarseScroll
		done:

		jsr fadeOutEndCredits
		jmp *

waitMutex:
		clc
		lda SRC_SCREEN+0
		adc #40
		sta SRC_SCREEN+0
		bcc :+
			inc SRC_SCREEN+1
		:

		:
			lda screenModeEndScroller::startScroll
			beq :+
			lda screenModeEndScroller::bufferMutex
		beq :-
		:
		lda #0
		sta screenModeEndScroller::bufferMutex
		rts
.endproc

.proc showThusBeginALegend
		ldy #0
		:
			lda fade,y
			bmi :++
			ldx #79
			:
				sta $d800+12*40,x
				dex
			bpl :-
			lda #5
			jsr wait
			iny
		jmp :--
		:

		rts
fade:	.byte 6,2,4,$c,5,3,7,1,$ff
.endproc

.proc fadeOutEndCredits
		ldy #0
		loop:
			lda fade,y
			bpl :+
				rts
			:
			ldx #79
			:
				sta $d800+9*40,x
				dex
			bpl :-
			cmp #$e
			bne :+
				lda #0
				sta $d01b
			:
			lda #5
			jsr wait
			iny
		jmp loop
fade:	.byte 1,$d,3,$e,4,$b,$9,0,$ff
.endproc

.proc shakeHands
		lda #<.bank(shakehands_gfx)
		ldy #<shakehands_gfx
		ldx #>shakehands_gfx
		jsr decrunchFarToFreeRamEnding
		memcpy vic_sprites, freeRamEnding+ending_left_hand_sprites, ending_left_hand_sprites_size
		lda #0
		ldx #62
		:
			sta vic_sprites+ending_left_hand_sprites_size,x
			dex
		bpl :-

		setBitBltSrc freeRamEnding+ending_right_hand_bitmap, freeRamEnding+ending_right_hand_screen, freeRamEnding+ending_right_hand_d800, 12

		setBitBltDst vic_bitmap, vic_screen
		lda #>$d800
		sta ending_d800_bitblt_dst

		ldx #$ff
		stx $d01c
		inx
		stx $d015
		stx $d017
		stx $d01b
		stx $d01d
		lda #8
		sta $d025
		lda #9
		sta $d026

		ldx #0
		:
			txa
			pha
			lda leftHandDx,x
			jsr spriteXpos
			lda leftHandDy,x
			sta screenMode::resetHandSprites::ypos+1

			lda #2
			jsr wait
			sec
			ldx #<screenMode::shakeHands
			ldy #>screenMode::shakeHands
			jsr setScreenMode
			pla
			tax
			inx
			cpx #5
		bne :-

		TEXT 8,$f ;Come sit down...

		lda #100
		jsr wait

		lda #0
		sta BLTSX
		sta BLTSY
		lda #12
		sta BLTW
		lda #9
		sta BLTH

		ldx #0
		loop:
			lda #2
			jsr wait
			txa
			pha
			lda rightHandDx,x
			sta BLTDX
			lda rightHandDy,x
			sta BLTDY
			jsr ending_bitblt
			pla
			pha
			cmp #1
			bne :+
				ldx #18+3+3+12
				jsr ending_vclear2
				ldx #18+3+3+13
				jsr ending_vclear2
				ldx #18+3+3+14
				jsr ending_vclear2
				ldy #6+1+2+9
				jsr ending_hclear
				ldy #6+1+2+9+1
				jsr ending_hclear
				jmp hclearDone
			:
			cmp #2
			bne :+
				ldx #18+3+12
				jsr ending_vclear2
				ldx #18+3+13
				jsr ending_vclear2
				ldx #18+3+14
				jsr ending_vclear2
				ldy #6+1+9
				jsr ending_hclear
				ldy #6+1+9+1
				jsr ending_hclear
				jmp hclearDone
			:
			cmp #3
			bne :+
				ldx #18+12
				jsr ending_vclear2
				ldx #18+13
				jsr ending_vclear2
				ldx #18+14
				jsr ending_vclear2
				ldy #6+1+8
				jsr ending_hclear
			:
			hclearDone:

			pla
			tax
			inx
			cpx #4
		bne loop

		lda #2
		jsr wait
		setBitBltSrc freeRamEnding+ending_shake_hands_bitmap, freeRamEnding+ending_shake_hands_screen, freeRamEnding+ending_shake_hands_d800, 15
		lda #15
		sta BLTW
		lda #8
		sta BLTH
		lda #10
		sta BLTDX
		lda #6
		sta BLTDY
		jsr ending_bitblt
		sec
		ldx #<screenMode::finalShake
		ldy #>screenMode::finalShake
		jsr setScreenMode
		lda #0
		sta $d015
		ldx #10+15
		jsr ending_vclear2
		ldx #10+16
		jsr ending_vclear2
		ldx #10+17
		jsr ending_vclear2
		ldx #10+18
		jsr ending_vclear2
		ldx #10+19
		jsr ending_vclear2
		ldy #6+1+7
		jsr ending_hclear

		lda #50
		jsr wait

		lda #3
		sta COUNT

		lda #$ff
		sta $ffff
		shakeLoop:
			ldy #0
			:
				lda #2
				jsr wait

				ldx shakeY,y
				bmi :+
					stx screenMode::scrollSplit2::scrollY+1
					iny
			jmp :-
			:
			dec COUNT
		bne shakeLoop

		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

		lda #>irqHandler
		sta $ffff

		rts

shakeY:
		.byte 3,2,1,0,7,6,5,4,5,6,7,0,1,2,$ff

leftHandDx: 
		.byte $72-8*12,$72-8*8,$72-8*5,$72-8*2,$72
leftHandDy:
		.byte $53+8*7, $53+8*5,$53+8*3,$53+8*1,$53

rightHandDx:
		.byte 18+3+3+3, 18+3+3, 18+3, 18
rightHandDy:
		.byte 6+1+2+2, 6+1+2, 6+1, 6

spriteXpos:
		sta $d00e
		sta $d004
		sta $d006
		clc
		adc #24
		sta $d00c
		adc #24
		sta $d00a
		sta $d002
		adc #24
		sta $d008
		sta $d000
		rts
.endproc

.proc eyeStalkBlink
		ldx #0
		:
			txa
			pha
			lda animation_eyes,x
			bmi :+
			jsr placeHighPriestSpritesDefault
			pla
			tax
			lda animation_wait,x
			jsr wait
			inx
		jmp :-
:		pla
		rts

animation_eyes: .byte 0, 1,0, 1,2,3,4, 0, 4,0, 4,3,2,1, 2,3, 4,$ff
animation_wait: .byte 2,60,2,40,3,3,60,3,30,5,30,3,3,70,3,3,70
.endproc

.proc fadeoutHighPriest
_screenHi = freeRamEnding
_screenLo = freeRamEnding+15*40
_d800 = freeRamEnding+15*40*2
FADELO_TAB = TMP+0
FADEHI_TAB = TMP2+0

		memcpy_pureram _screenHi, vic_screen+3*40, 15*40
		memcpy _d800, $d800+3*40, 15*40

		; Separate screen colors and make sure $d800 colors are 4-bit
		ldx #99
		:
			.repeat 6,I
				lda _screenHi+100*I,x
				tay
				lsr
				lsr
				lsr
				lsr
				sta _screenHi+100*I,x
				tya
				and #15
				sta _screenLo+100*I,x
				lda _d800+100*I,x
				and #15
				sta _d800+100*I,x
			.endrep
			dex
			bmi :+
		jmp :-
		:

		lda #<fadeLoTab
		sta FADELO_TAB+0
		lda #>fadeLoTab
		sta FADELO_TAB+1
		lda #<fadeHiTab
		sta FADEHI_TAB+0
		lda #>fadeHiTab
		sta FADEHI_TAB+1

		lda #7
		sta COUNT
		fadeLoop:
			lda #2
			jsr wait

			ldy #8
			lda (FADELO_TAB),y
			sta _d021

			ldx #99
			:
				.repeat 6,I
					ldy _screenHi+100*I,x
					lda (FADEHI_TAB),y
					ldy _screenLo+100*I,x
					ora (FADELO_TAB),y
					sta vic_screen+3*40+100*I,x
					ldy _d800+100*I,x
					lda (FADELO_TAB),y
					sta $d800+3*40+100*I,x
				.endrep
				dex
				bmi :+
			jmp :-
			:

			ldx #79
			ldy #$f
			lda (FADELO_TAB),y
			:
				sta $d800+23*40,x
				dex
			bpl :-

			clc
			lda FADELO_TAB+0
			adc #16
			sta FADELO_TAB+0
			bcc :+
				inc FADELO_TAB+1
			:

			clc
			lda FADEHI_TAB+0
			adc #16
			sta FADEHI_TAB+0
			bcc :+
				inc FADEHI_TAB+1
			:

			dec COUNT
			bmi :+
		jmp fadeLoop
		:

		jsr clearVicScreen
		lda #8
		sta _d021

		rts
.endproc

.proc fadeoutTable
_screenHi = freeRamEnding
_screenLo = freeRamEnding+9*40
_d800 = freeRamEnding+9*40*2
FADELO_TAB = TMP+0
FADEHI_TAB = TMP2+0

		memcpy_pureram _screenHi, vic_screen+6*40, 9*40
		memcpy _d800, $d800+6*40, 9*40

		; Separate screen colors and make sure $d800 colors are 4-bit
		ldx #119
		:
			.repeat 3,I
				lda _screenHi+3*40*I,x
				tay
				lsr
				lsr
				lsr
				lsr
				sta _screenHi+3*40*I,x
				tya
				and #15
				sta _screenLo+3*40*I,x
				lda _d800+3*40*I,x
				and #15
				sta _d800+3*40*I,x
			.endrep
			dex
		bpl :-

		lda #<fadeLoTab
		sta FADELO_TAB+0
		lda #>fadeLoTab
		sta FADELO_TAB+1
		lda #<fadeHiTab
		sta FADEHI_TAB+0
		lda #>fadeHiTab
		sta FADEHI_TAB+1

		lda #7
		sta COUNT
		fadeLoop:
			lda #4
			jsr wait

			ldy #8
			lda (FADELO_TAB),y
			sta _d021

			ldx #119
			:
				.repeat 3,I
					ldy _screenHi+3*40*I,x
					lda (FADEHI_TAB),y
					ldy _screenLo+3*40*I,x
					ora (FADELO_TAB),y
					sta vic_screen+6*40+3*40*I,x
					ldy _d800+3*40*I,x
					lda (FADELO_TAB),y
					sta $d800+6*40+3*40*I,x
				.endrep
				dex
			bpl :-

			ldx #79
			ldy #$f
			lda (FADELO_TAB),y
			:
				sta $d800+23*40,x
				dex
			bpl :-

			clc
			lda FADELO_TAB+0
			adc #16
			sta FADELO_TAB+0
			bcc :+
				inc FADELO_TAB+1
			:

			clc
			lda FADEHI_TAB+0
			adc #16
			sta FADEHI_TAB+0
			bcc :+
				inc FADEHI_TAB+1
			:

			dec COUNT
		bpl fadeLoop

		jsr clearVicScreen
		lda #8
		sta _d021

		rts
.endproc

fadeLoTab:
		.byte $0,$7,$6,$5,$2,$c,$0,$3, $b,$0,$e,$9,$4,$f,$8,$a
		.byte $0,$3,$0,$c,$6,$4,$0,$5, $9,$0,$8,$0,$2,$a,$b,$e
		.byte $0,$5,$0,$4,$0,$2,$0,$c, $0,$0,$b,$0,$6,$e,$9,$8
		.byte $0,$c,$0,$2,$0,$6,$0,$4, $0,$0,$9,$0,$0,$8,$0,$b
		.byte $0,$4,$0,$6,$0,$0,$0,$2, $0,$0,$0,$0,$0,$b,$0,$9
		.byte $0,$2,$0,$0,$0,$0,$0,$6, $0,$0,$0,$0,$0,$9,$0,$0
		.byte $0,$6,$0,$0,$0,$0,$0,$0, $0,$0,$0,$0,$0,$0,$0,$0
		.byte $0,$0,$0,$0,$0,$0,$0,$0, $0,$0,$0,$0,$0,$0,$0,$0

fadeHiTab:
		.byte $00,$10,$20,$30,$40,$50,$60,$70, $80,$90,$a0,$b0,$c0,$d0,$e0,$f0
		.byte $00,$70,$60,$50,$20,$c0,$00,$30, $b0,$00,$e0,$90,$40,$f0,$80,$a0
		.byte $00,$30,$00,$c0,$60,$40,$00,$50, $90,$00,$80,$00,$20,$a0,$b0,$e0
		.byte $00,$50,$00,$40,$00,$20,$00,$c0, $00,$00,$b0,$00,$60,$e0,$90,$80
		.byte $00,$c0,$00,$20,$00,$60,$00,$40, $00,$00,$90,$00,$00,$80,$00,$b0
		.byte $00,$40,$00,$60,$00,$00,$00,$20, $00,$00,$00,$00,$00,$b0,$00,$90
		.byte $00,$20,$00,$00,$00,$00,$00,$60, $00,$00,$00,$00,$00,$90,$00,$00
		.byte $00,$60,$00,$00,$00,$00,$00,$00, $00,$00,$00,$00,$00,$00,$00,$00
		.byte $00,$00,$00,$00,$00,$00,$00,$00, $00,$00,$00,$00,$00,$00,$00,$00

.proc transitionHighPriest
		lda #<.bank(highpriest_gfx)
		ldy #<highpriest_gfx
		ldx #>highpriest_gfx
		jsr decrunchFarToFreeRamEnding
		setBitBltSrc freeRamEnding+highpriest_bitmap, freeRamEnding+highpriest_screen, freeRamEnding+highpriest_d800, 12

		setBitBltDst vic_bitmap, vic_screen
		lda #>$d800
		sta ending_d800_bitblt_dst

		lda #0
		sta BLTSY
		lda #3
		sta BLTDY
		lda #1
		sta BLTW
		lda #15
		sta BLTH

		ldx #11
		loop:
			lda #3
			jsr wait

			stx BLTSX
			clc
			txa
			adc #(40-12)/2
			sta BLTDX

			jsr ending_bitblt

			ldx BLTSX
			dex
		bpl loop
		rts
.endproc

.proc scrollInEyeStalk
		lda d018tab
		sta _d018
		lda dd00tab
		sta _dd00
		lda #>OFF_D800
		sta ending_d800_bitblt_dst
		setBitBltDst OFF_BITMAP, OFF_SCREEN

		jsr clearOffscreen
		lda #0
		ldx #39
		:
			sta vic_screen+8*40,x
			dex
		bpl :-

		lda #<.bank(eye_stalk_gfx)
		ldy #<eye_stalk_gfx
		ldx #>eye_stalk_gfx
		jsr decrunchFarToFreeRamEnding
		setBitBltSrc freeRamEnding+eye_stalk_bitmap, freeRamEnding+eye_stalk_screen, freeRamEnding+eye_stalk_d800, 16

		lda #12
		sta BLTDX
		lda #11+8
		sta BLTDY
		lda #16
		sta BLTW
		lda #1
		sta BLTH
		jsr ending_bitblt

		lda #1
		jsr wait

		sec
		ldx #<screenMode::scrollInEyeStalk
		ldy #>screenMode::scrollInEyeStalk
		jsr setScreenMode

		:
			jsr swapBuffers
			dec BLTDY
			inc BLTH
			lda BLTH
			cmp #11
			beq :+
			jsr ending_bitblt
		jmp :-
:
		ldx #39
		:
			lda #0
			sta $d800+19*40,x
			lda #$ff
			sta vic_bitmap+19*320+40*0,x
			sta vic_bitmap+19*320+40*1,x
			sta vic_bitmap+19*320+40*2,x
			sta vic_bitmap+19*320+40*3,x
			sta vic_bitmap+19*320+40*4,x
			sta vic_bitmap+19*320+40*5,x
			sta vic_bitmap+19*320+40*6,x
			sta vic_bitmap+19*320+40*7,x
			dex
		bpl :-

		sec
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode
		rts

checkText:
		lda screenMode::frame
		cmp #60
		bne :+
			TEXT 5,$e ;No, my lords, it's Xanathar!
		:
		rts

swapBuffers:
		:
			jsr checkText
			lda scrollY
			and #7
			cmp #3
		beq :-
		:
			jsr checkText
			lda scrollY
			and #7
			cmp #3
		bne :-

		ldx #15
		:
			.repeat 10,I
				lda OFF_D800+(10+I)*40+12,x
				sta $d800+(10+I)*40+12,x
			.endrep
			dex
		bpl :-

setBuffer:
		ldx buffer
		lda d018tab,x
		sta _d018
		lda dd00tab,x
		sta _dd00
		inx
		txa
		and #1
		sta buffer
		bne :+
			setBitBltDst vic_bitmap, vic_screen
			rts
		:
		setBitBltDst OFF_BITMAP, OFF_SCREEN
		rts

d018tab:
		CALC_D018 vic_bitmap, vic_screen
		CALC_D018 OFF_BITMAP, OFF_SCREEN
dd00tab:
		CALC_DD00 vic_bitmap
		CALC_DD00 OFF_BITMAP

		setBitBltDst vic_bitmap, vic_screen

.pushseg
.segment "ENDING_RAM"
		buffer: .byte 1
.popseg

.endproc

.proc transitionHighPriestEyes
		lda #<.bank(highpriest_eyes_gfx)
		ldy #<highpriest_eyes_gfx
		ldx #>highpriest_eyes_gfx
		jsr decrunchFarToFreeRamEnding

		memcpy vic_sprites, freeRamEnding+highpriest_eyes_sprites, highpriest_eyes_sprites_size
		lda #$0f
		sta $d01c
		lda #0
		sta $d015
		sta $d01b
		sta $d017
		sta $d01d
		sta $d025
		ldx #$10
		:
			sta $d000,x
			dex
		bpl :-

		setBitBltSrc freeRamEnding+highpriest_eyes_bitmap, freeRamEnding+highpriest_eyes_screen, freeRamEnding+highpriest_eyes_d800, 16

		lda #0
		sta BLTSX
		sta BLTSY

		ldx #0
		:
			lda transition,x
			bmi done
			sta BLTDX
			inx
			lda transition,x
			sta BLTDY
			inx
			lda transition,x
			sta BLTW
			inx
			lda transition,x
			sta BLTH
			inx

			lda transition,x
			pha
			inx
			lda transition,x
			pha
			inx
			lda transition,x
			pha
			inx
			lda transition,x
			pha
			inx
			lda transition,x
			pha
			inx
			lda transition,x
			pha
			inx
			stx COUNT

			lda #3
			jsr wait

			jsr ending_bitblt

			pla
			tay
			pla
			tax
			lda #1
			jsr placeHighPriestSprites
			pla
			sta $d015

			pla
			tay
			jsr ending_hclear

			pla
			tax
			jsr ending_vclear

			pla
			tax
			jsr ending_vclear

			ldx COUNT
		jmp :-

done:	ldx #28		
		jmp ending_vclear

transition:
; dx,dy,w,h, clearCol1, clearCol2, clearRow,  $d015,eyes x,y
		.byte 18,6,1,1,  0,39,15, 0,0,0
		.byte 17,5,3,3,  1,38,14, 0,0,0
		.byte 16,4,5,5,  2,37,13, 1,118+32, 82+8
		.byte 15,3,7,5,  3,36,12, 1,118+24, 82
		.byte 14,3,9,5,  4,35,11, 1,118+16, 82
		.byte 13,3,11,5, 5,34,10, 1,118+8, 82
		.byte 12,3,13,5, 6,33,9,  3,118, 82
		.byte 12,3,15,5, 7,32,8,  3,118, 82
		.byte 12,3,16,5, 8,31,8,  3,118, 82
		.byte $ff
.endproc

placeHighPriestSpritesDefault:
	ldx #118
	ldy #82

;a=frame (0=closed, 1=most down, 2=down, 3=little down, 4=middle)
;x=xpos
;y=ypos
.proc placeHighPriestSprites
	sty $d001
	sty $d003
	sty $d005
	sty $d007

	cmp #0
	bne :+
		ldy #<(vic_sprites/$40)
		sty vic_screen+$3f8
		iny
		sty vic_screen+$3f9
		iny
		sty vic_screen+$3fa
		iny
		sty vic_screen+$3fb

		txa
		clc
		adc #8
		sta $d000
		adc #24
		sta $d002
		adc #48
		sta $d004
		adc #24
		sta $d006
		lda #$f
		sta $d015
		lda #$9
		sta $d026
		lda #$8
		jmp :++
	:
		clc
		adc #<((vic_sprites/$40)+4-1)
		sta vic_screen+$3f8
		adc #4
		sta vic_screen+$3f9

		txa
		adc #16
		sta $d000
		adc #48+24
		sta $d002
		lda #$3
		sta $d015
		lda #$6
		sta $d026
		lda #$e
	:
	sta $d027
	sta $d028
	sta $d029
	sta $d02a
	rts
.endproc

;c = talk boolean
;x = # ticks
.proc animateTableSubtle
		lda #0
		adc #0
		sta TMP3
		stx COUNT
		loop:
			ldx #0
			:
				ldy #0
				jsr rnd
				and #3
				bne :+
					iny
				:
				jsr drawCouncilCharacterHandsFrame
				inx
				cpx #5
			bne :--

			ldy #0
			lda TMP3
			beq :+
				jsr rnd
				and #1
				beq :+
					ldy #3
			:
			ldx #3
			jsr drawCouncilCharacterFaceFrame

			lda #FRAMES_PER_TICK
			jsr wait

			dec COUNT
		bne loop
		ldx #3
		ldy #0
		jsr drawCouncilCharacterFaceFrame
		rts
.endproc

;x = # ticks
.proc animateTableAgitated
		LOCALTMP = NUMERATOR32

		lda #0
		sta LOCALTMP+0
		sta LOCALTMP+1
		sta LOCALTMP+2
		sta LOCALTMP+3
		sta LOCALTMP+4
		sta LOCALTMP+5

		stx COUNT
		loop:
			ldx #5
			faceLoop:
				ldy LOCALTMP,x
				beq :+
					iny
					tya
					and #3
					tay
					jmp :++
				:
					jsr rnd
					and #3
					bne :+
						iny
				:
				sty LOCALTMP,x
				dex
			bpl faceLoop

			ldx #0
			ldy LOCALTMP+0
			jsr drawCouncilCharacterFaceFrame
			ldx #1
			ldy LOCALTMP+1
			jsr drawCouncilCharacterFaceFrame
			ldx #2
			ldy LOCALTMP+2
			jsr drawCouncilCharacterFaceFrame
			ldx #4
			ldy LOCALTMP+3
			jsr drawCouncilCharacterFaceFrame
			ldx #5
			ldy LOCALTMP+4
			jsr drawCouncilCharacterFaceFrame
			ldx #6
			ldy LOCALTMP+5
			jsr drawCouncilCharacterFaceFrame

			lda #FRAMES_PER_TICK
			jsr wait

			dec COUNT
		bne loop
		rts
.endproc

.proc animateTableShrug
		ldy #1
		ldx #3
		jsr drawCouncilCharacterFaceFrame
		lda #FRAMES_PER_TICK
		jsr wait
		ldy #2
		ldx #3
		jsr drawCouncilCharacterFaceFrame
		lda #FRAMES_PER_TICK
		jsr wait
		ldy #1
		ldx #3
		jsr drawCouncilCharacterFaceFrame
		lda #FRAMES_PER_TICK
		jsr wait
		ldy #0
		ldx #3
		jsr drawCouncilCharacterFaceFrame
		rts
.endproc

.proc renderTableOffscreen
		; Setup council table off screen first
		setBitBltSrc freeRamEnding+council_table_bitmap, freeRamEnding+council_table_screen, freeRamEnding+council_table_d800, 40
		setBitBltDst OFF_BITMAP, OFF_SCREEN

		lda #0
		sta BLTSX
		sta BLTSY
		sta BLTDX
		lda #BASEY
		sta BLTDY
		lda #20
		sta BLTW
		lda #9
		sta BLTH
		jsr ending_bitblt
		lda #20
		sta BLTSX
		sta BLTDX
		jsr ending_bitblt
		
		ldy #0
		jsr drawTableFrame

		ldx #4
		:
			jsr drawCouncilCharacterHandsFrame
			dex
		bpl :-
		ldx #6
		:
			jsr drawCouncilCharacterFaceFrame
			dex
		bpl :-

		rts
.endproc

.proc transitionTable2
		setBitBltSrc OFF_BITMAP, OFF_SCREEN, OFF_D800, 40
		setBitBltDst vic_bitmap, vic_screen
		lda #>$d800
		sta ending_d800_bitblt_dst

		lda #BASEY
		sta BLTSY
		sta BLTDY
		lda #1
		sta BLTW
		lda #9
		sta BLTH

		ldx #0
		loop:
			lda #2
			jsr wait

			stx BLTSX
			stx BLTDX

			cpx #20
			bne :+
				ldy #2
				jsr ending_hclear
				ldy #18
				jsr ending_hclear
			:
			cpx #22
			bne :+
				ldy #3
				jsr ending_hclear
				ldy #17
				jsr ending_hclear
			:
			cpx #24
			bne :+
				ldy #4
				jsr ending_hclear
				ldy #16
				jsr ending_hclear
				lda #0
				sta $d015
			:
			cpx #26
			bne :+
				ldy #5
				jsr ending_hclear
				ldy #15
				jsr ending_hclear
			:


			jsr ending_bitblt

			ldx BLTSX
			inx
			cpx #39
		bne loop
		rts
.endproc

.proc transitionTable
		setBitBltSrc OFF_BITMAP, OFF_SCREEN, $d800, 40
		setBitBltDst vic_bitmap, vic_screen

		lda #BASEY
		sta BLTSY
		sta BLTDY
		lda #1
		sta BLTW
		lda #9
		sta BLTH

		ldx #0
		:
			stx TMP
			lda #3
			jsr wait

			sec
			lda #19
			sbc TMP
			sta BLTSX
			sta BLTDX
			jsr ending_bitblt

			clc
			lda #20
			adc TMP
			sta BLTSX
			sta BLTDX
			jsr ending_bitblt

			ldx TMP
			inx
			cpx #20
		bne :-
		rts
.endproc

.proc animateCouncilSprites
		memset vic_screen2+256, $00, 1000-256
		ldx #8
		:
			lda colors,x
			sta $d025,x
			dex
		bpl :-

		lda #$7f
		sta $d015
		sta $d01c
		lda #0
		sta $d01b
		sta $d017
		sta $d01d
		ldx #$10
		:
			sta $d000,x
			dex
		bpl :-

		lda #25+<(vic_sprites/$40)
		ldx #6
		:
			sta vic_screen2+$3f8,x
			dex
		bne :-

		inc screenMode::stopCouncilSprites+1

		ldx #0
		:
			lda #7
			jsr wait
			txa
			pha
			ldy tableFrame,x
			jsr drawTableFrame
			pla
			pha
			tax
			lda spriteFrame,x
			ldy ypos,x
			ldx #176
			jsr placeCouncilSprites
			pla
			tax
			inx
			cpx #12
		bne :-

		lda #0
		sta $d015
		dec screenMode::stopCouncilSprites+1

		; We overwrite fffe/ffff above on vic_screen2 sprite pointers which is OK as long as kernel is swapped in. But once swapped out things will go south so restore vectors here
		lda #<irqHandler
		sta $fffe
		lda #>irqHandler
		sta $ffff

		rts

colors:
		.byte $9,$8, $a,$c,$a,$c,$0,$f,$7
spriteFrame:
		.byte 4,  0,  0,  0,  0,  0,  1,  2,  3,  4,  4,  4
tableFrame:
		.byte 0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  3
ypos:
		.byte 125+24,125+24,115+24,109+24,106+24,104+24,104+24,104+24,104+24,109+24,118+24,125+24
.endproc

; x:xpos
; y:ypos
; a:frame index
.proc placeCouncilSprites
		pha

		stx $d000
		stx $d002
		stx $d004
		stx $d006
		stx $d008
		stx $d00a
		stx $d00c
		
		sty $d001
		sty $d003
		clc
		tya
		adc #17
		sta $d009
		adc #4
		sta $d005
		sta $d007
		sta $d00b
		sta $d00d

		pla
		asl
		asl
		asl
		tax
		ldy #0
		:
			lda frames,x
			clc
			adc #<(vic_sprites/$40)
			sta vic_screen+$3f8,y
			inx
			iny
			cpy #7
		bne :-

		rts
frames:	.byte  0, 1, 2, 3, 4, 5, 6,	25
		.byte  7, 8, 9,10,11,12,13, 25
		.byte 14,15,16,17,18,19,20, 25
		.byte 25,21,22,23,24,25,25, 25
		.byte 25,25,22,25,25,25,25, 25
.endproc

; y=frame index [0..3]
.proc drawTableFrame
		tya
		pha

		lda #0
		sta BLTSX
		sta BLTSY
		lda #18
		sta BLTDX
		lda #6+BASEY
		sta BLTDY
		lda #5
		sta BLTW
		pha
		lda #3
		sta BLTH

		lda bitmap_lo,y
		sta SRC_BITMAP+0
		lda bitmap_hi,y
		sta SRC_BITMAP+1
		lda screen_lo,y
		sta SRC_SCREEN+0
		lda screen_hi,y
		sta SRC_SCREEN+1
		lda d800_lo,y
		sta SRC_D800+0
		lda d800_hi,y
		sta SRC_D800+1
		pla
		jsr ending_setBitBltSource
		jsr ending_bitblt

		pla
		tay
		rts

bitmap_lo:
		.byte <(freeRamEnding+council_frames_yellow_2_0_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_2_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_2_2_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_2_3_bitmap)

bitmap_hi:
		.byte >(freeRamEnding+council_frames_yellow_2_0_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_2_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_2_2_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_2_3_bitmap)

screen_lo:
		.byte <(freeRamEnding+council_frames_yellow_2_0_screen)
		.byte <(freeRamEnding+council_frames_yellow_2_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_2_2_screen)
		.byte <(freeRamEnding+council_frames_yellow_2_3_screen)

screen_hi:
		.byte >(freeRamEnding+council_frames_yellow_2_0_screen)
		.byte >(freeRamEnding+council_frames_yellow_2_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_2_2_screen)
		.byte >(freeRamEnding+council_frames_yellow_2_3_screen)

d800_lo:
		.byte <(freeRamEnding+council_frames_yellow_2_0_d800)
		.byte <(freeRamEnding+council_frames_yellow_2_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_2_2_d800)
		.byte <(freeRamEnding+council_frames_yellow_2_3_d800)

d800_hi:
		.byte >(freeRamEnding+council_frames_yellow_2_0_d800)
		.byte >(freeRamEnding+council_frames_yellow_2_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_2_2_d800)
		.byte >(freeRamEnding+council_frames_yellow_2_3_d800)
.endproc

; x=character index [0..4]
; y=frame index [0..3]
.proc drawCouncilCharacterHandsFrame
		txa
		pha
		tya
		pha

		lda #0
		sta BLTSX
		sta BLTSY
		lda dx,x
		sta BLTDX
		clc
		lda dy,x
		adc #BASEY
		sta BLTDY
		lda w,x
		sta BLTW
		pha
		lda h,x
		sta BLTH

		sty TMP
		txa
		asl
		asl
		ora TMP
		tax

		lda bitmap_lo,x
		sta SRC_BITMAP+0
		lda bitmap_hi,x
		sta SRC_BITMAP+1
		lda screen_lo,x
		sta SRC_SCREEN+0
		lda screen_hi,x
		sta SRC_SCREEN+1
		lda d800_lo,x
		sta SRC_D800+0
		lda d800_hi,x
		sta SRC_D800+1
		pla
		jsr ending_setBitBltSource
		jsr ending_bitblt

		pla
		tay
		pla
		tax
		rts

dx:		.byte 0,7,25,33,37
dy:		.byte 5,6, 4, 5, 6
w:		.byte 4,3, 3, 2, 2
h:		.byte 1,2, 2, 1, 2

bitmap_lo:
		.byte <(freeRamEnding+council_frames_yellow_0_0_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_0_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_0_2_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_0_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_1_0_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_1_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_1_2_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_1_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_3_0_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_3_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_3_2_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_3_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_4_0_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_4_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_4_2_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_4_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_5_0_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_5_1_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_5_2_bitmap)
		.byte <(freeRamEnding+council_frames_yellow_5_1_bitmap)

bitmap_hi:
		.byte >(freeRamEnding+council_frames_yellow_0_0_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_0_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_0_2_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_0_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_1_0_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_1_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_1_2_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_1_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_3_0_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_3_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_3_2_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_3_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_4_0_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_4_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_4_2_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_4_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_5_0_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_5_1_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_5_2_bitmap)
		.byte >(freeRamEnding+council_frames_yellow_5_1_bitmap)

screen_lo:
		.byte <(freeRamEnding+council_frames_yellow_0_0_screen)
		.byte <(freeRamEnding+council_frames_yellow_0_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_0_2_screen)
		.byte <(freeRamEnding+council_frames_yellow_0_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_1_0_screen)
		.byte <(freeRamEnding+council_frames_yellow_1_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_1_2_screen)
		.byte <(freeRamEnding+council_frames_yellow_1_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_3_0_screen)
		.byte <(freeRamEnding+council_frames_yellow_3_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_3_2_screen)
		.byte <(freeRamEnding+council_frames_yellow_3_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_4_0_screen)
		.byte <(freeRamEnding+council_frames_yellow_4_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_4_2_screen)
		.byte <(freeRamEnding+council_frames_yellow_4_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_5_0_screen)
		.byte <(freeRamEnding+council_frames_yellow_5_1_screen)
		.byte <(freeRamEnding+council_frames_yellow_5_2_screen)
		.byte <(freeRamEnding+council_frames_yellow_5_1_screen)

screen_hi:
		.byte >(freeRamEnding+council_frames_yellow_0_0_screen)
		.byte >(freeRamEnding+council_frames_yellow_0_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_0_2_screen)
		.byte >(freeRamEnding+council_frames_yellow_0_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_1_0_screen)
		.byte >(freeRamEnding+council_frames_yellow_1_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_1_2_screen)
		.byte >(freeRamEnding+council_frames_yellow_1_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_3_0_screen)
		.byte >(freeRamEnding+council_frames_yellow_3_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_3_2_screen)
		.byte >(freeRamEnding+council_frames_yellow_3_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_4_0_screen)
		.byte >(freeRamEnding+council_frames_yellow_4_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_4_2_screen)
		.byte >(freeRamEnding+council_frames_yellow_4_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_5_0_screen)
		.byte >(freeRamEnding+council_frames_yellow_5_1_screen)
		.byte >(freeRamEnding+council_frames_yellow_5_2_screen)
		.byte >(freeRamEnding+council_frames_yellow_5_1_screen)

d800_lo:
		.byte <(freeRamEnding+council_frames_yellow_0_0_d800)
		.byte <(freeRamEnding+council_frames_yellow_0_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_0_2_d800)
		.byte <(freeRamEnding+council_frames_yellow_0_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_1_0_d800)
		.byte <(freeRamEnding+council_frames_yellow_1_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_1_2_d800)
		.byte <(freeRamEnding+council_frames_yellow_1_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_3_0_d800)
		.byte <(freeRamEnding+council_frames_yellow_3_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_3_2_d800)
		.byte <(freeRamEnding+council_frames_yellow_3_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_4_0_d800)
		.byte <(freeRamEnding+council_frames_yellow_4_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_4_2_d800)
		.byte <(freeRamEnding+council_frames_yellow_4_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_5_0_d800)
		.byte <(freeRamEnding+council_frames_yellow_5_1_d800)
		.byte <(freeRamEnding+council_frames_yellow_5_2_d800)
		.byte <(freeRamEnding+council_frames_yellow_5_1_d800)

d800_hi:
		.byte >(freeRamEnding+council_frames_yellow_0_0_d800)
		.byte >(freeRamEnding+council_frames_yellow_0_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_0_2_d800)
		.byte >(freeRamEnding+council_frames_yellow_0_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_1_0_d800)
		.byte >(freeRamEnding+council_frames_yellow_1_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_1_2_d800)
		.byte >(freeRamEnding+council_frames_yellow_1_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_3_0_d800)
		.byte >(freeRamEnding+council_frames_yellow_3_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_3_2_d800)
		.byte >(freeRamEnding+council_frames_yellow_3_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_4_0_d800)
		.byte >(freeRamEnding+council_frames_yellow_4_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_4_2_d800)
		.byte >(freeRamEnding+council_frames_yellow_4_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_5_0_d800)
		.byte >(freeRamEnding+council_frames_yellow_5_1_d800)
		.byte >(freeRamEnding+council_frames_yellow_5_2_d800)
		.byte >(freeRamEnding+council_frames_yellow_5_1_d800)
.endproc

; x=character index [0..6]
; y=frame index [0..3]
.proc drawCouncilCharacterFaceFrame
		txa
		pha
		tya
		pha

		lda #0
		sta BLTSX
		sta BLTSY
		lda dx,x
		sta BLTDX
		clc
		lda dy,x
		adc #BASEY
		sta BLTDY
		lda w,x
		sta BLTW
		pha
		lda h,x
		sta BLTH

		sty TMP
		txa
		asl
		asl
		ora TMP
		tax

		lda bitmap_lo,x
		sta SRC_BITMAP+0
		lda bitmap_hi,x
		sta SRC_BITMAP+1
		lda screen_lo,x
		sta SRC_SCREEN+0
		lda screen_hi,x
		sta SRC_SCREEN+1
		lda d800_lo,x
		sta SRC_D800+0
		lda d800_hi,x
		sta SRC_D800+1
		pla
		jsr ending_setBitBltSource
		jsr ending_bitblt

		pla
		tay
		pla
		tax
		rts

dx:		.byte 0,7,12,17,25,31,37
dy:		.byte 2,1, 1, 3, 1, 2, 1
w:		.byte 3,3, 4, 7, 3, 3, 3
h:		.byte 3,5, 7, 2, 3, 3, 4

bitmap_lo:
		.byte <(freeRamEnding+council_frames_magenta_0_0_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_0_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_0_2_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_0_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_1_0_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_1_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_1_2_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_1_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_2_0_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_2_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_2_2_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_2_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_3_0_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_3_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_3_2_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_3_3_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_4_0_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_4_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_4_2_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_4_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_5_0_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_5_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_5_2_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_5_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_6_0_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_6_1_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_6_2_bitmap)
		.byte <(freeRamEnding+council_frames_magenta_6_1_bitmap)

bitmap_hi:
		.byte >(freeRamEnding+council_frames_magenta_0_0_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_0_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_0_2_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_0_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_1_0_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_1_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_1_2_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_1_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_2_0_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_2_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_2_2_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_2_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_3_0_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_3_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_3_2_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_3_3_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_4_0_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_4_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_4_2_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_4_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_5_0_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_5_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_5_2_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_5_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_6_0_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_6_1_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_6_2_bitmap)
		.byte >(freeRamEnding+council_frames_magenta_6_1_bitmap)

screen_lo:
		.byte <(freeRamEnding+council_frames_magenta_0_0_screen)
		.byte <(freeRamEnding+council_frames_magenta_0_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_0_2_screen)
		.byte <(freeRamEnding+council_frames_magenta_0_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_1_0_screen)
		.byte <(freeRamEnding+council_frames_magenta_1_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_1_2_screen)
		.byte <(freeRamEnding+council_frames_magenta_1_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_2_0_screen)
		.byte <(freeRamEnding+council_frames_magenta_2_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_2_2_screen)
		.byte <(freeRamEnding+council_frames_magenta_2_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_3_0_screen)
		.byte <(freeRamEnding+council_frames_magenta_3_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_3_2_screen)
		.byte <(freeRamEnding+council_frames_magenta_3_3_screen)
		.byte <(freeRamEnding+council_frames_magenta_4_0_screen)
		.byte <(freeRamEnding+council_frames_magenta_4_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_4_2_screen)
		.byte <(freeRamEnding+council_frames_magenta_4_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_5_0_screen)
		.byte <(freeRamEnding+council_frames_magenta_5_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_5_2_screen)
		.byte <(freeRamEnding+council_frames_magenta_5_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_6_0_screen)
		.byte <(freeRamEnding+council_frames_magenta_6_1_screen)
		.byte <(freeRamEnding+council_frames_magenta_6_2_screen)
		.byte <(freeRamEnding+council_frames_magenta_6_1_screen)

screen_hi:
		.byte >(freeRamEnding+council_frames_magenta_0_0_screen)
		.byte >(freeRamEnding+council_frames_magenta_0_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_0_2_screen)
		.byte >(freeRamEnding+council_frames_magenta_0_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_1_0_screen)
		.byte >(freeRamEnding+council_frames_magenta_1_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_1_2_screen)
		.byte >(freeRamEnding+council_frames_magenta_1_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_2_0_screen)
		.byte >(freeRamEnding+council_frames_magenta_2_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_2_2_screen)
		.byte >(freeRamEnding+council_frames_magenta_2_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_3_0_screen)
		.byte >(freeRamEnding+council_frames_magenta_3_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_3_2_screen)
		.byte >(freeRamEnding+council_frames_magenta_3_3_screen)
		.byte >(freeRamEnding+council_frames_magenta_4_0_screen)
		.byte >(freeRamEnding+council_frames_magenta_4_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_4_2_screen)
		.byte >(freeRamEnding+council_frames_magenta_4_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_5_0_screen)
		.byte >(freeRamEnding+council_frames_magenta_5_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_5_2_screen)
		.byte >(freeRamEnding+council_frames_magenta_5_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_6_0_screen)
		.byte >(freeRamEnding+council_frames_magenta_6_1_screen)
		.byte >(freeRamEnding+council_frames_magenta_6_2_screen)
		.byte >(freeRamEnding+council_frames_magenta_6_1_screen)

d800_lo:
		.byte <(freeRamEnding+council_frames_magenta_0_0_d800)
		.byte <(freeRamEnding+council_frames_magenta_0_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_0_2_d800)
		.byte <(freeRamEnding+council_frames_magenta_0_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_1_0_d800)
		.byte <(freeRamEnding+council_frames_magenta_1_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_1_2_d800)
		.byte <(freeRamEnding+council_frames_magenta_1_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_2_0_d800)
		.byte <(freeRamEnding+council_frames_magenta_2_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_2_2_d800)
		.byte <(freeRamEnding+council_frames_magenta_2_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_3_0_d800)
		.byte <(freeRamEnding+council_frames_magenta_3_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_3_2_d800)
		.byte <(freeRamEnding+council_frames_magenta_3_3_d800)
		.byte <(freeRamEnding+council_frames_magenta_4_0_d800)
		.byte <(freeRamEnding+council_frames_magenta_4_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_4_2_d800)
		.byte <(freeRamEnding+council_frames_magenta_4_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_5_0_d800)
		.byte <(freeRamEnding+council_frames_magenta_5_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_5_2_d800)
		.byte <(freeRamEnding+council_frames_magenta_5_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_6_0_d800)
		.byte <(freeRamEnding+council_frames_magenta_6_1_d800)
		.byte <(freeRamEnding+council_frames_magenta_6_2_d800)
		.byte <(freeRamEnding+council_frames_magenta_6_1_d800)

d800_hi:
		.byte >(freeRamEnding+council_frames_magenta_0_0_d800)
		.byte >(freeRamEnding+council_frames_magenta_0_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_0_2_d800)
		.byte >(freeRamEnding+council_frames_magenta_0_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_1_0_d800)
		.byte >(freeRamEnding+council_frames_magenta_1_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_1_2_d800)
		.byte >(freeRamEnding+council_frames_magenta_1_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_2_0_d800)
		.byte >(freeRamEnding+council_frames_magenta_2_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_2_2_d800)
		.byte >(freeRamEnding+council_frames_magenta_2_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_3_0_d800)
		.byte >(freeRamEnding+council_frames_magenta_3_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_3_2_d800)
		.byte >(freeRamEnding+council_frames_magenta_3_3_d800)
		.byte >(freeRamEnding+council_frames_magenta_4_0_d800)
		.byte >(freeRamEnding+council_frames_magenta_4_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_4_2_d800)
		.byte >(freeRamEnding+council_frames_magenta_4_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_5_0_d800)
		.byte >(freeRamEnding+council_frames_magenta_5_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_5_2_d800)
		.byte >(freeRamEnding+council_frames_magenta_5_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_6_0_d800)
		.byte >(freeRamEnding+council_frames_magenta_6_1_d800)
		.byte >(freeRamEnding+council_frames_magenta_6_2_d800)
		.byte >(freeRamEnding+council_frames_magenta_6_1_d800)
.endproc
