.include "../global.inc"

.segment "ENDING_BSS"
.res 1

.segment "MUSIC"
.include "../converters/music/eye-outro.inc"
.proc decrunchMusic
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<music
		ldx #>music
		jsr decrunchTo
		rts
music:	.incbin "../converters/music/eye-outro.prg.b2",2
.endproc

.segment "ENDING_RAM_INIT"
endingRamInit:
		memcpy __ENDING_RAM_RUN__, __ENDING_RAM_LOAD__, __ENDING_RAM_SIZE__
		jmp ending_generateBlitTables

.segment "ENDING"
.proc _newScene
		jsrf newScene
		rts
.endproc

.export ending
.proc ending
		lda #POINTERENABLED_NO
		sta pointerEnabled
		jsrf endingRamInit
		
		jsrf decrunchMusic

		lda system
		cmp #SYSTEM_PAL
		bne :++
			lda sidModel
			bne :+
				memcpy $9000, freeRam+eye_pal_outro_6581_fe, eye_pal_outro_6581_fe_size
				jmp musicInstalled
			:
				memcpy $9000, freeRam+eye_pal_outro_8580_fe, eye_pal_outro_8580_fe_size
				jmp musicInstalled
		:
			lda sidModel
			bne :+
				memcpy $9000, freeRam+eye_NTSC_outro_6581_fe, eye_NTSC_outro_6581_fe_size
				jmp musicInstalled
			:
				memcpy $9000, freeRam+eye_NTSC_outro_8580_fe, eye_NTSC_outro_8580_fe_size

		musicInstalled:
			jsr initMusic
			lda #15
			sta musicVolume
			lda #1
			sta musicEnabled

		jsrf checkAbortConsumeEvents

		jsr initTextEnding

		jmp council_scene
.endproc
