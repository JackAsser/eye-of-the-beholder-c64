.include "zp.auto.inc"

.define VERSION "1.00"

SAVEGAME_START = (__GAMESTATEM_RUN__ - $600)
SAVEGAME_END = (__QUICKSWAP_RUN__ + __QUICKSWAP_SIZE__)
SAVEGAME_SIZE = (SAVEGAME_END - SAVEGAME_START + 1)

;
; --- Memory map for the VIC bank ---
;
.define vic_sprites	 $c000 ;- $ca7f (7*6 sprites)
.define vic_sprites_size	$a80
.define automap_font $c000 ;- (128 bytes, 16 chars)
.define automap_d800 $c200 ;- $c5e7
.define automap_spr  $c600 ;- $c63f
.define mousepointer $ca80 ;- $cabf
.define emptysprite  $cac0 ;- $caff
.define backup_zp	 $cb00
.define backup_spr   $cb00 ;- $cbff (256 bytes)
.define vic_screen	 $cc00 ;- $cfe7
; (free)	         $cfe8 ;- $cff7 (16 bytes)
.define spriteptrs	 $cff8 ;- $cfff
.define vic_font     $d000 ;- $d3ff (1024 bytes, 128 chars)
.define visitedTiles $d400 ;- $d9ff (1536 bytes)
.define camp_spr	 $da00 ;- $db3f (120 bytes)
; (free)			 $da00 ;- $dfff (1536 bytes)
.define vic_bitmap	 $e000 ;- $fb7f (only the first 22 rows are used)
.define d800_backup  $fc00+40 ;- $fd8f+40 (Ten last rows of d800 backedup)
.define vic_screen2  $fc00 ;- $ffe7
; (free)			 $ffe8 ;- $fff7 (16 bytes)
.define spriteptrs2  $fff8 ;- $ffff
; (vectors)          $fffa ;- $ffff

.define SYSTEM_PAL		0
.define SYSTEM_NTSC		1
.define SYSTEM_NTSC_OLD	2
.define SYSTEM_DREAN	3
.define SYSTEM_UNKNOWN	$ff

.define SIDMODEL_6581	0
.define SIDMODEL_8580	1

.define SPRITE_COL0	$09	;%01 ;$d025
.define SPRITE_COL1	$0c	;%10 ;$d027
.define SPRITE_COL2	$0f	;%11 ;$d026
.define MOUSE_COLOR $01		 ;$d027+1

.define SCREEN_MODE_normal	0
.define SCREEN_MODE_text	1
.define SCREEN_MODE_mainmenu	2
.define SCREEN_MODE_text_header	3

.define MOUSETYPE_1351_ACC		0
.define MOUSETYPE_1351_NOACC	1
.define MOUSETYPE_NONE			2
.define MOUSETYPE_MAX			3

.define POINTERENABLED_NO		0
.define POINTERENABLED_ANY		1
.define POINTERENABLED_REAL		2

.define NORTH		0
.define EAST		1
.define SOUTH		2
.define WEST		3
.define NW			0
.define NE			1
.define SW			2
.define SE			3
.define CENTER		4
.define FLYING		4
.define COMPARTMENT	8

.define WALLFLAG_PASSPARTY		$01
.define WALLFLAG_PASSSMALL		$02
.define WALLFLAG_BLOCKMONSTER	$04
.define WALLFLAG_ISDOOR			$08
.define WALLFLAG_DOOROPEN		$10
.define WALLFLAG_DOORCLOSED		$20
.define WALLFLAG_UNDEFINED		$40
.define WALLFLAG_SHOWITEMS		$80

.struct ScreenMode
	setup				.word
	teardown			.word
	splitLength			.byte
	splitHandlers		.word ; [position, handler]*splitLength
.endstruct

.struct KeyEvent
	keyPressed 			.byte
	normalChar			.byte
	specialChar			.byte
	modifiers			.byte	
.endstruct

.struct MouseEvent
	buttons				.byte
	xpos				.word
	ypos				.byte			
.endstruct

.struct Timer
	handler 				.word
	handlerBank				.byte
	timeout 				.dword
	argument 				.word
	parameter 				.byte
	active 					.byte
	absoluteTimeout			.dword
.endstruct
.macro createTimer handler, timeout, argument, parameter, active
	.scope
		t0 = timeout&$ff
		t1 = (timeout>>8)&$ff
		t2 = (timeout>>16)&$ff
		t3 = (timeout>>24)&$ff
	.byte <handler, >handler, <.bank(handler), t0,t1,t2,t3, <argument, >argument, parameter, active, t0,t1,t2,t3
	.endscope
.endmacro

.struct Thrown
	active 					.byte
	movedOnlyWithinSubpos 	.byte
	position 				.word
	subpos 					.byte
	direction 				.byte
	itemOrSpellIndex 		.word
	spellGfxIndexOrItemType	.byte
	caster 					.byte
	flags 					.byte
	range 					.byte
	spellHandler 			.word
.endstruct

.struct MonsterCreate
	levelType 				.byte
	position 				.word
	subpos 					.byte
	direction 				.byte
	type 					.byte
	picture 				.byte
	phase 					.byte
	pause 					.byte
	weapon 					.byte
	pocketItem 				.byte	
.endstruct

.struct Item
	nameUnidentified 		.byte
	nameIdentified 			.byte
	flags 					.byte
	picture 				.byte
	type 					.byte
	subpos 					.byte
	position	 			.word
	level 					.byte
	value 					.byte
	previous 				.word
	next 					.word
.endstruct

.struct ItemType
	inventoryBits			.word
	;handBits				.word
	armourClassModifier		.byte
	classBits				.byte
	doubleHanded			.byte
	damageVsSmall			.res 3
	damageVsBig				.res 3
	usage 					.byte
.endstruct

.struct Spell
	type 					.byte
	range 					.byte
	flags 					.byte
	spellGfxIndex 			.byte
.endstruct

.struct SpellType
	usage 					.byte
	soundEffect				.byte
	castFunc				.word
	hitFunc					.word
	expiredFunc				.word
.endstruct

.struct PartyMember
	position				.byte
	status					.byte
	name 					.res 11
	strengthCurrent			.byte
	strength 				.byte
	extraStrengthCurrent 	.byte
	extraStrength 			.byte
	intelligenceCurrent 	.byte
	intelligence 			.byte
	wisdomCurrent 			.byte
	wisdom 					.byte
	dexterityCurrent 		.byte
	dexterity 				.byte
	constitutionCurrent 	.byte
	constitution 			.byte
	charismaCurrent 		.byte
	charisma 				.byte
	hpCurrent 				.word
	hp 						.word
	ac 						.byte
	disabledHands 			.byte
	race 					.byte
	class 					.byte
	alignment 				.byte
	picture 				.byte
	food 					.byte
	level 					.res 3
	experience 				.res 4*3
	unknown 				.byte
	mageSpells 				.res 5*6
	clericSpells 			.res 5*6
	scribedScrolls 			.dword
	inventory 				.res 28*2
	timeouts 				.res 10*4
	timeoutActions 			.res 10
	field_DF 				.byte
	attackSaves 			.byte
	field_E1 				.byte
	temporaryHP 			.byte
	activeSpells			.word
	damageInfo				.word
	hitInfo					.res 2*2
	magicalGlimmer			.byte
	flags					.res 7
.endstruct

.define THROWNFLAGS_CENTERED			$40

.define ATTACK_MISS						-1
.define ATTACK_HACK						-2
.define ATTACK_CANTREACH				-3
.define ATTACK_NOAMMO					-4

.define ITEM_FLAGS_REGAINHP				$08
.define ITEM_FLAGS_CHARGEMASK			$1f
.define ITEM_FLAGS_CONSUMABLE			$10
.define ITEM_FLAGS_CURSED				$20
.define ITEM_FLAGS_IDENTIFIED			$40
.define ITEM_FLAGS_MAGICAL				$80

.define MULTICLASS_FIGHTER 				$00
.define MULTICLASS_RANGER 				$01
.define MULTICLASS_PALADIN   			$02
.define MULTICLASS_MAGE 				$03
.define MULTICLASS_CLERIC 				$04
.define MULTICLASS_THIEF 				$05
.define MULTICLASS_FIGHTER_CLERIC 		$06
.define MULTICLASS_FIGHTER_THIEF 		$07
.define MULTICLASS_FIGHTER_MAGE 		$08
.define MULTICLASS_FIGHTER_MAGE_THIEF 	$09
.define MULTICLASS_THIEF_MAGE 			$0a
.define MULTICLASS_CLERIC_THIEF  		$0b
.define MULTICLASS_FIGHTER_CLERIC_MAGE 	$0c
.define MULTICLASS_RANGER_CLERIC 		$0d
.define MULTICLASS_CLERIC_MAGE 			$0e

.define CLASS_FIGHTER					$00
.define CLASS_MAGE						$01
.define CLASS_CLERIC					$02
.define CLASS_THIEF						$03
.define CLASS_PALADIN					$04
.define CLASS_RANGER					$05

.define IS_FIGHTER						$01
.define IS_MAGE							$02
.define IS_CLERIC						$04
.define IS_THIEF						$08

.define RACE_HUMAN						$00
.define RACE_ELF						$01
.define RACE_HALFELF					$02
.define RACE_DWARF						$03
.define RACE_GNOME						$04
.define RACE_HALFLING					$05

.define SEX_MALE						$00
.define SEX_FEMALE						$01

.define CLERIC_KNOWN_SPELLS				$007bffff
.define PALADIN_KNOWN_SPELLS			$00800ff2

.define SPELLFLAGS_FIRSTONPOSITION   	$01
.define SPELLFLAGS_ALLONPOSITION     	$02
.define SPELLFLAGS_ALWAYSHIT         	$04
.define SPELLFLAGS_DAMAGEREDUCTION   	$08
.define SPELLFLAGS_CANHITCASTER      	$10
.define SPELLFLAGS_EXPLOSION         	$20
.define SPELLFLAGS_DAMAGEEXPLOSION   	$40

.define SPELLTYPEUSAGE_ASKWHOM			$01
.define SPELLTYPEUSAGE_FREEHANDNEEDED	$02
.define SPELLTYPEUSAGE_AFFECTSINGLE		$04
.define SPELLTYPEUSAGE_AFFECTSALL		$08
.define SPELLTYPEUSAGE_DEACTIVATEINVISIBILITY $10

.define ALIGNMENT_LAWFUL_GOOD			$00
.define ALIGNMENT_NEUTRAL_GOOD			$01
.define ALIGNMENT_CHAOTIC_GOOD			$02
.define ALIGNMENT_LAWFUL_NEUTRAL		$03
.define ALIGNMENT_TRUE_NEUTRAL			$04
.define ALIGNMENT_CHAOTIC_NEUTRAL		$05
.define ALIGNMENT_LAWFUL_EVIL			$06
.define ALIGNMENT_NEUTRAL_EVIL			$07
.define ALIGNMENT_CHAOTIC_EVIL			$08

.define SPELL_DETECT_MAGIC				$0001
.define SPELL_SHIELD					$0002
.define SPELL_INVISIBLE					$0004
.define SPELL_HASTE 					$0008
.define SPELL_BLESS						$0010
.define SPELL_PRAYER					$0020
.define SPELL_AID 						$0040
.define SPELL_SLOW_POISON 				$0080
.define SPELL_MAGICAL_VESTMENT 			$0100
.define SPELL_PROTECTION_FROM_EVIL		$0200
.define SPELL_UNKNOWN 					$0400

.define PARTYMEMBER_STATUS_ACTIVE		$01
.define PARTYMEMBER_STATUS_POISONED		$02
.define PARTYMEMBER_STATUS_PARALYZED	$04

.define IS_ACTIVE						$01
.define IS_ALIVE						$02
.define IS_CONSCIOUS 					$04
.define NOT_PARALYZED					$08
.define NOT_POISONED					$10

.define TIMERMODE_TICKS					$01
.define TIMERMODE_STEPS					$02

.define TRIGGER_PLAYERENTER    			$01
.define TRIGGER_PLAYERLEAVE    			$02
.define TRIGGER_ITEMDROPPED    			$04
.define TRIGGER_ITEMTAKEN      			$08
.define TRIGGER_THROWNHITSWALL 			$10
.define TRIGGER_CREATEMONSTER  			$20
.define TRIGGER_PLAYERUSESWALL 			$40
.define TRIGGER_TIMER          			$80

.define MONSTER_STATE_PREPARE_ATTACK	-2
.define MONSTER_STATE_ATTACK  			-1
.define MONSTER_STATE_FORWARD 		 	0
.define MONSTER_STATE_BACKWARD 			1
.define MONSTER_STATE_LEFT 		 		2
.define MONSTER_STATE_RIGHT 		 	3
.define MONSTER_STATE_ADJTURN 		 	4
.define MONSTER_STATE_TURNLEFT 			5
.define MONSTER_STATE_TURNRIGHT 		6
.define MONSTER_STATE_READYHIT 			7
.define MONSTER_STATE_INACTIVE 			8
.define MONSTER_STATE_LEGWALK 		 	9
.define MONSTER_STATE_ISHIT  			10

.define MONSTER_FLAGS_ACTIVE 			$01
.define MONSTER_FLAGS_HIT 				$02
.define MONSTER_FLAGS_FLIP				$04
.define MONSTER_FLAGS_TURNED			$08
.define MONSTER_FLAGS_TRYCASTTURNSPELL	$10
.define MONSTER_FLAGS_STONE				$20
.define MONSTER_FLAGS_INACT 			$40

.define MONSTER_TYPEBITS_LO_QUARTERSIZE 	$00
.define MONSTER_TYPEBITS_LO_BIGSIZE 		$01
.define MONSTER_TYPEBITS_LO_SMALLSIZE 		$02
.define MONSTER_TYPEBITS_LO_REDUCEDAMAGE   	$04
.define MONSTER_TYPEBITS_LO_TWOATTACKS     	$08
.define MONSTER_TYPEBITS_LO_POISONUS       	$10
.define MONSTER_TYPEBITS_LO_MINDBLAST      	$20
.define MONSTER_TYPEBITS_LO_RANGEATTACK    	$40
.define MONSTER_TYPEBITS_LO_STEAL          	$80
.define MONSTER_TYPEBITS_HI_INVISIBLE      	$01
.define MONSTER_TYPEBITS_HI_BADSIGHT       	$02
.define MONSTER_TYPEBITS_HI_UNKNOWN2       	$04
.define MONSTER_TYPEBITS_HI_EYESFRONT      	$08
.define MONSTER_TYPEBITS_HI_CANOPENDOOR    	$10
.define MONSTER_TYPEBITS_HI_NOSPELLKILL    	$20
.define MONSTER_TYPEBITS_HI_MULTIATTACK    	$40
.define MONSTER_TYPEBITS_HI_TURNTOSTONE    	$80

.define MONSTER_ATTACKBITS_ISPERSON 	  	$01
.define MONSTER_ATTACKBITS_ISMONSTER 	  	$02

.define INVENTORY_LEFT_HAND     			$00
.define INVENTORY_RIGHT_HAND    			$01
.define INVENTORY_BACKPACK      			$02
.define INVENTORY_QUIVER        			$10
.define INVENTORY_BREAST_ARMOUR 			$11
.define INVENTORY_BRACERS       			$12
.define INVENTORY_HELMET        			$13
.define INVENTORY_NECKLACE      			$14
.define INVENTORY_BOOTS         			$15
.define INVENTORY_BELT          			$16
.define INVENTORY_LEFT_RING     			$19
.define INVENTORY_RIGHT_RING    			$1a
.define INVENTORY_SIZE          			$1b

.define PMTA_RESET_DISABLED_LEFT_HAND               0
.define PMTA_RESET_DISABLED_RIGHT_HAND              1
.define PMTA_RESET_HIT_INFO_LEFT_HAND               2
.define PMTA_RESET_HIT_INFO_RIGHT_HAND              3
.define PMTA_RESET_DISABLED_AND_HIT_INFO_LEFT_HAND  4
.define PMTA_RESET_DISABLED_AND_HIT_INFO_RIGHT_HAND 5
.define PMTA_RESET_DAMAGE_INFO                      6
.define PMTA_RESET_GIANT_STRENGTH                   7
.define PMTA_POISONED_CHARACTER                     8
.define PMTA_RESET_PARALYZED                        9

; Item usages
.define ITEMUSAGE_ARMOR						0
.define ITEMUSAGE_MELEE_WEAPONS				1
.define ITEMUSAGE_THROWABLES				2
.define ITEMUSAGE_RANGE_WEAPONS				3
.define ITEMUSAGE_SPECIAL					4
.define ITEMUSAGE_SPELLBOOKS				5
.define ITEMUSAGE_HOLYSYMBOLS				6
.define ITEMUSAGE_FOOD						7
.define ITEMUSAGE_BONES						8
.define ITEMUSAGE_MAGESCROLLS				9
.define ITEMUSAGE_CLERICSCROLLS				10
.define ITEMUSAGE_NOTES						11
.define ITEMUSAGE_STONEITEMS				12
.define ITEMUSAGE_KEYS						13
.define ITEMUSAGE_POTIONS					14
.define ITEMUSAGE_GEMS						15
.define ITEMUSAGE_RINGS						16
.define ITEMUSAGE_NOTUSED					17
.define ITEMUSAGE_WANDS						18

; Item types
.define ITEMTYPE_AXE						0
.define ITEMTYPE_LONG_SWORD					1
.define ITEMTYPE_SHORT_SWORD				2
.define ITEMTYPE_ORB						3
.define ITEMTYPE_DART						4
.define ITEMTYPE_DAGGER						5
.define ITEMTYPE_DWARVEN_POTION				6
.define ITEMTYPE_BOW						7
.define ITEMTYPE_8							8
.define ITEMTYPE_SPEAR						9
.define ITEMTYPE_HALBERD					10
.define ITEMTYPE_MACE 						11
.define ITEMTYPE_FLAIL						12
.define ITEMTYPE_STAFF						13
.define ITEMTYPE_SLING						14
.define ITEMTYPE_DART2						15
.define ITEMTYPE_ARROE						16
.define ITEMTYPE_17							17
.define ITEMTYPE_ROCK						18
.define ITEMTYPE_BANDED_ARMOR				19
.define ITEMTYPE_CHAINMAIL					20
.define ITEMTYPE_HELMET						21
.define ITEMTYPE_LEATHER_ARMOR				22
.define ITEMTYPE_23							23
.define ITEMTYPE_PLATEMAIL					24
.define ITEMTYPE_SCALEMAIL					25
.define ITEMTYPE_26							26
.define ITEMTYPE_SHIELD						27
.define ITEMTYPE_LOCKPICKS					28
.define ITEMTYPE_SPELLBOOK					29
.define ITEMTYPE_HOLYSYMBOL					30
.define ITEMTYPE_RATION						31
.define ITEMTYPE_BOOTS						32
.define ITEMTYPE_BONES						33
.define ITEMTYPE_MAGE_SCROLL				34
.define ITEMTYPE_CLERIC_SCROLL				35
.define ITEMTYPE_NOTE						36
.define ITEMTYPE_STONESYMBOL				37
.define ITEMTYPE_KEY						38
.define ITEMTYPE_POTION						39
.define ITEMTYPE_GEM						40
.define ITEMTYPE_ROBE						41
.define ITEMTYPE_RING						42
.define ITEMTYPE_BRACERS					43
.define ITEMTYPE_NECKLACE					44
.define ITEMTYPE_45							45
.define ITEMTYPE_46							46
.define ITEMTYPE_RING2						47
.define ITEMTYPE_WAND						48
.define ITEMTYPE_EGG						49

; Rings (usage 16)
.define RING_OF_ADORNMENT					0
.define RING_OF_WIZARDRY					1
.define RING_OF_SUSTENANCE 					2
.define RING_OF_FEATHER_FALL				3

; Potions (usage 14)
.define POTION_OF_GIANT_STRENGTH			1
.define POTION_OF_HEALING					2
.define POTION_OF_EXTRA_HEALING				3
.define POTION_OF_POISON					4
.define POTION_OF_VITALITY					5
.define POTION_OF_SPEED						6
.define POTION_OF_INVISIBILITY				7
.define POTION_OF_CURE_POISON				8 ;Also Dwarven healing potion

; Wands (usage 18)
.define WAND 								0
.define WAND_OF_LIGHTNING					1
.define WAND_OF_FROST						2
.define WAND_OF_FIREBALLS					4
.define WAND_OF_SILVIAS						5
.define WAND_OF_MAGIC_MISSILE				6

; Keys (usage 13)
.define SILVER_KEY							1
.define GOLD_KEY							2
.define DWARVEN_KEY							3
.define KEY 								4
.define SKULL_KEY							5
.define DROW_KEY							6
.define JEWELED_KEY							7
.define RUBY_KEY							8

; Stone items (usage 12)
.define STONE_HOLY_SYMBOL					1
.define STONE_NECKLACE						2
.define STONE_ORB							3
.define STONE_DAGGER						4
.define STONE_MEDALLION						5
.define STONE_SCEPTER						6
.define STONE_RING							7


.macro getPartyMember index
	ldx #index
	lda partyMembers_lo,x
	sta CURRENT_PARTYMEMBER+0
	lda partyMembers_hi,x
	sta CURRENT_PARTYMEMBER+1
.endmacro

.macro _jsr dest
	.assert .bank(*) = .bank(dest), error, "JSR to another bank is not allowed."
	jsr dest
.endmacro

.macro jsrf dest,bank
.scope
	lda #>(return-1)
	pha
	lda #<(return-1)
	pha
	lda BANK
	pha
	lda #>(jsrf_impl-1)
	pha
	lda #<(jsrf_impl-1)
	pha
	lda #>(dest-1)
	pha
	lda #<(dest-1)
	pha
	.ifnblank bank
		lda bank
	.else
		lda #<.bank(dest)
	.endif
	pha
	jmp jsrf_impl
	return:
.endscope
.endmacro

.macro memset dst,value,size
	lda #<(dst)
	sta MDST+0
	lda #>(dst)
	sta MDST+1
	lda #<(size)
	ldx #>(size)
	ldy #(value)
	jsr _memset
.endmacro

.macro memset_underio dst,value,size
	lda #<(dst)
	sta MDST+0
	lda #>(dst)
	sta MDST+1
	lda #<(size)
	ldx #>(size)
	ldy #(value)
	jsr _memset_underio
.endmacro

.macro memcpy dst,src,size
	lda #<(src)
	sta MSRC+0
	lda #>(src)
	sta MSRC+1
	lda #<(dst)
	sta MDST+0
	lda #>(dst)
	sta MDST+1
	lda #<(size)
	ldx #>(size)
	jsr _memcpy
.endmacro

.macro memcpy_ram dst,src,size
	lda #<(src)
	sta MSRC+0
	lda #>(src)
	sta MSRC+1
	lda #<(dst)
	sta MDST+0
	lda #>(dst)
	sta MDST+1
	lda #<(size)
	ldx #>(size)
	jsr _memcpy_ram
.endmacro

.macro memcpy_underio dst,src,size
	lda #<(src)
	sta MSRC+0
	lda #>(src)
	sta MSRC+1
	lda #<(dst)
	sta MDST+0
	lda #>(dst)
	sta MDST+1
	lda #<(size)
	ldx #>(size)
	jsr _memcpy_ram_underio
.endmacro

.macro memcpy_pureram dst,src,size
	lda #<(src)
	sta MSRC+0
	lda #>(src)
	sta MSRC+1
	lda #<(dst)
	sta MDST+0
	lda #>(dst)
	sta MDST+1
	lda #<(size)
	ldx #>(size)
	jsr _memcpy_pureram
.endmacro

.macro memcpy_bank dst,src,bank,size
	lda #<(src)
	sta MSRC+0
	lda #>(src)
	sta MSRC+1
	lda #<(dst)
	sta MDST+0
	lda #>(dst)
	sta MDST+1
	lda #<(size)
	ldx #>(size)
	ldy #(bank)
	jsr _memcpy_bank
.endmacro

.macro jmpf dest,bank
.scope
	lda #>(dest-1)
	pha
	lda #<(dest-1)
	pha
	.ifnblank bank
		lda bank
	.else
		lda #<.bank(dest)
	.endif
	pha
	jmp jsrf_impl
.endscope
.endmacro

.macro dice rolls,sides,base
	lda #rolls
	sta DICE_ROLLS
	lda #sides
	sta DICE_SIDES
	lda #base
	sta DICE_BASE
.endmacro

.macro bitblt gfx,width,height,dx,dy
.scope
	_bitmap = gfx
	_screen = _bitmap + width*height*8
	_d800 = _screen + width*height

	lda #<_bitmap
	sta SRC+0
	lda #>_bitmap
	sta SRC+1
	lda #<_screen
	sta SRC2+0
	lda #>_screen
	sta SRC2+1
	lda #<_d800
	sta SRC3+0
	lda #>_d800
	sta SRC3+1
	lda #(width*8)
	sta TMP2+0
	lda #(width)
	sta TMP2+1
	ldx #dx
	ldy #dy
	lda #height-1
	jsr _bitblt
.endscope
.endmacro

.macro grab gfx,width,height,dx,dy
.scope
	_bitmap = gfx
	_screen = _bitmap + width*height*8
	_d800 = _screen + width*height

	lda #<_bitmap
	sta SRC+0
	lda #>_bitmap
	sta SRC+1
	lda #<_screen
	sta SRC2+0
	lda #>_screen
	sta SRC2+1
	lda #<_d800
	sta SRC3+0
	lda #>_d800
	sta SRC3+1
	lda #(width*8)
	sta TMP2+0
	lda #(width)
	sta TMP2+1
	ldx #dx
	ldy #dy
	lda #height-1
	jsr _grab
.endscope
.endmacro

.macro bitblt2 gfx,width,height
.scope
	_bitmap = gfx
	_screen = _bitmap + width*height*8
	_d800 = _screen + width*height

	lda #<_bitmap
	sta SRC+0
	lda #>_bitmap
	sta SRC+1
	lda #<_screen
	sta SRC2+0
	lda #>_screen
	sta SRC2+1
	lda #<_d800
	sta SRC3+0
	lda #>_d800
	sta SRC3+1
	lda #(width*8)
	sta TMP2+0
	lda #(width)
	sta TMP2+1
	lda #height-1
	jsr _bitblt
.endscope
.endmacro

.macro CALC_D018 font, screen
				.byte <(((font/$400)&$e) | (((screen/$400)&$f)<<4))
.endmacro

.macro CALC_DD00 font
				.byte <((font/$4000)^3)
.endmacro

.macro SET_D018 font, screen
				lda #<(((font/$400)&$e) | (((screen/$400)&$f)<<4))
				sta $d018
.endmacro

.macro SET_DD00 font
				lda #<((font/$4000)^3)
				sta $dd00
.endmacro

