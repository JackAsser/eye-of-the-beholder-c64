.include "global.inc"

.segment "MAIN"
.export doorTimerHandler
.proc doorTimerHandler
	.pushseg
	.segment "BSS"
		doorMoveDirection: .res 1
	.popseg

	argument = ARGS+0

	; North-south or east-west door
	ldy #0
	lax (argument),y
	lda wallFlags,x
	ldy #0
	and #WALLFLAG_ISDOOR
	bne :+
		iny
	:

	; if (((doorMoveDirection==-1) && ((wm.flags&WallMapping.DOORCLOSED)!=0)) ||
	;  ((doorMoveDirection== 1) && ((wm.flags&WallMapping.DOOROPEN)!=0)) )
	lax (argument),y
	lda wallFlags,x
	pha
	ldx #Timer::parameter
	lda doorTimer,x
	sta doorMoveDirection
	bpl :+
		pla
		and #WALLFLAG_DOORCLOSED
		beq :++
			jmp stopTimer
	:
		pla
		and #WALLFLAG_DOOROPEN
		beq :+
			jmp stopTimer
	:

	;mb.walls[direction+0] += doorMoveDirection;
	clc
	lda doorMoveDirection
	adc (argument),y
	sta (argument),y

	lax (argument),y
	lda wallFlags,x
	pha

	;mb.walls[direction+2] += doorMoveDirection;
	iny
	iny
	clc
	lda doorMoveDirection
	adc (argument),y
	sta (argument),y
	dey
	dey

	; Convert mazeBlock to position
	sec
	lda ARGS+0
	sbc #<maze
	sta ARGS+0
	lda ARGS+1
	sbc #>maze
	lsr
	ror ARGS+0
	lsr
	ror ARGS+0
	sta ARGS+1

	jsrf gameState_isPositionVisible

	;int soundIndex = ((wm.flags&WallMapping.DOORCLOSED)!=0) ? 5 : ((doorMoveDirection!=-1) ? 4 : 3);
	pla
	and #WALLFLAG_DOORCLOSED
	beq :+
		ldx #5
		jmp :++
	:
		ldx #4
		lda doorMoveDirection
		bpl :+
			ldx #3
	:
	stx ARGS+0
	jsrf audio_playSoundAtPartyPosition

	rts

stopTimer:
	ldx #Timer::active
	lda #0
	sta doorTimer,x
	rts
.endproc
