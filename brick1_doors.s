.segment "BRICK1_DOORS"
.export brick1_doors
brick1_doors:

jmp renderDoor

doorGraphicsBitmap:
	.incbin "converters/doors/brick1_data.bin"

offsets_lo:
	.incbin "converters/doors/brick1_offsets_lo.bin"

offsets_hi:
	.incbin "converters/doors/brick1_offsets_hi.bin"

screen:
	.incbin "converters/doors/brick1_screen.bin"

d800:
	.incbin "converters/doors/brick1_d800.bin"

doorGraphicsWidth:
	.byte $0c,$0c,$0c,$0c,$0c,$0c,$09,$09,$09,$09,$09,$09,$06,$06,$06,$06,$06,$06

doorGraphicsHeight:
	.byte $0b,$0b,$0b,$0b,$0b,$0b,$06,$06,$06,$06,$06,$06,$05,$05,$05,$05,$05,$05

doorGraphicsOffsetsLo_lo:
	.byte <(offsets_lo+$0000),<(offsets_lo+$0084),<(offsets_lo+$0108),<(offsets_lo+$018c),<(offsets_lo+$0210),<(offsets_lo+$0294),<(offsets_lo+$0318),<(offsets_lo+$034e),<(offsets_lo+$0384),<(offsets_lo+$03ba),<(offsets_lo+$03f0),<(offsets_lo+$0426),<(offsets_lo+$045c),<(offsets_lo+$047a),<(offsets_lo+$0498),<(offsets_lo+$04b6),<(offsets_lo+$04d4),<(offsets_lo+$04f2)

doorGraphicsOffsetsLo_hi:
	.byte >(offsets_lo+$0000),>(offsets_lo+$0084),>(offsets_lo+$0108),>(offsets_lo+$018c),>(offsets_lo+$0210),>(offsets_lo+$0294),>(offsets_lo+$0318),>(offsets_lo+$034e),>(offsets_lo+$0384),>(offsets_lo+$03ba),>(offsets_lo+$03f0),>(offsets_lo+$0426),>(offsets_lo+$045c),>(offsets_lo+$047a),>(offsets_lo+$0498),>(offsets_lo+$04b6),>(offsets_lo+$04d4),>(offsets_lo+$04f2)

doorGraphicsOffsetsHi_lo:
	.byte <(offsets_hi+$0000),<(offsets_hi+$0084),<(offsets_hi+$0108),<(offsets_hi+$018c),<(offsets_hi+$0210),<(offsets_hi+$0294),<(offsets_hi+$0318),<(offsets_hi+$034e),<(offsets_hi+$0384),<(offsets_hi+$03ba),<(offsets_hi+$03f0),<(offsets_hi+$0426),<(offsets_hi+$045c),<(offsets_hi+$047a),<(offsets_hi+$0498),<(offsets_hi+$04b6),<(offsets_hi+$04d4),<(offsets_hi+$04f2)

doorGraphicsOffsetsHi_hi:
	.byte >(offsets_hi+$0000),>(offsets_hi+$0084),>(offsets_hi+$0108),>(offsets_hi+$018c),>(offsets_hi+$0210),>(offsets_hi+$0294),>(offsets_hi+$0318),>(offsets_hi+$034e),>(offsets_hi+$0384),>(offsets_hi+$03ba),>(offsets_hi+$03f0),>(offsets_hi+$0426),>(offsets_hi+$045c),>(offsets_hi+$047a),>(offsets_hi+$0498),>(offsets_hi+$04b6),>(offsets_hi+$04d4),>(offsets_hi+$04f2)

doorGraphicsOffsetsScreen_lo:
	.byte <(screen+$0000),<(screen+$0084),<(screen+$0108),<(screen+$018c),<(screen+$0210),<(screen+$0294),<(screen+$0318),<(screen+$034e),<(screen+$0384),<(screen+$03ba),<(screen+$03f0),<(screen+$0426),<(screen+$045c),<(screen+$047a),<(screen+$0498),<(screen+$04b6),<(screen+$04d4),<(screen+$04f2)

doorGraphicsOffsetsScreen_hi:
	.byte >(screen+$0000),>(screen+$0084),>(screen+$0108),>(screen+$018c),>(screen+$0210),>(screen+$0294),>(screen+$0318),>(screen+$034e),>(screen+$0384),>(screen+$03ba),>(screen+$03f0),>(screen+$0426),>(screen+$045c),>(screen+$047a),>(screen+$0498),>(screen+$04b6),>(screen+$04d4),>(screen+$04f2)

doorGraphicsOffsetsD800_lo:
	.byte <(d800+$0000),<(d800+$0084),<(d800+$0108),<(d800+$018c),<(d800+$0210),<(d800+$0294),<(d800+$0318),<(d800+$034e),<(d800+$0384),<(d800+$03ba),<(d800+$03f0),<(d800+$0426),<(d800+$045c),<(d800+$047a),<(d800+$0498),<(d800+$04b6),<(d800+$04d4),<(d800+$04f2)

doorGraphicsOffsetsD800_hi:
	.byte >(d800+$0000),>(d800+$0084),>(d800+$0108),>(d800+$018c),>(d800+$0210),>(d800+$0294),>(d800+$0318),>(d800+$034e),>(d800+$0384),>(d800+$03ba),>(d800+$03f0),>(d800+$0426),>(d800+$045c),>(d800+$047a),>(d800+$0498),>(d800+$04b6),>(d800+$04d4),>(d800+$04f2)

.include "render_door.s"
