.include "global.inc"

; X = message index
.export text_writeTextMessage
.proc text_writeTextMessage
	lda #1
	sta textColor2
	lda messages_lo,x
	ldy messages_hi,x
	tax
	jsr text_writeNullString2
	rts
.endproc
