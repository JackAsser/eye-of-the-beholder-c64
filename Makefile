.SUFFIXES: .o .s

SYS      = c64
AS       = ca65
LD       = ld65
DISK     = disk
BB       = b2
X64SC    = /Applications/vice-x86-64-gtk3-3.6.1/bin/x64sc
X128	 = /Applications/vice-x86-64-gtk3-3.6.1/bin/x128
CARTCONV = cartconv

all: pcdata
	$(MAKE) eob.crt

pcdata:
	./build.sh || rm -rf pcdata
	$(MAKE) eob.crt
	rm -rf link link.bootstrapped

intro/intro.lib: global.inc .PHONY
#	$(MAKE) -C converters/intro
	$(MAKE) -C intro

ending/ending.lib: global.inc .PHONY
	$(MAKE) -C ending

.PHONY:
	@true

.s.o:
	$(AS) --cpu 6502X -U -o $*.o $*.s

eapi/out/eapi-am29f040-14: eapi/eapi-am29f040.s
	$(MAKE) -C eapi

automap.s:: vdc.inc
	touch $@

detect.s:: vdc.inc
	touch $@

zp.auto.inc: zp.s
	grep exportzp $? | sed -e 's/exportzp/importzp/g' -e 's/=.*//g' > $@

global.inc: zp.auto.inc
	touch $@

generate_help: generate_help.o
	g++ -o $@ $?

help.bin help_d800: help.txt generate_help
	./generate_help

help.prg: help.bin
	loadaddr 0x0400 $? $@

help.prg.b2: help.prg
	b2 $?

help_d800.prg: help_d800.bin
	loadaddr 0xd800 $? $@

help_d800.prg.b2: help_d800.prg
	b2 $?

faq.bin faq: faq.txt generate_help
	./generate_help

faq.prg: faq.bin
	loadaddr 0x0400 $? $@

faq.prg.b2: faq.prg
	b2 $?

faq_d800.prg: faq_d800.bin
	loadaddr 0xd800 $? $@

faq_d800.prg.b2: faq_d800.prg
	b2 $?

help.s:: help.prg.b2 help_d800.prg.b2 faq.prg.b2 faq_d800.prg.b2
	touch $@

$(filter-out zp.s levels.s start.s, $(wildcard *.s)):: global.inc
	touch $@

LEVELS = $(wildcard resources/LEVEL*.MAZ)
resources/LEVEL%.bin: resources/LEVEL%.MAZ
	dd if=$? of=$@ bs=1 skip=6
resources/LEVEL%.prg: resources/LEVEL%.bin
	loadaddr 0x1000 $? $@
resources/LEVEL%.prg.b2: resources/LEVEL%.prg
	b2 $?
levels.s: global.inc $(patsubst %.MAZ, %.prg.b2, $(LEVELS))
	touch $@

boot.s:: eapi/out/eapi-am29f040-14
	touch $@

WALLSET_SOURCES = $(wildcard converters/wallsets2/wallset_*.s)
WALLSETS = $(patsubst converters/wallsets2/%.s, converters/wallsets2/%.o, $(WALLSET_SOURCES))
$(WALLSET_SOURCES):: wallset_renderer.s
	touch $@

DOOR_SOURCES = $(wildcard *_doors.s)
DOORS = $(patsubst %.s, %.o, $(DOOR_SOURCES))
$(DOOR_SOURCES):: render_door.s
	touch $@

MONSTER_SOURCES = $(wildcard converters/monsters/mon_*.s)
MONSTERS = $(patsubst converters/monsters/%.s, converters/monsters/%.o, $(MONSTER_SOURCES))
$(MONSTER_SOURCES):: mon_renderer.s
	touch $@

DECORATION_SOURCES = $(wildcard converters/decorations/dec_*.s)
DECORATIONS = $(patsubst converters/decorations/%.s, converters/decorations/%.o, $(DECORATION_SOURCES))
$(DECORATION_SOURCES):: dec_renderer.s
	touch $@
	
ITEM_SOURCES = $(wildcard converters/items/item_*.s)
ITEMS = $(patsubst converters/items/%.s, converters/items/%.o, $(ITEM_SOURCES))
$(ITEM_SOURCES):: item_renderer.s
	touch $@

converters/deathanim/deathanim_xanathar0.s:: deathanim_renderer.s
	touch $@
converters/deathanim/deathanim_xanathar1.s:: deathanim_renderer.s
	touch $@
converters/deathanim/deathanim_xanathar2.s:: deathanim_renderer.s
	touch $@
converters/deathanim/deathanim_xanathar3.s:: deathanim_renderer.s
	touch $@

converters/outtake/outtake_graphics.s:: outtake_renderer.s
	touch $@

converters/doors/button_graphics.s:: button_renderer.s
	touch $@

converters/text/text.s:: text_resources.s
	touch $@

savegame.s:: save.bins
	touch $@

mouse.s:: mouse_acc.bin mouse_1351.inc mouse_joy.inc
	touch $@

mouse_acc.bin: mouse_acc
	./mouse_acc

mouse_acc: mouse_acc.c
	g++ -o $@ $?

# CharacterGeneration
resources/CharacterGeneration.kla: resources/CharacterGeneration.png
	make -C tools/png2kla SRC=../../resources/CharacterGeneration.png DST=../../resources/CharacterGeneration.kla

resources/CharacterGeneration.kla.b2: resources/CharacterGeneration.kla
	b2 $?

converters/music/eye-chargen.prg.b2: converters/music/eye-NTSC-char-generator-BOTH-CHIPS.sid
	make -C converters/music/

chargen.s:: converters/music/eye-chargen.prg.b2 resources/CharacterGeneration.kla.b2 chargen_assets.prg.b2 chargen_assets.bin.inc
	touch $@

chargen_assets.prg.b2: chargen_assets.prg
	b2 $?

chargen_assets.prg: chargen_assets.bin
	loadaddr 0 $? $@

chargen_assets.bin.inc: chargen_assets.bin
	@true

chargen_assets.bin: converters/ui/chargen_back-bitmap.bin \
					converters/ui/chargen_back-screen.bin \
					converters/ui/chargen_back-d800.bin \
					converters/ui/chargen_back-sprites.bin \
					converters/ui/chargen_arrows-bitmap.bin \
					converters/ui/chargen_arrows-screen.bin \
					converters/ui/chargen_arrows-d800.bin \
					converters/ui/chargen_menu-bitmap.bin \
					converters/ui/chargen_menu-screen.bin \
					converters/ui/chargen_menu-d800.bin \
					converters/ui/chargen_menu-sprites.bin \
					converters/ui/chargen_modify-bitmap.bin \
					converters/ui/chargen_modify-screen.bin \
					converters/ui/chargen_modify-d800.bin \
					converters/ui/chargen_modify-sprites.bin \
					converters/ui/chargen_delete-bitmap.bin \
					converters/ui/chargen_delete-screen.bin \
					converters/ui/chargen_delete-d800.bin \
					converters/ui/chargen_delete-sprites.bin \
					converters/ui/chargen_play-bitmap.bin \
					converters/ui/chargen_play-screen.bin \
					converters/ui/chargen_play-d800.bin \

	@./tools/dog/dog $? > $@ 2> $@.inc


INF_SOURCES = $(wildcard converters/inf/inf_*.s)
INFS = $(patsubst converters/inf/%.s, converters/inf/%.o, $(INF_SOURCES))

OBJS = 	savegame.o zp.o boot.o start.o detect.o gamestate.o main.o gui.o text.o levels.o renderer.o framebuffer.o mouse.o \
		converters/inf/monsterTypes.o converters/items/itemdat.o converters/text/text.o converters/doors/button_graphics.o \
		timer.o thrownTimerHandler.o doorTimerHandler.o partyMemberTimerHandler.o monsterTimerHandler.o foodTimerHandler.o swappingMemberTimerHandler.o \
		scriptparser.o audio2.o converters/outtake/outtake_graphics.o sprite_icons.o \
		spells.o chargen.o teleport_renderer.o keyboard.o camp.o npc.o \
		screenmode.o screenmode_game.o io.o explosion.o portal.o \
		decruncher.o glimmer_renderer.o benchmark.o automap.o bestiary.o help.o \
		$(INFS) \
		$(WALLSETS) \
		$(DOORS) \
		$(DECORATIONS) converters/decorations/wmiToDecoration.o \
		$(MONSTERS) monster.o \
		$(ITEMS) items.o \
		finale.o \
		deathanim.o \
		converters/deathanim/deathanim_xanathar0.o \
		converters/deathanim/deathanim_xanathar1.o \
		converters/deathanim/deathanim_xanathar2.o \
		converters/deathanim/deathanim_xanathar3.o \
		intro/intro.lib \
		ending/ending.lib \
		clearspr.o

start.bootstrapped: zp.auto.inc
	dd if=/dev/zero of=BANK07bb0.prg.b2 bs=1 count=2
	dd if=/dev/zero of=BANK07bb1.prg.b2 bs=1 count=2
	dd if=/dev/zero of=BANK07bb2.prg.b2 bs=1 count=2
	dd if=/dev/zero of=BANK07bb3.prg.b2 bs=1 count=2
	$(AS) --cpu 6502X -U -o start.o start.s
	touch start.s start.bootstrapped

BANK07bb0.bin BANK07bb1.bin BANK07bb2.bin BANK07bb3.bin: start.bootstrapped link.bootstrapped $(filter-out start.o,$(OBJS))
	$(LD) -C link.bootstrapped $(OBJS)
BANK07bb0.prg: BANK07bb0.bin
	loadaddr 0x8000 $? $@
BANK07bb0.prg.b2: BANK07bb0.prg
	b2 $?
BANK07bb1.prg: BANK07bb1.bin
	loadaddr 0x8000 $? $@
BANK07bb1.prg.b2: BANK07bb1.prg
	b2 $?
BANK07bb2.prg: BANK07bb2.bin
	loadaddr 0x8000 $? $@
BANK07bb2.prg.b2: BANK07bb2.prg
	b2 $?
BANK07bb3.prg: BANK07bb3.bin
	loadaddr 0x8000 $? $@
BANK07bb3.prg.b2: BANK07bb3.prg
	b2 $?
start.s: BANK07bb0.prg.b2 BANK07bb1.prg.b2 BANK07bb2.prg.b2 BANK07bb3.prg.b2 global.inc
	touch $@

link.bootstrapped: cartman.js prelink.js $(filter-out start.o,$(OBJS))
	node cartman.js prelink.js $(OBJS) > $@

link: cartman.js prelink.js $(OBJS)
	node cartman.js prelink.js $(OBJS) > $@

eob.bin: pcdata link $(OBJS)
	$(LD) -C link -m map -Ln eob.labels -o $@ $(OBJS)

eob.png: eob.bin
	java -Djava.awt.headless=true -cp tools/crtfull CRTFull $? $@

eob.crt: eob.bin
	$(CARTCONV) -i $< -o $@ -t easy	

test.prg: test.s
	dreamass -o $@ $?

test.prg.b2: test.prg
	b2 -c 2000 $?

VICE_OPTIONS = +warp -VICIIborders 1 -sound -soundbufsize 100 -soundfragsize 4 -soundvolume 25 -drivesoundvolume 15 -drivesound -keymap 1 -remotemonitor -joydev1 0 -controlport1device 0 -joydev2 0 -mouse -controlport2device 3 -sidenginemodel 256 -8 eobsave.d64

run:
	($(X128)  $(VICE_OPTIONS) -model pal +hidevdcwindow eob.crt && true)

run_pal:
	($(X64SC) $(VICE_OPTIONS) -model pal eob.crt && true)

run_pal_dolphin:
	($(X64SC) $(VICE_OPTIONS) -model pal -drive8type 1541 -kernal dolphin/DolphinDOS_Kernal.bin -dos1541II dolphin/DolphinDOS_1541.bin && true)

run_oldntsc:
	($(X64SC) $(VICE_OPTIONS) -model oldntsc eob.crt && true)

run_newntsc:
	($(X64SC) $(VICE_OPTIONS) -model newntsc eob.crt && true)

run_drean:
	($(X64SC) $(VICE_OPTIONS) -model drean eob.crt && true)

clean:
	rm -f *~ map *.bin *.crt *.prg *.o *.b2 *.bin.inc start.bootstrapped link.bootstrapped .*~ converters/monsters/*.o eob.labels zp.auto.inc eob.png *.zip mouse_acc generate_help link
	rm -rf pcdata
	$(MAKE) -C eapi clean
	$(MAKE) -C intro clean
	$(MAKE) -C ending clean

savegame:
	$(CARTCONV) -i eob.crt -o save.bins
	touch savegame.s
	$(MAKE) eob.crt	

inject: eob.crt
	echo "detach 20\nattach \"$?\" 20\nreset 1\n" | nc 127.0.0.1 6510
