#include <math.h>
#include <stdio.h>

int main(void) {
	float acc[128];
	float c=1.5;

	for (int i=0; i<128; i++) {
		float v = i-64;
//		if (fabs(v)<=1)
//			v=0;

		if (v<0) {
			v = -pow(-v, c);
		} else if (v>0) {
			v = pow(v, c);
		}
		acc[i] = v;
	}

	FILE* f = fopen("mouse_acc.bin", "wb");
	for (int i=0; i<128; i++) {
		int j = ((int)round(acc[i]))&0xff;
		fputc(j, f);
	}
	for (int i=0; i<128; i++) {
		int j = ((int)round(acc[i]))>>8;
		fputc(j, f);
	}
	fclose(f);

	return 0;
}