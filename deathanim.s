.include "global.inc"
.segment "DEATHANIM"

backup_bitmap = __GAMESTATEM_RUN__+1000
backup_sprites = backup_bitmap+14*15*8
backup_screen = backup_sprites+14*15*8
backup_d800 = backup_screen+14*15

.define DOOR_FRAME 11

.export deathanim
.proc deathanim
		jsrf render_just_dungeon

		jsr backup

		jsrf draw_xanathar
		jsr show
		lda #25
		jsr wait

		ldx #0
		:
			txa
			pha

			jsr restore
			pla
			pha
			tax

			ldy frameBank,x
			lda frameLo,x
			sta drawJsr+1
			lda frameHi,x
			sta drawJsr+2
			lda frameIndex,x
			tax
			jsr draw
			jsr sound
			jsr show

			pla
			tax
			inx
			cpx #11
		bne :-

		jsr backup

		lda #11
		sta ROWSKIP
		:
			ldx #DOOR_FRAME
			ldy frameBank,x
			lda frameLo,x
			sta drawJsr+1
			lda frameHi,x
			sta drawJsr+2
			lda frameIndex,x
			tax
			jsr draw
			jsr slam
			jsr show
			jsr restore
			dec ROWSKIP
		bpl :-

		rts

.pushseg
.segment "MEMCODE_RW"
draw:
		sty BANK
		sty $de00
		drawJsr:jsr $dead
		lda #<.bank(deathanim)
		sta BANK
		sta $de00
		rts
.popseg

frameIndex:
		.byte 0
		.byte 1
		.byte 0
		.byte 1
		.byte 2
		.byte 0
		.byte 1
		.byte 2
		.byte 0
		.byte 1
		.byte 2
		.byte 3 ;door

frameLo:
		.byte <deathanim_xanathar0
		.byte <deathanim_xanathar0
		.byte <deathanim_xanathar1
		.byte <deathanim_xanathar1
		.byte <deathanim_xanathar1
		.byte <deathanim_xanathar2
		.byte <deathanim_xanathar2
		.byte <deathanim_xanathar2
		.byte <deathanim_xanathar3
		.byte <deathanim_xanathar3
		.byte <deathanim_xanathar3
		.byte <(deathanim_xanathar3+3) ;door
frameHi:
		.byte >deathanim_xanathar0
		.byte >deathanim_xanathar0
		.byte >deathanim_xanathar1
		.byte >deathanim_xanathar1
		.byte >deathanim_xanathar1
		.byte >deathanim_xanathar2
		.byte >deathanim_xanathar2
		.byte >deathanim_xanathar2
		.byte >deathanim_xanathar3
		.byte >deathanim_xanathar3
		.byte >deathanim_xanathar3
		.byte >(deathanim_xanathar3+3) ;door
frameBank:
		.byte <.bank(deathanim_xanathar0)
		.byte <.bank(deathanim_xanathar0)
		.byte <.bank(deathanim_xanathar1)
		.byte <.bank(deathanim_xanathar1)
		.byte <.bank(deathanim_xanathar1)
		.byte <.bank(deathanim_xanathar2)
		.byte <.bank(deathanim_xanathar2)
		.byte <.bank(deathanim_xanathar2)
		.byte <.bank(deathanim_xanathar3)
		.byte <.bank(deathanim_xanathar3)
		.byte <.bank(deathanim_xanathar3)
		.byte <.bank(deathanim_xanathar3) ;door

slam:
		ldx #5
		bne :+
sound:
		ldx #34
		:
		jsrf audio_playSoundEffect
		rts

backup:
		memcpy backup_bitmap, frameBuffer_bitmap+4*15*8, 14*15*8
		memcpy backup_sprites, frameBuffer_sprites+4*15*8, 14*15*8
		memcpy backup_screen, frameBuffer_screen+4*15, 14*15
		memcpy backup_d800, frameBuffer_d800+4*15, 14*15
		rts

restore:
		memcpy frameBuffer_bitmap+4*15*8, backup_bitmap, 14*15*8
		memcpy frameBuffer_sprites+4*15*8, backup_sprites, 14*15*8
		memcpy frameBuffer_screen+4*15, backup_screen, 14*15
		memcpy frameBuffer_d800+4*15, backup_d800, 14*15
		rts

show:
		jsrf showFrameBuffer
		rts
.endproc