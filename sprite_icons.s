.include "global.inc"

.segment "SPRITE_ICONS"

.export updatePointerSprite
.proc updatePointerSprite
	ldx #0
	lda POINTER_ITEM+0
	ora POINTER_ITEM+1
	beq :+
		ldy #Item::picture
		jsr lda_POINTER_ITEM_y
		tax
	:
	; *=32
	stx SRC+0
	lda #0
	asl SRC+0
	rol
	asl SRC+0
	rol
	asl SRC+0
	rol
	asl SRC+0
	rol
	asl SRC+0
	rol
	sta SRC+1
	clc
	lda #<sprite_icons
	adc SRC+0
	sta SRC+0
	lda #>sprite_icons
	adc SRC+1
	sta SRC+1

	.repeat 2,J
		.repeat 2,I
			ldy #I*8+J*16
			.repeat 8,K
				lda (SRC),y
				sta mousepointer+I+J*8*3+K*3
				iny
			.endrep
		.endrep
	.endrep

	rts
.endproc

sprite_icons:
.incbin "converters/icons/sprite_icons.bin"
