.include "global.inc"

.segment "MAIN"

.export foodTimerHandler
.proc foodTimerHandler
		ldx #0
		loop:
			lda partyMembers_lo,x
			sta CURRENT_PARTYMEMBER+0
			lda partyMembers_hi,x
			sta CURRENT_PARTYMEMBER+1

			stx ARGS+0
			lda #RING_OF_SUSTENANCE
			sta ARGS+1
			jsrf gameState_memberHaveRing
			lsr ARGS+1
			bcs continue

			ldy #PartyMember::food
			lda (CURRENT_PARTYMEMBER),y
			beq continue

			ldy #PartyMember::status
			lda (CURRENT_PARTYMEMBER),y
			and #PARTYMEMBER_STATUS_ACTIVE
			beq continue

			ldy #PartyMember::hpCurrent
			lda (CURRENT_PARTYMEMBER),y
			cmp #<-10
			bne :+
			iny
			lda (CURRENT_PARTYMEMBER),y
			cmp #>-10
			beq continue
			:
				ldy #PartyMember::food
				sec
				lda (CURRENT_PARTYMEMBER),y
				sbc #1
				sta (CURRENT_PARTYMEMBER),y
				inc partyMemberStatsChanged,x
			continue:
			inx
			cpx #6
		bne loop
		rts
.endproc