.include "global.inc"

.segment "LEVELSTATE"
.export storedMazes
.proc storedMazes
.export storedMaze01
storedMaze01:
	.res $08d
.export storedMaze02
storedMaze02:
	.res $12f
.export storedMaze03
storedMaze03:
	.res $0ec
.export storedMaze04
storedMaze04:
	.res $12d
.export storedMaze05
storedMaze05:
	.res $142
.export storedMaze06
storedMaze06:
	.res $0f8
.export storedMaze07
storedMaze07:
	.res $0fd
.export storedMaze08
storedMaze08:
	.res $0e9
.export storedMaze09
storedMaze09:
	.res $0eb
.export storedMaze10
storedMaze10:
	.res $0c1
.export storedMaze11
storedMaze11:
	.res $10d
.export storedMaze12
storedMaze12:
	.res $0ed
.endproc

.segment "BSS"
.export triggerBitsAndMonsterCount
triggerBitsAndMonsterCount:
	.res 1024

; Must be placed directly after triggerBitsAndMonsterCount because setTrigger skips forward $400
; %xyyyyyyy: x=visited tile. y=triggerindex+1 (0 = none)
.export triggerIndices
triggerIndices:
	.res 1024

.export topItemPtrs
topItemPtrs:
	.res 2048

.define maxNbrWallMappings 70
.export wallType
wallType:	.res maxNbrWallMappings

.export wallEventMask
wallEventMask:	.res maxNbrWallMappings

.export wallFlags
wallFlags:	.res maxNbrWallMappings

.export wallDecoration
wallDecoration:	.res maxNbrWallMappings

.export timerMode
timerMode: .res 1

.export timerTicks
timerTicks: .res 2

.export stepsUntilScriptCall
stepsUntilScriptCall: .res 2

.export stepCounter
stepCounter: .res 2

.segment "MEMCODE_RO"
; SRC points to original maze in RAM
; DST points to storedMazeXX
.proc compressMaze
	lda #<maze
	sta SRC2+0
	lda #>maze
	sta SRC2+1

	ldy #0
	ldx #0 ;current run-length
	loop:
		jsr compare
		bne :++
			inx
			bpl :+
				; If RLE ==$80 then emit $7f and reset
				lda #$7f
				jsr emit
				ldx #0
			:
			bcs break
			jmp loop
		:

		; Emit RLE
		php
		cpx #0
		beq :+
			pha
			dex
			txa
			jsr emit
			ldx #0
			pla
		:
		
		; Emit Diff
		ora #$80
		jsr emit

		plp
	bcc loop

break:	; emit final RLE here if any
	cpx #0
	beq :+
		dex
		txa
		jsr emit
	:

	rts

emit:	sta (DST),y
	inc DST+0
	bne :+
		inc DST+1
	:
	rts

compare:lda (SRC2),y
	cmp (SRC),y
	php
	pha
	inc SRC+0
	bne :+
		inc SRC+1
	:
	inc SRC2+0
	bne :+
		inc SRC2+1
	:
	lda SRC2+0
	cmp #<(maze+4096)
	bne :+
		lda SRC2+1
		cmp #>(maze+4096)
		bne :+
			pla
			plp
			sec
			rts
	:
	pla
	plp
	clc
	rts

.endproc

; SRC points to storedMazeXX
; maze is already filled with the original level, so just apply patch from SRC
.segment "START"
.proc decompressMaze
	lda 1
	pha
	lda #$30
	sta 1
	jsr decompressMazeImpl
	pla
	sta 1
	rts
.endproc

.proc decompressMazeImpl
	lda #<maze
	sta DST+0
	lda #>maze
	sta DST+1
	ldy #0
	loop:
		jsr fetch
		bpl :+
			and #$7f
			jsr emit
			bcc loop
			rts
		:

		tax
		:
			jsr skip
			bcs break
			dex
		bpl :-
	jmp loop

break:	rts

fetch:	lda (SRC),y
	php
	inc SRC+0
	bne :+
		inc SRC+1
	:
	plp
	rts

emit:	sta (DST),y

skip:	inc DST+0
	bne :+
		inc DST+1
	:
	lda DST+0
	cmp #<(maze+4096)
	bne :+
		lda DST+1
		cmp #>(maze+4096)
		bne :+
			sec
			rts
	:
	clc
	rts
.endproc

.segment "MAIN"
storedMazes_lo: .byte <storedMaze01,<storedMaze02,<storedMaze03,<storedMaze04,<storedMaze05,<storedMaze06,<storedMaze07,<storedMaze08,<storedMaze09,<storedMaze10,<storedMaze11,<storedMaze12
storedMazes_hi: .byte >storedMaze01,>storedMaze02,>storedMaze03,>storedMaze04,>storedMaze05,>storedMaze06,>storedMaze07,>storedMaze08,>storedMaze09,>storedMaze10,>storedMaze11,>storedMaze12
levels_lo: 		.byte <level1, <level2, <level3, <level4, <level5, <level6, <level7, <level8, <level9, <level10, <level11, <level12
levels_hi: 		.byte >level1, >level2, >level3, >level4, >level5, >level6, >level7, >level8, >level9, >level10, >level11, >level12
levels_bank:	.byte <.bank(level1), <.bank(level2), <.bank(level3), <.bank(level4), <.bank(level5), <.bank(level6), <.bank(level7), <.bank(level8), <.bank(level9), <.bank(level10), <.bank(level11), <.bank(level12)

visitedTiles_lo:
				.repeat 12,I
					.byte <(visitedTiles+128*I)
				.endrep
visitedTiles_hi:
				.repeat 12,I
					.byte >(visitedTiles+128*I)
				.endrep

.proc visitedTiles_swapOut
		lda #<triggerIndices
		sta SRC+0
		lda #>triggerIndices
		sta SRC+1

		ldy #0
		ldx #0
		lp:
			.repeat 8,I
				lda (SRC),y
				asl
				rol TMP
				iny
			.endrep
			bne :+
				inc SRC+1
			:
			lda TMP
			sta frameBuffer_bitmap,x
			inx
		bpl lp

		ldx levelIndex
		dex
		lda #<frameBuffer_bitmap
		sta MSRC+0
		lda #>frameBuffer_bitmap
		sta MSRC+1
		lda visitedTiles_lo,x
		sta MDST+0
		lda visitedTiles_hi,x
		sta MDST+1
		lda #<128
		ldx #>128
		jsr _memcpy_pureram

		rts
.endproc

.proc visitedTiles_swapIn
		ldx levelIndex
		dex
		lda visitedTiles_lo,x
		sta MSRC+0
		lda visitedTiles_hi,x
		sta MSRC+1
		lda #<frameBuffer_bitmap
		sta MDST+0
		lda #>frameBuffer_bitmap
		sta MDST+1
		lda #<128
		ldx #>128
		jsr _memcpy_pureram

		lda #<triggerIndices
		sta DST+0
		lda #>triggerIndices
		sta DST+1
		ldx #0
		ldy #0
		lp:
			.repeat 8,I
				asl frameBuffer_bitmap,x
				bcc :+
					lda (DST),y
					ora #$80
					sta (DST),y
				:
				iny
			.endrep
			bne :+
				inc DST+1
			:
			inx
		bpl lp
		rts
.endproc

.export saveMaze
.proc saveMaze
	jsr visitedTiles_swapOut
	jsrf monster_swapOut
	ldx levelIndex

	lda #<frameBuffer_bitmap
	sta BB_DST+0
	lda #>frameBuffer_bitmap
	sta BB_DST+1
	lda levels_bank-1,x
	pha
	ldy levels_lo-1,x
	lda levels_hi-1,x
	tax
	pla
	jsr decrunchFar

	ldx levelIndex
	lda #<frameBuffer_bitmap
	sta SRC+0
	lda #>frameBuffer_bitmap
	sta SRC+1
	lda storedMazes_lo-1,x
	sta DST+0
	lda storedMazes_hi-1,x
	sta DST+1
	jsr compressMaze
	rts
.endproc

.pushseg
.segment "BSS"
	.export shouldSkipLevelCompress
	shouldSkipLevelCompress: .res 1
.popseg

; x = new level index
.export loadMaze
.proc loadMaze
	txa
	pha

	lda shouldSkipLevelCompress
	bne skipLevelCompress

	; Init stored mazes during a just created game (levelIndex=0 in this case)
	ldx levelIndex
	bne :+
		memset storedMazes, $7f, .sizeof(storedMazes)
	:

	ldx levelIndex
	dex
	bmi :+ ;First time levelIndex is 0 so this will fail
		jsr saveMaze
	:

	skipLevelCompress:
	lda #0
	sta shouldSkipLevelCompress

	pla
	tax
	
	stx levelIndex
fromSave:
	lda #0
	sta shouldSkipLevelCompress

	lda levelWallsetBackgroundBank-1,x
	sta wallsetBackgroundBank
	lda levelWallsetBackgroundOffsetLo-1,x
	sta wallsetBackgroundOffset+0
	lda levelWallsetBackgroundOffsetHi-1,x
	sta wallsetBackgroundOffset+1

	lda levelDoorType0Bank-1,x
	sta doorBanks+0
	lda levelDoorType0OffsetLo-1,x
	sta doorOffsets+0*2+0
	lda levelDoorType0OffsetHi-1,x
	sta doorOffsets+1*2+0

	lda levelDoorType1Bank-1,x
	sta doorBanks+1
	lda levelDoorType0OffsetLo-1,x
	sta doorOffsets+0*2+1
	lda levelDoorType0OffsetHi-1,x
	sta doorOffsets+1*2+1

	lda levelDecorationsBank-1,x
	sta decorationBank
	lda levelDecorationsOffsetLo-1,x
	sta decorationOffset+0
	lda levelDecorationsOffsetHi-1,x
	sta decorationOffset+1

	clc
	lda levelDecorationsOffsetLo-1,x
	adc #3
	sta decorationOffset+2
	lda levelDecorationsOffsetHi-1,x
	adc #0
	sta decorationOffset+3

	clc
	lda levelDecorationsOffsetLo-1,x
	adc #6
	sta decorationOffset+4
	lda levelDecorationsOffsetHi-1,x
	adc #0
	sta decorationOffset+5

	lda #<maze
	sta BB_DST+0
	lda #>maze
	sta BB_DST+1
	lda levels_bank-1,x
	pha
	ldy levels_lo-1,x
	lda levels_hi-1,x
	tax
	pla
	jsr decrunchFar

	ldx levelIndex
	lda storedMazes_lo-1,x
	sta SRC+0
	lda storedMazes_hi-1,x
	sta SRC+1
	jsr decompressMaze

	jsrf installDrawMonsterHelper
	jsrf installDrawWallHelper

	jsrf items_clearTopItemIndex
	jsrf items_linkItemsOnLevel

	jsr installInf

	jsr visitedTiles_swapIn

	ldx #3
	lda #0
	:
		sta monsterTimers+0*.sizeof(Timer)+Timer::timeout,x
		sta monsterTimers+1*.sizeof(Timer)+Timer::timeout,x
		sta monsterTimers+2*.sizeof(Timer)+Timer::timeout,x
		sta monsterTimers+3*.sizeof(Timer)+Timer::timeout,x
		dex
	bpl :-

	ldx levelIndex
	clc
	lda levelMonsterTimerA-1,x
	sta monsterTimers+0*.sizeof(Timer)+Timer::timeout
	lsr
	sta monsterTimers+1*.sizeof(Timer)+Timer::timeout
	lda levelMonsterTimerB-1,x
	sta monsterTimers+2*.sizeof(Timer)+Timer::timeout
	lsr
	sta monsterTimers+3*.sizeof(Timer)+Timer::timeout

	ldx #4
	lda #<monsterTimers
	sta CURRENT_TIMER+0
	lda #>monsterTimers
	sta CURRENT_TIMER+1
	:
		jsr resetTimer
		clc
		lda CURRENT_TIMER+0
		adc #<.sizeof(Timer)
		sta CURRENT_TIMER+0
		lda CURRENT_TIMER+1
		adc #>.sizeof(Timer)
		sta CURRENT_TIMER+1
		dex
	bne :-

	lda #1
	ldx #20
	:
		sta affectedSpriteColumns,x
		dex
	bpl :-
	jsrf clearFrameBufferSprites

	sec
	jsrf automapRender

	rts

levelMonsterTimerA: .byte 25,35,35,20,25,25,25,25,35,25,35,15
levelMonsterTimerB: .byte 35,25,25,25,20,25,25,20,20,20,25,35

levelWallsetBackgroundBank:
		.byte <.bank(wallset_brick_background)
		.byte <.bank(wallset_brick_background)
		.byte <.bank(wallset_brick_background)
		.byte <.bank(wallset_blue_background)
		.byte <.bank(wallset_blue_background)
		.byte <.bank(wallset_blue_background)
		.byte <.bank(wallset_drow_background)
		.byte <.bank(wallset_drow_background)
		.byte <.bank(wallset_drow_background)
		.byte <.bank(wallset_green_background)
		.byte <.bank(wallset_green_background)
		.byte <.bank(wallset_xanatha_background)

levelWallsetBackgroundOffsetLo:
		.byte <wallset_brick_background
		.byte <wallset_brick_background
		.byte <wallset_brick_background
		.byte <wallset_blue_background
		.byte <wallset_blue_background
		.byte <wallset_blue_background
		.byte <wallset_drow_background
		.byte <wallset_drow_background
		.byte <wallset_drow_background
		.byte <wallset_green_background
		.byte <wallset_green_background
		.byte <wallset_xanatha_background

levelWallsetBackgroundOffsetHi:
		.byte >wallset_brick_background
		.byte >wallset_brick_background
		.byte >wallset_brick_background
		.byte >wallset_blue_background
		.byte >wallset_blue_background
		.byte >wallset_blue_background
		.byte >wallset_drow_background
		.byte >wallset_drow_background
		.byte >wallset_drow_background
		.byte >wallset_green_background
		.byte >wallset_green_background
		.byte >wallset_xanatha_background

levelDoorType0Bank:
		.byte <.bank(brick0_doors)
		.byte <.bank(brick0_doors)
		.byte <.bank(brick0_doors)
		.byte <.bank(blue_doors)
		.byte <.bank(blue_doors)
		.byte <.bank(blue_doors)
		.byte <.bank(drow_doors)
		.byte <.bank(drow_doors)
		.byte <.bank(drow_doors)
		.byte <.bank(green_doors)
		.byte <.bank(green_doors)
		.byte <.bank(xanatha_doors)

levelDoorType1Bank:
		.byte <.bank(brick1_doors)
		.byte <.bank(brick1_doors)
		.byte <.bank(brick1_doors)
		.byte <.bank(blue_doors)
		.byte <.bank(blue_doors)
		.byte <.bank(blue_doors)
		.byte <.bank(drow_doors)
		.byte <.bank(drow_doors)
		.byte <.bank(drow_doors)
		.byte <.bank(green_doors)
		.byte <.bank(green_doors)
		.byte <.bank(xanatha_doors)

levelDoorType0OffsetLo:
		.byte <(brick0_doors-1)
		.byte <(brick0_doors-1)
		.byte <(brick0_doors-1)
		.byte <(blue_doors-1)
		.byte <(blue_doors-1)
		.byte <(blue_doors-1)
		.byte <(drow_doors-1)
		.byte <(drow_doors-1)
		.byte <(drow_doors-1)
		.byte <(green_doors-1)
		.byte <(green_doors-1)
		.byte <(xanatha_doors-1)

levelDoorType1OffsetLo:
		.byte <(brick1_doors-1)
		.byte <(brick1_doors-1)
		.byte <(brick1_doors-1)
		.byte <(blue_doors-1)
		.byte <(blue_doors-1)
		.byte <(blue_doors-1)
		.byte <(drow_doors-1)
		.byte <(drow_doors-1)
		.byte <(drow_doors-1)
		.byte <(green_doors-1)
		.byte <(green_doors-1)
		.byte <(xanatha_doors-1)

levelDoorType0OffsetHi:
		.byte >(brick0_doors-1)
		.byte >(brick0_doors-1)
		.byte >(brick0_doors-1)
		.byte >(blue_doors-1)
		.byte >(blue_doors-1)
		.byte >(blue_doors-1)
		.byte >(drow_doors-1)
		.byte >(drow_doors-1)
		.byte >(drow_doors-1)
		.byte >(green_doors-1)
		.byte >(green_doors-1)
		.byte >(xanatha_doors-1)

levelDoorType1OffsetHi:
		.byte >(brick1_doors-1)
		.byte >(brick1_doors-1)
		.byte >(brick1_doors-1)
		.byte >(blue_doors-1)
		.byte >(blue_doors-1)
		.byte >(blue_doors-1)
		.byte >(drow_doors-1)
		.byte >(drow_doors-1)
		.byte >(drow_doors-1)
		.byte >(green_doors-1)
		.byte >(green_doors-1)
		.byte >(xanatha_doors-1)

levelDecorationsBank:
		.byte <.bank(dec_brick1)
		.byte <.bank(dec_brick2)
		.byte <.bank(dec_brick3)
		.byte <.bank(dec_blue)
		.byte <.bank(dec_blue)
		.byte <.bank(dec_blue)
		.byte <.bank(dec_drow)
		.byte <.bank(dec_drow)
		.byte <.bank(dec_drow)
		.byte <.bank(dec_green)
		.byte <.bank(dec_green)
		.byte <.bank(dec_xanatha)

levelDecorationsOffsetLo:
		.byte <(dec_brick1-1)
		.byte <(dec_brick2-1)
		.byte <(dec_brick3-1)
		.byte <(dec_blue-1)
		.byte <(dec_blue-1)
		.byte <(dec_blue-1)
		.byte <(dec_drow-1)
		.byte <(dec_drow-1)
		.byte <(dec_drow-1)
		.byte <(dec_green-1)
		.byte <(dec_green-1)
		.byte <(dec_xanatha-1)

levelDecorationsOffsetHi:
		.byte >(dec_brick1-1)
		.byte >(dec_brick2-1)
		.byte >(dec_brick3-1)
		.byte >(dec_blue-1)
		.byte >(dec_blue-1)
		.byte >(dec_blue-1)
		.byte >(dec_drow-1)
		.byte >(dec_drow-1)
		.byte >(dec_drow-1)
		.byte >(dec_green-1)
		.byte >(dec_green-1)
		.byte >(dec_xanatha-1)
.endproc
.export loadMazeFromSave = loadMaze::fromSave

.proc createMonstersFromInf
	jsrf monster_clearMonsters

	ldx levelIndex
	dex
	lda #0
	sta ARGS+0
	lda monsterCreate_lo,x
	sta ARGS+1
	lda monsterCreate_hi,x
	sta ARGS+2

	monsterLoop:
		ldy #0
		jsr lda_monsterCreate_ptr_y
		bpl :+
			rts
		:
		jsrf monster_createFromMonsterCreate

		inc ARGS+0
		
		clc
		lda ARGS+1
		adc #<.sizeof(MonsterCreate)
		sta ARGS+1
		lda ARGS+2
		adc #>.sizeof(MonsterCreate)
		sta ARGS+2
	jmp monsterLoop

monsterCreate_lo:
	.byte <lvl1MonsterCreate
	.byte <lvl2MonsterCreate
	.byte <lvl3MonsterCreate
	.byte <lvl4MonsterCreate
	.byte <lvl5MonsterCreate
	.byte <lvl6MonsterCreate
	.byte <lvl7MonsterCreate
	.byte <lvl8MonsterCreate
	.byte <lvl9MonsterCreate
	.byte <lvl10MonsterCreate
	.byte <lvl11MonsterCreate
	.byte <lvl12MonsterCreate

monsterCreate_hi:
	.byte >lvl1MonsterCreate
	.byte >lvl2MonsterCreate
	.byte >lvl3MonsterCreate
	.byte >lvl4MonsterCreate
	.byte >lvl5MonsterCreate
	.byte >lvl6MonsterCreate
	.byte >lvl7MonsterCreate
	.byte >lvl8MonsterCreate
	.byte >lvl9MonsterCreate
	.byte >lvl10MonsterCreate
	.byte >lvl11MonsterCreate
	.byte >lvl12MonsterCreate
.endproc

.segment "MEMCODE_RO"
.proc installInf
	lda BANK
	pha
	ldx levelIndex
	lda levels_inf_bank-1,x
	sta BANK
	sta INFBANK
	sta $de00

	ldx levelIndex
	lda loadedLevels-1,x
	bne :+
		jsrf createMonstersFromInf
		jmp :++
	:
		jsrf monster_swapIn
	:

	; Triggers should be installed after monsters since monsters clears triggerBitsAndMonsterCount
	jsr setTriggers

	jsr setWallMappingsAndTimer

	ldx levelIndex
	lda #1
	sta loadedLevels-1,x

	pla
	sta BANK
	sta $de00

	rts
.endproc

levels_inf_bank:
		.byte <.bank(lvl1WallType)
		.byte <.bank(lvl2WallType)
		.byte <.bank(lvl3WallType)
		.byte <.bank(lvl4WallType)
		.byte <.bank(lvl5WallType)
		.byte <.bank(lvl6WallType)
		.byte <.bank(lvl7WallType)
		.byte <.bank(lvl8WallType)
		.byte <.bank(lvl9WallType)
		.byte <.bank(lvl10WallType)
		.byte <.bank(lvl11WallType)
		.byte <.bank(lvl12WallType)

.segment "MEMCODE_RO"

; x = triggerIndex
.export levels_setInfPointer
.proc levels_setInfPointer
	;TMP = index*5
	lda #0
	sta TMP+1
	txa
	asl
	rol TMP+1
	asl
	rol TMP+1
	sta TMP+0
	txa
	clc
	adc TMP+0
	sta TMP+0
	bcc :+
		inc TMP+1
	:

	;TMP += triggers[levelIndex]
	ldx levelIndex
	dex
	clc
	lda triggers_lo,x
	adc TMP+0
	sta TMP+0
	lda triggers_hi,x
	adc TMP+1
	sta TMP+1

	lda BANK
	pha
	lda levels_inf_bank,x
	sta BANK
	sta $de00

	ldy #3
	lda (TMP),y
	sta INF_POINTER+0
	iny
	lda (TMP),y
	sta INF_POINTER+1

	pla
	sta BANK
	sta $de00

	rts
.endproc

.export levels_infReadByte
.proc levels_infReadByte
	tya
	pha
	txa
	pha
	ldx levelIndex
	dex
	lda BANK
	pha
	lda levels_inf_bank,x
	sta BANK
	sta $de00
	ldy #0
	lda (INF_POINTER),y
	inc INF_POINTER+0
	bne :+
		inc INF_POINTER+1
	:
	sta INF_READ_BYTE
	pla
	sta BANK
	sta $de00
	pla
	tax
	pla
	tay
	lda INF_READ_BYTE
	rts
.endproc

.export levels_writeString
.proc levels_writeString
	ldx levelIndex
	dex
	lda BANK
	pha
	lda levels_inf_bank,x
	sta BANK
	sta $de00
	jsr text_writeString
	jsrf text_newLine
	pla
	sta BANK
	sta $de00
	rts
.endproc

.proc setTriggers
	ldx levelIndex
	dex
	lda triggers_lo,x
	sta TMP+0
	lda triggers_hi,x
	sta TMP+1

	lda #0
	tax
	:
		sta triggerIndices+$000,x
		sta triggerIndices+$100,x
		sta triggerIndices+$200,x
		sta triggerIndices+$300,x
		inx
	bne :-
	inx
	triggerLoop:
		ldy #0
		clc
		lda (TMP),y
		adc #<triggerBitsAndMonsterCount
		sta TMP2+0
		iny
		lda (TMP),y
		adc #>triggerBitsAndMonsterCount
		sta TMP2+1
		bcc :+
			rts
		:

		iny
		lda (TMP),y

		; Mask in triggerBits
		ldy #0
		ora (TMP2),y
		sta (TMP2),y

		; Skip forward $400 bytes into triggerIncides and set X there
		clc
		lda TMP2+1
		adc #>$0400
		sta TMP2+1
		txa
		sta (TMP2),y

		inx
		clc
		lda TMP+0
		adc #5
		sta TMP+0
		lda TMP+1
		adc #0
		sta TMP+1
	jmp triggerLoop
.endproc

triggers_lo:
	.byte <lvl1Triggers
	.byte <lvl2Triggers
	.byte <lvl3Triggers
	.byte <lvl4Triggers
	.byte <lvl5Triggers
	.byte <lvl6Triggers
	.byte <lvl7Triggers
	.byte <lvl8Triggers
	.byte <lvl9Triggers
	.byte <lvl10Triggers
	.byte <lvl11Triggers
	.byte <lvl12Triggers

triggers_hi:
	.byte >lvl1Triggers
	.byte >lvl2Triggers
	.byte >lvl3Triggers
	.byte >lvl4Triggers
	.byte >lvl5Triggers
	.byte >lvl6Triggers
	.byte >lvl7Triggers
	.byte >lvl8Triggers
	.byte >lvl9Triggers
	.byte >lvl10Triggers
	.byte >lvl11Triggers
	.byte >lvl12Triggers

.proc setWallMappingsAndTimer
	ldx levelIndex
	dex

	lda wallType_lo,x
	sta SRC+0
	lda wallType_hi,x
	sta SRC+1
	lda wallDecoration_lo,x
	sta SRC4+0
	lda wallDecoration_hi,x
	sta SRC4+1

	ldy #0
	:
		lda (SRC),y
		sta wallType,y
		lda (SRC4),y
		sta wallDecoration,y
		iny
		cpy #maxNbrWallMappings
	bne :-
	:
		lda (SRC),y
		sta wallEventMask-maxNbrWallMappings,y
		iny
		cpy #(maxNbrWallMappings*2)
	bne :-
	:
		lda (SRC),y
		sta wallFlags-maxNbrWallMappings*2,y
		iny
		cpy #(maxNbrWallMappings*3)
	bne :-

	lda (SRC),y ;timerMode
	sta timerMode
	iny
	lda (SRC),y ;timerTicksLo
	sta timerTicks+0
	iny
	lda (SRC),y ;timerTicksHi
	sta timerTicks+1
	iny
	lda (SRC),y ;stepsUntilScriptCallLo
	sta stepsUntilScriptCall+0
	iny
	lda (SRC),y ;stepsUntilScriptCallHi
	sta stepsUntilScriptCall+1

	lda #0
	sta stepCounter+0
	sta stepCounter+1

	rts

wallType_lo:
	.byte <lvl1WallType
	.byte <lvl2WallType
	.byte <lvl3WallType
	.byte <lvl4WallType
	.byte <lvl5WallType
	.byte <lvl6WallType
	.byte <lvl7WallType
	.byte <lvl8WallType
	.byte <lvl9WallType
	.byte <lvl10WallType
	.byte <lvl11WallType
	.byte <lvl12WallType

wallType_hi:
	.byte >lvl1WallType
	.byte >lvl2WallType
	.byte >lvl3WallType
	.byte >lvl4WallType
	.byte >lvl5WallType
	.byte >lvl6WallType
	.byte >lvl7WallType
	.byte >lvl8WallType
	.byte >lvl9WallType
	.byte >lvl10WallType
	.byte >lvl11WallType
	.byte >lvl12WallType

wallDecoration_lo:
	.byte <lvl1WallDecoration
	.byte <lvl2WallDecoration
	.byte <lvl3WallDecoration
	.byte <lvl4WallDecoration
	.byte <lvl5WallDecoration
	.byte <lvl6WallDecoration
	.byte <lvl7WallDecoration
	.byte <lvl8WallDecoration
	.byte <lvl9WallDecoration
	.byte <lvl10WallDecoration
	.byte <lvl11WallDecoration
	.byte <lvl12WallDecoration

wallDecoration_hi:
	.byte >lvl1WallDecoration
	.byte >lvl2WallDecoration
	.byte >lvl3WallDecoration
	.byte >lvl4WallDecoration
	.byte >lvl5WallDecoration
	.byte >lvl6WallDecoration
	.byte >lvl7WallDecoration
	.byte >lvl8WallDecoration
	.byte >lvl9WallDecoration
	.byte >lvl10WallDecoration
	.byte >lvl11WallDecoration
	.byte >lvl12WallDecoration
.endproc

.segment "MAIN"

.proc installDrawWallHelper
	ldx levelIndex
	dex
	txa
	asl
	tax

	lda levelWallsetBank,x
	sta wallsetBank+0
	sta wallsetBank+1
	sta wallsetBank+2
	lda levelWallsetDrawLo,x
	sta wallsetDrawLo+0
	sta wallsetDrawLo+1
	sta wallsetDrawLo+2
	lda levelWallsetDrawHi,x
	sta wallsetDrawHi+0
	sta wallsetDrawHi+1
	sta wallsetDrawHi+2
	inx
	lda levelWallsetBank,x
	sta wallsetBank+3
	sta wallsetBank+4
	sta wallsetBank+5
	sta wallsetBank+6
	lda levelWallsetDrawLo,x
	sta wallsetDrawLo+3
	sta wallsetDrawLo+4
	sta wallsetDrawLo+5
	sta wallsetDrawLo+6
	lda levelWallsetDrawHi,x
	sta wallsetDrawHi+3
	sta wallsetDrawHi+4
	sta wallsetDrawHi+5
	sta wallsetDrawHi+6
	rts

levelWallsetBank:
	.byte <.bank(wallset_brick_walls1)
	.byte <.bank(wallset_brick_walls2)
	.byte <.bank(wallset_brick_walls1)
	.byte <.bank(wallset_brick_walls2)
	.byte <.bank(wallset_brick_walls1)
	.byte <.bank(wallset_brick_walls2)
	.byte <.bank(wallset_blue_walls1)
	.byte <.bank(wallset_blue_walls2)
	.byte <.bank(wallset_blue_walls1)
	.byte <.bank(wallset_blue_walls2)
	.byte <.bank(wallset_blue_walls1)
	.byte <.bank(wallset_blue_walls2)
	.byte <.bank(wallset_drow_walls1)
	.byte <.bank(wallset_drow_walls2)
	.byte <.bank(wallset_drow_walls1)
	.byte <.bank(wallset_drow_walls2)
	.byte <.bank(wallset_drow_walls1)
	.byte <.bank(wallset_drow_walls2)
	.byte <.bank(wallset_green_walls1)
	.byte <.bank(wallset_green_walls2)
	.byte <.bank(wallset_green_walls1)
	.byte <.bank(wallset_green_walls2)
	.byte <.bank(wallset_xanatha_walls1)
	.byte <.bank(wallset_xanatha_walls2)

levelWallsetDrawLo:
	.byte <wallset_brick_walls1
	.byte <wallset_brick_walls2
	.byte <wallset_brick_walls1
	.byte <wallset_brick_walls2
	.byte <wallset_brick_walls1
	.byte <wallset_brick_walls2
	.byte <wallset_blue_walls1
	.byte <wallset_blue_walls2
	.byte <wallset_blue_walls1
	.byte <wallset_blue_walls2
	.byte <wallset_blue_walls1
	.byte <wallset_blue_walls2
	.byte <wallset_drow_walls1
	.byte <wallset_drow_walls2
	.byte <wallset_drow_walls1
	.byte <wallset_drow_walls2
	.byte <wallset_drow_walls1
	.byte <wallset_drow_walls2
	.byte <wallset_green_walls1
	.byte <wallset_green_walls2
	.byte <wallset_green_walls1
	.byte <wallset_green_walls2
	.byte <wallset_xanatha_walls1
	.byte <wallset_xanatha_walls2

levelWallsetDrawHi:
	.byte >wallset_brick_walls1
	.byte >wallset_brick_walls2
	.byte >wallset_brick_walls1
	.byte >wallset_brick_walls2
	.byte >wallset_brick_walls1
	.byte >wallset_brick_walls2
	.byte >wallset_blue_walls1
	.byte >wallset_blue_walls2
	.byte >wallset_blue_walls1
	.byte >wallset_blue_walls2
	.byte >wallset_blue_walls1
	.byte >wallset_blue_walls2
	.byte >wallset_drow_walls1
	.byte >wallset_drow_walls2
	.byte >wallset_drow_walls1
	.byte >wallset_drow_walls2
	.byte >wallset_drow_walls1
	.byte >wallset_drow_walls2
	.byte >wallset_green_walls1
	.byte >wallset_green_walls2
	.byte >wallset_green_walls1
	.byte >wallset_green_walls2
	.byte >wallset_xanatha_walls1
	.byte >wallset_xanatha_walls2
.endproc

.export installDrawMonsterHelper 
.proc installDrawMonsterHelper
	ldx levelIndex
	dex
direct:txa
	asl
	tax

	clc
	lda levelMonsterLo,x
	sta mon0normal+1
	adc #3
	sta mon0flipped+1
	lda levelMonsterHi,x
	sta mon0normal+2
	adc #0
	sta mon0flipped+2
	lda levelMonsterBank,x
	sta monsterBanks+0
	inx
	clc
	lda levelMonsterLo,x
	sta mon1normal+1
	adc #3
	sta mon1flipped+1
	lda levelMonsterHi,x
	sta mon1normal+2
	adc #0
	sta mon1flipped+2
	lda levelMonsterBank,x
	sta monsterBanks+1
	rts

levelMonsterBank:
		.byte <.bank(mon_kobold),	<.bank(mon_leech)
		.byte <.bank(mon_zombie), 	<.bank(mon_skeleton)
		.byte <.bank(mon_kuotoa), 	<.bank(mon_flind)
		.byte <.bank(mon_spider), 	<.bank(mon_spider)
		.byte <.bank(mon_dwarf), 	<.bank(mon_spider)
		.byte <.bank(mon_kenku), 	<.bank(mon_mage)
		.byte <.bank(mon_drowelf), 	<.bank(mon_skelwar)
		.byte <.bank(mon_drider), 	<.bank(mon_hellhnd)
		.byte <.bank(mon_rust), 	<.bank(mon_disbeast)
		.byte <.bank(mon_shindia), 	<.bank(mon_mantis)
		.byte <.bank(mon_xorn), 	<.bank(mon_mflayer)
		.byte <.bank(mon_xanath), 	<.bank(mon_golem)

levelMonsterLo:
		.byte <mon_kobold,	<mon_leech
		.byte <mon_zombie, 	<mon_skeleton
		.byte <mon_kuotoa, 	<mon_flind
		.byte <mon_spider, 	<mon_spider
		.byte <mon_dwarf, 	<mon_spider
		.byte <mon_kenku, 	<mon_mage
		.byte <mon_drowelf, 	<mon_skelwar
		.byte <mon_drider, 	<mon_hellhnd
		.byte <mon_rust, 	<mon_disbeast
		.byte <mon_shindia, 	<mon_mantis
		.byte <mon_xorn, 	<mon_mflayer
		.byte <mon_xanath, 	<mon_golem

levelMonsterHi:
		.byte >mon_kobold,	>mon_leech
		.byte >mon_zombie, 	>mon_skeleton
		.byte >mon_kuotoa, 	>mon_flind
		.byte >mon_spider, 	>mon_spider
		.byte >mon_dwarf, 	>mon_spider
		.byte >mon_kenku, 	>mon_mage
		.byte >mon_drowelf, 	>mon_skelwar
		.byte >mon_drider, 	>mon_hellhnd
		.byte >mon_rust, 	>mon_disbeast
		.byte >mon_shindia, 	>mon_mantis
		.byte >mon_xorn, 	>mon_mflayer
		.byte >mon_xanath, 	>mon_golem
.endproc
.export installDrawMonsterHelperDirect=installDrawMonsterHelper::direct

.segment "LEVEL1"
level1:
	.incbin "resources/LEVEL1.prg.b2",2

.segment "LEVEL2"
level2:
	.incbin "resources/LEVEL2.prg.b2",2

.segment "LEVEL3"
level3:
	.incbin "resources/LEVEL3.prg.b2",2

.segment "LEVEL4"
level4:
	.incbin "resources/LEVEL4.prg.b2",2

.segment "LEVEL5"
level5:
	.incbin "resources/LEVEL5.prg.b2",2

.segment "LEVEL6"
level6:
	.incbin "resources/LEVEL6.prg.b2",2

.segment "LEVEL7"
level7:
	.incbin "resources/LEVEL7.prg.b2",2

.segment "LEVEL8"
level8:
	.incbin "resources/LEVEL8.prg.b2",2

.segment "LEVEL9"
level9:
	.incbin "resources/LEVEL9.prg.b2",2

.segment "LEVEL10"
level10:
	.incbin "resources/LEVEL10.prg.b2",2

.segment "LEVEL11"
level11:
	.incbin "resources/LEVEL11.prg.b2",2

.segment "LEVEL12"
level12:
	.incbin "resources/LEVEL12.prg.b2",2
