.macro setBitBltSrc bitmap, screen, d800, stride
	lda #<(bitmap)
	sta SRC_BITMAP+0
	lda #>(bitmap)
	sta SRC_BITMAP+1
	lda #<(screen)
	sta SRC_SCREEN+0
	lda #>(screen)
	sta SRC_SCREEN+1
	lda #<(d800)
	sta SRC_D800+0
	lda #>(d800)
	sta SRC_D800+1
	lda #(stride)
	jsr setBitBltSource
.endmacro

.macro setBitBltDst bitmap, screen
	lda #>(bitmap)
	sta dstBitmapHi
	lda #>(screen)
	sta dstScreenHi
.endmacro

.macro wait ticks
		lda #ticks*3
		jsr waitWithAbort
		beq :+
			jmp abort
		:
.endmacro

BLTSX = RESULT+0
BLTSY = RESULT+1
BLTDX = RESULT+2
BLTDY = RESULT+3
BLTW  = RESULT+4
BLTH  = RESULT+5
