.include "../global.inc"
.include "bitblt.inc"

.include "../converters/intro/compressed/intro_castle_entrance_sprites.inc"
sprites=$e000
cookieCutter	= sprites+intro_cityzoom_overlaysprites
heroesEnter		= sprites+intro_castle_entrance_heroes_enter
heroesExit		= sprites+intro_castle_entrance_heroes_exit

.segment "INTRO_RAM"
.proc screenMode
	.word init
	.word teardown
	.byte 2
	.word $20, top
	.word 209, hideSprites

	.proc top
		lda #1
		sta $d01b
		jmp exitIrq
	.endproc

	.proc hideSprites
		lda #$ff
		sta $d01b
		jmp exitIrq
	.endproc

	.proc init
		jsrf :+
		rts
.pushseg
.segment "INTRO"
:
		lda #$1b
		sta $d011
		lda #$18
		sta $d016
		lda $dd00
		and #$fc
		ora #<((zoom_font_8_9/$4000)^3)
		sta $dd00
		lda #<(((zoom_screen_7/$400)<<4) | (zoom_font_8_9/$400)&$e)
		sta $d018

		lda #<(cookieCutter/64)
		sta zoom_screen_7+$3f8
		lda #104
		sta $d000
		lda #189
		sta $d001

		lda #0
		sta $d017
		sta $d010
		
		lda #$20
		sta $d01c

		lda #$3f
		sta $d015

		lda #1 ;Cookie cutter behind chars and x-expanded
		sta $d01b
		sta $d01d

		lda #$01
		sta $d025
		lda #$0a
		sta $d026

		lda #$9
		sta $d027

		lda #$0
		sta $d028
		lda #$e
		sta $d029
		lda #$9
		sta $d02a
		sta $d02b
		lda #$5
		sta $d02c

		lda #$9
		sta $d021
		lda #$8
		sta $d022
		lda #$f
		sta $d023
		rts
.popseg
	.endproc

	.proc teardown
		lda #0
		sta $d011
		sta $d015
		sta $d020
		rts
	.endproc
.endproc

.segment "INTRO2"

; x=index 0..16
; a=frame 0 or 1
.proc animateChar
		ldy obj_y,x

		clc
		lda row_lo,y
		adc obj_x,x
		sta SRC+0
		sta DST+0
		php
		lda dst_row_hi,y
		adc #0
		sta DST+1
		plp

		lda ARGS+0
		beq :+
			lda src0_row_hi,y
			jmp :++
		:
			lda src1_row_hi,y
		:
		adc #0
		sta SRC+1

		ldy obj_h,x
		sty COUNT

		rowLoop:
			ldy obj_w,x
			dey
			colLoop:
				lda (SRC),y
				sta (DST),y
				dey
			bpl colLoop

			clc
			lda SRC+0
			adc #40
			sta SRC+0
			sta DST+0
			bcc :+
				inc SRC+1
				inc DST+1
			:

			dec COUNT
		bne rowLoop

		rts

obj_x:	.byte 1,3,5,8
		.byte 3,5,8,16
		.byte 13,15,17
		.byte 4,12
		.byte 10,15,19
		.byte 13

obj_y:	.byte 3,3,3,3
		.byte 6,5,5,5
		.byte 7,8,8
		.byte 9,9
		.byte 10,10,10
		.byte 14

obj_w:	.byte 1,2,1,1
		.byte 2,3,2,3
		.byte 2,1,1
		.byte 2,1
		.byte 3,1,1
		.byte 2

obj_h:	.byte 1,2,1,1
		.byte 1,3,1,3
		.byte 2,1,1
		.byte 1,1
		.byte 3,1,1
		.byte 2

row_lo:		.repeat 17,I
				.byte <(zoom_screen_7+10+40*(3+I))
			.endrep
dst_row_hi:	.repeat 17,I
				.byte >(zoom_screen_7+10+40*(3+I))
			.endrep

src0_row_hi:.repeat 17,I
				.byte >(zoom_screen_8+10+40*(3+I))
			.endrep

src1_row_hi:.repeat 17,I
				.byte >(zoom_screen_9+10+40*(3+I))
			.endrep
.endproc

.segment "INTRO"

; c=0 => enter, c=1 => Exit
; x: animation
; a: xpos
; y: ypos
.proc placeHeroes
		php

		; 1. +5, +0
		; 2. +6, +0
		; 3. +0, +0
		; 4. +5, +6
		; 5. +5, +0
		clc
		sta $d000+2*3 ;+0
		adc #5
		sta $d000+2*1 ;+5
		sta $d000+2*4 ;+5
		sta $d000+2*5 ;+5
		adc #1
		sta $d000+2*2 ;+6
		tya
		sta $d001+2*1 ;+0
		sta $d001+2*2 ;+0
		sta $d001+2*3 ;+0
		sta $d001+2*5 ;+0
		adc #6
		sta $d001+2*4 ;+6

		plp
		bcs :+
			lda spr1a,x
			sta zoom_screen_7+$3f9
			lda spr2a,x
			sta zoom_screen_7+$3fa
			lda spr3a,x
			sta zoom_screen_7+$3fb
			lda spr4a,x
			sta zoom_screen_7+$3fc
			lda spr0a,x
			sta zoom_screen_7+$3fd
			rts
		:
			lda spr1b,x
			sta zoom_screen_7+$3f9
			lda spr2b,x
			sta zoom_screen_7+$3fa
			lda spr3b,x
			sta zoom_screen_7+$3fb
			lda spr4b,x
			sta zoom_screen_7+$3fc
			lda spr0b,x
			sta zoom_screen_7+$3fd
		rts

spr0a:	.byte <(heroesEnter/64+5*0+0),<(heroesEnter/64+5*1+0),<(heroesEnter/64+5*2+0)
spr1a:	.byte <(heroesEnter/64+5*0+1),<(heroesEnter/64+5*1+1),<(heroesEnter/64+5*2+1)
spr2a:	.byte <(heroesEnter/64+5*0+2),<(heroesEnter/64+5*1+2),<(heroesEnter/64+5*2+2)
spr3a:	.byte <(heroesEnter/64+5*0+3),<(heroesEnter/64+5*1+3),<(heroesEnter/64+5*2+3)
spr4a:	.byte <(heroesEnter/64+5*0+4),<(heroesEnter/64+5*1+4),<(heroesEnter/64+5*2+4)

spr0b:	.byte <(heroesExit/64+5*0+0),<(heroesExit/64+5*1+0),<(heroesExit/64+5*2+0)
spr1b:	.byte <(heroesExit/64+5*0+1),<(heroesExit/64+5*1+1),<(heroesExit/64+5*2+1)
spr2b:	.byte <(heroesExit/64+5*0+2),<(heroesExit/64+5*1+2),<(heroesExit/64+5*2+2)
spr3b:	.byte <(heroesExit/64+5*0+3),<(heroesExit/64+5*1+3),<(heroesExit/64+5*2+3)
spr4b:	.byte <(heroesExit/64+5*0+4),<(heroesExit/64+5*1+4),<(heroesExit/64+5*2+4)
.endproc

.proc init
		memcpy zoom_screen_7, zoom_screen_8, 1000
		jsrf depackSprites

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode
		rts
.endproc

animFrames:	.byte 0,1,2,1
.pushseg
.segment "INTRO_RAM"
frameIndex: .byte 0
heroesXpos: .byte 210
heroesYpos: .byte 85
.popseg

.export waterdeep_exit_scene
.proc waterdeep_exit_scene
		lda #210-107
		sta heroesXpos
		lda #85+107
		sta heroesYpos
		jsrf fetchWaterdeep
		jsr init

		lda #70
		sta TMP2+1
		loop:
			ldy frameIndex
			ldx animFrames,y
			iny
			tya
			and #3
			sta frameIndex

			lda heroesXpos
			ldy heroesYpos
			sec
			jsr placeHeroes
			inc heroesXpos
			dec heroesYpos

			lda #5
			sta TMP
			tileChangeLoop:
				; 0..16
				jsr rnd
				and #15
				sta TMP2
				jsr rnd
				and #1
				clc
				adc TMP2
				tax

				jsr rnd
				and #1
				sta ARGS+0
				jsrf animateChar
				dec TMP
			bne tileChangeLoop
			wait 3
;		jmp loop
			dec TMP2+1
		bne loop
abort:
		rts
.endproc

.export waterdeep_entry_scene
.proc waterdeep_entry_scene
		jsr init

		lda #70
		sta TMP2+1
		loop:
			ldy frameIndex
			ldx animFrames,y
			iny
			tya
			and #3
			sta frameIndex

			lda heroesXpos
			ldy heroesYpos
			clc
			jsr placeHeroes
			dec heroesXpos
			inc heroesYpos

			lda #5
			sta TMP
			tileChangeLoop:
				; 0..16
				jsr rnd
				and #15
				sta TMP2
				jsr rnd
				and #1
				clc
				adc TMP2
				tax

				jsr rnd
				and #1
				sta ARGS+0
				jsrf animateChar
				dec TMP
			bne tileChangeLoop
			wait 3
;		jmp loop
			dec TMP2+1
		bne loop
abort:
		rts
.endproc

.segment "INTRO9"
.proc depackSprites
		lda #<sprites
		sta BB_DST+0
		lda #>sprites
		sta BB_DST+1
		ldy #<spriteData
		ldx #>spriteData
		jmp decrunchTo
spriteData:
		.incbin "../converters/intro/compressed/intro_castle_entrance_sprites.prg.b2",2
.endproc

