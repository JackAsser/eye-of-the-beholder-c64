tower_screen0	= $c000
tower_screen1	= $c400
tower_font		= $c800
tower_sprites 	= $e800

.include "../global.inc"
.define MULTIPLEXTIMINGS_LO CUR_ITEM
.define MULTIPLEXTIMINGS_HI TOP_ITEM

.segment "INTRO_RAM"
carpet_offset: .byte 0
scrollAmount: .byte 0

.proc screenMode
	.word init
	.word teardown
	.byte 2
	.word $1a, enableSprites
	.word $e0, prepareMultiplexer

	multiplexes_left: .byte 0

	.proc increaseSpritePositions
		clc
		lda $d001
		adc #21
		; Fallthrough to setSpritePositions
	.endproc

	.proc setSpritePositions
		.repeat 8,I
			sta $d001+I*2
		.endrep
		rts
	.endproc

	.proc _tower_updateCarpet
		jsrf tower_updateCarpet
		rts
	.endproc

	.proc timerSplit
		pha
		lda $dd00
		and #$fc
		ora #$01
		sta $dd00
		lda #$08
		sta $d016
		pla
		rti
	.endproc

	.proc enableSprites
		jsrf :+
.pushseg
.segment "INTRO"
timerLo:
		.byte <(190*63-20)
		.byte <(190*65-20)
		.byte <(190*64-20)
		.byte <(190*65-20)
timerHi:
		.byte >(190*63-20)
		.byte >(190*65-20)
		.byte >(190*64-20)
		.byte >(190*65-20)

:
		ldx system
		lda timerLo,x
		sta $dd04
		lda timerHi,x
		sta $dd05
		bit $dd0d
		lda #$81
		sta $dd0d
		lda #$19
		sta $dd0e

		lda #<timerSplit
		sta $0318
		lda #>timerSplit
		sta $0319

		lda $dd00
		and #$fc
		sta $dd00
		lda #$18
		sta $d016
		rts
.popseg

		lda multiplexes_left
		beq :+
			lda #$ff
			sta $d015
			lda #$1b
			sta $d011
			jmp nextMultiplex
		:
		jmp exitIrq_quick
	.endproc

	.proc prepareMultiplexer
		jsrf :+
		jmp exitIrq

.pushseg
.segment "INTRO_RAM"
multiplex_count: 	.byte 8
multiplex_start: 	.byte $32
multiplex_offset: 	.byte 9
speed:				.byte 2
.popseg

.pushseg
.segment "INTRO2"
:
		lda #0
		sta $d015
		lda multiplex_offset
		sta carpet_offset
		lda #$12
		sta $d018
		jsr _tower_updateCarpet
		lda RESULT+0
		sta $d01c
		lda RESULT+1
		sta $d027
		lda RESULT+2
		sta $d028
		lda RESULT+3
		sta $d029
		lda RESULT+4
		sta $d02a
		lda RESULT+5
		sta $d02b
		lda #$02
		sta $d018
		lda multiplex_count
		sta multiplexes_left
		lda multiplex_start
		jsr setSpritePositions
		jsr scroll
		rts

		.proc scroll
			ldy speed
			dey
			bne :+
				jsr normal
				ldy #2
			:
			sty speed
			rts
	normal:
			ldx scrollAmount
			beq :+
				dec scrollAmount
				ldy multiplex_offset
				ldx multiplex_start 
				inx
				cpx #$33
				bne skip
					dey
					ldx #$32-20
				skip:
				stx multiplex_start
				sty multiplex_offset
			:
			rts
		.endproc
.popseg
	.endproc

	.proc nextMultiplex
		lda $d001
		clc
		adc #21-3
		sta $d012
		tax
		and #7
		tay
		cmp #0
		bne :+
			dex
			dex
			stx $d012 ; Timing #0 requires somewhat stable IRQ, get two more lines to stabalize on
		:
		lda (MULTIPLEXTIMINGS_LO),y
		sta RASTER_IRQ+0
		lda (MULTIPLEXTIMINGS_HI),y
		sta RASTER_IRQ+1
		jsr _tower_updateCarpet
		jmp exitIrq_rerun
	.endproc

	.proc checkNextMultiplex
		dec multiplexes_left
		bne nextMultiplex
		jmp exitIrq_quick
	.endproc

	.proc _multiplex0_newntsc_drean
		nop
		jmp _multiplex0
	.endproc

	.proc _multiplex0_oldntsc
		jmp _multiplex0
	.endproc

	.proc _multiplex0
		jsr increaseSpritePositions
		pha
		pla
		pha
		pla
		pha
		pla
		ldy RESULT+1
		nop
		nop

		ldx RESULT+0
		bit $ff
		lda $d018
		eor #$10
		sta $d018
		lda RESULT+2
		sta $d028
		lda RESULT+4
		sta $d02a
		lda RESULT+3
		sta $d029
		sty $d027
		stx $d01c
		lda RESULT+5
		sta $d02b
		jmp checkNextMultiplex
	.endproc

	.proc _multiplex1_oldntsc_newntsc_drean
		jmp _multiplex1
	.endproc

	.proc _multiplex1
		jsr increaseSpritePositions

		lda $d018
		eor #$10
		sta $d018
		nop
		nop
		bit $ff
		lda RESULT+0
		sta $d01c
		lda RESULT+1
		sta $d027
		lda RESULT+2
		sta $d028
		lda RESULT+3
		sta $d029
		lda RESULT+4
		sta $d02a
		lda RESULT+5
		sta $d02b
		jmp checkNextMultiplex
	.endproc

	.proc _multiplex7
		lda 1
		pha
		lda #$35
		sta 1
		lda $fffe
		pha
		lda $ffff
		pha
		ldx system
		lda semiStable_lo,x
		sta $fffe
		lda semiStable_hi,x
		sta $ffff
		tsx
		inc $d012
		dec $d019
		cli
		.repeat 20
			nop
		.endrep
		.byte 2

semiStable_lo:
		.byte <semiStable_pal
		.byte <semiStable_newntsc_drean
		.byte <semiStable_oldntsc
		.byte <semiStable_newntsc_drean
semiStable_hi:
		.byte >semiStable_pal
		.byte >semiStable_newntsc_drean
		.byte >semiStable_oldntsc
		.byte >semiStable_newntsc_drean

semiStable_pal:
		txs
		jsr increaseSpritePositions
		lda $d018
		eor #$10
		tay
		jmp semiStableContinue

semiStable_oldntsc:
		txs
		jsr increaseSpritePositions
		lda $d018
		eor #$10
		tay
		nop
		jmp semiStableContinue

semiStable_newntsc_drean:
		txs
		jsr increaseSpritePositions
		lda $d018
		eor #$10
		tay
		nop
		nop
		jmp semiStableContinue

semiStableContinue:
		pha
		pla
		ldx RESULT+2
		lda RESULT+1
		sty $d018
		stx $d028
		sta $d027
		lda RESULT+4
		sta $d02a
		lda RESULT+3
		sta $d029
		lda RESULT+0
		sta $d01c
		lda RESULT+5
		sta $d02b
		pla
		sta $ffff
		pla 
		sta $fffe
		pla
		sta 1
		jmp checkNextMultiplex
	.endproc

	.proc multiplexX
		jmp exitIrq_quick
	.endproc

	multiplex0_pal=_multiplex0
	multiplex1_pal=_multiplex1
	multiplex2_pal=_multiplex0
	multiplex3_pal=_multiplex0
	multiplex4_pal=_multiplex1
	multiplex5_pal=_multiplex0
	multiplex6_pal=_multiplex1
	multiplex7_pal=_multiplex7

	multiplex0_oldntsc=_multiplex0_oldntsc
	multiplex1_oldntsc=_multiplex1_oldntsc_newntsc_drean
	multiplex2_oldntsc=_multiplex0_oldntsc
	multiplex3_oldntsc=_multiplex0_oldntsc
	multiplex4_oldntsc=_multiplex1_oldntsc_newntsc_drean
	multiplex5_oldntsc=_multiplex0_oldntsc
	multiplex6_oldntsc=_multiplex1_oldntsc_newntsc_drean
	multiplex7_oldntsc=_multiplex7

	multiplex0_newntsc_drean=_multiplex0_newntsc_drean
	multiplex1_newntsc_drean=_multiplex1_oldntsc_newntsc_drean
	multiplex2_newntsc_drean=_multiplex0_newntsc_drean
	multiplex3_newntsc_drean=_multiplex0_newntsc_drean
	multiplex4_newntsc_drean=_multiplex1_oldntsc_newntsc_drean
	multiplex5_newntsc_drean=_multiplex0_newntsc_drean
	multiplex6_newntsc_drean=_multiplex1_oldntsc_newntsc_drean
	multiplex7_newntsc_drean=_multiplex7

	.proc init
		lda #$18
		sta $d016
		lda #$02
		sta $d018

		ldx system
		cpx #SYSTEM_PAL
		bne :+
			lda #<multiplexTimings_pal_lo
			sta MULTIPLEXTIMINGS_LO+0
			lda #>multiplexTimings_pal_lo
			sta MULTIPLEXTIMINGS_LO+1
			lda #<multiplexTimings_pal_hi
			sta MULTIPLEXTIMINGS_HI+0
			lda #>multiplexTimings_pal_hi
			sta MULTIPLEXTIMINGS_HI+1
			rts
		:
		cpx #SYSTEM_NTSC_OLD
		bne :+
			lda #<multiplexTimings_oldntsc_lo
			sta MULTIPLEXTIMINGS_LO+0
			lda #>multiplexTimings_oldntsc_lo
			sta MULTIPLEXTIMINGS_LO+1
			lda #<multiplexTimings_oldntsc_hi
			sta MULTIPLEXTIMINGS_HI+0
			lda #>multiplexTimings_oldntsc_hi
			sta MULTIPLEXTIMINGS_HI+1
			rts
		:
		lda #<multiplexTimings_newntsc_drean_lo
		sta MULTIPLEXTIMINGS_LO+0
		lda #>multiplexTimings_newntsc_drean_lo
		sta MULTIPLEXTIMINGS_LO+1
		lda #<multiplexTimings_newntsc_drean_hi
		sta MULTIPLEXTIMINGS_HI+0
		lda #>multiplexTimings_newntsc_drean_hi
		sta MULTIPLEXTIMINGS_HI+1
		rts

multiplexTimings_pal_lo: .byte <multiplex7_pal,<multiplex4_pal,<multiplex1_pal,<multiplex6_pal,<multiplex3_pal,<multiplex0_pal,<multiplex5_pal,<multiplex2_pal
multiplexTimings_pal_hi: .byte >multiplex7_pal,>multiplex4_pal,>multiplex1_pal,>multiplex6_pal,>multiplex3_pal,>multiplex0_pal,>multiplex5_pal,>multiplex2_pal
multiplexTimings_oldntsc_lo: .byte <multiplex7_oldntsc,<multiplex4_oldntsc,<multiplex1_oldntsc,<multiplex6_oldntsc,<multiplex3_oldntsc,<multiplex0_oldntsc,<multiplex5_oldntsc,<multiplex2_oldntsc
multiplexTimings_oldntsc_hi: .byte >multiplex7_oldntsc,>multiplex4_oldntsc,>multiplex1_oldntsc,>multiplex6_oldntsc,>multiplex3_oldntsc,>multiplex0_oldntsc,>multiplex5_oldntsc,>multiplex2_oldntsc
multiplexTimings_newntsc_drean_lo: .byte <multiplex7_newntsc_drean,<multiplex4_newntsc_drean,<multiplex1_newntsc_drean,<multiplex6_newntsc_drean,<multiplex3_newntsc_drean,<multiplex0_newntsc_drean,<multiplex5_newntsc_drean,<multiplex2_newntsc_drean
multiplexTimings_newntsc_drean_hi: .byte >multiplex7_newntsc_drean,>multiplex4_newntsc_drean,>multiplex1_newntsc_drean,>multiplex6_newntsc_drean,>multiplex3_newntsc_drean,>multiplex0_newntsc_drean,>multiplex5_newntsc_drean,>multiplex2_newntsc_drean

	.endproc

	.proc teardown
		lda #0
		sta $d011
		lda $dd00
		and #$fc
		sta $dd00
		rts
	.endproc
.endproc

.segment "INTRO"
intro_tower_spritessheet_gfx:
		.incbin  "../converters/intro/compressed/intro_tower_spritesheet.prg.b2",2
		.include "../converters/intro/compressed/intro_tower_spritesheet.inc"

intro_tower_magecircle_gfx:
		.incbin  "../converters/intro/compressed/intro_tower_magecircle.prg.b2",2
		.include "../converters/intro/compressed/intro_tower_magecircle.inc"

.export tower_scene
.proc tower_scene
		lda #<tower_sprites
		sta BB_DST+0
		lda #>tower_sprites
		sta BB_DST+1
		ldy #<intro_tower_spritessheet_gfx
		ldx #>intro_tower_spritessheet_gfx
		jsr decrunchTo

		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<intro_tower_magecircle_gfx
		ldx #>intro_tower_magecircle_gfx
		jsr decrunchTo

		memcpy tower_font, (freeRam+intro_tower_magecircle_charset), intro_tower_magecircle_charset_size

		ldx #0
		txa
		:
			sta tower_screen0+$000,x
			sta tower_screen0+$100,x
			sta tower_screen0+$200,x
			sta tower_screen0+$300,x
			sta tower_screen1+$000,x
			sta tower_screen1+$100,x
			sta tower_screen1+$200,x
			sta tower_screen1+$300,x
			sta $d800,x
			sta $d900,x
			sta $da00,x
			sta $db00,x
			inx
		bne :-

		; Sprites 0..5 is the tower	(multi-color)
		; Sprites 6..7 is the underlay (x-expanded, single color)
		lda #$00
		sta $d010
		sta $d017
		sta $d01b
		lda #$c0
		sta $d01d
		lda #$ff
		sta $d015
		lda #$0c
		sta $d025
		lda #$0f
		sta $d026
		lda #$1
		sta $d02c
		lda #$0
		sta $d02d
		sta $d02e

		; Place overlay
		ldx #6
		ldy #0
		clc
		lda #24+(320-18*8)/2
		:
			sta $d000,y
			adc #24
			iny
			iny
			dex
		bne :-

		; Place underlay
		lda #24+(320-12*8)/2
		sta $d00c
		lda #24+(320-12*8)/2+6*8
		sta $d00e

		; Prepare gfx blit
		lda #<(tower_screen0+(40-12)/2+6*40)
		sta DST_SCREEN+0
		sta DST_D800+0
		sta TMP3+0
		lda #>(tower_screen0+(40-12)/2+6*40)
		sta DST_SCREEN+1
		lda #>(tower_screen1+(40-12)/2+6*40)
		sta TMP3+1
		lda #>($d800+(40-12)/2+6*40)
		sta DST_D800+1
		lda #$08
		sta $d022
		lda #$0e
		sta $d023
		lda #12
		sta TMP

		; Mage circle is 18, but we only want to show 12. Skip 3 initially, then skip 6
		ldx #3
		rowLoop:
			ldy #0
			:
				lda freeRam+intro_tower_magecircle_screen,x
				sta (DST_SCREEN),y
				sta (TMP3),y
				lda freeRam+intro_tower_magecircle_d800,x
				sta (DST_D800),y
				inx
				iny
				cpy #12
			bne :-

			clc
			txa
			adc #6
			tax

			lda DST_SCREEN+0
			adc #40
			sta DST_SCREEN+0
			sta TMP3+0
			sta DST_D800+0
			bcc :+
				inc DST_SCREEN+1
				inc TMP3+1
				inc DST_D800+1
			:
			dec TMP
		bne rowLoop

		lda #1
		ldx #0
		ldy #0
		jsr showText
		lda #1
		ldx #1
		ldy #1
		jsr showText

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

		lda #50
		jsr waitWithAbort

		lda #159
		sta scrollAmount

		:
			lda #1
			jsr waitWithAbort
			bne abort
			lda scrollAmount
		bne :-

		lda #1
		ldx #2
		ldy #0
		jsr showText
		lda #1
		ldx #3
		ldy #1
		jsr showText

		lda #150
		jsr waitWithAbort
abort:
		rts
.endproc

.proc tower_updateCarpet
		ldx carpet_offset
		inc carpet_offset
		jsr latch
		lda $d018
		and #$f0
		bne updateScreen0
updateScreen1:
		lda spr0,x
		sta tower_screen1+$3f8
		lda spr1,x
		sta tower_screen1+$3f9
		lda spr2,x
		sta tower_screen1+$3fa
		lda spr3,x
		sta tower_screen1+$3fb
		lda spr4,x
		sta tower_screen1+$3fc
		lda spr5,x
		sta tower_screen1+$3fd
		lda spr6,x
		sta tower_screen1+$3fe
		lda spr7,x
		sta tower_screen1+$3ff
		rts

updateScreen0:
		lda spr0,x
		sta tower_screen0+$3f8
		lda spr1,x
		sta tower_screen0+$3f9
		lda spr2,x
		sta tower_screen0+$3fa
		lda spr3,x
		sta tower_screen0+$3fb
		lda spr4,x
		sta tower_screen0+$3fc
		lda spr5,x
		sta tower_screen0+$3fd
		lda spr6,x
		sta tower_screen0+$3fe
		lda spr7,x
		sta tower_screen0+$3ff
		rts

latch:	
		lda _d01c,x
		sta RESULT+0
		lda _d027,x
		sta RESULT+1
		lda _d028,x
		sta RESULT+2
		lda _d029,x
		sta RESULT+3
		lda _d02a,x
		sta RESULT+4
		lda _d02b,x
		sta RESULT+5
		rts

s = (<(tower_sprites/$40))
spr0:	.byte s+12, s+18,s+24,s+30,s+36,s+40,s+44,s+52,s+59,    s+00,s+06,s+12,s+00,s+06,s+12,s+00,s+06,s+12
spr1:	.byte s+13, s+19,s+25,s+31,s+37,s+41,s+45,s+53,s+60,    s+01,s+07,s+13,s+01,s+07,s+13,s+01,s+07,s+13
spr2:	.byte s+14, s+20,s+26,s+32,s+65,s+65,s+46,s+54,s+61,    s+02,s+08,s+14,s+02,s+08,s+14,s+02,s+08,s+14
spr3:	.byte s+15, s+21,s+27,s+33,s+65,s+65,s+47,s+55,s+62,    s+03,s+09,s+15,s+03,s+09,s+15,s+03,s+09,s+15
spr4:	.byte s+16, s+22,s+28,s+34,s+38,s+42,s+48,s+56,s+63,    s+04,s+10,s+16,s+04,s+10,s+16,s+04,s+10,s+16
spr5:	.byte s+17, s+23,s+29,s+35,s+39,s+43,s+49,s+57,s+64,    s+05,s+11,s+17,s+05,s+11,s+17,s+05,s+11,s+17
spr6:	.byte s+58, s+58,s+65,s+65,s+65,s+65,s+50,s+58,s+58,    s+58,s+58,s+58,s+58,s+58,s+58,s+58,s+58,s+58
spr7:	.byte s+58, s+58,s+65,s+65,s+65,s+65,s+51,s+58,s+58,    s+58,s+58,s+58,s+58,s+58,s+58,s+58,s+58,s+58
_d01c:	.byte $3f,  $3f, $3f, $33, $23, $23, $3f, $3f, $3f,     $3f, $3f, $3f, $3f, $3f, $3f, $3f, $3f, $3f
_d027:	.byte $b,   $b,  $b,  $1,  $1,  $1,  $1,  $1,  $b,      $b,  $b,  $b,  $b,  $b,  $b,  $b,  $b,  $b
_d028:	.byte $1,   $1,  $1,  $6,  $e,  $e,  $e,  $1,  $b,      $1,  $1,  $1,  $1,  $1,  $1,  $1,  $1,  $1 
_d029:	.byte $1,   $1,  $1,  $6,  $e,  $e,  $e,  $1,  $b,      $1,  $b,  $1,  $1,  $b,  $1,  $1,  $b,  $1 
_d02a:	.byte $1,   $1,  $b,  $6,  $6,  $6,  $6,  $1,  $b,      $1,  $b,  $1,  $1,  $b,  $1,  $1,  $b,  $1 
_d02b:	.byte $1,   $1,  $1,  $0,  $6,  $6,  $6,  $1,  $1,      $1,  $1,  $1,  $1,  $1,  $1,  $1,  $1,  $1 
.endproc
