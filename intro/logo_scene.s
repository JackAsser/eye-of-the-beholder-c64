.include "../global.inc"

vic_eotb_logo_fadeframe_top_charset 	= $8000
vic_eotb_logo_fadeframe_bottom_charset 	= $8800
vic_eotb_logo_fadeframe_screen 			= $a000
vic_eotb_logo_fadeframe_dbscreen 		= $a400
vic_eotb_logo_fadeframe_sprites 		= $a800
db_screen 								= $c000

.segment "INTRO_BSS"
spritesMap: .res 5*7
plexPos: .res 1

.segment "INTRO_RAM"
.proc screenMode
	.word init
	.word teardown
	.byte 10
	.word $28, resetMultiplexer
	.word $3b, modeSwitch
	.word $32+39+5+21*0, advanceSprites
	.word $32+39-1+21*1, multiplex
	.word $32+39-2+21*2, multiplexFix
	.word $32+39-1+21*3, multiplex
	.word $32+39-1+21*4, multiplex
	.word $32+39-1+21*5, multiplex
	.word $32+(5+14)*8-1, fontSwitch
	.word $32+39-1+21*6, justMultiplex

	.proc resetMultiplexer
d018:	lda #$38
		sta $d018
		lda $dd00
		and #$fc
dd00:	ora #$00
		sta $dd00
d021:	lda #$07
		sta $d021
d022:	lda #$0f
		sta $d022
d023:	lda #$01
		sta $d023
sprcolor:
		lda #0
		ldx #4
		:
			sta $d027,x
			dex
		bpl :-
sprxpos:ldx #24+52-5
		jsrf :+
.pushseg
.segment "INTRO5"	
:
		clc
		txa
		sta $d000
		adc #48
		sta $d002
		adc #48
		sta $d004
		adc #48
		sta $d006
		adc #48
		sta $d008
		lda #$00
		bcc :+
			ora #$10
		:
		sta $d010
		lda #$1b
		sta $d011
		lda #$32+39
		sta $d001
		sta $d003
		sta $d005
		sta $d007
		sta $d009
		ldx #4
		:
			lda spritesMap+0*5,x
			sta vic_screen+$3f8,x
			sta vic_eotb_logo_fadeframe_screen+$3f8,x
			lda spritesMap+1*5,x
			sta db_screen+$3f8,x
			sta vic_eotb_logo_fadeframe_dbscreen+$3f8,x
			dex
		bpl :-
		lda #2*5
		sta plexPos
		rts
.popseg
		jmp exitIrq
	.endproc

	.proc advanceSprites
		clc
		lda $d001
		adc #21
		sta $d001
		sta $d003
		sta $d005
		sta $d007
		sta $d009
		jmp exitIrq_quick
	.endproc

	.proc multiplexFix
		pha
		pla
		pha
		pla
		pha
		pla
		pha
		pla
		nop
		nop
		lda system
		cmp #SYSTEM_PAL
		beq :+
			nop
		:
	.endproc

	.proc multiplex
		lda $d018
d018:	eor #$30
		sta $d018
		ldx #0
		ldy plexPos
		and #$f0
d018cmp:cmp #$30
		bne :+
			lp0:
				lda spritesMap,y
				sta vic_screen+$3f8,x
				sta vic_eotb_logo_fadeframe_screen+$3f8,x
				inx
				iny
				cpx #5
			bne lp0
			jmp :++
		:
			lp1:
				lda spritesMap,y
				sta db_screen+$3f8,x
				sta vic_eotb_logo_fadeframe_dbscreen+$3f8,x
				inx
				iny
				cpx #5
			bne lp1
		:
		sty plexPos
		jmp advanceSprites
	.endproc

	.proc justMultiplex
		lda $d018
d018:	eor #$30
		sta $d018
		jmp exitIrq_quick
	.endproc

	.proc modeSwitch
		ldx #5
		:dex
		bne :-
d011:	lda #$3b
		sta $d011
		jmp exitIrq
	.endproc

	.proc fontSwitch
		ldx #10
		lda $d015
		beq :+
		ldx #6
		:
			dex
		bne :-
		lda $d018
		ora #2
		sta $d018
		jmp exitIrq
	.endproc

	.proc init
		jsrf :+
.pushseg
.segment "INTRO5"
:
		lda #$18
		sta $d016
		lda #6
		sta $d020
		lda #$1f
		sta $d015
		sta $d01d
		lda #0
		sta $d017
		sta $d01b
		sta $d01c

		rts
.popseg
		rts
	.endproc

	.proc teardown
		lda #0
		sta $d015
		sta $d021
		sta $d011
		lda #6
		sta $d020
		rts
	.endproc
.endproc
_d018 = screenMode::resetMultiplexer::d018+1
_dd00 = screenMode::resetMultiplexer::dd00+1
_d021 = screenMode::resetMultiplexer::d021+1
_d022 = screenMode::resetMultiplexer::d022+1
_d023 = screenMode::resetMultiplexer::d023+1
_lowerD011 = screenMode::modeSwitch::d011+1
_d018eor_a = screenMode::multiplex::d018+1
_d018cmp = screenMode::multiplex::d018cmp+1
_d018eor_b = screenMode::justMultiplex::d018+1
_sprcolor = screenMode::resetMultiplexer::sprcolor+1
_sprxpos = screenMode::resetMultiplexer::sprxpos+1

.segment "INTRO5"
eotb_logo_gfx:
		.incbin  "../converters/intro/compressed/eotb_logo.prg.b2",2
		.include "../converters/intro/compressed/eotb_logo.inc"
eotb_logo_fadeframe_gfx:
		.incbin  "../converters/intro/compressed/eotb_logo_fadeframe.prg.b2",2
		.include "../converters/intro/compressed/eotb_logo_fadeframe.inc"

.proc setSpritesMap
		lda spritesMaps_lo,x
		sta TMP+0
		lda spritesMaps_hi,x
		sta TMP+1
		ldy #5*7-1
		:
			lda (TMP),y
			sta spritesMap,y
			dey
		bpl :-
		rts

spritesMaps = freeRam+eotb_logo_spritesmap
spritesMaps_lo:
		.byte <(freeRam+eotb_logo_fade_spritesmap)
		.repeat 22,I
			.byte <(spritesMaps+I*5*7)
		.endrep
spritesMaps_hi:
		.byte >(freeRam+eotb_logo_fade_spritesmap)
		.repeat 22,I
			.byte >(spritesMaps+I*5*7)
		.endrep
.endproc

.proc setD800ForChars
		lda #6+8
		ldx #31
		:
			.repeat 16,I
				sta $d800+(5+I)*40+4,x
			.endrep
			dex
		bpl :-
		rts
.endproc

.proc setD800ForLogo
		lda #<(freeRam+eotb_logo_d800+5*40+4)
		sta SRC+0
		lda #>(freeRam+eotb_logo_d800+5*40+4)
		sta SRC+1
		lda #<($d800+5*40+4)
		sta DST+0
		lda #>($d800+5*40+4)
		sta DST+1

		ldx #16
		rowloop:
			ldy #0
			.repeat 32,I
				lda (SRC),y
				sta (DST),y
				iny
			.endrep

			clc
			lda SRC+0
			adc #40
			sta SRC+0
			bcc :+
				inc SRC+1
			:
			clc
			lda DST+0
			adc #40
			sta DST+0
			bcc :+
				inc DST+1
			:
			dex
			beq :+
		jmp rowloop
		:rts
.endproc

.export logo_scene
.proc logo_scene
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<eotb_logo_fadeframe_gfx
		ldx #>eotb_logo_fadeframe_gfx
		jsr decrunchTo
		memset vic_eotb_logo_fadeframe_screen, $00, 1000
		memset $d800, $06, 1000
		memset vic_eotb_logo_fadeframe_screen, $00, 1000
		memset vic_eotb_logo_fadeframe_dbscreen, $00, 1000
		memcpy vic_eotb_logo_fadeframe_top_charset, (freeRam+eotb_logo_fadeframe_top_charset), eotb_logo_fadeframe_top_charset_size
		memcpy vic_eotb_logo_fadeframe_screen+5*40, (freeRam+eotb_logo_fadeframe_top_screen), eotb_logo_fadeframe_top_screen_size
		memcpy vic_eotb_logo_fadeframe_dbscreen+5*40, (freeRam+eotb_logo_fadeframe_top_screen), eotb_logo_fadeframe_top_screen_size
		memcpy vic_eotb_logo_fadeframe_bottom_charset, (freeRam+eotb_logo_fadeframe_bottom_charset), eotb_logo_fadeframe_bottom_charset_size
		memcpy vic_eotb_logo_fadeframe_screen+(5+14)*40, (freeRam+eotb_logo_fadeframe_bottom_screen), eotb_logo_fadeframe_bottom_screen_size
		memcpy vic_eotb_logo_fadeframe_dbscreen+(5+14)*40, (freeRam+eotb_logo_fadeframe_bottom_screen), eotb_logo_fadeframe_bottom_screen_size
		memcpy vic_eotb_logo_fadeframe_sprites, (freeRam+eotb_logo_fade_sprites), eotb_logo_fade_sprites_size

		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<eotb_logo_gfx
		ldx #>eotb_logo_gfx
		jsr decrunchTo ;$2b00-65e3
		memcpy vic_bitmap, (freeRam+eotb_logo_bitmap), eotb_logo_bitmap_size
		memcpy vic_screen, (freeRam+eotb_logo_screen), eotb_logo_screen_size
		memcpy db_screen, (freeRam+eotb_logo_screen), eotb_logo_screen_size
		memcpy_underio $d000, (freeRam+eotb_logo_sprites), eotb_logo_sprites_size

		ldx #39
		:
			lda #40
			sta vic_screen,x
			lda #6
			sta $d800,x
			dex
		bpl :-

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

loop:
		lda #$06
		sta _d021
		sta _d022
		sta _d023
		lda #0
		sta $d015
		jsr setCharLogo
		beq :+
			jmp abort
		:

		lda #$04
		sta _d021
		lda #$06
		sta _d022
		sta _d023
		jsr setCharLogo
		beq :+
			jmp abort
		:

		lda #$0e
		sta _d021
		lda #$04
		sta _d022
		lda #$06
		sta _d023
		jsr setCharLogo
		beq :+
			jmp abort
		:

		lda #$0f
		sta _d021
		lda #$0e
		sta _d022
		lda #$04
		sta _d023
		jsr setCharLogo
		beq :+
			jmp abort
		:

		lda #$01
		sta _d021
		lda #$0f
		sta _d022
		lda #$0c
		sta _d023
		lda #$1f
		sta $d015
		jsr setCharLogo
		beq :+
			jmp abort
		:

		lda #$e0
		:cmp $d012
		bne :-
		ldx #1
		jsr setSpritesMap
		lda #$00
		sta _dd00
		lda #$38
		sta _d018
		lda #$3b
		sta _lowerD011
		lda #($38^$08)
		sta _d018eor_a
		sta _d018eor_b
		lda #$00
		sta _d018cmp
		lda #$07
		sta _d021
		lda #24+52
		sta _sprxpos
		lda #$a
		sta _sprcolor
		lda #$1f
		sta $d015
		jsr setD800ForLogo

		lda #100
		jsr waitWithAbort
		beq :+
			jmp abort
		:

		ldx #2
		:
			jsr setSpritesMap
			inx
			cpx #12
			beq :+
			lda #2
			jsr waitWithAbort
		jmp :-
		:
		jsr setSpritesMap

		lda #50
		jsr waitWithAbort

		:
			jsr setSpritesMap
			inx
			cpx #23
			beq :+
			lda #2
			jsr waitWithAbort
		jmp :-
		:

		lda #75
		jsr waitWithAbort

		lda #$07
		sta _d021
		lda #$0f
		sta _d022
		lda #$0a
		sta _d023
		jsr setCharLogo
		beq :+
			jmp abort
		:

		lda #$0f
		sta _d021
		lda #$0a
		sta _d022
		lda #$04
		sta _d023
		lda #0
		sta $d015
		jsr setCharLogo
		beq :+
			jmp abort
		:

		lda #$0c
		sta _d021
		lda #$04
		sta _d022
		lda #$06
		sta _d023
		jsr setCharLogo
		beq :+
			jmp abort
		:

		lda #$04
		sta _d021
		lda #$06
		sta _d022
		lda #$06
		sta _d023
		jsr setCharLogo
		beq :+
			jmp abort
		:

		lda #$06
		sta _d021
		lda #$06
		sta _d022
		lda #$06
		sta _d023
		jsr setCharLogo
		beq :+
			jmp abort
		:

;		lda #100
;		jsr waitWithAbort
;		beq :+
;			jmp abort
;		:

;		jmp loop
abort:
		rts

setCharLogo:
		ldx #0
		jsr setSpritesMap
		lda #$01
		sta _dd00
		lda #$80
		sta _d018
		lda #$1b
		sta _lowerD011
		lda #($80^$90)
		sta _d018eor_a
		sta _d018eor_b
		lda #$90
		sta _d018cmp
		lda #24+52-5
		sta _sprxpos
		lda #0
		sta _sprcolor
		jsr setD800ForChars
		lda #5
		jmp waitWithAbort
.endproc

.export drawLogoAtTop
.proc drawLogoAtTop
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<eotb_logo_gfx
		ldx #>eotb_logo_gfx
		jsr decrunchTo
		ldx #6
		jsrf clearTextScreenColors
		memcpy vic_bitmap, (freeRam+eotb_logo_bitmap+5*320), (eotb_logo_bitmap_size-8*320)
		memcpy vic_screen, (freeRam+eotb_logo_screen+5*40), (eotb_logo_screen_size-8*40)
		memcpy $d800, (freeRam+eotb_logo_d800+5*40), (eotb_logo_d800_size-8*40)
		rts
.endproc
