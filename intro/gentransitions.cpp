#include <stdio.h>

void boxMorphTransition(
	int targetDestX, 	int targetDestY,
	int targetFinalX, 	int targetFinalY,
	int targetSrcX, 	int targetSrcY,
	int targetFinalW, 	int targetFinalH,
	int originX1, 		int originY1,
	int originW, 		int originH,
	int sourceW, 		int sourceH,
	bool genclear) {

	int originX2 = originX1 + originW;
	int originY2 = originY1 + originH;
	if (originY2 > 21)
		originY2 = 21;

	int w = 1;
	int h = 1;
	while(true) {
		// _screen->copyRegion(targetSrcX << 3, targetSrcY << 3, targetDestX << 3, targetDestY << 3, w << 3, h << 3, 2, 0, Screen::CR_NO_P_CHECK);

		printf("_begin\n");

		int blitw = (targetSrcX+w)<sourceW ? w : sourceW-targetSrcX;
		int blith = (targetSrcY+h)<sourceH ? h : sourceH-targetSrcY;

 		printf("_bitblt %d,%d, %d,%d, %d,%d ;%d,%d\n", targetSrcX,targetSrcY, targetDestX,targetDestY, blitw, blith, w, h);

		if (w>blitw) {
			printf("_fill %d,%d,%d,%d\n", targetDestX+blitw, targetDestY, w-blitw, h);
		}
		if (h>blith) {
			printf("_fill %d,%d,%d,%d\n", targetDestX, targetDestY+blith, w, h-blith);			
		}

		if (genclear) {
			if (originX1 < targetDestX)
				// _screen->copyRegion(312, 0, originX1 << 3, 0, 8, 176, 0, 0, Screen::CR_NO_P_CHECK);
				if ((originX1>=0) && (originX1<40))
					printf("_vclear %d\n", originX1);
			if (originY1 < targetDestY)
				// _screen->copyRegion(0, 192, 0, originY1 << 3, 320, 8, 0, 0, Screen::CR_NO_P_CHECK);
				if ((originY1>=0) && (originY1<25))
					printf("_hclear %d\n", originY1);
			if ((targetFinalX + targetFinalW) <= originX2)
				// _screen->copyRegion(312, 0, originX2 << 3, 0, 8, 176, 0, 0, Screen::CR_NO_P_CHECK);
				if ((originX2>=0) && (originX2<40))
					printf("_vclear %d\n", originX2);
			if ((targetFinalY + targetFinalH) <= originY2)
				// _screen->copyRegion(0, 192, 0, originY2 << 3, 320, 8, 0, 0, Screen::CR_NO_P_CHECK);
				if ((originY2>=0) && (originY2<25))
					printf("_hclear %d\n", originY2);
		}

		printf("_end\n\n");

		if (!(targetDestX != targetFinalX || targetDestY != targetFinalY || w != targetFinalW || h != targetFinalH || originX1 < targetFinalX || originY1 < targetFinalY || (targetFinalX + targetFinalW) < originX2 || (targetFinalY + targetFinalH) < originY2))
			break;

		int v = targetFinalX - targetDestX;
		v = (v < 0) ? -1 : ((v > 0) ? 1 : 0);
		targetDestX += v;
		v = targetFinalY - targetDestY;
		v = (v < 0) ? -1 : ((v > 0) ? 1 : 0);
		targetDestY += v;

		if (w != targetFinalW)
			w += 2;
		if (w > targetFinalW)
			w = targetFinalW;

		if (h != targetFinalH)
			h += 2;
		if (h > targetFinalH)
			h = targetFinalH;

		if (++originX1 > targetFinalX)
			originX1 = targetFinalX;

		if (++originY1 > targetFinalY)
			originY1 = targetFinalY;

		if ((targetFinalX + targetFinalW) < originX2)
			originX2--;

		if ((targetFinalY + targetFinalH) < originY2)
			originY2--;
	}
}

void throneRoomTransition() {
	int x = 15;
	int y = 14;
	int w = 1;
	int h = 1;
	int ox=-1;
	int oy=-1;
	int ow=-1;
	int oh=-1;
	for (int i=0; i<9; i++) {
		if (x > 6)
			x--;
		if (y > 0)
			y-=2;
		w+=3;
		if (x+w > 34)
			w = 34-x;
		h+=3;
		if (y+h > 23)
			h = 23-y;

		printf("_wait 1\n");
		if (ox==-1)
			printf("_bitblt2 %d,%d, %d,%d, %d,%d\n", x-6,y, x,y, w,h);
		else {
			if (y<oy) {
				printf("_bitblt2 %d,%d, %d,%d, %d,%d\n", x-6,y, x,y, w,oy-y);
			}
			if (x<ox) {
				printf("_bitblt2 %d,%d, %d,%d, %d,%d\n", x-6,y, x,y, ox-x,h);				
			}
			if (x+w > ox+ow) {
				printf("_bitblt2 %d,%d, %d,%d, %d,%d\n", ox+ow-6,y, ox+ow,y, x+w-(ox+ow), h);				
			}
			if (y+h > oy+oh) {
				printf("_bitblt2 %d,%d, %d,%d, %d,%d\n", x-6,oy+oh, x,oy+oh, w, y+h-(oy+oh));								
			}
		}

		printf("\n");

		ox=x;
		oy=y;
		ow=w;
		oh=h;
	}

}

int main(void) {
//	boxMorphTransition(25, 8, 18, 4, 3, 0, 21, 8, 6, 0, 28, 23,   21,9,false);
//	boxMorphTransition(18, 16, 10, 12, 0, 10, 17, 8, 17, 3, 25, 10,   16,16,true);
//	boxMorphTransition(9, 6, 0, 0, 7, 7, 18, 12, 8, 11, 21, 10,  18,17,true);
//	boxMorphTransition(20, 13, 16, 6, 0, 0, 11, 14, 0, 0, 24, 16, 10,20,true);

	throneRoomTransition();
	return 0;
}
