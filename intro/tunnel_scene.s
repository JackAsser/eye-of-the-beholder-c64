;text_template	= $5000
;text_screen0	= $8000
;text_screen1	= $8400
;text_font		= $8800

d800_data				= $3a00

intro_move_01_screen 	= $5400
intro_move_01_04_font 	= $5800
intro_move_02_05_font 	= $6000
intro_move_07_font 		= $6800
intro_move_02_screen 	= $7000
intro_move_04_screen 	= $7400
intro_move_05_screen 	= $7800
intro_move_07_screen 	= $7c00
fade_d800				= $7c00

intro_move_03_screen 	= $8400
intro_move_03_06_font 	= $a000
intro_move_08_font 		= $a800
intro_move_06_screen 	= $b000
intro_move_08_screen 	= $b400

intro_move_09_font 		= $c000
intro_move_10_font 		= $c800
intro_move_11_font 		= $d000
intro_move_12_font 		= $d800
intro_move_09_screen	= $e000
intro_move_10_screen	= $e400
intro_move_11_screen	= $e800
intro_move_12_screen	= $ec00

intro_move_13_screen	= $5400
intro_move_13_font 		= $5800
intro_move_14_font 		= $6000
intro_move_15_font 		= $6800
intro_move_14_screen	= $7000
intro_move_15_screen	= $7400

intro_move_16_screen	= $8400
intro_move_16_font 		= $a000
intro_move_17_font 		= $a800
intro_move_18_font 		= $b000
intro_move_17_screen	= $b800
intro_move_18_screen 	= $bc00

intro_rotation_01_font	= $c000
intro_rotation_02_font	= $c800
intro_rotation_03_font	= $d000
intro_rotation_04_font	= $d800
intro_rotation_01_screen= $e000
intro_rotation_02_screen= $e400
intro_rotation_03_screen= $e800
intro_rotation_04_screen= $ec00

intro_rotation_05_screen= $5400
intro_rotation_05_font	= $5800
intro_rotation_06_font	= $6000
intro_rotation_07_font	= $6800
intro_rotation_06_screen= $7000
intro_rotation_07_screen= $7400

intro_rotation_08_screen= $8400
intro_rotation_08_font	= $a000
intro_rotation_09_font	= $a800
intro_rotation_10_font	= $b000
intro_rotation_09_screen= $b800
intro_rotation_10_screen= $bc00

intro_door_01_screen	= $5400
intro_door_font 		= $5800
intro_door_05_screen	= $6000
intro_door_sprites		= $6400

intro_rockfall_01_screen= $8400
intro_rockfall_01_font	= $a000
intro_rockfall_02_font	= $a800
intro_rockfall_03_font	= $b000
intro_rockfall_02_screen= $b800
intro_rockfall_03_screen= $bc00

intro_rockfall_04_screen= $c000
intro_rockfall_05_screen= $c400
intro_rockfall_06_screen= $c800
intro_rockfall_04_font	= $d000
intro_rockfall_05_06_font=$d800

sphere_frame_0				= $c000 ;-$c340)
intro_rockfall_f0_f1_screen = intro_rockfall_06_screen
.export intro_rockfall_f0_f1_screen
intro_rockfall_f0_f1_font	= intro_rockfall_05_06_font
.export intro_rockfall_f0_f1_font
sphere_frame_1				= $e000 ;-$e200)

intro_rockfall_f2_screen 	= $8400
.export intro_rockfall_f2_screen
intro_rockfall_f2_font		= $a000
.export intro_rockfall_f2_font
sphere_frame_2				= $a800 ;-$b9c0)

intro_rockfall_f3_screen 	= $5400
.export intro_rockfall_f3_screen
intro_rockfall_f3_font		= $5800
.export intro_rockfall_f3_font
sphere_frame_3				= $6000 ;-$7100)

.include "../global.inc"
.include "bitblt.inc"

.segment "INTRO_TUNNEL_MOVE_01_04"
intro_tunnel_x1_move_01_04_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_01_04.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_01_04.inc"
.segment "INTRO_TUNNEL_MOVE_02_05"
intro_tunnel_x1_move_02_05_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_02_05.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_02_05.inc"
.segment "INTRO_TUNNEL_MOVE_03_06"
intro_tunnel_x1_move_03_06_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_03_06.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_03_06.inc"
.segment "INTRO_TUNNEL_MOVE_07"
intro_tunnel_x1_move_07_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_07.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_07.inc"
.segment "INTRO_TUNNEL_MOVE_08"
intro_tunnel_x1_move_08_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_08.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_08.inc"
.segment "INTRO_TUNNEL_MOVE_09"
intro_tunnel_x1_move_09_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_09.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_09.inc"
.segment "INTRO_TUNNEL_MOVE_10"
intro_tunnel_x1_move_10_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_10.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_10.inc"
.segment "INTRO_TUNNEL_MOVE_11"
intro_tunnel_x1_move_11_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_11.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_11.inc"
.segment "INTRO_TUNNEL_MOVE_12"
intro_tunnel_x1_move_12_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_12.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_12.inc"
.segment "INTRO_TUNNEL_MOVE_13"
intro_tunnel_x1_move_13_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_13.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_13.inc"
.segment "INTRO_TUNNEL_MOVE_14"
intro_tunnel_x1_move_14_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_14.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_14.inc"
.segment "INTRO_TUNNEL_MOVE_15"
intro_tunnel_x1_move_15_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_15.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_15.inc"
.segment "INTRO_TUNNEL_MOVE_16"
intro_tunnel_x1_move_16_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_16.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_16.inc"
.segment "INTRO_TUNNEL_MOVE_17"
intro_tunnel_x1_move_17_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_17.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_17.inc"
.segment "INTRO_TUNNEL_MOVE_18"
intro_tunnel_x1_move_18_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_18.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_18.inc"
.segment "INTRO_TUNNEL_MOVE_D800"
intro_tunnel_x1_move_d800_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x1_move_d800.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x1_move_d800.inc"

.segment "INTRO_TUNNEL_ROTATION_01"
intro_tunnel_x2_rotation_01_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_01.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_01.inc"
.segment "INTRO_TUNNEL_ROTATION_02"
intro_tunnel_x2_rotation_02_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_02.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_02.inc"
.segment "INTRO_TUNNEL_ROTATION_03"
intro_tunnel_x2_rotation_03_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_03.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_03.inc"
.segment "INTRO_TUNNEL_ROTATION_04"
intro_tunnel_x2_rotation_04_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_04.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_04.inc"
.segment "INTRO_TUNNEL_ROTATION_05"
intro_tunnel_x2_rotation_05_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_05.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_05.inc"
.segment "INTRO_TUNNEL_ROTATION_06"
intro_tunnel_x2_rotation_06_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_06.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_06.inc"
.segment "INTRO_TUNNEL_ROTATION_07"
intro_tunnel_x2_rotation_07_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_07.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_07.inc"
.segment "INTRO_TUNNEL_ROTATION_08"
intro_tunnel_x2_rotation_08_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_08.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_08.inc"
.segment "INTRO_TUNNEL_ROTATION_09"
intro_tunnel_x2_rotation_09_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_09.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_09.inc"
.segment "INTRO_TUNNEL_ROTATION_10"
intro_tunnel_x2_rotation_10_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_10.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_10.inc"
.segment "INTRO_TUNNEL_ROTATION_D800"
intro_tunnel_x2_rotation_d800_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x2_rotation_d800.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x2_rotation_d800.inc"

.segment "INTRO_TUNNEL_DOOR"
intro_tunnel_x3_door_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x3_door.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x3_door.inc"

.segment "INTRO_TUNNEL_ROCKFALL_01"
intro_tunnel_x4_rockfall_01_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x4_rockfall_01.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x4_rockfall_01.inc"
.segment "INTRO_TUNNEL_ROCKFALL_02"
intro_tunnel_x4_rockfall_02_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x4_rockfall_02.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x4_rockfall_02.inc"
.segment "INTRO_TUNNEL_ROCKFALL_03"
intro_tunnel_x4_rockfall_03_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x4_rockfall_03.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x4_rockfall_03.inc"
.segment "INTRO_TUNNEL_ROCKFALL_04"
intro_tunnel_x4_rockfall_04_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x4_rockfall_04.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x4_rockfall_04.inc"
.segment "INTRO_TUNNEL_ROCKFALL_05_06"
intro_tunnel_x4_rockfall_05_06_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x4_rockfall_05_06.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x4_rockfall_05_06.inc"
.segment "INTRO_TUNNEL_ROCKFALL_D800"
intro_tunnel_x4_rockfall_d800_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_x4_rockfall_d800.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_x4_rockfall_d800.inc"

.segment "INTRO_TUNNEL_SPRITES"
intro_tunnel_sprites_gfx:
		.incbin  "../converters/intro/compressed/intro_tunnel_sprites.prg.b2",2
		.include "../converters/intro/compressed/intro_tunnel_sprites.inc"

.segment "INTRO_RAM"
d018tab:
	CALC_D018 intro_move_01_04_font, intro_move_01_screen
	CALC_D018 intro_move_02_05_font, intro_move_02_screen
	CALC_D018 intro_move_03_06_font, intro_move_03_screen
	CALC_D018 intro_move_01_04_font, intro_move_04_screen
	CALC_D018 intro_move_02_05_font, intro_move_05_screen
	CALC_D018 intro_move_03_06_font, intro_move_06_screen
	CALC_D018 intro_move_07_font, intro_move_07_screen
	CALC_D018 intro_move_08_font, intro_move_08_screen
	CALC_D018 intro_move_09_font, intro_move_09_screen
	CALC_D018 intro_move_10_font, intro_move_10_screen
	CALC_D018 intro_move_11_font, intro_move_11_screen
	CALC_D018 intro_move_12_font, intro_move_12_screen
	CALC_D018 intro_move_13_font, intro_move_13_screen
	CALC_D018 intro_move_14_font, intro_move_14_screen
	CALC_D018 intro_move_15_font, intro_move_15_screen
	CALC_D018 intro_move_16_font, intro_move_16_screen
	CALC_D018 intro_move_17_font, intro_move_17_screen
	CALC_D018 intro_move_18_font, intro_move_18_screen
	CALC_D018 intro_rotation_01_font, intro_rotation_01_screen
	CALC_D018 intro_rotation_02_font, intro_rotation_02_screen
	CALC_D018 intro_rotation_03_font, intro_rotation_03_screen
	CALC_D018 intro_rotation_04_font, intro_rotation_04_screen
	CALC_D018 intro_rotation_05_font, intro_rotation_05_screen
	CALC_D018 intro_rotation_06_font, intro_rotation_06_screen
	CALC_D018 intro_rotation_07_font, intro_rotation_07_screen
	CALC_D018 intro_rotation_08_font, intro_rotation_08_screen
	CALC_D018 intro_rotation_09_font, intro_rotation_09_screen
	CALC_D018 intro_rotation_10_font, intro_rotation_10_screen
	CALC_D018 intro_door_font, intro_door_01_screen
	CALC_D018 intro_door_font, intro_door_05_screen
	CALC_D018 intro_rockfall_01_font, intro_rockfall_01_screen
	CALC_D018 intro_rockfall_02_font, intro_rockfall_02_screen
	CALC_D018 intro_rockfall_03_font, intro_rockfall_03_screen
	CALC_D018 intro_rockfall_04_font, intro_rockfall_04_screen
	CALC_D018 intro_rockfall_05_06_font, intro_rockfall_05_screen
	CALC_D018 intro_rockfall_05_06_font, intro_rockfall_06_screen


dd00tab:
	CALC_DD00 intro_move_01_04_font
	CALC_DD00 intro_move_02_05_font
	CALC_DD00 intro_move_03_06_font
	CALC_DD00 intro_move_01_04_font
	CALC_DD00 intro_move_02_05_font
	CALC_DD00 intro_move_03_06_font
	CALC_DD00 intro_move_07_font
	CALC_DD00 intro_move_08_font
	CALC_DD00 intro_move_09_font
	CALC_DD00 intro_move_10_font
	CALC_DD00 intro_move_11_font
	CALC_DD00 intro_move_12_font
	CALC_DD00 intro_move_13_font
	CALC_DD00 intro_move_14_font
	CALC_DD00 intro_move_15_font
	CALC_DD00 intro_move_16_font
	CALC_DD00 intro_move_17_font
	CALC_DD00 intro_move_18_font
	CALC_DD00 intro_rotation_01_font
	CALC_DD00 intro_rotation_02_font
	CALC_DD00 intro_rotation_03_font
	CALC_DD00 intro_rotation_04_font
	CALC_DD00 intro_rotation_05_font
	CALC_DD00 intro_rotation_06_font
	CALC_DD00 intro_rotation_07_font
	CALC_DD00 intro_rotation_08_font
	CALC_DD00 intro_rotation_09_font
	CALC_DD00 intro_rotation_10_font
	CALC_DD00 intro_door_font
	CALC_DD00 intro_door_font
	CALC_DD00 intro_rockfall_01_font
	CALC_DD00 intro_rockfall_02_font
	CALC_DD00 intro_rockfall_03_font
	CALC_DD00 intro_rockfall_04_font
	CALC_DD00 intro_rockfall_05_06_font
	CALC_DD00 intro_rockfall_05_06_font

.macro blitTunnelFrame dst, src
		lda #<(dst+4*40+10)
		sta DST+0
		lda #>(dst+4*40+10)
		sta DST+1
		lda #<(src)
		sta SRC+0
		lda #>(src)
		sta SRC+1
		jsr blitTunnelFrameHelper
.endmacro

startAnimation: .byte 0
endAnimationFrame: .byte 18
pause: .byte 0
_d015: .byte 0
shake: .byte 0

.proc screenMode
	.word init
	.word teardown
	.byte 3
	.word $2e, top
	.word $d8, preBottom
	.word $e8, bottom

	.proc top
		lda #$ff
		sta $ffff
		lda #3
		ldx shake
		beq :+
			jsr rnd
			and #7
		:
		ora #$18
		sta $d011
		lda #$18
		sta $d016
		
		lda startAnimation
		beq :++
			dec pause
			bpl :++
				ldx system
				lda system_pause,x
				sta pause
				inc animationFrame
				ldx animationFrame
				cpx endAnimationFrame
				bne :+
					lda #0
					sta startAnimation
					dec animationFrame
					jmp :++
				:
				jsr setScreen
				lda _d015
				sta $d015
				jsrf copyD800
				jmp exitIrq_quick
		:

		ldx animationFrame
		jsr setScreen
		jmp exitIrq_quick

		setScreen:
			lda $dd00
			and #$fc
			ora dd00tab,x
			sta $dd00
			lda d018tab,x
			sta $d018
			lda #$40
			:cmp $d012
			bne :-
			lda #$9
			sta $d021
			lda #>irqHandler
			sta $ffff
			rts

		system_pause: .byte 6,7,7,6
	.endproc

	.proc preBottom
		lda $d021
		and #$f
		beq :+
			lda #0
			sta $d021
		:
		jmp exitIrq_quick
	.endproc

	.proc bottom
		lda $dd00
		and #$fc
		ora #$01
		sta $dd00
		lda #$02
		sta $d018
		lda #$08
		sta $d016
		jmp exitIrq
	.endproc

	.proc init
		lda #0
		sta animationFrame
		sta $d020
		sta $d015
		sta $d022
		lda #$c
		sta $d023
		rts
	.endproc

	.proc teardown
		lda #0
		sta $d011
		sta $d015
		lda $dd00
		and #$fc
		sta $dd00
		rts
	.endproc

.endproc

.segment "INTRO10"
.proc copyD800
		lda DST+0
		pha
		lda DST+1
		pha
		lda SRC+0
		pha
		lda SRC+1
		pha
		lda #<($d800+4*40+10)
		sta DST+0
		lda #>($d800+4*40+10)
		sta DST+1
		lda d800_offsets_lo,x
		sta SRC+0
		lda d800_offsets_hi,x
		sta SRC+1
		jsr blitTunnelFrameHelper
		pla
		sta SRC+1
		pla
		sta SRC+0
		pla
		sta DST+1
		pla
		sta DST+0
		rts

d800_offsets_lo:
		.byte <(d800_data+intro_tunnel_x1_move_01_04_d800)
		.byte <(d800_data+intro_tunnel_x1_move_02_05_d800)
		.byte <(d800_data+intro_tunnel_x1_move_03_06_d800)
		.byte <(d800_data+intro_tunnel_x1_move_01_04_d800+300)
		.byte <(d800_data+intro_tunnel_x1_move_02_05_d800+300)
		.byte <(d800_data+intro_tunnel_x1_move_03_06_d800+300)
		.byte <(d800_data+intro_tunnel_x1_move_07_d800)
		.byte <(d800_data+intro_tunnel_x1_move_08_d800)
		.byte <(d800_data+intro_tunnel_x1_move_09_d800)
		.byte <(d800_data+intro_tunnel_x1_move_10_d800)
		.byte <(d800_data+intro_tunnel_x1_move_11_d800)
		.byte <(d800_data+intro_tunnel_x1_move_12_d800)
		.byte <(d800_data+intro_tunnel_x1_move_13_d800)
		.byte <(d800_data+intro_tunnel_x1_move_14_d800)
		.byte <(d800_data+intro_tunnel_x1_move_15_d800)
		.byte <(d800_data+intro_tunnel_x1_move_16_d800)
		.byte <(d800_data+intro_tunnel_x1_move_17_d800)
		.byte <(d800_data+intro_tunnel_x1_move_18_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_01_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_02_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_03_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_04_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_05_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_06_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_07_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_08_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_09_d800)
		.byte <(d800_data+intro_tunnel_x2_rotation_10_d800)
		.byte <(d800_data+intro_tunnel_x3_door_d800)
		.byte <(d800_data+intro_tunnel_x3_door_d800+300)
		.byte <(d800_data+intro_tunnel_x4_rockfall_01_d800)
		.byte <(d800_data+intro_tunnel_x4_rockfall_02_d800)
		.byte <(d800_data+intro_tunnel_x4_rockfall_03_d800)
		.byte <(d800_data+intro_tunnel_x4_rockfall_04_d800)
		.byte <(d800_data+intro_tunnel_x4_rockfall_05_06_d800)
		.byte <(d800_data+intro_tunnel_x4_rockfall_05_06_d800+300)

d800_offsets_hi:
		.byte >(d800_data+intro_tunnel_x1_move_01_04_d800)
		.byte >(d800_data+intro_tunnel_x1_move_02_05_d800)
		.byte >(d800_data+intro_tunnel_x1_move_03_06_d800)
		.byte >(d800_data+intro_tunnel_x1_move_01_04_d800+300)
		.byte >(d800_data+intro_tunnel_x1_move_02_05_d800+300)
		.byte >(d800_data+intro_tunnel_x1_move_03_06_d800+300)
		.byte >(d800_data+intro_tunnel_x1_move_07_d800)
		.byte >(d800_data+intro_tunnel_x1_move_08_d800)
		.byte >(d800_data+intro_tunnel_x1_move_09_d800)
		.byte >(d800_data+intro_tunnel_x1_move_10_d800)
		.byte >(d800_data+intro_tunnel_x1_move_11_d800)
		.byte >(d800_data+intro_tunnel_x1_move_12_d800)
		.byte >(d800_data+intro_tunnel_x1_move_13_d800)
		.byte >(d800_data+intro_tunnel_x1_move_14_d800)
		.byte >(d800_data+intro_tunnel_x1_move_15_d800)
		.byte >(d800_data+intro_tunnel_x1_move_16_d800)
		.byte >(d800_data+intro_tunnel_x1_move_17_d800)
		.byte >(d800_data+intro_tunnel_x1_move_18_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_01_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_02_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_03_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_04_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_05_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_06_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_07_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_08_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_09_d800)
		.byte >(d800_data+intro_tunnel_x2_rotation_10_d800)
		.byte >(d800_data+intro_tunnel_x3_door_d800)
		.byte >(d800_data+intro_tunnel_x3_door_d800+300)
		.byte >(d800_data+intro_tunnel_x4_rockfall_01_d800)
		.byte >(d800_data+intro_tunnel_x4_rockfall_02_d800)
		.byte >(d800_data+intro_tunnel_x4_rockfall_03_d800)
		.byte >(d800_data+intro_tunnel_x4_rockfall_04_d800)
		.byte >(d800_data+intro_tunnel_x4_rockfall_05_06_d800)
		.byte >(d800_data+intro_tunnel_x4_rockfall_05_06_d800+300)
.endproc

.proc decompressNext
		.pushseg
		.segment "INTRO_RAM"
		chunk: .byte 0
		.popseg

		ldx chunk
		lda compressedBank,x
		pha
		ldy compressedAddrLo,x
		lda compressedAddrHi,x
		tax
		pla
		jsr decrunchFarToFreeRam

		ldx chunk
		lda #<freeRam
		sta MSRC+0
		lda #>freeRam
		sta MSRC+1
		lda #0
		sta MDST+0
		lda destinationFont,x
		sta MDST+1
		and #$f0
		tay
		lda #<$800
		ldx #>$800
		cpy #>$d000
		beq :+
			jsr _memcpy_ram
			jmp :++
		:
			jsr _memcpy_ram_underio
		:

		lda chunk
		asl
		tax
		lda destinationScreen,x
		jsr clearScreen
		lda chunk
		asl
		tax
		lda destinationScreen+1,x
		beq :+
			jsr clearScreen
		:

		lda chunk
		asl
		tax
		ldy #<0
		sty SRC+0
		ldy #>0
		sty SRC+1
		lda destinationScreen,x
		jsr blitScreen
		lda chunk
		asl
		tax
		lda destinationScreen+1,x
		beq :+
			ldy #<300
			sty SRC+0
			ldy #>300
			sty SRC+1
			jsr blitScreen
		:

		ldx chunk
		inx
		stx chunk

		lda KEYEVENT+KeyEvent::keyPressed
		bne :+
		lda MOUSEEVENT+MouseEvent::buttons
		bpl :+
		lda #0
		:
		rts

blitScreen:
		ldx chunk
		adc #>(4*40+10)
		sta DST+1
		lda #<(4*40+10)
		sta DST+0
		lda SRC+0
		adc compressedScreenOffsetLo,x
		sta SRC+0
		lda SRC+1
		adc compressedScreenOffsetHi,x
		sta SRC+1
		jmp blitTunnelFrameHelper

clearScreen:
		sta MDST+1
		lda #0
		sta MDST+0
		lda #<1000
		ldx #>1000
		ldy #0
		jmp _memset

compressedBank:
		.byte <.bank(intro_tunnel_x1_move_01_04_gfx)
		.byte <.bank(intro_tunnel_x1_move_02_05_gfx)
		.byte <.bank(intro_tunnel_x1_move_03_06_gfx)
		.byte <.bank(intro_tunnel_x1_move_07_gfx)
		.byte <.bank(intro_tunnel_x1_move_08_gfx)
		.byte <.bank(intro_tunnel_x1_move_09_gfx)
		.byte <.bank(intro_tunnel_x1_move_10_gfx)
		.byte <.bank(intro_tunnel_x1_move_11_gfx)
		.byte <.bank(intro_tunnel_x1_move_12_gfx)
		.byte <.bank(intro_tunnel_x1_move_13_gfx)
		.byte <.bank(intro_tunnel_x1_move_14_gfx)
		.byte <.bank(intro_tunnel_x1_move_15_gfx)
		.byte <.bank(intro_tunnel_x1_move_16_gfx)
		.byte <.bank(intro_tunnel_x1_move_17_gfx)
		.byte <.bank(intro_tunnel_x1_move_18_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_01_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_02_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_03_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_04_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_05_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_06_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_07_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_08_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_09_gfx)
		.byte <.bank(intro_tunnel_x2_rotation_10_gfx)
		.byte <.bank(intro_tunnel_x3_door_gfx)
		.byte <.bank(intro_tunnel_x4_rockfall_01_gfx)
		.byte <.bank(intro_tunnel_x4_rockfall_02_gfx)
		.byte <.bank(intro_tunnel_x4_rockfall_03_gfx)
		.byte <.bank(intro_tunnel_x4_rockfall_04_gfx)
		.byte <.bank(intro_tunnel_x4_rockfall_05_06_gfx)

compressedAddrLo:
		.byte <intro_tunnel_x1_move_01_04_gfx
		.byte <intro_tunnel_x1_move_02_05_gfx
		.byte <intro_tunnel_x1_move_03_06_gfx
		.byte <intro_tunnel_x1_move_07_gfx
		.byte <intro_tunnel_x1_move_08_gfx
		.byte <intro_tunnel_x1_move_09_gfx
		.byte <intro_tunnel_x1_move_10_gfx
		.byte <intro_tunnel_x1_move_11_gfx
		.byte <intro_tunnel_x1_move_12_gfx
		.byte <intro_tunnel_x1_move_13_gfx
		.byte <intro_tunnel_x1_move_14_gfx
		.byte <intro_tunnel_x1_move_15_gfx
		.byte <intro_tunnel_x1_move_16_gfx
		.byte <intro_tunnel_x1_move_17_gfx
		.byte <intro_tunnel_x1_move_18_gfx
		.byte <intro_tunnel_x2_rotation_01_gfx
		.byte <intro_tunnel_x2_rotation_02_gfx
		.byte <intro_tunnel_x2_rotation_03_gfx
		.byte <intro_tunnel_x2_rotation_04_gfx
		.byte <intro_tunnel_x2_rotation_05_gfx
		.byte <intro_tunnel_x2_rotation_06_gfx
		.byte <intro_tunnel_x2_rotation_07_gfx
		.byte <intro_tunnel_x2_rotation_08_gfx
		.byte <intro_tunnel_x2_rotation_09_gfx
		.byte <intro_tunnel_x2_rotation_10_gfx
		.byte <intro_tunnel_x3_door_gfx
		.byte <intro_tunnel_x4_rockfall_01_gfx
		.byte <intro_tunnel_x4_rockfall_02_gfx
		.byte <intro_tunnel_x4_rockfall_03_gfx
		.byte <intro_tunnel_x4_rockfall_04_gfx
		.byte <intro_tunnel_x4_rockfall_05_06_gfx

compressedAddrHi:
		.byte >intro_tunnel_x1_move_01_04_gfx
		.byte >intro_tunnel_x1_move_02_05_gfx
		.byte >intro_tunnel_x1_move_03_06_gfx
		.byte >intro_tunnel_x1_move_07_gfx
		.byte >intro_tunnel_x1_move_08_gfx
		.byte >intro_tunnel_x1_move_09_gfx
		.byte >intro_tunnel_x1_move_10_gfx
		.byte >intro_tunnel_x1_move_11_gfx
		.byte >intro_tunnel_x1_move_12_gfx
		.byte >intro_tunnel_x1_move_13_gfx
		.byte >intro_tunnel_x1_move_14_gfx
		.byte >intro_tunnel_x1_move_15_gfx
		.byte >intro_tunnel_x1_move_16_gfx
		.byte >intro_tunnel_x1_move_17_gfx
		.byte >intro_tunnel_x1_move_18_gfx
		.byte >intro_tunnel_x2_rotation_01_gfx
		.byte >intro_tunnel_x2_rotation_02_gfx
		.byte >intro_tunnel_x2_rotation_03_gfx
		.byte >intro_tunnel_x2_rotation_04_gfx
		.byte >intro_tunnel_x2_rotation_05_gfx
		.byte >intro_tunnel_x2_rotation_06_gfx
		.byte >intro_tunnel_x2_rotation_07_gfx
		.byte >intro_tunnel_x2_rotation_08_gfx
		.byte >intro_tunnel_x2_rotation_09_gfx
		.byte >intro_tunnel_x2_rotation_10_gfx
		.byte >intro_tunnel_x3_door_gfx
		.byte >intro_tunnel_x4_rockfall_01_gfx
		.byte >intro_tunnel_x4_rockfall_02_gfx
		.byte >intro_tunnel_x4_rockfall_03_gfx
		.byte >intro_tunnel_x4_rockfall_04_gfx
		.byte >intro_tunnel_x4_rockfall_05_06_gfx

compressedScreenOffsetLo:
		.byte <(freeRam+intro_tunnel_x1_move_01_04_screen)
		.byte <(freeRam+intro_tunnel_x1_move_02_05_screen)
		.byte <(freeRam+intro_tunnel_x1_move_03_06_screen)
		.byte <(freeRam+intro_tunnel_x1_move_07_screen)
		.byte <(freeRam+intro_tunnel_x1_move_08_screen)
		.byte <(freeRam+intro_tunnel_x1_move_09_screen)
		.byte <(freeRam+intro_tunnel_x1_move_10_screen)
		.byte <(freeRam+intro_tunnel_x1_move_11_screen)
		.byte <(freeRam+intro_tunnel_x1_move_12_screen)
		.byte <(freeRam+intro_tunnel_x1_move_13_screen)
		.byte <(freeRam+intro_tunnel_x1_move_14_screen)
		.byte <(freeRam+intro_tunnel_x1_move_15_screen)
		.byte <(freeRam+intro_tunnel_x1_move_16_screen)
		.byte <(freeRam+intro_tunnel_x1_move_17_screen)
		.byte <(freeRam+intro_tunnel_x1_move_18_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_01_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_02_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_03_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_04_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_05_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_06_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_07_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_08_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_09_screen)
		.byte <(freeRam+intro_tunnel_x2_rotation_10_screen)
		.byte <(freeRam+intro_tunnel_x3_door_screen)
		.byte <(freeRam+intro_tunnel_x4_rockfall_01_screen)
		.byte <(freeRam+intro_tunnel_x4_rockfall_02_screen)
		.byte <(freeRam+intro_tunnel_x4_rockfall_03_screen)
		.byte <(freeRam+intro_tunnel_x4_rockfall_04_screen)
		.byte <(freeRam+intro_tunnel_x4_rockfall_05_06_screen)

compressedScreenOffsetHi:
		.byte >(freeRam+intro_tunnel_x1_move_01_04_screen)
		.byte >(freeRam+intro_tunnel_x1_move_02_05_screen)
		.byte >(freeRam+intro_tunnel_x1_move_03_06_screen)
		.byte >(freeRam+intro_tunnel_x1_move_07_screen)
		.byte >(freeRam+intro_tunnel_x1_move_08_screen)
		.byte >(freeRam+intro_tunnel_x1_move_09_screen)
		.byte >(freeRam+intro_tunnel_x1_move_10_screen)
		.byte >(freeRam+intro_tunnel_x1_move_11_screen)
		.byte >(freeRam+intro_tunnel_x1_move_12_screen)
		.byte >(freeRam+intro_tunnel_x1_move_13_screen)
		.byte >(freeRam+intro_tunnel_x1_move_14_screen)
		.byte >(freeRam+intro_tunnel_x1_move_15_screen)
		.byte >(freeRam+intro_tunnel_x1_move_16_screen)
		.byte >(freeRam+intro_tunnel_x1_move_17_screen)
		.byte >(freeRam+intro_tunnel_x1_move_18_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_01_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_02_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_03_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_04_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_05_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_06_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_07_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_08_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_09_screen)
		.byte >(freeRam+intro_tunnel_x2_rotation_10_screen)
		.byte >(freeRam+intro_tunnel_x3_door_screen)
		.byte >(freeRam+intro_tunnel_x4_rockfall_01_screen)
		.byte >(freeRam+intro_tunnel_x4_rockfall_02_screen)
		.byte >(freeRam+intro_tunnel_x4_rockfall_03_screen)
		.byte >(freeRam+intro_tunnel_x4_rockfall_04_screen)
		.byte >(freeRam+intro_tunnel_x4_rockfall_05_06_screen)

destinationFont:
		.byte >intro_move_01_04_font
		.byte >intro_move_02_05_font
		.byte >intro_move_03_06_font
		.byte >intro_move_07_font
		.byte >intro_move_08_font
		.byte >intro_move_09_font
		.byte >intro_move_10_font
		.byte >intro_move_11_font
		.byte >intro_move_12_font
		.byte >intro_move_13_font
		.byte >intro_move_14_font
		.byte >intro_move_15_font
		.byte >intro_move_16_font
		.byte >intro_move_17_font
		.byte >intro_move_18_font
		.byte >intro_rotation_01_font
		.byte >intro_rotation_02_font
		.byte >intro_rotation_03_font
		.byte >intro_rotation_04_font
		.byte >intro_rotation_05_font
		.byte >intro_rotation_06_font
		.byte >intro_rotation_07_font
		.byte >intro_rotation_08_font
		.byte >intro_rotation_09_font
		.byte >intro_rotation_10_font
		.byte >intro_door_font
		.byte >intro_rockfall_01_font
		.byte >intro_rockfall_02_font
		.byte >intro_rockfall_03_font
		.byte >intro_rockfall_04_font
		.byte >intro_rockfall_05_06_font

destinationScreen:
		.byte >intro_move_01_screen, >intro_move_04_screen
		.byte >intro_move_02_screen, >intro_move_05_screen
		.byte >intro_move_03_screen, >intro_move_06_screen
		.byte >intro_move_07_screen, 0
		.byte >intro_move_08_screen, 0
		.byte >intro_move_09_screen, 0
		.byte >intro_move_10_screen, 0
		.byte >intro_move_11_screen, 0
		.byte >intro_move_12_screen, 0
		.byte >intro_move_13_screen, 0
		.byte >intro_move_14_screen, 0
		.byte >intro_move_15_screen, 0
		.byte >intro_move_16_screen, 0
		.byte >intro_move_17_screen, 0
		.byte >intro_move_18_screen, 0
		.byte >intro_rotation_01_screen, 0
		.byte >intro_rotation_02_screen, 0
		.byte >intro_rotation_03_screen, 0
		.byte >intro_rotation_04_screen, 0
		.byte >intro_rotation_05_screen, 0
		.byte >intro_rotation_06_screen, 0
		.byte >intro_rotation_07_screen, 0
		.byte >intro_rotation_08_screen, 0
		.byte >intro_rotation_09_screen, 0
		.byte >intro_rotation_10_screen, 0
		.byte >intro_door_01_screen, >intro_door_05_screen
		.byte >intro_rockfall_01_screen, 0
		.byte >intro_rockfall_02_screen, 0
		.byte >intro_rockfall_03_screen, 0
		.byte >intro_rockfall_04_screen, 0
		.byte >intro_rockfall_05_screen, >intro_rockfall_06_screen
.endproc

.export tunnel_scene
.proc tunnel_scene
		memset $d800, 0, 1000
		lda #<d800_data
		sta BB_DST+0
		lda #>d800_data
		sta BB_DST+1
		lda #<.bank(intro_tunnel_x1_move_d800_gfx)
		ldy #<intro_tunnel_x1_move_d800_gfx
		ldx #>intro_tunnel_x1_move_d800_gfx
		jsr decrunchFar
		ldx #0
		jsr copyD800

		jsr decompressNext

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

		:
			jsr decompressNext
			beq :+
				jmp abort
			:
			cpx #8
		bne :--

		inc startAnimation

		:
			jsr decompressNext
			beq :+
				jmp abort
			:
			cpx #15
		bne :--

		:
			lda #1
			jsr waitWithAbort
			beq :+
				jmp abort
			:
			lda startAnimation
		bne :--

		lda #2
		ldx #10
		ldy #1
		jsr showTextExt

		lda #<d800_data
		sta BB_DST+0
		lda #>d800_data
		sta BB_DST+1
		lda #<.bank(intro_tunnel_x2_rotation_d800_gfx)
		ldy #<intro_tunnel_x2_rotation_d800_gfx
		ldx #>intro_tunnel_x2_rotation_d800_gfx
		jsr decrunchFar

		:
			jsr decompressNext
			beq :+
				jmp abort
			:
			cpx #22
		bne :--

		lda #28
		sta endAnimationFrame
		inc startAnimation

		:
			jsr decompressNext
			beq :+
				jmp abort
			:
			cpx #26
		bne :--

		lda #<intro_door_sprites
		sta BB_DST+0
		lda #>intro_door_sprites
		sta BB_DST+1
		lda #<.bank(intro_tunnel_sprites_gfx)
		ldy #<intro_tunnel_sprites_gfx
		ldx #>intro_tunnel_sprites_gfx
		jsr decrunchFar

		:
			lda #1
			jsr waitWithAbort
			beq :+
				jmp abort
			:
			lda startAnimation
		bne :--

		lda #29
		sta endAnimationFrame
		lda #0
		sta pause
		inc startAnimation

		lda #$0f
		sta _d015
		lda #$03
		sta $d01c
		lda #0
		sta $d010
		sta $d027
		sta $d028
		sta $d029
		sta $d02a
		sta $d01b
		sta $d01d
		sta $d017
		lda #$9
		sta $d025
		lda #$c
		sta $d026
		clc
		lda #64+10*8+24
		sta $d000
		sta $d004
		adc #24
		sta $d002
		sta $d006
		lda #32+4*8+50
		sta $d001
		sta $d003
		adc #21
		sta $d005
		sta $d007

		ldy #0
		doorLoop:
			ldx #0
			:
				lda spriteIndices,y
				sta intro_door_01_screen+$3f8,x
				iny
				inx
				cpx #4
			bne :-

			:lda startAnimation
			bne :-

			lda pause
			jsr waitWithAbort
			beq :+
				jmp abort
			:

			cpy #4*4
		bne doorLoop

		lda #30
		sta endAnimationFrame
		lda #0
		sta _d015
		sta pause
		inc startAnimation

		ldx #159
		:
			sta $d800+21*40,x
			dex
		bne :-

		lda #50
		jsr waitWithAbort
		beq :+
			jmp abort
		:

		lda #$ff
		sta $7fff
		sta $bfff
		inc shake

		:
			jsr decompressNext
			beq :+
				jmp abort
			:
			cpx #31
		bne :--

		lda #<d800_data
		sta BB_DST+0
		lda #>d800_data
		sta BB_DST+1
		lda #<.bank(intro_tunnel_x4_rockfall_d800_gfx)
		ldy #<intro_tunnel_x4_rockfall_d800_gfx
		ldx #>intro_tunnel_x4_rockfall_d800_gfx
		jsr decrunchFar

		lda #36
		sta endAnimationFrame
		inc startAnimation

		:
			lda #1
			jsr waitWithAbort
			beq :+
				jmp abort
			:
			lda startAnimation
		bne :--

		dec shake

		; Switch over to sphere
		;
		;	- Decrunch sphere
		jsr decrunchSphere
		;	- Copy final frame's font and screen
		memcpy intro_rockfall_f2_screen, intro_rockfall_f0_f1_screen, 1000
		memcpy intro_rockfall_f3_screen, intro_rockfall_f0_f1_screen, 1000
		memcpy_pureram intro_rockfall_f2_font, intro_rockfall_f0_f1_font, 2048
		memcpy_pureram intro_rockfall_f3_font, intro_rockfall_f0_f1_font, 2048
		;	- Fix last frame with gray at top and bottom
		ldx #4
		:
			lda #1
			sta intro_rockfall_f3_screen+3*40+17,x
			sta intro_rockfall_f3_screen+19*40+17,x
			lda #$8
			sta $d800+3*40+17,x
			sta $d800+19*40+17,x
			dex
		bpl :-

		;	- Place the sphere sprites
		jsrf setupSphereSprites

		;	- Switch to sphere multiplexer at frame 10
		sec
		ldx #<screenModeSphere
		ldy #>screenModeSphere
		jsr setScreenMode
		lda #10
		sta animationFrame ;Tunnel sphere tables starts at frame 10 in sphere_scene

		;	- Animate it
		sphereLoop:
			lda pause
			jsr waitWithAbort
			beq :+
				jmp abort
			:
			ldx animationFrame
			inx
			stx animationFrame
			cpx #10+3
		bne sphereLoop

		lda #2
		ldx #11
		ldy #1
		jsr showTextExt

		lda #150
		jsr waitWithAbort

		lda #0
		ldx #11
		ldy #1
		jsr showTextExt

		jsrf fadeOut
		bcs abort
		:
			lda #5
			jsr waitWithAbort
			bne abort
			dec musicVolume
		bpl :-
		
		abort:
		rts

spriteIndices:
		.byte <(intro_door_sprites/$40+ 0), <(intro_door_sprites/$40+ 1)
		.byte <(intro_door_sprites/$40+ 8), <(intro_door_sprites/$40+ 9)

		.byte <(intro_door_sprites/$40+ 2), <(intro_door_sprites/$40+ 3)
		.byte <(intro_door_sprites/$40+10), <(intro_door_sprites/$40+11)

		.byte <(intro_door_sprites/$40+ 4), <(intro_door_sprites/$40+ 5)
		.byte <(intro_door_sprites/$40+12), <(intro_door_sprites/$40+13)

		.byte <(intro_door_sprites/$40+ 6), <(intro_door_sprites/$40+ 7)
		.byte <(intro_door_sprites/$40+14), <(intro_door_sprites/$40+15)
.endproc

.proc decrunchSphere
		lda #<sphere_frame_0
		sta BB_DST+0
		lda #>sphere_frame_0
		sta BB_DST+1
		lda #<.bank(intro_sphere_0_gfx)
		ldy #<intro_sphere_0_gfx
		ldx #>intro_sphere_0_gfx
		jsr decrunchFar

		lda #<sphere_frame_1
		sta BB_DST+0
		lda #>sphere_frame_1
		sta BB_DST+1
		lda #<.bank(intro_sphere_1_gfx)
		ldy #<intro_sphere_1_gfx
		ldx #>intro_sphere_1_gfx
		jsr decrunchFar

		lda #<sphere_frame_2
		sta BB_DST+0
		lda #>sphere_frame_2
		sta BB_DST+1
		lda #<.bank(intro_sphere_2_gfx)
		ldy #<intro_sphere_2_gfx
		ldx #>intro_sphere_2_gfx
		jsr decrunchFar

		lda #<sphere_frame_3
		sta BB_DST+0
		lda #>sphere_frame_3
		sta BB_DST+1
		lda #<.bank(intro_sphere_3_gfx)
		ldy #<intro_sphere_3_gfx
		ldx #>intro_sphere_3_gfx
		jsr decrunchFar

		rts
.endproc

.segment "INTRO11"
.proc fadeOut
		ldx #18
		:
			.repeat 17,I
				lda $d800+11+(3+I)*40,x
				and #$f
				sta fade_d800+I*19,x
			.endrep
			dex
			bmi :+
		jmp :-
		:

		ldx #0
		:
			txa
			pha
			jsr doFade
			lda #5
			jsr waitWithAbort
			beq :+
				pla
				sec
				rts
			:
			pla
			dec musicVolume
			tax
			inx
			cpx #8
		bne :--
		clc
		rts
.endproc

.proc doFade
		txa
		tay
		asl
		asl
		asl
		asl
		adc #<fadeTable
		sta TMP2+0
		lda #0
		adc #>fadeTable
		sta TMP2+1
		ldx animationFrame

		lda fadeD021,y
		sta sphere_d021tab,x
		lda fadeD022,y
		sta sphere_d022tab,x
		lda fadeD023,y
		sta sphere_d023tab,x

		lda fadeD025,y
		sta sphere_d025
		lda fadeD026,y
		sta sphere_d026
		lda fadeD02C,y
		sta sphere_d02c

		lda #<($d800+11+3*40)
		sta DST_D800+0
		lda #>($d800+11+3*40)
		sta DST_D800+1
		
		lda #13
		sta TMP
		ldx #0
		rowLoop2:
			.repeat 19,I
				ldy fade_d800+I,x
				lda (TMP2),y
				ldy #I
				sta (DST_D800),y
			.endrep
			clc
			txa
			adc #19
			tax

			clc
			lda DST_D800+0
			adc #40
			sta DST_D800+0
			bcc :+
				inc DST_D800+1
			:
			dec TMP
			beq :+
		jmp rowLoop2

:
		lda #4
		sta TMP
		ldx #0
		rowLoop3:
			.repeat 18,I
				ldy fade_d800+13*19+I,x
				lda (TMP2),y
				ldy #I
				sta (DST_D800),y
			.endrep
			clc
			txa
			adc #19
			tax

			clc
			lda DST_D800+0
			adc #40
			sta DST_D800+0
			bcc :+
				inc DST_D800+1
			:
			dec TMP
			bne :+
				rts
			:
		jmp rowLoop3

fadeD021:.byte $9,$9,$9,$9,$9,$9,$9,$0
fadeD022:.byte $0,$0,$0,$0,$0,$0,$0,$0
fadeD023:.byte $c,$8,$8,$b,$b,$9,$9,$0
fadeD025:.byte $6,$6,$0,$0,$0,$0,$0,$0
fadeD026:.byte $e,$e,$8,$b,$6,$0,$0,$0
fadeD02C:.byte $d,$f,$5,$c,$8,$b,$6,$0

fadeTable:
		.byte $0,$1,$2,$3,$4,$5,$6,$7, $0+8,$1+8,$2+8,$3+8,$4+8,$5+8,$6+8,$7+8
		.byte $0,$7,$2,$3,$4,$5,$6,$3, $0+8,$7+8,$2+8,$3+8,$4+8,$5+8,$6+8,$3+8
		.byte $0,$3,$6,$5,$2,$4,$6,$5, $0+8,$3+8,$6+8,$5+8,$2+8,$4+8,$6+8,$5+8
		.byte $0,$5,$6,$4,$2,$2,$6,$4, $0+8,$5+8,$6+8,$4+8,$2+8,$2+8,$6+8,$4+8
		.byte $0,$4,$6,$4,$2,$2,$0,$2, $0+8,$4+8,$6+8,$4+8,$2+8,$2+8,$0+8,$2+8
		.byte $0,$2,$0,$2,$6,$6,$0,$6, $0+8,$2+8,$0+8,$2+8,$6+8,$6+8,$0+8,$6+8
		.byte $0,$6,$0,$6,$6,$0,$0,$0, $0+8,$6+8,$0+8,$6+8,$6+8,$0+8,$0+8,$0+8
		.byte $0,$0,$0,$0,$0,$0,$0,$0, $0+8,$0+8,$0+8,$0+8,$0+8,$0+8,$0+8,$0+8
.endproc
