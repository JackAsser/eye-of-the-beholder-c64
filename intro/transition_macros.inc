; code cmd    params
; 00   begin  -
; 01   bitblt sx,sy,dx,dy,w,h
; 02   hclear row
; 03   vclear col
; 04   fill   x,y,w,h
; 05   sprites x
; 06   prio   prio
; 07   sword  x,y
; 08   pray   y
; 09   timerSplit timerLo, timerHi, maskX, maskY
; 0a   end 	  -
; ff   done   -

.macro _begin
		.byte 0
.endmacro

.macro _bitblt sx,sy, dx,dy, w,h
		.byte 1,sx,sy,dx,dy,w,h
.endmacro

.macro _vclear col
		.byte 2,col
.endmacro

.macro _hclear row
		.byte 3,row
.endmacro

.macro _fill dx,dy,w,h
		.byte 4,dx,dy,w,h
.endmacro

.macro _sprites xpos
		.byte 5,<(xpos),>(xpos)
.endmacro

.macro _prio prio
		.byte 6,prio
.endmacro

.macro _sword xpos,ypos
		.byte 7,<(xpos),>(xpos),ypos
.endmacro

.macro _pray ypos
		.byte 8,ypos
.endmacro

.macro _timerSplit ypos, maskx, masky
		.byte 9
		.byte <(((ypos)-7)*63-1), >(((ypos)-7)*63-1)
		.byte <(((ypos)-7)*64-1), >(((ypos)-7)*64-1)
		.byte <(((ypos)-7)*65-1), >(((ypos)-7)*65-1)
		.byte maskx, masky
.endmacro

.macro _end
		.byte $a
.endmacro

.macro _bitblt2 sx,sy, dx,dy, w,h
		.byte $b,sx,sy,dx,dy,w,h
.endmacro

.macro _wait amount
		.byte $c,amount
.endmacro

.macro _done
		.byte $ff
.endmacro