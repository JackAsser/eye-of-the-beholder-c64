.include "../global.inc"
.include "bitblt.inc"

.segment "INTRO_BSS"
		srcBitmapRowLo: .res 25
		srcBitmapRowHi: .res 25
		srcScreenRowLo: .res 25
		srcScreenRowHi: .res 25
		srcD800RowLo: 	.res 25
		srcD800RowHi: 	.res 25

.export dstBitmapHi
		dstBitmapHi: 	.res 1
.export dstScreenHi
		dstScreenHi: 	.res 1

.align 256
.export db_d800
		db_d800:		.res 1024

.segment "INTRO_RAM"
.export setBitBltSource
.proc setBitBltSource
		sta strideScreen+1
		sta strideD800+1

		ldy #0
		sty TMP
		asl
		rol TMP
		asl
		rol TMP
		asl
		rol TMP
		sta strideBitmapLo+1
		lda TMP
		sta strideBitmapHi+1

		lda #<srcBitmapRowLo
		sta DST+0
		lda #>srcBitmapRowLo
		sta DST+1
		lda #<srcBitmapRowHi
		sta DST2+0
		lda #>srcBitmapRowHi
		sta DST2+1

		lda #<srcScreenRowLo
		sta DST3+0
		lda #>srcScreenRowLo
		sta DST3+1
		lda #<srcScreenRowHi
		sta DST4+0
		lda #>srcScreenRowHi
		sta DST4+1

		lda #<srcD800RowLo
		sta DST5+0
		lda #>srcD800RowLo
		sta DST5+1
		lda #<srcD800RowHi
		sta DST6+0
		lda #>srcD800RowHi
		sta DST6+1

		clc
		ldy #0
		:
			lda SRC_BITMAP+0
			sta (DST),y
			strideBitmapLo:adc #00
			sta SRC_BITMAP+0
			lda SRC_BITMAP+1
			sta (DST2),y
			strideBitmapHi:adc #00
			sta SRC_BITMAP+1

			lda SRC_SCREEN+0
			sta (DST3),y
			strideScreen:adc #00
			sta SRC_SCREEN+0
			lda SRC_SCREEN+1
			sta (DST4),y
			adc #00
			sta SRC_SCREEN+1

			lda SRC_D800+0
			sta (DST5),y
			strideD800:adc #00
			sta SRC_D800+0
			lda SRC_D800+1
			sta (DST6),y
			adc #00
			sta SRC_D800+1

			iny
			cpy #25
		bne :-
		rts
.endproc

.export bitblt 
.proc bitblt
		lda #>db_d800
		sta _d800dst+1

direct:	lda BLTSY
		pha
		lda BLTDY
		pha
		lda BLTH
		pha

		rowLoop:
			; Prepare source
			ldx BLTSX
			ldy BLTSY

			;SRC_BITMAP = srcBitmap + x*8 + y*srcStride*8 
			clc
			lda srcBitmapRowLo,y
			adc mul8lo,x
			sta SRC_BITMAP+0
			lda srcBitmapRowHi,y
			adc mul8hi,x
			sta SRC_BITMAP+1

			;SRC_SCREEN = srcScreen + x + y*srcStride
			clc
			txa
			adc srcScreenRowLo,y
			sta srcScrn+1
			lda #0
			adc srcScreenRowHi,y
			sta srcScrn+2

			;SRC_D800 = srcD800 + x + y*srcStride
			clc
			txa
			adc srcD800RowLo,y
			sta srcD800+1
			lda #0
			adc srcD800RowHi,y
			sta srcD800+2

			; Prepare destination
			ldx BLTDX
			ldy BLTDY

			;DST_BITMAP = dstBitmap + x*8 + y*320
			clc
			lda mul8lo,x
			adc mul320lo,y
			sta DST_BITMAP+0
			lda mul8hi,x
			adc mul320hi,y
			ora dstBitmapHi
			sta DST_BITMAP+1

			;DST_SCREEN = dstScreen + x + y*40
			clc
			txa
			adc mul40lo,y
			sta dstScrn+1
			lda mul40hi,y
			adc #0
			ora dstScreenHi
			sta dstScrn+2

			;DST_SCREEN = $d800 + x + y*40
			clc
			txa
			adc mul40lo,y
			sta dstD800+1
			lda mul40hi,y
_d800dst:	adc #>db_d800
			sta dstD800+2

			ldx #0
			ldy #0
			colLoop:
				srcScrn:lda $1000,x
				dstScrn:sta $1000,x
				srcD800:lda $1000,x
				dstD800:sta $1000,x
				.repeat 8,I
					lda (SRC_BITMAP),y
					sta (DST_BITMAP),y
					iny
				.endrep
				inx
				cpx BLTW
			bne colLoop

			inc BLTSY
			inc BLTDY
			dec BLTH

			bne :+
				pla
				sta BLTH
				pla
				sta BLTDY
				pla
				sta BLTSY
				rts
			:
		jmp rowLoop
.endproc

.export bitbltNoDblDuffer 
.proc bitbltNoDblDuffer
		lda #>$d800
		sta bitblt::_d800dst+1
		jmp bitblt::direct
.endproc

.export fill 
.proc fill
		lda BLTH
		pha

		rowLoop:
			; Prepare destination
			ldx BLTDX
			ldy BLTDY

			;DST_BITMAP = dstBitmap + x*8 + y*320
			clc
			lda mul8lo,x
			adc mul320lo,y
			sta DST_BITMAP+0
			lda mul8hi,x
			adc mul320hi,y
			ora dstBitmapHi
			sta DST_BITMAP+1

			;DST_SCREEN = dstScreen + x + y*40
			clc
			txa
			adc mul40lo,y
			sta dstScrn+1
			lda mul40hi,y
			adc #0
			ora dstScreenHi
			sta dstScrn+2

			ldx #0
			ldy #0
			colLoop:
				lda #$40
				dstScrn:sta $1000,x
				lda #$55
				.repeat 8,I
					sta (DST_BITMAP),y
					iny
				.endrep
				inx
				cpx BLTW
			bne colLoop

			inc BLTDY
			dec BLTH

			bne :+
				pla
				sta BLTH
				rts
			:
		jmp rowLoop
.endproc

; x = column
.export vclear
.proc vclear
		lda mul8lo,x
		sta DST_BITMAP+0
		lda mul8hi,x
		ora dstBitmapHi
		sta DST_BITMAP+1

		stx dstD800+1
		lda #>db_d800
		sta dstD800+2

		clc
		ldx #25
		rowLoop:
			ldy #0
			dstD800:sty db_d800
			lda #$ff
			.repeat 8,I
				sta (DST_BITMAP),y
				iny
			.endrep

			lda dstD800+1
			adc #40
			sta dstD800+1
			bcc :+
				inc dstD800+2
				clc
			:

			lda DST_BITMAP+0
			adc #<320
			sta DST_BITMAP+0
			lda DST_BITMAP+1
			adc #>320
			sta DST_BITMAP+1

			dex
		bne rowLoop
		rts
.endproc

; y = row
.export hclear
.proc hclear
		lda mul320lo,y
		sta DST_BITMAP+0
		lda mul320hi,y
		ora dstBitmapHi
		sta DST_BITMAP+1

		lda mul40lo,y
		sta dstD800a+1
		sta dstD800b+1
		clc
		lda mul40hi,y
		adc #>db_d800
		sta dstD800a+2
		sta dstD800b+2

		ldy #0
		ldx #0
		:
			lda #0
			dstD800a:sta db_d800,x
			inx
			lda #$ff
			.repeat 8,I
				sta (DST_BITMAP),y
				iny
			.endrep
		bne :-
		inc DST_BITMAP+1
		:
			lda #0
			dstD800b:sta db_d800,x
			inx
			lda #$ff
			.repeat 8,I
				sta (DST_BITMAP),y
				iny
			.endrep
			cpx #40
		bne :-
		rts
.endproc

mul8lo:		.repeat 40,I
				.byte <(I*8)
			.endrep
mul8hi:		.repeat 40,I
				.byte >(I*8)
			.endrep
mul40lo:	.repeat 25,I
				.byte <(I*40)
			.endrep
mul40hi:	.repeat 25,I
				.byte >(I*40)
			.endrep
mul320lo:	.repeat 25,I
				.byte <(I*320)
			.endrep
mul320hi:	.repeat 25,I
				.byte >(I*320)
			.endrep
