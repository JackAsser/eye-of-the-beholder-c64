.include "../global.inc"

.segment "INTRO_RAM"
.export decrunchToUnderIO
.proc decrunchToUnderIO
		lda 1
		pha
		lda #$31
		sta 1
		jsr decrunchTo
		pla
		sta 1
		rts
.endproc

.segment "MUSIC"
.include "../converters/music/eye-intro.inc"
.proc decrunchMusic
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<music
		ldx #>music
		jsr decrunchTo
		rts
music:	.incbin "../converters/music/eye-intro.prg.b2",2
.endproc

.segment "INTRO"

.export checkAbort
.proc checkAbort
	lda KEYEVENT+KeyEvent::keyPressed
	bne :+
	lda MOUSEEVENT+MouseEvent::buttons
	bpl :+
		lda #0
		rts
	:
	
	lda #1 ; Change to 0 to differentiate between escape and other keys
	sta TMP

	lda MOUSEEVENT+MouseEvent::buttons
	cmp #$ff
	beq :+
		and #2
		beq :+
			inc TMP
		:

	lda KEYEVENT+KeyEvent::keyPressed
	beq :++
		lda KEYEVENT+KeyEvent::modifiers
		and #$80
		beq :+
			inc TMP
		:
		lda KEYEVENT+KeyEvent::normalChar
		cmp #$1f
		bne :+
			inc TMP
		:

consumeEvents:
	lda #0
	sta KEYEVENT+KeyEvent::keyPressed
	sta KEYEVENT+KeyEvent::normalChar
	sta KEYEVENT+KeyEvent::specialChar
	sta KEYEVENT+KeyEvent::modifiers
	lda #$ff
	sta MOUSEEVENT+MouseEvent::buttons
	lda TMP
	rts
.endproc
.export checkAbortConsumeEvents = checkAbort::consumeEvents

.export newScene
.proc newScene
		jsr checkAbort
		php
		clc
		ldx #<screenModeNoop
		ldy #>screenModeNoop
		jsr setScreenMode
		lda #0
		sta $d015
		plp
		rts
.endproc

.pushseg
.segment "INTRO_RAM_INIT"
introRamInit:
		memcpy __INTRO_RAM_RUN__, __INTRO_RAM_LOAD__, __INTRO_RAM_SIZE__
		rts		
.popseg

.export intro
.proc intro
		jsrf introRamInit
		jsrf decrunchMusic

		lda system
		cmp #SYSTEM_PAL
		bne :++
			lda sidModel
			bne :+
				memcpy $9000, freeRam+eye_pal_intro_6581_fe, eye_pal_intro_6581_fe_size
				jmp musicInstalled
			:
				memcpy $9000, freeRam+eye_pal_intro_8580_fe, eye_pal_intro_8580_fe_size
				jmp musicInstalled
		:
			lda sidModel
			bne :+
				memcpy $9000, freeRam+eye_NTSC_intro_6581_fe, eye_NTSC_intro_6581_fe_size
				jmp musicInstalled
			:
				memcpy $9000, freeRam+eye_NTSC_intro_8580_fe, eye_NTSC_intro_8580_fe_size

		musicInstalled:
			jsr initMusic
			lda #15
			sta musicVolume
			lda #1
			sta musicEnabled

		jsr checkAbort::consumeEvents
;jmp skip
		jsrf logo_scene
		jsr newScene
		beq :+
			jmp abort
		:

		jsr credits_scene
		jsr newScene
		beq :+
			jmp abort
		:

		jsr initText
		jsr tower_scene
		jsr newScene
		beq :+
			jmp abort
		:

		jsr sphere_scene

		jsr waterdeep_entry_scene
		jsr newScene
		bne abort

		jsrf throneroom_scene
		jsr newScene

		jsr hands_knife_scene
		jsr newScene
		bne abort

		jsr waterdeep_exit_scene
		jsr newScene
		bne abort

		jsr initText
		jsrf entrance_scene
		jsr newScene

		jsr initText
		jsrf tunnel_scene
		jsr newScene

abort:
		lda #0
		sta musicEnabled
		ldx #$1f
		:
			sta $d400,x
			dex
		bpl :-
		sec
		ldx #<screenModeNoop
		ldy #>screenModeNoop
		jsr setScreenMode
		lda #0
		sta $d020
		sta $d011
		sta $d015
		rts
.endproc
