text_template	= $5000
.export text_screen0
text_screen0	= $8000
.export text_screen1
text_screen1	= $8400
text_font		= $8800
.export text_emptyspr
text_emptyspr	= $bf00

.include "../global.inc"

.segment "INTRO_RAM"
; x = src row
; y = dst row
; ARGS0 = color
.export showTextExt
.proc showTextExt
		sta ARGS+0
		jsrf showText_fromFarCall
		rts
.endproc

.segment "INTRO"

intro_text_gfx:						.incbin  "../converters/intro/compressed/intro_text.prg.b2",2
									.include "../converters/intro/compressed/intro_text.inc"

; x = src row
; y = dst row
; a = color
.export showText
showText_fromFarCall:
		lda ARGS+0
.proc showText
		pha
		lda template_lo,x
		sta SRC_SCREEN+0
		lda template_hi,x
		sta SRC_SCREEN+1
		lda dst_screen_lo,y
		sta DST_SCREEN+0
		sta DST_D800+0
		sta DST2+0
		lda #>(text_screen0+$300)
		sta DST_SCREEN+1
		lda #>(text_screen1+$300)
		sta DST2+1
		lda #>($d800+$300)
		sta DST_D800+1
		pla
		tax

		ldy #79
		:
			txa
			sta (DST_D800),y
			lda (SRC_SCREEN),y
			sta (DST_SCREEN),y
			sta (DST2),y
			dey
		bpl :-

.if 0
		ldx #0
		:
			txa
			.repeat 4,I
				sta text_screen0+I*$100,x
				sta text_screen1+I*$100,x
				sta $d800+I*$100,x
			.endrep
			inx
		bne :-
.endif
		rts

		template_lo:
			.repeat 12,I
				.byte <(text_template+2*40*I)
			.endrep

		template_hi:
			.repeat 12,I
				.byte >(text_template+2*40*I)
			.endrep

		dst_screen_lo:
			.byte <(text_screen0+21*40)
			.byte <(text_screen0+23*40)
.endproc

.export initText
.proc initText
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<intro_text_gfx
		ldx #>intro_text_gfx
		jsr decrunchTo
		memcpy text_font, (freeRam+intro_text_charset), intro_text_charset_size
		memcpy text_template, (freeRam+intro_text_screen), intro_text_screen_size

noDecrunch:
		lda #0
		ldx #62
		:
			sta text_emptyspr,x
			dex
		bpl :-
		inx
		:
			sta text_screen0+$000,x
			sta text_screen0+$100,x
			sta text_screen0+$200,x
			sta text_screen0+$300,x
			sta text_screen1+$000,x
			sta text_screen1+$100,x
			sta text_screen1+$200,x
			sta text_screen1+$300,x
			inx
		bne :-
		lda #<(text_emptyspr/$40)
		ldx #7
		:
			sta text_screen0+$3f8,x
			sta text_screen1+$3f8,x
			dex
		bpl :-
		rts
.endproc

.segment "ENDING"
ending_text_gfx:		.incbin  "../converters/ending/compressed/ending_text.prg.b2",2
						.include "../converters/ending/compressed/ending_text.inc"

.export initTextEnding 
.proc initTextEnding
		lda #<freeRamEnding
		sta BB_DST+0
		lda #>freeRamEnding
		sta BB_DST+1
		ldy #<ending_text_gfx
		ldx #>ending_text_gfx
		jsr decrunchTo
		memcpy text_font, (freeRamEnding+ending_text_charset), ending_text_charset_size
		memcpy text_template, (freeRamEnding+ending_text_screen), ending_text_screen_size
		jsrf initText::noDecrunch
		rts
.endproc

.segment "ENDING_RAM"
; x = src row
; y = dst row
; ARGS0 = color
.export showTextExtEnding
.proc showTextExtEnding
		sta ARGS+0
		jsrf showText_fromFarCall
		rts
.endproc
