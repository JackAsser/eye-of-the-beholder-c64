.include "../global.inc"

.segment "INTRO_BSS"
plexPos: .res 1
plexRound: .res 1

.segment "INTRO_RAM"
spriteColors: .res 9,6
spritesMap:
	.incbin "../converters/intro/converted/intro_credits_spritesmap.bin"

.proc screenMode
	.word init
	.word teardown
	.byte 7
	.word $20, resetMultiplexer
	.word $3b-4+32, multiplex
	.word $3b-4+32+24, multiplex
	.word $3b-4+32+24+24, multiplex
	.word $3b-4+32+24+24+24, multiplex
	.word $3b-4+32+24+24+24+32, multiplex
	.word $3b-4+32+24+24+24+32+32, multiplex

secondScreen:
	.word init
	.word teardown
	.byte 2
	.word $20, resetMultiplexer2
	.word $33-6+10*8+32, multiplex

noop:
	.word init2
	.word teardown2
	.byte 1
	.word $20, exitIrq

.pushseg
.segment "INTRO"
	.proc reset
		stx $d001
		stx $d003
		stx $d005
		stx $d007
		stx $d009
		stx $d00b
		stx $d00d
		stx $d00f

		ldx #7
		lda spriteColors,y
		:
			sta $d027,x
			dex
		bpl :-

		tya
		asl
		asl
		asl
		tay
		ldx #0
		:
			lda spritesMap,y
			sta vic_screen+$3f8,x
			iny
			inx
			cpx #8
		bne :-
		sty plexPos

		lda #0
		sta plexRound
		rts
	.endproc
.popseg
	doReset: jsrf reset
	jmp exitIrq

	.proc resetMultiplexer2
		ldx #$33+10*8
		ldy #7
		jmp doReset
	.endproc

	.proc resetMultiplexer
		ldx #$3b
		ldy #0
		jmp doReset
	.endproc

	.proc multiplex
		jsrf :+
.pushseg
.segment "INTRO4"
:
		clc
		lda $d001
		ldy plexRound
		adc plexAdd,y
		iny
		sty plexRound
		sta $d001
		sta $d003
		sta $d005
		sta $d007
		sta $d009
		sta $d00b
		sta $d00d
		sta $d00f
		lda plexPos
		lsr
		lsr
		lsr
		tay
		lda spriteColors,y
		sta $d027
		sta $d028
		sta $d029
		sta $d02a
		sta $d02b
		sta $d02c
		sta $d02d
		sta $d02e

		ldy plexPos
		.repeat 8,I
			lda spritesMap+I,y
			sta vic_screen+$3f8+I
		.endrep

		clc
		tya
		adc #8
		sta plexPos
		rts
plexAdd:.byte 32,24,24,24,32,32
.popseg
		jmp exitIrq_quick
	.endproc

	.proc init
		jsrf :+
		rts
.pushseg
.segment "INTRO"
:
		lda #$18
		sta $d016
		lda $dd00
		and #$fc
		sta $dd00
		lda #$3b
		sta $d011
		lda #$38
		sta $d018
		lda #6
		sta $d020
		sta $d021

		lda #48*0
		sta $d000
		lda #48*1
		sta $d002
		lda #48*2
		sta $d004
		lda #48*3
		sta $d006
		lda #48*4
		sta $d008
		lda #48*5
		sta $d00a
		lda #<(48*6)
		sta $d00c
		lda #<(48*7)
		sta $d00e
		lda #$c0
		sta $d010

		lda #$ff
		sta $d015
		sta $d01d
		lda #0
		sta $d017
		sta $d01b
		sta $d01c

		rts
.popseg
	.endproc

	.proc init2
		rts
	.endproc

	.proc teardown
		lda #0
		sta $d015
		sta $d011
		rts
	.endproc

	.proc teardown2
		lda #0
		sta $d021
		sta $d020
		rts
	.endproc
.endproc

.segment "INTRO"
intro_credits_gfx:
		.incbin  "../converters/intro/compressed/intro_credits.prg.b2",2
		.include "../converters/intro/compressed/intro_credits.inc"

.segment "INTRO2"
.proc fadeRows
		lda sramp,x
		sta spriteColors,y
		lda ramp_lo,x
		sta sm4+1
		sta sm5+1
		lda ramp_hi,x
		sta sm4+2
		sta sm5+2
		lda srcScreenRow_lo,y
		sta sm0+1
		lda srcScreenRow_hi,y
		sta sm0+2
		lda srcD800Row_lo,y
		sta sm1+1
		lda srcD800Row_hi,y
		sta sm1+2
		lda dstScreenRow_lo,y
		sta sm2+1
		lda dstScreenRow_hi,y
		sta sm2+2
		lda dstD800Row_lo,y
		sta sm3+1
		lda dstD800Row_hi,y
		sta sm3+2
		jmp :+
.pushseg
.segment "INTRO_RAM"
		:
		ldx #3*40-1
		:
sm0:		ldy freeRam+intro_credits_screen,x
sm4:		lda ramps,y
sm2:		sta vic_screen,x
sm1:		ldy freeRam+intro_credits_d800,x
sm5:		lda ramps,y
sm3:		sta $d800,x
			dex
		bpl :-
		rts
.popseg

ramp_lo:.repeat 12,I
			.byte <(ramps+I)
		.endrep

ramp_hi:.repeat 12,I
			.byte >(ramps+I)
		.endrep

srcScreenRow_lo:
		.byte <(freeRam+intro_credits_screen+1*40)
		.byte <(freeRam+intro_credits_screen+5*40)
		.byte <(freeRam+intro_credits_screen+8*40)
		.byte <(freeRam+intro_credits_screen+11*40)
		.byte <(freeRam+intro_credits_screen+14*40)
		.byte <(freeRam+intro_credits_screen+18*40)
		.byte <(freeRam+intro_credits_screen+22*40)
		.byte <(freeRam+intro_credits_cover_screen+0*40)
		.byte <(freeRam+intro_credits_cover_screen+4*40)

srcScreenRow_hi:
		.byte >(freeRam+intro_credits_screen+1*40)
		.byte >(freeRam+intro_credits_screen+5*40)
		.byte >(freeRam+intro_credits_screen+8*40)
		.byte >(freeRam+intro_credits_screen+11*40)
		.byte >(freeRam+intro_credits_screen+14*40)
		.byte >(freeRam+intro_credits_screen+18*40)
		.byte >(freeRam+intro_credits_screen+22*40)
		.byte >(freeRam+intro_credits_cover_screen+0*40)
		.byte >(freeRam+intro_credits_cover_screen+4*40)

srcD800Row_lo:
		.byte <(freeRam+intro_credits_d800+1*40)
		.byte <(freeRam+intro_credits_d800+5*40)
		.byte <(freeRam+intro_credits_d800+8*40)
		.byte <(freeRam+intro_credits_d800+11*40)
		.byte <(freeRam+intro_credits_d800+14*40)
		.byte <(freeRam+intro_credits_d800+18*40)
		.byte <(freeRam+intro_credits_d800+22*40)
		.byte <(freeRam+intro_credits_cover_d800+0*40)
		.byte <(freeRam+intro_credits_cover_d800+4*40)

srcD800Row_hi:
		.byte >(freeRam+intro_credits_d800+1*40)
		.byte >(freeRam+intro_credits_d800+5*40)
		.byte >(freeRam+intro_credits_d800+8*40)
		.byte >(freeRam+intro_credits_d800+11*40)
		.byte >(freeRam+intro_credits_d800+14*40)
		.byte >(freeRam+intro_credits_d800+18*40)
		.byte >(freeRam+intro_credits_d800+22*40)
		.byte >(freeRam+intro_credits_cover_d800+0*40)
		.byte >(freeRam+intro_credits_cover_d800+4*40)

dstScreenRow_lo:
		.byte <(vic_screen+1*40)
		.byte <(vic_screen+5*40)
		.byte <(vic_screen+8*40)
		.byte <(vic_screen+11*40)
		.byte <(vic_screen+14*40)
		.byte <(vic_screen+18*40)
		.byte <(vic_screen+22*40)
		.byte <(vic_screen+10*40)
		.byte <(vic_screen+14*40)

dstScreenRow_hi:
		.byte >(vic_screen+1*40)
		.byte >(vic_screen+5*40)
		.byte >(vic_screen+8*40)
		.byte >(vic_screen+11*40)
		.byte >(vic_screen+14*40)
		.byte >(vic_screen+18*40)
		.byte >(vic_screen+22*40)
		.byte >(vic_screen+10*40)
		.byte >(vic_screen+14*40)

dstD800Row_lo:
		.byte <($d800+1*40)
		.byte <($d800+5*40)
		.byte <($d800+8*40)
		.byte <($d800+11*40)
		.byte <($d800+14*40)
		.byte <($d800+18*40)
		.byte <($d800+22*40)
		.byte <($d800+10*40)
		.byte <($d800+14*40)

dstD800Row_hi:
		.byte >($d800+1*40)
		.byte >($d800+5*40)
		.byte >($d800+8*40)
		.byte >($d800+11*40)
		.byte >($d800+14*40)
		.byte >($d800+18*40)
		.byte >($d800+22*40)
		.byte >($d800+10*40)
		.byte >($d800+14*40)

sramp:	.byte $06,$0b,$0c,$0e,$0e,$0c,$0b,$00,$0b,$06,$06,$06
ramps:
		.byte $06,$06,$06,$0b,$07,$0f,$0f,$0f,$0e,$06,$06,$06;0
		.byte $66,$66,$66,$bb,$77,$ff,$ff,$ff,$ee,$66,$66,$66;1
		.byte $66,$66,$66,$bb,$77,$ff,$7f,$17,$df,$3e,$e6,$66;2
		.byte $06,$06,$06,$0b,$07,$0f,$0f,$0c,$06,$06,$06,$06;3
		.byte $66,$66,$66,$bb,$77,$ff,$ff,$f7,$ef,$6e,$66,$66;4
		.byte $06,$06,$06,$0b,$07,$0f,$0a,$08,$09,$06,$06,$06;5
		.byte $66,$66,$66,$bb,$77,$ff,$7f,$1c,$d6,$36,$e6,$66;6
		.byte $66,$66,$66,$bb,$77,$ff,$ff,$7c,$f6,$e6,$66,$66;7
		.byte $66,$66,$66,$bb,$77,$ff,$fa,$78,$f9,$e6,$66,$66;8
		.byte $06,$06,$06,$0b,$0f,$0a,$08,$09,$06,$06,$06,$06;9
		.byte $66,$66,$66,$bb,$77,$ff,$fa,$f8,$e9,$66,$66,$66;a
		.byte $66,$66,$66,$bb,$7f,$fa,$f8,$79,$f6,$e6,$66,$66;b
		.byte $66,$66,$66,$bb,$77,$ff,$ff,$fc,$e6,$66,$66,$66;c
		.byte $06,$06,$06,$0b,$07,$0f,$0f,$07,$0f,$0e,$06,$06;d
		.byte $66,$66,$66,$bb,$77,$ff,$f7,$f1,$ed,$63,$6e,$66;e
.endproc

.proc fadeIn
		ldy #0
		fadeLoop:
			sty COUNT
			.repeat 7,I
				ldx fadeTab+(6-I),y
				ldy #I
				jsr fadeRows
				ldy COUNT
			.endrep

			lda #3
			jsr waitWithAbort
			bne abort
			iny
			cpy #fadeTabSize-1*7+1
		bne fadeLoop
abort:	rts

secondScreen:
		ldy #0
		fadeLoop2:
			sty COUNT
			.repeat 2,I
				ldx fadeTab+(6-I),y
				ldy #I+7
				jsr fadeRows
				ldy COUNT
			.endrep

			lda #3
			jsr waitWithAbort
			bne abort
			iny
			cpy #fadeTabSize-1*7+1
		bne fadeLoop2
		jmp abort

fadeTab:.res 7,0
		.byte 1,2,3,4,5,6
		.res 7*5,7
		.byte 8,9,10
		.res 7,11
fadeTabSize = *-fadeTab
.endproc

.proc fadeOut
		ldx #0
		:
			lda #3
			jsr waitWithAbort
			bne abort
			bit $d011
			bmi *-3
			lda system
			cmp #SYSTEM_PAL
			beq :+
			cmp #SYSTEM_DREAN
			beq :+
				lda #16
				cmp $d012
				bne *-3
			:
			lda colors,x
			sta $d020
			inx
			cpx #15
		bne :--
		lda #25
		jsr waitWithAbort
		bne abort
abort:	rts

colors:	.byte $b,$4,$e,$5,$3,$d,$1,$d,$3,$5,$e,$4,$b,$6,$0
.endproc

.segment "INTRO"
.export credits_scene
.proc credits_scene
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<intro_credits_gfx
		ldx #>intro_credits_gfx
		jsr decrunchTo

		lda #$66
		ldx #0
		:
			.repeat 4,I
				sta vic_screen+I*$100,x
				sta $d800+I*$100,x
			.endrep
			inx
		bne :-

		memcpy vic_bitmap, (freeRam+intro_credits_bitmap), intro_credits_bitmap_size
		.assert intro_credits_sprites_size < vic_sprites_size, error, "Too many sprites."
		memcpy vic_sprites, (freeRam+intro_credits_sprites), intro_credits_sprites_size

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

		jsrf fadeIn

		sec
		ldx #<screenMode::secondScreen
		ldy #>screenMode::secondScreen
		jsr setScreenMode

		memcpy vic_bitmap+10*320, (freeRam+intro_credits_cover_bitmap), intro_credits_cover_bitmap_size

		jsrf fadeIn::secondScreen

		clc
		ldx #<screenMode::noop
		ldy #>screenMode::noop
		jsr setScreenMode

		jsrf fadeOut

		rts
.endproc
