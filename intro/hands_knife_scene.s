.include "../global.inc"
.include "bitblt.inc"

db_sprites= $4000
db_screen = $4c00
db_bitmap = $6000

.define INCFRAMECOUNTER_POS $b0

.segment "INTRO_BSS"
spritePos:		.res 2
swordYPos:		.res 1
spritePos_latch:.res 2
swordYPos_latch:.res 1
prayYPos:		.res 1
timerLo_latch:	.res 1
timerHi_latch:	.res 1
maskX_latch:	.res 1
maskY_latch:	.res 1

.segment "INTRO_RAM"

frameCounter: 	.byte 0
nextCounter: 	.byte 1

splitPosition: 	.res 1
splitLines: 	.res 5+1
splitAction:	.byte 0,1,0,1,0,1,0,1

.proc screenMode
	.word init
	.word teardown
	.byte 6
	.word $20, resetMultiplexer
	.word $50, multiplex
	.word $60, plexOverlay1
	.word $80, plexOverlay2
	.word INCFRAMECOUNTER_POS, incFrameCounter
	.word $d8, bottom

	_dd00:.byte $00
	_d018:.byte $38
	_d016:.byte $10
	.proc bottom
		; First create a FLD-line
		inc $d011
		pha
		pla
		pha
		pla
		lda #0
		sta $d021

		; Then on the FLD line, switch to text mode
		lda $dd00
		and #$fc
		ora #$01
		sta $dd00
		lda #$02
		sta $d018
		lda #$08
		sta $d016
		lda #$1c
		sta $d011

		jmp exitIrq_quick
	.endproc

	.proc incFrameCounter
		inc frameCounter
		jmp exitIrq_quick
	.endproc

	.proc bitmapMode
		lda $dd00
		and #$fc
		ora _dd00
		sta $dd00
		lda _d018
		sta $d018
		lda _d016
		sta $d016
		lda #$3b
		sta $d011
		lda #08
		sta $d021
		rts
	.endproc

	.proc init
		rts
	.endproc

	.proc resetMultiplexer
		jsr bitmapMode

		lda #$f
		sta $d028
		sta $d029
		lda #0
		sta $d02a
		lda #$a
		sta $d02b
		sta $d02c
		sta $d02d

		ldx spritePos+0
		ldy spritePos+1
		jsr place_knife_sprites

		lda #$55
		jsr spriteYpos

		ldx #5
		jsr resetSpritePointers

		lda #<(vic_sprites/$40 + 20)
		sta vic_screen+$3f8+1
		sta db_screen+$3f8+1
		lda #<(vic_sprites/$40 + 23)
		sta vic_screen+$3f8+2
		sta db_screen+$3f8+2

		lda #0
		sta splitPosition

		jmp exitIrq
	.endproc

	.proc plexOverlay1
		clc
		lda sprx_lo+0
		adc #6
		sta $d002
		lda sprx_hi+0
		adc #0
		sta sprx_hi+0
		clc
		lda sprx_lo+1
		adc #6
		sta $d004
		lda sprx_hi+1
		adc #0
		sta sprx_hi+1
		jsr calc_d010
		
		clc
		lda $d003
		adc #21
		sta $d003
		sta $d005
		lda #0
		sta $d029
		inc vic_screen+$3f8+1
		inc db_screen+$3f8+1
		inc vic_screen+$3f8+2
		inc db_screen+$3f8+2
		jmp exitIrq_quick
	.endproc

	.proc plexOverlay2
		clc
		lda $d005
		adc #21
		sta $d005

		ldx vic_screen+$3f9
		inx
		stx vic_screen+$3fa
		stx db_screen+$3fa

		lda #0
		sta $d02a
		sta $d02b
		sta $d02c
		sta $d02d

		lda #$b
		sta $d029
		jmp exitIrq_quick
	.endproc

	.proc multiplex
		jsr exitIrq_next
		dec $d019
		cli
poll:
		lda $d012
stop:	cmp #$85
		bcc :+
			jmp exitIrq_rerun
		:

		; Have we passed the trigger line
		ldx splitPosition
		lda $d012
		cmp splitLines,x
		bcc :++
			; Yes, check action
			lda splitAction,x
			bne :+
				; Multiplex
				clc
				lda $d007
				adc #21
				sta $d007
				sta $d009
				sta $d00b
				sta $d00d
			:

			; Change image
			inc vic_screen+$3fb
			inc db_screen+$3fb
			inc vic_screen+$3fc
			inc db_screen+$3fc
			inc vic_screen+$3fd
			inc db_screen+$3fd
			inc vic_screen+$3fe
			inc db_screen+$3fe

			inx
		:
		stx splitPosition

		jmp poll
	.endproc

	.proc resetSpritePointers
		stx stride+1
		clc
		lda #<(vic_sprites/$40 + 0)
		ldx #0
		:
			sta vic_screen+$3fb,x
			sta db_screen+$3fb,x
stride:		adc #5
			inx
			cpx #4
		bne :-
		rts
	.endproc

	.proc spriteYpos
		sta $d003
		sta $d005
		sta $d007
		sta $d009
		sta $d00b
		sta $d00d

		clc
		adc #10
		sta splitLines+0
		adc #11
		sta splitLines+1
		adc #10
		sta splitLines+2
		adc #11
		sta splitLines+3
		adc #10
		sta multiplex::stop+1
		lda #$ff
		sta splitLines+4

		rts
	.endproc

	.proc teardown
		lda #0
		sta $d021
		sta $d011
		rts
	.endproc
.endproc

.proc screenMode2
	.word screenMode::init
	.word screenMode::teardown
	.byte 5
	.word $20, resetMultiplexer
multiplexStart: .word $50, screenMode::multiplex
colorSplitPos: .word $7d, colorSplit
	.word INCFRAMECOUNTER_POS, screenMode::incFrameCounter
	.word $d8, screenMode::bottom

_d01b:.byte %01100100
_d015mask:.byte $ff

	.proc resetMultiplexer
timerLo:lda #0
		sta $dd04
timerHi:lda #0
		sta $dd05
		ora timerLo+1
		beq :+
			bit $dd0d
			lda #$81
			sta $dd0d
			lda #$19
			sta $dd0e
			lda #<timerSplit
			sta $0318
			lda #>timerSplit
			sta $0319
			lda _d01b
			sta $d01b
			lda #$7f
			and _d015mask
			sta $d015
		:

		jsr screenMode::bitmapMode

		lda #$f
		sta $d02b

		ldx spritePos+0
		ldy spritePos+1
		jsr place_sword_sprites

		lda swordYPos
		clc
		adc #($7d-$53)
		sta colorSplitPos

		lda swordYPos
		jsr screenMode::spriteYpos

		sec
		lda swordYPos
		sbc #5
		sta multiplexStart
		
		lda swordYPos
		clc
		adc #20
		sta $d003
		adc #22
		sta $d005

		ldx #5
		jsr screenMode::resetSpritePointers

		lda #<(vic_sprites/$40 + 20)
		sta vic_screen+$3f8+1
		sta db_screen+$3f8+1
		lda #<(vic_sprites/$40 + 21)
		sta vic_screen+$3f8+2
		sta db_screen+$3f8+2

		lda #0
		sta splitPosition

		jmp exitIrq
	.endproc

	.proc timerSplit
		pha
		lda #$ff
		sta $d01b
		lda #1
		sta $d015
		lda #$ff
		sta $d010
		pla
		rti
	.endproc

	.proc colorSplit
		lda #$9
		sta $d02b
		jmp exitIrq_quick
	.endproc
.endproc

.proc screenMode3
	.word screenMode::init
	.word screenMode::teardown
	.byte 4
	.word $20, resetMultiplexer
multiplexStart: .word $50, screenMode::multiplex
	.word INCFRAMECOUNTER_POS, screenMode::incFrameCounter
	.word $d8, screenMode::bottom

	.proc resetMultiplexer
		jsr screenMode::bitmapMode

		lda prayYPos
		jsr screenMode::spriteYpos
		lda #$ff
		sta splitLines+2

		sec
		lda prayYPos
		sbc #5
		sta multiplexStart

		ldx #3
		jsr screenMode::resetSpritePointers

		lda #0
		sta splitPosition

		jmp exitIrq
	.endproc
.endproc

.pushseg
.segment "INTRO_BSS"
sprx_lo: .res 8
sprx_hi: .res 8
.popseg

.proc place_knife_sprites
		clc
		txa
		adc #24*2-6
		sta sprx_lo+0
		sta $d002
		tya
		adc #0
		sta sprx_hi+0

		clc
		txa
		adc #24*3-6
		sta sprx_lo+1
		sta $d004
		tya
		adc #0
		sta sprx_hi+1
		
		clc
		txa
		adc #24*0
		sta sprx_lo+2
		sta $d006
		tya
		adc #0
		sta sprx_hi+2

		clc
		txa
		adc #24*1
		sta sprx_lo+3
		sta $d008
		tya
		adc #0
		sta sprx_hi+3

		clc
		txa
		adc #24*2
		sta sprx_lo+4
		sta $d00a
		tya
		adc #0
		sta sprx_hi+4

		clc
		txa
		adc #24*3
		sta sprx_lo+5
		sta $d00c
		tya
		adc #0
		sta sprx_hi+5

		jsr calc_d010

		rts
.endproc

.proc place_sword_sprites
		clc
		txa
		adc #24
		sta sprx_lo+0
		sta $d002
		tya
		adc #0
		sta sprx_hi+0

		clc
		txa
		adc #38
		sta sprx_lo+1
		sta $d004
		tya
		adc #0
		sta sprx_hi+1
		
		clc
		txa
		adc #24*0
		sta sprx_lo+2
		sta $d006
		tya
		adc #0
		sta sprx_hi+2

		clc
		txa
		adc #24*1
		sta sprx_lo+3
		sta $d008
		tya
		adc #0
		sta sprx_hi+3

		clc
		txa
		adc #24*2
		sta sprx_lo+4
		sta $d00a
		tya
		adc #0
		sta sprx_hi+4

		clc
		txa
		adc #24*3
		sta sprx_lo+5
		sta $d00c
		tya
		adc #0
		sta sprx_hi+5

		jsr calc_d010

		rts
.endproc

.proc calc_d010
		lda #0
		ldx sprx_hi+5
		cpx #1
		rol
		ldx sprx_hi+4
		cpx #1
		rol
		ldx sprx_hi+3
		cpx #1
		rol
		ldx sprx_hi+2
		cpx #1
		rol
		ldx sprx_hi+1
		cpx #1
		rol
		ldx sprx_hi+0
		cpx #1
		rol
		asl
		sta $d010
		rts
.endproc

.segment "INTRO2"
hands_spellbook_gfx:
		.incbin  "../converters/intro/compressed/intro_hands_spellbook.prg.b2",2
		.include "../converters/intro/compressed/intro_hands_spellbook.inc"
.proc depack_spellbook
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<hands_spellbook_gfx
		ldx #>hands_spellbook_gfx
		jsr decrunchTo
		rts
.endproc

hands_knife_gfx:
		.incbin  "../converters/intro/compressed/intro_hands_knife.prg.b2",2
		.include "../converters/intro/compressed/intro_hands_knife.inc"
.proc depack_knife
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<hands_knife_gfx
		ldx #>hands_knife_gfx
		jsr decrunchTo

		.assert intro_hands_knife_sprites_size < vic_sprites_size, error, "Too many sprites."
		memcpy vic_sprites, (freeRam+intro_hands_knife_sprites), intro_hands_knife_sprites_size
		memcpy db_sprites, (freeRam+intro_hands_knife_sprites), intro_hands_knife_sprites_size
		rts
.endproc

.segment "INTRO"

.proc clearBitmap
		ldx #0
		:
			lda #0
			.repeat 4,I
				sta vic_screen+$100*I,x
				sta db_screen+$100*I,x
				sta $d800+$100*I,x
				sta db_d800+$100*I,x
			.endrep
			lda #$ff
			.repeat 31,I
				sta vic_bitmap+$100*I,x
				sta db_bitmap+$100*I,x
			.endrep
			inx
			beq :+
		jmp :-
		:
		ldx #$3f
		:
			sta vic_bitmap+$1f00,x
			sta db_bitmap+$1f00,x
			dex
		bpl :-
		rts
.endproc

.proc setup_knife_sprites
		lda #$08
		sta $d025
		lda #$09
		sta $d026

		lda #$7e
		sta $d015
		sta $d01b
		sta $d01c
		lda #0
		sta $d017
		sta $d01d

		rts
.endproc

.proc setup_sword_sprites
		lda #$07
		sta $d025
		lda #$08
		sta $d026
		lda #$01
		sta $d028
		sta $d029
		lda #$0c
		sta $d02a
		sta $d02c
		sta $d02d

		lda #0
		sta $d027
		lda #<(vic_sprites/$40 + 22)
		sta vic_screen+$3f8
		sta db_screen+$3f8

		lda #%01100100
		sta screenMode2::_d01b

		lda #$7e
		sta $d01c
		sta $d01b
		lda #1
		sta $d017
		sta $d01d

		rts
.endproc

.proc setup_pray_sprites
		lda #$0c
		sta $d025
		lda #$0f
		sta $d026
		lda #$0b
		sta $d02a
		lda #$01
		sta $d02b
		sta $d02c
		sta $d02d

		lda #%01111000
		sta $d015
		sta $d01c
		sta $d01b
		lda #0
		sta $d017
		sta $d01d
		sta $d010

		lda #24+16*8-6
		clc
		sta $d006
		adc #24
		sta $d008
		adc #24
		sta $d00a
		adc #24
		sta $d00c

		rts
.endproc

.export hands_knife_scene
.proc hands_knife_scene
		lda #0
		sta timerLo_latch
		sta timerHi_latch
		
		jsrf depack_knife
		jsr clearBitmap
		jsr setup_knife_sprites

		setBitBltDst db_bitmap, db_screen

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

		setBitBltSrc freeRam+intro_hands_knife_bitmap, freeRam+intro_hands_knife_screen, freeRam+intro_hands_knife_d800, 21
		
		jsrf transition_show_knife

		lda #1
		ldx #8
		ldy #0
		jsr showTextExt

		wait 15

		jsrf transition_move_knife

		lda #0
		ldx #8
		ldy #0
		jsr showTextExt

		wait 15

		jsrf depack_spellbook
		setBitBltSrc freeRam+intro_hands_spellbook_bitmap, freeRam+intro_hands_spellbook_screen, freeRam+intro_hands_spellbook_d800, 16

		jsrf transition_hide_knife
		jsrf transition_knife_to_spellbook

		lda #0
		sta $d015

		wait 15

		jsrf transition_take_spellbook

		wait 5

		jsrf transition_show_spellbook

		wait 15 

		lda #$18
		sta $d016
		lda #<(70+9*8)
		sta spritePos+0
		lda #>(70+9*8)
		sta spritePos+1
		lda #$53+6*8
		sta swordYPos

		jsrf depack_sword
		sec
		ldx #<screenMode2
		ldy #>screenMode2
		jsr setScreenMode
		jsr setup_sword_sprites
		setBitBltSrc freeRam+intro_hands_sword_bitmap, freeRam+intro_hands_sword_screen, freeRam+intro_hands_sword_d800, 18

		jsrf transition_spellbook_to_sword
		lda #0
		sta screenMode2::_d01b

		wait 15 

		jsrf transition_move_sword

		wait 30

		lda #102
		sta $d000
		lda #104+42
		sta $d001
		lda #%01000000
		sta screenMode2::_d01b
		jsrf depack_pray
		setBitBltSrc freeRam+intro_hands_pray_bitmap, freeRam+intro_hands_pray_screen, freeRam+intro_hands_pray_d800, 10
		jsrf transition_sword_to_pray

		lda #0
		sta screenMode2::_d01b
		sta screenMode2::_d015mask

		lda #58
		sta prayYPos

		sec
		ldx #<screenMode3
		ldy #>screenMode3
		jsr setScreenMode
		lda #0
		sta $d015
		jsrf move_pray_sprites
		jsr setup_pray_sprites

		wait 15

		jsrf transition_move_pray

		wait 48

abort:	rts
.endproc

.segment "INTRO4"
hands_sword_gfx:
		.incbin  "../converters/intro/compressed/intro_hands_sword.prg.b2",2
		.include "../converters/intro/compressed/intro_hands_sword.inc"
.proc depack_sword
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<hands_sword_gfx
		ldx #>hands_sword_gfx
		jsr decrunchTo
		ldx #62
		lda #$ff
		:
			sta freeRam+intro_hands_sword_sprites+intro_hands_sword_sprites_size,x
			dex
		bpl :-

		.assert (intro_hands_sword_sprites_size+64) < vic_sprites_size, error, "Too many sprites."
		memcpy vic_sprites, (freeRam+intro_hands_sword_sprites), (intro_hands_sword_sprites_size+64)
		memcpy db_sprites, (freeRam+intro_hands_sword_sprites), (intro_hands_sword_sprites_size+64)
		rts
.endproc

hands_pray_gfx:
		.incbin  "../converters/intro/compressed/intro_hands_pray.prg.b2",2
		.include "../converters/intro/compressed/intro_hands_pray.inc"
.proc depack_pray
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<hands_pray_gfx
		ldx #>hands_pray_gfx
		jsr decrunchTo
		rts
.endproc
.proc move_pray_sprites
		.assert intro_hands_pray_sprites_size < vic_sprites_size, error, "Too many sprites."
		memcpy vic_sprites, (freeRam+intro_hands_pray_sprites), (intro_hands_pray_sprites_size)
		memcpy db_sprites, (freeRam+intro_hands_pray_sprites), (intro_hands_pray_sprites_size)
		rts
.endproc

.segment "INTRO3"
.include "transition_macros.inc"

.export playTransition
.proc playTransition
		stx TMP+0
		sty TMP+1
		loop:
			ldy #0
repeat:		lda (TMP),y
			bpl :+
				rts
			:
			bne :+
				; begin
				lda #2
				sta COUNT
				lda TMP+0
				sta TMP2+0
				lda TMP+1
				sta TMP2+1
				jmp next
			:
			cmp #1
			bne :+
				; bitblt
				iny
				lda (TMP),y;#sx
				sta BLTSX
				iny
				lda (TMP),y;#sy
				sta BLTSY
				iny
				lda (TMP),y;#dx
				sta BLTDX
				iny
				lda (TMP),y;#dy
				sta BLTDY
				iny
				lda (TMP),y;#w
				sta BLTW
				iny
				lda (TMP),y;#h
				sta BLTH
				tya
				pha
				jsr bitblt
				pla
				tay
				jmp next
			:
			cmp #2
			bne :+
				; vclear
				iny
				tya
				pha
				lda (TMP),y;#col
				tax
				jsr vclear
				pla
				tay
				jmp next
			:
			cmp #3
			bne :+
				; hclear
				iny
				tya
				pha
				lda (TMP),y;#row
				tay
				jsr hclear
				pla
				tay
				jmp next
			:
			cmp #4
			bne :+
				iny
				lda (TMP),y;#dx
				sta BLTDX
				iny
				lda (TMP),y;#dy
				sta BLTDY
				iny
				lda (TMP),y;#w
				sta BLTW
				iny
				lda (TMP),y;#h
				sta BLTH
				tya
				pha
				jsr fill
				pla
				tay				
				jmp next
			:
			cmp #5
			bne :+
				iny
				lda (TMP),y;#knife x lo
				sta spritePos_latch+0
				iny
				lda (TMP),y;#knife x hi
				sta spritePos_latch+1
				jmp next
			:
			cmp #6
			bne :+
				iny
				lda (TMP),y
				sta $d01b
				sta screenMode2::_d01b
				jmp next
			:
			cmp #7
			bne :+
				iny
				lda (TMP),y;#sword x lo
				sta spritePos_latch+0
				iny
				lda (TMP),y;#sword x hi
				sta spritePos_latch+1
				iny
				lda (TMP),y;#sword y
				sta swordYPos_latch
				jmp next
			:
			cmp #8
			bne :+
				iny
				lda (TMP),y;#pray y
				sta prayYPos
				lda #3
				jsr wait
				jmp next
			:
			cmp #9
			bne skip9
				ldx system
				cpx #SYSTEM_PAL
				bne :+
					jsr fetchTimer
					iny
					iny
					iny
					iny
					jmp cont9
				:
				cpx #SYSTEM_NTSC_OLD
				bne :+
					iny
					iny
					jsr fetchTimer
					iny
					iny
					jmp cont9
				:
				iny
				iny
				iny
				iny
				jsr fetchTimer

cont9:
				iny
				lda (TMP),y;#mask x
				sta maskX_latch
				iny
				lda (TMP),y;#mask y
				sta maskY_latch
				jmp next
fetchTimer:
				iny
				lda (TMP),y;#timer y lo
				sta timerLo_latch
				iny
				lda (TMP),y;#timer y hi
				sta timerHi_latch
				rts
			skip9:
			cmp #$a
			bne :+
				dec COUNT
				beq next

				jsr swap

				lda TMP2+0
				sta TMP+0
				lda TMP2+1
				sta TMP+1

				ldy #1
				jmp repeat
			:
			cmp #$b
			bne :+
				; bitblt2
				iny
				lda (TMP),y;#sx
				sta BLTSX
				iny
				lda (TMP),y;#sy
				sta BLTSY
				iny
				lda (TMP),y;#dx
				sta BLTDX
				iny
				lda (TMP),y;#dy
				sta BLTDY
				iny
				lda (TMP),y;#w
				sta BLTW
				iny
				lda (TMP),y;#h
				sta BLTH
				tya
				pha
				jsr bitbltNoDblDuffer
				pla
				tay
				jmp next
			:
			cmp #$c
			bne :+
				; wait
				iny
				lda (TMP),y;#amount
				jsr wait
				jmp next
			:
			inc $d020
			jmp *-3

			next:
			iny
			tya
			clc
			adc TMP+0
			sta TMP+0
			bcc :+
				inc TMP+1
			:
		jmp loop
.endproc

.proc transition_show_knife
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		_sprites 24+285

		.include "transition1.inc"
		_done
.endproc

.proc transition_move_knife
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		_prio 0

		_begin
		_bitblt 2,0, 18,4, 19,8
		_sprites 24+285-8*1
		_end

		_begin
		_bitblt 1,0, 18,4, 20,8
		_sprites 24+285-8*2-1
		_end

		_begin
		_bitblt 0,0, 18,4, 21,8
		_sprites 24+285-8*3-2
		_end

		_begin
		_bitblt 0,0, 19,4, 20,8
		_fill 18,4,1,8
		_sprites 24+285-8*4-2
		_end

		_begin
		_bitblt 0,0, 19,4, 20,8
		_sprites 24+285-8*5-3
		_end

		_begin
		_bitblt 0,0, 19,4, 20,8
		_sprites 24+285-8*6-3
		_end

		_begin
		_bitblt 0,0, 19,4, 20,8
		_sprites 24+285-8*7-4
		_end

		_done
.endproc

.proc transition_hide_knife
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		_begin
		_fill 29,4,10,8
		_fill 28,7,1,1
		_end
		_prio $7e

		_done
.endproc

.proc transition_knife_to_spellbook
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		.include "transition2.inc"
		_done
.endproc

.proc transition_take_spellbook
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		_begin
		_bitblt 0,9, 10,12, 16,7
		_end

		_begin
		_bitblt 0,8, 10,12, 16,8
		_end

		_begin
		_bitblt 0,7, 10,12, 16,8
		_end

		_begin
		_bitblt 0,6, 10,12, 16,8
		_end

		_begin
		_bitblt 0,5, 10,12, 16,8
		_end

		_begin
		_bitblt 0,4, 10,12, 16,8
		_end

		_begin
		_bitblt 0,3, 10,12, 16,8
		_end

		_begin
		_bitblt 0,2, 10,12, 16,8
		_end

		_begin
		_bitblt 0,1, 10,12, 16,8
		_end

		_begin
		_bitblt 0,0, 10,12, 16,8
		_end

		_done
.endproc

.proc transition_show_spellbook
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		_begin
		_bitblt 0,1, 10,12, 16,7
		_bitblt 0,18,10,19, 16,1
		_end

		_begin
		_bitblt 0,2, 10,12, 16,6
		_bitblt 0,18,10,18, 16,2
		_end

		_begin
		_bitblt 0,3, 10,12, 16,5
		_bitblt 0,18,10,17, 16,3
		_end

		_begin
		_bitblt 0,4, 10,12, 16,4
		_bitblt 0,18,10,16, 16,4
		_end

		_begin
		_bitblt 0,5, 10,12, 16,3
		_bitblt 0,18,10,15, 16,5
		_end

		_begin
		_bitblt 0,6, 10,12, 16,2
		_bitblt 0,18,10,14, 16,6
		_end

		_begin
		_bitblt 0,7, 10,12, 16,1
		_bitblt 0,18,10,13, 16,7
		_end

		_done
.endproc

.proc transition_spellbook_to_sword
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		.include "transition3.inc"
		_done
.endproc

.proc transition_move_sword
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		_begin
		_bitblt 6,6, 0,0, 12,11 ;18,12
		_end

		_begin
		_bitblt 5,5, 0,0, 13,12 ;18,12
		_end

		_begin
		_bitblt 4,4, 0,0, 14,12 ;18,12
		_end

		_begin
		_bitblt 3,3, 0,0, 15,12 ;18,12
		_end

		_begin
		_bitblt 2,2, 0,0, 16,12 ;18,12
		_end

		_sword 70+0*8+5,$53+0*8+5
		_begin
		_bitblt 1,1, 0,0, 17,12 ;18,12
		_fill 15,10,3,2
		_end

		_done
.endproc

.proc transition_sword_to_pray
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		.include "transition4.inc"
		_done
.endproc

.proc transition_move_pray
		ldx #<:+
		ldy #>:+
		jmp playTransition
		:

		_begin
		_bitblt 0,1, 16,6, 10,14 
		_end

		_begin
		_bitblt 0,2, 16,6, 10,14 
		_end

		_begin
		_bitblt 0,3, 16,6, 10,14 
		_end

		_begin
		_bitblt 0,4, 16,6, 10,14 
		_end

		_begin
		_bitblt 0,5, 16,6, 10,14 
		_end

		_begin
		_bitblt 0,6, 16,6, 10,14 
		_end

		_begin
		_fill 16,6,10,5
		_end

		.repeat 10,I
			_pray 58+(I+1)*4
		.endrep

		_done
.endproc

.proc swap
		jsr pace

		lda spritePos_latch+0
		sta spritePos+0
		lda spritePos_latch+1
		sta spritePos+1
		lda swordYPos_latch
		sta swordYPos
		lda timerLo_latch
		sta screenMode2::resetMultiplexer::timerLo+1
		lda timerHi_latch
		sta screenMode2::resetMultiplexer::timerHi+1
		lda maskX_latch
		sta $d000
		lda maskY_latch
		sta $d001

		lda screenMode::_dd00
		eor #($fc^$fe)
		sta screenMode::_dd00
		and #3
		beq :+
			setBitBltDst vic_bitmap, vic_screen
			jmp :++
		:
			setBitBltDst db_bitmap, db_screen
		:

		ldx #0
		.repeat 3,I
			:
				.repeat 16
					lda db_d800+I*$100,x
					sta $d800+I*$100,x
					inx
				.endrep
			bne :-
		.endrep
		:
			lda db_d800+$300,x
			sta $d800+$300,x
			inx
			cpx #72
		bne :-


		rts
.endproc

.proc pace
;		lda #30
;		jsr wait
		lda frameCounter
		:cmp frameCounter
		beq :-
		rts
.endproc
