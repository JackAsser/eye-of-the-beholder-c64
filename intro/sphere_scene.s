;text_template	= $5000
;text_screen0	= $8000
;text_screen1	= $8400
;text_font		= $8800

; Bank 1
zoom_font_1_2	= $4000
zoom_font_3_4	= $4800
tower_screen 	= $5400
zoom_screen_1	= $5400
fade_d800 		= $5800
zoom_screen_2	= $5800
zoom_screen_3	= $5c00
zoom_screen_4	= $6000
sphere_frame_3	= $6400 ;..$74ff
tower_font2		= $7800

; Bank 2
zoom_font_5		= $a000
zoom_screen_5	= $a800
sphere_frame_2	= $ac00 ;..$bebf

; Bank 3
zoom_screen_6	= $c000
.export zoom_screen_7
zoom_screen_7	= $c400
.export zoom_screen_8
zoom_screen_8	= $c800
.export zoom_screen_9
zoom_screen_9	= $cc00
.export zoom_font_8_9
zoom_font_6_7	= $d000
zoom_font_8_9	= $d800
sphere_frame_1	= $e000 ;..$f1ff
sphere_frame_0	= $f200 ;..$f53f

backup			= $3c00

tower_font		= $c800 ;In memory since previous scene

.include "../global.inc"
.include "bitblt.inc"

.segment "SPHEREMAPPINGS"
sphere_frame_0_mapping:
	.incbin "../converters/intro/converted/intro_sphere_0_sprites.map"
sphere_frame_1_mapping:
	.incbin "../converters/intro/converted/intro_sphere_1_sprites.map"
sphere_frame_2_mapping:
	.incbin "../converters/intro/converted/intro_sphere_2_sprites.map"
sphere_frame_3_mapping:
	.incbin "../converters/intro/converted/intro_sphere_3_sprites.map"

sphere_frame_0b_mapping:
	.incbin "../converters/intro/converted/intro_sphere_0b_sprites.map"
sphere_frame_1b_mapping:
	.incbin "../converters/intro/converted/intro_sphere_1b_sprites.map"
sphere_frame_2b_mapping:
	.incbin "../converters/intro/converted/intro_sphere_2b_sprites.map"
sphere_frame_3b_mapping:
	.incbin "../converters/intro/converted/intro_sphere_3b_sprites.map"

.segment "INTRO_RAM"
sprmapping_lo:
	.byte <sphere_frame_3_mapping
	.byte <sphere_frame_3_mapping
	.byte <sphere_frame_3_mapping
	.byte <sphere_frame_3_mapping
	.byte <sphere_frame_3_mapping
	.byte <sphere_frame_2_mapping
	.byte <sphere_frame_1_mapping
	.byte <sphere_frame_0_mapping
	.byte <sphere_frame_0_mapping
	.byte <sphere_frame_0_mapping
	; Tunnel sphere
	.byte <sphere_frame_0b_mapping
	.byte <sphere_frame_1b_mapping
	.byte <sphere_frame_2b_mapping
	.byte <sphere_frame_3b_mapping

sprmapping_hi:
	.byte >sphere_frame_3_mapping
	.byte >sphere_frame_3_mapping
	.byte >sphere_frame_3_mapping
	.byte >sphere_frame_3_mapping
	.byte >sphere_frame_3_mapping
	.byte >sphere_frame_2_mapping
	.byte >sphere_frame_1_mapping
	.byte >sphere_frame_0_mapping
	.byte >sphere_frame_0_mapping
	.byte >sphere_frame_0_mapping
	; Tunnel sphere
	.byte >sphere_frame_0b_mapping
	.byte >sphere_frame_1b_mapping
	.byte >sphere_frame_2b_mapping
	.byte >sphere_frame_3b_mapping

sprptrs_lo:
	.byte <(tower_screen+$3f8)
	.byte <(zoom_screen_1+$3f8)
	.byte <(zoom_screen_2+$3f8)
	.byte <(zoom_screen_3+$3f8)
	.byte <(zoom_screen_4+$3f8)
	.byte <(zoom_screen_5+$3f8)
	.byte <(zoom_screen_6+$3f8)
	.byte <(zoom_screen_7+$3f8)
	.byte <(zoom_screen_8+$3f8)
	.byte <(zoom_screen_9+$3f8)
	; Tunnel sphere
	.byte <(intro_rockfall_f0_f1_screen+$3f8)
	.byte <(intro_rockfall_f0_f1_screen+$3f8)
	.byte <(intro_rockfall_f2_screen+$3f8)
	.byte <(intro_rockfall_f3_screen+$3f8)

sprptrs_hi:
	.byte >(tower_screen+$3f8)
	.byte >(zoom_screen_1+$3f8)
	.byte >(zoom_screen_2+$3f8)
	.byte >(zoom_screen_3+$3f8)
	.byte >(zoom_screen_4+$3f8)
	.byte >(zoom_screen_5+$3f8)
	.byte >(zoom_screen_6+$3f8)
	.byte >(zoom_screen_7+$3f8)
	.byte >(zoom_screen_8+$3f8)
	.byte >(zoom_screen_9+$3f8)
	; Tunnel sphere
	.byte >(intro_rockfall_f0_f1_screen+$3f8)
	.byte >(intro_rockfall_f0_f1_screen+$3f8)
	.byte >(intro_rockfall_f2_screen+$3f8)
	.byte >(intro_rockfall_f3_screen+$3f8)

d015tab:
	.byte $ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$00,$00
	; Tunnel sphere
	.byte $ff,$ff,$ff,$ff

d018tab:
	CALC_D018 tower_font2, tower_screen
	CALC_D018 zoom_font_1_2, zoom_screen_1
	CALC_D018 zoom_font_1_2, zoom_screen_2
	CALC_D018 zoom_font_3_4, zoom_screen_3
	CALC_D018 zoom_font_3_4, zoom_screen_4
	CALC_D018 zoom_font_5, zoom_screen_5
	CALC_D018 zoom_font_6_7, zoom_screen_6
	CALC_D018 zoom_font_6_7, zoom_screen_7
	CALC_D018 zoom_font_8_9, zoom_screen_8
	CALC_D018 zoom_font_8_9, zoom_screen_9
	; Tunnel sphere
	CALC_D018 intro_rockfall_f0_f1_font, intro_rockfall_f0_f1_screen
	CALC_D018 intro_rockfall_f0_f1_font, intro_rockfall_f0_f1_screen
	CALC_D018 intro_rockfall_f2_font, intro_rockfall_f2_screen
	CALC_D018 intro_rockfall_f3_font, intro_rockfall_f3_screen

dd00tab:
	CALC_DD00 tower_font2
	CALC_DD00 zoom_font_1_2
	CALC_DD00 zoom_font_1_2
	CALC_DD00 zoom_font_3_4
	CALC_DD00 zoom_font_3_4
	CALC_DD00 zoom_font_5
	CALC_DD00 zoom_font_6_7
	CALC_DD00 zoom_font_6_7
	CALC_DD00 zoom_font_8_9
	CALC_DD00 zoom_font_8_9
	; Tunnel sphere
	CALC_DD00 intro_rockfall_f0_f1_font
	CALC_DD00 intro_rockfall_f0_f1_font
	CALC_DD00 intro_rockfall_f2_font
	CALC_DD00 intro_rockfall_f3_font

d021tab:
	.byte $0,$b,$9,$9,$9,$9,$9,$9,$9,$9
	; Tunnel sphere
	.byte $9,$9,$9,$9

d022tab:
	.byte $8,$8,$8,$8,$8,$8,$8,$8,$8,$8
	; Tunnel sphere
	.byte $0,$0,$0,$0

d023tab:
	.byte $e,$e,$e,$c,$f,$f,$f,$f,$f,$f
	; Tunnel sphere
	.byte $c,$c,$c,$c

frame: .byte 0
.export animationFrame=frame
.export sphere_d021tab=d021tab
.export sphere_d022tab=d022tab
.export sphere_d023tab=d023tab

spriteYpos = $32

.export screenModeSphere=screenMode
.proc screenMode
	.word init
	.word teardown
	.byte 18
	.word $20, top
	.word spriteYpos+0*21+11, multiplexSprites
	.word spriteYpos+1*21+0, updateSpritePointers
	.word spriteYpos+1*21+11, multiplexSprites
	.word spriteYpos+2*21+0, inc_d02c
	.word spriteYpos+2*21+11, multiplexSprites
	.word spriteYpos+3*21+0, updateSpritePointers
	.word spriteYpos+3*21+11, multiplexSprites
	.word spriteYpos+4*21+0, updateSpritePointers
	.word spriteYpos+4*21+11, multiplexSprites
	.word spriteYpos+5*21+0, dec_d02c
	.word spriteYpos+5*21+11, multiplexSprites
	.word spriteYpos+6*21+0, updateSpritePointers
	.word spriteYpos+6*21+11, multiplexSprites
	.word spriteYpos+7*21+0, updateSpritePointers
	.word spriteYpos+7*21+11, multiplexSprites
	.word spriteYpos+8*21+0, updateSpritePointers
	.word $e8, bottom

	.proc dec_d02c
		lda #0
		sta $d02c
		jmp updateSpritePointers
	.endproc
	.proc inc_d02c
_d02c:	lda #1
		sta $d02c
	.endproc
	.proc updateSpritePointers
		lda #<.bank(sphere_frame_0_mapping)
		sta $de00
plexRound:
		ldy #0
sm0:	lda $dead,y
sm1:	sta $dead
		lda $dead,y
		sta $dead
		lda $dead,y
		sta $dead
		lda $dead,y
		sta $dead
		lda $dead,y
		sta $dead
		lda $dead,y
		sta $dead
		lda $dead,y
		sta $dead
		lda $dead,y
		sta $dead
		inc plexRound+1
		lda BANK
		sta $de00
		jmp exitIrq_quick
	.endproc

	.proc top
		lda #$1b
		sta $d011
		lda $dd00
		and #$fc
		ldx frame
		ora dd00tab,x
		sta $dd00
		lda d018tab,x
		sta $d018
		lda #$18
		sta $d016
		lda d021tab,x
		sta $d021
		lda d022tab,x
		sta $d022
		lda d023tab,x
		sta $d023
		lda d015tab,x
		sta $d015

_d025:	lda #$06
		sta $d025
_d026:	lda #$0e
		sta $d026
		ldx #7
		lda #0
		:
			sta $d027,x
			dex
		bpl :-
		lda #0
		sta $d02c
		sta updateSpritePointers::plexRound+1

		ldx frame

		clc
		ldy sprmapping_hi,x
		lda sprmapping_lo,x

		.repeat 8,I
			sty updateSpritePointers::sm0+2+6*I
			sta updateSpritePointers::sm0+1+6*I
			adc #17
			bcc :+
				iny
				clc
			:
		.endrep

		ldy sprptrs_lo,x
		sty updateSpritePointers::sm1+1+6*0
		iny
		sty updateSpritePointers::sm1+1+6*1
		iny
		sty updateSpritePointers::sm1+1+6*2
		iny
		sty updateSpritePointers::sm1+1+6*3
		iny
		sty updateSpritePointers::sm1+1+6*4
		iny
		sty updateSpritePointers::sm1+1+6*5
		iny
		sty updateSpritePointers::sm1+1+6*6
		iny
		sty updateSpritePointers::sm1+1+6*7
		lda sprptrs_hi,x
		sta updateSpritePointers::sm1+2+6*0
		sta updateSpritePointers::sm1+2+6*1
		sta updateSpritePointers::sm1+2+6*2
		sta updateSpritePointers::sm1+2+6*3
		sta updateSpritePointers::sm1+2+6*4
		sta updateSpritePointers::sm1+2+6*5
		sta updateSpritePointers::sm1+2+6*6
		sta updateSpritePointers::sm1+2+6*7
		lda #spriteYpos
		jsr setSpriteY
		jmp updateSpritePointers
	.endproc

	.proc multiplexSprites
		lda $d001
		clc
		adc #21
		jsr setSpriteY
		jmp updateSpritePointers
	.endproc

	.proc setSpriteY
		sta $d001
		sta $d003
		sta $d005
		sta $d007
		sta $d009
		sta $d00b
		sta $d00d
		sta $d00f
		rts
	.endproc

	.proc bottom
		lda $d021
		and #$f
		beq :+
			lda #0
			sta $d021
		:
		lda $dd00
		and #$fc
		ora #$01
		sta $dd00
		lda #$02
		sta $d018
		lda #$08
		sta $d016
		jmp exitIrq
	.endproc

	.proc init
		rts
	.endproc

	.proc teardown
		lda #0
		sta $d011
		sta $d015
		lda $dd00
		and #$fc
		sta $dd00
		rts
	.endproc
.endproc

.export sphere_d025 = screenMode::top::_d025+1
.export sphere_d026 = screenMode::top::_d026+1
.export sphere_d02c = screenMode::inc_d02c::_d02c+1

.segment "INTRO4"
intro_cityzoom_01_02_gfx:
		.incbin  "../converters/intro/compressed/intro_cityzoom_01-02.prg.b2",2
		.include "../converters/intro/compressed/intro_cityzoom_01-02.inc"
.export intro_sphere_0_gfx
intro_sphere_0_gfx:
		.incbin "../converters/intro/compressed/intro_sphere_0_sprites.prg.b2",2
		.include "../converters/intro/compressed/intro_sphere_0_sprites.inc"
.segment "INTRO6"
intro_cityzoom_05_gfx:
		.incbin  "../converters/intro/compressed/intro_cityzoom_05.prg.b2",2
		.include "../converters/intro/compressed/intro_cityzoom_05.inc"
.segment "INTRO8"
intro_cityzoom_03_04_gfx:
		.incbin  "../converters/intro/compressed/intro_cityzoom_03-04.prg.b2",2
		.include "../converters/intro/compressed/intro_cityzoom_03-04.inc"
.export intro_sphere_3_gfx
intro_sphere_3_gfx:
		.incbin "../converters/intro/compressed/intro_sphere_3_sprites.prg.b2",2
		.include "../converters/intro/compressed/intro_sphere_3_sprites.inc"
.segment "INTRO7"
intro_cityzoom_06_07_gfx:
		.incbin  "../converters/intro/compressed/intro_cityzoom_06-07.prg.b2",2
		.include "../converters/intro/compressed/intro_cityzoom_06-07.inc"
.segment "INTRO9"
.export intro_sphere_1_gfx
intro_sphere_1_gfx:
		.incbin "../converters/intro/compressed/intro_sphere_1_sprites.prg.b2",2
		.include "../converters/intro/compressed/intro_sphere_1_sprites.inc"
.export intro_sphere_2_gfx
intro_sphere_2_gfx:
		.incbin "../converters/intro/compressed/intro_sphere_2_sprites.prg.b2",2
		.include "../converters/intro/compressed/intro_sphere_2_sprites.inc"

.segment "INTRO10"
intro_cityzoom_08_09_gfx:
		.incbin  "../converters/intro/compressed/intro_cityzoom_08-09.prg.b2",2
		.include "../converters/intro/compressed/intro_cityzoom_08-09.inc"
intro_cityzoom_d800_gfx:
		.incbin  "../converters/intro/compressed/intro_cityzoom_d800.prg.b2",2
		.include "../converters/intro/compressed/intro_cityzoom_d800.inc"

.segment "INTRO"
.include "../converters/intro/compressed/intro_tower_magecircle.inc"

.macro blitCityZoomFrame dst, src
		lda #<(dst+3*40+10)
		sta DST+0
		lda #>(dst+3*40+10)
		sta DST+1
		lda #<(src)
		sta SRC+0
		lda #>(src)
		sta SRC+1
		jsr blitCityZoomFrameHelper
.endmacro

.export fetchWaterdeep
.proc fetchWaterdeep
		memset zoom_screen_8, 0, 1000
		jsr decrunchCityFramesFinalFrames
		jsr decrunchCityFrames2
		blitCityZoomFrame $d800, freeRam+7*340
		rts
.endproc

.export setupSphereSprites 
.proc setupSphereSprites
		; All 8 sprites multicolor, non expanded, ontop, centered
		lda #$00
		sta $d017
		sta $d01b
		sta $d01d
		sta $d015
		lda #$ff
		sta $d01c

		; Place overlay
		ldx #7
		ldy #0
		clc
		lda #24+8+(320-24*8)/2
		:
			sta $d000,y
			adc #24
			iny
			iny
			dex
		bpl :-
		lda #$80
		sta $d010
		rts
.endproc

.export sphere_scene
.proc sphere_scene
		; jsr fetchWaterdeep

		jsr setupSphereSprites

		memset $d800, 0, 1000
		memcpy tower_font2, tower_font, $800
		memcpy (fade_d800+3*18), (freeRam+intro_tower_magecircle_d800), 1000
		memset zoom_screen_1, 0, 1000
		memset zoom_screen_3, 0, 1000
		memset zoom_screen_4, 0, 1000
		memset zoom_screen_5, 0, 1000
		memset zoom_screen_6, 0, 1000
		memset zoom_screen_7, 0, 1000
		memset zoom_screen_8, 0, 1000
		memset zoom_screen_9, 0, 1000

		; Blit mage circle again, but this time the full graphics
		lda #<(tower_screen+11+6*40)
		sta DST_SCREEN+0
		sta TMP3+0
		lda #>(tower_screen+11+6*40)
		sta DST_SCREEN+1
		lda #$00
		sta $d022
		sta $d023
		lda #12
		sta TMP

		ldx #0
		rowLoop:
			ldy #0
			:
				lda freeRam+intro_tower_magecircle_screen,x
				sta (DST_SCREEN),y
				inx
				iny
				cpy #18
			bne :-

			clc
			lda DST_SCREEN+0
			adc #40
			sta DST_SCREEN+0
			bcc :+
				inc DST_SCREEN+1
			:
			dec TMP
		bne rowLoop

		jsr decrunchSphere

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

		jsr fadeIn
		bcs abort

		lda #10
		jsr wait

		lda #2
		ldx #4
		ldy #1
		jsr showText

		jsr decrunchCityFrames

		jsr fadeOut
		bcs abort

		memset $d800,0,(23*40)

		jsr decrunchCityFrames2

		lda #2
		ldx #5
		ldy #1
		jsr showText

		lda #0
		sta d021tab+1
		sta d022tab+1
		sta d023tab+1
		lda #1
		sta frame
		jsr fadeIn
		bcs abort

		wait 50

		memcpy zoom_screen_2, backup, 1000

		jsr doZoom

abort:
		rts
.endproc

.proc doZoom
		wait 2
		lda #1
		sta frame
		blitCityZoomFrame $d800, freeRam+0*340
		wait 2
		inc frame
		blitCityZoomFrame $d800, freeRam+1*340
		wait 2
		inc frame
		blitCityZoomFrame $d800, freeRam+2*340
		wait 2
		inc frame
		blitCityZoomFrame $d800, freeRam+3*340
		wait 2
		inc frame
		blitCityZoomFrame $d800, freeRam+4*340
		wait 2
		inc frame
		blitCityZoomFrame $d800, freeRam+5*340
		wait 2
		inc frame
		blitCityZoomFrame $d800, freeRam+6*340
		wait 2
		inc frame
		blitCityZoomFrame $d800, freeRam+7*340

		lda #0
		ldx #5
		ldy #1
		jsr showText
abort:
		rts
.endproc

.proc decrunchSphere
		lda #<sphere_frame_0
		sta BB_DST+0
		lda #>sphere_frame_0
		sta BB_DST+1
		lda #<.bank(intro_sphere_0_gfx)
		ldy #<intro_sphere_0_gfx
		ldx #>intro_sphere_0_gfx
		jsr decrunchFar

		lda #<sphere_frame_1
		sta BB_DST+0
		lda #>sphere_frame_1
		sta BB_DST+1
		lda #<.bank(intro_sphere_1_gfx)
		ldy #<intro_sphere_1_gfx
		ldx #>intro_sphere_1_gfx
		jsr decrunchFar

		lda #<sphere_frame_2
		sta BB_DST+0
		lda #>sphere_frame_2
		sta BB_DST+1
		lda #<.bank(intro_sphere_2_gfx)
		ldy #<intro_sphere_2_gfx
		ldx #>intro_sphere_2_gfx
		jsr decrunchFar

		lda #<sphere_frame_3
		sta BB_DST+0
		lda #>sphere_frame_3
		sta BB_DST+1
		lda #<.bank(intro_sphere_3_gfx)
		ldy #<intro_sphere_3_gfx
		ldx #>intro_sphere_3_gfx
		jsr decrunchFar

		rts
.endproc

.proc decrunchCityFrames
		lda #<.bank(intro_cityzoom_03_04_gfx)
		ldy #<intro_cityzoom_03_04_gfx
		ldx #>intro_cityzoom_03_04_gfx
		jsr decrunchFarToFreeRam
		memcpy zoom_font_3_4, freeRam+intro_cityzoom_03_04_charset, intro_cityzoom_03_04_charset_size
		blitCityZoomFrame zoom_screen_3, freeRam+intro_cityzoom_03_04_screen
		blitCityZoomFrame zoom_screen_4, freeRam+intro_cityzoom_03_04_screen+340

		lda #<.bank(intro_cityzoom_05_gfx)
		ldy #<intro_cityzoom_05_gfx
		ldx #>intro_cityzoom_05_gfx
		jsr decrunchFarToFreeRam
		memcpy zoom_font_5, freeRam+intro_cityzoom_05_charset, intro_cityzoom_05_charset_size
		blitCityZoomFrame zoom_screen_5, freeRam+intro_cityzoom_05_screen

		lda #<.bank(intro_cityzoom_06_07_gfx)
		ldy #<intro_cityzoom_06_07_gfx
		ldx #>intro_cityzoom_06_07_gfx
		jsr decrunchFarToFreeRam
		memcpy_underio zoom_font_6_7, freeRam+intro_cityzoom_06_07_charset, intro_cityzoom_06_07_charset_size
		blitCityZoomFrame zoom_screen_6, freeRam+intro_cityzoom_06_07_screen
		blitCityZoomFrame zoom_screen_7, freeRam+intro_cityzoom_06_07_screen+340
.endproc

.proc decrunchCityFramesFinalFrames
		lda #<.bank(intro_cityzoom_08_09_gfx)
		ldy #<intro_cityzoom_08_09_gfx
		ldx #>intro_cityzoom_08_09_gfx
		jsr decrunchFarToFreeRam
		memcpy_underio zoom_font_8_9, freeRam+intro_cityzoom_08_09_charset, intro_cityzoom_08_09_charset_size
		blitCityZoomFrame zoom_screen_8, freeRam+intro_cityzoom_08_09_screen
		blitCityZoomFrame zoom_screen_9, freeRam+intro_cityzoom_08_09_screen+340

		rts
.endproc

.proc decrunchCityFrames2
		lda #<.bank(intro_cityzoom_01_02_gfx)
		ldy #<intro_cityzoom_01_02_gfx
		ldx #>intro_cityzoom_01_02_gfx
		jsr decrunchFarToFreeRam
		memcpy zoom_font_1_2, freeRam+intro_cityzoom_01_02_charset, intro_cityzoom_01_02_charset_size
		blitCityZoomFrame zoom_screen_1, freeRam+intro_cityzoom_01_02_screen
		memset backup, 0, 1000
		blitCityZoomFrame backup, freeRam+intro_cityzoom_01_02_screen+340

		lda #<.bank(intro_cityzoom_d800_gfx)
		ldy #<intro_cityzoom_d800_gfx
		ldx #>intro_cityzoom_d800_gfx
		jsr decrunchFarToFreeRam
		ldx #17
		:
			.repeat 17,I
				lda freeRam+1+I*20,x
				sta fade_d800+I*18,x
			.endrep
			dex
		bpl :-

		rts
.endproc

.pushseg
.segment "INTRO_RAM"
.proc blitCityZoomFrameHelper
		ldx #17
		rowLoop:
			ldy #19
			colLoop:
				lda (SRC),y
				sta (DST),y
				dey
				lda (SRC),y
				sta (DST),y
				dey
				lda (SRC),y
				sta (DST),y
				dey
				lda (SRC),y
				sta (DST),y
				dey
				lda (SRC),y
				sta (DST),y
				dey
			bpl colLoop
			clc
			lda SRC+0
			adc #20
			sta SRC+0
			bcc :+
				inc SRC+1
				clc
			:
			lda DST+0
			adc #40
			sta DST+0
			bcc :+
				inc DST+1
			:
			dex
		bne rowLoop
		rts
.endproc
.export blitTunnelFrameHelper 
.proc blitTunnelFrameHelper
		ldx #15
		bne blitCityZoomFrameHelper::rowLoop
.endproc
.popseg

.proc fadeOut
		ldx #0
		:
			txa
			pha
			jsr doFade
			lda #5
			jsr waitWithAbort
			beq :+
				pla
				sec
				rts
			:
			pla
			tax
			inx
			cpx #8
		bne :--
		clc
		rts
.endproc

.proc fadeIn
		ldx #6
		:
			lda #5
			jsr waitWithAbort
			beq :+
				sec
				rts
			:
			txa
			pha
			jsr doFade
			pla
			tax
			dex
		bpl :--
		clc
		rts
.endproc

.proc doFade
		txa
		tay
		asl
		asl
		asl
		asl
		adc #<fadeTable
		sta TMP2+0
		lda #0
		adc #>fadeTable
		sta TMP2+1
		ldx frame

		lda fadeD021_tab_lo,x
		sta TMP+0
		lda fadeD021_tab_hi,x
		sta TMP+1
		lda (TMP),y
		sta d021tab,x

		lda fadeD022_tab_lo,x
		sta TMP+0
		lda fadeD022_tab_hi,x
		sta TMP+1
		lda (TMP),y
		sta d022tab,x

		lda fadeD023_tab_lo,x
		sta TMP+0
		lda fadeD023_tab_hi,x
		sta TMP+1
		lda (TMP),y
		sta d023tab,x

		lda #<($d800+11+3*40)
		sta DST_D800+0
		lda #>($d800+11+3*40)
		sta DST_D800+1
		
		lda #14
		sta TMP
		ldx #0
		rowLoop2:
			.repeat 18,I
				ldy fade_d800+I,x
				lda (TMP2),y
				ldy #I
				sta (DST_D800),y
			.endrep
			clc
			txa
			adc #18
			tax

			clc
			lda DST_D800+0
			adc #40
			sta DST_D800+0
			bcc :+
				inc DST_D800+1
			:
			dec TMP
			beq :+
		jmp rowLoop2

:
		lda #3
		sta TMP
		ldx #0
		rowLoop3:
			.repeat 18,I
				ldy fade_d800+14*18+I,x
				lda (TMP2),y
				ldy #I
				sta (DST_D800),y
			.endrep
			clc
			txa
			adc #18
			tax

			clc
			lda DST_D800+0
			adc #40
			sta DST_D800+0
			bcc :+
				inc DST_D800+1
			:
			dec TMP
			bne :+
				rts
			:
		jmp rowLoop3

fadeD021a:.byte $0,$0,$0,$0,$0,$0,$0,$0
fadeD022a:.byte $8,$8,$b,$b,$6,$6,$0,$0
fadeD023a:.byte $e,$8,$8,$b,$b,$6,$6,$0

fadeD021b:.byte $b,$b,$b,$9,$9,$9,$9,$0
fadeD022b:.byte $8,$8,$b,$b,$6,$6,$0,$0
fadeD023b:.byte $e,$8,$8,$b,$b,$6,$6,$0

fadeD021_tab_lo: .byte <fadeD021a,<fadeD021b
fadeD021_tab_hi: .byte >fadeD021a,>fadeD021b
fadeD022_tab_lo: .byte <fadeD022a,<fadeD022b
fadeD022_tab_hi: .byte >fadeD022a,>fadeD022b
fadeD023_tab_lo: .byte <fadeD023a,<fadeD023b
fadeD023_tab_hi: .byte >fadeD023a,>fadeD023b

fadeTable:
		.byte $0,$1,$2,$3,$4,$5,$6,$7, $0+8,$1+8,$2+8,$3+8,$4+8,$5+8,$6+8,$7+8
		.byte $0,$7,$2,$3,$4,$5,$6,$3, $0+8,$7+8,$2+8,$3+8,$4+8,$5+8,$6+8,$3+8
		.byte $0,$3,$6,$5,$2,$4,$6,$5, $0+8,$3+8,$6+8,$5+8,$2+8,$4+8,$6+8,$5+8
		.byte $0,$5,$6,$4,$2,$2,$6,$4, $0+8,$5+8,$6+8,$4+8,$2+8,$2+8,$6+8,$4+8
		.byte $0,$4,$6,$4,$2,$2,$0,$2, $0+8,$4+8,$6+8,$4+8,$2+8,$2+8,$0+8,$2+8
		.byte $0,$2,$0,$2,$6,$6,$0,$6, $0+8,$2+8,$0+8,$2+8,$6+8,$6+8,$0+8,$6+8
		.byte $0,$6,$0,$6,$6,$0,$0,$0, $0+8,$6+8,$0+8,$6+8,$6+8,$0+8,$0+8,$0+8
		.byte $0,$0,$0,$0,$0,$0,$0,$0, $0+8,$0+8,$0+8,$0+8,$0+8,$0+8,$0+8,$0+8
.endproc
