SPRITEINDEX=$2f

db_sprites= $4000
db_screen = $4c00
db_bitmap = $6000

.include "../global.inc"

.segment "INTRO_RAM"
frameCount: .byte 0
scrollY: .byte 0
startScroll: .byte 0
.proc screenMode
	.word init
	.word teardown
	.byte 2
	.word $28, toBitmap
textTrigger:
	.word $32+21*8+4, toText

	.proc init
		rts
	.endproc

	.proc teardown
		lda #0
		sta $d011
		sta $d015
		lda $dd00
		and #$fc
		sta $dd00
		rts
	.endproc

	.proc toBitmap
		lda #$ff
		sta $d015
		sta $d01d
		lda #$00
		sta $d01b
		sta $d01c
		sta $d017

		lda #$29
		sta $d001
		sta $d003
		sta $d005
		sta $d007
		lda #$30+21+18*8
		sta $d009
		sta $d00b
		sta $d00d
		sta $d00f

		clc
		lda #$50
		sta $d000
		sta $d008
		adc #24*2
		sta $d002
		sta $d00a
		adc #24*2
		sta $d004
		sta $d00c
		adc #24*2
		sta $d006
		sta $d00e
		lda #0
		sta $d010

		lda #9
		sta $d021
		lda scrollY
		and #7
		eor #7
		pha
		ora #$78
		sta $d011
		lda #$18
		sta $d016
		lda scrollY
		lsr
		lsr
		lsr
		and #1
		tax
		lda $dd00
		and #$fc
		ora _dd00,x
		sta $dd00
		lda #$38
		sta $d018
		pla
		clc
		adc #$32+21*8+3
		sta textTrigger+0

		lda $d011
		and #$3f
		ldx #$38
		:cpx $d012
		bne :-
		sta $d011

		jmp exitIrq
_dd00:	.byte $00,$02
	.endproc

	.proc toText
		nop
		lda #0
		sta $d021
		lda #$1b
		sta $d011
		lda $dd00
		and #$fc
		ora #1
		sta $dd00
		lda scrollY
		and #7
		eor #7
		tax
		lda _d018,x
		sta $d018
		lda #$08
		sta $d016
		inc frameCount

		lda startScroll
		beq :+
			lda frameCount
			and #1
			bne :+
				ldx scrollY
				inx
				stx scrollY
				cpx #105
				bne :+
					lda #0
					sta startScroll
		:

		jmp exitIrq_quick
_d018:	.byte $02,$02,$02,$02,$12,$12,$12,$12
	.endproc
.endproc

.export textScreen1OneUp
.proc textScreen1OneUp
		dec 1
		ldx #39
		:
			lda text_screen0+21*40,x
			sta text_screen1+20*40,x
			lda text_screen0+22*40,x
			sta text_screen1+21*40,x
			lda text_screen0+23*40,x
			sta text_screen1+22*40,x
			lda text_screen0+24*40,x
			sta text_screen1+23*40,x
			lda #1
			sta $d800+22*40,x
			lda #3
			sta text_screen0+22*40,x
			dex
		bpl :-
		inc 1
		rts
.endproc

.proc scrollD800
		lda #<($d800+10+2*40)
		sta sm0+1
		lda #>($d800+10+2*40)
		sta sm0+2
		lda #<($d800+10+1*40)
		sta sm1+1
		lda #>($d800+10+1*40)
		sta sm1+2
		ldy #19
		rowLoop:
			ldx #19
			:
sm0:			lda $d800,x
sm1:			sta $d800,x
				dex
			bpl :-
			clc
			lda sm0+1
			adc #40
			sta sm0+1
			bcc :+
				inc sm0+2
			:
			clc
			lda sm1+1
			adc #40
			sta sm1+1
			bcc :+
				inc sm1+2
			:
			dey
		bpl rowLoop
		rts
.endproc

.proc scroll
		lda #$35
		sta 1

		lda scrollY
		lsr
		lsr
		lsr
		and #1
		bne :+
			lda #<(vic_bitmap+10*8+2*320)
			sta sm0+1
			lda #>(vic_bitmap+10*8+2*320)
			sta sm0+2
			lda #<(db_bitmap+10*8+1*320)
			sta sm1+1
			lda #>(db_bitmap+10*8+1*320)
			sta sm1+2
			lda #<(vic_bitmap+10*8+2*320+10*8)
			sta sm2+1
			lda #>(vic_bitmap+10*8+2*320+10*8)
			sta sm2+2
			lda #<(db_bitmap+10*8+1*320+10*8)
			sta sm3+1
			lda #>(db_bitmap+10*8+1*320+10*8)
			sta sm3+2

			lda #<(vic_screen+10+2*40)
			sta sm4+1
			lda #>(vic_screen+10+2*40)
			sta sm4+2
			lda #<(db_screen+10+1*40)
			sta sm5+1
			lda #>(db_screen+10+1*40)
			sta sm5+2
			jmp :++
		:
			lda #<(db_bitmap+10*8+2*320)
			sta sm0+1
			lda #>(db_bitmap+10*8+2*320)
			sta sm0+2
			lda #<(vic_bitmap+10*8+1*320)
			sta sm1+1
			lda #>(vic_bitmap+10*8+1*320)
			sta sm1+2
			lda #<(db_bitmap+10*8+2*320+10*8)
			sta sm2+1
			lda #>(db_bitmap+10*8+2*320+10*8)
			sta sm2+2
			lda #<(vic_bitmap+10*8+1*320+10*8)
			sta sm3+1
			lda #>(vic_bitmap+10*8+1*320+10*8)
			sta sm3+2

			lda #<(db_screen+10+2*40)
			sta sm4+1
			lda #>(db_screen+10+2*40)
			sta sm4+2
			lda #<(vic_screen+10+1*40)
			sta sm5+1
			lda #>(vic_screen+10+1*40)
			sta sm5+2
		:

		ldy #19
		rowLoop:
			ldx #79
			:
sm0:			lda vic_bitmap,x
sm1:			sta db_bitmap,x
sm2:			lda vic_bitmap+10*8,x
sm3:			sta db_bitmap+10*8,x
				dex
			bpl :-
			ldx #19
			:
sm4:			lda vic_screen,x
sm5:			sta db_screen,x
				dex
			bpl :-

			clc
			lda sm0+1
			adc #<320
			sta sm0+1
			lda sm0+2
			adc #>320
			sta sm0+2
			clc
			lda sm1+1
			adc #<320
			sta sm1+1
			lda sm1+2
			adc #>320
			sta sm1+2
			clc
			lda sm2+1
			adc #<320
			sta sm2+1
			lda sm2+2
			adc #>320
			sta sm2+2
			clc
			lda sm3+1
			adc #<320
			sta sm3+1
			lda sm3+2
			adc #>320
			sta sm3+2
			clc
			lda sm4+1
			adc #40
			sta sm4+1
			bcc :+
				inc sm4+2
			:
			clc
			lda sm5+1
			adc #40
			sta sm5+1
			bcc :+
				inc sm5+2
			:

			dey
			bmi :+
		jmp rowLoop
		:

		lda #$37
		sta 1
		rts
.endproc

.segment "INTRO2"
dungeonEntrance_gfx:
		.incbin "../resources/dungeonentrance.kla.b2",2

.proc copyRow
		ldy #20-1
		:
			lda (SRC_SCREEN),y
			sta (DST_SCREEN),y
			lda (SRC_D800),y
			sta (DST_D800),y
			dey
		bpl :-
		ldy #0
		:
			lda (SRC_DATA),y
			sta (DST_BITMAP),y
			iny
			cpy #8*20
		bne :-

		clc
		lda SRC_DATA+0
		adc #<(20*8)
		sta SRC_DATA+0
		bcc :+
			inc SRC_DATA+1
		:
		clc
		lda SRC_SCREEN+0
		adc #20
		sta SRC_SCREEN+0
		bcc :+
			inc SRC_SCREEN+1
		:
		clc
		lda SRC_D800+0
		adc #20
		sta SRC_D800+0
		bcc :+
			inc SRC_D800+1
		:

		rts
.endproc

.export intro_clearBitmap
.proc intro_clearBitmap
		ldx #0
		:
			lda #0
			.repeat 4,I
				sta $d800+I*$100,x
			.endrep
			lda #$ff
			.repeat 31,I
				sta vic_bitmap+I*$100,x
				sta db_bitmap+I*$100,x
			.endrep
			inx
			beq :+
		jmp :-
		:
		rts
.endproc

.export entrance_scene
.proc entrance_scene
		jsr intro_clearBitmap

		lda #1
		ldx #9
		ldy #1
		jsr showTextExt
		jsr textScreen1OneUp

		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<dungeonEntrance_gfx
		ldx #>dungeonEntrance_gfx
		jsr decrunchTo

		lda #$ff
		ldx #$3e
		:
			sta vic_sprites+SPRITEINDEX*$40,x
			sta db_sprites+SPRITEINDEX*$40,x
			sta text_screen0,x
			sta text_screen1,x
			dex
		bpl :-

		ldx #7
		:
			lda #<(vic_sprites/$40+SPRITEINDEX)
			sta db_screen+$3f8,x
			sta vic_screen+$3f8,x
			lda #<(vic_sprites/$40)
			sta text_screen0+$3f8,x
			sta text_screen1+$3f8,x
			lda #0
			sta $d027,x
			dex
		bpl :-

		lda #<freeRam
		sta SRC_DATA+0
		lda #>freeRam
		sta SRC_DATA+1
		lda #<(vic_bitmap+10*8+1*320)
		sta DST_BITMAP+0
		lda #>(vic_bitmap+10*8+1*320)
		sta DST_BITMAP+1
		lda #<(freeRam+20*8*32)
		sta SRC_SCREEN+0
		lda #>(freeRam+20*8*32)
		sta SRC_SCREEN+1
		lda #<(vic_screen+10+1*40)
		sta DST_SCREEN+0
		lda #>(vic_screen+10+1*40)
		sta DST_SCREEN+1
		lda #<(freeRam+20*8*32+20*32)
		sta SRC_D800+0
		lda #>(freeRam+20*8*32+20*32)
		sta SRC_D800+1
		lda #<($d800+10+1*40)
		sta DST_D800+0
		lda #>($d800+10+1*40)
		sta DST_D800+1

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

		ldx #21
		rowLoop:
			lda #2
			jsr waitWithAbort
			beq :+
				jmp abort
			:

			jsr copyRow
			dex
			beq doneRows

			clc
			lda DST_BITMAP+0
			adc #<320
			sta DST_BITMAP+0
			lda DST_BITMAP+1
			adc #>320
			sta DST_BITMAP+1

			clc
			lda DST_SCREEN+0
			adc #40
			sta DST_SCREEN+0
			bcc :+
				inc DST_SCREEN+1
			:
			clc
			lda DST_D800+0
			adc #40
			sta DST_D800+0
			bcc :+
				inc DST_D800+1
			:
		jmp rowLoop
		doneRows:

		lda #100
		jsr waitWithAbort
		bne abort

		inc startScroll
		scrollLoop:
			lda scrollY
			and #7
			bne :++
				lda scrollY
				beq :+
					jsr scrollD800
					lda DST_BITMAP+1
					eor #$80
					sta DST_BITMAP+1
					lda DST_SCREEN+1
					eor #$80
					sta DST_SCREEN+1
					jsr copyRow
				:
				jsr scroll
			:
			lda #1
			jsr waitWithAbort
			bne abort
			lda startScroll
		bne scrollLoop

		lda #150
		jsr waitWithAbort

abort:	rts
.endproc
