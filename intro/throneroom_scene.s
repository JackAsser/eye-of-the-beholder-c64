.include "../global.inc"
.include "bitblt.inc"

throneroom_screen  = $c000
throneroom_screen2 = $c400
throneroom_sprites = $c800
throneroom_bitmap  = $e000

.segment "INTRO_BSS"
IRQ_TMP:						.res 2
IRQ_TMP2:						.res 2

intro_throneroom_sprites_map:	.res 72
splitPosition:					.res 4
splitImage:						.res 4

; This chunk MUST be linear
splitLine0_lo: 					.res 9
splitLine1_lo: 					.res 9
splitLine2_lo: 					.res 9
splitLine3_lo: 					.res 9

; This chunk MUST be linear
splitLine0_hi: 					.res 9
splitLine1_hi: 					.res 9
splitLine2_hi: 					.res 9
splitLine3_hi: 					.res 9

.segment "INTRO_RAM"
characterYpos: 					.res 4,$e4
stepY: 							.repeat 4,I
									.byte (I*5)&3
								.endrep

.proc screenMode
	.word init
	.word teardown
	.byte 3
	.word $20, resetMultiplexer
	.word 125, multiplex
textModePosition: .word $e9, textMode

	splitAction0:	.byte 0,1,0,1,0,1,0,1
	splitAction1:	.byte 0,1,0,1,0,1,0,1
	splitAction2:	.byte 0,1,0,1,0,1,0,1
	splitAction3:	.byte 0,1,0,1,0,1,0,1

	.proc resetMultiplexer
		jsr init

		; Reset split images
		lda #9*0
		sta splitImage+0
		lda intro_throneroom_sprites_map+9*0
		sta throneroom_screen+$3f8
		lda intro_throneroom_sprites_map+9*1
		sta throneroom_screen+$3f9
		lda #9*2
		sta splitImage+1
		lda intro_throneroom_sprites_map+9*2
		sta throneroom_screen+$3fa
		lda intro_throneroom_sprites_map+9*3
		sta throneroom_screen+$3fb
		lda #9*4
		sta splitImage+2
		lda intro_throneroom_sprites_map+9*4
		sta throneroom_screen+$3fc
		lda intro_throneroom_sprites_map+9*5
		sta throneroom_screen+$3fd
		lda #9*6
		sta splitImage+3
		lda intro_throneroom_sprites_map+9*6
		sta throneroom_screen+$3fe
		lda intro_throneroom_sprites_map+9*7
		sta throneroom_screen+$3ff

		; Place sprites
		lda characterYpos+0
		sta $d001
		sta $d003
		lda characterYpos+1
		sta $d005
		sta $d007
		lda characterYpos+2
		sta $d009
		sta $d00b
		lda characterYpos+3
		sta $d00d
		sta $d00f

		; Calculate split lines
		ldy #0
		ldx #0
		:
			lda #0
			sta splitPosition,x
			clc
			lda characterYpos,x
			adc #10
			sta IRQ_TMP+0
			lda #0
			sta IRQ_TMP+1

			lda #3
			sta IRQ_TMP2
			:
				clc
				lda IRQ_TMP+0
				sta splitLine0_lo,y
				adc #<11
				sta IRQ_TMP+0
				lda IRQ_TMP+1
				sta splitLine0_hi,y
				adc #>11
				sta IRQ_TMP+1
				iny

				clc
				lda IRQ_TMP+0
				sta splitLine0_lo,y
				adc #<10
				sta IRQ_TMP+0
				lda IRQ_TMP+1
				sta splitLine0_hi,y
				adc #>10
				sta IRQ_TMP+1
				iny

				dec IRQ_TMP2
			bpl :-

			lda #$ff
			sta splitLine0_lo,y
			sta splitLine0_hi,y
			iny
			inx
			cpx #4
		bne :--

		jmp exitIrq
	.endproc

	.proc textMode
		; First cut sprites and create a FLD-line
		inc $d011
		lda #$18
		sta $d018
		pha
		pla
		pha
		pla
		lda #0
		sta $d021

		; Then on the FLD line, switch to text mode
		lda $dd00
		and #$fc
		ora #$01
		sta $dd00
		lda #$02
		sta $d018
		lda #$08
		sta $d016
		lda #$1c
		sta $d011

		jmp exitIrq_quick
	.endproc

	.proc multiplex
		jsr exitIrq_next
		dec $d019
		cli
		ldx #230-3
		lda system
		cmp #SYSTEM_PAL
		beq :+
			ldx #230-1
		:
		stx stopLine+1
poll:
		lda $d012
stopLine:cmp #230-1
		bcc :+
			jmp exitIrq_rerun
		:

		; Have we passed the trigger line
		.repeat 4,I
			ldx splitPosition+I
			lda splitLine0_hi+I*9,x
			bmi :++
			lda $d012
			cmp splitLine0_lo+I*9,x
			bcc :++
				; Yes, check action
				lda splitAction0+I*8,x
				bne :+
					; Multiplex
					clc
					lda $d001+I*4
					adc #21
					sta $d001+I*4
					sta $d003+I*4
				:

				; Change image
				ldy splitImage+I
				iny
				lda intro_throneroom_sprites_map,y
				sta throneroom_screen+$3f8+I*2
				lda intro_throneroom_sprites_map+9,y
				sta throneroom_screen+$3f9+I*2
				sty splitImage+I

				inx
			:
			stx splitPosition+I
		.endrep

		jmp poll
	.endproc

	.proc init
		lda #$18
		sta $d016
		lda $dd00
		and #$fc
		sta $dd00
		lda #$3b
		sta $d011
		lda #$08
		sta $d018
		lda #08
		sta $d021
		rts
	.endproc

	.proc teardown
		lda #0
		sta $d011
		sta $d020
		sta $d015
		rts
	.endproc
.endproc

.segment "INTRO3"
intro_throneroom_gfx:
	.incbin  "../converters/intro/compressed/intro_throneroom.prg.b2",2
	.include "../converters/intro/compressed/intro_throneroom.inc"

.proc intro_throneroom_sprites_map_raw
	.incbin "../converters/intro/converted/intro_throneroom_sprites.map"
.endproc

.proc intro_throneroom_sprites_gfx
	.incbin "../converters/intro/compressed/intro_throneroom_sprites.prg.b2",2
.endproc

.include "transition_macros.inc"

.proc showThrownRoom
		ldx #<:+
		ldy #>:+
		jsrf playTransition
		rts
		:

		.include "transition_throneroom.inc"
		_done
.endproc

.proc ramReset
		.res 4,$e4
		.repeat 4,I
			.byte (I*5)&3
		.endrep
.endproc

.export throneroom_scene
.proc throneroom_scene
		lda #$e9
		sta screenMode::textModePosition
		ldx #.sizeof(ramReset)-1
		:
			lda ramReset,x
			sta characterYpos,x
			dex
		bpl :-

		jsrf intro_clearBitmap
		
		ldx #0
		:
			clc
			lda intro_throneroom_sprites_map_raw,x
			adc #<(throneroom_sprites/$40)
			sta intro_throneroom_sprites_map,x
			inx
			cpx #.sizeof(intro_throneroom_sprites_map_raw)
		bne :-

.if 0
		memcpy freeRam, intro_throneroom_sprites_gfx, .sizeof(intro_throneroom_sprites_gfx)
		lda #<throneroom_sprites
		sta BB_DST+0
		lda #>throneroom_sprites
		sta BB_DST+1
		ldy #<freeRam
		ldx #>freeRam
		jsr decrunchToUnderIO
.endif
		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<intro_throneroom_sprites_gfx
		ldx #>intro_throneroom_sprites_gfx
		jsr decrunchTo
		memcpy_underio throneroom_sprites, freeRam, $1000

		lda #<freeRam
		sta BB_DST+0
		lda #>freeRam
		sta BB_DST+1
		ldy #<intro_throneroom_gfx
		ldx #>intro_throneroom_gfx
		jsr decrunchTo

		setBitBltDst throneroom_bitmap, throneroom_screen
		setBitBltSrc freeRam+intro_throneroom_bitmap, freeRam+intro_throneroom_screen, freeRam+intro_throneroom_d800, 28
		
;		memcpy throneroom_bitmap, (freeRam+intro_throneroom_bitmap), intro_throneroom_bitmap_size
;		memcpy throneroom_screen, (freeRam+intro_throneroom_screen), intro_throneroom_screen_size
;		memcpy $d800, (freeRam+intro_throneroom_d800), intro_throneroom_d800_size

		lda #<(throneroom_sprites/$40)
		ldx #7
		:
			sta throneroom_screen2+$3f8,x
			dex
		bpl :-

		; Setup sprites
		lda #$ff
		sta $d015

		lda #24+8*8
		sta $d000
		lda #24+8*8+24
		sta $d002

		lda #24+14*8+4
		sta $d004
		lda #24+14*8+4+24
		sta $d006

		lda #24+21*8+2
		sta $d008
		lda #24+21*8+2+24
		sta $d00a

		lda #<(24+27*8+4)
		sta $d00c
		lda #<(24+27*8+4+24)
		sta $d00e

		lda #$80
		sta $d010

		lda #$ff
		sta $d01c
		lda #0
		sta $d017
		sta $d01b
		sta $d01d

		lda #0
		sta $d025
		lda #$c
		sta $d026
		lda #6
		sta $d027
		sta $d029
		sta $d02b
		sta $d02c
		sta $d02d
		lda #9
		sta $d028
		sta $d02e
		lda #$b
		sta $d02a

		clc
		ldx #<screenMode
		ldy #>screenMode
		jsr setScreenMode

		jsr showThrownRoom
		memcpy throneroom_screen2, throneroom_screen, 1000

		wait 25

		loop:
			lda #3
			sta COUNT

			ldx #3
			characterLoop:
				lda characterYpos,x
				cmp #129+1
				bcc nextCharacter

				dec COUNT

				clc
				lda stepY,x
				adc #1
				and #7
				tay
				sta stepY,x

				clc
				lda characterYpos,x
				adc modY,y
				cmp #129
				bcs :+
					lda #129
				:
				sta characterYpos,x

				nextCharacter:
				dex
			bpl characterLoop

			lda COUNT
			beq doneWalk

			wait 2
		beq loop
		bne abort
doneWalk:
		sec
		lda screenMode::textModePosition
		sbc #16
		sta screenMode::textModePosition
		lda #1
		jsr wait
		
		lda #1
		ldx #6
		ldy #0
		jsr showTextExt
		lda #1
		ldx #7
		ldy #1
		jsr showTextExt

		wait 35
		wait 35

abort:	rts
modY:	.byte <-4, <-8, <-2, <-2, <1, <0, <0, <0 
.endproc
