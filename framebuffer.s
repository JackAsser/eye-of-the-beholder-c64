.include "global.inc"

;.feature force_range

.segment "FRAMEBUFFER"

.export portalAnimationFrames
portalAnimationFrames:

.export maze
.res 4 ;wrappers for automap rendering
maze:
	.res 4096
.res 4 ;wrappers for automap rendering

.align 8
.export scratchArea
scratchArea:

.export frameBuffer_bitmap
frameBuffer_bitmap:	.res 22*15*8
.export frameBuffer_sprites
frameBuffer_sprites:	.res 22*15*8
.export frameBuffer_screen
frameBuffer_screen:	.res 22*15
.export frameBuffer_d800
frameBuffer_d800:	.res 22*15

.segment "BSS"
.export affectedSpriteColumns
affectedSpriteColumns:
	.res 22
dirtySpriteColumns:
	.res 22

.segment "MEMCODE_RO"
.align 256
.export fliptab
fliptab:.repeat 256,I
		.byte ((I&$03)<<6) | ((I&$0c)<<2) | ((I&$30)>>2) | ((I&$c0)>>6)
	.endrep
.export mask_hi
mask_hi:
	.repeat 256,I
	.scope
		v = (I>>4)&15
		.byte ((v&8)*(3<<3)) | ((v&4)*(3<<2)) | ((v&2)*(3<<1)) | ((v&1)*3)
	.endscope
	.endrep
.export mask_lo
mask_lo:
	.repeat 256,I
	.scope
		v = I&15
		.byte ((v&8)*(3<<3)) | ((v&4)*(3<<2)) | ((v&2)*(3<<1)) | ((v&1)*3)
	.endscope
	.endrep

flipmask_hi:
	.repeat 256,I
	.scope
		v = (I>>4)&15
		.byte (((v&8)*3)>>3) | ((v&4)*3) | ((v&2)*(3<<3)) | ((v&1)*(3<<6))
	.endscope
	.endrep

flipmask_lo:
	.repeat 256,I
	.scope
		v = I&15
		.byte (((v&8)*3)>>3) | ((v&4)*3) | ((v&2)*(3<<3)) | ((v&1)*(3<<6))
	.endscope
	.endrep

zBufferPtr_lo:
	.repeat 22,I
		.byte <(frameBuffer_screen+15*I+5)
	.endrep
zBufferPtr_hi:
	.repeat 22,I
		.byte >(frameBuffer_screen+15*I+5)
	.endrep

.segment "MEMCODE_RW"
.export drawGfxObject
.proc drawGfxObject
	ldy #0
	sty ROWSKIP
withRowSkip:
	jsrf drawGfxObjectSetup

	lda ROWSKIP
	beq :+
		sec
		lda DST_SCREEN
		sbc ROWSKIP
		sta DST_SCREEN
		sec
		lda DST_D800
		sbc ROWSKIP
		sta DST_D800
	:

	columnLoop:
		ldy #0
		lda (SRC_DATA),y
		inc SRC_DATA+0
		bne :+
			inc SRC_DATA+1
		:

		ldy XPOS
		cpy WALLX0
		bpl :++
			clc
			adc SRC_DATA+0
			sta SRC_DATA+0
			bcc :+
				inc SRC_DATA+1
			:
			jmp skipRow
		:
		cpy WALLX1
		bmi :+
			rts
		:

		lda #1
		sta affectedSpriteColumns,y

		ldy #0
		lda DST_BITMAP+0
		sta DST_BITMAP_USE+0
		lda DST_BITMAP+1
		sta DST_BITMAP_USE+1
		lda DST_SPRITES+0
		sta DST_SPRITES_USE+0
		lda DST_SPRITES+1
		sta DST_SPRITES_USE+1
		lda ROWSKIP
		pha
		rowLoop:
			lda ROWSKIP
			beq noRowSkip
				lax (SRC_SCREEN),y
				bne :+
					jmp skipChar
				:
				lda (SRC_D800),y
				bmi :+
					lda #8
					jmp :++
				:
					lda #12
				:
				clc
				adc SRC_DATA+0
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:
				jmp skipChar
			noRowSkip:

			; CharType
			;
			; Empty if SCREEN=0
			; Bitmap if D800 is pos
			; Sprite if D800 is neg
			lax (SRC_SCREEN),y
			bne :+
				jmp skipChar
			:
			lda (SRC_D800),y
			bmi spriteChar
			bitmapChar:
				sta (DST_D800),y
				txa
				sta (DST_SCREEN),y
				sty YREG
				ldx #0
				.repeat 8,I
					.if I=0
						ldy #0
					.else
						iny
					.endif
					lda (SRC_DATA),y
					sta (DST_BITMAP_USE),y
					txa
					sta (DST_SPRITES_USE),y
				.endrep
				clc
				lda SRC_DATA+0
				adc #8
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:
				ldy YREG
				jmp skipChar
			spriteChar:
				sty YREG
				ldy #0
				lax (SRC_DATA),y
				lda mask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda mask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				ldy #1
				lax (SRC_DATA),y
				ldy #2
				lda mask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda mask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				ldy #2
				lax (SRC_DATA),y
				ldy #4
				lda mask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda mask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				ldy #3
				lax (SRC_DATA),y
				ldy #6
				lda mask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda mask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				clc
				lda SRC_DATA+0
				adc #4
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:

				.repeat 8,I
					pla
					and (DST_SPRITES_USE),y
					ora (SRC_DATA),y
					sta (DST_SPRITES_USE),y
					dey
				.endrep
				clc
				lda SRC_DATA+0
				adc #8
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:
				ldy YREG
			skipChar:

			iny
			cpy HEIGHT
			beq doneRow

			lda ROWSKIP
			beq :+
				dec ROWSKIP
				jmp rowLoop
			:

			clc
			lda DST_BITMAP_USE+0
			adc #8
			sta DST_BITMAP_USE+0
			bcc :+
				inc DST_BITMAP_USE+1
				clc
			:
			lda DST_SPRITES_USE+0
			adc #8
			sta DST_SPRITES_USE+0
			bcc :+
				inc DST_SPRITES_USE+1
			:
		jmp rowLoop
		doneRow:
		pla
		sta ROWSKIP
		skipRow:

		inc XPOS
		dec WIDTH
		bne :+
			rts
		:

		clc
		lda SRC_SCREEN+0
		adc HEIGHT
		sta SRC_SCREEN+0
		bcc :+
			inc SRC_SCREEN+1
			clc
		:
		lda SRC_D800+0
		adc HEIGHT
		sta SRC_D800+0
		bcc :+
			inc SRC_D800+1
		:

		jsr forwardDestination
	jmp columnLoop
	rts
.endproc
.export drawGfxObjectWithRowSkip = drawGfxObject::withRowSkip

.export forwardDestination
.proc forwardDestination
		clc
		lda DST_BITMAP+0
		adc #<15*8
		sta DST_BITMAP+0
		lda DST_BITMAP+1
		adc #>15*8
		sta DST_BITMAP+1

		clc
		lda DST_SPRITES+0
		adc #<15*8
		sta DST_SPRITES+0
		lda DST_SPRITES+1
		adc #>15*8
		sta DST_SPRITES+1

		clc
		lda DST_SCREEN+0
		adc #15
		sta DST_SCREEN+0
		bcc :+
			inc DST_SCREEN+1
			clc
		:

		lda DST_D800+0
		adc #15
		sta DST_D800+0
		bcc :+
			inc DST_D800+1
		:
		rts
.endproc

.export backwardDestination
.proc backwardDestination
		sec
		lda DST_BITMAP+0
		sbc #<15*8
		sta DST_BITMAP+0
		lda DST_BITMAP+1
		sbc #>15*8
		sta DST_BITMAP+1

		sec
		lda DST_SPRITES+0
		sbc #<15*8
		sta DST_SPRITES+0
		lda DST_SPRITES+1
		sbc #>15*8
		sta DST_SPRITES+1

		sec
		lda DST_SCREEN+0
		sbc #15
		sta DST_SCREEN+0
		lda DST_SCREEN+1
		sbc #0
		sta DST_SCREEN+1

		sec
		lda DST_D800+0
		sbc #15
		sta DST_D800+0
		lda DST_D800+1
		sbc #0
		sta DST_D800+1
		rts
.endproc

.export drawFlippedGfxObject
.proc drawFlippedGfxObject
	jsrf drawGfxObjectSetup
	columnLoop:
		ldy #0
		lda (SRC_DATA),y
		inc SRC_DATA+0
		bne :+
			inc SRC_DATA+1
		:

		ldy XPOS
		cpy WALLX0
		bpl :+
			rts
		:
		cpy WALLX1
		bmi :++
			clc
			adc SRC_DATA+0
			sta SRC_DATA+0
			bcc :+
				inc SRC_DATA+1
			:
			jmp doneRow
		:

		lda #1
		sta affectedSpriteColumns,y

		ldy #0
		lda DST_BITMAP+0
		sta DST_BITMAP_USE+0
		lda DST_BITMAP+1
		sta DST_BITMAP_USE+1
		lda DST_SPRITES+0
		sta DST_SPRITES_USE+0
		lda DST_SPRITES+1
		sta DST_SPRITES_USE+1
		rowLoop:
			; CharType
			;
			; Empty if SCREEN=0
			; Bitmap if D800 is pos
			; Sprite if D800 is neg
			lax (SRC_SCREEN),y
			bne :+
				jmp skipChar
			:
			lda (SRC_D800),y
			bmi spriteChar
			bitmapChar:
				sta (DST_D800),y
				txa
				sta (DST_SCREEN),y
				sty YREG
				.repeat 8,I
					.if I=0
						ldy #0
					.else
						iny
					.endif
					lax (SRC_DATA),y
					lda fliptab,x
					sta (DST_BITMAP_USE),y
					lda #0
					sta (DST_SPRITES_USE),y
				.endrep
				clc
				lda SRC_DATA+0
				adc #8
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:
				ldy YREG
				jmp skipChar
			spriteChar:
				sty YREG
				ldy #0
				lax (SRC_DATA),y
				lda flipmask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda flipmask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				ldy #1
				lax (SRC_DATA),y
				ldy #2
				lda flipmask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda flipmask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				ldy #2
				lax (SRC_DATA),y
				ldy #4
				lda flipmask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda flipmask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				ldy #3
				lax (SRC_DATA),y
				ldy #6
				lda flipmask_hi,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y
				iny
				lda flipmask_lo,x
				pha
				and (DST_BITMAP_USE),y
				sta (DST_BITMAP_USE),y

				clc
				lda SRC_DATA+0
				adc #4
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:

				.repeat 8,I
					lax (SRC_DATA),y
					pla
					and (DST_SPRITES_USE),y
					ora fliptab,x
					sta (DST_SPRITES_USE),y
					dey
				.endrep
				clc
				lda SRC_DATA+0
				adc #8
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:
				ldy YREG
			skipChar:

			iny
			cpy HEIGHT
			beq doneRow

			clc
			lda DST_BITMAP_USE+0
			adc #8
			sta DST_BITMAP_USE+0
			bcc :+
				inc DST_BITMAP_USE+1
				clc
			:
			lda DST_SPRITES_USE+0
			adc #8
			sta DST_SPRITES_USE+0
			bcc :+
				inc DST_SPRITES_USE+1
			:
		jmp rowLoop
		doneRow:
		dec XPOS
		dec WIDTH
		bne :+
			rts
		:

		clc
		lda SRC_SCREEN+0
		adc HEIGHT
		sta SRC_SCREEN+0
		bcc :+
			inc SRC_SCREEN+1
			clc
		:
		lda SRC_D800+0
		adc HEIGHT
		sta SRC_D800+0
		bcc :+
			inc SRC_D800+1
		:

		jsr backwardDestination
	jmp columnLoop
	rts
.endproc

.export drawWhiteGfxObject
.proc drawWhiteGfxObject
	jsrf drawGfxObjectSetup
	columnLoop:
		ldy #0
		lda (SRC_DATA),y
		inc SRC_DATA+0
		bne :+
			inc SRC_DATA+1
		:

		ldy XPOS
		cpy WALLX0
		bpl :++
			clc
			adc SRC_DATA+0
			sta SRC_DATA+0
			bcc :+
				inc SRC_DATA+1
			:
			jmp doneRow
		:
		cpy WALLX1
		bmi :+
			rts
		:

		lda #1
		sta affectedSpriteColumns,y

		ldy #0
		lda DST_SPRITES+0
		sta DST_SPRITES_USE+0
		lda DST_SPRITES+1
		sta DST_SPRITES_USE+1
		rowLoop:
			; CharType
			;
			; Empty if SCREEN=0
			; Bitmap if D800 is pos
			; Sprite if D800 is neg
			lax (SRC_SCREEN),y
			bne :+
				jmp skipChar
			:
			lda (SRC_D800),y
			bmi spriteChar
			bitmapChar:
				sty YREG
				lda #$ff
				.repeat 8,I
					.if I=0
						ldy #0
					.else
						iny
					.endif
					sta (DST_SPRITES_USE),y
				.endrep
				clc
				lda SRC_DATA+0
				adc #8
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:
				ldy YREG
				jmp skipChar
			spriteChar:
				sty YREG
				ldy #0
				lax (SRC_DATA),y
				lda mask_hi,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y
				iny
				lda mask_lo,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y

				ldy #1
				lax (SRC_DATA),y
				ldy #2
				lda mask_hi,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y
				iny
				lda mask_lo,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y

				ldy #2
				lax (SRC_DATA),y
				ldy #4
				lda mask_hi,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y
				iny
				lda mask_lo,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y

				ldy #3
				lax (SRC_DATA),y
				ldy #6
				lda mask_hi,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y
				iny
				lda mask_lo,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y

				clc
				lda SRC_DATA+0
				adc #12
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:

				ldy YREG
			skipChar:

			iny
			cpy HEIGHT
			beq doneRow

			clc
			lda DST_SPRITES_USE+0
			adc #8
			sta DST_SPRITES_USE+0
			bcc :+
				inc DST_SPRITES_USE+1
			:
		jmp rowLoop
		doneRow:
		inc XPOS
		dec WIDTH
		bne :+
			rts
		:

		clc
		lda SRC_SCREEN+0
		adc HEIGHT
		sta SRC_SCREEN+0
		bcc :+
			inc SRC_SCREEN+1
			clc
		:
		lda SRC_D800+0
		adc HEIGHT
		sta SRC_D800+0
		bcc :+
			inc SRC_D800+1
		:

		jsr forwardDestination
	jmp columnLoop
	rts
.endproc

.export drawFlippedWhiteGfxObject
.proc drawFlippedWhiteGfxObject
	jsrf drawGfxObjectSetup
	columnLoop:
		ldy #0
		lda (SRC_DATA),y
		inc SRC_DATA+0
		bne :+
			inc SRC_DATA+1
		:

		ldy XPOS
		cpy WALLX0
		bpl :+
			rts
		:
		cpy WALLX1
		bmi :++
			clc
			adc SRC_DATA+0
			sta SRC_DATA+0
			bcc :+
				inc SRC_DATA+1
			:
			jmp doneRow
		:

		lda #1
		sta affectedSpriteColumns,y

		ldy #0
		lda DST_SPRITES+0
		sta DST_SPRITES_USE+0
		lda DST_SPRITES+1
		sta DST_SPRITES_USE+1
		rowLoop:
			; CharType
			;
			; Empty if SCREEN=0
			; Bitmap if D800 is pos
			; Sprite if D800 is neg
			lax (SRC_SCREEN),y
			bne :+
				jmp skipChar
			:
			lda (SRC_D800),y
			bmi spriteChar
			bitmapChar:
				sty YREG
				lda #$ff
				.repeat 8,I
					.if I=0
						ldy #0
					.else
						iny
					.endif
					sta (DST_SPRITES_USE),y
				.endrep
				clc
				lda SRC_DATA+0
				adc #8
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:
				ldy YREG
				jmp skipChar
			spriteChar:
				sty YREG
				ldy #0
				lax (SRC_DATA),y
				lda flipmask_hi,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y
				iny
				lda flipmask_lo,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y

				ldy #1
				lax (SRC_DATA),y
				ldy #2
				lda flipmask_hi,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y
				iny
				lda flipmask_lo,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y

				ldy #2
				lax (SRC_DATA),y
				ldy #4
				lda flipmask_hi,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y
				iny
				lda flipmask_lo,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y

				ldy #3
				lax (SRC_DATA),y
				ldy #6
				lda flipmask_hi,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y
				iny
				lda flipmask_lo,x
				eor #$ff
				ora (DST_SPRITES_USE),y
				sta (DST_SPRITES_USE),y

				clc
				lda SRC_DATA+0
				adc #12
				sta SRC_DATA+0
				bcc :+
					inc SRC_DATA+1
				:
				ldy YREG
			skipChar:

			iny
			cpy HEIGHT
			beq doneRow

			clc
			lda DST_SPRITES_USE+0
			adc #8
			sta DST_SPRITES_USE+0
			bcc :+
				inc DST_SPRITES_USE+1
			:
		jmp rowLoop
		doneRow:
		dec XPOS
		dec WIDTH
		bne :+
			rts
		:

		clc
		lda SRC_SCREEN+0
		adc HEIGHT
		sta SRC_SCREEN+0
		bcc :+
			inc SRC_SCREEN+1
			clc
		:
		lda SRC_D800+0
		adc HEIGHT
		sta SRC_D800+0
		bcc :+
			inc SRC_D800+1
		:

		jsr backwardDestination
	jmp columnLoop
	rts
.endproc

.export drawWallObject
.proc drawWallObject
	jsrf drawGfxObjectSetup

	lda HEIGHT
	sta sm_height0+1
	sta sm_height1+1
	sta sm_height2+1
	lda SRC_SCREEN+0
	sta src_screen+1
	lda SRC_SCREEN+1
	sta src_screen+2
	lda SRC_D800+0
	sta src_d800+1
	lda SRC_D800+1
	sta src_d800+2
	lda DST_SCREEN+0
	sta dst_screen+1
	lda DST_SCREEN+1
	sta dst_screen+2
	lda DST_D800+0
	sta dst_d800+1
	lda DST_D800+1
	sta dst_d800+2
	lda DST_BITMAP+1
	.repeat 8,I
		sta sm+6*I+4
	.endrep

	columnLoop:
		ldy #0
		clc
		lda (SRC_DATA),y
		adc #1 ;When flipped is also optimized, this can be removed by altering the generated data
		iny
		sty TMP3

		ldy XPOS
		cpy WALLX1
		bmi :+
			rts
		:
		cpy WALLX0
		bpl :+
			jmp doneRow
		:

		ldx zBufferPtr_lo,y
		stx :+ +1
		ldx zBufferPtr_hi,y
		stx :+ +2
		:ldx $dead
		bne doneRow
		; x=0 here

		ldy DST_BITMAP+0
		.repeat 8,I
			sty sm+6*I+3
			.if I<7
				iny
			.endif
		.endrep

		ldy #0
		rowLoop:
			; CharType
			;
			; Empty if SCREEN=0 else bitmap
src_screen:	lda $dead,y ;(SRC_SCREEN)
			beq :+
dst_screen:		sta $dead,y ;(DST_SCREEN)
src_d800:		lda $dead,y ;(SRC_D800)
dst_d800:		sta $dead,y ;(DST_D800)
				sty YREG
				ldy TMP3
				sm:
				.repeat 8,I
					lda (SRC_DATA),y
					sta $dead,x
					iny
				.endrep
				sty TMP3
				ldy YREG
			:
			txa
			axs #((8^$ff)+1)
			iny
			sm_height0:cpy #0 ;HEIGHT
		bne rowLoop
		lda TMP3
doneRow:
		inc XPOS
		dec WIDTH
		beq exit

		clc
		adc SRC_DATA+0
		sta SRC_DATA+0
		bcc :+
			inc SRC_DATA+1
			clc
		:

		lda src_screen+1
		sm_height1: adc #0 ;HEIGHT
		sta src_screen+1
		bcc :+
			inc src_screen+2
			clc
		:
		lda src_d800+1
		sm_height2: adc #0 ;HEIGHT
		sta src_d800+1
		bcc :+
			inc src_d800+2
			clc
		:

		lda dst_screen+1
		adc #15
		sta dst_screen+1
		bcc :+
			inc dst_screen+2
			clc
		:

		lda dst_d800+1
		adc #15
		sta dst_d800+1
		bcc :+
			inc dst_d800+2
			clc
		:

		lda DST_BITMAP+0
		adc #15*8
		sta DST_BITMAP+0
		bcc :+
			inc DST_BITMAP+1
			lda DST_BITMAP+1
			.repeat 8,I
				sta sm+6*I+4
			.endrep
		:
	jmp columnLoop
exit:	rts
.endproc

.export drawFlippedWallObject
.proc drawFlippedWallObject
	jsrf drawGfxObjectSetup

	lda HEIGHT
	sta sm_height0+1
	sta sm_height1+1
	sta sm_height2+1
	lda SRC_SCREEN+0
	sta src_screen+1
	lda SRC_SCREEN+1
	sta src_screen+2
	lda SRC_D800+0
	sta src_d800+1
	lda SRC_D800+1
	sta src_d800+2
	lda DST_SCREEN+0
	sta dst_screen+1
	lda DST_SCREEN+1
	sta dst_screen+2
	lda DST_D800+0
	sta dst_d800+1
	lda DST_D800+1
	sta dst_d800+2

	columnLoop:
		ldy #0
		lda (SRC_DATA),y
		inc SRC_DATA+0
		bne :+
			inc SRC_DATA+1
		:

		ldx XPOS
		cpx WALLX0
		bpl :+
			rts
		:
		cpx WALLX1
		bmi :+
			jmp doneRow
		:

		ldy zBufferPtr_hi,x
		sty :+ +2
		ldy zBufferPtr_lo,x
		:ldx $de00,y
		bne doneRow
		; x=0

		ldy #0
		lda DST_BITMAP+0
		sta DST_BITMAP_USE+0
		lda DST_BITMAP+1
		sta DST_BITMAP_USE+1
		clc
		rowLoop:
src_screen:	lda $dead,x ;(SRC_SCREEN),y
			bne :+
				lda DST_BITMAP_USE+0
				adc #8
				sta DST_BITMAP_USE+0
				bcc :++
					inc DST_BITMAP_USE+1
					clc
					jmp :++
			:

dst_screen:	sta $dead,x ;(DST_SCREEN),y
src_d800:	lda $dead,x ;(SRC_D800),y
dst_d800:	sta $dead,x ;(DST_D800),y
			stx XREG
			.repeat 8,I
				lax (SRC_DATA),y
				lda fliptab,x
				sta (DST_BITMAP_USE),y
				iny
			.endrep
			ldx XREG
				
			:
			inx
			sm_height0: cpx #0; HEIGHT
		bne rowLoop
		tya

doneRow:
		dec XPOS
		dec WIDTH
		bne :+
			rts
		:

		clc
		adc SRC_DATA+0
		sta SRC_DATA+0
		bcc :+
			inc SRC_DATA+1
			clc
		:

		lda src_screen+1
		sm_height1: adc #0 ;HEIGHT
		sta src_screen+1
		bcc :+
			inc src_screen+2
			clc
		:
		lda src_d800+1
		sm_height2: adc #0 ;HEIGHT
		sta src_d800+1
		bcc :+
			inc src_d800+2
		:

		sec
		lda DST_BITMAP+0
		sbc #15*8
		sta DST_BITMAP+0
		bcs :+
			dec DST_BITMAP+1
			sec
		:

		lda dst_screen+1
		sbc #15
		sta dst_screen+1
		bcs :+
			dec dst_screen+2
			sec
		:

		sec
		lda dst_d800+1
		sbc #15
		sta dst_d800+1
		bcs:+
			dec dst_d800+2
			sec
		:
	jmp columnLoop
	rts
.endproc

.export drawBackground
.proc drawBackground
	lda #22
	sta WIDTH
	lda #<frameBuffer_screen
	sta dst_screen+1
	sta dst_screen2+1
	lda #>frameBuffer_screen
	sta dst_screen+2
	sta dst_screen2+2
	lda #<frameBuffer_d800
	sta dst_d800+1
	lda #>frameBuffer_d800
	sta dst_d800+2

	ldx #>frameBuffer_bitmap
	ldy #<frameBuffer_bitmap
	jmp first
	columnLoop:
		bcc :+
			ldx sm+4
			inx
first:		.repeat 8,I
				stx sm+6*I+4
			.endrep
		:

		.repeat 8,I
			sty sm+6*I+3
			.if I<7
				iny
			.endif
		.endrep

		ldy #1
		sty TMP3
		clc

		ldy #0
		rowLoop:
src_screen:	lda $dead,y ;(SRC_SCREEN)
			beq :++
dst_screen2:	ldx $dead,y ;(DST_SCREEN)
				beq :+
					lda TMP3
					adc #8
					sta TMP3
					jmp :++
				:
dst_screen:			sta $dead,y ;(DST_SCREEN)
src_d800:			lda $dead,y ;(SRC_D800)
dst_d800:			sta $dead,y ;(DST_D800)
					sty YREG
					ldx offsets,y
					ldy TMP3
					sm:
					.repeat 8,I
						lda (SRC_DATA),y
						sta $dead,x
						iny
					.endrep
					sty TMP3
					ldy YREG
			:
			iny
			cpy #15
		bne rowLoop

		dec WIDTH
		beq exit

		clc
		lda TMP3
		adc SRC_DATA+0
		sta SRC_DATA+0
		bcc :+
			inc SRC_DATA+1
			clc
		:

		lda src_screen+1
		adc #15
		sta src_screen+1
		bcc :+
			inc src_screen+2
			clc
		:
		lda src_d800+1
		adc #15
		sta src_d800+1
		bcc :+
			inc src_d800+2
			clc
		:

		lda dst_screen+1
		adc #15
		sta dst_screen+1
		sta dst_screen2+1
		bcc :+
			inc dst_screen+2
			inc dst_screen2+2
			clc
		:

		lda dst_d800+1
		adc #15
		sta dst_d800+1
		bcc :+
			inc dst_d800+2
			clc
		:

		lda sm+3
		adc #<(15*8)
		tay
	jmp columnLoop
exit:	rts
offsets:
	.repeat 15,I
		.byte I*8
	.endrep
.endproc
.export drawBackground_src_screen = drawBackground::src_screen+1
.export drawBackground_src_d800 = drawBackground::src_d800+1


.export drawFlippedBackground
.proc drawFlippedBackground
	lda #22
	sta WIDTH
	lda #<(frameBuffer_screen+15*21)
	sta dst_screen+1
	sta dst_screen2+1
	lda #>(frameBuffer_screen+15*21)
	sta dst_screen+2
	sta dst_screen2+2
	lda #<(frameBuffer_d800+15*21)
	sta dst_d800+1
	lda #>(frameBuffer_d800+15*21)
	sta dst_d800+2

	ldx #>(frameBuffer_bitmap+15*21*8)
	ldy #<(frameBuffer_bitmap+15*21*8)
	jmp first
	columnLoop:
		cpx sm+4
		beq :+
first:		.repeat 8,I
				stx sm+6*I+4
			.endrep
		:

		.repeat 8,I
			sty sm+6*I+3
			.if I<7
				iny
			.endif
		.endrep

		ldy #1
		sty TMP3

		ldy #0
		rowLoop:
src_screen:	lda $dead,y ;(SRC_SCREEN)
			beq :++
dst_screen2:	ldx $dead,y ;(DST_SCREEN)
				beq :+
					clc
					lda TMP3
					adc #8
					sta TMP3
					jmp :++
				:
dst_screen:			sta $dead,y ;(DST_SCREEN)
src_d800:			lda $dead,y ;(SRC_D800)
dst_d800:			sta $dead,y ;(DST_D800)
					sty YREG
					ldx offsets,y
					ldy TMP3
					sm:
					.repeat 8,I
						lda (SRC_DATA),y
						sta $dead,x
						iny
					.endrep
					sty TMP3
					ldy YREG
			:
			iny
			cpy #15
		bne rowLoop

		dec WIDTH
		beq exit

		clc
		lda TMP3
		adc SRC_DATA+0
		sta SRC_DATA+0
		bcc :+
			inc SRC_DATA+1
			clc
		:

		lda src_screen+1
		adc #15
		sta src_screen+1
		bcc :+
			inc src_screen+2
			clc
		:
		lda src_d800+1
		adc #15
		sta src_d800+1
		bcc :+
			inc src_d800+2
		:

		sec
		lda dst_screen+1
		sbc #15
		sta dst_screen+1
		sta dst_screen2+1
		lda dst_screen+2
		sbc #0
		sta dst_screen+2
		sta dst_screen2+2

		sec
		lda dst_d800+1
		sbc #15
		sta dst_d800+1
		lda dst_d800+2
		sbc #0
		sta dst_d800+2

		sec
		lda sm+3
		sbc #<(15*8)
		tay
		lda sm+4
		sbc #>(15*8)
		tax
	jmp columnLoop
exit:	rts
offsets:
	.repeat 15,I
		.byte I*8
	.endrep
.endproc
.export drawFlippedBackground_src_screen = drawFlippedBackground::src_screen+1
.export drawFlippedBackground_src_d800 = drawFlippedBackground::src_d800+1


.segment "RENDERER"

; XPOS
.export getSpriteColumn
.proc getSpriteColumn
	clc
	ldx XPOS
	lda mul120_lo,x
	adc #<frameBuffer_sprites
	sta DST_SPRITES_USE+0
	lda mul120_hi,x
	adc #>frameBuffer_sprites
	sta DST_SPRITES_USE+1
	rts
.endproc

.export drawGfxObjectSetup
.proc drawGfxObjectSetup
	clc
	ldx XPOS
	lda mul120_lo,x
	ldy YPOS
	adc mul8,y
	sta TMP+0
	lda mul120_hi,x
	adc #0
	sta TMP+1

	clc
	lda #<frameBuffer_bitmap
	adc TMP+0
	sta DST_BITMAP+0
	lda #>frameBuffer_bitmap
	adc TMP+1
	sta DST_BITMAP+1

	clc
	lda #<frameBuffer_sprites
	adc TMP+0
	sta DST_SPRITES+0
	lda #>frameBuffer_sprites
	adc TMP+1
	sta DST_SPRITES+1

	clc
	ldx XPOS
	lda mul15_lo,x
	adc YPOS
	sta TMP+0
	lda mul15_hi,x
	adc #0
	sta TMP+1

	clc
	lda #<frameBuffer_screen
	adc TMP+0
	sta DST_SCREEN+0
	lda #>frameBuffer_screen
	adc TMP+1
	sta DST_SCREEN+1

	clc
	lda #<frameBuffer_d800
	adc TMP+0
	sta DST_D800+0
	lda #>frameBuffer_d800
	adc TMP+1
	sta DST_D800+1

	rts
.endproc

.align 256
mul15_lo:
	.repeat 128,I
		.byte <(I*15)
	.endrep
	.repeat 128,I
		.byte <((-128+I)*15)
	.endrep
mul15_hi:
	.repeat 128,I
		.byte >(I*15)
	.endrep
	.repeat 128,I
		.byte >((-128+I)*15)
	.endrep
mul120_lo:
	.repeat 128,I
		.byte <(I*120)
	.endrep
	.repeat 128,I
		.byte <((-128+I)*120)
	.endrep
mul120_hi:
	.repeat 128,I
		.byte >(I*120)
	.endrep
	.repeat 128,I
		.byte >((-128+I)*120)
	.endrep
mul8:	.repeat 15,I
		.byte I*8
	.endrep

.export clearFrameBuffer
.proc clearFrameBuffer
	lda #0

	ldx #14
	:
		.repeat 22,I
			sta frameBuffer_screen+I*15,x
		.endrep
		dex
	bpl :-

	ldx #14
	:
		.repeat 22,I
			sta frameBuffer_d800+I*15,x
		.endrep
		dex
	bpl :-

sprites:
	ldy #0
	ldx #20
	:
		lda affectedSpriteColumns,x
		beq :+
			lda #>(return-1)
			pha
			lda #<(return-1)
			pha
			lda BANK
			pha
			lda #>(jsrf_impl-1)
			pha
			lda #<(jsrf_impl-1)
			pha
			lda clearspr_hi,x
			pha
			lda clearspr_lo,x
			pha
			lda clearspr_bank,x
			pha
			jmp jsrf_impl
			return:
		:
		dex
	bpl :--

	rts

clearspr_lo:	.byte <(clearspr0-1),<(clearspr1-1),<(clearspr2-1),<(clearspr3-1),<(clearspr4-1),<(clearspr5-1),<(clearspr6-1),<(clearspr7-1),<(clearspr8-1),<(clearspr9-1),<(clearspr10-1),<(clearspr11-1),<(clearspr12-1),<(clearspr13-1),<(clearspr14-1),<(clearspr15-1),<(clearspr16-1),<(clearspr17-1),<(clearspr18-1),<(clearspr19-1),<(clearspr20-1)
clearspr_hi:	.byte >(clearspr0-1),>(clearspr1-1),>(clearspr2-1),>(clearspr3-1),>(clearspr4-1),>(clearspr5-1),>(clearspr6-1),>(clearspr7-1),>(clearspr8-1),>(clearspr9-1),>(clearspr10-1),>(clearspr11-1),>(clearspr12-1),>(clearspr13-1),>(clearspr14-1),>(clearspr15-1),>(clearspr16-1),>(clearspr17-1),>(clearspr18-1),>(clearspr19-1),>(clearspr20-1)
clearspr_bank:	.byte <.bank(clearspr0),<.bank(clearspr1),<.bank(clearspr2),<.bank(clearspr3),<.bank(clearspr4),<.bank(clearspr5),<.bank(clearspr6),<.bank(clearspr7),<.bank(clearspr8),<.bank(clearspr9),<.bank(clearspr10),<.bank(clearspr11),<.bank(clearspr12),<.bank(clearspr13),<.bank(clearspr14),<.bank(clearspr15),<.bank(clearspr16),<.bank(clearspr17),<.bank(clearspr18),<.bank(clearspr19),<.bank(clearspr20)
.endproc
.export clearFrameBufferSprites = clearFrameBuffer::sprites

.export showFrameBuffer
.proc showFrameBuffer
	lda twomhzMode
	beq :+
		bit $d011
		bpl *-3
	:

.if 0
	lax #0
	:
		.repeat 10,I
		sta frameBuffer_sprites+I*$100,x
		.endrep
		inx
	bne :-
	ldx #$49
	:
		sta frameBuffer_sprites+$a00,x
		dex
	bpl :-
.endif
	jsrf showFrameBuffer0
	jsrf showFrameBuffer1
	jsrf showFrameBuffer2

	rts
.endproc

.macro _showFrameBuffer startRow,rows
	.repeat 22,I
		.repeat rows,J
			lda frameBuffer_screen + 15*I + (J+startRow)
			sta vic_screen + 40*(J+startRow) + I
			
			lda frameBuffer_d800 + 15*I + (J+startRow)
			sta $d800 + 40*(J+startRow) + I
			
			.repeat 8,K
			.scope
				lda frameBuffer_bitmap + 15*8*I + (J+startRow)*8 + K
				sta vic_bitmap + 40*8*(J+startRow) + I*8 + K
			.endscope
			.endrep
		.endrep
	.endrep

	.repeat 22,I
		.repeat rows,J
			.repeat 8,K
			.scope
				chx = I
				line = (J+startRow)*8+K

				.if (chx<21) && (line>10)
					spriteIndex = ((line-1)/21)*7 + chx/3
					spriteOffset = (chx .mod 3) + ((line-1) .mod 21)*3
					lda frameBuffer_sprites + 15*8*I + (J+startRow)*8 + K
					sta vic_sprites + spriteIndex*64 + spriteOffset
				.endif
			.endscope
			.endrep
		.endrep
	.endrep
	rts
.endmacro

.macro _showFrameBufferCols startCol, cols
	.repeat cols,I
		.repeat 15,J
			.scope
				chx = I+startCol

				lda frameBuffer_screen + 15*chx + J
				sta vic_screen + 40*J + chx
				
				lda frameBuffer_d800 + 15*chx + J
				sta $d800 + 40*J + chx
				
				.repeat 8,K
				.scope
					lda frameBuffer_bitmap + 15*8*chx + J*8 + K
					sta vic_bitmap + 40*8*J + chx*8 + K
				.endscope
				.endrep
			.endscope
		.endrep

		.scope
			chx = I+startCol
			lda affectedSpriteColumns+chx
			beq maybeClearColumn
				jmp blitColumn
			maybeClearColumn:
				lda dirtySpriteColumns+chx
				bne clearColumn
					jmp nextColumn
				clearColumn:
				lda #0
				sta dirtySpriteColumns+chx
				.repeat 15,J
					.repeat 8,K
					.scope
						chx = I+startCol
						line = J*8+K

						.if (chx<21) && (line>10)
							spriteIndex = ((line-1)/21)*7 + chx/3
							spriteOffset = (chx .mod 3) + ((line-1) .mod 21)*3
							sta vic_sprites + spriteIndex*64 + spriteOffset
						.endif
					.endscope
					.endrep
				.endrep
				jmp nextColumn
			blitColumn:
				lda #1
				sta dirtySpriteColumns+chx
				.repeat 15,J
					.repeat 8,K
					.scope
						chx = I+startCol
						line = J*8+K

						.if (chx<21) && (line>10)
							spriteIndex = ((line-1)/21)*7 + chx/3
							spriteOffset = (chx .mod 3) + ((line-1) .mod 21)*3
							lda frameBuffer_sprites + 15*8*(I+startCol) + J*8 + K
							sta vic_sprites + spriteIndex*64 + spriteOffset
						.endif
					.endscope
					.endrep
				.endrep
			nextColumn:
		.endscope
	.endrep
	rts
.endmacro

.segment "SHOWFB0"
.proc showFrameBuffer0
	_showFrameBufferCols 0,8
.endproc

.segment "SHOWFB1"
.proc showFrameBuffer1
	_showFrameBufferCols 8,8
.endproc

.segment "SHOWFB2"
.proc showFrameBuffer2
	_showFrameBufferCols 16,6
.endproc

.export darkenFrameBuffer
.proc darkenFrameBuffer
	; Make sure the frame buffer is in OK condition (shared by the scratch area!)
	jsrf render

	lda #<frameBuffer_bitmap
	sta DST+0
	lda #>frameBuffer_bitmap
	sta DST+1
	lda #<frameBuffer_sprites
	sta DST2+0
	lda #>frameBuffer_sprites
	sta DST2+1
	lda #0
	ldx #22
	:
		lda #0
		ldy #1
		:
			lda (DST),y
			and #%11001100
			sta (DST),y
			lda (DST2),y
			and #%11001100
			sta (DST2),y
			iny
			iny
			lda (DST),y
			and #%00110011
			sta (DST),y
			lda (DST2),y
			and #%00110011
			sta (DST2),y
			iny
			iny
			lda (DST),y
			and #%11001100
			sta (DST),y
			lda (DST2),y
			and #%11001100
			sta (DST2),y
			iny
			iny
			lda (DST),y
			and #%00110011
			sta (DST),y
			lda (DST2),y
			and #%00110011
			sta (DST2),y
			iny
			iny
			cpy #15*8+1
		bne :-
		clc
		lda DST+0
		adc #<15*8
		sta DST+0
		lda DST+1
		adc #>15*8
		sta DST+1
		clc
		lda DST2+0
		adc #<15*8
		sta DST2+0
		lda DST2+1
		adc #>15*8
		sta DST2+1
		dex
	bne :--
	jsrf showFrameBuffer
	inc SHOULDRENDER

	rts
.endproc

